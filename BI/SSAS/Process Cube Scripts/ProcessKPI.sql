USE [Warehouse]
GO
/****** Object:  StoredProcedure [SSAS].[SP_ProcessKPI]    Script Date: 18/05/2016 1:47:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROC [SSAS].[SP_ProcessKPI] 
(
	@BatchKey		INT,
	@ProcessType	CHAR(4) -- 'Inc' or 'Full'
)
AS
BEGIN
BEGIN TRY
	DECLARE @Process NVARCHAR(MAX)

	EXEC Warehouse.[Control].Sp_Log	@BatchKey,'SP_ProcessKPI','ProcessTables','Start'

	IF ISNULL(@BatchKey,-1) >=0 AND @ProcessType='Inc'
	BEGIN
		SET @Process=N'<Process xmlns="http://schemas.microsoft.com/analysisservices/2003/engine">
						  <Type>ProcessData</Type>
						  <Object>
							<DatabaseID>Bonza</DatabaseID>
							<CubeID>Model</CubeID>
							<MeasureGroupID>KPI_b9163b76-0007-43d4-9e11-e50a3bc78073</MeasureGroupID>
							<PartitionID>ModifiedBatch_'+CONVERT(VARCHAR(10),@BatchKey)+'</PartitionID>
						  </Object>
						</Process>
						'
	END

	IF ISNULL(@BatchKey,-1) >=0 AND @ProcessType='Full'
	BEGIN
		SET @Process=N'<Process xmlns="http://schemas.microsoft.com/analysisservices/2003/engine">
						  <Type>ProcessData</Type>
						  <Object>
							<DatabaseID>Bonza</DatabaseID>
							<DimensionID>KPI_b9163b76-0007-43d4-9e11-e50a3bc78073</DimensionID>
						  </Object>
						</Process>
						'
	END

	EXEC(@Process) AT [WHTABULARMODEL]

	EXEC Warehouse.[Control].Sp_Log @BatchKey,'SP_ProcessKPI',NULL,'Success'

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE() + ' ' + suser_sname();

	EXEC Warehouse.[Control].Sp_Log @BatchKey, 'SP_ProcessKPI', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

END
USE [Warehouse]
GO
/****** Object:  StoredProcedure [SSAS].[SP_ProcessContract]    Script Date: 18/05/2016 1:47:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROC [SSAS].[SP_ProcessContract] 
(
	@BatchKey		INT,
	@ProcessType	CHAR(4) -- 'Inc' or 'Full'
)
AS
BEGIN
BEGIN TRY
	DECLARE @Process NVARCHAR(MAX)

	EXEC Warehouse.[Control].Sp_Log	@BatchKey,'SP_ProcessContract','ProcessTables','Start'

	IF ISNULL(@BatchKey,-1) >=0 AND @ProcessType='Inc'
	BEGIN
		
		BEGIN TRY DROP TABLE ##PartitionList END TRY BEGIN CATCH END CATCH
		DECLARE @MinPartition INT
		DECLARE @MaxPartition INT

		SELECT DISTINCT P.PartitionNo 
		INTO ##PartitionList
		FROM Warehouse.Dimension.[Contract] C WITH(NOLOCK) 
		INNER JOIN Warehouse.ssas.ContractPartitions P ON C.ContractKey >=ISNULL(P.FromKey,C.ContractKey) AND C.ContractKey <P.ToKey
		WHERE ModifiedBatchKey=@BatchKey

		SELECT @MinPartition = MIN(PartitionNo) FROM ##PartitionList
		SELECT @MaxPartition = MAX(PartitionNo) FROM ##PartitionList

		-- Clear Multiple Partitions

		WHILE (@MinPartition<=@MaxPartition)
		BEGIN
				SET @Process=N'<Process xmlns="http://schemas.microsoft.com/analysisservices/2003/engine">
						  <Type>ProcessClear</Type>
						  <Object>
							<DatabaseID>Bonza</DatabaseID>
							<CubeID>Model</CubeID>
							<MeasureGroupID>Contract_49a6a2b8-6c4e-43ae-97af-827460f1f174</MeasureGroupID>
							<PartitionID>Range_'+CONVERT(VARCHAR(10),@MinPartition)+'</PartitionID>
						  </Object>
						</Process>
						'
				EXEC(@Process) AT [WHTABULARMODEL]

				SELECT @MinPartition = MIN(PartitionNo) FROM ##PartitionList WHERE PartitionNo>@MinPartition

		END

		SELECT @MinPartition = MIN(PartitionNo) FROM ##PartitionList
		SELECT @MaxPartition = MAX(PartitionNo) FROM ##PartitionList

		-- Inc Process Multiple Partitions

		WHILE (@MinPartition<=@MaxPartition)
		BEGIN
				SET @Process=N'<Process xmlns="http://schemas.microsoft.com/analysisservices/2003/engine">
						  <Type>ProcessData</Type>
						  <Object>
							<DatabaseID>Bonza</DatabaseID>
							<CubeID>Model</CubeID>
							<MeasureGroupID>Contract_49a6a2b8-6c4e-43ae-97af-827460f1f174</MeasureGroupID>
							<PartitionID>Range_'+CONVERT(VARCHAR(10),@MinPartition)+'</PartitionID>
						  </Object>
						</Process>
						'
				EXEC(@Process) AT [WHTABULARMODEL]

				SELECT @MinPartition = MIN(PartitionNo) FROM ##PartitionList WHERE PartitionNo>@MinPartition

		END


	END

	IF ISNULL(@BatchKey,-1) >=0 AND @ProcessType='Full'
	BEGIN
		SET @Process=N'<Process xmlns="http://schemas.microsoft.com/analysisservices/2003/engine">
						  <Type>ProcessData</Type>
						  <Object>
							<DatabaseID>Bonza</DatabaseID>
							<DimensionID>Contract_49a6a2b8-6c4e-43ae-97af-827460f1f174</DimensionID>
						  </Object>
						</Process>
						'
		EXEC(@Process) AT [WHTABULARMODEL]
	END

	
	EXEC Warehouse.[Control].Sp_Log @BatchKey,'SP_ProcessContract',NULL,'Success'

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE() + ' ' + suser_sname();

	EXEC Warehouse.[Control].Sp_Log @BatchKey, 'SP_ProcessContract', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

END
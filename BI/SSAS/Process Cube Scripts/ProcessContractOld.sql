USE [Warehouse]
GO
/****** Object:  StoredProcedure [SSAS].[SP_ProcessContractOld]    Script Date: 18/05/2016 1:47:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROC [SSAS].[SP_ProcessContractOld] 
(
	@BatchKey		INT,
	@ProcessType	CHAR(4) -- 'Inc' or 'Full'
)
AS
BEGIN
BEGIN TRY
	DECLARE @Process NVARCHAR(MAX)

	EXEC Warehouse.[Control].Sp_Log	@BatchKey,'SP_ProcessContract','ProcessTables','Start'

	IF ISNULL(@BatchKey,-1) >=0 AND @ProcessType='Inc'
	BEGIN
		SET @Process=N'<Process xmlns="http://schemas.microsoft.com/analysisservices/2003/engine">
						  <Type>ProcessFull</Type>
						  <Object>
							<DatabaseID>Bonza</DatabaseID>
							<CubeID>Model</CubeID>
							<MeasureGroupID>Contract_49a6a2b8-6c4e-43ae-97af-827460f1f174</MeasureGroupID>
							<PartitionID>ModifiedBatch_'+CONVERT(VARCHAR(10),@BatchKey)+'</PartitionID>
						  </Object>
						</Process>
						'
	END

	IF ISNULL(@BatchKey,-1) >=0 AND @ProcessType='Full'
	BEGIN
		SET @Process=N'<Process xmlns="http://schemas.microsoft.com/analysisservices/2003/engine">
						  <Type>ProcessFull</Type>
						  <Object>
							<DatabaseID>Bonza</DatabaseID>
							<DimensionID>Contract_49a6a2b8-6c4e-43ae-97af-827460f1f174</DimensionID>
						  </Object>
						</Process>
						'
	END

	EXEC(@Process) AT [WHTABULARMODEL]

	EXEC Warehouse.[Control].Sp_Log @BatchKey,'SP_ProcessContract',NULL,'Success'

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE() + ' ' + suser_sname();

	EXEC Warehouse.[Control].Sp_Log @BatchKey, 'SP_ProcessContract', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

END
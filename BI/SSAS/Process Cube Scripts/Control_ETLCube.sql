USE [Warehouse]
GO
/****** Object:  StoredProcedure [Control].[ETLCube]    Script Date: 18/05/2016 1:46:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [Control].[ETLCube]
	(
		@BatchKey		BIGINT	= NULL
	)
AS 

BEGIN

		BEGIN TRY

			DECLARE @item NVARCHAR(100)=''

			IF ISNUMERIC(@BatchKey)<>1																									-- Auto Run - Pick next best batch to run
				SELECT @BatchKey=MIN(BatchKey) 
				FROM 
				(	SELECT BatchKey 
					FROM [Staging].[Control].[ETLController] WITH(NOLOCK)
					WHERE (TransactionStatus=1 AND KPIFactStatus=1 AND PositionFactStatus=1 AND BalanceFactStatus=1) 
					EXCEPT
					SELECT BatchKey 
					FROM Warehouse.[Control].[ETLCubeController] WITH(NOLOCK)
					WHERE ISNULL(CubeStatus,0)=1
				) X
				
			IF ISNUMERIC(@BatchKey)=1																									-- Only ProcessCube if there is an Cube batch that needs to be processed.
				BEGIN

					UPDATE [Control].[ETLCubeController]
					SET CubeStatus		=NULL,
						CubeStartDate	=GETDATE(),
						CubeEndDate		=NULL,
						ErrorMessage	=NULL
					WHERE BatchKey		=@BatchKey

					INSERT INTO [Control].[ETLCubeController] (BatchKey,BatchFromDate,BatchToDate,CubeStatus,CubeStartDate,CubeEndDate,ErrorMessage)
					SELECT A.BatchKey,A.BatchFromDate,A.BatchToDate,NULL,GETDATE(),NULL,NULL
					FROM [Staging].[Control].[ETLController] A WITH(NOLOCK) 
					LEFT OUTER JOIN [Control].[ETLCubeController] B WITH(NOLOCK) ON A.BatchKey=B.BatchKey
					WHERE A.BatchKey=@BatchKey
					AND B.BatchKey IS NULL
					
					SET @item='SP_ProcessAccount'
					EXEC [Warehouse].[SSAS].[SP_ProcessAccount]				@BatchKey,'Full'				-- Type 1/2 hence full process
					
					SET @item='SP_ProcessBalance'															-- Fact table inc process
					EXEC [Warehouse].[SSAS].[SP_ProcessBalance]				@BatchKey,'Inc'		
					
					SET @item='SP_ProcessBetType'															-- Type 1/2 hence full process
					EXEC [Warehouse].[SSAS].[SP_ProcessBetType]				@BatchKey,'Full'				
				
					SET @item='SP_ProcessContract'															-- Type 0 hence increment process
					EXEC [Warehouse].[SSAS].[SP_ProcessContract]			@BatchKey,'Inc'	
					
					SET @item='SP_ProcessInstrument'														-- Type 1/2 hence full process
					EXEC [Warehouse].[SSAS].[SP_ProcessInstrument]			@BatchKey,'Full'	

					SET @item='SP_ProcessInstrumentSecure'													-- Type 1/2 hence full process
					EXEC [Warehouse].[SSAS].[SP_ProcessInstrumentSecure]	@BatchKey,'Full'		
					
					SET @item='SP_ProcessKPI'																-- Fact table inc process
					EXEC [Warehouse].[SSAS].[SP_ProcessKPI]					@BatchKey,'Inc'		
					
					SET @item='SP_ProcessMarket'															-- Type 1/2 hence full process
					EXEC [Warehouse].[SSAS].[SP_ProcessMarket]				@BatchKey,'Full'								
																
					SET @item='SP_ProcessPosition'															-- Fact table inc process
					EXEC [Warehouse].[SSAS].[SP_ProcessPosition]			@BatchKey,'Inc'	
					
					SET @item='SP_ProcessTransaction'														-- Fact table inc process
					EXEC [Warehouse].[SSAS].[SP_ProcessTransaction]			@BatchKey,'Inc'			
					
					SET @item='SP_ProcessWallet'															-- Type 1/2 hence full process
					EXEC [Warehouse].[SSAS].[SP_ProcessWallet]				@BatchKey,'Full'		
																		
					SET @item='SP_ProcessUser'																-- Type 1/2 hence full process
					EXEC [Warehouse].[SSAS].[SP_ProcessUser]				@BatchKey,'Full'		

					SET @item='SP_ProcessRecalc'															
					EXEC [Warehouse].[SSAS].[SP_ProcessRecalc]				@BatchKey	


			
					UPDATE [Control].[ETLCubeController] SET CubeEndDate=GETDATE()	WHERE BatchKey=@BatchKey				-- Set EndTime for Cube
					UPDATE [Control].[ETLCubeController] SET CubeStatus=1			WHERE BatchKey=@BatchKey				-- Set 'Success' RunStatus for Cube
				
					DECLARE @CompleteDate	DATETIME2
					SELECT @CompleteDate=	MAX(MASTERTRANSACTIONTIMESTAMP) 
											FROM Dimensional.Fact.[Transaction] T
											INNER JOIN Dimensional.Dimension.Day D WITH(NOLOCK)	ON T.DayKey=D.DayKey
											WHERE T.CreatedBatchKey=@BatchKey
											AND D.DayDate>=GETDATE()-10

					UPDATE Warehouse.[Control].[CubeCompleteDate]									
					SET CompleteDate=ISNULL(@CompleteDate,CompleteDate)

				END

		END TRY
		
		BEGIN CATCH
			UPDATE [Control].[ETLCubeController] SET CubeEndDate=GETDATE()	WHERE BatchKey=@BatchKey									-- Set EndTime for Cube
			UPDATE [Control].[ETLCubeController] SET CubeStatus=-1			WHERE BatchKey=@BatchKey;									-- Set 'Failure' RunStatus for Cube
			UPDATE [Control].[ETLCubeController] SET ErrorMessage = @item + ' Failed with Error Message - ' + ERROR_MESSAGE() WHERE BatchKey=@BatchKey;
			THROW;
		END CATCH

END
USE [Warehouse]
GO
/****** Object:  StoredProcedure [SSAS].[SP_ProcessNote]    Script Date: 18/05/2016 1:48:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROC [SSAS].[SP_ProcessNote] 
(
	@BatchKey		INT,
	@ProcessType	CHAR(4) -- 'Inc' or 'Full'
)
AS
BEGIN
BEGIN TRY
	DECLARE @Process NVARCHAR(MAX)

	EXEC Warehouse.[Control].Sp_Log	@BatchKey,'SP_ProcessNote','ProcessTables','Start'

	--IF ISNULL(@BatchKey,-1) >=0 AND @ProcessType='Inc'
	--BEGIN
	--	--SET @Process=N'<Process xmlns="http://schemas.microsoft.com/analysisservices/2003/engine">
	--	--				  <Type>ProcessData</Type>
	--	--				  <Object>
	--	--					<DatabaseID>Bonza</DatabaseID>
	--	--					<CubeID>Model</CubeID>
	--	--					<MeasureGroupID>Note_21a474aa-971b-45db-ba1d-c3cdf39e5e57</MeasureGroupID>
	--	--					<PartitionID>ModifiedBatch_'+CONVERT(VARCHAR(10),@BatchKey)+'</PartitionID>
	--	--				  </Object>
	--	--				</Process>
	--	--				'
	--END

	IF ISNULL(@BatchKey,-1) >=0 AND @ProcessType='Full'
	BEGIN
		SET @Process=N'<Process xmlns="http://schemas.microsoft.com/analysisservices/2003/engine">
						  <Type>ProcessData</Type>
						  <Object>
							<DatabaseID>Bonza</DatabaseID>
							<DimensionID>Note_9a29a329-462d-42ef-984d-3b722e6c0f22</DimensionID>
						  </Object>
						</Process>
						'
	END

	EXEC(@Process) AT [WHTABULARMODEL]

	EXEC Warehouse.[Control].Sp_Log @BatchKey,'SP_ProcessNote',NULL,'Success'

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE() + ' ' + suser_sname();

	EXEC Warehouse.[Control].Sp_Log @BatchKey, 'SP_ProcessNote', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

END
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CRMManager
{
    public partial class ChaseThePointsAFL : Global
    {
        public int iRow;
        public string user;

        public ChaseThePointsAFL()
        {
            InitializeComponent();

            lblLoggedIn.Text = "Logged in as : " + NameoftheUser;


        } 

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'bIDDataSet.DIM_CRM_Manager_ChaseThePointsAFL' table. You can move, or remove it, as needed.
            this.dIM_CRM_Manager_ChaseThePointsAFLTableAdapter.Fill(this.bIDDataSet.DIM_CRM_Manager_ChaseThePointsAFL);
            // TODO: This line of code loads data into the 'bIDDataSet.Competitions' table. You can move, or remove it, as needed.
            this.competitionsTableAdapter.Fill(this.bIDDataSet.Competitions);

            cbCompetitions.SelectedIndex = -1;
            

            //tsCopy.Click += tsCopy_Click;
            tsDelete.Click += btnDelete_Click;

        }


        private void Save_Click(object sender, EventArgs e)
        {
                    DataRow drRow = this.bIDDataSet.DIM_CRM_Manager_ChaseThePointsAFL.NewRow();
                    try
                    {
                        if (IsValidated())
                        {



                            drRow["EventDate"] = dtpDate.Value;
                            drRow["MasterEventId"] = cbCompetitions.SelectedValue.ToString();
                            drRow["Eventid"] = cbMatch.SelectedValue.ToString();
                            drRow["EventName"] = (((DataRowView)(cbMatch.SelectedItem)).Row).ItemArray[1].ToString();
                            drRow["CompetitorId"] = cbPlayer.SelectedValue.ToString();
                            drRow["CompetitorName"] = (((DataRowView)(cbPlayer.SelectedItem)).Row).ItemArray[1].ToString();
                            drRow["Goals"] = txtGoals.Text.ToString();
                            drRow["Behinds"] = txtBehinds.Text.ToString();
                            drRow["EnteredDate"] = DateTime.Now;
                            drRow["EnteredBy"] = UserName;

                            bIDDataSet.DIM_CRM_Manager_ChaseThePointsAFL.Rows.Add(drRow);

                            this.dIM_CRM_Manager_ChaseThePointsAFLTableAdapter.Update(bIDDataSet);

                            ClearFields();

                            this.dIM_CRM_Manager_ChaseThePointsAFLTableAdapter.Fill(this.bIDDataSet.DIM_CRM_Manager_ChaseThePointsAFL);



                            btnClear.Visible = true;
                            btnDelete.Visible = true;
                            btnEdit.Visible = true;
                            btnRefresh.Visible = true;

                        }

                    }

                    catch (System.Data.SqlClient.SqlException ex)
                    {
                        if (ex.GetBaseException().Message.Contains("insert duplicate key"))
                        {
                            MessageBox.Show("You have entered duplicate entry. Please enter the record again.", "CRM Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            bIDDataSet.DIM_CRM_Manager_ChaseThePointsAFL.Rows.Remove(drRow);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.GetBaseException().Message, "CRM Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
        }

        private void dgvVIP_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvVIP.CurrentCell != null)
                iRow = dgvVIP.CurrentCell.RowIndex;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {

                DataRow drRowDelete = this.bIDDataSet.DIM_CRM_Manager_ChaseThePointsAFL.Rows[iRow];               



                dtpDate.Value = (DateTime)drRowDelete["EventDate"];
                cbCompetitions.SelectedValue = (int)drRowDelete["MasterEventId"];
                cbMatch.SelectedValue =  (int)drRowDelete["Eventid"];
                cbPlayer.SelectedValue = (int)drRowDelete["CompetitorId"];
                txtBehinds.Text = drRowDelete["Behinds"].ToString();
                txtGoals.Text = drRowDelete["Goals"].ToString();



                this.dIM_CRM_Manager_ChaseThePointsAFLTableAdapter.DeletePlayer((int)(drRowDelete["Eventid"]), (int)(drRowDelete["CompetitorId"]));

                this.bIDDataSet.DIM_CRM_Manager_ChaseThePointsAFL.Rows.Remove(drRowDelete);

                this.dIM_CRM_Manager_ChaseThePointsAFLTableAdapter.Update(bIDDataSet);

                btnClear.Visible = false;
                btnDelete.Visible = false;
                btnEdit.Visible = false;
                btnRefresh.Visible = false;
            }


            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "CRM Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("mailto:BI.Service.Reports@williamhill.com.au");
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            ClearFields();

            cbCompetitions.SelectedIndex = -1;
            cbMatch.SelectedIndex = -1;
            dtpDate.Value = DateTime.Today;

        }

        private void ClearFields()
        {
            txtBehinds.Text = "";
            txtGoals.Text = "";
            cbPlayer.SelectedIndex = -1;
        }

        private bool IsValidated()
        {
            if (cbCompetitions.SelectedValue == null)
            {
                MessageBox.Show("Please select a Competition.", "CRM Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            else if (cbMatch.SelectedValue == null)
            {
                MessageBox.Show("Please select a Match.", "CRM Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            else if (cbPlayer.SelectedValue == null)
            {
                MessageBox.Show("Please select a Player.", "CRM Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            else if (txtBehinds.Text.Trim() == "")
            {
                MessageBox.Show("Please enter the behinds value.", "CRM Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            else if (txtGoals.Text.Trim() == "")
            {
                MessageBox.Show("Please enter the goals value", "CRM Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            else return true;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {

                if (MessageBox.Show("Do you want to delete the highlighted record ?", "CRM Manager", MessageBoxButtons.YesNo, MessageBoxIcon.Information) 
                    == System.Windows.Forms.DialogResult.Yes)
                {
                    DataRow drRowDelete = this.bIDDataSet.DIM_CRM_Manager_ChaseThePointsAFL.Rows[iRow];   

                    this.dIM_CRM_Manager_ChaseThePointsAFLTableAdapter.DeletePlayer((int)(drRowDelete["Eventid"]), (int)(drRowDelete["CompetitorId"]));

                    this.bIDDataSet.DIM_CRM_Manager_ChaseThePointsAFL.Rows.Remove(drRowDelete);

                    this.dIM_CRM_Manager_ChaseThePointsAFLTableAdapter.Update(bIDDataSet);
                }

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "CRM Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void tsCopy_Click(object sender, EventArgs e)
        {
            Clipboard.SetDataObject(dgvVIP.GetClipboardContent());
        }

        private void dtpDate_ValueChanged(object sender, EventArgs e)
        {
            if (cbCompetitions.SelectedValue == null)
                return;

            this.matchesTableAdapter.Fill(this.bIDDataSet.Matches, (int)cbCompetitions.SelectedValue, dtpDate.Value.ToString("yyyy-MM-dd"));

            if (cbMatch.SelectedValue != null)
                this.competitorsTableAdapter.Fill(this.bIDDataSet.Competitors, (int)cbMatch.SelectedValue);
        }

        private void cbCompetitions_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (cbCompetitions.SelectedValue == null)
                return;

            this.matchesTableAdapter.Fill(this.bIDDataSet.Matches, (int)cbCompetitions.SelectedValue, dtpDate.Value.ToString("yyyy-MM-dd"));

            if (cbMatch.SelectedValue != null)
                this.competitorsTableAdapter.Fill(this.bIDDataSet.Competitors, (int)cbMatch.SelectedValue);
        }

        private void cbMatch_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (cbMatch.SelectedValue == null)
                return;

            this.competitorsTableAdapter.Fill(this.bIDDataSet.Competitors, (int)cbMatch.SelectedValue);
        }

        private void txtBehinds_TextChanged(object sender, EventArgs e)
        {
            int Behinds;
            if (!int.TryParse(txtBehinds.Text, out Behinds) && txtBehinds.Text != "")
            {
                MessageBox.Show("Entered Behinds value is not valid.", "CRM Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void txtGoals_TextChanged(object sender, EventArgs e)
        {
            int Goals;
            if (!int.TryParse(txtGoals.Text, out Goals) && txtBehinds.Text != "")
            {
                MessageBox.Show("Entered Goals value is not valid.", "CRM Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            this.dIM_CRM_Manager_ChaseThePointsAFLTableAdapter.Fill(this.bIDDataSet.DIM_CRM_Manager_ChaseThePointsAFL);
        }
    }
}

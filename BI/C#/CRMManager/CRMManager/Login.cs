﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CRMManager
{
    public partial class Login : Global
    {
        public Login()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            BIDDataSet.DIM_CRM_Manager_UserDataTable dtTable =
              this.dIM_CRM_Manager_UserTableAdapter.GetDataByUser(txtUserName.Text.Trim(), txtPassword.Text.Trim());

            if (dtTable.Rows.Count == 0)
            {
                MessageBox.Show("Login Failed");
                DialogResult = System.Windows.Forms.DialogResult.None;   
               
            }
           else
            {
                try
                {

                    UserName = txtUserName.Text.Trim();
                    NameoftheUser = dtTable.Rows[0]["Name"].ToString();
                    Role = dtTable.Rows[0]["Role"].ToString();
                    DialogResult = System.Windows.Forms.DialogResult.OK;

                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.GetBaseException().Message);
                }
            }
        }

        private void Login_Load(object sender, EventArgs e)
        {
           
            this.dIM_CRM_Manager_UserTableAdapter.Fill(this.bIDDataSet.DIM_CRM_Manager_User);

        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtUserName.Text = "";
            txtPassword.Text = "";
        }

        private void Login_Leave(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CRMManager
{
    public partial class AdminUsers : Form
    {

        private int iRow;
        private string updateUser;

        public AdminUsers()
        {
            InitializeComponent();
        }

        private void AdminUsers_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'bIDDataSet.DIM_VIP_Dashboard_User' table. You can move, or remove it, as needed.
            this.dIM_CRM_Manager_UserTableAdapter.Fill(this.bIDDataSet.DIM_CRM_Manager_User);

            this.tsDelete.Click += tsDelete_Click;

        }

        private void tsDelete_Click(object sender, EventArgs e)
        {
            try
            {

                if (MessageBox.Show("Do you want to delete the highlighted record ?", "CRM Manager", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
                    == System.Windows.Forms.DialogResult.Yes)
                {
                    DataRow drRowDelete = this.bIDDataSet.DIM_CRM_Manager_User.Rows[iRow];

                    this.dIM_CRM_Manager_UserTableAdapter.DeleteUser(drRowDelete["UserName"].ToString());

                    this.dIM_CRM_Manager_UserTableAdapter.Update(bIDDataSet);

                    this.dIM_CRM_Manager_UserTableAdapter.Fill(this.bIDDataSet.DIM_CRM_Manager_User);
                }

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "CRM Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                if (txtUserName.Text.Trim() == "")
                {
                    MessageBox.Show("Entered User Name is not valid.", "CRM Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else if (txtPassword.Text.Trim() == "")
                {
                    MessageBox.Show("Password cannot be blank.", "CRM Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else if (txtReType.Text.Trim() == "")
                {
                    MessageBox.Show("Retype Password cannot be blank.", "CRM Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else if (txtPassword.Text.Trim() != txtReType.Text.Trim())
                {
                    MessageBox.Show("Entered passwords are not matched.Please Re-Enter the passwords again.", "CRM Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtPassword.Text = "";
                    txtReType.Text = "";

                    return;
                }
                else if (txtName.Text.Trim() == "")
                {
                    MessageBox.Show("Name cannot be blank.", "CRM Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else if (cmbRole.SelectedIndex == -1)
                {
                    MessageBox.Show("Please select a Role.", "CRM Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }




                if (updateUser != "" && updateUser != null)
                {

                    this.dIM_CRM_Manager_UserTableAdapter.UpdateUser(txtUserName.Text.Trim(), txtPassword.Text.Trim(), txtName.Text.Trim(),cmbRole.SelectedItem.ToString(),updateUser);

                    this.dIM_CRM_Manager_UserTableAdapter.Update(bIDDataSet);

                }
                else
                {
                    DataRow drRow = this.bIDDataSet.DIM_CRM_Manager_User.NewRow();

                    drRow["UserName"] = txtUserName.Text.Trim();
                    drRow["Pword"] = txtPassword.Text.Trim();
                    drRow["Name"] = txtName.Text.Trim();
                    drRow["Role"] = cmbRole.SelectedItem.ToString();

                    bIDDataSet.DIM_CRM_Manager_User.Rows.Add(drRow);

                    this.dIM_CRM_Manager_UserTableAdapter.Update(bIDDataSet);
                }

                ClearFields();
                this.dIM_CRM_Manager_UserTableAdapter.Fill(this.bIDDataSet.DIM_CRM_Manager_User);

                btnEdit.Visible = true;
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "CRM Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {

                DataRow drRowEdit = this.bIDDataSet.DIM_CRM_Manager_User.Rows[iRow];

                txtUserName.Text = drRowEdit["UserName"].ToString();
                txtPassword.Text = drRowEdit["Pword"].ToString();
                txtReType.Text = drRowEdit["Pword"].ToString();
                txtName.Text = drRowEdit["Name"].ToString();
                cmbRole.SelectedItem = drRowEdit["Role"];

                updateUser = drRowEdit["UserName"].ToString();

                btnEdit.Visible = false;
            }


            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "CRM Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void ClearFields()
        {
            txtUserName.Text = "";
            txtPassword.Text = "";
            txtReType.Text = "";
            txtName.Text = "";
            cmbRole.SelectedIndex = -1;
            updateUser = "";
        }

        private void dgvUsers_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvUsers.CurrentCell != null)
                iRow = dgvUsers.CurrentCell.RowIndex;
        }
    }
}

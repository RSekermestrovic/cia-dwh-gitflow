﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BIDM
{
    public partial class GiftingHospitality : Form
    {
        private int iRow, iClientId;
        private long iGiftingId;

        public GiftingHospitality()
        {
            InitializeComponent();

            lblLoggedIn.Text = "Logged in as : " + Global.NameoftheUser;
        }

        private void GiftingHospitality_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'bIDMDataSet.ClientGifting' table. You can move, or remove it, as needed.
            this.clientGiftingTableAdapter.Fill(this.bIDMDataSet.ClientGifting, Global.UserId);

            var dtTable = categoryTableAdapter.GetDataByGifting();

            cbCategory.ValueMember = "CategoryId";
            cbCategory.DisplayMember = "CategoryName";

            cbCategory.DataSource = dtTable;

            var dtUserTable = userTableAdapter.GetDataByVIPManager();

            cbVIPManager.ValueMember = "UserId";
            cbVIPManager.DisplayMember = "Name";

            cbVIPManager.DataSource = dtUserTable;

            cbCategory.SelectedIndex = -1;
            cbVIPManager.SelectedValue = Global.UserId;

            tsCopy.Click += tsCopy_Click;
            tsDelete.Click += btnDelete_Click;
        }

        private void dgvVIP_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvVIP.CurrentCell != null)
                iRow = dgvVIP.CurrentCell.RowIndex;
        }

        private void txtPIN_TextChanged(object sender, EventArgs e)
        {
            int PIN;
            if (!int.TryParse(txtPIN.Text, out PIN) && txtPIN.Text != "")
            {
                MessageBox.Show("Entered PIN is not valid.", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void txtValue_TextChanged(object sender, EventArgs e)
        {
            double Value;
            if (!double.TryParse(txtValue.Text, out Value) && txtValue.Text != "")
            {
                MessageBox.Show("Entered Value is not valid.", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsValidated())
                {
                    var dtClient = clientsTableAdapter.GetClientByPIN(Convert.ToInt32(txtPIN.Text.Trim()));

                    if (dtClient.Rows[0]["ClientId"] != DBNull.Value)
                        iClientId = (int)dtClient.Rows[0]["ClientId"];
                    else
                    {
                        MessageBox.Show("Client not found! Please change the Client PIN ", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }


                    if (iGiftingId != 0)
                    {
                        clientGiftingTableAdapter.UpdateGifting(dtpDate.Value, Convert.ToInt32(txtPIN.Text.Trim()), iClientId, (int)cbCategory.SelectedValue,
                          txtGift.Text.Trim(), Convert.ToDecimal(txtValue.Text.Trim()), txtComments.Text.Trim(), (int)cbVIPManager.SelectedValue, DateTime.Now, Global.UserId,
                              iGiftingId);

                        btnClear.Visible = true;
                        btnDelete.Visible = true;
                        btnEdit.Visible = true;
                    }
                    else
                    {
                        clientGiftingTableAdapter.InsertGifting(dtpDate.Value, Convert.ToInt32(txtPIN.Text.Trim()), iClientId, (int)cbCategory.SelectedValue,
                         txtGift.Text.Trim(), Convert.ToDecimal(txtValue.Text.Trim()), txtComments.Text.Trim(), DateTime.Now, Global.UserId, (int)cbVIPManager.SelectedValue, DateTime.Now, Global.UserId);

                    }

                    ClearFields();

                    this.clientGiftingTableAdapter.Fill(this.bIDMDataSet.ClientGifting, Global.UserId);

                }

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {

                DataRow drRowDelete = this.bIDMDataSet.ClientGifting.Rows[iRow];

                dtpDate.Value = (DateTime)drRowDelete["Date"];
                txtPIN.Text = drRowDelete["PIN"].ToString();
                txtGift.Text = drRowDelete["Gift"].ToString();
                txtValue.Text = drRowDelete["Value"].ToString();
                cbCategory.SelectedValue = (int)drRowDelete["CategoryId"];
                txtComments.Text = drRowDelete["Comments"].ToString();
                cbVIPManager.SelectedValue = (int)drRowDelete["VIPManagerUserId"];


                iGiftingId = (long)drRowDelete["GiftingId"];


                btnClear.Visible = false;
                btnDelete.Visible = false;
                btnEdit.Visible = false;
            }


            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private bool IsValidated()
        {
            if (txtPIN.Text.Trim() == "")
            {
                MessageBox.Show("Entered PIN is not valid.", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            else if (txtGift.Text.Trim() == "")
            {
                MessageBox.Show("Please enter the gift.", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            else if (txtValue.Text.Trim() == "")
            {
                MessageBox.Show("Please enter the Value.", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            else if (cbCategory.SelectedValue == null)
            {
                MessageBox.Show("Please select a category.", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            else if (cbVIPManager.SelectedValue == null)
            {
                MessageBox.Show("Please select a BI Data Manager.", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            else return true;
        }


        private void btnClear_Click(object sender, EventArgs e)
        {
            ClearFields();
        }

        private void ClearFields()
        {
            dtpDate.Value = DateTime.Today;
            txtGift.Text = "";
            txtPIN.Text = "";
            cbCategory.SelectedIndex = -1;
            txtValue.Text = "";
            txtComments.Text = "";
            cbVIPManager.SelectedValue = Global.UserId;
            iGiftingId = 0;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {

                if (MessageBox.Show("Do you want to delete the highlighted record ?", "BI Data Manager", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
                    == System.Windows.Forms.DialogResult.Yes)
                {
                    DataRow drRowDelete = this.bIDMDataSet.ClientGifting.Rows[iRow];

                    this.clientGiftingTableAdapter.DeleteGifting((long)(drRowDelete["GiftingId"]));

                    this.clientGiftingTableAdapter.Fill(this.bIDMDataSet.ClientGifting, Global.UserId);

                }

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void tsCopy_Click(object sender, EventArgs e)
        {
            Clipboard.SetDataObject(dgvVIP.GetClipboardContent());
        }

        private void linkLabel1_Click(object sender, EventArgs e)
        {
            Process.Start("mailto:BI.Service.Reports@williamhill.com.au");
        }

    }
}

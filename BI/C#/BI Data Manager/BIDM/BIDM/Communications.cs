﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BIDM
{
    public partial class Communications : Form
    {
        private int iRow,  iClientId;
        private long iCommunicationId;
        private bool bSuccess = false;


        public Communications()
        {
            InitializeComponent();

            lblLoggedIn.Text = "Logged in as : " + Global.NameoftheUser;
          
        }



        private void Form1_Load(object sender, EventArgs e)
        {

            // TODO: This line of code loads data into the 'bIDMDataSet.ClientCommunication' table. You can move, or remove it, as needed.
            this.clientCommunicationTableAdapter.Fill(this.bIDMDataSet.ClientCommunication, Global.UserId,9999999);

            this.directionTableAdapter.Fill(this.bIDMDataSet.Direction);

            this.channelTableAdapter.Fill(this.bIDMDataSet.Channel);

    
            var dtTable = categoryTableAdapter.GetDataByCommunication();

            cbCategory.ValueMember = "CategoryId";
            cbCategory.DisplayMember = "CategoryName"; 

            cbCategory.DataSource = dtTable;

            var dtUserTable = userTableAdapter.GetDataByVIPManager();

            cbVIPManager.ValueMember = "UserId";
            cbVIPManager.DisplayMember = "Name";

            cbVIPManager.DataSource = dtUserTable;
           
            cbChannel.SelectedIndex = -1;
            cbCategory.SelectedIndex = -1;
            cbDirection.SelectedIndex = -1;
            cbVIPManager.SelectedValue = Global.UserId;

            tsCopy.Click += tsCopy_Click;
            tsDelete.Click += btnDelete_Click;

        }


        private void Save_Click(object sender, EventArgs e)
        {

            try
            {
                if (IsValidated())
                {
                    var dtClient = clientsTableAdapter.GetClientByPIN(Convert.ToInt32(txtPIN.Text.Trim()));

                    if (dtClient.Rows[0]["ClientId"] != DBNull.Value)
                        iClientId = (int)dtClient.Rows[0]["ClientId"];
                    else
                    {
                        MessageBox.Show("Client not found! Please change the Client PIN ", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    if (cbSuccess.SelectedItem.ToString() == "Yes")
                        bSuccess = true;
                    else
                        bSuccess = false;

                    if (iCommunicationId != 0)
                    {                        
                        clientCommunicationTableAdapter.UpdateComms(dtpDate.Value, Convert.ToInt32(txtPIN.Text.Trim()), iClientId, (int)cbChannel.SelectedValue,
                            (int)cbDirection.SelectedValue, (int)cbCategory.SelectedValue, bSuccess, txtComments.Text.Trim(), (int)cbVIPManager.SelectedValue, DateTime.Now, Global.UserId,
                                iCommunicationId);

                        btnClear.Visible = true;
                        btnDelete.Visible = true;
                        btnEdit.Visible = true;
                       
                    }
                    else
                    {
                        clientCommunicationTableAdapter.InsertComms(dtpDate.Value, Convert.ToInt32(txtPIN.Text.Trim()), iClientId, (int)cbChannel.SelectedValue,
                            (int)cbDirection.SelectedValue, bSuccess, txtComments.Text.Trim(), DateTime.Now, Global.UserId, (int)cbVIPManager.SelectedValue,
                                DateTime.Now, Global.UserId, (int)cbCategory.SelectedValue);    
                    }

                    ClearFields();

                    this.clientCommunicationTableAdapter.Fill(this.bIDMDataSet.ClientCommunication, Global.UserId, 9999999);
                }

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvVIP_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvVIP.CurrentCell != null)
                iRow = dgvVIP.CurrentCell.RowIndex;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                DataRow drRowDelete = this.bIDMDataSet.ClientCommunication.Rows[iRow];

                dtpDate.Value = (DateTime)drRowDelete["Date"];
                txtPIN.Text = drRowDelete["PIN"].ToString();
                cbChannel.SelectedValue = (int)drRowDelete["ChannelId"];
                cbCategory.SelectedValue = (int)drRowDelete["CategoryId"];
                cbSuccess.SelectedItem = drRowDelete["IsSuccess"].ToString();
                txtComments.Text = drRowDelete["Comments"].ToString();
                cbVIPManager.SelectedValue = (int)drRowDelete["VIPManagerUserId"];
                cbDirection.SelectedValue = (int)drRowDelete["DirectionId"];

                iCommunicationId = (long)drRowDelete["CommunicationId"];

                btnClear.Visible = false;
                btnDelete.Visible = false;
                btnEdit.Visible = false;
            }


            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("mailto:BI.Service.Reports@williamhill.com.au");
        }

        private void txtPIN_TextChanged(object sender, EventArgs e)
        {
            int PIN;
            if (!Int32.TryParse(txtPIN.Text, out PIN) && txtPIN.Text != "")
            {
                MessageBox.Show("Entered PIN is not valid.", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtPIN.Text = "";
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            ClearFields();
        }

        private void ClearFields()
        {
            dtpDate.Value = DateTime.Today;
            txtPIN.Text = "";
            cbChannel.SelectedIndex = -1;
            cbCategory.SelectedIndex = -1;
            cbSuccess.SelectedIndex = -1;
            txtComments.Text = "";
            cbDirection.SelectedIndex = -1;
            cbVIPManager.SelectedValue = Global.UserId;
            iCommunicationId = 0;
        }

        private bool IsValidated()
        {
            if (txtPIN.Text.Trim() == "")
            {
                MessageBox.Show("Entered PIN is not valid.", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            else if (txtComments.Text.Trim() == "")
            {
                MessageBox.Show("Please enter some comments.", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            else if (cbChannel.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a channel.", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            else if (cbCategory.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a category.", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            else if (cbVIPManager.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a VIP Manager.", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            else if (cbSuccess.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a success.", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            else if (cbDirection.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a direction.","BI Data Manager",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                return false;
            }
            else return true;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {

                if (MessageBox.Show("Do you want to delete the highlighted record ?", "BI Data Manager", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
                    == System.Windows.Forms.DialogResult.Yes)
                {
                    DataRow drRowDelete = this.bIDMDataSet.ClientCommunication.Rows[iRow];

                    this.clientCommunicationTableAdapter.DeleteComms((long)(drRowDelete["CommunicationId"]));

                    this.clientCommunicationTableAdapter.Fill(this.bIDMDataSet.ClientCommunication, Global.UserId, 9999999);
                }

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void tsCopy_Click(object sender, EventArgs e)
        {
            Clipboard.SetDataObject(dgvVIP.GetClipboardContent());
        }
    }
}

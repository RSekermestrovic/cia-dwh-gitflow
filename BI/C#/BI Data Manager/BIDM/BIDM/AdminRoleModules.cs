﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BIDM
{
    public partial class AdminRoleModules : Form
    {

        private int iRow;
        private int moduleId, roleId;

        public AdminRoleModules()
        {
            InitializeComponent();
        }

        private void AdminModules_Load(object sender, EventArgs e)
        {
            this.role_ModuleTableAdapter.Fill(this.bIDMDataSet.Role_Module);

            this.roleTableAdapter.Fill(this.bIDMDataSet.Role,Global.Team);
            
            this.moduleTableAdapter.Fill(this.bIDMDataSet.Module);

            this.tsDelete.Click += tsDelete_Click;

        }

        private void tsDelete_Click(object sender, EventArgs e)
        {
            try
            {

                if (MessageBox.Show("Do you want to delete the highlighted record ?", "BI Data Manager", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
                    == System.Windows.Forms.DialogResult.Yes)
                {
                    DataRow drRowDelete = this.bIDMDataSet.Role_Module.Rows[iRow];

                    this.role_ModuleTableAdapter.DeleteRoleModule((int)(drRowDelete["RoleId"]), (int)(drRowDelete["ModuleId"]));

                    this.role_ModuleTableAdapter.Fill(this.bIDMDataSet.Role_Module);
                }

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                if (cmbModule.SelectedIndex == -1)
                {
                    MessageBox.Show("Please select a Module.", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else if (cmbRole.SelectedIndex == -1)
                {
                    MessageBox.Show("Please select a Role.", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }


                if (moduleId != 0 && roleId != 0)
                {

                    this.role_ModuleTableAdapter.UpdateRoleModule((int)cmbRole.SelectedValue, (int)cmbModule.SelectedValue,DateTime.Now,Global.UserId,roleId,moduleId);
                }
                else
                {
                    this.role_ModuleTableAdapter.InsertRoleModule((int)cmbRole.SelectedValue, (int)cmbModule.SelectedValue, DateTime.Now, Global.UserId, DateTime.Now, Global.UserId);
                }

                ClearFields();
                this.role_ModuleTableAdapter.Fill(this.bIDMDataSet.Role_Module);

                btnEdit.Visible = true;
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {

                DataRow drRowEdit = this.bIDMDataSet.Role_Module.Rows[iRow];

                cmbRole.SelectedValue = (int)drRowEdit["RoleId"];
                cmbModule.SelectedValue = (int)drRowEdit["ModuleId"];

                moduleId = (int)drRowEdit["ModuleId"];
                roleId = (int)drRowEdit["RoleId"];

                btnEdit.Visible = false;
            }


            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void ClearFields()
        {
            cmbRole.SelectedIndex = -1;
            cmbModule.SelectedIndex = -1;
            moduleId = 0;
            roleId = 0;
        }

        
        private void dgvChannels_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvRoleModule.CurrentCell != null)
                iRow = dgvRoleModule.CurrentCell.RowIndex;
        }
    }
}

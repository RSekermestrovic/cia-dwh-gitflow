﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;

namespace BIDM
{
    static class Program
    {

       
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                // Application.Run(new Login());

                int value = Int32.Parse(ConfigurationManager.AppSettings["ByPassLogin"]);
                string username = Environment.UserName;


                if (value == 1)
                {
                    var dtTable = (new BIDMDataSetTableAdapters.UserTableAdapter()).GetUser(username, "", 1);

                    if (dtTable.Rows.Count == 0)
                    {
                        MessageBox.Show("Login Failed", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    else
                    {
                        Global.UserName = username;
                        Global.NameoftheUser = dtTable.Rows[0]["Name"].ToString();
                        Global.Team = dtTable.Rows[0]["Team"].ToString();
                        Global.RoleId = (int)dtTable.Rows[0]["RoleId"];
                        Global.UserId = (int)dtTable.Rows[0]["UserId"];

                        Application.Run(new MainScreen());
                    }
                }
                else
                {
                    Login fLogin = new Login();
                    if (fLogin.ShowDialog() == DialogResult.OK)
                    {
                        Application.Run(new MainScreen());
                    }
                    else
                    {
                        Application.Exit();
                    }
                }
            }

            catch(Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}

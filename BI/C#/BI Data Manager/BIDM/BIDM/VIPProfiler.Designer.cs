﻿namespace BIDM
{
    partial class VIPProfiler
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VIPProfiler));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.cmpComms = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsCopyComms = new System.Windows.Forms.ToolStripMenuItem();
            this.lblLoggedIn = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtManagedBy = new System.Windows.Forms.TextBox();
            this.cbStatus = new System.Windows.Forms.ComboBox();
            this.vIPStatusBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bIDMDataSet = new BIDM.BIDMDataSet();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.txtFactor = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtDP = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtInternetProfile = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtOptInOut = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtFacility = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtState = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtDOB = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtSignupDate = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtLastContactDate = new System.Windows.Forms.TextBox();
            this.txtSurname = new System.Windows.Forms.TextBox();
            this.txtFirstName = new System.Windows.Forms.TextBox();
            this.cbContact = new System.Windows.Forms.ComboBox();
            this.contactFrequencyIndexBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label10 = new System.Windows.Forms.Label();
            this.btnDelete = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.cbRelationship = new System.Windows.Forms.ComboBox();
            this.relationshipIndexBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnClear = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.txtComments = new System.Windows.Forms.RichTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cbPortfolio = new System.Windows.Forms.ComboBox();
            this.portfolioBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnEdit = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.Save = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtPIN = new System.Windows.Forms.TextBox();
            this.cbWealth = new System.Windows.Forms.ComboBox();
            this.wealthIndexBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.BIServiceLink = new System.Windows.Forms.LinkLabel();
            this.clientCommunicationBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.userTableAdapter = new BIDM.BIDMDataSetTableAdapters.UserTableAdapter();
            this.portfolioTableAdapter = new BIDM.BIDMDataSetTableAdapters.PortfolioTableAdapter();
            this.relationshipIndexTableAdapter = new BIDM.BIDMDataSetTableAdapters.RelationshipIndexTableAdapter();
            this.vIPStatusTableAdapter = new BIDM.BIDMDataSetTableAdapters.VIPStatusTableAdapter();
            this.contactFrequencyIndexTableAdapter = new BIDM.BIDMDataSetTableAdapters.ContactFrequencyIndexTableAdapter();
            this.wealthIndexTableAdapter = new BIDM.BIDMDataSetTableAdapters.WealthIndexTableAdapter();
            this.clientsTableAdapter = new BIDM.BIDMDataSetTableAdapters.ClientsTableAdapter();
            this.clientSubscriptionsTableAdapter = new BIDM.BIDMDataSetTableAdapters.ClientSubscriptionsTableAdapter();
            this.factorsTableAdapter = new BIDM.BIDMDataSetTableAdapters.FactorsTableAdapter();
            this.differentialPricingTableAdapter = new BIDM.BIDMDataSetTableAdapters.DifferentialPricingTableAdapter();
            this.managedByTableAdapter = new BIDM.BIDMDataSetTableAdapters.ManagedByTableAdapter();
            this.clientCommunicationTableAdapter = new BIDM.BIDMDataSetTableAdapters.ClientCommunicationTableAdapter();
            this.commentsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ModifiedBy = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.enteredByDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vIPManagerDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.isSuccessDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.categoryNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.directionNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.channelNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pINDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvCommunication = new System.Windows.Forms.DataGridView();
            this.label21 = new System.Windows.Forms.Label();
            this.communicationIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.channelIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.directionIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.categoryIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vIPManagerUserIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.channelNameDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.directionNameDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.categoryNameDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vIPManagerDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.modifiedByDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.isSuccessDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clientProfilerTableAdapter = new BIDM.BIDMDataSetTableAdapters.ClientProfilerTableAdapter();
            this.clientProfilerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dgvVIPProfiler = new System.Windows.Forms.DataGridView();
            this.cmpVIP = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsCopyVIP = new System.Windows.Forms.ToolStripMenuItem();
            this.tsDeleteVIP = new System.Windows.Forms.ToolStripMenuItem();
            this.ProfileId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pINDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vIPStatusNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contFreqIndexNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.relIndexNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wealthIndexNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.portfolioNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.commentsDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.enteredByDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmpComms.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vIPStatusBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bIDMDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.contactFrequencyIndexBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.relationshipIndexBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.portfolioBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wealthIndexBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientCommunicationBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCommunication)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientProfilerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVIPProfiler)).BeginInit();
            this.cmpVIP.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmpComms
            // 
            this.cmpComms.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsCopyComms});
            this.cmpComms.Name = "cmpComms";
            this.cmpComms.Size = new System.Drawing.Size(103, 26);
            // 
            // tsCopyComms
            // 
            this.tsCopyComms.Image = ((System.Drawing.Image)(resources.GetObject("tsCopyComms.Image")));
            this.tsCopyComms.Name = "tsCopyComms";
            this.tsCopyComms.Size = new System.Drawing.Size(102, 22);
            this.tsCopyComms.Text = "&Copy";
            // 
            // lblLoggedIn
            // 
            this.lblLoggedIn.AutoSize = true;
            this.lblLoggedIn.Location = new System.Drawing.Point(23, 110);
            this.lblLoggedIn.Name = "lblLoggedIn";
            this.lblLoggedIn.Size = new System.Drawing.Size(77, 13);
            this.lblLoggedIn.TabIndex = 20;
            this.lblLoggedIn.Text = "Logged in as : ";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(48)))), ((int)(((byte)(86)))));
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Location = new System.Drawing.Point(21, 22);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1031, 82);
            this.panel1.TabIndex = 19;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(250, 82);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 28F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(282, 20);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(211, 43);
            this.label7.TabIndex = 0;
            this.label7.Text = "VIP Profiler";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.txtManagedBy);
            this.groupBox1.Controls.Add(this.cbStatus);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.txtFactor);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.txtDP);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.txtInternetProfile);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.txtOptInOut);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.txtFacility);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.txtState);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.txtDOB);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.txtSignupDate);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.txtLastContactDate);
            this.groupBox1.Controls.Add(this.txtSurname);
            this.groupBox1.Controls.Add(this.txtFirstName);
            this.groupBox1.Controls.Add(this.cbContact);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.btnDelete);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.cbRelationship);
            this.groupBox1.Controls.Add(this.btnClear);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtComments);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.cbPortfolio);
            this.groupBox1.Controls.Add(this.btnEdit);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.Save);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtPIN);
            this.groupBox1.Controls.Add(this.cbWealth);
            this.groupBox1.Location = new System.Drawing.Point(21, 124);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1031, 283);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            // 
            // txtManagedBy
            // 
            this.txtManagedBy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtManagedBy.Location = new System.Drawing.Point(794, 19);
            this.txtManagedBy.Name = "txtManagedBy";
            this.txtManagedBy.ReadOnly = true;
            this.txtManagedBy.Size = new System.Drawing.Size(181, 20);
            this.txtManagedBy.TabIndex = 99;
            // 
            // cbStatus
            // 
            this.cbStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbStatus.DataSource = this.vIPStatusBindingSource;
            this.cbStatus.DisplayMember = "VIPStatusName";
            this.cbStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbStatus.FormattingEnabled = true;
            this.cbStatus.Location = new System.Drawing.Point(794, 99);
            this.cbStatus.Name = "cbStatus";
            this.cbStatus.Size = new System.Drawing.Size(181, 21);
            this.cbStatus.TabIndex = 4;
            this.cbStatus.ValueMember = "VIPStatusId";
            // 
            // vIPStatusBindingSource
            // 
            this.vIPStatusBindingSource.DataMember = "VIPStatus";
            this.vIPStatusBindingSource.DataSource = this.bIDMDataSet;
            // 
            // bIDMDataSet
            // 
            this.bIDMDataSet.DataSetName = "BIDMDataSet";
            this.bIDMDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label19
            // 
            this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(749, 102);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(37, 13);
            this.label19.TabIndex = 40;
            this.label19.Text = "Status";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(45, 157);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(60, 13);
            this.label20.TabIndex = 38;
            this.label20.Text = "Factor Y/N";
            // 
            // txtFactor
            // 
            this.txtFactor.Location = new System.Drawing.Point(111, 152);
            this.txtFactor.Name = "txtFactor";
            this.txtFactor.ReadOnly = true;
            this.txtFactor.Size = new System.Drawing.Size(181, 20);
            this.txtFactor.TabIndex = 99;
            // 
            // label17
            // 
            this.label17.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(434, 155);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(22, 13);
            this.label17.TabIndex = 36;
            this.label17.Text = "DP";
            // 
            // txtDP
            // 
            this.txtDP.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDP.Location = new System.Drawing.Point(462, 152);
            this.txtDP.Name = "txtDP";
            this.txtDP.ReadOnly = true;
            this.txtDP.Size = new System.Drawing.Size(181, 20);
            this.txtDP.TabIndex = 99;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(30, 131);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(75, 13);
            this.label18.TabIndex = 34;
            this.label18.Text = "Internet Profile";
            // 
            // txtInternetProfile
            // 
            this.txtInternetProfile.Location = new System.Drawing.Point(111, 126);
            this.txtInternetProfile.Name = "txtInternetProfile";
            this.txtInternetProfile.ReadOnly = true;
            this.txtInternetProfile.Size = new System.Drawing.Size(181, 20);
            this.txtInternetProfile.TabIndex = 99;
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(392, 129);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(64, 13);
            this.label15.TabIndex = 32;
            this.label15.Text = "Opt In / Out";
            // 
            // txtOptInOut
            // 
            this.txtOptInOut.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtOptInOut.Location = new System.Drawing.Point(462, 126);
            this.txtOptInOut.Name = "txtOptInOut";
            this.txtOptInOut.ReadOnly = true;
            this.txtOptInOut.Size = new System.Drawing.Size(181, 20);
            this.txtOptInOut.TabIndex = 99;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(66, 105);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(39, 13);
            this.label16.TabIndex = 30;
            this.label16.Text = "Facility";
            // 
            // txtFacility
            // 
            this.txtFacility.Location = new System.Drawing.Point(111, 100);
            this.txtFacility.Name = "txtFacility";
            this.txtFacility.ReadOnly = true;
            this.txtFacility.Size = new System.Drawing.Size(181, 20);
            this.txtFacility.TabIndex = 99;
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(424, 102);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(32, 13);
            this.label13.TabIndex = 28;
            this.label13.Text = "State";
            // 
            // txtState
            // 
            this.txtState.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtState.Location = new System.Drawing.Point(462, 99);
            this.txtState.Name = "txtState";
            this.txtState.ReadOnly = true;
            this.txtState.Size = new System.Drawing.Size(181, 20);
            this.txtState.TabIndex = 99;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(39, 77);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(66, 13);
            this.label14.TabIndex = 26;
            this.label14.Text = "Date of Birth";
            // 
            // txtDOB
            // 
            this.txtDOB.Location = new System.Drawing.Point(111, 74);
            this.txtDOB.Name = "txtDOB";
            this.txtDOB.ReadOnly = true;
            this.txtDOB.Size = new System.Drawing.Size(181, 20);
            this.txtDOB.TabIndex = 99;
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(390, 74);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(66, 13);
            this.label12.TabIndex = 24;
            this.label12.Text = "Signup Date";
            // 
            // txtSignupDate
            // 
            this.txtSignupDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSignupDate.Location = new System.Drawing.Point(462, 71);
            this.txtSignupDate.Name = "txtSignupDate";
            this.txtSignupDate.ReadOnly = true;
            this.txtSignupDate.Size = new System.Drawing.Size(181, 20);
            this.txtSignupDate.TabIndex = 99;
            // 
            // label22
            // 
            this.label22.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(363, 22);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(93, 13);
            this.label22.TabIndex = 22;
            this.label22.Text = "Last Contact Date";
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(407, 48);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(49, 13);
            this.label11.TabIndex = 22;
            this.label11.Text = "Surname";
            // 
            // txtLastContactDate
            // 
            this.txtLastContactDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLastContactDate.Location = new System.Drawing.Point(462, 19);
            this.txtLastContactDate.Name = "txtLastContactDate";
            this.txtLastContactDate.ReadOnly = true;
            this.txtLastContactDate.Size = new System.Drawing.Size(181, 20);
            this.txtLastContactDate.TabIndex = 99;
            this.txtLastContactDate.TextChanged += new System.EventHandler(this.txtLastContactDate_TextChanged);
            // 
            // txtSurname
            // 
            this.txtSurname.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSurname.Location = new System.Drawing.Point(462, 45);
            this.txtSurname.Name = "txtSurname";
            this.txtSurname.ReadOnly = true;
            this.txtSurname.Size = new System.Drawing.Size(181, 20);
            this.txtSurname.TabIndex = 99;
            // 
            // txtFirstName
            // 
            this.txtFirstName.Location = new System.Drawing.Point(111, 48);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.ReadOnly = true;
            this.txtFirstName.Size = new System.Drawing.Size(181, 20);
            this.txtFirstName.TabIndex = 99;
            // 
            // cbContact
            // 
            this.cbContact.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbContact.DataSource = this.contactFrequencyIndexBindingSource;
            this.cbContact.DisplayMember = "ContFreqIndexName";
            this.cbContact.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbContact.FormattingEnabled = true;
            this.cbContact.Location = new System.Drawing.Point(794, 153);
            this.cbContact.Name = "cbContact";
            this.cbContact.Size = new System.Drawing.Size(181, 21);
            this.cbContact.TabIndex = 6;
            this.cbContact.ValueMember = "ContFreqIndexId";
            // 
            // contactFrequencyIndexBindingSource
            // 
            this.contactFrequencyIndexBindingSource.DataMember = "ContactFrequencyIndex";
            this.contactFrequencyIndexBindingSource.DataSource = this.bIDMDataSet;
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(719, 22);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(67, 13);
            this.label10.TabIndex = 19;
            this.label10.Text = "Managed By";
            // 
            // btnDelete
            // 
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelete.Location = new System.Drawing.Point(192, 247);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 10;
            this.btnDelete.Text = "DELETE";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(716, 129);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(70, 13);
            this.label9.TabIndex = 13;
            this.label9.Text = "Wealth Index";
            // 
            // cbRelationship
            // 
            this.cbRelationship.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbRelationship.DataSource = this.relationshipIndexBindingSource;
            this.cbRelationship.DisplayMember = "RelIndexName";
            this.cbRelationship.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbRelationship.FormattingEnabled = true;
            this.cbRelationship.Location = new System.Drawing.Point(794, 72);
            this.cbRelationship.Name = "cbRelationship";
            this.cbRelationship.Size = new System.Drawing.Size(181, 21);
            this.cbRelationship.TabIndex = 3;
            this.cbRelationship.ValueMember = "RelIndexId";
            // 
            // relationshipIndexBindingSource
            // 
            this.relationshipIndexBindingSource.DataMember = "RelationshipIndex";
            this.relationshipIndexBindingSource.DataSource = this.bIDMDataSet;
            // 
            // btnClear
            // 
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClear.Location = new System.Drawing.Point(273, 247);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 23);
            this.btnClear.TabIndex = 11;
            this.btnClear.Text = "CLEAR";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(45, 181);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Comments";
            // 
            // txtComments
            // 
            this.txtComments.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtComments.Location = new System.Drawing.Point(111, 178);
            this.txtComments.MaxLength = 900;
            this.txtComments.Name = "txtComments";
            this.txtComments.Size = new System.Drawing.Size(864, 63);
            this.txtComments.TabIndex = 7;
            this.txtComments.Text = "";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(741, 48);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Portfolio";
            // 
            // cbPortfolio
            // 
            this.cbPortfolio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbPortfolio.DataSource = this.portfolioBindingSource;
            this.cbPortfolio.DisplayMember = "PortfolioName";
            this.cbPortfolio.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPortfolio.FormattingEnabled = true;
            this.cbPortfolio.Location = new System.Drawing.Point(794, 45);
            this.cbPortfolio.Name = "cbPortfolio";
            this.cbPortfolio.Size = new System.Drawing.Size(181, 21);
            this.cbPortfolio.TabIndex = 2;
            this.cbPortfolio.ValueMember = "PortfolioId";
            // 
            // portfolioBindingSource
            // 
            this.portfolioBindingSource.DataMember = "Portfolio";
            this.portfolioBindingSource.DataSource = this.bIDMDataSet;
            // 
            // btnEdit
            // 
            this.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEdit.Location = new System.Drawing.Point(111, 247);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(75, 23);
            this.btnEdit.TabIndex = 9;
            this.btnEdit.Text = "EDIT";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(48, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "First Name";
            // 
            // Save
            // 
            this.Save.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Save.Location = new System.Drawing.Point(30, 247);
            this.Save.Name = "Save";
            this.Save.Size = new System.Drawing.Size(75, 23);
            this.Save.TabIndex = 8;
            this.Save.Text = "SAVE";
            this.Save.UseVisualStyleBackColor = true;
            this.Save.Click += new System.EventHandler(this.Save_Click);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(686, 156);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Contact Freq. Index";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(51, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Client PIN";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(692, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Relationship Index";
            // 
            // txtPIN
            // 
            this.txtPIN.Location = new System.Drawing.Point(111, 22);
            this.txtPIN.Name = "txtPIN";
            this.txtPIN.Size = new System.Drawing.Size(181, 20);
            this.txtPIN.TabIndex = 1;
            this.txtPIN.TextChanged += new System.EventHandler(this.txtPIN_TextChanged);
            this.txtPIN.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPIN_KeyPress);
            // 
            // cbWealth
            // 
            this.cbWealth.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbWealth.DataSource = this.wealthIndexBindingSource;
            this.cbWealth.DisplayMember = "WealthIndexName";
            this.cbWealth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbWealth.FormattingEnabled = true;
            this.cbWealth.Location = new System.Drawing.Point(794, 126);
            this.cbWealth.Name = "cbWealth";
            this.cbWealth.Size = new System.Drawing.Size(181, 21);
            this.cbWealth.TabIndex = 5;
            this.cbWealth.ValueMember = "WealthIndexId";
            // 
            // wealthIndexBindingSource
            // 
            this.wealthIndexBindingSource.DataMember = "WealthIndex";
            this.wealthIndexBindingSource.DataSource = this.bIDMDataSet;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.DimGray;
            this.label8.Location = new System.Drawing.Point(22, 835);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(64, 13);
            this.label8.TabIndex = 45;
            this.label8.Text = "Provided By";
            // 
            // BIServiceLink
            // 
            this.BIServiceLink.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BIServiceLink.AutoSize = true;
            this.BIServiceLink.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BIServiceLink.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(155)))), ((int)(((byte)(211)))));
            this.BIServiceLink.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(155)))), ((int)(((byte)(211)))));
            this.BIServiceLink.Location = new System.Drawing.Point(82, 835);
            this.BIServiceLink.Name = "BIServiceLink";
            this.BIServiceLink.Size = new System.Drawing.Size(66, 13);
            this.BIServiceLink.TabIndex = 44;
            this.BIServiceLink.TabStop = true;
            this.BIServiceLink.Text = "BI Service";
            this.BIServiceLink.Click += new System.EventHandler(this.BIServiceLink_Click);
            // 
            // clientCommunicationBindingSource
            // 
            this.clientCommunicationBindingSource.DataMember = "ClientCommunication";
            this.clientCommunicationBindingSource.DataSource = this.bIDMDataSet;
            // 
            // userTableAdapter
            // 
            this.userTableAdapter.ClearBeforeFill = true;
            // 
            // portfolioTableAdapter
            // 
            this.portfolioTableAdapter.ClearBeforeFill = true;
            // 
            // relationshipIndexTableAdapter
            // 
            this.relationshipIndexTableAdapter.ClearBeforeFill = true;
            // 
            // vIPStatusTableAdapter
            // 
            this.vIPStatusTableAdapter.ClearBeforeFill = true;
            // 
            // contactFrequencyIndexTableAdapter
            // 
            this.contactFrequencyIndexTableAdapter.ClearBeforeFill = true;
            // 
            // wealthIndexTableAdapter
            // 
            this.wealthIndexTableAdapter.ClearBeforeFill = true;
            // 
            // clientsTableAdapter
            // 
            this.clientsTableAdapter.ClearBeforeFill = true;
            // 
            // clientSubscriptionsTableAdapter
            // 
            this.clientSubscriptionsTableAdapter.ClearBeforeFill = true;
            // 
            // factorsTableAdapter
            // 
            this.factorsTableAdapter.ClearBeforeFill = true;
            // 
            // differentialPricingTableAdapter
            // 
            this.differentialPricingTableAdapter.ClearBeforeFill = true;
            // 
            // managedByTableAdapter
            // 
            this.managedByTableAdapter.ClearBeforeFill = true;
            // 
            // clientCommunicationTableAdapter
            // 
            this.clientCommunicationTableAdapter.ClearBeforeFill = true;
            // 
            // commentsDataGridViewTextBoxColumn
            // 
            this.commentsDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.commentsDataGridViewTextBoxColumn.DataPropertyName = "Comments";
            this.commentsDataGridViewTextBoxColumn.HeaderText = "Comments";
            this.commentsDataGridViewTextBoxColumn.Name = "commentsDataGridViewTextBoxColumn";
            this.commentsDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // ModifiedBy
            // 
            this.ModifiedBy.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.ModifiedBy.DataPropertyName = "ModifiedBy";
            this.ModifiedBy.HeaderText = "ModifiedBy";
            this.ModifiedBy.Name = "ModifiedBy";
            this.ModifiedBy.ReadOnly = true;
            this.ModifiedBy.Width = 84;
            // 
            // enteredByDataGridViewTextBoxColumn
            // 
            this.enteredByDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.enteredByDataGridViewTextBoxColumn.DataPropertyName = "EnteredBy";
            this.enteredByDataGridViewTextBoxColumn.HeaderText = "EnteredBy";
            this.enteredByDataGridViewTextBoxColumn.Name = "enteredByDataGridViewTextBoxColumn";
            this.enteredByDataGridViewTextBoxColumn.ReadOnly = true;
            this.enteredByDataGridViewTextBoxColumn.Width = 81;
            // 
            // vIPManagerDataGridViewTextBoxColumn
            // 
            this.vIPManagerDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.vIPManagerDataGridViewTextBoxColumn.DataPropertyName = "VIPManager";
            this.vIPManagerDataGridViewTextBoxColumn.HeaderText = "VIPManager";
            this.vIPManagerDataGridViewTextBoxColumn.Name = "vIPManagerDataGridViewTextBoxColumn";
            this.vIPManagerDataGridViewTextBoxColumn.ReadOnly = true;
            this.vIPManagerDataGridViewTextBoxColumn.Width = 91;
            // 
            // isSuccessDataGridViewTextBoxColumn
            // 
            this.isSuccessDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.isSuccessDataGridViewTextBoxColumn.DataPropertyName = "IsSuccess";
            this.isSuccessDataGridViewTextBoxColumn.HeaderText = "IsSuccess";
            this.isSuccessDataGridViewTextBoxColumn.Name = "isSuccessDataGridViewTextBoxColumn";
            this.isSuccessDataGridViewTextBoxColumn.ReadOnly = true;
            this.isSuccessDataGridViewTextBoxColumn.Width = 81;
            // 
            // categoryNameDataGridViewTextBoxColumn
            // 
            this.categoryNameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.categoryNameDataGridViewTextBoxColumn.DataPropertyName = "CategoryName";
            this.categoryNameDataGridViewTextBoxColumn.HeaderText = "CategoryName";
            this.categoryNameDataGridViewTextBoxColumn.Name = "categoryNameDataGridViewTextBoxColumn";
            this.categoryNameDataGridViewTextBoxColumn.ReadOnly = true;
            this.categoryNameDataGridViewTextBoxColumn.Width = 102;
            // 
            // directionNameDataGridViewTextBoxColumn
            // 
            this.directionNameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.directionNameDataGridViewTextBoxColumn.DataPropertyName = "DirectionName";
            this.directionNameDataGridViewTextBoxColumn.HeaderText = "DirectionName";
            this.directionNameDataGridViewTextBoxColumn.Name = "directionNameDataGridViewTextBoxColumn";
            this.directionNameDataGridViewTextBoxColumn.ReadOnly = true;
            this.directionNameDataGridViewTextBoxColumn.Width = 102;
            // 
            // channelNameDataGridViewTextBoxColumn
            // 
            this.channelNameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.channelNameDataGridViewTextBoxColumn.DataPropertyName = "ChannelName";
            this.channelNameDataGridViewTextBoxColumn.HeaderText = "ChannelName";
            this.channelNameDataGridViewTextBoxColumn.Name = "channelNameDataGridViewTextBoxColumn";
            this.channelNameDataGridViewTextBoxColumn.ReadOnly = true;
            this.channelNameDataGridViewTextBoxColumn.Width = 99;
            // 
            // pINDataGridViewTextBoxColumn1
            // 
            this.pINDataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.pINDataGridViewTextBoxColumn1.DataPropertyName = "PIN";
            this.pINDataGridViewTextBoxColumn1.HeaderText = "PIN";
            this.pINDataGridViewTextBoxColumn1.Name = "pINDataGridViewTextBoxColumn1";
            this.pINDataGridViewTextBoxColumn1.ReadOnly = true;
            this.pINDataGridViewTextBoxColumn1.Width = 50;
            // 
            // dateDataGridViewTextBoxColumn
            // 
            this.dateDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.dateDataGridViewTextBoxColumn.DataPropertyName = "Date";
            this.dateDataGridViewTextBoxColumn.HeaderText = "Date";
            this.dateDataGridViewTextBoxColumn.Name = "dateDataGridViewTextBoxColumn";
            this.dateDataGridViewTextBoxColumn.ReadOnly = true;
            this.dateDataGridViewTextBoxColumn.Width = 55;
            // 
            // dgvCommunication
            // 
            this.dgvCommunication.AllowUserToAddRows = false;
            this.dgvCommunication.AllowUserToDeleteRows = false;
            this.dgvCommunication.AllowUserToOrderColumns = true;
            this.dgvCommunication.AllowUserToResizeRows = false;
            this.dgvCommunication.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvCommunication.AutoGenerateColumns = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvCommunication.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvCommunication.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCommunication.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dateDataGridViewTextBoxColumn,
            this.pINDataGridViewTextBoxColumn1,
            this.channelNameDataGridViewTextBoxColumn,
            this.directionNameDataGridViewTextBoxColumn,
            this.categoryNameDataGridViewTextBoxColumn,
            this.isSuccessDataGridViewTextBoxColumn,
            this.vIPManagerDataGridViewTextBoxColumn,
            this.enteredByDataGridViewTextBoxColumn,
            this.ModifiedBy,
            this.commentsDataGridViewTextBoxColumn});
            this.dgvCommunication.ContextMenuStrip = this.cmpComms;
            this.dgvCommunication.DataSource = this.clientCommunicationBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvCommunication.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvCommunication.Location = new System.Drawing.Point(21, 655);
            this.dgvCommunication.MultiSelect = false;
            this.dgvCommunication.Name = "dgvCommunication";
            this.dgvCommunication.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvCommunication.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvCommunication.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCommunication.Size = new System.Drawing.Size(1030, 173);
            this.dgvCommunication.TabIndex = 46;
            // 
            // label21
            // 
            this.label21.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(18, 639);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(113, 13);
            this.label21.TabIndex = 47;
            this.label21.Text = "Client Communications";
            // 
            // communicationIdDataGridViewTextBoxColumn
            // 
            this.communicationIdDataGridViewTextBoxColumn.DataPropertyName = "CommunicationId";
            this.communicationIdDataGridViewTextBoxColumn.HeaderText = "CommunicationId";
            this.communicationIdDataGridViewTextBoxColumn.Name = "communicationIdDataGridViewTextBoxColumn";
            this.communicationIdDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dateDataGridViewTextBoxColumn1
            // 
            this.dateDataGridViewTextBoxColumn1.DataPropertyName = "Date";
            this.dateDataGridViewTextBoxColumn1.HeaderText = "Date";
            this.dateDataGridViewTextBoxColumn1.Name = "dateDataGridViewTextBoxColumn1";
            // 
            // channelIdDataGridViewTextBoxColumn
            // 
            this.channelIdDataGridViewTextBoxColumn.DataPropertyName = "ChannelId";
            this.channelIdDataGridViewTextBoxColumn.HeaderText = "ChannelId";
            this.channelIdDataGridViewTextBoxColumn.Name = "channelIdDataGridViewTextBoxColumn";
            // 
            // directionIdDataGridViewTextBoxColumn
            // 
            this.directionIdDataGridViewTextBoxColumn.DataPropertyName = "DirectionId";
            this.directionIdDataGridViewTextBoxColumn.HeaderText = "DirectionId";
            this.directionIdDataGridViewTextBoxColumn.Name = "directionIdDataGridViewTextBoxColumn";
            // 
            // categoryIdDataGridViewTextBoxColumn
            // 
            this.categoryIdDataGridViewTextBoxColumn.DataPropertyName = "CategoryId";
            this.categoryIdDataGridViewTextBoxColumn.HeaderText = "CategoryId";
            this.categoryIdDataGridViewTextBoxColumn.Name = "categoryIdDataGridViewTextBoxColumn";
            // 
            // vIPManagerUserIdDataGridViewTextBoxColumn
            // 
            this.vIPManagerUserIdDataGridViewTextBoxColumn.DataPropertyName = "VIPManagerUserId";
            this.vIPManagerUserIdDataGridViewTextBoxColumn.HeaderText = "VIPManagerUserId";
            this.vIPManagerUserIdDataGridViewTextBoxColumn.Name = "vIPManagerUserIdDataGridViewTextBoxColumn";
            // 
            // channelNameDataGridViewTextBoxColumn1
            // 
            this.channelNameDataGridViewTextBoxColumn1.DataPropertyName = "ChannelName";
            this.channelNameDataGridViewTextBoxColumn1.HeaderText = "ChannelName";
            this.channelNameDataGridViewTextBoxColumn1.Name = "channelNameDataGridViewTextBoxColumn1";
            // 
            // directionNameDataGridViewTextBoxColumn1
            // 
            this.directionNameDataGridViewTextBoxColumn1.DataPropertyName = "DirectionName";
            this.directionNameDataGridViewTextBoxColumn1.HeaderText = "DirectionName";
            this.directionNameDataGridViewTextBoxColumn1.Name = "directionNameDataGridViewTextBoxColumn1";
            // 
            // categoryNameDataGridViewTextBoxColumn1
            // 
            this.categoryNameDataGridViewTextBoxColumn1.DataPropertyName = "CategoryName";
            this.categoryNameDataGridViewTextBoxColumn1.HeaderText = "CategoryName";
            this.categoryNameDataGridViewTextBoxColumn1.Name = "categoryNameDataGridViewTextBoxColumn1";
            // 
            // vIPManagerDataGridViewTextBoxColumn1
            // 
            this.vIPManagerDataGridViewTextBoxColumn1.DataPropertyName = "VIPManager";
            this.vIPManagerDataGridViewTextBoxColumn1.HeaderText = "VIPManager";
            this.vIPManagerDataGridViewTextBoxColumn1.Name = "vIPManagerDataGridViewTextBoxColumn1";
            // 
            // modifiedByDataGridViewTextBoxColumn
            // 
            this.modifiedByDataGridViewTextBoxColumn.DataPropertyName = "ModifiedBy";
            this.modifiedByDataGridViewTextBoxColumn.HeaderText = "ModifiedBy";
            this.modifiedByDataGridViewTextBoxColumn.Name = "modifiedByDataGridViewTextBoxColumn";
            // 
            // isSuccessDataGridViewTextBoxColumn1
            // 
            this.isSuccessDataGridViewTextBoxColumn1.DataPropertyName = "IsSuccess";
            this.isSuccessDataGridViewTextBoxColumn1.HeaderText = "IsSuccess";
            this.isSuccessDataGridViewTextBoxColumn1.Name = "isSuccessDataGridViewTextBoxColumn1";
            this.isSuccessDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // clientProfilerTableAdapter
            // 
            this.clientProfilerTableAdapter.ClearBeforeFill = true;
            // 
            // clientProfilerBindingSource
            // 
            this.clientProfilerBindingSource.DataMember = "ClientProfiler";
            this.clientProfilerBindingSource.DataSource = this.bIDMDataSet;
            // 
            // dgvVIPProfiler
            // 
            this.dgvVIPProfiler.AllowUserToAddRows = false;
            this.dgvVIPProfiler.AllowUserToDeleteRows = false;
            this.dgvVIPProfiler.AllowUserToOrderColumns = true;
            this.dgvVIPProfiler.AllowUserToResizeRows = false;
            this.dgvVIPProfiler.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvVIPProfiler.AutoGenerateColumns = false;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvVIPProfiler.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvVIPProfiler.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVIPProfiler.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ProfileId,
            this.pINDataGridViewTextBoxColumn,
            this.vIPStatusNameDataGridViewTextBoxColumn,
            this.contFreqIndexNameDataGridViewTextBoxColumn,
            this.relIndexNameDataGridViewTextBoxColumn,
            this.wealthIndexNameDataGridViewTextBoxColumn,
            this.portfolioNameDataGridViewTextBoxColumn,
            this.commentsDataGridViewTextBoxColumn1,
            this.enteredByDataGridViewTextBoxColumn1});
            this.dgvVIPProfiler.ContextMenuStrip = this.cmpVIP;
            this.dgvVIPProfiler.DataSource = this.clientProfilerBindingSource;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvVIPProfiler.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgvVIPProfiler.Location = new System.Drawing.Point(21, 413);
            this.dgvVIPProfiler.MultiSelect = false;
            this.dgvVIPProfiler.Name = "dgvVIPProfiler";
            this.dgvVIPProfiler.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvVIPProfiler.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvVIPProfiler.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvVIPProfiler.Size = new System.Drawing.Size(1031, 223);
            this.dgvVIPProfiler.TabIndex = 48;
            this.dgvVIPProfiler.SelectionChanged += new System.EventHandler(this.dgvVIPProfiler_SelectionChanged);
            // 
            // cmpVIP
            // 
            this.cmpVIP.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsCopyVIP,
            this.tsDeleteVIP});
            this.cmpVIP.Name = "cmpComms";
            this.cmpVIP.Size = new System.Drawing.Size(108, 48);
            // 
            // tsCopyVIP
            // 
            this.tsCopyVIP.Image = ((System.Drawing.Image)(resources.GetObject("tsCopyVIP.Image")));
            this.tsCopyVIP.Name = "tsCopyVIP";
            this.tsCopyVIP.Size = new System.Drawing.Size(107, 22);
            this.tsCopyVIP.Text = "&Copy";
            // 
            // tsDeleteVIP
            // 
            this.tsDeleteVIP.Image = ((System.Drawing.Image)(resources.GetObject("tsDeleteVIP.Image")));
            this.tsDeleteVIP.Name = "tsDeleteVIP";
            this.tsDeleteVIP.Size = new System.Drawing.Size(107, 22);
            this.tsDeleteVIP.Text = "&Delete";
            // 
            // ProfileId
            // 
            this.ProfileId.DataPropertyName = "ProfileId";
            this.ProfileId.HeaderText = "ProfileId";
            this.ProfileId.Name = "ProfileId";
            this.ProfileId.ReadOnly = true;
            this.ProfileId.Visible = false;
            // 
            // pINDataGridViewTextBoxColumn
            // 
            this.pINDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.pINDataGridViewTextBoxColumn.DataPropertyName = "PIN";
            this.pINDataGridViewTextBoxColumn.HeaderText = "PIN";
            this.pINDataGridViewTextBoxColumn.Name = "pINDataGridViewTextBoxColumn";
            this.pINDataGridViewTextBoxColumn.ReadOnly = true;
            this.pINDataGridViewTextBoxColumn.Width = 50;
            // 
            // vIPStatusNameDataGridViewTextBoxColumn
            // 
            this.vIPStatusNameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.vIPStatusNameDataGridViewTextBoxColumn.DataPropertyName = "VIPStatusName";
            this.vIPStatusNameDataGridViewTextBoxColumn.HeaderText = "Status";
            this.vIPStatusNameDataGridViewTextBoxColumn.Name = "vIPStatusNameDataGridViewTextBoxColumn";
            this.vIPStatusNameDataGridViewTextBoxColumn.ReadOnly = true;
            this.vIPStatusNameDataGridViewTextBoxColumn.Width = 62;
            // 
            // contFreqIndexNameDataGridViewTextBoxColumn
            // 
            this.contFreqIndexNameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.contFreqIndexNameDataGridViewTextBoxColumn.DataPropertyName = "ContFreqIndexName";
            this.contFreqIndexNameDataGridViewTextBoxColumn.HeaderText = "Contact Freq. Index";
            this.contFreqIndexNameDataGridViewTextBoxColumn.Name = "contFreqIndexNameDataGridViewTextBoxColumn";
            this.contFreqIndexNameDataGridViewTextBoxColumn.ReadOnly = true;
            this.contFreqIndexNameDataGridViewTextBoxColumn.Width = 91;
            // 
            // relIndexNameDataGridViewTextBoxColumn
            // 
            this.relIndexNameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.relIndexNameDataGridViewTextBoxColumn.DataPropertyName = "RelIndexName";
            this.relIndexNameDataGridViewTextBoxColumn.HeaderText = "Relationship Index";
            this.relIndexNameDataGridViewTextBoxColumn.Name = "relIndexNameDataGridViewTextBoxColumn";
            this.relIndexNameDataGridViewTextBoxColumn.ReadOnly = true;
            this.relIndexNameDataGridViewTextBoxColumn.Width = 109;
            // 
            // wealthIndexNameDataGridViewTextBoxColumn
            // 
            this.wealthIndexNameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.wealthIndexNameDataGridViewTextBoxColumn.DataPropertyName = "WealthIndexName";
            this.wealthIndexNameDataGridViewTextBoxColumn.HeaderText = "Wealth Index";
            this.wealthIndexNameDataGridViewTextBoxColumn.Name = "wealthIndexNameDataGridViewTextBoxColumn";
            this.wealthIndexNameDataGridViewTextBoxColumn.ReadOnly = true;
            this.wealthIndexNameDataGridViewTextBoxColumn.Width = 87;
            // 
            // portfolioNameDataGridViewTextBoxColumn
            // 
            this.portfolioNameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.portfolioNameDataGridViewTextBoxColumn.DataPropertyName = "PortfolioName";
            this.portfolioNameDataGridViewTextBoxColumn.HeaderText = "Portfolio";
            this.portfolioNameDataGridViewTextBoxColumn.Name = "portfolioNameDataGridViewTextBoxColumn";
            this.portfolioNameDataGridViewTextBoxColumn.ReadOnly = true;
            this.portfolioNameDataGridViewTextBoxColumn.Width = 70;
            // 
            // commentsDataGridViewTextBoxColumn1
            // 
            this.commentsDataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.commentsDataGridViewTextBoxColumn1.DataPropertyName = "Comments";
            this.commentsDataGridViewTextBoxColumn1.HeaderText = "Comments";
            this.commentsDataGridViewTextBoxColumn1.Name = "commentsDataGridViewTextBoxColumn1";
            this.commentsDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // enteredByDataGridViewTextBoxColumn1
            // 
            this.enteredByDataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.enteredByDataGridViewTextBoxColumn1.DataPropertyName = "EnteredBy";
            this.enteredByDataGridViewTextBoxColumn1.HeaderText = "EnteredBy";
            this.enteredByDataGridViewTextBoxColumn1.Name = "enteredByDataGridViewTextBoxColumn1";
            this.enteredByDataGridViewTextBoxColumn1.ReadOnly = true;
            this.enteredByDataGridViewTextBoxColumn1.Width = 81;
            // 
            // VIPProfiler
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1072, 852);
            this.Controls.Add(this.dgvVIPProfiler);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.dgvCommunication);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.BIServiceLink);
            this.Controls.Add(this.lblLoggedIn);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "VIPProfiler";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "VIP Profiler";
            this.Load += new System.EventHandler(this.VIPProfiler_Load);
            this.cmpComms.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vIPStatusBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bIDMDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.contactFrequencyIndexBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.relationshipIndexBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.portfolioBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wealthIndexBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientCommunicationBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCommunication)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientProfilerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVIPProfiler)).EndInit();
            this.cmpVIP.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip cmpComms;
        private System.Windows.Forms.ToolStripMenuItem tsCopyComms;
        private System.Windows.Forms.Label lblLoggedIn;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cbStatus;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtFactor;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtDP;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtInternetProfile;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtOptInOut;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtFacility;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtState;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtDOB;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtSignupDate;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtLastContactDate;
        private System.Windows.Forms.TextBox txtSurname;
        private System.Windows.Forms.TextBox txtFirstName;
        private System.Windows.Forms.ComboBox cbContact;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cbRelationship;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RichTextBox txtComments;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbPortfolio;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Save;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtPIN;
        private System.Windows.Forms.ComboBox cbWealth;
        private BIDMDataSetTableAdapters.UserTableAdapter userTableAdapter;
        private BIDMDataSet bIDMDataSet;
        private System.Windows.Forms.BindingSource portfolioBindingSource;
        private BIDMDataSetTableAdapters.PortfolioTableAdapter portfolioTableAdapter;
        private System.Windows.Forms.BindingSource relationshipIndexBindingSource;
        private BIDMDataSetTableAdapters.RelationshipIndexTableAdapter relationshipIndexTableAdapter;
        private System.Windows.Forms.BindingSource vIPStatusBindingSource;
        private BIDMDataSetTableAdapters.VIPStatusTableAdapter vIPStatusTableAdapter;
        private System.Windows.Forms.BindingSource contactFrequencyIndexBindingSource;
        private BIDMDataSetTableAdapters.ContactFrequencyIndexTableAdapter contactFrequencyIndexTableAdapter;
        private System.Windows.Forms.BindingSource wealthIndexBindingSource;
        private BIDMDataSetTableAdapters.WealthIndexTableAdapter wealthIndexTableAdapter;
        private BIDMDataSetTableAdapters.ClientsTableAdapter clientsTableAdapter;
        private BIDMDataSetTableAdapters.ClientSubscriptionsTableAdapter clientSubscriptionsTableAdapter;
        private BIDMDataSetTableAdapters.FactorsTableAdapter factorsTableAdapter;
        private BIDMDataSetTableAdapters.DifferentialPricingTableAdapter differentialPricingTableAdapter;
        private BIDMDataSetTableAdapters.ManagedByTableAdapter managedByTableAdapter;
        private System.Windows.Forms.TextBox txtManagedBy;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.LinkLabel BIServiceLink;
        private System.Windows.Forms.BindingSource clientCommunicationBindingSource;
        private BIDMDataSetTableAdapters.ClientCommunicationTableAdapter clientCommunicationTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn commentsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ModifiedBy;
        private System.Windows.Forms.DataGridViewTextBoxColumn enteredByDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn vIPManagerDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn isSuccessDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn categoryNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn directionNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn channelNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pINDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridView dgvCommunication;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.DataGridViewTextBoxColumn communicationIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn channelIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn directionIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn categoryIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn vIPManagerUserIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn channelNameDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn directionNameDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn categoryNameDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn vIPManagerDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn modifiedByDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn isSuccessDataGridViewTextBoxColumn1;
        private BIDMDataSetTableAdapters.ClientProfilerTableAdapter clientProfilerTableAdapter;
        private System.Windows.Forms.BindingSource clientProfilerBindingSource;
        private System.Windows.Forms.DataGridView dgvVIPProfiler;
        private System.Windows.Forms.ContextMenuStrip cmpVIP;
        private System.Windows.Forms.ToolStripMenuItem tsCopyVIP;
        private System.Windows.Forms.ToolStripMenuItem tsDeleteVIP;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProfileId;
        private System.Windows.Forms.DataGridViewTextBoxColumn pINDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn vIPStatusNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn contFreqIndexNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn relIndexNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn wealthIndexNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn portfolioNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn commentsDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn enteredByDataGridViewTextBoxColumn1;
    }
}


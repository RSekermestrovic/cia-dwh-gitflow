﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BIDM
{
    public partial class MainScreen : Form
    {
        private bool bFound = false;
        private int i = 0;
        public MainScreen()
        {
            InitializeComponent();

            DataTable dtTable = this.role_ModuleTableAdapter.GetDataByRole(Global.RoleId);

            try
            {

                for (int j = 0; j < menuStrip1.Items.Count; j++)
                {
                    ToolStripMenuItem tsmDropDownItem = (ToolStripMenuItem)menuStrip1.Items[j];

                    while (i < tsmDropDownItem.DropDownItems.Count)
                    {
                        ToolStripItem tsmItem = tsmDropDownItem.DropDownItems[i];

                        foreach (DataRow drRow in dtTable.Rows)
                        {
                            if (drRow["ModuleName"].ToString().Contains(tsmItem.Text.Trim()) || tsmItem.Text.Trim() == "Exit")
                            {
                                bFound = true;
                                break;
                            }
                        }

                        if (!bFound)
                        {
                            tsmDropDownItem.DropDownItems.Remove(tsmItem);
                            i = 0;
                        }
                        else
                            i++;


                        bFound = false;
                    }

                    if (tsmDropDownItem.DropDownItems.Count == 0)
                        menuStrip1.Items.Remove(tsmDropDownItem);

                    i = 0;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Please contact BI Team ! Error was :" + ex.GetBaseException().Message, "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }
        }
        
        private void vIPCommunicationsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Communications vipComm = new Communications();

            vipComm.MdiParent = this;

            vipComm.Show();

            this.LayoutMdi(MdiLayout.TileVertical);
        }

        private void vIPGiftingHospitalityToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GiftingHospitality vipGifting = new GiftingHospitality();

            vipGifting.MdiParent = this;

            vipGifting.Show();

            this.LayoutMdi(MdiLayout.TileVertical);
        }

        private void channelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AdminChannels channels = new AdminChannels();

            channels.MdiParent = this;

            channels.Show();
        }

        private void categoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AdminCategory category = new AdminCategory();

            category.MdiParent = this;

            category.Show();
        }

        private void usersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AdminUsers user = new AdminUsers();

            user.MdiParent = this;

            user.Show();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void roleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AdminRoles roles = new AdminRoles();

            roles.MdiParent = this;

            roles.Show();
        }

        private void moduleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AdminModules modules = new AdminModules();

            modules.MdiParent = this;

            modules.Show();
        }

        private void roleModulesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AdminRoleModules roleModules = new AdminRoleModules();

            roleModules.MdiParent = this;

            roleModules.Show();

        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            PromotionalEvents promoEvents = new PromotionalEvents();

            promoEvents.MdiParent = this;

            promoEvents.Show();
        }

        private void vIPProfilerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            VIPProfiler vipProfiler = new VIPProfiler();

            vipProfiler.MdiParent = this;

            vipProfiler.Show();
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BIDM
{
    public class Global 
    {
        public static string UserName { get; set; }

        public static string NameoftheUser { get; set; }

        public static int RoleId { get; set; }

        public static int UserId { get; set; }

        public static string Team { get; set; }

        public static bool ByPassLogin { get; set; }

    }
}

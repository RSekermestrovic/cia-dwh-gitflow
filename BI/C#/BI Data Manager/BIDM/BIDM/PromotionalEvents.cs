﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BIDM
{
    public partial class PromotionalEvents : Form
    {
        private int iEventid = 0, iRow = 0;
        private long iPromotionEventsId = 0;
  

        public PromotionalEvents()
        {
            InitializeComponent();

            tsCopy.Click += tsCopy_Click;
            tsDelete.Click += btnDelete_Click;


            lblLoggedIn.Text = "Logged in as : " + Global.NameoftheUser;

        }


        private void PromotionalEvents_Load(object sender, EventArgs e)
        {

            try
            {
                // TODO: This line of code loads data into the 'bIDMDataSet.Promotion' table. You can move, or remove it, as needed.
                this.promotionTableAdapter.Fill(this.bIDMDataSet.Promotion);

                this.promotionalEventsTableAdapter.Fill(this.bIDMDataSet.PromotionalEvents, Global.UserId, Global.RoleId.ToString());


                cbPromoCode.SelectedIndex = -1;
                cbCompetitor.SelectedIndex = -1;
                chkbAll.Checked = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void tsCopy_Click(object sender, EventArgs e)
        {
            Clipboard.SetDataObject(dgvPromoEvents.GetClipboardContent());
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsValidated())
                {

                    if (iPromotionEventsId != 0)
                    {
                        promotionalEventsTableAdapter.UpdatePromoEvents(txtComments.Text.Trim(), DateTime.Now, Global.UserId,
                            Convert.ToInt32(txtEventId.Text.Trim()), cbPromoCode.SelectedValue.ToString(), chkbAll.Checked ? 0 : Convert.ToInt32(cbCompetitor.SelectedValue), iPromotionEventsId);

                        btnClear.Visible = true;
                        btnDelete.Visible = true;
                        btnEdit.Visible = true;
                    }
                    else
                    {
                        promotionalEventsTableAdapter.InsertPromoEvents(Convert.ToInt32(txtEventId.Text.Trim()), txtComments.Text.Trim(), chkbAll.Checked ? 0 : Convert.ToInt32(cbCompetitor.SelectedValue), cbPromoCode.SelectedValue.ToString()
                            , DateTime.Now, Global.UserId, DateTime.Now, Global.UserId);

                    }

                    ClearFields();

                    this.promotionalEventsTableAdapter.Fill(this.bIDMDataSet.PromotionalEvents, Global.UserId, Global.RoleId.ToString());

                }

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private bool IsValidated()
        {
            if (txtEventId.Text.Trim() == "")
            {
                MessageBox.Show("Entered EventId is not valid.", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            else if (!chkbAll.Checked && cbCompetitor.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a competitor.", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;

            }
            else if (cbPromoCode.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a Promo Code.", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            else return true;
        }


        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                DataRow drRowDelete = this.bIDMDataSet.PromotionalEvents.Rows[iRow];

                txtEventId.Text = drRowDelete["EventId"].ToString();
                cbPromoCode.SelectedValue = drRowDelete["PromoCode"].ToString();

                if ((int)drRowDelete["CompetitorId"] == 0)
                    cbCompetitor.SelectedIndex = -1;
                else
                {
                    cbCompetitor.SelectedValue = (int)drRowDelete["CompetitorId"];
                    chkbAll.Checked = false;
                }
                txtComments.Text = drRowDelete["Comments"].ToString();

                var dTable = this.eventsTableAdapter.GetData((int)drRowDelete["EventId"]);

                if (dTable.Rows.Count > 0)
                {
                    txtEventName.Text = this.eventsTableAdapter.GetData(iEventid).Rows[0]["EventName"].ToString();
                }

                iPromotionEventsId = (long)drRowDelete["PromoEventsId"];

                btnClear.Visible = false;
                btnDelete.Visible = false;
                btnEdit.Visible = false;
            }


            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {

                if (MessageBox.Show("Do you want to delete the highlighted record ?", "BI Data Manager", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
                    == System.Windows.Forms.DialogResult.Yes)
                {
                    DataRow drRowDelete = this.bIDMDataSet.PromotionalEvents.Rows[iRow];

                    this.promotionalEventsTableAdapter.DeletePromoEvents((long)(drRowDelete["PromoEventsId"]));

                    promotionalEventsTableAdapter.Fill(this.bIDMDataSet.PromotionalEvents, Global.UserId, Global.RoleId.ToString());
                }

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            ClearFields();
        }

        private void dgvPromoEvents_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvPromoEvents.CurrentCell != null)
                iRow = dgvPromoEvents.CurrentCell.RowIndex;
        }

        private void ClearFields()
        {
            txtEventId.Text = "";
            txtEventName.Text = "";
            txtComments.Text = "";
            cbCompetitor.SelectedIndex = -1;
            cbPromoCode.SelectedIndex = -1;
            chkbAll.Checked = true;
            iPromotionEventsId = 0;
        }

        private void txtEventId_TextChanged(object sender, EventArgs e)
        {


            if (!Int32.TryParse(txtEventId.Text, out iEventid) && txtEventId.Text != "")
            {
                MessageBox.Show("Entered Event Id is not valid.", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtEventId.Text = "";
            }



            var dTable = this.eventsTableAdapter.GetData(iEventid);

            if (dTable.Rows.Count > 0)
            {
                txtEventName.Text = this.eventsTableAdapter.GetData(iEventid).Rows[0]["EventName"].ToString();

                this.competitorTableAdapter.Fill(this.bIDMDataSet.Competitor, iEventid);

                cbCompetitor.SelectedIndex = -1;
            }

        }

        private void chkbAll_CheckedChanged(object sender, EventArgs e)
        {
            if (chkbAll.Checked)
                cbCompetitor.Enabled = false;
            else
                cbCompetitor.Enabled = true;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BIDM
{
    public partial class AdminCategory : Form
    {

        private int iRow;
        private int categoryId;

        public AdminCategory()
        {
            InitializeComponent();
        }

        private void AdminCategory_Load(object sender, EventArgs e)
        {
            try
            {
                this.categoryTableAdapter.Fill(this.bIDMDataSet.Category);
                
                this.moduleTableAdapter.Fill(this.bIDMDataSet.Module);

                this.tsDelete.Click += tsDelete_Click;
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void tsDelete_Click(object sender, EventArgs e)
        {
            try
            {

                if (MessageBox.Show("Do you want to delete the highlighted record ?", "BI Data Manager", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
                    == System.Windows.Forms.DialogResult.Yes)
                {
                    DataRow drRowDelete = this.bIDMDataSet.Category.Rows[iRow];

                    this.categoryTableAdapter.DeleteCategory((int)(drRowDelete["CategoryId"]));

                    this.categoryTableAdapter.Fill(this.bIDMDataSet.Category);
                }

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

               if (txtCategoryName.Text.Trim() == "")
                {
                    MessageBox.Show("Entered Category Name is not valid.", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }


               if (categoryId != 0)
                {

                    this.categoryTableAdapter.UpdateCategory(txtCategoryName.Text.Trim(), (int)cmbModule.SelectedValue, categoryId);
                }
                else
                {
                    this.categoryTableAdapter.InsertCategory(txtCategoryName.Text.Trim(), (int)cmbModule.SelectedValue);
                }

                ClearFields();

                this.categoryTableAdapter.Fill(this.bIDMDataSet.Category);

                btnEdit.Visible = true;
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {

                DataRow drRowEdit = this.bIDMDataSet.Category.Rows[iRow];

                txtCategoryName.Text = drRowEdit["CategoryName"].ToString();
                cmbModule.SelectedValue = (int)drRowEdit["ModuleId"];

                categoryId = (int)drRowEdit["CategoryId"];

                btnEdit.Visible = false;
            }


            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void ClearFields()
        {
            txtCategoryName.Text = "";
            categoryId = 0;
            cmbModule.SelectedIndex = -1;
        }

        private void dgvCategorys_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvCategory.CurrentCell != null)
                iRow = dgvCategory.CurrentCell.RowIndex;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BIDM
{
    public partial class AdminModules : Form
    {

        private int iRow;
        private int moduleId;

        public AdminModules()
        {
            InitializeComponent();
        }

        private void AdminModules_Load(object sender, EventArgs e)
        {
            
            this.moduleTableAdapter.Fill(this.bIDMDataSet.Module);

            this.tsDelete.Click += tsDelete_Click;

        }

        private void tsDelete_Click(object sender, EventArgs e)
        {
            try
            {

                if (MessageBox.Show("Do you want to delete the highlighted record ?", "BI Data Manager", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
                    == System.Windows.Forms.DialogResult.Yes)
                {
                    DataRow drRowDelete = this.bIDMDataSet.Module.Rows[iRow];

                    this.moduleTableAdapter.DeleteModule((int)(drRowDelete["ModuleId"]));

                    this.moduleTableAdapter.Update(bIDMDataSet);

                    this.moduleTableAdapter.Fill(this.bIDMDataSet.Module);
                }

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                if (txtModuleName.Text.Trim() == "")
                {
                    MessageBox.Show("Entered Module Name is not valid.", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }


                if (moduleId != 0)
                {

                    this.moduleTableAdapter.UpdateModule(txtModuleName.Text.Trim(), moduleId);

                    this.moduleTableAdapter.Update(bIDMDataSet);

                }
                else
                {
                    DataRow drRow = this.bIDMDataSet.Module.NewRow();

                    drRow["ModuleName"] = txtModuleName.Text.Trim();

                    bIDMDataSet.Module.Rows.Add(drRow);

                    this.moduleTableAdapter.Update(bIDMDataSet);
                }

                ClearFields();
                this.moduleTableAdapter.Fill(this.bIDMDataSet.Module);

                btnEdit.Visible = true;
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {

                DataRow drRowEdit = this.bIDMDataSet.Module.Rows[iRow];

                txtModuleName.Text = drRowEdit["ModuleName"].ToString();

                moduleId = (int)drRowEdit["ModuleId"];

                btnEdit.Visible = false;
            }


            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void ClearFields()
        {
            txtModuleName.Text = "";
            moduleId = 0;
        }

        
        private void dgvChannels_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvChannels.CurrentCell != null)
                iRow = dgvChannels.CurrentCell.RowIndex;
        }
    }
}

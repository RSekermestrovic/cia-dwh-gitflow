﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VIPDashboard
{
    public partial class Login : Global
    {
        public Login()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            BIDDataSet.DIM_VIP_Dashboard_UserDataTable dtTable =
              this.dIM_VIP_Dashboard_UserTableAdapter.GetDataByUser(txtUserName.Text.Trim(), txtPassword.Text.Trim());

            if (dtTable.Rows.Count == 0)
            {
                MessageBox.Show("Login Failed");
                DialogResult = System.Windows.Forms.DialogResult.None;   
               
            }
           else
            {
                try
                {

                    UserName = txtUserName.Text.Trim();
                    NameoftheUser = dtTable.Rows[0]["Name"].ToString();
                    Role = dtTable.Rows[0]["Role"].ToString();
                    DialogResult = System.Windows.Forms.DialogResult.OK;

                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.GetBaseException().Message);
                }
            }
        }

        private void Login_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'bIDDataSet.DIM_VIP_Dashboard_User' table. You can move, or remove it, as needed.
            this.dIM_VIP_Dashboard_UserTableAdapter.Fill(this.bIDDataSet.DIM_VIP_Dashboard_User);

        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtUserName.Text = "";
            txtPassword.Text = "";
        }

        private void Login_Leave(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}

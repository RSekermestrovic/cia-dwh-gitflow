﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VIPDashboard
{
    public partial class AdminCategory : Form
    {

        private int iRow;
        private int updateCategory;

        public AdminCategory()
        {
            InitializeComponent();
        }

        private void AdminCategory_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'bIDDataSet.DIM_VIP_Dashboard_Reasons' table. You can move, or remove it, as needed.
            this.dIM_VIP_Dashboard_ReasonsTableAdapter.Fill(this.bIDDataSet.DIM_VIP_Dashboard_Reasons);
            // TODO: This line of code loads data into the 'bIDDataSet.DIM_VIP_Dashboard_Categorys' table. You can move, or remove it, as needed.

            this.tsDelete.Click += tsDelete_Click;

        }

        private void tsDelete_Click(object sender, EventArgs e)
        {
            try
            {

                if (MessageBox.Show("Do you want to delete the highlighted record ?", "VIP Manager", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
                    == System.Windows.Forms.DialogResult.Yes)
                {
                    DataRow drRowDelete = this.bIDDataSet.DIM_VIP_Dashboard_Reasons.Rows[iRow];

                    this.dIM_VIP_Dashboard_ReasonsTableAdapter.DeleteCategory((int)(drRowDelete["Reason"]));

                    this.dIM_VIP_Dashboard_ReasonsTableAdapter.Update(bIDDataSet);

                    this.dIM_VIP_Dashboard_ReasonsTableAdapter.Fill(this.bIDDataSet.DIM_VIP_Dashboard_Reasons);
                }

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "VIP Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                if (txtCategory.Text.Trim() == "")
                {
                    MessageBox.Show("Entered Category ID is not valid.", "VIP Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else if (txtCategoryName.Text.Trim() == "")
                {
                    MessageBox.Show("Entered Category Name is not valid.", "VIP Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }


                if (updateCategory != 0)
                {

                    this.dIM_VIP_Dashboard_ReasonsTableAdapter.UpdateCategory(Convert.ToInt32(txtCategory.Text), txtCategoryName.Text.Trim(),cmbType.SelectedItem.ToString(), updateCategory);

                    this.dIM_VIP_Dashboard_ReasonsTableAdapter.Update(bIDDataSet);

                }
                else
                {
                    DataRow drRow = this.bIDDataSet.DIM_VIP_Dashboard_Reasons.NewRow();

                    drRow["Reason"] = txtCategory.Text.Trim();
                    drRow["ReasonName"] = txtCategoryName.Text.Trim();
                    drRow["Type"] = cmbType.SelectedItem.ToString();

                    bIDDataSet.DIM_VIP_Dashboard_Reasons.Rows.Add(drRow);

                    this.dIM_VIP_Dashboard_ReasonsTableAdapter.Update(bIDDataSet);
                }

                ClearFields();
                this.dIM_VIP_Dashboard_ReasonsTableAdapter.Fill(this.bIDDataSet.DIM_VIP_Dashboard_Reasons);

                btnEdit.Visible = true;
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "VIP Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {

                DataRow drRowEdit = this.bIDDataSet.DIM_VIP_Dashboard_Reasons.Rows[iRow];

                txtCategory.Text = drRowEdit["Reason"].ToString();
                txtCategoryName.Text = drRowEdit["ReasonName"].ToString();
                cmbType.SelectedItem = drRowEdit["Type"];

                updateCategory = (int)drRowEdit["Reason"];

                btnEdit.Visible = false;
            }


            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "VIP Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void ClearFields()
        {
            txtCategory.Text = "";
            txtCategoryName.Text = "";
            updateCategory = 0;
            cmbType.SelectedIndex = -1;
        }

        private void txtCategory_TextChanged(object sender, EventArgs e)
        {
            int Category;
            if (!int.TryParse(txtCategory.Text, out Category) && txtCategory.Text != "")
            {
                MessageBox.Show("Entered Category ID is not valid.", "VIP Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void dgvCategorys_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvCategory.CurrentCell != null)
                iRow = dgvCategory.CurrentCell.RowIndex;
        }
    }
}

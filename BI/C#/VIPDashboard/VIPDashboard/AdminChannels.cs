﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VIPDashboard
{
    public partial class AdminChannels : Form
    {

        private int iRow;
        private int updateChannel;

        public AdminChannels()
        {
            InitializeComponent();
        }

        private void AdminChannels_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'bIDDataSet.DIM_VIP_Dashboard_Channels' table. You can move, or remove it, as needed.
            this.dIM_VIP_Dashboard_ChannelsTableAdapter.Fill(this.bIDDataSet.DIM_VIP_Dashboard_Channels);

            this.tsDelete.Click += tsDelete_Click;

        }

        private void tsDelete_Click(object sender, EventArgs e)
        {
            try
            {

                if (MessageBox.Show("Do you want to delete the highlighted record ?", "VIP Manager", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
                    == System.Windows.Forms.DialogResult.Yes)
                {
                    DataRow drRowDelete = this.bIDDataSet.DIM_VIP_Dashboard_Channels.Rows[iRow];

                    this.dIM_VIP_Dashboard_ChannelsTableAdapter.DeleteChannel((int)(drRowDelete["Channel"]));         

                    this.dIM_VIP_Dashboard_ChannelsTableAdapter.Update(bIDDataSet);

                    this.dIM_VIP_Dashboard_ChannelsTableAdapter.Fill(this.bIDDataSet.DIM_VIP_Dashboard_Channels);
                }

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "VIP Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                if (txtChannel.Text.Trim() == "")
                {
                    MessageBox.Show("Entered Channel ID is not valid.", "VIP Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else if (txtChannelName.Text.Trim() == "")
                {
                    MessageBox.Show("Entered Channel Name is not valid.", "VIP Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }


                if (updateChannel != 0)
                {

                    this.dIM_VIP_Dashboard_ChannelsTableAdapter.UpdateChannels(Convert.ToInt32(txtChannel.Text), txtChannelName.Text.Trim(), updateChannel);

                    this.dIM_VIP_Dashboard_ChannelsTableAdapter.Update(bIDDataSet);

                }
                else
                {
                    DataRow drRow = this.bIDDataSet.DIM_VIP_Dashboard_Channels.NewRow();

                    drRow["Channel"] = txtChannel.Text.Trim();
                    drRow["ChannelName"] = txtChannelName.Text.Trim();

                    bIDDataSet.DIM_VIP_Dashboard_Channels.Rows.Add(drRow);

                    this.dIM_VIP_Dashboard_ChannelsTableAdapter.Update(bIDDataSet);
                }

                ClearFields();
                this.dIM_VIP_Dashboard_ChannelsTableAdapter.Fill(this.bIDDataSet.DIM_VIP_Dashboard_Channels);

                btnEdit.Visible = true;
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "VIP Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {

                DataRow drRowEdit = this.bIDDataSet.DIM_VIP_Dashboard_Channels.Rows[iRow];

                txtChannel.Text = drRowEdit["Channel"].ToString();
                txtChannelName.Text = drRowEdit["ChannelName"].ToString();

                updateChannel = (int)drRowEdit["Channel"];

                btnEdit.Visible = false;
            }


            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "VIP Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void ClearFields()
        {
            txtChannel.Text = "";
            txtChannelName.Text = "";
            updateChannel = 0;
        }

        private void txtChannel_TextChanged(object sender, EventArgs e)
        {
            int Channel;
            if (!int.TryParse(txtChannel.Text, out Channel) && txtChannel.Text != "")
            {
                MessageBox.Show("Entered Channel ID is not valid.", "VIP Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void dgvChannels_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvChannels.CurrentCell != null)
                iRow = dgvChannels.CurrentCell.RowIndex;
        }
    }
}

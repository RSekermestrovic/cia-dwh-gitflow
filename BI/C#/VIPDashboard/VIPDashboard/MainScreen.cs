﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VIPDashboard
{
    public partial class MainScreen : Global
    {
        public MainScreen()
        {
            InitializeComponent();


            if (Role == "Admin")
                tsmAdmin.Visible = true;
            else
                tsmAdmin.Visible = false;
       
        }
        
        private void vIPCommunicationsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Communications vipComm = new Communications();

            vipComm.MdiParent = this;

            vipComm.Show();

            this.LayoutMdi(MdiLayout.TileVertical);
        }

        private void vIPGiftingHospitalityToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GiftingHospitality vipGifting = new GiftingHospitality();

            vipGifting.MdiParent = this;

            vipGifting.Show();

            this.LayoutMdi(MdiLayout.TileVertical);
        }

        private void channelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AdminChannels channels = new AdminChannels();

            channels.MdiParent = this;

            channels.Show();
        }

        private void categoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AdminCategory category = new AdminCategory();

            category.MdiParent = this;

            category.Show();
        }

        private void usersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AdminUsers user = new AdminUsers();

            user.MdiParent = this;

            user.Show();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

    }
}

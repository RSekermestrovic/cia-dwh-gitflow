﻿##Find empty folders

$Item = @{
Path = “F:\”
Recurse = $true
Force = $true
}
$Filter = @{
FilterScript = {$_.PSIsContainer -eq $True}
}
$Where = @{
FilterScript = {($_.GetFiles().Count -eq 0) -and
($_.GetDirectories().Count -eq 0)}
}
$a = Get-ChildItem @Item |Where-Object @Filter
$a |Where-Object @Where |Select-Object FullName
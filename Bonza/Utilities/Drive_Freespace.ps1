﻿Function Get-DisksSpace ($ServerName, $unit= “GB”)
{
$measure = “1$unit”
Get-WmiObject -ComputerName $ServerName -query “
SELECT SystemName, Name, DriveType, FileSystem, FreeSpace, Capacity, Label
FROM Win32_Volume
WHERE DriveType = 2 or DriveType = 3” |
SELECT SystemName, `
Name, `
@{Label=”SizeIn$unit”;Exp={“{0:n2}” -f($_.Capacity/$measure)}}, `
@{Label=”FreeIn$unit”;Exp={“{0:n2}” -f($_.freespace/$measure)}}, `
@{Label=”PercentFree”;Exp={“{0:n2}” -f(($_.freespace/$_.Capacity)*100)}}, `
Label
}#Get-DisksSpace

Get-DisksSpace LocalHost MB
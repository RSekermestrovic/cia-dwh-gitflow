﻿##############################################################################
##
## Get-DiskUsage
##
## From Windows PowerShell Cookbook (O'Reilly)
## by Lee Holmes (http://www.leeholmes.com/guide)
##
##############################################################################

# Usage: .\D:\cia-dwh\Bonza\Utilities\Get_DiskUsage.ps1

param(
## Switch to include subdirectories in the size of each directory
[switch] $IncludeSubdirectories
)
Set-StrictMode -Version 3
## If they specify the -IncludeSubdirectories flag, then we want to account
## for all subdirectories in the size of each directory
if($includeSubdirectories)
{
Get-ChildItem -Directory |
Select-Object Name,@{ Name="Size";
Expression={ ($_ | Get-ChildItem -Recurse |
Measure-Object -Sum Length).Sum + 0 } }
}
## Otherwise, we just find all directories below the current directory,
## and determine their size
else
{
Get-ChildItem -Recurse -Directory |
Select-Object FullName,
@{ Name="Size";
Expression={ ($_ | Get-ChildItem |
Measure-Object -Sum Length).Sum + 0 } }
}
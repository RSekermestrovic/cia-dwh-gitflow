################### DESCRIPTION ###########
## DEVELOPER: AARON JACKSON
## DATE: 04/08/2015
## DESC: Utility to build projects using powershell
## https://git-scm.com/docs/git-merge
################## END DESCRIPTION ########

## TEST
# .\Merge_Git_Branches.ps1 -Base_Dir "D:\cia-dwh\Bonza\"
##

################ VARIABLES #################
Param (
    [String]
    [ValidateScript({Test-Path $_ -PathType 'Container'})]
    [Parameter(Mandatory = $true)] 
    $Base_Dir
)
################ END VARIABLES #################


################ SCRIPT ########################

    Clear-Host

    cd $Base_Dir

    Write-Host "**** Merge changes from Development **** `n"

    #check no outstanding commits

    git merge origin/Development_Branch

    #check all is good

    If ($LASTEXITCODE -eq 0)
    {
        git commit
    }
    Else
    {
        #manually merge
    }

    <#Write-Host "**** Merge changes from Test **** `n"

    git merge origin/Version_15.9.0

    #check all is good

    If ($LASTEXITCODE -eq 0)
    {
        git commit
    }
    Else
    {
        #manually merge
    }#>

<#    Write-Host "**** Merge changes from Master **** `n"

    git merge origin/master

    #check all is good

    If ($LASTEXITCODE -eq 0)
    {
        git commit
    }
    Else
    {
        #manually merge
    }
    #>
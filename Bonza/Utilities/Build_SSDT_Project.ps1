################### DESCRIPTION ###########
## DEVELOPER: AARON JACKSON
## DATE: 04/08/2015
## DESC: Utility to build projects using powershell
################## END DESCRIPTION ########

## Usage
# .\Build_SSDT_Project.ps1 -Base_Dir "D:\cia-dwh\Bonza\" -Env "POC" -Action "Build"
##

################ VARIABLES #################
Param (
    [String]
    [ValidateScript({Test-Path $_ -PathType 'Container'})]
    [Parameter(Mandatory = $true)] 
    $Base_Dir,
    
    [String]
    [ValidateSet("POC", "POC2", "DEV", "DEV2")]
    [Parameter(Mandatory = $true)] 
    $Env,

    [String]
    [ValidateSet("Clean", "Build", "IncrementalBuild", "Deploy", "DeployOnly")]
    [Parameter(Mandatory = $true)] 
    $Action
)
################ END VARIABLES #################


################ SCRIPT ########################

    Clear-Host

	[bool]$BuildSuccessFlag = $false
	[bool]$DeploySuccessFlag = $false
	[bool]$BuildFlag = $false
	[bool]$DeployFlag = $false
	[bool]$CleanFlag = $false

	$Utilities_Dir = $Base_Dir + "Utilities\"
    $StagingProject = $Base_Dir + "Staging\Staging.sqlproj" 
    $DimensionalProject = $Base_Dir + "Dimensional\Dimensional.sqlproj"

    $env:VisualStudioVersion=11.0

	Switch ($Env)
    {
        "POC" 
		{
			$StagingPublishProfile = $Base_Dir + "Staging\Publish Settings\localhost.publish.xml"
			$DimensionalPublishProfile = $Base_Dir + "Dimensional\Publish Settings\localhost.publish.xml"
		}

		"POC2" 
		{
			$StagingPublishProfile = $Base_Dir + "Staging\Publish Settings\localhostUpdate.publish.xml"
			$DimensionalPublishProfile = $Base_Dir + "Dimensional\Publish Settings\localhostUpdate.publish.xml"
		}

        "DEV" 
		{
			$StagingPublishProfile = $Base_Dir + "Staging\Publish Settings\AJDev.publish.xml"
			$DimensionalPublishProfile = $Base_Dir + "Dimensional\Publish Settings\AJDev.publish.xml"
		}
		
		"DEV2" 
		{
			$StagingPublishProfile = $Base_Dir + "Staging\Publish Settings\AJDevUpdate.publish.xml"
			$DimensionalPublishProfile = $Base_Dir + "Dimensional\Publish Settings\AJDevUpdate.publish.xml"
		}
    }

	Switch ($Action)
    {
        "Clean" 
		{
			$Target = "Clean"
			$DeployFlag = $false
			$BuildFlag = $true
			$CleanFlag = $true
		}

        "Build" 
		{
			$Target = "Clean;Compile"
			$DeployFlag = $false
			$BuildFlag = $true
			$CleanFlag = $true
		}

		"IncrementalBuild" 
		{
			$Target = "Compile"
			$DeployFlag = $false
			$BuildFlag = $true
		}

		"Deploy" 
		{
			$Target = "Clean;Compile"
			$DeployFlag = $true
			$BuildFlag = $true
			$CleanFlag = $true
		}

		"DeployOnly" 
		{
			$Target = "Clean;Compile"
			$DeployFlag = $true
			$BuildFlag = $false
			$BuildSuccessFlag = $true

		}
    }

	#Clean

	if ($CleanFlag -eq $true)
	{

		Write-Host "Cleaning..."

		Remove-Item -Path $Base_Dir"Acquisition\bin\" -Include *.* -Recurse
		Remove-Item -Path $Base_Dir"Acquisition\obj\" -Include *.* -Recurse

		Remove-Item -Path $Base_Dir"Staging\bin\" -Include *.* -Recurse
		Remove-Item -Path $Base_Dir"Staging\obj\" -Include *.* -Recurse

		Remove-Item -Path $Base_Dir"Dimensional\bin\" -Include *.* -Recurse
		Remove-Item -Path $Base_Dir"Dimensional\obj\" -Include *.* -Recurse

	}
    
    #Begin Build

    CD "C:\Program Files (x86)\MSBuild\12.0\bin\"

	if ($BuildFlag -eq $true)
	{

		.\msbuild.exe $StagingProject /t:$Target
		
		$get_date = Get-Date
		Write-Host "Build finished: $get_date"

		If ($LASTEXITCODE -eq 0)
		{
			Write-Host "`nBuild Successful" -ForegroundColor Green
			$BuildSuccessFlag = $true
		}
		Else
		{
			Write-Error "`nBuild Failed"
		}

	}

	<#
		Create dacpac output required for deployment
	#>
    
    # Deploy solution

    If ($BuildSuccessFlag -and $DeployFlag)
    {
        
        $Target = "Publish"
				
		If (Test-Path $Base_Dir"Dimensional\bin\Output\Dimensional.dacpac")
		{
			Copy-Item $Base_Dir"Dimensional\bin\Output\Dimensional.dacpac" $Base_Dir"Dimensional\bin\Debug\" -Force
		}     

        .\msbuild.exe $DimensionalProject /t:$Target /p:SqlPublishProfilePath=$DimensionalPublishProfile

		$get_date = Get-Date
		Write-Host "Build finished: $get_date"

        If ($LASTEXITCODE -eq 0)
        {
            Write-Host "`nDeploy Successful" -ForegroundColor Green
			$DeploySuccessFlag = $true
        }
        Else
        {
            Write-Error "`nDeploy Failed"
        }

    }

	If (($BuildSuccessFlag -eq $true) -and ($DeploySuccessFlag -eq $true))
    {
        $Target = "Publish"
    
		If (Test-Path $Base_Dir"Staging\bin\Output\Staging.dacpac")
		{
			Copy-Item $Base_Dir"Staging\bin\Output\Staging.dacpac" $Base_Dir"Staging\bin\Debug\" -Force
		}    

        .\msbuild.exe $StagingProject /t:$Target /p:SqlPublishProfilePath=$StagingPublishProfile

		$get_date = Get-Date
		Write-Host "Build finished: $get_date"

        If ($LASTEXITCODE -eq 0)
        {
            Write-Host "`nDeploy Successful" -ForegroundColor Green
        }
        Else
        {
            Write-Error "`nDeploy Failed"
        }

    }

	CD $Utilities_Dir
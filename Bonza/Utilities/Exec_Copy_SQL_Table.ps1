################### DESCRIPTION ###########
## DEVELOPER: AARON JACKSON
## DATE: 31/07/2015
## DESC: Copy selected tables into a new environment
################## END DESCRIPTION ########

<#
#Lookups
Start-Job -ScriptBlock {& D:\cia-dwh\Bonza\Utilities\Copy_SQL_Table.ps1 -SrcServer "EQ3WNDVDB01" -SrcDatabase "Acquisition" -SrcTable "IntraBet.tblLULedgers" -DestServer "localhost" -Truncate} | Out-Null
Start-Job -ScriptBlock {& D:\cia-dwh\Bonza\Utilities\Copy_SQL_Table.ps1 -SrcServer "EQ3WNDVDB01" -SrcDatabase "Acquisition" -SrcTable "IntraBet.tblLUVenues" -DestServer "localhost" -Truncate} | Out-Null
Start-Job -ScriptBlock {& D:\cia-dwh\Bonza\Utilities\Copy_SQL_Table.ps1 -SrcServer "EQ3WNDVDB01" -SrcDatabase "Acquisition" -SrcTable "IntraBet.tblLUStates" -DestServer "localhost" -Truncate} | Out-Null
Start-Job -ScriptBlock {& D:\cia-dwh\Bonza\Utilities\Copy_SQL_Table.ps1 -SrcServer "EQ3WNDVDB01" -SrcDatabase "Acquisition" -SrcTable "Intrabet.tblLUEventTypes" -DestServer "localhost" -Truncate} | Out-Null
Start-Job -ScriptBlock {& D:\cia-dwh\Bonza\Utilities\Copy_SQL_Table.ps1 -SrcServer "EQ3WNDVDB01" -SrcDatabase "Acquisition" -SrcTable "IntraBet.tblLUHeardAboutUs" -DestServer "localhost" -Truncate} | Out-Null
Start-Job -ScriptBlock {& D:\cia-dwh\Bonza\Utilities\Copy_SQL_Table.ps1 -SrcServer "EQ3WNDVDB01" -SrcDatabase "Acquisition" -SrcTable "IntraBet.tblLUCountry" -DestServer "localhost" -Truncate} | Out-Null
 
While ($(Get-Job -State Running).count -gt 0){
                   
            #waiiiit for the jobs to finish
        
        }
        
Get-Job | Receive-Job | Select-Object * -ExcludeProperty RunspaceId | out-gridview

#Client
Start-Job -ScriptBlock {& D:\cia-dwh\Bonza\Utilities\Copy_SQL_Table.ps1 -SrcServer "EQ3WNDVDB01" -SrcDatabase "Acquisition" -SrcTable "Intrabet.tblClientWithdrawRequest" -DestServer "localhost" -Truncate} | Out-Null
Start-Job -ScriptBlock {& D:\cia-dwh\Bonza\Utilities\Copy_SQL_Table.ps1 -SrcServer "EQ3WNDVDB01" -SrcDatabase "Acquisition" -SrcTable "Intrabet.tblClientBpaydetails" -DestServer "localhost" -Truncate} | Out-Null
Start-Job -ScriptBlock {& D:\cia-dwh\Bonza\Utilities\Copy_SQL_Table.ps1 -SrcServer "EQ3WNDVDB01" -SrcDatabase "Acquisition" -SrcTable "Intrabet.tblClientBankdetails" -DestServer "localhost" -Truncate} | Out-Null
Start-Job -ScriptBlock {& D:\cia-dwh\Bonza\Utilities\Copy_SQL_Table.ps1 -SrcServer "EQ3WNDVDB01" -SrcDatabase "Acquisition" -SrcTable "IntraBet.tblClientInfo" -DestServer "localhost" -Truncate} | Out-Null
Start-Job -ScriptBlock {& D:\cia-dwh\Bonza\Utilities\Copy_SQL_Table.ps1 -SrcServer "EQ3WNDVDB01" -SrcDatabase "Acquisition" -SrcTable "IntraBet.tblClientAddresses" -DestServer "localhost" -Truncate} | Out-Null
Start-Job -ScriptBlock {& D:\cia-dwh\Bonza\Utilities\Copy_SQL_Table.ps1 -SrcServer "EQ3WNDVDB01" -SrcDatabase "Acquisition" -SrcTable "IntraBet.tblClients" -DestServer "localhost" -Truncate} | Out-Null
 
While ($(Get-Job -State Running).count -gt 0){
                   
            #waiiiit for the jobs to finish
        
        }
        
Get-Job | Receive-Job | Select-Object * -ExcludeProperty RunspaceId | out-gridview

Start-Job -ScriptBlock {& D:\cia-dwh\Bonza\Utilities\Copy_SQL_Table.ps1 -SrcServer "EQ3WNDVDB01" -SrcDatabase "Acquisition" -SrcTable "Intrabet.tblMoneybookersdepositresponse" -DestServer "localhost" -Truncate} | Out-Null
Start-Job -ScriptBlock {& D:\cia-dwh\Bonza\Utilities\Copy_SQL_Table.ps1 -SrcServer "EQ3WNDVDB01" -SrcDatabase "Acquisition" -SrcTable "Intrabet.tblCreditCardTransactions" -DestServer "localhost" -Truncate} | Out-Null
Start-Job -ScriptBlock {& D:\cia-dwh\Bonza\Utilities\Copy_SQL_Table.ps1 -SrcServer "EQ3WNDVDB01" -SrcDatabase "Acquisition" -SrcTable "Intrabet.tblPayPalDEC" -DestServer "localhost" -Truncate} | Out-Null
Start-Job -ScriptBlock {& D:\cia-dwh\Bonza\Utilities\Copy_SQL_Table.ps1 -SrcServer "EQ3WNDVDB01" -SrcDatabase "Acquisition" -SrcTable "Intrabet.tblPayPalGEC" -DestServer "localhost" -Truncate} | Out-Null

While ($(Get-Job -State Running).count -gt 0){
                   
            #waiiiit for the jobs to finish
        
        }
        
Get-Job | Receive-Job | Select-Object * -ExcludeProperty RunspaceId | out-gridview
#>


.\Copy_SQL_Table.ps1 -SrcServer "EQ3WNDVDB01" -SrcDatabase "Acquisition" -SrcTable "IntraBet.tblIntroducers" -DestServer "localhost" -Truncate
.\Copy_SQL_Table.ps1 -SrcServer "EQ3WNDVDB01" -SrcDatabase "Acquisition" -SrcTable "IntraBet.tblUsers" -DestServer "localhost" -Truncate

.\Copy_SQL_Table.ps1 -SrcServer "EQ3WNDVDB01" -SrcDatabase "Acquisition" -SrcTable "Intrabet.tblCancelledBets" -DestServer "localhost" -Truncate
.\Copy_SQL_Table.ps1 -SrcServer "EQ3WNDVDB01" -SrcDatabase "Acquisition" -SrcTable "IntraBet.Latency" -DestServer "localhost" -Truncate
.\Copy_SQL_Table.ps1 -SrcServer "EQ3WNDVDB01" -SrcDatabase "Acquisition" -SrcTable "IntraBet.tblEvents" -DestServer "localhost" -Truncate
.\Copy_SQL_Table.ps1 -SrcServer "EQ3WNDVDB01" -SrcDatabase "Acquisition" -SrcTable "IntraBet.tblAllUps" -DestServer "localhost" -Truncate
.\Copy_SQL_Table.ps1 -SrcServer "EQ3WNDVDB01" -SrcDatabase "Acquisition" -SrcTable "Intrabet.tblAllUpGroupBets" -DestServer "localhost" -Truncate
.\Copy_SQL_Table.ps1 -SrcServer "EQ3WNDVDB01" -SrcDatabase "Acquisition" -SrcTable "Intrabet.tblLUAllupGroups" -DestServer "localhost" -Truncate
.\Copy_SQL_Table.ps1 -SrcServer "EQ3WNDVDB01" -SrcDatabase "Acquisition" -SrcTable "Intrabet.tblExoticGroupBets" -DestServer "localhost" -Truncate
.\Copy_SQL_Table.ps1 -SrcServer "EQ3WNDVDB01" -SrcDatabase "Acquisition" -SrcTable "Intrabet.tblBonusTransactions" -DestServer "localhost" -Truncate
.\Copy_SQL_Table.ps1 -SrcServer "EQ3WNDVDB01" -SrcDatabase "Acquisition" -SrcTable "Intrabet.tblPromotionalBets" -DestServer "localhost" -Truncate
.\Copy_SQL_Table.ps1 -SrcServer "EQ3WNDVDB01" -SrcDatabase "Acquisition" -SrcTable "Intrabet.tblAccounts" -DestServer "localhost" -Truncate
.\Copy_SQL_Table.ps1 -SrcServer "EQ3WNDVDB01" -SrcDatabase "Acquisition" -SrcTable "Intrabet.tblGroupedBets" -DestServer "localhost" -Truncate
.\Copy_SQL_Table.ps1 -SrcServer "EQ3WNDVDB01" -SrcDatabase "Acquisition" -SrcTable "IntraBet.tblCompetitors" -DestServer "localhost" -Truncate
.\Copy_SQL_Table.ps1 -SrcServer "EQ3WNDVDB01" -SrcDatabase "Acquisition" -SrcTable "IntraBet.tblRacingResults" -DestServer "localhost" -Truncate

.\Copy_SQL_Table.ps1 -SrcServer "EQ3WNDVDB01" -SrcDatabase "Acquisition" -SrcTable "IntraBet.tblTransactions" -DestServer "localhost" -Truncate
.\Copy_SQL_Table.ps1 -SrcServer "EQ3WNDVDB01" -SrcDatabase "Acquisition" -SrcTable "IntraBet.tblDeletedTransactions" -DestServer "localhost" -Truncate
.\Copy_SQL_Table.ps1 -SrcServer "EQ3WNDVDB01" -SrcDatabase "Acquisition" -SrcTable "IntraBet.tblTransactionLogs" -DestServer "localhost" -Truncate
.\Copy_SQL_Table.ps1 -SrcServer "EQ3WNDVDB01" -SrcDatabase "Acquisition" -SrcTable "IntraBet.tblPromotions" -DestServer "localhost" -Truncate
.\Copy_SQL_Table.ps1 -SrcServer "EQ3WNDVDB01" -SrcDatabase "Acquisition" -SrcTable "Intrabet.tblBets" -DestServer "localhost" -Truncate
#>
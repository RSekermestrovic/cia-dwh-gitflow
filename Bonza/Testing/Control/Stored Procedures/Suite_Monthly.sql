﻿Create Procedure [control].Suite_Monthly

as

Declare @TestRunNo varchar(23)
Set @TestRunNo = convert (varchar(23),getdate(),126)

Begin Try 
	exec [control].ExecuteTestSuite 3,3,3,3,3,3,3,3,1,@TestRunNo
End Try
Begin Catch
	EXEC msdb.dbo.sp_send_dbmail
	@profile_name = 'DataWarehouse_Auto_Mails',
	@body = 'Please investigate!',
	@body_format ='TEXT',
	@recipients = 'bi.service@williamhill.com.au', 
	@subject = 'The Suite_Monthly has failed';
End Catch

Select		*
into		#Temp
From		[test].[tblResults]
where		RunID = @TestRunNo
			and [Status] != 'Pass'

Declare @Counter int
Set @Counter = (
				Select		count (*)
				From		#Temp
				)
				 
Declare @SubjectLine varchar(1000)
Set @SubjectLine = test.fnGetServer() + ' Suite_Monthly - ' + @TestRunNo

IF @Counter = 0
	Begin
		Print 'No Tests Failed'
	End
	Else
	Begin
		DECLARE @xml NVARCHAR(MAX)
		DECLARE @body NVARCHAR(MAX)

		SET @xml = CAST(
						( SELECT	[ID] AS 'td'
									,''
									, RunID AS 'td'
									,''
									, TestID AS 'td'
									,''
									, TestName AS 'td'
									,''
									, Notes AS 'td'
							FROM		#Temp 
							ORDER BY TestID 
							FOR XML PATH('tr'), ELEMENTS 
							) AS NVARCHAR(MAX)
						)

		SET @body =	'<html>
						<body>
							<H3>Failed Tests</H3>
							<table border = 1> 
							<tr>
								<th> ID </th>
								<th> RunID </th>
								<th> TestID </th>
								<th> TestName </th>
								<th> Notes </th>
							</tr>'    

		SET @body = @body + @xml +'</table></body></html>'

		EXEC msdb.dbo.sp_send_dbmail
		@profile_name = 'DataWarehouse_Auto_Mails',
		@body = @body,
		@body_format ='HTML',
		@recipients = 'bi.service@williamhill.com.au', 
		@subject = @SubjectLine;
	End
﻿--Tests
--exec [control].ExecuteTestSuite 1,2,1,0,0,0,0,0,0,'Adhoc'
--exec [control].ExecuteTestSuite 1,0,0,0,0,0,0,0,1,'Adhoc'
--exec [control].ExecuteTestSuite 1,1,1,1,0,0,0,0,1,'Adhoc'
Create Procedure [control].ExecuteTestSuite @Balance int, @Position int, @KPI int, @Transaction int, @Bet int, @Risk int, @Contact int, @Factor int, @Monitoring int = 0, @TestRunNo varchar(23)

as

Declare @FromDateText varchar(10)
If @Monitoring = 0
Begin
	Set @FromDateText = 'null'
End
Else
Begin
	Set @FromDateText = char(39) + (Select Convert (varchar(8), Max (BatchFromDate),112) from [$(Staging)].[Control].[ETLController] where BatchStatus = 1) + char(39)
End

If @Balance > 0
Begin
	Insert into [Control].[Runs] ([RunType], [RunID], Start) 
	(Select	'Suite_Balance_' + Suite as [RunType]
			, @TestRunNo as [RunID]
			, getdate() as Start
	From	[Control].[SuiteLevels]
	Where	[Level] = @Balance
	)
End

If @Position > 0
Begin
	Insert into [Control].[Runs] ([RunType], [RunID], Start) 
	(Select	'Suite_Position_' + Suite as [RunType]
			, @TestRunNo as [RunID]
			, getdate() as Start
	From	[Control].[SuiteLevels]
	Where	[Level] = @Position
	)
End

If @KPI > 0
Begin
	Insert into [Control].[Runs] ([RunType], [RunID], Start) 
	(Select	'Suite_KPI_' + Suite as [RunType]
			, @TestRunNo as [RunID]
			, getdate() as Start
	From	[Control].[SuiteLevels]
	Where	[Level] = @KPI
	)
End

If @Transaction > 0
Begin
	Insert into [Control].[Runs] ([RunType], [RunID], Start) 
	(Select	'Suite_Transaction_' + Suite as [RunType]
			, @TestRunNo as [RunID]
			, getdate() as Start
	From	[Control].[SuiteLevels]
	Where	[Level] = @Transaction
	)
End

If @Bet > 0
Begin
	Insert into [Control].[Runs] ([RunType], [RunID], Start) 
	(Select	'Suite_Bet_' + Suite as [RunType]
			, @TestRunNo as [RunID]
			, getdate() as Start
	From	[Control].[SuiteLevels]
	Where	[Level] = @Bet
	)
End

If @Risk > 0
Begin
	Insert into [Control].[Runs] ([RunType], [RunID], Start) 
	(Select	'Suite_Risk_' + Suite as [RunType]
			, @TestRunNo as [RunID]
			, getdate() as Start
	From	[Control].[SuiteLevels]
	Where	[Level] = @Risk
	)
End

If @Contact > 0
Begin
	Insert into [Control].[Runs] ([RunType], [RunID], Start) 
	(Select	'Suite_Contact_' + Suite as [RunType]
			, @TestRunNo as [RunID]
			, getdate() as Start
	From	[Control].[SuiteLevels]
	Where	[Level] = @Contact
	)
End

If @Factor > 0
Begin
	Insert into [Control].[Runs] ([RunType], [RunID], Start) 
	(Select	'Suite_Factor_' + Suite as [RunType]
			, @TestRunNo as [RunID]
			, getdate() as Start
	From	[Control].[SuiteLevels]
	Where	[Level] = @Factor
	)
End

Select		Header + '[' + StoredProcedure + ']' + Parameter1 + Parameter2 as Sproc
			, StoredProcedure as TestName
			, 'TestID' = CASE
				When Right (Left (StoredProcedure, 3), 1) like '[0-9]' then Left (StoredProcedure, 3)
				When Right (Left (StoredProcedure, 2), 1) like '[0-9]' then Left (StoredProcedure, 2)
				When Right (Left (StoredProcedure, 3), 1) like '[0-9]' then Left (StoredProcedure, 1)
				else 'Unknown'
				End
into		#Tests
From		(
			Select		distinct 
						'Exec Test.'  as Header
						, TestSuites.StoredProcedure
						, 'Parameter1' = CASE
							When ParameterInfo.Name is null then ' ' + char(39) + @TestRunNo + char(39)
							else ' ' + char(39) + @TestRunNo + char(39) + ', '
							end
						, 'Parameter2' = CASE
							When ParameterInfo.Name is null then ''
							Else @FromDateText
							End
			From		[control].TestSuites as TestSuites
						left join	(
									select		[Objects].name
									from		sys.all_objects as [Objects]
												inner join sys.schemas as [Schemas] on [Objects].[schema_id] = [Schemas].[schema_id]
												inner join sys.all_parameters as [Parameters] on [Objects].[object_id] = [Parameters].[object_id]
									where		[Objects].[type] = 'P'
												and [Schemas].name = 'test'
												and [Parameters].name = '@FromDate'
									) as ParameterInfo on TestSuites.StoredProcedure = ParameterInfo.Name
			where		(TestSuites.[Level] = @Balance and TestSuites.StarName = 'Balance')
						or (TestSuites.[Level] = @Position and TestSuites.StarName = 'Position')
						or (TestSuites.[Level] = @KPI and TestSuites.StarName = 'KPI')
						or (TestSuites.[Level] = @Transaction and TestSuites.StarName = 'Transaction')
			) as Data

--Cursor
declare @Sproc varchar(1000)
declare @TestName varchar(1000)
declare @TestID varchar(10)

declare TestCursor CURSOR LOCAL for
    select Sproc, TestName, TestID from #Tests

open TestCursor

fetch next from TestCursor into @Sproc, @TestName, @TestID

while @@FETCH_STATUS = 0 BEGIN

    --execute sproc on each row
    Begin Try 
		Insert into [Control].[Runs] ([RunType], [RunID], Start) Values (@TestName, @TestRunNo, getdate())
		Execute (@Sproc)
		Update [Control].[Runs] set [End] = getdate() where RunID = @TestRunNo and RunType = @TestName
	End Try

	Begin Catch
		Insert into Test.tblResults Values (@TestRunNo, @TestID, @TestName, 'Fail', ERROR_MESSAGE() + ' ' + @Sproc)
		Update [Control].[Runs] set [End] = getdate() where RunID = @TestRunNo and RunType = @TestName
	End Catch

    fetch next from TestCursor into @Sproc, @TestName, @TestID
END

close TestCursor
deallocate TestCursor

Update [Control].[Runs] set [End] = getdate() where RunID = @TestRunNo and RunType like 'Suite_%'

Go
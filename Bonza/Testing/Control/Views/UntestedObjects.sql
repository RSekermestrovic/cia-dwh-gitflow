﻿Create View [control].UntestedObjects

as

Select		Distinct 
			Coalesce (TestedObjects.TestName, (ObjectsUnderTest.[Table] + '.' + ObjectsUnderTest.[Column]))  as 'TableName'
			, TestTypes.TestType
			, TestTypes.[Priority]
			, Tests.StoredProcedure
From		[Control].ObjectsUnderTest as ObjectsUnderTest
			left join [control].TestedObjects as TestedObjects on ObjectsUnderTest.[Table] = TestedObjects.StarName
																	and ObjectsUnderTest.[Column] = TestedObjects.FieldName
			left join [Control].TableTypes as TableTypes on TestedObjects.TestName = TableTypes.TestName
			left join [Control].TestTypes as TestTypes on TableTypes.TableType = TestTypes.TableType
			left join [Control].Tests as Tests on TestedObjects.TestName = Tests.TestName
													and TestTypes.TestType = Tests.TestType
Where		(
			Tests.StoredProcedure is null
			and TestedObjects.IsTestable != 0
			and TestTypes.TestType not like 'Defects%'
			)
						
			Or
						
			(
			Tests.StoredProcedure is not null
			and TestedObjects.IsTestable != 0
			and TestTypes.TestType not like 'Defects%'
			and (Tests.Complete is null or Tests.Complete <> 1)
			)
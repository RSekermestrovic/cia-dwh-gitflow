﻿CREATE VIEW [Control].[FailedTests]

AS

select	Results.*
		, jira.[Priority] as [Priority]
		, jira.FoundVersion
		, jira.Resolution
		, jira.[Version] as FixedVersion
from	Test.tblResults as Results
		left join test.tblJira as Jira on Results.Notes = Jira.JiraID
where	notes is not null
		or Results.[Status] != 'Pass'

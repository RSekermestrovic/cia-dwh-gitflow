﻿Create View [control].TestSuites

as

Select		TestedObjects.StarName
			, Suites.Suite
			, SuiteLevels.[Level]
			, Tests.TestType
			, Tests.TestName
			, Tests.StoredProcedure
From		[control].Suites as Suites
			inner join [control].Tests as Tests on Suites.TestType = Tests.TestType
			inner join [control].TestedObjects as TestedObjects on tests.testname = TestedObjects.TestName
			inner join [Control].SuiteLevels as SuiteLevels on Suites.Suite = SuiteLevels.Suite
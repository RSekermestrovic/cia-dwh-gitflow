﻿Create table [control].TestedObjects	(
										ID int identity (1,1)
										, StarName varchar(100)
										, FieldName varchar(1000)
										, IsTestable int
										, TestName varchar(1000)
										)
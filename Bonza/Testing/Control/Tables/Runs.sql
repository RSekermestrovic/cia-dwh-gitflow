﻿Create Table [Control].[Runs] (
							  id int identity (1,1)
							  , [RunType] varchar(100)
							  , [RunID] varchar(23)
							  , Start datetime2
							  , [End] datetime2
							  )
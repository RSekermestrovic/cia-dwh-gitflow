﻿CREATE TABLE [Test].[Uptime](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FromDate] [datetime2] NOT NULL,
	[ToDate] [datetime2] NOT NULL,
	[Status] [int] NOT NULL
) ON [PRIMARY]
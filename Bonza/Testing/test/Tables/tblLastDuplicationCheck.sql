﻿CREATE TABLE [Test].[tblLastDuplicationCheck]
(
	[Id] INT IDENTITY (1, 1) NOT NULL PRIMARY KEY, 
    [TableName] VARCHAR(50) NOT NULL, 
    [LastKeyChecked] INT NOT NULL, 
    [DateTime] DATETIME NOT NULL
)

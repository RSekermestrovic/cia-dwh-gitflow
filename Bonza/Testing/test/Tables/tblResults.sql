﻿CREATE TABLE [Test].[tblResults] (
    [ID]       INT            IDENTITY (1, 1) NOT NULL,
    [RunID]    VARCHAR (23)   NULL,
    [TestID]   VARCHAR (100)  NULL,
    [TestName] VARCHAR (1000) NULL,
    [Status]   VARCHAR (10)   NULL,
    [Notes]    VARCHAR (1000) NULL,
    CONSTRAINT [PK_tblResults] PRIMARY KEY CLUSTERED ([ID] ASC)
);


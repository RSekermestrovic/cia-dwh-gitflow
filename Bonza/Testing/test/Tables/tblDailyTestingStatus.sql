﻿CREATE TABLE [Control].[tblDailyTestingStatus](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TableName] varchar(100) NOT NULL,
	[StartDate] [datetime2](7) NOT NULL,
	[CompleteDate] [datetime2](7) NULL
) ON [PRIMARY]
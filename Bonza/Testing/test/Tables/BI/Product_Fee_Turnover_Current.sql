﻿create table test.Product_Fee_Turnover_Current
(
Operator varchar(1000)
, MeetingDate date
, ChannelName varchar(1000)
, RaceNumber int
, RaceCode varchar(1000)
, MeetingVenueId int
, MeetingVenue varchar(1000)
, MeetingState varchar(1000)
, MeetingCountry varchar(1000)
, Turnover decimal (18,2)
, Payout decimal (18,2)
, MultiTurnover decimal (18,2)
, MultiPayout decimal (18,2)
, BetBackTurnover decimal (18,2)
, BetbackPayout decimal (18,2)
, FreeBetTurnover decimal (18,2)
, FreeBetPayout decimal (18,2) 
)
﻿create table Test.[SP_RevenueBreakdown_Historic]
(
Brandcode varchar(1000)
, ClassName varchar(1000)
, ChannelName varchar(1000)
, GrossWin decimal (18,4)
, Turnover decimal (18,4)
, Payout decimal (18,4)
)

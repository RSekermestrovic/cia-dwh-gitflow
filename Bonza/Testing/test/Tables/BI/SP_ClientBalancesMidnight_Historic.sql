﻿create table test.SP_ClientBalancesMidnight_Historic
(
ClientID int
, PIN int
, Ledger int
, WHACash decimal (18,4)
, CBTCash decimal (18,4)
, TWHCash decimal (18,4)
, AccountTypeName varchar(1000)
)
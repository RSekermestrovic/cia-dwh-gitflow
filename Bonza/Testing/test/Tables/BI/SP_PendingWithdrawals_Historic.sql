﻿create table test.SP_PendingWithdrawals_Historic(
BrandCode varchar(1000)
, PIN int
, Forename varchar(1000)
, Surname varchar(1000)
, ClosingBalance decimal(18,2)
)
﻿CREATE TABLE [test].[Exclusions] (
    [BetId]             BIGINT        NOT NULL,
    [SnapshotName]      VARCHAR (30)  NOT NULL,
    [ReasonId]          INT           NOT NULL,
    [ReasonDescription] VARCHAR (MAX) NULL,
    [Resolved]          VARCHAR (3)   NULL,
    [Jira]              VARCHAR (10)  NULL
);


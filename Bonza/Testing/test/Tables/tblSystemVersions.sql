﻿CREATE TABLE [Test].[tblSystemVersions] (
    [ID]             INT            IDENTITY (1, 1) NOT NULL,
    [Version]        VARCHAR (1000) NOT NULL,
    [DateLive]       DATETIME       NOT NULL,
    [SystemStandard] VARCHAR (20)   NULL,
	[JiraLabel] [varchar](50) NULL
    CONSTRAINT [PK_tblSystemVersions] PRIMARY KEY CLUSTERED ([ID] ASC)
);


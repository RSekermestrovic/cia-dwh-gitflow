﻿CREATE TABLE [Test].[tblQueryPerformance] (
    [ID]       INT            IDENTITY (1, 1) NOT NULL,
    [RunID]    VARCHAR (23)   NULL,
    [TestID]   VARCHAR (100)  NULL,
    [TestName] VARCHAR (1000) NULL,
    [Start]    DATETIME       NULL,
    [Ends]     DATETIME       NULL,
    [MSeconds] INT            NULL,
    [TargetMSeconds] INT NULL, 
    CONSTRAINT [PK_tblQueryPerformance] PRIMARY KEY CLUSTERED ([ID] ASC)
);


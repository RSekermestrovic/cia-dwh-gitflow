﻿--Test Code
--exec Test.[28DataDefault_Dimension_Note] 'ADHOC'
Create Procedure Test.[NoTypeTables] @TestRunNo varchar(23)

as


If not exists  (
				Select		NoteKey
				From		[$(Dimensional)].[Dimension].[Note]
				where		NoteKey = 0
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '28', 'DataDefault_Dimension_Note Table ID0', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '28', 'DataDefault_Dimension_Note Table ID0', 'Fail', 'No ID 0 record found')
End
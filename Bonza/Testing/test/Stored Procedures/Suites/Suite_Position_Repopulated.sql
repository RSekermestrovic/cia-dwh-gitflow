﻿--Test Code
--exec Test.Suite_Position_Repopulated

Create Procedure Test.Suite_Position_Repopulated

as

Declare @TestRunNo varchar(23)
Set @TestRunNo = convert (varchar(23),getdate(),126)

Declare @LastBatch int
Set @LastBatch = (Select Max([BatchKey]) From [$(Staging)].[Control].[ETLController])

Select	*
into	#BatchDetails
From	[$(Staging)].[Control].[ETLController]
Where	[BatchKey] = @LastBatch

Declare @BatchFromDate datetime
Set @BatchFromDate = (Select BatchFromDate From #BatchDetails)

Declare @BatchToDate datetime
Set @BatchToDate = (Select @BatchToDate From #BatchDetails)

Insert into [test].[tblRuns] (RunType, RunID, [TestID], [State], [Time]) values ('Suite_Position_Repopulated', @TestRunNo, '00', 'Start', getdate())

--Tests 1.1 Tables Exist 
exec Test.[63TableExists_Star_Position] @TestRunNo

--Tests 1.2 Fields Exist
exec test.[64FieldExists_Fact_Position] @TestRunNo
exec test.[15FieldExists_Dimension_Class] @TestRunNo
exec test.[60DataDuplicates_Dimension_Time] @TestRunNo
exec test.[8FieldExists_Dimension_Channel] @TestRunNo
exec test.[18FieldExists_Dimension_BetType] @TestRunNo
exec test.[65FieldExists_Dimension_Activity] @TestRunNo
exec test.[3FieldExists_Dimension_Account] @TestRunNo
exec test.[115FieldExists_Dimension_AccountStatus] @TestRunNo
exec test.[66FieldExists_Dimension_Lifecycle] @TestRunNo
exec Test.[143FieldExists_Dimension_ValueTiers] @TestRunNo

--Tests 2.1.1 Timely Update - Slowly Changing 
exec test.[43DataCurrent_Dimension_Day] @TestRunNo
exec test.[73DataCurrent_Dimension_Channel] @TestRunNo
exec test.[76DataCurrent_Dimension_BetType] @TestRunNo
exec test.[77DataCurrent_Dimension_Activity] @TestRunNo
exec test.[78DataCurrent_Dimension_Lifecycle] @TestRunNo

--Tests 2.1.2 Timely Update - Daily Changing 
exec test.[85DataCurrent_Dimension_Class] @TestRunNo
exec test.[87DataCurrent_Dimension_Account] @TestRunNo
exec test.[118DataCurrent_Dimension_AccountStatus] @TestRunNo

--Tests 2.1.3 Timely Update - Constant Changing 
exec Test.[89DataCurrent_Fact_Position] @TestRunNo

--Tests 2.2 Default Records
exec Test.[31DataDefault_Dimension_Class] @TestRunNo
exec Test.[23DataDefault_Dimension_Day] @TestRunNo
exec Test.[25DataDefault_Dimension_Channel] @TestRunNo
exec Test.[34DataDefault_Dimension_BetType] @TestRunNo
--activity
exec Test.[37DataDefault_Dimension_Account] @TestRunNo
exec [Test].[123DataDefault_Dimension_AccountStatus] @TestRunNo
exec Test.[92DataDefault_Dimension_Lifecycle] @TestRunNo
exec [Test].[144DataDefault_Dimension_ValueTiers] @TestRunNo

--Tests 2.3.1 Record Count - Volatile
--Fact_Position
--Account Status

--Tests 2.3.2 Record Count - Non-Volatile
--fact_Position
exec test.[38DataCount_Dimension_Day] @TestRunNo
--channel
--bet type
--activity
exec test.[96DataCount_Dimension_Account] @TestRunNo
--Account Status
--lifecycle
exec Test.[145DataCount_Dimension_ValueTiers] @TestRunNo

--Tests 2.4.1 Record Checksum - Volatile
--fact_posiiton
--Account Status

--Tests 2.4.2 Record Checksum - Non-Volatile
--Class
--Channel
--Bet Type 
--Activity
--Account
--Lifecycle
exec Test.[146DataCheckSum_Dimension_ValueTiers] @TestRunNo

--Tests 3.1 Validation of Records
--fact_position
exec test.[47BVA_Dimension_Class] @TestRunNo, null
exec test.[97BVA_Dimension_Day] @TestRunNo, null
exec test.[111BVA_Dimension_Channel] @TestRunNo, null
exec Test.[142BVA_Dimension_BetType] @TestRunNo, null
exec Test.[137DataValid_Dimension_Activity] @TestRunNo
exec Test.[40BVA_Dimension_Account] @TestRunNo, null
exec Test.[141BVA_Dimension_AccountStatus] @TestRunNo, null
--lifecycle
exec Test.[147DataValid_Dimension_ValueTiers] @TestRunNo

--Tests 3.2 No Duplicates 
exec [Test].[132DataDuplicates_Fact_Position] @TestRunNo, null
exec test.[54DataDuplicates_Dimension_Class] @TestRunNo, null
exec test.[59DataDuplicates_Dimension_Day] @TestRunNo
exec test.[48DataDuplicates_Dimension_Channel] @TestRunNo
exec test.[57DataDuplicates_Dimension_BetType] @TestRunNo
exec [Test].[135DataDuplicates_Dimension_Activity] @TestRunNo, null
exec test.[58DataDuplicates_Dimension_Account] @TestRunNo, null
exec test.[131DataDuplicates_Dimension_AccountStatus] @TestRunNo, null
exec [Test].[136DataDuplicates_Dimension_Lifecycle] @TestRunNo, null
exec [Test].[148DataDuplicates_Dimension_ValueTiers] @TestRunNo

--Tests 3.3 Field Mappings
--fact_Position
exec Test.[149DataValid_Dimension_Class] @TestRunNo
exec Test.[36DataValid_Dimension_Day] @TestRunNo
--Channel
--Bet Type 
exec Test.[137DataValid_Dimension_Activity] @TestRunNo
exec test.[46DataValid_Dimension_Account] @TestRunNo
--Account Status
--Lifecycle
exec Test.[147DataValid_Dimension_ValueTiers] @TestRunNo

--Tests 3.4 Foreign Key Contraints
exec Test.[122DataFK_Fact_Position] @TestRunNo, null

--Tests 3.5 Truncated Values
--Fact_Position
--Class
--Day
--Channel
--Bet Type 
--Activity
--Account
--Account Status
--Lifecycle
--ValueTiers

--Test 3.6 Destructive Injection
--Fact_Position
--Class
--Day
--Channel
--Bet Type 
--Activity
--Account
--Account Status
--Lifecycle
--ValueTiers

--Tests 4.1 Cross Referencing Stars
--Fact_Position
--ValueTiers

--Tests 5.1.1 Defect - S1/2

--Tests 5.1.2 Defect - S3/4
exec Test.[CCIA-3708] @TestRunNo

--Tests 5.1.3 Defect - S5


Insert into [test].[tblRuns] (RunType, RunID, [TestID], [State], [Time]) values ('Suite_Position_Repopulated', @TestRunNo, '00', 'End', getdate())

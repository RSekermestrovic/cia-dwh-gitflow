﻿--Test Code
--exec Test.Suite_Balance_Repopulated

Create Procedure Test.Suite_Balance_Repopulated

as

Declare @TestRunNo varchar(23)
Set @TestRunNo = convert (varchar(23),getdate(),126)

Declare @LastBatch int
Set @LastBatch = (Select Max([BatchKey]) From [$(Staging)].[Control].[ETLController])

Select	*
into	#BatchDetails
From	[$(Staging)].[Control].[ETLController]
Where	[BatchKey] = @LastBatch

Declare @BatchFromDate datetime
Set @BatchFromDate = (Select BatchFromDate From #BatchDetails)

Declare @BatchToDate datetime
Set @BatchToDate = (Select @BatchToDate From #BatchDetails)

Insert into [test].[tblRuns] (RunType, RunID, [TestID], [State], [Time]) values ('Suite_Balance_Repopulated', @TestRunNo, '00', 'Start', getdate())

--Tests 1.1 Tables Exist
exec Test.[61TableExists_Star_Balance] @TestRunNo

--Tests 1.2 Fields Exist
exec Test.[62FieldExists_Fact_Balance] @TestRunNo
exec Test.[4FieldExists_Dimension_Wallet] @TestRunNo
exec Test.[6FieldExists_Dimension_Day] @TestRunNo
exec Test.[13FieldExists_Dimension_Balance] @TestRunNo
exec Test.[3FieldExists_Dimension_Account] @TestRunNo
exec test.[115FieldExists_Dimension_AccountStatus] @TestRunNo

--Tests 2.1.1 Timely Update - Slowly Changing
exec Test.[80DataCurrent_Dimension_Wallet] @TestRunNo
exec Test.[43DataCurrent_Dimension_Day] @TestRunNo
exec Test.[72DataCurrent_Dimension_BalanceType] @TestRunNo

--Tests 2.1.2 Timely Update - Daily Changing
exec Test.[87DataCurrent_Dimension_Account] @TestRunNo
exec test.[118DataCurrent_Dimension_AccountStatus] @TestRunNo

--Tests 2.1.3 Timely Update - Constant Changing
exec Test.[88DataCurrent_Fact_Balance] @TestRunNo

--Tests 2.2 Default Records
exec [test].[20DataDefault_Dimension_Wallet] @TestRunNo
exec [test].[23DataDefault_Dimension_Day] @TestRunNo
exec [test].[29DataDefault_Dimension_Balance] @TestRunNo
exec [test].[37DataDefault_Dimension_Account] @TestRunNo
exec [Test].[123DataDefault_Dimension_AccountStatus] @TestRunNo

--Tests 2.3.1 Record Count - Volatile
exec test.[101DataCount_FactBalance] @TestRunNo
--Account Status

--Tests 2.3.2 Record Count - Non-Volatile
exec test.[113DataCount_Dimension_Wallet] @TestRunNo
exec test.[38DataCount_Dimension_Day] @TestRunNo
exec test.[95DataCount_Dimension_Balance] @TestRunNo
exec test.[96DataCount_Dimension_Account] @TestRunNo

--Tests 2.4.1 Record Checksum - Volatile
exec [Test].[109DataAdd_FactBalance] @TestRunNo
--Account Status

--Tests 2.4.2 Record Checksum - Non-Volatile
exec Test.[114DataCheckSum_Dimension_Wallet] @TestRunNo
--balancetype
--Account

--Tests 3.1 Validation of Records
exec test.[112BVA_Fact_Balance]  @TestRunNo, null
exec Test.[138DataValid_Dimension_Wallet] @TestRunNo
exec test.[97BVA_Dimension_Day] @TestRunNo, null
exec [Test].[110BVA_Dimension_BalanceType] @TestRunNo
exec Test.[40BVA_Dimension_Account] @TestRunNo, null
--Account Status

--Tests 3.2 No Duplicates 
exec test.[102DataDuplicates_Fact_Balance] @TestRunNo, null
exec Test.[41DataDuplicates_Dimension_Wallet] @TestRunNo, null
exec test.[59DataDuplicates_Dimension_Day] @TestRunNo
exec test.[52DataDuplicates_Dimension_Balance] @TestRunNo
exec test.[58DataDuplicates_Dimension_Account] @TestRunNo, null
exec test.[131DataDuplicates_Dimension_AccountStatus] @TestRunNo, null

--Tests 3.3 Field Mappings
exec test.[107DataValid_Fact_Balance] @TestRunNo
exec Test.[138DataValid_Dimension_Wallet] @TestRunNo
exec test.[36DataValid_Dimension_Day] @TestRunNo
exec [Test].[110BVA_Dimension_BalanceType] @TestRunNo
exec test.[46DataValid_Dimension_Account] @TestRunNo
--Account Status

--Tests 3.4 Foreign Key Contraints
exec test.[103DataFK_Star_Balance] @TestRunNo, null

--Tests 3.5 Truncated Values
--Fact_Balance
--Wallet
--Day
--Balance Type
--Account
--Account Status


--Test 3.6 Destructive Injection
--Fact_Balance
--Wallet
--Day
--Balance Type
--Account
--Account Status

--Tests 4.1 Cross Referencing Stars
exec test.[104CrossRef_Balance] @TestRunNo
exec test.[106CrossRef_Transaction_Balance] @TestRunNo

--Tests 5.1.1 Defect - S1/2

--Tests 5.1.2 Defect - S3/4

--Tests 5.1.3 Defect - S5
exec Test.[CCIA-4979] @TestRunNo
exec Test.[CCIA-4682] @TestRunNo

Insert into [test].[tblRuns] (RunType, RunID, [TestID], [State], [Time]) values ('Suite_Balance_Repopulated', @TestRunNo, '00', 'End', getdate())

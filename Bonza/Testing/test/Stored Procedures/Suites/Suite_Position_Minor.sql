﻿--Test Code
--exec Test.Suite_Position_Minor

Create Procedure Test.Suite_Position_Minor

as

Declare @TestRunNo varchar(23)
Set @TestRunNo = convert (varchar(23),getdate(),126)

Declare @LastBatch int
Set @LastBatch = (Select Max([BatchKey]) From [$(Staging)].[Control].[ETLController])

Select	*
into	#BatchDetails
From	[$(Staging)].[Control].[ETLController]
Where	[BatchKey] = @LastBatch

Declare @BatchFromDate datetime
Set @BatchFromDate = (Select BatchFromDate From #BatchDetails)

Declare @BatchToDate datetime
Set @BatchToDate = (Select @BatchToDate From #BatchDetails)

Insert into [Test].[tblRuns] (RunType, RunID, [TestID], [State], [Time]) values ('Suite_Position_Minor', @TestRunNo, '00', 'Start', getdate())

--Tests 1.1 Tables Exist
exec Test.[63TableExists_Star_Position] @TestRunNo

--Tests 1.2 Fields Exist
exec test.[64FieldExists_Fact_Position] @TestRunNo
exec test.[15FieldExists_Dimension_Class] @TestRunNo
exec test.[60DataDuplicates_Dimension_Time] @TestRunNo
exec test.[8FieldExists_Dimension_Channel] @TestRunNo
exec test.[18FieldExists_Dimension_BetType] @TestRunNo
exec test.[65FieldExists_Dimension_Activity] @TestRunNo
exec test.[3FieldExists_Dimension_Account] @TestRunNo
exec test.[115FieldExists_Dimension_AccountStatus] @TestRunNo
exec test.[66FieldExists_Dimension_Lifecycle] @TestRunNo
exec Test.[143FieldExists_Dimension_ValueTiers] @TestRunNo

--2.1.2 Timely Update - Daily Changing
exec test.[85DataCurrent_Dimension_Class] @TestRunNo
exec test.[87DataCurrent_Dimension_Account] @TestRunNo
exec test.[118DataCurrent_Dimension_AccountStatus] @TestRunNo

--Tests 2.1.3 Timely Update - Constant
exec Test.[89DataCurrent_Fact_Position] @TestRunNo

--Tests 2.3.1 Record Counts - Volatile
--Fact_Position
--Account Status

--Tests 2.4.1 Record Checksum - Volatile
--fact_posiiton
--Account Status

--4.1 Cross Stars Referencing
--Fact_Position

--5.1.1 Defects Regression - S1/2


Insert into [test].[tblRuns] (RunType, RunID, [TestID], [State], [Time]) values ('Suite_Position_Minor', @TestRunNo, '00', 'End', getdate())

Select		*
into		#Temp
From		[test].[tblResults]
where		RunID = @TestRunNo
			and [Status] != 'Pass'

Declare @Counter int
Set @Counter = (
				Select		count (*)
				From		#Temp
				)
				 
Declare @SubjectLine varchar(1000)
Set @SubjectLine = test.fnGetServer() + ' Suite_Position_Minor - ' + @TestRunNo

IF @Counter = 0
	Begin
		Print 'No Tests Failed'
	End
	Else
	Begin
		DECLARE @xml NVARCHAR(MAX)
		DECLARE @body NVARCHAR(MAX)

		SET @xml = CAST(
						( SELECT	[ID] AS 'td'
									,''
									, RunID AS 'td'
									,''
									, TestID AS 'td'
									,''
									, TestName AS 'td'
									,''
									, Notes AS 'td'
						  FROM		#Temp 
						  ORDER BY TestID 
						  FOR XML PATH('tr'), ELEMENTS 
						  ) AS NVARCHAR(MAX)
						)

		SET @body =	'<html>
						<body>
							<H3>Failed Tests</H3>
							<table border = 1> 
							<tr>
								<th> ID </th>
								<th> RunID </th>
								<th> TestID </th>
								<th> TestName </th>
								<th> Notes </th>
							</tr>'    

		SET @body = @body + @xml +'</table></body></html>'

		EXEC msdb.dbo.sp_send_dbmail
		@profile_name = 'DataWarehouse_Auto_Mails',
		@body = @body,
		@body_format ='HTML',
		@recipients = 'james.foley@williamhill.com.au', 
		@subject = @SubjectLine;
	End



GO

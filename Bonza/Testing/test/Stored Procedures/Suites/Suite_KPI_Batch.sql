﻿--Test Code
--exec Test.Suite_KPI_Batch

CREATE Procedure [Test].[Suite_KPI_Batch]

as

Declare @TestRunNo varchar(23)
Set @TestRunNo = convert (varchar(23),getdate(),126)

Declare @LastBatch int
Set @LastBatch = (Select Max([BatchKey]) From [$(Staging)].[Control].[ETLController] where BatchStatus = 1)

Select	BatchFromDate
		, BatchToDate
		, BatchKey
into	#BatchDetails
From	[$(Staging)].[Control].[ETLController]
Where	[BatchKey] = @LastBatch

Declare @BatchFromDate datetime
Set @BatchFromDate = (Select BatchFromDate From #BatchDetails)

Declare @BatchToDate datetime
Set @BatchToDate = (Select BatchToDate From #BatchDetails)

Declare @BatchKey int
Set @BatchKey = (Select BatchKey From #BatchDetails)

Drop Table #BatchDetails

Insert into [Test].[tblRuns] (RunType, RunID, [TestID], [State], [Time]) values ('Suite_KPI_Batch', @TestRunNo, '00', 'Start', getdate())

--Tests 2.1.3 Timely Update - Constant
EXEC [$(Staging)].[Control].[Sp_Log]	@BatchKey,'Suite_KPI_Batch','Timely Update','Start'
exec Test.[90DataCurrent_Fact_KPI] @TestRunNo

--Tests 2.3.1 Record Counts - Volatile
--Fact_KPI

--Tests 2.4.1 Record Checksum - Volatile
--Fact_KPI


--Tests 3.2 No Duplicates
EXEC [$(Staging)].[Control].[Sp_Log]	@BatchKey,'Suite_KPI_Batch','Duplicates','Start'
exec [Test].[125DataDuplicates_Fact_KPI] @TestRunNo, @BatchFromDate

--Tests 3.4 Foreign Key Constraints 
EXEC [$(Staging)].[Control].[Sp_Log]	@BatchKey,'Suite_KPI_Batch','FK COntraints','Start'
exec Test.[121DataFK_Fact_KPI] @TestRunNo, @BatchFromDate

--Tests 4.1 Cross Star Reference
exec Test.[150CrossRef_KPI] @TestRunNo

EXEC [$(Staging)].[Control].[Sp_Log] @BatchKey,'Suite_KPI_Batch',NULL,'Success'
Insert into [test].[tblRuns] (RunType, RunID, [TestID], [State], [Time]) values ('Suite_KPI_Batch', @TestRunNo, '00', 'End', getdate())

Select		*
into		#Temp
From		[test].[tblResults]
where		RunID = @TestRunNo
			and [Status] != 'Pass'

Declare @Counter int
Set @Counter = (
				Select		count (*)
				From		#Temp
				)
				 
Declare @SubjectLine varchar(1000)
Set @SubjectLine = test.fnGetServer() + ' Suite_KPI_Batch - ' + @TestRunNo

IF @Counter = 0
	Begin
		Print 'No Tests Failed'
	End
	Else
	Begin
		DECLARE @xml NVARCHAR(MAX)
		DECLARE @body NVARCHAR(MAX)

		SET @xml = CAST(
						( SELECT	[ID] AS 'td'
									,''
									, RunID AS 'td'
									,''
									, TestID AS 'td'
									,''
									, TestName AS 'td'
									,''
									, Notes AS 'td'
						  FROM		#Temp 
						  ORDER BY TestID 
						  FOR XML PATH('tr'), ELEMENTS 
						  ) AS NVARCHAR(MAX)
						)

		SET @body =	'<html>
						<body>
							<H3>Failed Tests</H3>
							<table border = 1> 
							<tr>
								<th> ID </th>
								<th> RunID </th>
								<th> TestID </th>
								<th> TestName </th>
								<th> Notes </th>
							</tr>'    

		SET @body = @body + @xml +'</table></body></html>'

		EXEC msdb.dbo.sp_send_dbmail
		@profile_name = 'DataWarehouse_Auto_Mails',
		@body = @body,
		@body_format ='HTML',
		@recipients = 'DW.Service@williamhill.com.au', 
		@subject = @SubjectLine;
	End



GO
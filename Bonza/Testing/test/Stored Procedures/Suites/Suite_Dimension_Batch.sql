﻿--Test Code
--exec Test.Suite_Dimension_Batch

CREATE Procedure [Test].[Suite_Dimension_Batch]

as

Declare @TestRunNo varchar(23)
Set @TestRunNo = convert (varchar(23),getdate(),126)

Declare @LastBatch int
Set @LastBatch = (Select Max([BatchKey]) From [$(Staging)].[Control].[ETLController] where BatchStatus = 1)

Select	BatchFromDate
		, BatchToDate
		, BatchKey
into	#BatchDetails
From	[$(Staging)].[Control].[ETLController]
Where	[BatchKey] = @LastBatch

Declare @BatchFromDate datetime
Set @BatchFromDate = (Select BatchFromDate From #BatchDetails)

Declare @BatchToDate datetime
Set @BatchToDate = (Select BatchToDate From #BatchDetails)

Declare @BatchKey int
Set @BatchKey = (Select BatchKey From #BatchDetails)


Insert into [Test].[tblRuns] (RunType, RunID, [TestID], [State], [Time]) values ('Suite_Dimension_Batch', @TestRunNo, '00', 'Start', getdate())

--Tests 2.1.3 Timely Update - Constant
--Contract

--Tests 2.3.1 Record Counts - Volatile
--Contract
--Account Status

--Tests 2.4.1 Record Checksum - Volatile
--Contract
--Account Status

--Tests 3.2 No Duplicates
EXEC [$(Staging)].[Control].[Sp_Log]	@BatchKey,'Suite_Dimension_Batch','Duplicates','Start'
exec Test.[58DataDuplicates_Dimension_Account] @TestRunNo, @BatchFromDate
exec test.[131DataDuplicates_Dimension_AccountStatus] @TestRunNo, @BatchFromDate
--Activity
exec Test.[52DataDuplicates_Dimension_Balance] @TestRunNo
--BetLeg
--BetStatus
exec test.[57DataDuplicates_Dimension_BetType] @TestRunNo
exec test.[134DataDuplicates_Dimension_Campaign] @TestRunNo, @BatchFromDate
exec test.[48DataDuplicates_Dimension_Channel] @TestRunNo
exec test.[54DataDuplicates_Dimension_Class] @TestRunNo, @BatchFromDate
--Company
exec test.[98DataDuplicates_Dimension_Contract] @TestRunNo, @BatchFromDate
exec Test.[59DataDuplicates_Dimension_Day] @TestRunNo
--DayZone
exec test.[50DataDuplicates_Dimension_Instrument] @TestRunNo, @BatchFromDate
exec test.[133DataDuplicates_Dimension_LegType] @TestRunNo, @BatchFromDate
--LifeCycle
exec test.[56DataDuplicates_Dimension_Market] @TestRunNo, @BatchFromDate
exec test.[51DataDuplicates_Dimension_Note] @TestRunNo, @BatchFromDate
--PriceType
exec test.[55DataDuplicates_Dimension_Price] @TestRunNo
exec test.[130DataDuplicates_Dimension_Promotion] @TestRunNo, @BatchFromDate
exec test.[60DataDuplicates_Dimension_Time] @TestRunNo
exec test.[53DataDuplicates_Dimension_TransactionDetail] @TestRunNo
exec test.[49DataDuplicates_Dimension_User] @TestRunNo
exec Test.[41DataDuplicates_Dimension_Wallet] @TestRunNo, @BatchFromDate

--Tests 3.4 Foreign Key Constraints 
EXEC [$(Staging)].[Control].[Sp_Log]	@BatchKey,'Suite_Dimension_Batch','FK Contraints','Start'
exec test.[120DataFK_Dimension_Account] @TestRunNo


EXEC [$(Staging)].[Control].[Sp_Log] @BatchKey,'Suite_Dimension_Batch',NULL,'Success'

Insert into [test].[tblRuns] (RunType, RunID, [TestID], [State], [Time]) values ('Suite_Dimension_Batch', @TestRunNo, '00', 'End', getdate())

Select		*
into		#Temp
From		[test].[tblResults]
where		RunID = @TestRunNo
			and [Status] != 'Pass'

Declare @Counter int
Set @Counter = (
				Select		count (*)
				From		#Temp
				)
				 
Declare @SubjectLine varchar(1000)
Set @SubjectLine = test.fnGetServer() + ' Suite_Dimension_Batch - ' + @TestRunNo

IF @Counter = 0
	Begin
		Print 'No Tests Failed'
	End
	Else
	Begin
		DECLARE @xml NVARCHAR(MAX)
		DECLARE @body NVARCHAR(MAX)

		SET @xml = CAST(
						( SELECT	[ID] AS 'td'
									,''
									, RunID AS 'td'
									,''
									, TestID AS 'td'
									,''
									, TestName AS 'td'
									,''
									, Notes AS 'td'
						  FROM		#Temp 
						  ORDER BY TestID 
						  FOR XML PATH('tr'), ELEMENTS 
						  ) AS NVARCHAR(MAX)
						)

		SET @body =	'<html>
						<body>
							<H3>Failed Tests</H3>
							<table border = 1> 
							<tr>
								<th> ID </th>
								<th> RunID </th>
								<th> TestID </th>
								<th> TestName </th>
								<th> Notes </th>
							</tr>'    

		SET @body = @body + @xml +'</table></body></html>'

		EXEC msdb.dbo.sp_send_dbmail
		@profile_name = 'DataWarehouse_Auto_Mails',
		@body = @body,
		@body_format ='HTML',
		@recipients = 'DW.Service@williamhill.com.au', 
		@subject = @SubjectLine;
	End



GO
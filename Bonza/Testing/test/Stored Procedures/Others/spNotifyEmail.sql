﻿--Test Code
--exec Test.spNotifyEmail 'Hello World', 'There has been a failure of a step in the SQL Job' 

Create Procedure Test.spNotifyEmail @SubjectLine varchar(1000), @body NVARCHAR(MAX)

as

EXEC msdb.dbo.sp_send_dbmail
@profile_name = 'DataWarehouse_Auto_Mails',
@body = @body,
@body_format ='TEXT',
@recipients = 'BI.Service@williamhill.com.au', 
@subject = @SubjectLine;
﻿--Test
--exec Test.CrossRefHistory
Create Procedure Test.CrossRefHistory

as

Create Table #Previous	(
						VersionNumber varchar(100)
							, TestDate Date
							, [Type] varchar(100)
							, Balance varchar(100)
							, WalletName varchar(100)
							, BrandCode varchar(100)
							, TotalDiscrepancy decimal (18,4)
						)

If (Select Acquisition From [Control].[Acquisition]) = 'Small_Acquisition'
Begin
	Insert into #Previous
	Select		VersionNumber
				, TestDate
				, [Type]
				, Balance
				, WalletName
				, BrandCode
				, 'TotalDiscrepancy' = CASE
					When TotalDiscrepancy >= 0 then TotalDiscrepancy
					else TotalDiscrepancy * -1
					End
	FROM		[Test].[SmallPreviousCrossRef]
End
Else
Begin
	Insert into #Previous
	Select		VersionNumber
				, TestDate
				, [Type]
				, Balance
				, WalletName
				, BrandCode
				, 'TotalDiscrepancy' = CASE
					When TotalDiscrepancy >= 0 then TotalDiscrepancy
					else TotalDiscrepancy * -1
					End
	FROM		[Test].[PreviousCrossRef]
End

Select		*
Into		#Data
From		(Select		VersionNumber
						, [Type]
						, TestDate
						, Balance
						, WalletName
						, BrandCode
						, Sum(TotalDiscrepancy) as TotalDiscrepancy
						, Count (TotalDiscrepancy) as AccountsInError
			From		(
						Select		VersionNumber
									, TestDate
									, [Type]
									, Balance
									, WalletName
									, BrandCode
									, 'TotalDiscrepancy' = CASE
										When TotalDiscrepancy >= 0 then TotalDiscrepancy
										else TotalDiscrepancy * -1
										End
						FROM		#Previous
						) as Data
			Group by	VersionNumber
						, [Type]
						, TestDate
						, Balance
						, WalletName
						, BrandCode
			Union
			Select		'Current' as VersionNumber
						, [Type]
						, TestDate
						, Balance
						, WalletName
						, BrandCode
						, Sum(TotalDiscrepancy) as TotalDiscrepancy
						, Count (TotalDiscrepancy) as AccountsInError
			From		(
						Select		TestDate
									, [Type]
									, Balance
									, WalletName
									, BrandCode
									, 'TotalDiscrepancy' = CASE
										When TotalDiscrepancy >= 0 then TotalDiscrepancy
										else TotalDiscrepancy * -1
										End
						FROM		[Test].[CurrentCrossRef]
						) as Data
			Group by	[Type]
						, TestDate
						, Balance
						, WalletName
						, BrandCode
			) as Data

Select		distinct VersionNumber
Into		#Versions
From		#Data

Select		distinct [Type]
Into		#Type
From		#Data

Select		distinct Balance
Into		#Balance
From		#Data

Select		distinct TestDate
Into		#TestDate
From		#Data

Select		distinct BrandCode
Into		#BrandCode
From		#Data

Select		#Versions.VersionNumber
			, #Type.[Type]
			, #TestDate.TestDate
			, #Balance.Balance
			, #BrandCode.BrandCode
			, Coalesce (#Data.TotalDiscrepancy, 0.00) as TotalDiscrepancy
			, Coalesce (#Data.AccountsInError, 0.00) as AccountsInError
Into		#FullData
From		#Versions
			Cross Join #Type
			Cross Join #TestDate
			Cross Join #Balance
			Cross Join #BrandCode
			left join #Data on #Versions.VersionNumber = #Data.VersionNumber
								and #Type.[Type] = #Data.[Type]
								and #TestDate.TestDate = #Data.TestDate
								and #Balance.Balance = #Data.Balance
								and #BrandCode.BrandCode = #Data.BrandCode
Order by	#Versions.VersionNumber
			, #Type.[Type]
			, #TestDate.TestDate
			, #Balance.Balance
			, #BrandCode.BrandCode

Delete from #FullData where VersionNumber in ('15.08', '15.09') and [Type] = 'KPI/Position' -- these are pre the KPI tests being added

Select		#FullData.[Type]
			, #FullData.TestDate
			, #FullData.Balance
			, #FullData.BrandCode
			, Data8.TotalDiscrepancy as [15.08_TotalDiscrepancy]
			, Data8.AccountsInError as [15.08_AccountsInError]
			, Data9.TotalDiscrepancy as [15.09_TotalDiscrepancy]
			, Data9.AccountsInError as [15.09_AccountsInError]
			, #FullData.TotalDiscrepancy as CurrentTotalDiscrepancy
			, #FullData.AccountsInError as CurrentAccountsInError
			, 'TotalDiscrepancy_Change' = CASE
				When #FullData.TotalDiscrepancy < Data9.TotalDiscrepancy then 'Better'
				When #FullData.TotalDiscrepancy > Data9.TotalDiscrepancy then 'Worse'
				When #FullData.TotalDiscrepancy = Data9.TotalDiscrepancy then 'Same'
				Else 'Unknown'
				End
			, 'AccountsInError_Change' = CASE
				When #FullData.AccountsInError < Data9.AccountsInError then 'Better'
				When #FullData.AccountsInError > Data9.AccountsInError then 'Worse'
				When #FullData.AccountsInError = Data9.AccountsInError then 'Same'
				Else 'Unknown'
				End
From		#FullData
			left join	(Select		*
						 From		#FullData
						 Where		VersionNumber = '15.08'
						 ) as Data8 on #FullData.[Type] = Data8.[Type]
										and #FullData.TestDate = Data8.TestDate
										and #FullData.Balance = Data8.Balance
										and #FullData.BrandCode = Data8.BrandCode
			left join	(Select		*
						 From		#FullData
						 Where		VersionNumber = '15.09'
						 ) as Data9 on #FullData.[Type] = Data9.[Type]
										and #FullData.TestDate = Data9.TestDate
										and #FullData.Balance = Data9.Balance
										and #FullData.BrandCode = Data9.BrandCode
Where		#FullData.VersionNumber = 'Current'
			and (
					(#FullData.[Type] = 'KPI/Position' and #FullData.Balance = 'Client')
					OR (#FullData.[Type] = 'Transaction/Balance' and #FullData.Balance = 'Client')
					OR (#FullData.[Type] = 'Transaction/Position')
				)
			and (Coalesce (Data8.TotalDiscrepancy, 0) 
				 + Coalesce (Data8.AccountsInError, 0) 
				 + Coalesce (Data9.TotalDiscrepancy, 0) 
				 + Coalesce (Data9.AccountsInError, 0) 
				 + Coalesce (#FullData.TotalDiscrepancy, 0) 
				 + Coalesce (#FullData.AccountsInError, 0)
				 ) != 0
Order by	#FullData.[Type]
			, #FullData.TestDate
			, #FullData.Balance
			, #FullData.BrandCode
﻿--Test Code
--exec spPerformanceOfLoad 'AdHoc', '999', 348, 'Atomic'

Create Procedure Test.spPerformanceOfLoad @TestRunNo varchar(23), @TestID varchar(10), @BatchKey int, @Table varchar(1000)

as

Create Table #Temp (
					BatchKey varchar(100)
					, StartDate datetime2
					, EndDate datetime2
					, RowsProcessed int
					)

Insert into #Temp
Exec ('
		select		BatchKey
					, '+@Table+'StartDate as StartDate
					, '+@Table+'EndDate as EndDate
					, '+@Table+'RowsProcessed as RowsProcessed
		From		[$(Staging)].[Control].[ETLController]
		Where		'+@Table+'Status = 1
	')


Select		#Temp.BatchKey
			, DateDiff(SECOND,#Temp.StartDate,#Temp.EndDate) as Sec_Duration
			, #Temp.RowsProcessed
			, #Temp.RowsProcessed/(DateDiff(SECOND,#Temp.StartDate,#Temp.EndDate)) as RowsPerSecond
			, Average.Sec_Duration as Ave_Sec_Duration
			, Average.RowsProcessed as Ave_RowsProcessed
			, Average.RowsPerSecond as Ave_RowsPerSecond
			, (Average.Sec_Duration/100)*110 as Ave_Sec_Duration110
			, (Average.RowsProcessed/100)*110 as Ave_RowsProcessed110
			, (Average.RowsPerSecond/100)*90 as Ave_RowsPerSecond90
Into		#ProcessedData
From		#Temp
			cross join  (Select		Avg(Sec_Duration) as Sec_Duration
									, Avg (RowsProcessed) as RowsProcessed
									, Avg (RowsPerSecond) as RowsPerSecond
							From		(
									Select		BatchKey
												, DateDiff(SECOND,StartDate,EndDate) as Sec_Duration
												, RowsProcessed
												, RowsProcessed/(DateDiff(SECOND,StartDate,EndDate)) as RowsPerSecond
										From		#Temp
										where		StartDate != '1900-01-01 00:00:00.000'
												and EndDate != '1900-01-01 00:00:00.000'
									) as Data
						) as Average
where		BatchKey = @BatchKey

If		(Select		'Fail' = CASE
						When RowsPerSecond < Ave_RowsPerSecond90 then 1
						Else 0
						End
		 From		#ProcessedData
		 ) = 1
Begin
	Insert into test.tblResults ([RunID], [TestID], [TestName], [Status], [Notes]) Values (@TestRunNo, @TestID, 'PerformanceOfLoad_' + @Table, 'Fail', 'Rows Processed per Second is less than 90% of average')
End
else
Begin
	Insert into test.tblResults ([RunID], [TestID], [TestName], [Status], [Notes]) Values (@TestRunNo, @TestID, 'PerformanceOfLoad_' + @Table, 'Pass', Null)
End
﻿--Test Code
--exec Test.spSelectValid 'ADHOC', '2','Structure - Transaction Table', 'select * from test.tblresults'
--exec Test.spSelectValid 'ADHOC', '2','Structure - Transaction Table', 'select * from test.tblresultsxxx'

Create Procedure Test.spSelectValid (@TestRunNo varchar(23), @ID varchar(100), @Name varchar(1000), @Select varchar(max))

as

Begin Try 
	Execute (@Select)
	Insert into Test.tblResults Values (@TestRunNo, @ID, @Name, 'Pass', Null)
End Try

Begin Catch
	Insert into Test.tblResults Values (@TestRunNo, @ID, @Name, 'Fail', ERROR_MESSAGE())
End Catch





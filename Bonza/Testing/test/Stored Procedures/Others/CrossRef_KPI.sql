﻿--Test Code
--exec Test.[CrossRef_KPI] 'AdHoc'
Create Procedure [Test].[CrossRef_KPI] @TestRunNo varchar(23), @FromDate datetime2

as

select	*,
		-- Given this procedure enforces a single day window, the following values are direct derivations and do not require the ovewrhead of the windowed functions below
		NetClosingBalance - NextSourceOpeningBalance DayClosingDiscrepancy,
		NetClosingBalance - NextSourceOpeningBalance OngoingClosingDiscrepancy,
		NetClosingBalance - NextSourceOpeningBalance TotalDiscrepancy
FROM	(	SELECT	DayDate, 
					BrandCode,
					AccountID,
					LedgerID,
					SourceOpeningBalance,
					TransactedAmount, 
					TransactionCount,
					SourceOpeningBalance + TransactedAmount NetClosingBalance,
					LEAD(SourceOpeningBalance) OVER (PARTITION BY BrandCode, LedgerID ORDER BY DayDate) NextSourceOpeningBalance--,
--					SourceOpeningBalance + TransactedAmount - LEAD(SourceOpeningBalance,1,0) OVER (PARTITION BY BrandCode, LedgerID ORDER BY DayDate) DayClosingDiscrepancy,
--					FIRST_VALUE(SourceOpeningBalance) OVER (PARTITION BY BrandCode, LedgerID ORDER BY DayDate) + SUM(TransactedAmount) OVER (PARTITION BY BrandCode, LedgerID ORDER BY DayDate RANGE BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) - LEAD(SourceOpeningBalance,1,0) OVER (PARTITION BY BrandCode, LedgerID ORDER BY DayDate) OngoingClosingDiscrepancy,
--					FIRST_VALUE(SourceOpeningBalance) OVER (PARTITION BY BrandCode, LedgerID ORDER BY DayDate) + SUM(TransactedAmount) OVER (PARTITION BY BrandCode, LedgerID) - FIRST_VALUE(SourceOpeningBalance) OVER (PARTITION BY BrandCode, LedgerID ORDER BY DayDate DESC) TotalDiscrepancy
			FROM	(	SELECT BrandCode,
								LedgerID, 
								AccountID,
								DayDate,
								ISNULL(SUM(SourceOpeningBalance) ,0) SourceOpeningBalance, 
								ISNULL(SUM(TransactedAmount),0) TransactedAmount,
								SUM(CASE WHEN TransactedAmount <> 0 THEN 1 ELSE 0 END) TransactionCount
						FROM	(	select	CASE WHEN BrandCode = 'Unk' AND a.LedgerID NOT IN (777768,779522,783893,817235,825388,1187572,1250958) THEN 'RWWA' ELSE BrandCode END BrandCode, 
											a.LedgerID,
											AccountID, 
											DayDate,
											NULL as SourceOpeningBalance, 
											DepositsCompleted + Transfers + Stakes + Winnings + WithdrawalsRequested + BonusWinnings + Promotions + Adjustments as TransactedAmount
									from   [$(Dimensional)].Dimension.day d with (nolock)
											inner join [$(Dimensional)].Dimension.dayzone z with (nolock) on (z.MasterDayKey=d.DayKey)
											inner join [$(Dimensional)].Fact.KPI t with (nolock) on (t.DayKey=z.DayKey)
											inner join [$(Dimensional)].Dimension.account a with (nolock) on (a.AccountKey = t.AccountKey)
											inner join [$(Dimensional)].Dimension.accountStatus as accountStatus with (nolock) on (t.AccountstatusKey = accountStatus.AccountstatusKey)
											inner join [$(Dimensional)].Dimension.company c with (nolock) on (c.CompanyKey = a.CompanyKey)
											--inner join [$(Dimensional)].Dimension.Wallet w with (nolock) on (t.WalletKey = w.WalletKey)
									where DayDate BETWEEN @FromDate AND @FromDate
											and a.LedgerType NOT IN ('Write Offs','Vendor Splits','Vendor W.Offs/Other','Vendor Arrangements','Vendor 3rd Party Collection','Test','Betback') -- Added Test
											and accountStatus.AccountType = 'Client'
											--and w.WalletName = 'Cash'
									UNION ALL
									select	CASE WHEN BrandCode = 'Unk' AND a.LedgerID NOT IN (777768,779522,783893,817235,825388,1187572,1250958) THEN 'RWWA' ELSE BrandCode END BrandCode, 
											a.LedgerID,
											AccountID, 
											DayDate,
											OpeningBalance, 
											NULL TransactedAmount
									from   [$(Dimensional)].Dimension.day d with (nolock)
											inner join [$(Dimensional)].Dimension.dayzone z with (nolock) on (z.MasterDayKey=d.DayKey)
											inner join [$(Dimensional)].Fact.[Position] t with (nolock) on (t.DayKey=z.DayKey)
											inner join [$(Dimensional)].Dimension.BalanceType b with (nolock) on (b.BalanceTypeKey = t.BalanceTypeKey)
											inner join [$(Dimensional)].Dimension.account a with (nolock) on (a.AccountKey = t.AccountKey)
											inner join [$(Dimensional)].Dimension.accountStatus as accountStatus with (nolock) on (t.AccountstatusKey = accountStatus.AccountstatusKey)
											inner join [$(Dimensional)].Dimension.company c with (nolock) on (c.CompanyKey = a.CompanyKey)
									where DayDate BETWEEN @FromDate AND DateAdd(Day,1,@FromDate)
											and BalanceTypeName IN ('Client')--,'Hold')
											and a.LedgerType NOT IN ('Write Offs','Vendor Splits','Vendor W.Offs/Other','Vendor Arrangements','Vendor 3rd Party Collection','Test','Betback') -- Added Test
											and accountStatus.AccountType = 'Client'
						) z					
					group by BrandCode, DayDate, LedgerID, AccountID
				) y
		) x
WHERE NextSourceOpeningBalance <> NetClosingBalance	
-- and (DayClosingDiscrepancy <> 0 )
-- and TotalDiscrepancy <> 0
order by DayDate, BrandCode, AccountID
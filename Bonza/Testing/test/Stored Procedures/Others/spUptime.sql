﻿--exec Test.spUptime
Create Procedure Test.spUptime

as

Declare @Last int
Set @Last = 	(Select		Max (ID) 
					From		Test.Uptime
				)

Select	ID
		, FromDate
		, ToDate
		, [Status]
into	#Uptime
From	Test.Uptime
Where	ID = @Last

Declare @FromDate datetime2
Set @FromDate =  (Select FromDate From #Uptime)

Declare @ToDate datetime2
Set @ToDate =  (Select ToDate From #Uptime)

Declare @Status int
Set @Status =  (Select [Status] From #Uptime)


Begin Try
	select		top 1 WalletKey 
	From		[$(Dimensional)].Dimension.Wallet with (nolock)

	--there are no records in the table
	If @Status is null
		Begin
			Insert into Test.Uptime (FromDate, ToDate, [Status]) Values (getdate(), getdate(), 1)
		End

	--the last records is more than 2 minutes ago, so there has been downtime
	If @ToDate < dateadd(minute,-2,getdate())
		Begin
			Insert into Test.Uptime (FromDate, ToDate, [Status]) Values (@ToDate, getdate(), 0)
			Insert into Test.Uptime (FromDate, ToDate, [Status]) Values (getdate(), getdate(), 1)
		End
	else
	Begin
		update Test.Uptime set ToDate = getdate() where id = @Last
	End
End Try

Begin Catch
	If @Status = 0
	Begin
		update Test.Uptime set ToDate = getdate() where id = @Last
	End
	else
	Begin
		Insert into Test.Uptime (FromDate, ToDate, [Status]) Values (Convert(date,getdate()), Left (Convert(date,getdate()),5), 0)
	End
End Catch
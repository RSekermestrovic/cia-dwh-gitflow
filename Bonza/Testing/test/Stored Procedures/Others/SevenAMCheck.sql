﻿--test
--exec test.SevenAMCheck
Create procedure test.SevenAMCheck

as

Declare @SubjectLine varchar(1000)
Declare @body NVARCHAR(MAX)
Declare @Complete datetime2
Declare @Last datetime2 
Declare @Server varchar(100)

Set @Complete = (select max(batchtodate) from [$(Staging)].control.ETLController where BatchStatus = 1)

Set @Last = (select max(batchtodate) from [$(Staging)].control.ETLController)

Set @Server = (select [Test].[fnGetServer] ())

If @Complete != @Last
Begin
	Set @SubjectLine = 'FAO - DW Operations: There is a problem with daily batch on' + @Server
	Set @body = 'There is a difference between the latest batch date ' + Convert (varchar(40), @Last, 121) + ' and the last completed batch ' + Convert (varchar(40), @Complete, 121) + ' suggesting that the batch has not yet completed, please investigate!'
	
	Exec [Test].[spAlertEmail] @SubjectLine, @body
End
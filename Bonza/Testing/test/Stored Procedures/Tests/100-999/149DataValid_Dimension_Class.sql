﻿--Test Code
--exec Test.[149DataValid_Dimension_Class] 'ADHOC'
Create Procedure Test.[149DataValid_Dimension_Class] @TestRunNo varchar(23)

as

Declare @FromDate datetime2
Set @FromDate = (Select Max (BatchToDate) From [$(Staging)].Control.ETLController where BatchStatus = 1)

If not exists  (
				select		*  
				from		[$(Acquisition)].IntraBet.tblLUEventTypes as tblLUEventTypes
							left join [$(Dimensional)].[Dimension].[Class] as [Class] on tblLUEventTypes.EventType = [Class].ClassId
				where		tblLUEventTypes.fromdate <= @FromDate
							and tblLUEventTypes.todate > @FromDate
							and [Class].classkey is null
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '149', 'DataValid_Dimension_Class', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '149', 'DataValid_Dimension_Class', 'Fail', 'Not all ClassIDs have been populated')
End

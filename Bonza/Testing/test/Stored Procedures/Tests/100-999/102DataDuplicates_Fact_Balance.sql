﻿--Test Code
--exec Test.[102DataDuplicates_Fact_Balance] 'ADHOC', null
--exec Test.[102DataDuplicates_Fact_Balance] 'ADHOC', '2014-07-11'

Create Procedure Test.[102DataDuplicates_Fact_Balance] @TestRunNo varchar(23), @FromDate datetime

as

Declare @FromDate2 date
Declare @DayKey char(8)
Declare @FromDayKey int
Declare @ToDayKey int

If @FromDate is not null
Begin
	Set @FromDate2 = @FromDate
	Set @DayKey = (select DayKey from [$(Dimensional)].Dimension.[Day] with (nolock) where DayDate = @FromDate2)
	Set @FromDayKey = @DayKey + '00'
	Set @ToDayKey = @DayKey + '03'
End

If not exists(
		Select		Count(*)
		From		[$(Dimensional)].Fact.[Balance] as [Balance] with (nolock)
		where		((Balance.DayKey between @FromDayKey and @ToDayKey)
								or (@FromDate is null))	
		Group by	[AccountKey], [DayKey], [BalanceTypeKey], [WalletKey]
		Having		Count(*) > 1
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '102', 'DataDuplicates_Fact_Balance', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '102', 'DataDuplicates_Fact_Balance', 'Fail', 'Duplicates of Balance found')
End


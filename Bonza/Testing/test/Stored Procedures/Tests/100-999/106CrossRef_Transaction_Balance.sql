﻿
--Test Code
--exec Test.[106CrossRef_Transaction_Balance] 'AdHoc'
CREATE Procedure [Test].[106CrossRef_Transaction_Balance] @TestRunNo varchar(23)

as

Declare @FromDate date

Set @FromDate = (Select Max (BatchToDate) from [$(Staging)].[Control].[ETLController] where BatchStatus = 1)
Set @FromDate = DateAdd(Day,-1,@FromDate)

Declare @Counter int

Create Table #Errors (
	[Balance] [varchar](23) NULL,
	[WalletName] [varchar](23) NULL,
	[TestDate] [date] NULL,
	[BrandCode] [varchar](23) NULL,
	[AccountID] [varchar](23) NULL,
	[LedgerID] [varchar](23) NULL,
	[SourceOpeningBalance] [decimal](18, 4) NULL,
	[TransactedAmount] [decimal](18, 4) NULL,
	[TransactionCount] [int] NULL,
	[NetClosingBalance] [decimal](18, 4) NULL,
	[NextSourceOpeningBalance] [decimal](18, 4) NULL,
	[DayClosingDiscrepancy] [decimal](18, 4) NULL,
	[OngoingClosingDiscrepancy] [decimal](18, 4) NULL,
	[TotalDiscrepancy] [decimal](18, 4) NULL
	)

Insert into #Errors
exec Test.[CrossRef_Transaction_Balance] @TestRunNo, @FromDate

	Select		*
				, 'CashFlowStatus' = CASE
										When TotalErrors >= 20 then 'Red'
										When ErrorAmount >= 10000 then 'Red'
										When TotalErrors >= 1 then 'Orange'
										When ErrorAmount >= 0.01 then 'Orange'
										Else 'Green'
										End
	Into		#Summary
	From		(
				Select		@FromDate as TestDate
							, 'Balance' as [Star1]
							, Balance
							, Coalesce (Sum (NumberOfAccounts), 0) as TotalErrors
							, Coalesce (Sum (Total),0.0) as ErrorAmount
				From		(
							Select		Balance
										, 1 as NumberOfAccounts
										, 'Total' = CASE
													When DayClosingDiscrepancy < 0 then DayClosingDiscrepancy * -1
													Else DayClosingDiscrepancy
													End
							From		#Errors
							where		BrandCode != 'RWWA'
							) as Data
				Group By	Balance
				) as GroupedData

	Begin Transaction
		Delete from test.tblCrossRef where TestDate = @FromDate and [Type] = 'Transaction/Balance'
		Insert Into	test.tblCrossRef
		Select		@TestRunNo as ID
					, @FromDate as TestDate
					, 'Transaction/Balance' as [Type]
					, Balance
					, WalletName
					, BrandCode
					, AccountID
					, LedgerID
					, SourceOpeningBalance
					, TransactedAmount
					, TransactionCount
					, NetClosingBalance
					, NextSourceOpeningBalance
					, DayClosingDiscrepancy
					, OngoingClosingDiscrepancy
					, TotalDiscrepancy
		From		#Errors
	If @@ERROR != 0
		Begin
			Rollback Transaction
		End
	Else
		Begin
			Commit Transaction
		End
	
	Begin Transaction
		Delete from [Test].[tblCashFlowStatus] where TestDate = @FromDate and Facts = 'Transaction/Balance'
		Insert into [Test].[tblCashFlowStatus] ([TestDate] ,[Balance],[Status], Facts) 
		Select		@FromDate as TestDate
					, Balance.BalanceTypeName
					, Coalesce (CashFlowStatus, 'Green') as CashFlowStatus
					, 'Transaction/Balance' as Facts
		From		[$(Dimensional)].Dimension.BalanceType as Balance
					left join #Summary on balance.BalanceTypeName = #Summary.Balance
		If @@ERROR != 0
		Begin
			Rollback Transaction
		End
		Else
		Begin
			Commit Transaction
		End

	If (Select Count(*) From test.tblCrossRef where TestDate = @FromDate and [Type] = 'Transaction/Balance') = 0
	Begin
		Insert into test.tblResults ([RunID], [TestID], [TestName], [Status], [Notes]) Values (@TestRunNo, '106', 'CrossRef_Transaction_Balance', 'Pass', Null)
	End
	Else
	Begin
		Insert into test.tblResults ([RunID], [TestID], [TestName], [Status], [Notes]) Values (@TestRunNo, '106', 'CrossRef_Transaction_Balance', 'Fail', 'There is a mismatch between Master_Transaction/Balance, please use http://businessintelligence/Reports/Pages/Report.aspx?ItemPath=%2fSystem+Monitoring%2fCross+Reference+Checks for investigation')
	End


--Test on AnalysisDate
Truncate table #Errors

Insert into #Errors
exec Test.[CrossRefA_Transaction_Balance] @TestRunNo, @FromDate

Begin Transaction
		Delete from test.tblCrossRef where TestDate = @FromDate and [Type] = 'A_Transaction/Balance'
		Insert Into	test.tblCrossRef
		Select		@TestRunNo as ID
					, @FromDate as TestDate
					, 'A_Transaction/Balance' as [Type]
					, Balance
					, WalletName
					, BrandCode
					, AccountID
					, LedgerID
					, SourceOpeningBalance
					, TransactedAmount
					, TransactionCount
					, NetClosingBalance
					, NextSourceOpeningBalance
					, DayClosingDiscrepancy
					, OngoingClosingDiscrepancy
					, TotalDiscrepancy
		From		#Errors
	If @@ERROR != 0
		Begin
			Rollback Transaction
		End
	Else
		Begin
			Commit Transaction
		End

	If (Select Count(*) From test.tblCrossRef where TestDate = @FromDate and [Type] = 'A_Transaction/Balance') = 0
	Begin
		Insert into test.tblResults ([RunID], [TestID], [TestName], [Status], [Notes]) Values (@TestRunNo, '106.1', 'CrossRef_Transaction_Balance AnalysisDate', 'Pass', Null)
	End
	Else
	Begin
		Insert into test.tblResults ([RunID], [TestID], [TestName], [Status], [Notes]) Values (@TestRunNo, '106.1', 'CrossRef_Transaction_Balance AnalysisDate', 'Fail', 'There is a mismatch between Analysis_Transaction/Balance,please use http://businessintelligence/Reports/Pages/Report.aspx?ItemPath=%2fSystem+Monitoring%2fCross+Reference+Checks for investigation')
	End
--Test Code
--exec Test.[301FieldExists_Fact_Contact] 'ADHOC'
Create Procedure Test.[301FieldExists_Fact_Contact] @TestRunNo varchar(23)

as

Declare @Database varchar(100)
Set @Database = '[$(Dimensional)]'

Declare @SQL varchar(max)
Set @SQL = 'Select top 1 
			   [AccountKey]
			  ,[AccountStatusKey]
			  ,[CommunicationId]
			  ,[CommunicationKey]
			  ,[StoryKey]
			  ,[ChannelKey]
			  ,[PromotionKey]
			  ,[BetTypeKey]
			  ,[ClassKey]
			  ,[CommunicationCompanyKey]
			  ,[EventKey]
			  ,[InteractionTypeKey]
			  ,[InteractionDayKey]
			  ,[InteractionTimeKey]
			  ,[Details]
			  ,[CreatedDate]
			  ,[CreatedBatchKey]
			  --,[CreatedBy] CCIA-6646
			  ,[ModifiedDate]
			  ,[ModifiedBatchKey]
			  --, ModifiedBy CCIA-6646
			from ' + @Database + '.[Fact].[Contact]'

Exec Test.spSelectValid @TestRunNo, '301','Structure - Fact Contact Table', @SQL

If		(
		Select		Count(*)
		From		[$(Dimensional)].sys.all_objects as [Objects]
					inner join [$(Dimensional)].sys.all_columns as [Columns] on [Objects].[object_id] = [Columns].[object_id]
					inner join [$(Dimensional)].sys.schemas as [schemas] on [Objects].[schema_id] = [schemas].[schema_id]
		where		[Objects].name = 'Contact'
					and [Objects].[type] = 'U'
					and [schemas].Name = 'Fact'
		) = 19
Begin
	Insert into test.tblResults Values (@TestRunNo, '301.1', 'Structure - Fact Contact Table', 'Pass', Null)
End
Else
Begin
	--CCIA-6646 missing ModifiedBy and CreatedBy Columns
	Insert into test.tblResults Values (@TestRunNo, '301.1', 'Structure - Fact Transaction Table', 'Fail', 'Wrong number of columns')
End



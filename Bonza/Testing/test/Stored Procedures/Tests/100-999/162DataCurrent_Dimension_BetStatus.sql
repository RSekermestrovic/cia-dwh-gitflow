﻿--Test Code
--exec Test.[162DataCurrent_Dimension_BetStatus] 'ADHOC'
Create Procedure Test.[162DataCurrent_Dimension_BetStatus] @TestRunNo varchar(23)

as

Select	*
into #Dimension
From [$(Dimensional)].[Dimension].[BetStatus]  

Declare @NumberOfRecords int
Set @NumberOfRecords = 
					(
					Select		count(*) as NumberOfRecords
					From		#Dimension
					)
If @NumberOfRecords = 9
Begin
	Insert into test.tblResults Values (@TestRunNo, '162', 'DataCurrent_Dimension_BetStatus Record Count', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '162', 'DataCurrent_Dimension_BetStatus Record Count', 'Fail', 'Expected 9 records, ' + Cast (@NumberOfRecords as varchar(5)) + ' found.')
End

IF 					(
					Select		Min (StatusKey) as Record
					From		#Dimension
					where		StatusKey not in (0 ,-1)
					)
					= 1
Begin
	Insert into test.tblResults Values (@TestRunNo, '162.1', 'DataCurrent_Dimension_BetStatus Record Count', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '162.1', 'DataCurrent_Dimension_BetStatus Record Count', 'Fail', 'The lowest record key is not 1')
End

IF 					(
					Select		Max (StatusKey) as Record
					From		#Dimension
					where		StatusKey not in (0 ,-1)
					)
					= 7
Begin
	Insert into test.tblResults Values (@TestRunNo, '162.2', 'DataCurrent_Dimension_BetStatus Record Count', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '162.2', 'DataCurrent_Dimension_BetStatus Record Count', 'Fail', 'The highest record key is not 7')
End


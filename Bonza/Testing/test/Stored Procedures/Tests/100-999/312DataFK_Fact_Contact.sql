﻿--Test Code
--exec Test.[312DataFK_Fact_Contact] 'ADHOC', null

Create Procedure Test.[312DataFK_Fact_Contact] @TestRunNo varchar(23), @FromDate datetime

as

--AccountKey
If not exists(
				Select		Contact.AccountKey
				From		[$(Dimensional)].Fact.Contact as Contact
							left join [$(Dimensional)].Dimension.Account as Account on Contact.AccountKey = Account.AccountKey
				Where		Account.AccountKey is null
							and (
								(Contact.CreatedDate >= @FromDate)
								or (@FromDate is null)
								)				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.0', 'DataFK_Fact_Contact AccountKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.0', 'DataFK_Fact_Contact AccountKey', 'Fail', 'Missing AccountKey')
End

If not exists(
				Select		Contact.AccountKey
				From		[$(Dimensional)].Fact.Contact as Contact
				Where		Contact.AccountKey = -1
							and (
								(Contact.CreatedDate >= @FromDate)
								or (@FromDate is null)
								)				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.1', 'DataFK_Fact_Contact AccountKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.1', 'DataFK_Fact_Contact AccountKey', 'Fail', 'Missing AccountKey')
End

--AccountStatusKey
If not exists(
				Select		Contact.AccountStatusKey
				From		[$(Dimensional)].Fact.Contact as Contact
							left join [$(Dimensional)].Dimension.AccountStatus as AccountStatus on Contact.AccountStatusKey = AccountStatus.AccountStatusKey
				Where		AccountStatus.AccountStatusKey is null
							and (
								(Contact.CreatedDate >= @FromDate)
								or (@FromDate is null)
								)				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.2', 'DataFK_Fact_Contact AccountStatusKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.2', 'DataFK_Fact_Contact AccountStatusKey', 'Fail', 'Missing AccountStatusKey')
End

If not exists(
				Select		Contact.AccountStatusKey
				From		[$(Dimensional)].Fact.Contact as Contact
				Where		Contact.AccountStatusKey = -1
							and (
								(Contact.CreatedDate >= @FromDate)
								or (@FromDate is null)
								)				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.3', 'DataFK_Fact_Contact AccountStatusKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.3', 'DataFK_Fact_Contact AccountStatusKey', 'Fail', 'Missing AccountStatusKey')
End

--CommunicationId n/a
--CommunicationKey
If not exists(
				Select		Contact.CommunicationKey
				From		[$(Dimensional)].Fact.Contact as Contact
							left join [$(Dimensional)].Dimension.Communication as Communication on Contact.CommunicationKey = Communication.CommunicationKey
				Where		Communication.CommunicationKey is null
							and (
								(Contact.CreatedDate >= @FromDate)
								or (@FromDate is null)
								)				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.4', 'DataFK_Fact_Contact CommunicationKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.4', 'DataFK_Fact_Contact CommunicationKey', 'Fail', 'Missing CommunicationKey')
End

If not exists(
				Select		Contact.CommunicationKey
				From		[$(Dimensional)].Fact.Contact as Contact
				Where		Contact.CommunicationKey = -1
							and (
								(Contact.CreatedDate >= @FromDate)
								or (@FromDate is null)
								)				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.5', 'DataFK_Fact_Contact CommunicationKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.5', 'DataFK_Fact_Contact CommunicationKey', 'Fail', 'Missing CommunicationKey')
End

--StoryKey
If not exists(
				Select		Contact.StoryKey
				From		[$(Dimensional)].Fact.Contact as Contact
							left join [$(Dimensional)].Dimension.Story as Story on Contact.StoryKey = Story.StoryKey
				Where		Story.StoryKey is null
							and (
								(Contact.CreatedDate >= @FromDate)
								or (@FromDate is null)
								)				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.6', 'DataFK_Fact_Contact StoryKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.6', 'DataFK_Fact_Contact StoryKey', 'Fail', 'Missing StoryKey')
End

If not exists(
				Select		Contact.StoryKey
				From		[$(Dimensional)].Fact.Contact as Contact
				Where		Contact.StoryKey = -1
							and (
								(Contact.CreatedDate >= @FromDate)
								or (@FromDate is null)
								)				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.7', 'DataFK_Fact_Contact StoryKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.7', 'DataFK_Fact_Contact StoryKey', 'Fail', 'Missing StoryKey')
End

--ChannelKey
If not exists(
				Select		Contact.ChannelKey
				From		[$(Dimensional)].Fact.Contact as Contact
							left join [$(Dimensional)].Dimension.Channel as Channel on Contact.ChannelKey = Channel.ChannelKey
				Where		Channel.ChannelKey is null
							and (
								(Contact.CreatedDate >= @FromDate)
								or (@FromDate is null)
								)				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.8', 'DataFK_Fact_Contact ChannelKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.8', 'DataFK_Fact_Contact ChannelKey', 'Fail', 'Missing ChannelKey')
End

If not exists(
				Select		Contact.ChannelKey
				From		[$(Dimensional)].Fact.Contact as Contact
				Where		Contact.ChannelKey = -1
							and (
								(Contact.CreatedDate >= @FromDate)
								or (@FromDate is null)
								)				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.9', 'DataFK_Fact_Contact ChannelKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.9', 'DataFK_Fact_Contact ChannelKey', 'Fail', 'Missing ChannelKey')
End

--PromotionKey
If not exists(
				Select		Contact.PromotionKey
				From		[$(Dimensional)].Fact.Contact as Contact
							left join [$(Dimensional)].Dimension.Promotion as Promotion on Contact.PromotionKey = Promotion.PromotionKey
				Where		Promotion.PromotionKey is null
							and (
								(Contact.CreatedDate >= @FromDate)
								or (@FromDate is null)
								)				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.10', 'DataFK_Fact_Contact PromotionKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.10', 'DataFK_Fact_Contact PromotionKey', 'Fail', 'Missing PromotionKey')
End

If not exists(
				Select		Contact.PromotionKey
				From		[$(Dimensional)].Fact.Contact as Contact
				Where		Contact.PromotionKey = -1
							and (
								(Contact.CreatedDate >= @FromDate)
								or (@FromDate is null)
								)				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.11', 'DataFK_Fact_Contact PromotionKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.11', 'DataFK_Fact_Contact PromotionKey', 'Fail', 'Missing PromotionKey')
End

--BetTypeKey
If not exists(
				Select		Contact.BetTypeKey
				From		[$(Dimensional)].Fact.Contact as Contact
							left join [$(Dimensional)].Dimension.BetType as BetType on Contact.BetTypeKey = BetType.BetTypeKey
				Where		BetType.BetTypeKey is null
							and (
								(Contact.CreatedDate >= @FromDate)
								or (@FromDate is null)
								)				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.12', 'DataFK_Fact_Contact BetTypeKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.12', 'DataFK_Fact_Contact BetTypeKey', 'Fail', 'Missing BetTypeKey')
End

If not exists(
				Select		Contact.BetTypeKey
				From		[$(Dimensional)].Fact.Contact as Contact
				Where		Contact.BetTypeKey = -1
							and (
								(Contact.CreatedDate >= @FromDate)
								or (@FromDate is null)
								)				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.13', 'DataFK_Fact_Contact BetTypeKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.13', 'DataFK_Fact_Contact BetTypeKey', 'Fail', 'Missing BetTypeKey')
End

--ClassKey
If not exists(
				Select		Contact.ClassKey
				From		[$(Dimensional)].Fact.Contact as Contact
							left join [$(Dimensional)].Dimension.Class as Class on Contact.ClassKey = Class.ClassKey
				Where		Class.ClassKey is null
							and (
								(Contact.CreatedDate >= @FromDate)
								or (@FromDate is null)
								)				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.14', 'DataFK_Fact_Contact ClassKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.14', 'DataFK_Fact_Contact ClassKey', 'Fail', 'Missing ClassKey')
End

If not exists(
				Select		Contact.ClassKey
				From		[$(Dimensional)].Fact.Contact as Contact
				Where		Contact.ClassKey = -1
							and (
								(Contact.CreatedDate >= @FromDate)
								or (@FromDate is null)
								)				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.15', 'DataFK_Fact_Contact ClassKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.15', 'DataFK_Fact_Contact ClassKey', 'Fail', 'Missing ClassKey')
End

--CommunicationCompanyKey
If not exists(
				Select		Contact.CommunicationCompanyKey
				From		[$(Dimensional)].Fact.Contact as Contact
							left join [$(Dimensional)].Dimension.Company as Company on Contact.CommunicationCompanyKey = Company.CompanyKey
				Where		Company.CompanyKey is null
							and (
								(Contact.CreatedDate >= @FromDate)
								or (@FromDate is null)
								)				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.16', 'DataFK_Fact_Contact CommunicationCompanyKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.16', 'DataFK_Fact_Contact CommunicationCompanyKey', 'Fail', 'Missing CommunicationCompanyKey')
End

If not exists(
				Select		Contact.CommunicationCompanyKey
				From		[$(Dimensional)].Fact.Contact as Contact
				Where		Contact.CommunicationCompanyKey = -1
							and (
								(Contact.CreatedDate >= @FromDate)
								or (@FromDate is null)
								)				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.17', 'DataFK_Fact_Contact CommunicationCompanyKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.17', 'DataFK_Fact_Contact CommunicationCompanyKey', 'Fail', 'Missing CommunicationCompanyKey')
End

--EventKey
If not exists(
				Select		Contact.EventKey
				From		[$(Dimensional)].Fact.Contact as Contact
							left join [$(Dimensional)].Dimension.[Event] as [Event] on Contact.EventKey = [Event].EventKey
				Where		[Event].EventKey is null
							and (
								(Contact.CreatedDate >= @FromDate)
								or (@FromDate is null)
								)				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.18', 'DataFK_Fact_Contact EventKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.18', 'DataFK_Fact_Contact EventKey', 'Fail', 'Missing EventKey')
End

If not exists(
				Select		Contact.EventKey
				From		[$(Dimensional)].Fact.Contact as Contact
				Where		Contact.EventKey = -1
							and (
								(Contact.CreatedDate >= @FromDate)
								or (@FromDate is null)
								)				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.19', 'DataFK_Fact_Contact EventKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.19', 'DataFK_Fact_Contact EventKey', 'Fail', 'Missing EventKey')
End

--InteractionTypeKey
If not exists(
				Select		Contact.InteractionTypeKey
				From		[$(Dimensional)].Fact.Contact as Contact
							left join [$(Dimensional)].Dimension.InteractionType as InteractionType on Contact.InteractionTypeKey = InteractionType.InteractionTypeKey
				Where		InteractionType.InteractionTypeKey is null
							and (
								(Contact.CreatedDate >= @FromDate)
								or (@FromDate is null)
								)				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.20', 'DataFK_Fact_Contact InteractionTypeKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.20', 'DataFK_Fact_Contact InteractionTypeKey', 'Fail', 'Missing InteractionTypeKey')
End

If not exists(
				Select		Contact.InteractionTypeKey
				From		[$(Dimensional)].Fact.Contact as Contact
				Where		Contact.InteractionTypeKey = -1
							and (
								(Contact.CreatedDate >= @FromDate)
								or (@FromDate is null)
								)				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.21', 'DataFK_Fact_Contact InteractionTypeKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.21', 'DataFK_Fact_Contact InteractionTypeKey', 'Fail', 'Missing InteractionTypeKey')
End

--InteractionDayKey
If not exists(
				Select		Contact.InteractionDayKey
				From		[$(Dimensional)].Fact.Contact as Contact
							left join [$(Dimensional)].Dimension.DayZone as DayZone on Contact.InteractionDayKey = DayZone.DayKey
				Where		DayZone.DayKey is null
							and (
								(Contact.CreatedDate >= @FromDate)
								or (@FromDate is null)
								)				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.22', 'DataFK_Fact_Contact InteractionDayKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.22', 'DataFK_Fact_Contact InteractionDayKey', 'Fail', 'Missing InteractionDayKey')
End

If not exists(
				Select		Contact.InteractionDayKey
				From		[$(Dimensional)].Fact.Contact as Contact
				Where		Contact.InteractionDayKey = -1
							and (
								(Contact.CreatedDate >= @FromDate)
								or (@FromDate is null)
								)				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.23', 'DataFK_Fact_Contact InteractionDayKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.23', 'DataFK_Fact_Contact InteractionDayKey', 'Fail', 'Missing InteractionDayKey')
End

--InteractionTimeKey
If not exists(
				Select		Contact.InteractionTimeKeyMaster
				From		[$(Dimensional)].Fact.Contact as Contact
							left join [$(Dimensional)].Dimension.[Time] as [Time] on Contact.InteractionTimeKeyMaster = [Time].[TimeKey]
				Where		[Time].TimeKey is null
							and (
								(Contact.CreatedDate >= @FromDate)
								or (@FromDate is null)
								)				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.24', 'DataFK_Fact_Contact InteractionTimeKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.24', 'DataFK_Fact_Contact InteractionTimeKey', 'Fail', 'Missing InteractionTimeKey')
End

If not exists(
				Select		Contact.InteractionTimeKeyMaster
				From		[$(Dimensional)].Fact.Contact as Contact
				Where		Contact.InteractionTimeKeyMaster = -1
							and (
								(Contact.CreatedDate >= @FromDate)
								or (@FromDate is null)
								)				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.25', 'DataFK_Fact_Contact InteractionTimeKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '312.25', 'DataFK_Fact_Contact InteractionTimeKey', 'Fail', 'Missing InteractionTimeKey')
End

--Details
--CreatedDate
--CreatedBatchKey
--ModifiedDate
--ModifiedBatchKey

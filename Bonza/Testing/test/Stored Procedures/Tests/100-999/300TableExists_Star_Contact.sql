--Test Code
--exec Test.[300TableExists_Star_Contact] 'ADHOC2'
Create Procedure Test.[300TableExists_Star_Contact] @TestRunNo varchar(23)

as

Declare @Database varchar(100)
Set @Database = '[$(Dimensional)]'

Insert into Test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('300.0','Structure - Contact Table', 'Fact', 'Contact')

Insert into Test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('300.1','Structure - Communication Table', 'Dimension', 'Communication')

Insert into Test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('300.2','Structure - Story Table', 'Dimension', 'Story')

Insert into Test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('300.3','Structure - Channel Table', 'Dimension', 'Channel')

Insert into Test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('300.4','Structure - Promotion Table', 'Dimension', 'Promotion')

Insert into Test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('300.5','Structure - BetType Table', 'Dimension', 'BetType')

Insert into Test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('300.6','Structure - Class Table', 'Dimension', 'Class')

Insert into Test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('300.7','Structure - Communication Table', 'Dimension', 'Communication')

Insert into Test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('300.8','Structure - Event Table', 'Dimension', 'Event')

Insert into Test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('300.9','Structure - InteractionType Table', 'Dimension', 'InteractionType')

Insert into Test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('300.10','Structure - Day Table', 'Dimension', 'Day')

Insert into Test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('300.11','Structure - Time Table', 'Dimension', 'Time')
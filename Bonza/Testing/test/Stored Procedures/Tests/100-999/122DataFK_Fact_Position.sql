﻿--Test Code
--exec Test.[122DataFK_Fact_Position] 'ADHOC', '2016-02-02'

Create Procedure [Test].[122DataFK_Fact_Position] @TestRunNo varchar(23), @FromDate datetime

as

Select		Position.*
into		#Position
From		[$(Dimensional)].Fact.Position as Position with (nolock)
			inner join [$(Dimensional)].Dimension.DayZone as DayZone on Position.DayKey = DayZone.DayKey
			inner join [$(Dimensional)].Dimension.[Day] as [Day] on DayZone.MasterDayKey = [Day].DayKey
where		[Day].DayDate = Cast (@FromDate as Date)
			or @FromDate is null	

If not exists(
				Select		Position.DayKey
				From		#Position as Position
							left join [$(Dimensional)].Dimension.[DayZone] as [DayZone] on Position.DayKey = DayZone.DayKey
							left join [$(Dimensional)].Dimension.[Day] as [Day] on DayZone.MasterDayKey = [Day].DayKey
				Where		[Day].DayKey is null			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.0', 'DataFK_Fact_Position Day', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.0', 'DataFK_Fact_Position Day', 'Fail', 'Missing DayKeys')
End

If not exists(
				Select		Position.DayKey
				From		#Position as Position
				Where		Position.DayKey = -1			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.1', 'DataFK_Fact_Position Day', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.1', 'DataFK_Fact_Position Day', 'Fail', 'Missing DayKeys')
End

If not exists(
				Select		Position.DayKey
				From		#Position as Position
							left join [$(Dimensional)].Dimension.Account as Account on Position.AccountKey = Account.AccountKey
				Where		Account.AccountKey is null			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.2', 'DataFK_Fact_Position Account', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.2', 'DataFK_Fact_Position Account', 'Fail', 'Missing AccountKeys')
End

If not exists(
				Select		Position.DayKey
				From		#Position as Position
				Where		Position.AccountKey = -1			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.3', 'DataFK_Fact_Position Account', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.3', 'DataFK_Fact_Position Account', 'Fail', 'Missing AccountKeys')
End

If not exists(
				Select		Position.DayKey
				From		#Position as Position
							left join [$(Dimensional)].Dimension.AccountStatus as AccountStatus on Position.AccountStatusKey = AccountStatus.AccountStatusKey
				Where		AccountStatus.AccountStatusKey is null
							and Position.AccountStatusKey not in (0,-1)			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.4', 'DataFK_Fact_Position AccountStatus', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.4', 'DataFK_Fact_Position AccountStatus', 'Fail', 'Missing AccountStatusKeys')
End

If not exists(
				Select		Position.DayKey
				From		#Position as Position
				Where		Position.AccountStatusKey = -1			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.5', 'DataFK_Fact_Position AccountStatus', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-7990'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '122.5', 'DataFK_Fact_Position AccountStatus', 'Pass', 'CCIA-5363')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '122.5', 'DataFK_Fact_Position AccountStatus', 'Fail', 'Missing AccountStatusKeys')
	End
End

If not exists(
				Select		Position.DayKey
				From		#Position as Position
							left join [$(Dimensional)].Dimension.BalanceType as BalanceType on Position.BalanceTypeKey = BalanceType.BalanceTypeKey
				Where		BalanceType.BalanceTypeKey is null			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.6', 'DataFK_Fact_Position BalanceType', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.6', 'DataFK_Fact_Position BalanceType', 'Fail', 'Missing BalanceTypeKeys')
End

If not exists(
				Select		Position.DayKey
				From		#Position as Position
				Where		Position.BalanceTypeKey = -1			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.7', 'DataFK_Fact_Position BalanceType', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.7', 'DataFK_Fact_Position BalanceType', 'Fail', 'Missing BalanceTypeKeys')
End

If not exists(
				Select		Position.DayKey
				From		#Position as Position
							left join [$(Dimensional)].Dimension.Activity as Activity on Position.ActivityKey = Activity.ActivityKey
				Where		Activity.ActivityKey is null			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.8', 'DataFK_Fact_Position Activity', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.8', 'DataFK_Fact_Position Activity', 'Fail', 'Missing ActivityKeys')
End

If not exists(
				Select		Position.DayKey
				From		#Position as Position
				Where		Position.ActivityKey = -1			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.9', 'DataFK_Fact_Position Activity', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.9', 'DataFK_Fact_Position Activity', 'Fail', 'Missing ActivityKeys')
End

If not exists(
				Select		Position.FirstDepositDayKey
				From		#Position as Position
							left join [$(Dimensional)].Dimension.[DayZone] as [DayZone] on Position.FirstDepositDayKey = DayZone.DayKey
							left join [$(Dimensional)].Dimension.[Day] as [Day] on DayZone.MasterDayKey = [Day].DayKey
				Where		[Day].DayKey is null			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.12', 'DataFK_Fact_Position FirstDepositDayKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.12', 'DataFK_Fact_Position FirstDepositDayKey', 'Fail', 'Missing FirstDepositDayKey')
End

If not exists(
				Select		Position.FirstDepositDayKey
				From		#Position as Position
				Where		Position.FirstDepositDayKey = -1				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.13', 'DataFK_Fact_Position FirstDepositDayKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.13', 'DataFK_Fact_Position FirstDepositDayKey', 'Fail', 'Missing FirstDepositDayKey')
End


If not exists(
				Select		Position.FirstBetDayKey
				From		#Position as Position
							left join [$(Dimensional)].Dimension.[DayZone] as [DayZone] on Position.FirstBetDayKey = DayZone.DayKey
							left join [$(Dimensional)].Dimension.[Day] as [Day] on DayZone.MasterDayKey = [Day].DayKey
				Where		[Day].DayKey is null			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.14', 'DataFK_Fact_Position FirstBetDayKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.14', 'DataFK_Fact_Position FirstBetDayKey', 'Fail', 'Missing FirstBetDayKey')
End

If not exists(
				Select		Position.FirstBetDayKey
				From		#Position as Position
				Where		Position.FirstBetDayKey = -1			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.15', 'DataFK_Fact_Position FirstBetDayKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.15', 'DataFK_Fact_Position FirstBetDayKey', 'Fail', 'Missing FirstBetDayKey')
End

If not exists(
				Select		Position.FirstBetTypeKey
				From		#Position as Position
							left join [$(Dimensional)].Dimension.BetType as BetType on Position.FirstBetTypeKey = BetType.BetTypeKey
				Where		BetType.BetTypeKey is null				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.16', 'DataFK_Fact_Position FirstBetTypeKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.16', 'DataFK_Fact_Position FirstBetTypeKey', 'Fail', 'Missing FirstBetTypeKey')
End

If not exists(
				Select		Position.FirstBetTypeKey
				From		#Position as Position
				Where		Position.FirstBetTypeKey = -1			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.17', 'DataFK_Fact_Position FirstBetTypeKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.17', 'DataFK_Fact_Position FirstBetTypeKey', 'Fail', 'Missing FirstBetTypeKey')
End


If not exists(
				Select		Position.FirstClassKey
				From		#Position as Position
							left join [$(Dimensional)].Dimension.Class as Class on Position.FirstClassKey = Class.ClassKey
				Where		Class.ClassKey is null			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.18', 'DataFK_Fact_Position FirstClassKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.18', 'DataFK_Fact_Position FirstClassKey', 'Fail', 'Missing FirstClassKey')
End

If not exists(
				Select		Position.FirstClassKey
				From		#Position as Position
				Where		Position.FirstClassKey = -1	
							AND DayKey >= 2014010100		--start of warehouse data
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.19', 'DataFK_Fact_Position FirstClassKey', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-7803'
							and [Status] != 'Done'
				)
	and
	not exists     (Select		Position.FirstClassKey
					From		#Position as Position
								inner join [$(Dimensional)].Dimension.Account as Account on Position.AccountKey = Account.AccountKey
					Where		Position.FirstClassKey = -1	
								AND DayKey >= 2014010100		--start of warehouse data
								and  Account.AccountOpenedDayKey >= 2016121903
					)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '122.19', 'DataFK_Fact_Position FirstClassKey', 'Pass', 'CCIA-7803')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '122.19', 'DataFK_Fact_Position FirstClassKey', 'Fail', 'Missing FirstClassKey')
	End
End



If not exists(
				Select		Position.FirstChannelKey
				From		#Position as Position
							left join [$(Dimensional)].Dimension.Channel as Channel on Position.FirstChannelKey = Channel.ChannelKey
				Where		Channel.ChannelKey is null			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.20', 'DataFK_Fact_Position FirstChannelKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.20', 'DataFK_Fact_Position FirstChannelKey', 'Fail', 'Missing FirstChannelKey')
End

If not exists(
				Select		Position.FirstChannelKey
				From		#Position as Position
				Where		Position.FirstChannelKey = -1				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.21', 'DataFK_Fact_Position FirstChannelKey', 'Pass', Null)
End
Else
BEGIN
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-7516'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '122.21', 'DataFK_Fact_Position FirstChannelKey', 'Pass', 'CCIA-7516')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '122.21', 'DataFK_Fact_Position FirstChannelKey', 'Fail', 'Missing FirstChannelKey')	
	End
End

If not exists(
				Select		Position.LastBetDayKey
				From		#Position as Position
							left join [$(Dimensional)].Dimension.[DayZone] as [DayZone] on Position.LastBetDayKey = DayZone.DayKey
							left join [$(Dimensional)].Dimension.[Day] as [Day] on DayZone.MasterDayKey = [Day].DayKey
				Where		[Day].DayKey is null				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.22', 'DataFK_Fact_Position LastBetDayKey', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-5429'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '122.22', 'DataFK_Fact_Position LastBetDayKey', 'Pass', 'CCIA-5429')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '122.22', 'DataFK_Fact_Position LastBetDayKey', 'Fail', 'Missing LastBetDayKey')
	End
End

If not exists(
				Select		Position.LastBetDayKey
				From		#Position as Position
				Where		Position.LastBetDayKey = -1			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.23', 'DataFK_Fact_Position LastBetDayKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.23', 'DataFK_Fact_Position LastBetDayKey', 'Fail', 'Missing LastBetDayKey')
End

If not exists(
				Select		Position.LastBetTypeKey
				From		#Position as Position
							left join [$(Dimensional)].Dimension.BetType as BetType on Position.LastBetTypeKey = BetType.BetTypeKey
				Where		BetType.BetTypeKey is null				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.24', 'DataFK_Fact_Position LastBetTypeKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.24', 'DataFK_Fact_Position LastBetTypeKey', 'Fail', 'Missing LastBetTypeKey')
End

If not exists(
				Select		Position.LastBetTypeKey
				From		#Position as Position
				Where		Position.LastBetTypeKey = -1				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.25', 'DataFK_Fact_Position LastBetTypeKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.25', 'DataFK_Fact_Position LastBetTypeKey', 'Fail', 'Missing LastBetTypeKey')
End


If not exists(
				Select		Position.LastClassKey
				From		#Position as Position
							left join [$(Dimensional)].Dimension.Class as Class on Position.LastClassKey = Class.ClassKey
				Where		Class.ClassKey is null				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.26', 'DataFK_Fact_Position LastClassKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.26', 'DataFK_Fact_Position LastClassKey', 'Fail', 'Missing LastClassKey')
End

If not exists(
				Select		Position.LastClassKey
				From		#Position as Position
				Where		Position.LastClassKey = -1			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.27', 'DataFK_Fact_Position LastClassKey', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-5429'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '122.27', 'DataFK_Fact_Position LastClassKey', 'Pass', 'CCIA-5429')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '122.27', 'DataFK_Fact_Position LastClassKey', 'Fail', 'Missing LastClassKey')
	End
End



If not exists(
				Select		Position.LastChannelKey
				From		#Position as Position
							left join [$(Dimensional)].Dimension.Channel as Channel on Position.LastChannelKey = Channel.ChannelKey
				Where		Channel.ChannelKey is null			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.28', 'DataFK_Fact_Position LastChannelKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.28', 'DataFK_Fact_Position LastChannelKey', 'Fail', 'Missing LastChannelKey')
End

If not exists(
				Select		Position.LastChannelKey
				From		#Position as Position
				Where		Position.LastChannelKey = -1			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.29', 'DataFK_Fact_Position LastChannelKey', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-5429'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '122.29', 'DataFK_Fact_Position LastChannelKey', 'Pass', 'CCIA-5429')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '122.29', 'DataFK_Fact_Position LastChannelKey', 'Fail', 'Missing LastChannelKey')
	End
End

If not exists(
				Select		Position.TierKey
				From		#Position as Position
				Where		Position.TierKey = -1			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.30', 'DataFK_Fact_Position TierKey', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-5646'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '122.30', 'DataFK_Fact_Position TierKey', 'Pass', 'CCIA-5646')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '122.30', 'DataFK_Fact_Position TierKey', 'Fail', 'Missing TierKey')
	End
End

If not exists(
				Select		Position.TierKey
				From		#Position as Position
							left join [$(Dimensional)].Dimension.ValueTiers as ValueTiers on Position.TierKey = ValueTiers.TierKey
				Where		ValueTiers.TierKey is null			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.31', 'DataFK_Fact_Position TierKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.31', 'DataFK_Fact_Position TierKey', 'Fail', 'Missing TierKey')
End

If not exists(
				Select		Position.TierKeyFiscal
				From		#Position as Position
				Where		Position.TierKeyFiscal = -1			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.32', 'DataFK_Fact_Position TierKeyFiscal', 'Pass', null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-5646'
							and [Status] != 'Done'
				)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '122.32', 'DataFK_Fact_Position TierKeyFiscal', 'Pass', 'CCIA-5646')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '122.32', 'DataFK_Fact_Position TierKeyFiscal', 'Fail', 'Missing TierKeyFiscal')
	End
End

If not exists(
				Select		Position.TierKeyFiscal
				From		#Position as Position
							left join [$(Dimensional)].Dimension.ValueTiers as ValueTiers on Position.TierKeyFiscal = ValueTiers.TierKey
				Where		ValueTiers.TierKey is null			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.33', 'DataFK_Fact_Position TierKeyFiscal', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.33', 'DataFK_Fact_Position TierKeyFiscal', 'Fail', 'Missing TierKeyFiscal')
End

--RevenueLifeCycleKey
If not exists(
				Select		Position.DayKey
				From		#Position as Position
							left join [$(Dimensional)].Dimension.RevenueLifecycle as RevenueLifecycle on Position.RevenueLifecycleKey = RevenueLifecycle.RevenueLifecycleKey
				Where		RevenueLifecycle.RevenueLifecycleKey is null		
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.34', 'DataFK_Fact_Position RevenueLifeCycleKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.34', 'DataFK_Fact_Position RevenueLifeCycleKey', 'Fail', 'Missing RevenueLifeCycleKey')
End

If not exists(
				Select		Position.DayKey
				From		#Position as Position
				Where		Position.RevenueLifecycleKey = -1			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.35', 'DataFK_Fact_Position RevenueLifeCycleKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.35', 'DataFK_Fact_Position RevenueLifeCycleKey', 'Fail', 'Missing RevenueLifeCycleKey')
End

--LifeStagesKey
If not exists(
				Select		Position.DayKey
				From		#Position as Position
							left join [$(Dimensional)].Dimension.Lifestage as Lifestage on Position.LifestageKey = Lifestage.LifestageKey
				Where		Lifestage.LifestageKey is null				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.36', 'DataFK_Fact_Position Lifestage', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.36', 'DataFK_Fact_Position Lifestage', 'Fail', 'Missing LifestageKeys')
End

If not exists(
				Select		Position.DayKey
				From		#Position as Position
				Where		Position.LifestageKey = -1				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.37', 'DataFK_Fact_Position Lifestage', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.37', 'DataFK_Fact_Position Lifestage', 'Fail', 'Missing LifestageKeys')
End

--PreviousLifeStagesKey
If not exists(
				Select		Position.DayKey
				From		#Position as Position
							left join [$(Dimensional)].Dimension.Lifestage as Lifestage on PreviousLifeStageKey = Lifestage.LifestageKey
				Where		Lifestage.LifestageKey is null			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.38', 'DataFK_Fact_Position PreviousLifeStageKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.38', 'DataFK_Fact_Position PreviousLifeStageKey', 'Fail', 'Missing PreviousLifeStageKey')
End

If not exists(
				Select		Position.DayKey
				From		#Position as Position
				Where		PreviousLifeStageKey = -1				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.39', 'DataFK_Fact_Position PreviousLifeStageKey', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-6212'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '122.39', 'DataFK_Fact_Position PreviousLifeStageKey', 'Pass', 'CCIA-6212')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '122.39', 'DataFK_Fact_Position PreviousLifeStageKey', 'Fail', 'Missing PreviousLifeStageKey')
	End
End

--StructureKey
If not exists(
				Select		Position.DayKey
				From		#Position as Position
							left join [$(Dimensional)].Dimension.Lifestage as Lifestage on Position.LifestageKey = Lifestage.LifestageKey
				Where		Lifestage.LifestageKey is null				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.36', 'DataFK_Fact_Position Lifestage', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.36', 'DataFK_Fact_Position Lifestage', 'Fail', 'Missing LifestageKeys')
End

If not exists(
				Select		Position.DayKey
				From		#Position as Position
				Where		Position.LifestageKey = -1				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.37', 'DataFK_Fact_Position Lifestage', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '122.37', 'DataFK_Fact_Position Lifestage', 'Fail', 'Missing LifestageKeys')
End
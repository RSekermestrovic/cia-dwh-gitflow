﻿--Test Code
--exec [Test].[124DataCurrent_Dimension_LegType] 'ADHOC'
Create Procedure [Test].[124DataCurrent_Dimension_LegType] @TestRunNo varchar(23)

as

Select	*
into #DimensionLegType
From [$(Dimensional)].Dimension.[LegType]

Declare @NumberOfRecords int
Set @NumberOfRecords = 
					(
					Select		count(*) as NumberOfRecords
					From		#DimensionLegType
					)
If @NumberOfRecords >= 5052
Begin
	Insert into test.tblResults Values (@TestRunNo, '124', 'DataCurrent_Dimension_LegType Record Count', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '124', 'DataCurrent_Dimension_LegType Record Count', 'Fail', 'Expected at least 5052 records, ' + Cast (@NumberOfRecords as varchar(5)) + ' found.')
End

IF 					(
					Select		Min (LegTypeKey) as Record
					From		#DimensionLegType
					where		LegTypeKey not in (0 ,-1)
					)
					= 1
Begin
	Insert into test.tblResults Values (@TestRunNo, '124.1', 'DataCurrent_Dimension_LegType Record Count', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '124.1', 'DataCurrent_Dimension_LegType Record Count', 'Fail', 'The lowest record key is not 1')
End

IF 					(
					Select		Max (LegTypeKey) as Record
					From		#DimensionLegType
					where		LegTypeKey not in (0 ,-1)
					)
					>= 5050
Begin
	Insert into test.tblResults Values (@TestRunNo, '124.2', 'DataCurrent_Dimension_LegType Record Count', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '124.2', 'DataCurrent_Dimension_LegType Record Count', 'Fail', 'The highest record key should be over 5049')
End



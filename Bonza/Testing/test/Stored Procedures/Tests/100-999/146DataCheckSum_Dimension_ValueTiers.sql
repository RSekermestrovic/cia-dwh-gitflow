﻿--Test Code
--exec Test.[146DataCheckSum_Dimension_ValueTiers] 'ADHOC'
Create Procedure Test.[146DataCheckSum_Dimension_ValueTiers] @TestRunNo varchar(23)

as

If	(Select		Sum (TierNumber)
	 From		[$(Dimensional)].Dimension.ValueTiers
	 ) = 35
Begin
	Insert into test.tblResults Values (@TestRunNo, '146', 'DataCheckSum_Dimension_ValueTiers', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '146', 'DataCheckSum_Dimension_ValueTiers', 'Fail', 'The sum of records is incorrect')
End
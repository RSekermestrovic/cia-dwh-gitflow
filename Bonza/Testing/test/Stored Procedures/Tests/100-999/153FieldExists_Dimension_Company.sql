--Test Code
--exec Test.[153FieldExists_Dimension_Company] 'ADHOC'
Create Procedure Test.[153FieldExists_Dimension_Company] @TestRunNo varchar(23)

as

Declare @Database varchar(100)
Set @Database = '[$(Dimensional)]'

Declare @SQL varchar(max)
Set @SQL = 'Select top 1 
[CompanyKey]
      ,[OrgId]
      ,[DivisionId]
      ,[DivisionName]
      ,[BrandId]
      ,[BrandCode]
      ,[BrandName]
      ,[BusinessUnitId]
      ,[BusinessUnitCode]
      ,[BusinessUnitName]
      ,[CompanyID]
      ,[CompanyCode]
      ,[CompanyName]
      ,[FromDate]
      ,[ToDate]
      ,[FirstDate]
      ,[CreatedDate]
      ,[CreatedBatchKey]
      ,[CreatedBy]
      ,[ModifiedDate]
      ,[ModifiedBatchKey]
      ,[ModifiedBy]
			from ' + @Database + '.[Dimension].[Company]'

Exec Test.spSelectValid @TestRunNo, '153','Structure - Dimension Company Table', @SQL

If		(
		Select		Count(*)
		From		[$(Dimensional)].sys.all_objects as [Objects]
					inner join [$(Dimensional)].sys.all_columns as [Columns] on [Objects].[object_id] = [Columns].[object_id]
					inner join [$(Dimensional)].sys.schemas as [schemas] on [Objects].[schema_id] = [schemas].[schema_id]
		where		[Objects].name = 'Company'
					and [Objects].[type] = 'U'
					and [schemas].Name = 'Dimension'
		) = 22
Begin
	Insert into test.tblResults Values (@TestRunNo, '153.1', 'Structure - Dimension Company Table', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '153.1', 'Structure - Dimension Company Table', 'Fail', 'Wrong number of columns')
End
﻿--test
--exec [Test].[160DataDuplicates_Dimension_Event] 'ADHOC'

CREATE PROCEDURE [Test].[160DataDuplicates_Dimension_Event] @TestRunNo varchar(23)

as

If not exists(
		Select		Count(*)
		From		[$(Dimensional)].Dimension.[Event] as [Event]
		Where		EventKey not in (0, -1)
		Group by	[Event].EventID
		Having		Count(*) > 1
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '160', 'DataDuplicates_Dimension_Event', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '160', 'DataDuplicates_Dimension_Event', 'Fail', 'Duplicates of Event found')
End
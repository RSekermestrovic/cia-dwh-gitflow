﻿--Test Code
--exec [Test].[307DataDefault_Dimension_Story] 'ADHOC'
Create Procedure [Test].[307DataDefault_Dimension_Story] @TestRunNo varchar(23)

as
If exists(
		Select		StoryKey
		From		[$(Dimensional)].[Dimension].Story
		where		StoryKey = 0
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '307', 'DataDefault_Dimension_Story Table ID0', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-6652'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '307', 'DataDefault_Dimension_Story Table ID0', 'Pass', 'CCIA-6650')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '307', 'DataDefault_Dimension_Story Table ID0', 'Fail', 'No ID 0 record found')
	End
End

If exists(
		Select		StoryKey
		From		[$(Dimensional)].[Dimension].Story
		where		StoryKey =  -1
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '307.1', 'DataDefault_Dimension_Story Table ID-1', 'Pass', Null)
End
Else
Begin
If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-6652'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '307.1', 'DataDefault_Dimension_Story Table ID-1', 'Pass', 'CCIA-6651')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '307.1', 'DataDefault_Dimension_Story Table ID-1', 'Fail', 'No ID -1 record found')
	End
End

﻿--Test Code
--exec Test.[308BVA_Fact_Contact] 'ADHOC', '2015-04-03'

Create Procedure Test.[308BVA_Fact_Contact] @TestRunNo varchar(23), @FromDate datetime

as

Declare @ErrorText varchar(100)
If @FromDate is not null
Begin
Set @ErrorText = ' since ' + Convert (varchar(20), @FromDate, 103)
End
else
Begin
Set @ErrorText = '' 
End

Select		* 
into		#Contact
From		[$(Dimensional)].fact.Contact
--where		((FromDate > @FromDate)
			--		or (@FromDate is null))
			--and ToDate = '9999-12-31'

--AccountKey n/a
--AccountStatusKey n/a
--CommunicationId
If not exists(
				Select		*
				From		#Contact
				where		CommunicationId <= 0
			) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '308.0', 'BVA_Fact_Contact CommunicationId', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '308.0', 'BVA_Fact_Contact CommunicationId', 'Fail', 'There are records with an invalid CommunicationId' + @ErrorText)
End

--CommunicationKey n/a
--StoryKey n/a
--ChannelKey n/a
--PromotionKey n/a
--BetTypeKey n/a
--ClassKey n/a
--CommunicationCompanyKey n/a
--EventKey n/a
--InteractionTypeKey n/a
--InteractionDayKey n/a
--InteractionTimeKey n/a
--Details
If not exists(
				Select		*
				From		#Contact
				where		Details is null
			) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '308.1', 'BVA_Fact_Contact CommunicationId', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '308.1', 'BVA_Fact_Contact CommunicationId', 'Fail', 'There are records with an invalid Details' + @ErrorText)
End

--CreatedDate n/a
--CreatedBatchKey n/a
--ModifiedDate n/a
--ModifiedBatchKey n/a
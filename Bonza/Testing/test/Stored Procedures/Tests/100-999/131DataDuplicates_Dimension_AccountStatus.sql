﻿--test
--exec [Test].[131DataDuplicates_Dimension_AccountStatus] 'ADHOC', null

CREATE PROCEDURE [Test].[131DataDuplicates_Dimension_AccountStatus] @TestRunNo varchar(23), @FromDate datetime

as

If not exists(
		Select		Count(*)
		From		[$(Dimensional)].Dimension.AccountStatus
		where		@FromDate is null
					or CreatedDate >= @FromDate
		Group by	[AccountKey], [FromDate]
		Having		Count(*) > 1
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '131', 'DataDuplicates_Dimension_AccountStatus', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '131', 'DataDuplicates_Dimension_AccountStatus', 'Fail', 'Duplicates of AccountStatus found')
End
Declare @Counter int
Set @Counter  = (
				Select		Count(*)
				From		[$(Dimensional)].Dimension.AccountStatus as Account1
							left join [$(Dimensional)].Dimension.AccountStatus as Account2 on Account1.AccountKey = Account2.AccountKey
				Where		Account1.AccountStatusKey < Account2.AccountStatusKey
							and Account1.ToDate > Account2.FromDate
				)
IF @Counter = 0				 
Begin
	Insert into test.tblResults Values (@TestRunNo, '131.1', 'DataDuplicates_Dimension_AccountStatus', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '131.1', 'DataDuplicates_Dimension_AccountStatus', 'Fail', 'From/To Overlap of AccountStatus found')
End
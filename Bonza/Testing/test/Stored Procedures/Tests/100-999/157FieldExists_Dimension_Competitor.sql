--Test Code
--exec Test.[157FieldExists_Dimension_Competitor] 'ADHOC'
Create Procedure Test.[157FieldExists_Dimension_Competitor] @TestRunNo varchar(23)

as

Declare @Database varchar(100)
Set @Database = '[$(Dimensional)]'

Declare @SQL varchar(max)
Set @SQL = 'Select top 1 
[CompetitorKey]
      ,[CompetitorID]
      ,[CompetitorSource]
      ,[SaddleNumber]
      ,[CompetitorName]
      ,[Scratched]
      ,[Jockey]
      ,[Draw]
      ,[Form]
      ,[History]
      ,[Weight]
      ,[Trainer]
      ,[ManualIntPrice]
      ,[bestTime]
      ,[scratchedLate]
      ,[rulesOverrideState]
      ,[ManualIntPriceCB]
      ,[TabRollCount]
      ,[IsKnockedOut]
      ,[EVENT_SK]
      ,[ManualIntPriceTW]
      ,[FromDate]
      ,[ToDate]
      ,[FirstDate]
      ,[CreatedDate]
      ,[CreatedBatchKey]
      ,[CreatedBy]
      ,[ModifiedDate]
      ,[ModifiedBatchKey]
      ,[ModifiedBy]
			from ' + @Database + '.[Dimension].[Competitor]'

Exec Test.spSelectValid @TestRunNo, '157','Structure - Dimension Competitor Table', @SQL

If		(
		Select		Count(*)
		From		[$(Dimensional)].sys.all_objects as [Objects]
					inner join [$(Dimensional)].sys.all_columns as [Columns] on [Objects].[object_id] = [Columns].[object_id]
					inner join [$(Dimensional)].sys.schemas as [schemas] on [Objects].[schema_id] = [schemas].[schema_id]
		where		[Objects].name = 'Competitor'
					and [Objects].[type] = 'U'
					and [schemas].Name = 'Dimension'
		) = 30
Begin
	Insert into test.tblResults Values (@TestRunNo, '153.1', 'Structure - Dimension Competitor Table', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '153.1', 'Structure - Dimension Competitor Table', 'Fail', 'Wrong number of columns')
End
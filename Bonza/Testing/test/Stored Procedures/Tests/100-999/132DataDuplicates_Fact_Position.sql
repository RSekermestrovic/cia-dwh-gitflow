﻿--test
--exec [Test].[132DataDuplicates_Fact_Position] 'ADHOC', '2016-02-02'

Create PROCEDURE [Test].[132DataDuplicates_Fact_Position] @TestRunNo varchar(23), @FromDate datetime

as

Select		Distinct DayKey
into		#DayKeys
From		(
			Select		Dayzone.DayKey
			From		[$(Dimensional)].Dimension.[Day] as [Day]
						inner join [$(Dimensional)].Dimension.DayZone as DayZone on [Day].DayKey = DayZone.MasterDayKey
			Where		[Day].DayDate = Cast (@FromDate as date)
			union
			Select		Dayzone.DayKey
			From		[$(Dimensional)].Dimension.[Day] as [Day]
						inner join [$(Dimensional)].Dimension.DayZone as DayZone on [Day].DayKey = DayZone.AnalysisDayKey
			Where		[Day].DayDate = Cast (@FromDate as date)
			) as Data


Declare @Counter int
Set @Counter = (
				Select		Count(*)
				From		[$(Dimensional)].Fact.[Position] as [Position] with (nolock)
				where		DayKey in (Select DayKey from #DayKeys)
				Group by	[BalanceTypeKey], [AccountKey], [DayKey]
				Having		Count(*) > 1
				) 
IF @Counter = 0 or @Counter is null
Begin
	Insert into test.tblResults Values (@TestRunNo, '132', 'DataDuplicates_Fact_Position', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '132', 'DataDuplicates_Fact_Position', 'Fail', 'Duplicates of Position found')
End

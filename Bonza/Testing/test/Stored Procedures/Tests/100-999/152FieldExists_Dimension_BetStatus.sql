--Test Code
--exec Test.[152FieldExists_Dimension_BetStatus] 'ADHOC'
Create Procedure Test.[152FieldExists_Dimension_BetStatus] @TestRunNo varchar(23)

as

Declare @Database varchar(100)
Set @Database = '[$(Dimensional)]'

Declare @SQL varchar(max)
Set @SQL = 'Select top 1 
[StatusKey]
      ,[StatusCode]
      ,[StatusName]
      ,[OutcomeCode]
      ,[OutcomeName]
      ,[FromDate]
      ,[ToDate]
      ,[FirstDate]
      ,[CreatedDate]
      ,[CreatedBatchKey]
      ,[CreatedBy]
      ,[ModifiedDate]
      ,[ModifiedBatchKey]
      ,[ModifiedBy]
			from ' + @Database + '.[Dimension].[BetStatus]'

Exec Test.spSelectValid @TestRunNo, '152','Structure - Dimension BetStatus Table', @SQL

If		(
		Select		Count(*)
		From		[$(Dimensional)].sys.all_objects as [Objects]
					inner join [$(Dimensional)].sys.all_columns as [Columns] on [Objects].[object_id] = [Columns].[object_id]
					inner join [$(Dimensional)].sys.schemas as [schemas] on [Objects].[schema_id] = [schemas].[schema_id]
		where		[Objects].name = 'BetStatus'
					and [Objects].[type] = 'U'
					and [schemas].Name = 'Dimension'
		) = 14
Begin
	Insert into test.tblResults Values (@TestRunNo, '152.1', 'Structure - Dimension BetStatus Table', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '152.1', 'Structure - Dimension BetStatus Table', 'Fail', 'Wrong number of columns')
End
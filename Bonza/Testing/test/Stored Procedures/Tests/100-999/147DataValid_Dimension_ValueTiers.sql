﻿--Test Code
--exec Test.[147DataValid_Dimension_ValueTiers] 'ADHOC'
Create Procedure Test.[147DataValid_Dimension_ValueTiers] @TestRunNo varchar(23)

as


If		  (
			Select		Count(*)
			From		[$(Dimensional)].[Dimension].ValueTiers
			where		TierName like '>=%'
			) = 1
Begin
	Insert into test.tblResults Values (@TestRunNo, '147', 'DataValid_Dimension_ValueTiers', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '147', 'DataValid_Dimension_ValueTiers', 'Fail', 'Incorrect number of top tiers')
End

If		  (
			Select		Count(*)
			From		[$(Dimensional)].[Dimension].ValueTiers
			where		TierName like '<%'
			) = 1
Begin
	Insert into test.tblResults Values (@TestRunNo, '147.1', 'DataValid_Dimension_ValueTiers', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '147.1', 'DataValid_Dimension_ValueTiers', 'Fail', 'Incorrect number of bottom tiers')
End

Select		TierName
			, 'LowerBand' = CASE
				When TierName not like '%X%' and TierName like '>=%' then Replace (TierName, '>= ', '')
				When TierName not like '%X%' and TierName like '<%' then 0
				Else Replace (Left (TierName,CHARINDEX('X', TierName,1)), ' <= X', '')
				End
			, 'UpperBand' = CASE
				When TierName not like '%X%' and TierName like '>=%' then 99999999999
				When TierName not like '%X%' and TierName like '<%' then Replace (TierName, '<','')
				Else Replace (Right (TierName,CHARINDEX('X', TierName,1)), 'X <', '')
				End
into		#Data
From		[$(Dimensional)].Dimension.ValueTiers
Where		TierKey not in (0, -1)

IF not exists (
				Select		*
				From		#Data as Data1
							cross join #Data as Data2
				Where		Data1.Tiername != Data2.Tiername
							and Data2.Lowerband < Data1.Upperband
							and Data2.Upperband > Data1.Lowerband
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '147.2', 'DataValid_Dimension_ValueTiers', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '147.2', 'DataValid_Dimension_ValueTiers', 'Fail', 'Overlaping ranges')
End
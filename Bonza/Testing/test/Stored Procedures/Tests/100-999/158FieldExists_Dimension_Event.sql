--Test Code
--exec Test.[158FieldExists_Dimension_Event] 'ADHOC'
Create Procedure Test.[158FieldExists_Dimension_Event] @TestRunNo varchar(23)

as

Declare @Database varchar(100)
Set @Database = '[$(Dimensional)]'

Declare @SQL varchar(max)
Set @SQL = 'Select top 1 
	[EventKey]
      ,[Source]
      ,[SourceEventId]
      ,[EventId]
      ,[SourceEvent]
      ,[SourceEventType]
      ,[ParentEventId]
      ,[ParentEvent]
      ,[GrandparentEventId]
      ,[GrandparentEvent]
      ,[GreatGrandparentEventId]
      ,[GreatGrandparentEvent]
      ,[SourceEventDate]
      ,[ClassID]
      ,[ClassKey]
      ,[SourceRaceNumber]
      ,[Event]
      ,[Round]
      ,[RoundSequence]
      ,[Competition]
      ,[CompetitionSeason]
      ,[CompetitionStartDate]
      ,[CompetitionEndDate]
      ,[Grade]
      ,[Distance]
      ,[SourcePrizePool]
      ,[PrizePool]
      ,[SourceTrackCondition]
      ,[SourceEventWeather]
      ,[TrackCondition]
      ,[EventWeather]
      ,[EventAbandoned]
      ,[EventAbandonedDateTime]
      ,[EventAbandonedDayKey]
      ,[LiveVideoStreamed]
      ,[LiveScoreBoard]
      ,[ScheduledDateTime]
      ,[ScheduledDayKey]
      ,[ScheduledVenueDateTime]
      ,[ResultedDateTime]
      ,[ResultedDayKey]
      ,[ResultedVenueDateTime]
      ,[SourceVenueId]
      ,[SourceVenue]
      ,[SourceVenueType]
      ,[SourceVenueEventType]
      ,[SourceVenueEventTypeName]
      ,[SourceVenueStateCode]
      ,[SourceVenueState]
      ,[SourceVenueCountryCode]
      ,[SourceVenueCountry]
      ,[VenueId]
      ,[Venue]
      ,[VenueType]
      ,[VenueStateCode]
      ,[VenueState]
      ,[VenueCountryCode]
      ,[VenueCountry]
      ,[SourceSubSportType]
      ,[SourceRaceGroup]
      ,[SourceRaceClass]
      ,[SourceHybridPricingTemplate]
      ,[SourceWeightType]
      ,[SubSportType]
      ,[RaceGroup]
      ,[RaceClass]
      ,[HybridPricingTemplate]
      ,[WeightType]
      ,[SourceDistance]
      ,[SourceEventAbandoned]
      ,[SourceLiveStreamPerformId]
      ,[ColumnLock]
      ,[FromDate]
      ,[FirstDate]
      ,[CreatedDate]
      ,[CreatedBatchKey]
      ,[CreatedBy]
      ,[ModifiedDate]
      ,[ModifiedBatchKey]
      ,[ModifiedBy]
			from ' + @Database + '.[Dimension].[Event]'

Exec Test.spSelectValid @TestRunNo, '158','Structure - Dimension Event Table', @SQL

If		(
		Select		Count(*)
		From		[$(Dimensional)].sys.all_objects as [Objects]
					inner join [$(Dimensional)].sys.all_columns as [Columns] on [Objects].[object_id] = [Columns].[object_id]
					inner join [$(Dimensional)].sys.schemas as [schemas] on [Objects].[schema_id] = [schemas].[schema_id]
		where		[Objects].name = 'Event'
					and [Objects].[type] = 'U'
					and [schemas].Name = 'Dimension'
		) = 69
Begin
	Insert into test.tblResults Values (@TestRunNo, '158.1', 'Structure - Dimension Event Table', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '158.1', 'Structure - Dimension Event Table', 'Fail', 'Wrong number of columns')
End
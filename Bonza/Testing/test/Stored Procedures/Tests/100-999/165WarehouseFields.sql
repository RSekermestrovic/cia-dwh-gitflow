﻿--Test Code
--exec Test.[165WarehouseFields] 'ADHOC'
CREATE PROCEDURE [Test].[165WarehouseFields] @TestRunNo varchar(23)

as

If Not Exists	(
				select		[schemas].Name as [Schema]
							, [Objects].Name as [Table]
							, [columns].name as [Column]
							, OpenWarehouseBugs.JiraID
							, JIra.[Status]
				From		[$(Dimensional)].sys.all_objects as [Objects]
							inner join [$(Dimensional)].sys.schemas as [schemas] on [Objects].[schema_id] = [schemas].[schema_id]
							inner join [$(Dimensional)].sys.all_columns as [columns] on [Objects].[object_id] = [columns].[object_id]
							left join	(
										select		[schemas].Name as [Schema]
													, [Objects].Name as [Table]
													, [columns].name as [Column]
										From		[$(Warehouse)].sys.all_objects as [Objects]
													inner join [$(Warehouse)].sys.schemas as [schemas] on [Objects].[schema_id] = [schemas].[schema_id]
													inner join [$(Warehouse)].sys.all_columns as [columns] on [Objects].[object_id] = [columns].[object_id]
										Where		[schemas].name in ('Fact', 'Dimension')
										) as Warehouse on [schemas].Name = Warehouse.[Schema]
														and [Objects].Name = Warehouse.[Table]
														and [columns].name = Warehouse.[Column]
							left join test.OpenWarehouseBugs as OpenWarehouseBugs on [schemas].Name = OpenWarehouseBugs.[Schema]
																					and [Objects].Name = OpenWarehouseBugs.[Table]
																					and ([columns].name = OpenWarehouseBugs.[Column] or OpenWarehouseBugs.[Column] = 'ALL')
							left join test.tblJira as Jira on OpenWarehouseBugs.JiraID = Jira.JiraID
				Where		[schemas].name in ('Fact', 'Dimension')
							and Warehouse.[Column] is null
							and [columns].name not in ('FirstDate', 'CreatedDate', 'CreatedBatchKey', 'CreatedBy', 'ModifiedDate', 'ModifiedBy', 'FromDate', 'ToDate' -- Metadata
														, 'ColumnLock' --special field to determin if manually edited
														, 'ChannelID' --CCIA-5791
														, 'SafeAccountNumber' -- is CASE option on AccountNumber
														
														)
							and (Jira.[Status] not in ('Backlog','Ready for work', 'In Progress') or Jira.[Status] is null)
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '165', 'WarehouseFields', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '165', 'WarehouseFields', 'Fail', 'Fields are missing')
End

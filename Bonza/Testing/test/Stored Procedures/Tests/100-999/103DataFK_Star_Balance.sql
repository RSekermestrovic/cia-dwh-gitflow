﻿--Test Code
--exec Test.[103DataFK_Star_Balance] 'ADHOC', '2016-02-03'

Create Procedure [Test].[103DataFK_Star_Balance] @TestRunNo varchar(23), @FromDate datetime

as

Select		Balance.*
into		#Balance
From		[$(Dimensional)].Fact.Balance as Balance with (nolock)
			inner join [$(Dimensional)].Dimension.DayZone as DayZone with (nolock) on Balance.DayKey = DayZone.DayKey
			inner join [$(Dimensional)].Dimension.[Day] as [Day] with (nolock) on DayZone.MasterDayKey = [Day].DayKey
where		[Day].DayDate = Cast (@FromDate as Date)
			or @FromDate is null	


--[AccountKey]
If not exists(
				Select		Balance.DayKey
				From		#Balance as Balance
							left join [$(Dimensional)].Dimension.Account as Account with (nolock) on Balance.AccountKey = Account.AccountKey
				Where		Account.AccountKey is null		
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '103.1', 'DataFK_Fact_Balance Account', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '103.1', 'DataFK_Fact_Balance Account', 'Fail', 'Missing AccountKeys')
End

If			(
				Select		count (*)
				From		#Balance as Balance
				Where		Balance.AccountKey = -1			
			 ) = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '103.2', 'DataFK_Fact_Balance Account', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '103.2', 'DataFK_Fact_Balance Account', 'Fail', 'Missing AccountKeys')
End

--[AccountStatusKey]
Declare @Counter4 int
Set @Counter4 =	(
				Select		Count (*)
				From		#Balance as Balance
				Where		Balance.AccountStatusKey not in (Select AccountStatusKey from [$(Dimensional)].Dimension.AccountStatus with (nolock))		
			 )
If @Counter4 = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '103.3', 'DataFK_Fact_Balance AccountStatus', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-5538'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '103.3', 'DataFK_Fact_Balance AccountStatus', 'Pass', 'CCIA-5538')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '103.3', 'DataFK_Fact_Balance AccountStatus', 'Fail', 'Missing AccountStatusKeys')
	End
End

Declare @Counter5 int
Set @Counter5 =	(
				Select		Count(*)
				From		#Balance as Balance
				Where		Balance.AccountStatusKey = -1				
			 )
If @Counter5 = 0			  
Begin
	Insert into test.tblResults Values (@TestRunNo, '103.4', 'DataFK_Fact_Balance AccountStatus', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-5538'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '103.4', 'DataFK_Fact_Balance AccountStatus', 'Pass', 'CCIA-5538')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '103.4', 'DataFK_Fact_Balance AccountStatus', 'Fail', 'Missing AccountStatusKeys')
	ENd
End

--[LedgerID] n/a
--[LedgerSource] n/a

--[DayKey]
IF @FromDate is null --this test doesn't perform on live volumes due to the size of the Balance table
Begin
	Declare @Counter0 int
	Set @Counter0 =	(
					Select		Count (*)
					From		[$(Dimensional)].Fact.Balance as Balance with (nolock)
								left join [$(Dimensional)].Dimension.[DayZone] as [DayZone] with (nolock) on Balance.DayKey = DayZone.DayKey
					Where		[DayZone].DayKey is null
					)
	If @Counter0 = 0
	Begin
		Insert into test.tblResults Values (@TestRunNo, '103.5', 'DataFK_Fact_Balance Day', 'Pass', Null)
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '103.5', 'DataFK_Fact_Balance Day', 'Fail', 'Missing DayKeys')
	End

	If not exists(
					Select		Balance.DayKey
					From		[$(Dimensional)].Fact.Balance as Balance with (nolock)
					Where		Balance.DayKey = -1			
				 ) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '103.6', 'DataFK_Fact_Balance Day', 'Pass', Null)
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '103.6', 'DataFK_Fact_Balance Day', 'Fail', 'Missing DayKeys')
	End
End

--[BalanceTypeKey]
Declare @CounterBT int
Set @CounterBT =	(
				Select		Count (*)
				From		#Balance as Balance
							left join [$(Dimensional)].Dimension.BalanceType as BalanceType with (nolock) on Balance.BalanceTypeKey = BalanceType.BalanceTypeKey
				Where		BalanceType.BalanceTypeKey is null			
			 )
If @CounterBT = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '103.7', 'DataFK_Fact_Balance BalanceTypeKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '103.7', 'DataFK_Fact_Balance BalanceTypeKey', 'Fail', 'Missing BalanceTypeKey')
End

If not exists(
				Select		Balance.DayKey
				From		#Balance as Balance
				Where		Balance.BalanceTypeKey = -1		
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '103.8', 'DataFK_Fact_Balance BalanceTypeKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '103.8', 'DataFK_Fact_Balance BalanceTypeKey', 'Fail', 'Missing BalanceTypeKey')
End

--[WalletKey]
Declare @CounterW int
Set @CounterW =	(
				Select		Count (*)
				From		#Balance as Balance
							left join [$(Dimensional)].Dimension.Wallet as Wallet with (nolock) on Balance.WalletKey = Wallet.WalletKey
				Where		Wallet.WalletKey is null	
			 )
If @CounterW = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '103.9', 'DataFK_Fact_Balance WalletKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '103.9', 'DataFK_Fact_Balance WalletKey', 'Fail', 'Missing WalletKey')
End

If not exists(
				Select		Balance.DayKey
				From		#Balance as Balance
				Where		Balance.WalletKey = -1	
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '103.10', 'DataFK_Fact_Balance WalletKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '103.10', 'DataFK_Fact_Balance WalletKey', 'Fail', 'Missing WalletKey')
End

--[OpeningBalance] n/a
--[TotalTransactedAmount] n/a
--[ClosingBalance] n/a
--[CreatedDate] n/a
--[CreatedBatchKey] n/a
--[ModifiedDate] n/a
--[ModifiedBatchKey] n/a

--StructureKey
If not exists(
				Select		Balance.DayKey
				From		#Balance as Balance
							left join [$(Dimensional)].Dimension.Structure as Structure with (nolock) on Balance.StructureKey = Structure.StructureKey
				Where		Structure.StructureKey is null		
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '103.11', 'DataFK_Fact_Balance Structure', 'Pass', Null)
End
Else
BEGIN
If not exists(
				Select		Balance.DayKey
				From		#Balance as Balance
							left join [$(Dimensional)].Dimension.Structure as Structure with (nolock) on Balance.StructureKey = Structure.StructureKey
				Where		Structure.StructureKey is NULL
							AND Balance.StructureKey NOT IN (34933, 35326, 822390)	
			 ) 
	BEGIN
		IF exists	(Select		*
					 From		test.tblDefects
					 Where		DefectID = 'CCIA-7511'
								and [Status] != 'Done'
					) 
		Begin
				INSERT into test.tblResults Values (@TestRunNo, '103.11', 'DataFK_Fact_Balance Structure', 'Pass', 'CCIA-7511')
		End
		Else
		Begin
				Insert into test.tblResults Values (@TestRunNo, '103.11', 'DataFK_Fact_Balance Structure', 'Fail', 'Missing StructureKeys')
		End
	END
	ELSE
	Begin		 			
	Insert into test.tblResults Values (@TestRunNo, '103.11', 'DataFK_Fact_Balance Structure', 'Fail', 'Missing StructureKeys')
	End
End

-- the -1 test is not valid, as there is no -1 scenario in this table
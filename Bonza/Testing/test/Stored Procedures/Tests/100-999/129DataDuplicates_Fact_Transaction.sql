﻿--test
--exec [Test].[129DataDuplicates_Fact_Transaction] 'ADHOC', null

CREATE PROCEDURE [Test].[129DataDuplicates_Fact_Transaction] @TestRunNo varchar(23), @FromDate datetime

as


If not exists(
		Select		Count(*)
		From		[$(Dimensional)].Fact.[Transaction] as [Transaction]
		where		@FromDate is null
					or CreatedDate >= @FromDate
		Group by	[DayKey], [TransactionId], [TransactionIdSource], [BalanceTypeKey], [LegTypeKey], TransactionDetailKey, ContractKey, MarketKey, MasterTransactionTimestamp
		Having		Count(*) > 1
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '129', 'DataDuplicates_Fact_Transaction', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '129', 'DataDuplicates_Fact_Transaction', 'Fail', 'Duplicates of Transaction found')
End
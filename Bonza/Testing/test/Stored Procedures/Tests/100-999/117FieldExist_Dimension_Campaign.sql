﻿--Test Code
--exec Test.[117FieldExist_Dimension_Campaign] 'ADHOC'
Create Procedure Test.[117FieldExist_Dimension_Campaign] @TestRunNo varchar(23)

as

Declare @Database varchar(100)
Set @Database = '[$(Dimensional)]'

Declare @SQL varchar(max)
Set @SQL = 'Select top 1 
[CampaignKey]
      ,[CampaignId]
      ,[CampaignName]
      ,[CampaignComment]
      ,[CampaignActive]
      ,[CreatedDayKey]
      ,[CampaignTypeKey]
      ,[CampaignTypeId]
      ,[CampaignTypeName]
      ,[CampaignTypeActive]
      ,[FromDate]
      ,[ToDate]
      ,[FirstDate]
      ,[CreatedDate]
      ,[CreatedBatchKey]
      ,[CreatedBy]
      ,[ModifiedDate]
      ,[ModifiedBatchKey]
      ,[ModifiedBy]
			from ' + @Database + '.[Dimension].[Campaign]'

Exec Test.spSelectValid @TestRunNo, '117','Structure - Dimension Campaign Table', @SQL

If		(
		Select		Count(*)
		From		[$(Dimensional)].sys.all_objects as [Objects]
					inner join [$(Dimensional)].sys.all_columns as [Columns] on [Objects].[object_id] = [Columns].[object_id]
					inner join [$(Dimensional)].sys.schemas as [schemas] on [Objects].[schema_id] = [schemas].[schema_id]
		where		[Objects].name = 'Campaign'
					and [Objects].[type] = 'U'
					and [schemas].Name = 'Dimension'
		) = 19
Begin
	Insert into test.tblResults Values (@TestRunNo, '117.1', 'Structure - Dimension Campaign Table', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '117.1', 'Structure - Dimension Campaign Table', 'Fail', 'Wrong number of columns')
End
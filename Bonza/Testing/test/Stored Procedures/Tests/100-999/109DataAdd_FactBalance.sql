﻿--Test Code
--exec Test.[109DataAdd_FactBalance] 'Adhoc'
Create Procedure [Test].[109DataAdd_FactBalance] @TestRunNo varchar(23)

as

Declare @LastBatch int
Set @LastBatch = (Select Max([BatchKey]) From [$(Staging)].[Control].[ETLController] where BatchStatus = 1)

Select	*
into	#BatchDetails
From	[$(Staging)].[Control].[ETLController]
Where	[BatchKey] = @LastBatch

If	(
	Select	BalanceFactRowsProcessed
	From	#BatchDetails
	) = 0
Begin
	Insert into test.tblResults ([RunID], [TestID], [TestName], [Status], [Notes]) Values (@TestRunNo, '109', 'DataAdd_FactBalance', 'Pass', 'No records were loaded in the batch')
End
else
Begin
	Declare @BatchToDate datetime2
	Set @BatchToDate = (
						Select	BatchToDate
						From		#BatchDetails	
						)

	Declare @DayDate datetime2(3)
	Set @DayDate = (
						select	top 1 CONVERT(datetime2(3),CONVERT(date,CorrectedDate))
						from	#BatchDetails
								Inner Join [$(Acquisition)].Intrabet.Latency ON OriginalDate	<= @BatchToDate
						order by OriginalDate desc
						)

	Declare @DayFromDate datetime2(3)
	Set @DayFromDate = (
						select	top 1 CorrectedDate
						from	[$(Acquisition)].Intrabet.Latency l
						where CorrectedDate	<= @DayDate
								and OriginalDate > DATEADD(day,-1,@DayDate)
						order by CorrectedDate desc
						)

	Declare @Acquisition money
	Set @Acquisition = 
		(
		Select  Sum (Balance) 
		From	[$(Acquisition)].IntraBet.tblAccounts as tblAccounts
		where	FromDate <= @DayFromDate
				and ToDate > @DayFromDate
		)
	Declare @Fact money
	Set @Fact = 
		(
		select		Sum (OpeningBalance)
		from		[$(Dimensional)].fact.Balance as Balance with (nolock)
					inner join [$(Dimensional)].Dimension.DayZone as DayZone on Balance.DayKey = DayZone.DayKey
					inner join [$(Dimensional)].Dimension.[Day] as [Day] on DayZone.MasterDayKey = [Day].DayKey
					inner join [$(Dimensional)].Dimension.BalanceType as t on t.BalanceTypeKey = Balance.BalanceTypeKey
					inner join [$(Dimensional)].Dimension.Wallet as w on w.WalletKey = Balance.WalletKey
		where		[Day].DayDate = @DayDate
					and t.BalanceTypeID = 'CLI'
					and w.WalletId = 'C'
		)

If @Acquisition = @Fact
	Begin
		Insert into test.tblResults ([RunID], [TestID], [TestName], [Status], [Notes]) Values (@TestRunNo, '109', 'DataCount_FactBalance', 'Pass', Null)
	End
	Else
	Begin
		If exists	(Select		*
					From		test.tblDefects
					Where		DefectID = 'CCIA-5369'
								and [Status] != 'Done'
				) 
		Begin
			Insert into test.tblResults ([RunID], [TestID], [TestName], [Status], [Notes]) Values (@TestRunNo, '109', 'DataCount_FactBalance', 'Pass', 'CCIA-5369')
		End
		Else
		Begin
			Insert into test.tblResults ([RunID], [TestID], [TestName], [Status], [Notes]) Values (@TestRunNo, '109', 'DataCount_FactBalance', 'Fail', cast (@Acquisition as varchar(20)) + ' not ' + Cast (@Fact as varchar(20)))
		End
	End

End
﻿--test
--exec [Test].[136DataDuplicates_Dimension_RevenueLifecycle] 'ADHOC', null

CREATE PROCEDURE [Test].[136DataDuplicates_Dimension_RevenueLifecycle] @TestRunNo varchar(23)

as


If not exists(
		Select		Count(*)
		From		[$(Dimensional)].Dimension.RevenueLifecycle as Lifecycle
		Group by	RevenueLifecycle
		Having		Count(*) > 1
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '136', 'DataDuplicates_Dimension_RevenueLifecycle', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '136', 'DataDuplicates_Dimension_RevenueLifecycle', 'Fail', 'Duplicates of Lifecycle found')
End

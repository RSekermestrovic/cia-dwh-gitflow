--Test Code
--exec Test.[151FieldExists_Dimension_BetLeg] 'ADHOC'
Create Procedure Test.[151FieldExists_Dimension_BetLeg] @TestRunNo varchar(23)

as

Declare @Database varchar(100)
Set @Database = '[$(Dimensional)]'

Declare @SQL varchar(max)
Set @SQL = 'Select top 1 
[ContractKey]
      ,[LegNumber]
      ,[NumberOfLegs]
      ,[WeightingFactor]
      ,[BetTypeKey]
      ,[ClassKey]
      ,[FromDate]
      ,[ToDate]
      ,[FirstDate]
      ,[CreatedDate]
      ,[CreatedBatchKey]
      ,[CreatedBy]
      ,[ModifiedDate]
      ,[ModifiedBatchKey]
      ,[ModifiedBy]
			from ' + @Database + '.[Dimension].[BetLeg]'

Exec Test.spSelectValid @TestRunNo, '151','Structure - Dimension BetLeg Table', @SQL

If		(
		Select		Count(*)
		From		[$(Dimensional)].sys.all_objects as [Objects]
					inner join [$(Dimensional)].sys.all_columns as [Columns] on [Objects].[object_id] = [Columns].[object_id]
					inner join [$(Dimensional)].sys.schemas as [schemas] on [Objects].[schema_id] = [schemas].[schema_id]
		where		[Objects].name = 'BetLeg'
					and [Objects].[type] = 'U'
					and [schemas].Name = 'Dimension'
		) = 15
Begin
	Insert into test.tblResults Values (@TestRunNo, '151.1', 'Structure - Dimension BetLeg Table', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '151.1', 'Structure - Dimension BetLeg Table', 'Fail', 'Wrong number of columns')
End
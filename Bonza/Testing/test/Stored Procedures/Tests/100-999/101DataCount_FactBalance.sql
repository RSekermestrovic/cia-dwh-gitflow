﻿--Test Code
--exec Test.[101DataCount_FactBalance] 'Adhoc'
Create Procedure [Test].[101DataCount_FactBalance] @TestRunNo varchar(23)

as

/*
Declare @LastBatch int
Set @LastBatch = (Select Max([BatchKey]) From [$(Staging)].[Control].[ETLController] where BatchStatus = 1)

Select	*
into	#BatchDetails
From	[$(Staging)].[Control].[ETLController]
Where	[BatchKey] = @LastBatch

If	(
	Select	BalanceFactRowsProcessed
	From	#BatchDetails
	) = 0
Begin
	Insert into test.tblResults ([RunID], [TestID], [TestName], [Status], [Notes]) Values (@TestRunNo, '101', 'DataCount_FactBalance', 'Fail', 'No Balance records were loaded in the batch')
End
else
Begin

	Declare @BatchToDate datetime2
	Set @BatchToDate = (
						Select	BatchToDate
						From		#BatchDetails	
						)

	Declare @Acquisition int
	Set @Acquisition = 
		(
		Select  Count(*) * 12
		From	(
				select distinct (AccountID)
				from [$(Acquisition)].IntraBet.tblAccounts with (nolock)
				where FromDate <= @BatchToDate
				) as Data 
		)
	Declare @Fact int
	Set @Fact = 
		(
		select		count (*)
		from		[$(Dimensional)].fact.Balance as Balance with (nolock)
					inner join [$(Dimensional)].Dimension.DayZone as DayZone with (nolock) on Balance.DayKey = DayZone.DayKey
					inner join [$(Dimensional)].Dimension.[Day] as [Day] with (nolock) on DayZone.MasterDayKey = [Day].DayKey
		where		[Day].DayDate = Cast (@BatchToDate as Date)
		)
	If @Acquisition = @Fact
	Begin
		Insert into test.tblResults ([RunID], [TestID], [TestName], [Status], [Notes]) Values (@TestRunNo, '101', 'DataCount_FactBalance', 'Pass', Null)
	End
	Else
	Begin
		Insert into test.tblResults ([RunID], [TestID], [TestName], [Status], [Notes]) Values (@TestRunNo, '101', 'DataCount_FactBalance ', 'Fail', cast (@Fact as varchar(20)) + ' should be ' + Cast (@Acquisition as varchar(20)))
	End
End

*/
﻿--Test Code
--exec [Test].[123DataDefault_Dimension_AccountStatus] 'ADHOC'
Create Procedure [Test].[123DataDefault_Dimension_AccountStatus] @TestRunNo varchar(23)

as
If exists(
		Select		AccountStatusKey
		From		[$(Dimensional)].[Dimension].AccountStatus
		where		AccountStatusKey = 0
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '123', 'DataDefault_Dimension_AccountStatus Table ID0', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-5544'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '123', 'DataDefault_Dimension_AccountStatus Table ID0', 'Pass', 'CCIA-5544')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '123', 'DataDefault_Dimension_AccountStatus Table ID0', 'Fail', 'No ID 0 record found')
	End
End

If exists(
		Select		AccountStatusKey
		From		[$(Dimensional)].[Dimension].AccountStatus
		where		AccountStatusKey = -1
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '123.1', 'DataDefault_Dimension_AccountStatus Table ID-1', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-5544'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '123.1', 'DataDefault_Dimension_AccountStatus Table ID-1', 'Pass', 'CCIA-5544')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '123.1', 'DataDefault_Dimension_AccountStatus Table ID-1', 'Fail', 'No ID -1 record found')
	End
End




﻿--Test Code
--exec [Test].[127DataDefault_Dimension_LegType] 'ADHOC'
Create Procedure [Test].[127DataDefault_Dimension_LegType] @TestRunNo varchar(23)

as
If exists(
		Select		LegTypeKey
		From		[$(Dimensional)].[Dimension].LegType
		where		LegTypeKey = 0
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '127', 'DataDefault_Dimension_LegType Table ID0', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '127', 'DataDefault_Dimension_LegType Table ID0', 'Fail', 'No ID 0 record found')
End

If exists(
		Select		LegTypeKey
		From		[$(Dimensional)].[Dimension].LegType
		where		LegTypeKey = -1
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '127.1', 'DataDefault_Dimension_LegType Table ID-1', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '127.1', 'DataDefault_Dimension_LegType Table ID-1', 'Fail', 'No ID -1 record found')
End

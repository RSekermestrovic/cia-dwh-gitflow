﻿--Test Code
--exec Test.[107DataValid_Fact_Balance] 'ADHOC'
Create Procedure [Test].[107DataValid_Fact_Balance] @TestRunNo varchar(23), @FromDate datetime

as

Declare @FromDate2 date
Declare @DayKey char(8)
Declare @FromDayKey int
Declare @ToDayKey int

If @FromDate is not null
Begin
	Set @FromDate2 = @FromDate
	Set @DayKey = (select DayKey from [$(Dimensional)].Dimension.[Day] where DayDate = @FromDate2)
	Set @FromDayKey = @DayKey + '00'
	Set @ToDayKey = @DayKey + '03'
End

--AccountKey
--n/a

--AccountStatusKey
IF 	not exists		(
					Select		*
					From		[$(Dimensional)].Fact.[Balance] as Balance with (nolock) 
								inner join [$(Dimensional)].Dimension.AccountStatus as AccountStatus with (nolock) on Balance.AccountStatusKey = AccountStatus.AccountStatusKey
								left join [$(Dimensional)].Dimension.Account as Account with (nolock) on AccountStatus.AccountKey = Account.AccountKey
					where		DayKey not in (0, -1)
								and Balance.AccountKey != Account.AccountKey
								and ((Balance.DayKey between @FromDayKey and @ToDayKey)
								or (@FromDate is null))
					)
Begin
	Insert into Test.tblResults Values (@TestRunNo, '107', 'DataValid_Fact_Balance AccountID', 'Pass', Null)
End
Else
Begin
	Insert into Test.tblResults Values (@TestRunNo, '107', 'DataValid_Fact_Balance AccountID', 'Fail', 'AccountID/AccountKey mismatch')
End

--AccountID/LedgerID
IF 	not exists		(
					Select		*
					From		[$(Dimensional)].Fact.[Balance] as Balance with (nolock) 
								inner join [$(Dimensional)].Dimension.Account as Account with (nolock) on Balance.AccountKey = Account.AccountKey 
					where		DayKey not in (0, -1)
								and Balance.LedgerID != Account.LedgerID
								and ((Balance.DayKey between @FromDayKey and @ToDayKey)
								or (@FromDate is null))
					)
Begin
	Insert into Test.tblResults Values (@TestRunNo, '107.1', 'DataValid_Fact_Balance AccountID', 'Pass', Null)
End
Else
Begin
	Insert into Test.tblResults Values (@TestRunNo, '107.1', 'DataValid_Fact_Balance AccountID', 'Fail', 'AccountID/AccountKey mismatch')
End

--AccountSourceID/LedgerSource
IF 	not exists		(
					Select		*
					From		[$(Dimensional)].Fact.[Balance] as Balance with (nolock) 
								inner join [$(Dimensional)].Dimension.Account as Account with (nolock) on Balance.AccountKey = Account.AccountKey 
					where		DayKey not in (0, -1)
								and Balance.LedgerSource != Account.LedgerSource
								and ((Balance.DayKey between @FromDayKey and @ToDayKey)
								or (@FromDate is null))
					)
Begin
	Insert into Test.tblResults Values (@TestRunNo, '107.2', 'DataValid_Fact_Balance AccountID', 'Pass', Null)
End
Else
Begin
	Insert into Test.tblResults Values (@TestRunNo, '107.2', 'DataValid_Fact_Balance AccountID', 'Fail', 'LedgerSource/AccountSourceID mismatch')
End

--DayKey
IF 	not exists		(
					Select		top 1 *
					From		[$(Dimensional)].Fact.[Balance] with (nolock)
					where		DayKey = -1
					)
Begin
	Insert into Test.tblResults Values (@TestRunNo, '107.3', 'DataValid_Fact_Balance -1Keys - DayKey', 'Pass', Null)
End
Else
Begin
	Insert into Test.tblResults Values (@TestRunNo, '107.3', 'DataValid_Fact_Balance -1Keys - DayKey', 'Fail', 'There are links to the -1 Unknown Key')
End

--BalanceTypeKey
IF 	not exists		(
					Select		top 1 *
					From		[$(Dimensional)].Fact.[Balance] with (nolock)
					where		BalanceTypeKey = -1
								and ((Balance.DayKey between @FromDayKey and @ToDayKey)
								or (@FromDate is null))
					)
Begin
	Insert into Test.tblResults Values (@TestRunNo, '107.4', 'DataValid_Fact_Balance -1Keys - BalanceTypeKey', 'Pass', Null)
End
Else
Begin
	Insert into Test.tblResults Values (@TestRunNo, '107.4', 'DataValid_Fact_Balance -1Keys - BalanceTypeKey', 'Fail', 'There are links to the -1 Unknown Key')
End

--WalletKey
IF 	not exists		(
					Select		top 1 *
					From		[$(Dimensional)].Fact.[Balance] with (nolock)
					where		WalletKey = -1
								and ((Balance.DayKey between @FromDayKey and @ToDayKey)
								or (@FromDate is null))
					)
Begin
	Insert into Test.tblResults Values (@TestRunNo, '107.5', 'DataValid_Fact_Balance -1Keys - WalletKey', 'Pass', Null)
End
Else
Begin
	Insert into Test.tblResults Values (@TestRunNo, '107.5', 'DataValid_Fact_Balance -1Keys - WalletKey', 'Fail', 'There are links to the -1 Unknown Key')
End

--Opening Balance
--n/a as covered by cross reference tests

--TotalTransactedAmount & ClosingBalance
IF 	not exists		(
					select		top 1 * 
					from		[$(Dimensional)].fact.[balance] with (nolock)
					where		[ClosingBalance] - [OpeningBalance] != [TotalTransactedAmount]
								and ((Balance.DayKey between @FromDayKey and @ToDayKey)
								or (@FromDate is null))
					)
Begin
	Insert into Test.tblResults Values (@TestRunNo, '107.6', 'DataValid_Fact_Balance - OpeningBalance, TotalTransactedAmount & ClosingBalance Triangulation', 'Pass', Null)
End
Else
Begin
	Insert into Test.tblResults Values (@TestRunNo, '107.6', 'DataValid_Fact_Balance - OpeningBalance, TotalTransactedAmount & ClosingBalance', 'Fail', 'There is a miscalculation in the trinagulation')
End

--Metadata n/a

--StructureKey
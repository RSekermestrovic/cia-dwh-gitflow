﻿--Test Code
--exec Test.[112BVA_Fact_Balance] 'ADHOC', null
--exec Test.[112BVA_Fact_Balance] 'ADHOC', '2016-05-04'

Create Procedure [Test].[112BVA_Fact_Balance] @TestRunNo varchar(23), @FromDate datetime

as

Declare @DayKey int
Declare @DayZoneKey1 char(10)
Declare @DayZoneKey2 char(10)
Declare @DayZone1 int
Declare @DayZone2 int

If @FromDate is not null
Begin
	Set @DayKey = (Select DayKey From [$(Dimensional)].Dimension.[Day] where daydate = @FromDate)
	set @DayZoneKey1 = Cast (@DayKey as char(8)) + '02'
	set @DayZoneKey2 = Cast (@DayKey as char(8)) + '03'
	set @DayZone1 = cast (@DayZoneKey1 as int)
	set @DayZone2 = cast (@DayZoneKey2 as int)
End

--AccountKey n/a FK
--AccountStatusKey n/a FK
--LedgerID
If not exists  (
				Select		AccountKey
				From		[$(Dimensional)].Fact.Balance as Balance
							inner join [$(Dimensional)].Dimension.DayZone as DayZone on Balance.DayKey = [DayZone].DayKey
				Where		(
							[DayZone].DayKey >= @DayZone1
							Or 
							[DayZone].DayKey >= @DayZone2
							or
							@FromDate is null
							)
							and LedgerID < 0
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '112.0', 'DataValid_Fact_Balance LedgerID', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '112.0', 'DataValid_Fact_Balance LedgerID', 'Fail', 'Invalid LedgerID')
End

--LedgerSource
If not exists  (
				Select		AccountKey
				From		[$(Dimensional)].Fact.Balance
							inner join [$(Dimensional)].Dimension.DayZone as DayZone on Balance.DayKey = [DayZone].DayKey
				Where		(
							[DayZone].DayKey >= @DayZone1
							or
							[DayZone].DayKey >= @DayZone2
							Or 
							@FromDate is null
							)
							and LedgerSource != 'IAA'
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '112.1', 'DataValid_Fact_Balance LedgerSource', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '112.1', 'DataValid_Fact_Balance LedgerSource', 'Fail', 'Invalid LedgerSource')
End

--DayKey n/a FK
--BalanceTypeKey n/a FK
--WalletKey n/a FK
--OpeningBalance n/a negative and positive acceptable
--TotalTransactedAmount  n/a negative and positive acceptable
--ClosingBalance n/a negative and positive acceptable

--Metadata n/a
--CreatedDate
--CreatedBatchKey
--ModifiedDate
--ModifiedBatchKey

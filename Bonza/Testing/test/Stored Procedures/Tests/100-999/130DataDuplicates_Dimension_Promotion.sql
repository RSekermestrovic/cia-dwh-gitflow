﻿--test
--exec [Test].[130DataDuplicates_Dimension_Promotion] 'ADHOC', null

CREATE PROCEDURE [Test].[130DataDuplicates_Dimension_Promotion] @TestRunNo varchar(23), @FromDate datetime

as


If not exists(
		Select		Count(*)
		From		[$(Dimensional)].Dimension.Promotion as Promotion
		where		@FromDate is null
					or CreatedDate >= @FromDate
		Group by	[PromotionId], [Source]
		Having		Count(*) > 1
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '130', 'DataDuplicates_Dimension_Promotion', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '130', 'DataDuplicates_Dimension_Promotion', 'Fail', 'Duplicates of Promotion found')
End

﻿--Test Code
--exec Test.[118DataCurrent_Dimension_AccountStatus] 'ADHOC'
Create Procedure Test.[118DataCurrent_Dimension_AccountStatus] @TestRunNo varchar(23)

as

Declare	@BatchDate datetime2
Set	@BatchDate = (Select Max(BatchFromDate) From [$(Staging)].[Control].ETLController Where DimensionStatus = 1)

Declare @LastDate varchar(10)
Set		@LastDate = 
					(Select		max (CreatedDate)
					From		[$(Dimensional)].[Dimension].[AccountStatus]
					)

If @LastDate >= @BatchDate or @LastDate >= DateAdd (Day,-1,getdate())
Begin
	Insert into test.tblResults Values (@TestRunNo, '118', 'DataCurrent_Dimension_AccountStatus Table', 'Pass', Null)
End
Else
Begin
	
	Insert into test.tblResults Values (@TestRunNo, '118', 'DataCurrent_Dimension_AccountStatus Table', 'Fail', 'The last update in the dimension is ' + cast (@LastDate as varchar(10)) + ' expect an update each day')
End
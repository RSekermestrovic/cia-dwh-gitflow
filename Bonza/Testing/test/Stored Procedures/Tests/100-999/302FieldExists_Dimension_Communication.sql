--Test Code
--exec Test.[302FieldExists_Dimension_Communication] 'ADHOC'
Create Procedure Test.[302FieldExists_Dimension_Communication] @TestRunNo varchar(23)

as

Declare @Database varchar(100)
Set @Database = '[$(Dimensional)]'

Declare @SQL varchar(max)
Set @SQL = 'Select top 1 
			  [CommunicationKey]
			  ,[CommunicationId]
			  ,[CommunicationSource]
			  ,[Title]
			  ,[Details]
			  ,[SubDetails]
			  --, FirstDate CCIA-6647
			  --, FromDate CCIA-6647
			  --, ToDate CCIA-6647
			  ,[CreatedDate]
			  ,[CreatedBatchKey]
			  ,[CreatedBy]
			  ,[ModifiedDate]
			  ,[ModifiedBatchKey]
			  ,[ModifiedBy]
			from ' + @Database + '.[Dimension].[Communication]'

Exec Test.spSelectValid @TestRunNo, '302','Structure - Dimension Communication Table', @SQL

If		(
		Select		Count(*)
		From		[$(Dimensional)].sys.all_objects as [Objects]
					inner join [$(Dimensional)].sys.all_columns as [Columns] on [Objects].[object_id] = [Columns].[object_id]
					inner join [$(Dimensional)].sys.schemas as [schemas] on [Objects].[schema_id] = [schemas].[schema_id]
		where		[Objects].name = 'Communication'
					and [Objects].[type] = 'U'
					and [schemas].Name = 'Dimension'
		) = 12
Begin
	Insert into test.tblResults Values (@TestRunNo, '302.1', 'Structure - Dimension Communication Table', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '302.1', 'Structure - Dimension Communication Table', 'Fail', 'Wrong number of columns')
End



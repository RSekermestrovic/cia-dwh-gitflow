﻿--Test Code
--exec Test.[311BVA_Dimension_Story] 'ADHOC', '2015-04-03'

Create Procedure Test.[311BVA_Dimension_Story] @TestRunNo varchar(23), @FromDate datetime

as

Declare @ErrorText varchar(100)
If @FromDate is not null
Begin
Set @ErrorText = ' since ' + Convert (varchar(20), @FromDate, 103)
End
else
Begin
Set @ErrorText = '' 
End

Select		* 
into		#Data
From		[$(Dimensional)].Dimension.Story
where		[StoryKey] not in (0, -1)
			--and ((FromDate > @FromDate)
			--		or (@FromDate is null))
			--and ToDate = '9999-12-31'

--StoryKey n/a
--[Story]
If not exists(
				Select		*
				From		#Data
				where		Story = ''
							or Story is null
						
			) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '311.0', 'BVA_Dimension_Story Story', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '311.0', 'BVA_Dimension_Story Story', 'Fail', 'There are records with an invalid Story' + @ErrorText)
End

--CreatedDate n/a
--CreatedBatchKey n/a
--CreatedBy n/a
--ModifiedDate n/a
--ModifiedBatchKey n/a
--ModifiedBy n/a
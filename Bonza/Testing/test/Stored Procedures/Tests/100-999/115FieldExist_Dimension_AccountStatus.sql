﻿--Test Code
--exec Test.[115FieldExists_Dimension_AccountStatus] 'ADHOC'
Create Procedure Test.[115FieldExists_Dimension_AccountStatus] @TestRunNo varchar(23)

as

Declare @Database varchar(100)
Set @Database = '[$(Dimensional)]'

Declare @SQL varchar(max)
Set @SQL = 'Select top 1 
		   [AccountStatusKey]
      ,[AccountKey]
      ,[LedgerID]
      ,[LedgerSource]
      ,[LedgerEnabled]
      ,[LedgerStatus]
      ,[AccountType]
      ,[InternetProfile]
      ,[VIP]
	  ,[CompanyKey]
      ,[CreditLimit]
      ,[MinBetAmount]
      ,[MaxBetAmount]
      ,[Introducer]
      ,[Handled]
      ,[PhoneColourDesc]
      ,[DisableDeposit]
      ,[DisableWithdrawal]
      ,[AccountClosedByUserID]
      ,[AccountClosedByUserSource]
      ,[AccountClosedByUserName]
      ,[AccountClosedDate]
      ,[ReIntroducedBy]
      ,[ReIntroducedDate]
      ,[AccountClosureReason]
      ,[DepositLimit]
      ,[LossLimit]
      ,[LimitPeriod]
      ,[LimitTimeStamp]
      ,[ReceiveMarketingEmail]
      ,[ReceiveCompetitionEmail]
      ,[ReceiveSMS]
      ,[ReceivePostalMail]
      ,[ReceiveFax]
      ,[CurrentVersion]
      ,[FromDate]
      ,[ToDate]
      ,[FirstDate]
      ,[CreatedDate]
      ,[CreatedBatchKey]
      ,[CreatedBy]
      ,[ModifiedDate]
      ,[ModifiedBatchKey]
      ,[ModifiedBy]
			from ' + @Database + '.[Dimension].[AccountStatus]'

Exec Test.spSelectValid @TestRunNo, '115','Structure - Dimension Account Status Table', @SQL

If		(
		Select		Count(*)
		From		[$(Dimensional)].sys.all_objects as [Objects]
					inner join [$(Dimensional)].sys.all_columns as [Columns] on [Objects].[object_id] = [Columns].[object_id]
					inner join [$(Dimensional)].sys.schemas as [schemas] on [Objects].[schema_id] = [schemas].[schema_id]
		where		[Objects].name = 'AccountStatus'
					and [Objects].[type] = 'U'
					and [schemas].Name = 'Dimension'
		) = 44
Begin
	Insert into test.tblResults Values (@TestRunNo, '115.1', 'Structure - Dimension AccountStatus Table', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '115.1', 'Structure - Dimension AccountStatus Table', 'Fail', 'Wrong number of columns')
End
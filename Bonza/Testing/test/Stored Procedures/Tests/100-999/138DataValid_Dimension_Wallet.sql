﻿--Test Code
--exec Test.[138DataValid_Dimension_Wallet] 'ADHOC'
Create Procedure Test.[138DataValid_Dimension_Wallet] @TestRunNo varchar(23)

as


If		  (
			Select		Count(*)
			From		[$(Dimensional)].[Dimension].Wallet
			where		(WalletId = 'U' and WalletName = 'Unknown')
						or (WalletId = 'N' and WalletName = 'N/A')
						or (WalletId = 'C' and WalletName = 'Cash')
						or (WalletId = 'B' and WalletName = 'Bonus')
			) = 4
Begin
	Insert into test.tblResults Values (@TestRunNo, '138', 'DataValid_Dimension_Wallet', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '138', 'DataValid_Dimension_Wallet', 'Fail', 'Records Missing or Incorrect')
End

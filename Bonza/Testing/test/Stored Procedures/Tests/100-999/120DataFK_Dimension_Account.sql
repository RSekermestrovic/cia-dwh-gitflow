﻿--Test Code
--exec Test.[120DataFK_Dimension_Account] 'ADHOC'

Create Procedure Test.[120DataFK_Dimension_Account] @TestRunNo varchar(23)

as

If not exists(
		Select		AccountKey
		From		[$(Dimensional)].Dimension.Account
		Where		CompanyKey = -1
					and AccountKey != -1
					--CCIA-4746 has flagged accounts which Intrabet does not state a company code for
					and (AccountID not in (779221,777467,825076,1260760,1268344,816926,783590,777651, -1))
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '120', '120DataFK_Dimension_Account Company', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-4746'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '120', '120DataFK_Dimension_Account Company', 'Pass', 'CCIA-4746')
	End
	Else
	Begin
			Insert into test.tblResults Values (@TestRunNo, '120', '120DataFK_Dimension_Account Company', 'Fail', 'Missing CompanyKeys')
	End
End

If not exists(
		Select		AccountStatus.AccountStatusKey
		From		[$(Dimensional)].Dimension.AccountStatus as AccountStatus
					inner join [$(Dimensional)].Dimension.Account as Account on Account.AccountKey = AccountStatus.AccountKey
		Where		AccountStatus.CompanyKey = -1
					and AccountStatus.AccountStatusKey != -1
					--CCIA-4746 has flagged accounts which Intrabet does not state a company code for
					and (Account.AccountID not in (779221,777467,825076,1260760,1268344,816926,783590,777651, -1))
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '120a', '120DataFK_Dimension_AccountStatus Company', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-4746'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '120a', '120DataFK_Dimension_AccountStatus Company', 'Pass', 'CCIA-4746')
	End
	Else
	Begin
			Insert into test.tblResults Values (@TestRunNo, '120a', '120DataFK_Dimension_AccountStatus Company', 'Fail', 'Missing CompanyKeys')
	End
End


If not exists(
		Select		AccountKey
		From		[$(Dimensional)].Dimension.Account
		Where		AccountOpenedDayKey = -1
					and AccountKey != -1
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '120.1', '120DataFK_Dimension_Account AccountOpenedDayKey', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-5286'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '120.1', '120DataFK_Dimension_Account AccountOpenedDayKey', 'Pass', 'CCIA-5286')
	End
	Else
	Begin
			Insert into test.tblResults Values (@TestRunNo, '120.1', '120DataFK_Dimension_Account AccountOpenedDayKey', 'Fail', 'Missing AccountOpenedDayKey')
	End
End
﻿--Test Code
--exec Test.[161FieldMappings_Fact_Position] 'ADHOC'
Create Procedure Test.[161FieldMappings_Fact_Position] @TestRunNo varchar(23)

as

Declare @Counter int

--BalanceTypeKey n/a as Key
--AccountKey n/a as Key
--AccountStatusKey n/a as Key
--LedgerID n/a as ID field
--LedgerSource n/a as ID field
--DayKey n/a as Key

--ActivityKey
	--Has Bet
	Set @Counter = (
					(
						Select		Count (*)
						From		[$(Dimensional)].[Fact].[Position] as Position
									Inner join [$(Dimensional)].[Dimension].Activity as Activity on Position.ActivityKey = Activity.ActivityKey
									Inner join [$(Dimensional)].[Dimension].DayZone on Position.DayKey = DayZone.DayKey							
									left join	(Select		[Transaction].AccountKey
															, DayZone.MasterDayKey
													From	[$(Dimensional)].[Fact].[Transaction] as [Transaction]
															Inner join [$(Dimensional)].[Dimension].TransactionDetail as TDetail on [Transaction].TransactionDetailKey = TDetail.TransactionDetailKey
															Inner join [$(Dimensional)].[Dimension].DayZOne on [Transaction].DayKey = DayZone.DayKey
													Where	TDetail.TransactionTypeID = 'BT'
															and TDetail.TransactionStatusID = 'RE'
													Group by AccountKey
															, DayZone.MasterDayKey
												) as Transactions on Position.AccountKey = Transactions.AccountKey
																and DayZone.MasterDayKey = Transactions.MasterDayKey
						Where		Right (Position.DayKey,2) = 02
									and ((Activity.HasBet = 'Yes' and Transactions.AccountKey is null)
										Or (Activity.HasBet = 'No' and Transactions.AccountKey is not null))
					)
					+
						(
						Select		Count (*)
						From		[$(Dimensional)].[Fact].[Position] as Position
									Inner join [$(Dimensional)].[Dimension].Activity as Activity on Position.ActivityKey = Activity.ActivityKey
									Inner join [$(Dimensional)].[Dimension].DayZone on Position.DayKey = DayZone.DayKey							
									left join	(Select		[Transaction].AccountKey
															, DayZone.AnalysisDayKey
													From	[$(Dimensional)].[Fact].[Transaction] as [Transaction]
															Inner join [$(Dimensional)].[Dimension].TransactionDetail as TDetail on [Transaction].TransactionDetailKey = TDetail.TransactionDetailKey
															Inner join [$(Dimensional)].[Dimension].DayZOne on [Transaction].DayKey = DayZone.DayKey
													Where	TDetail.TransactionTypeID = 'BT'
															and TDetail.TransactionStatusID = 'RE'
													Group by AccountKey
															, DayZone.AnalysisDayKey
												) as Transactions on Position.AccountKey = Transactions.AccountKey
																and DayZone.AnalysisDayKey = Transactions.AnalysisDayKey
						Where		Right (Position.DayKey,2) = 03
									and ((Activity.HasBet = 'Yes' and Transactions.AccountKey is null)
										Or (Activity.HasBet = 'No' and Transactions.AccountKey is not null))
						)
					) 
	If @Counter = 0
	Begin
		Insert into test.tblResults Values (@TestRunNo, '161', 'DataValid_Fact_Position ActivityKey - Has Bet', 'Pass', Null)
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '161', 'DataValid_Fact_Position ActivityKey - Has Bet', 'Fail', 'ActivityKey is incorrect')
	End

	--Has Deposited
	Set @Counter = (
					(
						Select		Count (*)
						From		[$(Dimensional)].[Fact].[Position] as Position
									Inner join [$(Dimensional)].[Dimension].Activity as Activity on Position.ActivityKey = Activity.ActivityKey
									Inner join [$(Dimensional)].[Dimension].DayZone on Position.DayKey = DayZone.DayKey							
									left join	(Select		[Transaction].AccountKey
															, DayZone.MasterDayKey
													From	[$(Dimensional)].[Fact].[Transaction] as [Transaction]
															Inner join [$(Dimensional)].[Dimension].TransactionDetail as TDetail on [Transaction].TransactionDetailKey = TDetail.TransactionDetailKey
															Inner join [$(Dimensional)].[Dimension].DayZOne on [Transaction].DayKey = DayZone.DayKey
													Where	TDetail.TransactionTypeID = 'DP'
															and TDetail.TransactionStatusID = 'CO'
													Group by AccountKey
															, DayZone.MasterDayKey
												) as Transactions on Position.AccountKey = Transactions.AccountKey
																and DayZone.MasterDayKey = Transactions.MasterDayKey
						Where		Right (Position.DayKey,2) = 02
									and ((Activity.HasDeposited = 'Yes' and Transactions.AccountKey is null)
										Or (Activity.HasDeposited = 'No' and Transactions.AccountKey is not null))
					)
					+
						(
						Select		Count (*)
						From		[$(Dimensional)].[Fact].[Position] as Position
									Inner join [$(Dimensional)].[Dimension].Activity as Activity on Position.ActivityKey = Activity.ActivityKey
									Inner join [$(Dimensional)].[Dimension].DayZone on Position.DayKey = DayZone.DayKey							
									left join	(Select		[Transaction].AccountKey
															, DayZone.AnalysisDayKey
													From	[$(Dimensional)].[Fact].[Transaction] as [Transaction]
															Inner join [$(Dimensional)].[Dimension].TransactionDetail as TDetail on [Transaction].TransactionDetailKey = TDetail.TransactionDetailKey
															Inner join [$(Dimensional)].[Dimension].DayZOne on [Transaction].DayKey = DayZone.DayKey
													Where	TDetail.TransactionTypeID = 'DP'
															and TDetail.TransactionStatusID = 'CO'
													Group by AccountKey
															, DayZone.AnalysisDayKey
												) as Transactions on Position.AccountKey = Transactions.AccountKey
																and DayZone.AnalysisDayKey = Transactions.AnalysisDayKey
						Where		Right (Position.DayKey,2) = 03
									and ((Activity.HasDeposited = 'Yes' and Transactions.AccountKey is null)
										Or (Activity.HasDeposited = 'No' and Transactions.AccountKey is not null))
						)
					) 
	If @Counter = 0
	Begin
		Insert into test.tblResults Values (@TestRunNo, '161.1', 'DataValid_Fact_Position ActivityKey - Has Deposited', 'Pass', Null)
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '161.1', 'DataValid_Fact_Position ActivityKey - Has Deposited', 'Fail', 'ActivityKey is incorrect')
	End
	--Has Withdrawn
		Set @Counter = (
					(
						Select		Count (*)
						From		[$(Dimensional)].[Fact].[Position] as Position
									Inner join [$(Dimensional)].[Dimension].Activity as Activity on Position.ActivityKey = Activity.ActivityKey
									Inner join [$(Dimensional)].[Dimension].DayZone on Position.DayKey = DayZone.DayKey							
									left join	(Select		[Transaction].AccountKey
															, DayZone.MasterDayKey
													From	[$(Dimensional)].[Fact].[Transaction] as [Transaction]
															Inner join [$(Dimensional)].[Dimension].TransactionDetail as TDetail on [Transaction].TransactionDetailKey = TDetail.TransactionDetailKey
															Inner join [$(Dimensional)].[Dimension].DayZOne on [Transaction].DayKey = DayZone.DayKey
													Where	TDetail.TransactionTypeID = 'WI'
															and TDetail.TransactionStatusID = 'RE'
													Group by AccountKey
															, DayZone.MasterDayKey
												) as Transactions on Position.AccountKey = Transactions.AccountKey
																and DayZone.MasterDayKey = Transactions.MasterDayKey
						Where		Right (Position.DayKey,2) = 02
									and ((Activity.HasWithdrawn = 'Yes' and Transactions.AccountKey is null)
										Or (Activity.HasWithdrawn = 'No' and Transactions.AccountKey is not null))
					)
					+
						(
						Select		Count (*)
						From		[$(Dimensional)].[Fact].[Position] as Position
									Inner join [$(Dimensional)].[Dimension].Activity as Activity on Position.ActivityKey = Activity.ActivityKey
									Inner join [$(Dimensional)].[Dimension].DayZone on Position.DayKey = DayZone.DayKey							
									left join	(Select		[Transaction].AccountKey
															, DayZone.AnalysisDayKey
													From	[$(Dimensional)].[Fact].[Transaction] as [Transaction]
															Inner join [$(Dimensional)].[Dimension].TransactionDetail as TDetail on [Transaction].TransactionDetailKey = TDetail.TransactionDetailKey
															Inner join [$(Dimensional)].[Dimension].DayZOne on [Transaction].DayKey = DayZone.DayKey
													Where	TDetail.TransactionTypeID = 'WI'
															and TDetail.TransactionStatusID = 'RE'
													Group by AccountKey
															, DayZone.AnalysisDayKey
												) as Transactions on Position.AccountKey = Transactions.AccountKey
																and DayZone.AnalysisDayKey = Transactions.AnalysisDayKey
						Where		Right (Position.DayKey,2) = 03
									and ((Activity.HasWithdrawn = 'Yes' and Transactions.AccountKey is null)
										Or (Activity.HasWithdrawn = 'No' and Transactions.AccountKey is not null))
						)
					) 
	If @Counter = 0
	Begin
		Insert into test.tblResults Values (@TestRunNo, '161.2', 'DataValid_Fact_Position ActivityKey - Has Withdrawn', 'Pass', Null)
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '161.2', 'DataValid_Fact_Position ActivityKey - Has Withdrawn', 'Fail', 'ActivityKey is incorrect')
	End
	--Has Won
	Set @Counter = (
					(
						Select		Count (*)
						From		[$(Dimensional)].[Fact].[Position] as Position
									Inner join [$(Dimensional)].[Dimension].Activity as Activity on Position.ActivityKey = Activity.ActivityKey
									Inner join [$(Dimensional)].[Dimension].DayZone on Position.DayKey = DayZone.DayKey							
									left join	(Select		[Transaction].AccountKey
															, DayZone.MasterDayKey
													From	[$(Dimensional)].[Fact].[Transaction] as [Transaction]
															Inner join [$(Dimensional)].[Dimension].TransactionDetail as TDetail on [Transaction].TransactionDetailKey = TDetail.TransactionDetailKey
															Inner join [$(Dimensional)].[Dimension].DayZOne on [Transaction].DayKey = DayZone.DayKey
													Where	TDetail.TransactionTypeID = 'BT'
															and TDetail.TransactionStatusID = 'WN'
													Group by AccountKey
															, DayZone.MasterDayKey
												) as Transactions on Position.AccountKey = Transactions.AccountKey
																and DayZone.MasterDayKey = Transactions.MasterDayKey
						Where		Right (Position.DayKey,2) = 02
									and ((Activity.HasWon = 'Yes' and Transactions.AccountKey is null)
										Or (Activity.HasWon = 'No' and Transactions.AccountKey is not null))
					)
					+
						(
						Select		Count (*)
						From		[$(Dimensional)].[Fact].[Position] as Position
									Inner join [$(Dimensional)].[Dimension].Activity as Activity on Position.ActivityKey = Activity.ActivityKey
									Inner join [$(Dimensional)].[Dimension].DayZone on Position.DayKey = DayZone.DayKey							
									left join	(Select		[Transaction].AccountKey
															, DayZone.AnalysisDayKey
													From	[$(Dimensional)].[Fact].[Transaction] as [Transaction]
															Inner join [$(Dimensional)].[Dimension].TransactionDetail as TDetail on [Transaction].TransactionDetailKey = TDetail.TransactionDetailKey
															Inner join [$(Dimensional)].[Dimension].DayZOne on [Transaction].DayKey = DayZone.DayKey
													Where	TDetail.TransactionTypeID = 'BT'
															and TDetail.TransactionStatusID = 'WN'
													Group by AccountKey
															, DayZone.AnalysisDayKey
												) as Transactions on Position.AccountKey = Transactions.AccountKey
																and DayZone.AnalysisDayKey = Transactions.AnalysisDayKey
						Where		Right (Position.DayKey,2) = 03
									and ((Activity.HasWon = 'Yes' and Transactions.AccountKey is null)
										Or (Activity.HasWon = 'No' and Transactions.AccountKey is not null))
						)
					) 
	If @Counter = 0
	Begin
		Insert into test.tblResults Values (@TestRunNo, '161.3', 'DataValid_Fact_Position ActivityKey - Has Won', 'Pass', Null)
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '161.3', 'DataValid_Fact_Position ActivityKey - Has Won', 'Fail', 'ActivityKey is incorrect')
	End
	--Has Lost
	Set @Counter = (
					(
						Select		Count (*)
						From		[$(Dimensional)].[Fact].[Position] as Position
									Inner join [$(Dimensional)].[Dimension].Activity as Activity on Position.ActivityKey = Activity.ActivityKey
									Inner join [$(Dimensional)].[Dimension].DayZone on Position.DayKey = DayZone.DayKey							
									left join	(Select		[Transaction].AccountKey
															, DayZone.MasterDayKey
													From	[$(Dimensional)].[Fact].[Transaction] as [Transaction]
															Inner join [$(Dimensional)].[Dimension].TransactionDetail as TDetail on [Transaction].TransactionDetailKey = TDetail.TransactionDetailKey
															Inner join [$(Dimensional)].[Dimension].DayZOne on [Transaction].DayKey = DayZone.DayKey
													Where	TDetail.TransactionTypeID = 'BT'
															and TDetail.TransactionStatusID = 'LO'
													Group by AccountKey
															, DayZone.MasterDayKey
												) as Transactions on Position.AccountKey = Transactions.AccountKey
																and DayZone.MasterDayKey = Transactions.MasterDayKey
						Where		Right (Position.DayKey,2) = 02
									and ((Activity.HasLost = 'Yes' and Transactions.AccountKey is null)
										Or (Activity.HasLost = 'No' and Transactions.AccountKey is not null))
					)
					+
						(
						Select		Count (*)
						From		[$(Dimensional)].[Fact].[Position] as Position
									Inner join [$(Dimensional)].[Dimension].Activity as Activity on Position.ActivityKey = Activity.ActivityKey
									Inner join [$(Dimensional)].[Dimension].DayZone on Position.DayKey = DayZone.DayKey							
									left join	(Select		[Transaction].AccountKey
															, DayZone.AnalysisDayKey
													From	[$(Dimensional)].[Fact].[Transaction] as [Transaction]
															Inner join [$(Dimensional)].[Dimension].TransactionDetail as TDetail on [Transaction].TransactionDetailKey = TDetail.TransactionDetailKey
															Inner join [$(Dimensional)].[Dimension].DayZOne on [Transaction].DayKey = DayZone.DayKey
													Where	TDetail.TransactionTypeID = 'BT'
															and TDetail.TransactionStatusID = 'LO'
													Group by AccountKey
															, DayZone.AnalysisDayKey
												) as Transactions on Position.AccountKey = Transactions.AccountKey
																and DayZone.AnalysisDayKey = Transactions.AnalysisDayKey
						Where		Right (Position.DayKey,2) = 03
									and ((Activity.HasLost = 'Yes' and Transactions.AccountKey is null)
										Or (Activity.HasLost = 'No' and Transactions.AccountKey is not null))
						)
					) 
	If @Counter = 0
	Begin
		Insert into test.tblResults Values (@TestRunNo, '161.4', 'DataValid_Fact_Position ActivityKey - Has Lost', 'Pass', Null)
	End
	Else
	Begin
		If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-5762'
							and [Status] != 'Done'
				) 
		Begin
			Insert into test.tblResults Values (@TestRunNo, '161.4', 'DataValid_Fact_Position ActivityKey - Has Lost', 'Pass', 'CCIA-5762')
		End
		Else
		Begin
			Insert into test.tblResults Values (@TestRunNo, '161.4', 'DataValid_Fact_Position ActivityKey - Has Lost', 'Fail', 'ActivityKey is incorrect')
		End
	End
--LifecycleKey
--FirstDepositDayKey
--FirstBetDayKey
--FirstBetTypeKey
--FirstClassKey
--FirstChannelKey
--LastDepositDayKey
--LastBetDayKey
--LastBetTypeKey
--LastClassKey
--LastChannelKey

--OpeningBalance  covered by Cross Ref

--TransactedAmount
Set @Counter = (
				Select		Count(*)
				From		[$(Dimensional)].[Fact].[Position] as Position
				Where		OpeningBalance + TransactedAmount != ClosingBalance
				)
If @Counter = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '161.5', 'DataValid_Fact_Position TransactedAmount', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '161.5', 'DataValid_Fact_Position TransactedAmount', 'Fail', 'TransactedAmount is incorrect')
End

--ClosingBalance  covered by Cross Ref

--DaysSinceAccountOpened
Set @Counter =	(
				Select		Count(*)
				From		(
							Select		Position.DayKey
										, AccountOpenedDayKey
										, DaysSinceAccountOpened
							From		[$(Dimensional)].[Fact].[Position] as Position
										Inner join [$(Dimensional)].[Dimension].DayZone as DayZone on Position.DayKey = DayZone.DayKey
										Inner join [$(Dimensional)].[Dimension].[Day] as [Day] on DayZone.MasterDayKey = [Day].DayKey
										inner join [$(Dimensional)].[Dimension].Account as Account on Position.AccountKey = Account.AccountKey
										Inner join [$(Dimensional)].[Dimension].DayZone as DayZone2 on Account.AccountOpenedDayKey = [DayZone2].DayKey
										Inner join [$(Dimensional)].[Dimension].[Day] as [Day2] on DayZone2.MasterDayKey = [Day2].DayKey
							Where		Right (Position.DayKey,2) = 02
										and DaysSinceAccountOpened != DateDiff([Day],Day2.DayDate,[Day].DayDate)
							) as Data
				Where		(AccountOpenedDayKey != 0 and DaysSinceAccountOpened is null)
							or (AccountOpenedDayKey = 0 and DaysSinceAccountOpened is not null)
				) 
If @Counter = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '161.6', 'DataValid_Fact_Position DaysSinceFirstDeposit', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '161.6', 'DataValid_Fact_Position DaysSinceFirstDeposit', 'Fail', 'DaysSinceFirstDeposit is incorrect')
End

--DaysSinceFirstDeposit
Set @Counter =	(
				Select		Count(*)
				From		(
							Select		Position.DayKey
										, FirstDepositDayKey
										, DaysSinceFirstDeposit
							From		[$(Dimensional)].[Fact].[Position] as Position
										Inner join [$(Dimensional)].[Dimension].DayZone as DayZone on Position.DayKey = DayZone.DayKey
										Inner join [$(Dimensional)].[Dimension].[Day] as [Day] on DayZone.MasterDayKey = [Day].DayKey
										Inner join [$(Dimensional)].[Dimension].DayZone as DayZone2 on Position.FirstDepositDayKey = [DayZone2].DayKey
										Inner join [$(Dimensional)].[Dimension].[Day] as [Day2] on DayZone2.MasterDayKey = [Day2].DayKey
							Where		Right (Position.DayKey,2) = 02
										and DaysSinceFirstDeposit != DateDiff([Day],Day2.DayDate,[Day].DayDate)
							) as Data
				Where		(FirstDepositDayKey != 0 and DaysSinceFirstDeposit is null)
							or (FirstDepositDayKey = 0 and DaysSinceFirstDeposit is not null)
				) 
If @Counter = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '161.7', 'DataValid_Fact_Position DaysSinceFirstDeposit', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-6139'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '161.7', 'DataValid_Fact_Position DaysSinceFirstDeposit', 'Pass', 'CCIA-6139')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '161.7', 'DataValid_Fact_Position DaysSinceFirstDeposit', 'Fail', 'DaysSinceFirstDeposit is incorrect')
	End
End

--DaysSinceFirstBet
Set @Counter =	(
				Select		Count(*)
				From		(
							Select		Position.DayKey
										, FirstBetDayKey
										, DaysSinceFirstBet
							From		[$(Dimensional)].[Fact].[Position] as Position
										Inner join [$(Dimensional)].[Dimension].DayZone as DayZone on Position.DayKey = DayZone.DayKey
										Inner join [$(Dimensional)].[Dimension].[Day] as [Day] on DayZone.MasterDayKey = [Day].DayKey
										Inner join [$(Dimensional)].[Dimension].DayZone as DayZone2 on Position.FirstBetDayKey = [DayZone2].DayKey
										Inner join [$(Dimensional)].[Dimension].[Day] as [Day2] on DayZone2.MasterDayKey = [Day2].DayKey
							Where		Right (Position.DayKey,2) = 02
										and DaysSinceFirstBet != DateDiff([Day],Day2.DayDate,[Day].DayDate)
							) as Data
				Where		(FirstBetDayKey != 0 and DaysSinceFirstBet is null)
							or (FirstBetDayKey = 0 and DaysSinceFirstBet is not null)
				) 
If @Counter = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '161.8', 'DataValid_Fact_Position DaysSinceFirstBet', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-6139'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '161.8', 'DataValid_Fact_Position DaysSinceFirstBet', 'Pass', 'CCIA-6139')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '161.8', 'DataValid_Fact_Position DaysSinceFirstBet', 'Fail', 'DaysSinceFirstBet is incorrect')
	End
End

--DaysSinceLastDeposit
Set @Counter =	(
				Select		Count(*)
				From		(
							Select		Position.DayKey
										, LastDepositDayKey
										, DaysSinceLastDeposit
							From		[$(Dimensional)].[Fact].[Position] as Position
										Inner join [$(Dimensional)].[Dimension].DayZone as DayZone on Position.DayKey = DayZone.DayKey
										Inner join [$(Dimensional)].[Dimension].[Day] as [Day] on DayZone.MasterDayKey = [Day].DayKey
										Inner join [$(Dimensional)].[Dimension].DayZone as DayZone2 on Position.LastDepositDayKey = [DayZone2].DayKey
										Inner join [$(Dimensional)].[Dimension].[Day] as [Day2] on DayZone2.MasterDayKey = [Day2].DayKey
							Where		Right (Position.DayKey,2) = 02
										and DaysSinceLastDeposit != DateDiff([Day],Day2.DayDate,[Day].DayDate)
							) as Data
				Where		(LastDepositDayKey != 0 and DaysSinceLastDeposit is null)
							or (LastDepositDayKey = 0 and DaysSinceLastDeposit is not null)
				) 
If @Counter = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '161.9', 'DataValid_Fact_Position DaysSinceLastDeposit', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-6139'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '161.9', 'DataValid_Fact_Position DaysSinceLastDeposit', 'Pass', 'CCIA-6139')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '161.9', 'DataValid_Fact_Position DaysSinceLastDeposit', 'Fail', 'DaysSinceLastDeposit is incorrect')
	End
End

--DayssinceLastBet
Set @Counter =	(
				Select		Count(*)
				From		(
							Select		Position.DayKey
										, LastBetDayKey
										, DayssinceLastBet
							From		[$(Dimensional)].[Fact].[Position] as Position
										Inner join [$(Dimensional)].[Dimension].DayZone as DayZone on Position.DayKey = DayZone.DayKey
										Inner join [$(Dimensional)].[Dimension].[Day] as [Day] on DayZone.MasterDayKey = [Day].DayKey
										Inner join [$(Dimensional)].[Dimension].DayZone as DayZone2 on Position.LastBetDayKey = [DayZone2].DayKey
										Inner join [$(Dimensional)].[Dimension].[Day] as [Day2] on DayZone2.MasterDayKey = [Day2].DayKey
							Where		Right (Position.DayKey,2) = 02
										and DayssinceLastBet != DateDiff([Day],Day2.DayDate,[Day].DayDate)
							) as Data
				Where		(LastBetDayKey != 0 and DayssinceLastBet is null)
							or (LastBetDayKey = 0 and DayssinceLastBet is not null)
				) 
If @Counter = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '161.10', 'DataValid_Fact_Position DaysSinceLastDeposit', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-6139'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '161.10', 'DataValid_Fact_Position DaysSinceLastDeposit', 'Pass', 'CCIA-6139')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '161.10', 'DataValid_Fact_Position DaysSinceLastDeposit', 'Fail', 'DaysSinceLastDeposit is incorrect')
	End
End

--LifetimeTurnover
--LifetimeGrossWin
--LifetimeBetCount
--LifetimeBonus
--LifetimeDeposits
--LifetimeWithdrawals
--TurnoverMTDCalender
--TurnoverMTDFiscal

--TierKey
Select		TierName
			, TierKey
			, 'LowerBand' = CASE
				When TierName not like '%X%' and TierName like '>=%' then Replace (TierName, '>= ', '')
				When TierName not like '%X%' and TierName like '<%' then 0
				Else Replace (Left (TierName,CHARINDEX('X', TierName,1)), ' <= X', '')
				End
			, 'UpperBand' = CASE
				When TierName not like '%X%' and TierName like '>=%' then 99999999999
				When TierName not like '%X%' and TierName like '<%' then Replace (TierName, '<','')
				Else Replace (Right (TierName,CHARINDEX('X', TierName,1)), 'X <', '')
				End
into		#Data
From		[$(Dimensional)].Dimension.ValueTiers
Where		TierKey not in (-1, 0)

Set @Counter = 	(
				SELECT		Count(*)
				FROM		[$(Dimensional)].[Fact].[Position] as Position
							left join #Data on Position.TurnoverMTDCalender >= #Data.LowerBand
												and Position.TurnoverMTDCalender < #Data.UpperBand
				Where		Position.TierKey != #Data.TierKey
							and Position.TierKey != 0
				) + 
				(
				SELECT		Count(*)
				FROM		[$(Dimensional)].[Fact].[Position] as Position
				Where		TurnoverMTDCalender = 0 and TierKey != 0
				)
IF @Counter = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '161.11', 'DataValid_Fact_Position TierKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '161.11', 'DataValid_Fact_Position TierKey', 'Fail', 'Tier Key is incorrect')
End

--TierKeyFiscal
Set @Counter = 	(
				SELECT		Count(*)
				FROM		[$(Dimensional)].[Fact].[Position] as Position
							left join #Data on Position.TurnoverMTDFiscal >= #Data.LowerBand
												and Position.TurnoverMTDFiscal < #Data.UpperBand
				Where		Position.TierKeyFiscal != #Data.TierKey
							and Position.TierKeyFiscal != 0
				) + 
				(
				SELECT		Count(*)
				FROM		[$(Dimensional)].[Fact].[Position] as Position
				Where		TurnoverMTDFiscal = 0 and TierKeyFiscal != 0
				)
IF @Counter = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '161.12', 'DataValid_Fact_Position TierKeyFiscal', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '161.12', 'DataValid_Fact_Position TierKeyFiscal', 'Fail', 'Fiscal Tier Key is incorrect')
End

--RevenueLifeCycleKey
If Not exists (
				Select		*
				From		(
							select		Position.AccountKey
										, Position.DayKey
										, RevenueLifeCycle.RevenueLifeCycle
										, Position.TurnoverMTDCalender
										, Position.LifetimeTurnover
										, LastMonthPosition.TurnoverMTDCalender as TurnoverLastMonth
										, LastMonthPosition.LifetimeTurnover as LifeTurnoverToLastMonth
										, 'Correction' = CASE
											When Position.TurnoverMTDCalender = 0 then 'N/A'
											When LastMonthPosition.LifetimeTurnover = 0 and Position.LifetimeTurnover != 0 then 'FTB'
											When LastMonthPosition.LifetimeTurnover is null and Position.LifetimeTurnover != 0 then 'FTB'
											When LastMonthPosition.TurnoverMTDCalender != 0 then 'Retained'
											When Position.LifetimeTurnover != 0 then 'Reactivated'
											else 'ERROR'
											End
							From		[$(Dimensional)].fact.Position as Position
										inner join [$(Dimensional)].Dimension.RevenueLifeCycle as RevenueLifeCycle on Position.RevenueLifeCycleKey = RevenueLifeCycle.RevenueLifeCycleKey
										left join	(Select	AccountKey
															, DayKey
															, TurnoverMTDCalender
															, LifetimeTurnover
													 From	[$(Dimensional)].fact.Position
													 Where	Left (DayKey, 8) = 20151031 --need to get dynamicly
													) as LastMonthPosition on Right (Position.DayKey,2) = Right (LastMonthPosition.DayKey,2)
																				and Position.AccountKey = LastMonthPosition.AccountKey
							Where		Position.DayKey >= 2015110100 --need to get dynamicly
							) as Data
				Where		Correction != RevenueLifeCycle
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '161.12a', 'DataValid_Fact_Position RevenueLifeCycle', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '161.12a', 'DataValid_Fact_Position RevenueLifeCycle', 'Fail', 'RevenueLifeCycle is incorrect')
End

--LifeStageKey
Set @Counter = 
			(Select		Count(*)
			From		(
						select		'Correction' = CASE
										When AccountStatus.LedgerStatus != 'A' then 'Out Of Play'
										When DaysSinceAccountOpened < 2 then 'Acquisition'
										When DaysSinceAccountOpened > 7 and DaysSinceFirstBet is null then 'Convert'
										When DaysSinceAccountOpened between 2 and 11 then 'On Boarding'
										when DayssinceLastBet between 14 and 29 then 'Win back'
										when DayssinceLastBet >= 30 then 'Re-Activate'
										else 'Retain'
										end
									, LifeStage.LifeStage as LifeStage
						From		[$(Dimensional)].Fact.Position as Position
									inner join [$(Dimensional)].Dimension.AccountStatus as AccountStatus on Position.AccountStatusKey = AccountStatus.AccountStatusKey
									inner join [$(Dimensional)].Dimension.Lifestage as LifeStage on Position.LifeStageKey = LifeStage.LifeStageKey
						) as Data
			Where		Correction != LifeStage
			)
IF @Counter = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '161.13', 'DataValid_Fact_Position LifeStage', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '161.13', 'DataValid_Fact_Position LifeStage', 'Fail', 'LifeStage is incorrect')
End

--PreviousLifeStageKey
Set @Counter = 
			(
			Select		Count(*)
			From		[$(Dimensional)].Fact.Position as Position
						inner join [$(Dimensional)].dimension.DayZone as DayZone on Position.DayKey = DayZone.DayKey
						inner join [$(Dimensional)].dimension.[day] as [day] on DayZone.MasterDayKey = [Day].DayKey
						inner join [$(Dimensional)].dimension.DayZone as DayZone2 on [Day].PreviousDayKey = DayZone2.MasterDayKey
						inner join [$(Dimensional)].Fact.Position as Position2 on Dayzone2.DayKey = Position2.DayKey
													and Position.AccountKey = Position2.AccountKey
			Where		right (Position.DayKey, 2) = 02
						and right (Position2.DayKey, 2) = 02
						and Position.PreviousLifeStageKey != Position2.LifeStageKey
						and Position.DaysSinceAccountOpened != 0
			)
			+
			(
			Select		Count(*)
			From		[$(Dimensional)].Fact.Position as Position
						inner join [$(Dimensional)].dimension.DayZone as DayZone on Position.DayKey = DayZone.DayKey
						inner join [$(Dimensional)].dimension.[day] as [day] on DayZone.AnalysisDayKey = [Day].DayKey
						inner join [$(Dimensional)].dimension.DayZone as DayZone2 on [Day].PreviousDayKey = DayZone2.AnalysisDayKey
						inner join [$(Dimensional)].Fact.Position as Position2 on Dayzone2.DayKey = Position2.DayKey
													and Position.AccountKey = Position2.AccountKey
			Where		right (Position.DayKey, 2) = 03
						and right (Position2.DayKey, 2) = 03
						and Position.PreviousLifeStageKey != Position2.LifeStageKey
						and Position.DaysSinceAccountOpened != 0
			)

IF @Counter = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '161.14', 'DataValid_Fact_Position PreviousLifeStage', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-6214'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '161.14', 'DataValid_Fact_Position PreviousLifeStage', 'Pass', 'CCIA-6214')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '161.14', 'DataValid_Fact_Position PreviousLifeStage', 'Fail', 'PreviousLifeStage is incorrect')
	End
End

--Metadata
--CreatedDate
--CreatedBatchKey
--CreatedBy
--ModifiedDate
--ModifiedBatchKey
--ModifiedBy
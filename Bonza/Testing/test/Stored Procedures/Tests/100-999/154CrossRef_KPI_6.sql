﻿--Test Code
--exec Test.[154CrossRef_KPI_6] 'AdHoc'
CREATE Procedure [Test].[154CrossRef_KPI_6] @TestRunNo varchar(23)

as

Declare @Counter int

Create Table #Errors (
	[TestDate] [date] NULL,
	[BrandCode] [varchar](23) NULL,
	[AccountID] [varchar](23) NULL,
	[LedgerID] [varchar](23) NULL,
	[SourceOpeningBalance] [decimal](18, 4) NULL,
	[TransactedAmount] [decimal](18, 4) NULL,
	[TransactionCount] [int] NULL,
	[NetClosingBalance] [decimal](18, 4) NULL,
	[NextSourceOpeningBalance] [decimal](18, 4) NULL,
	[DayClosingDiscrepancy] [decimal](18, 4) NULL,
	[OngoingClosingDiscrepancy] [decimal](18, 4) NULL,
	[TotalDiscrepancy] [decimal](18, 4) NULL
	)

Delete from test.CurrentCrossRef where [type] = 'KPI/Position'

Insert into #Errors
exec Test.[CrossRef_KPI] @TestRunNo, '2016-05-17'

Insert Into	test.CurrentCrossRef
Select		'2016-05-17' as TestDate
			, 'KPI/Position' as [Type]
			, 'Client' as Balance
			, 'Cash' as WalletName
			, BrandCode
			, AccountID
			, LedgerID
			, SourceOpeningBalance
			, TransactedAmount
			, TransactionCount
			, NetClosingBalance
			, NextSourceOpeningBalance
			, DayClosingDiscrepancy
			, OngoingClosingDiscrepancy
			, TotalDiscrepancy
			, '3.23'
From		#Errors

Truncate Table #Errors
Insert into #Errors
exec Test.[CrossRef_KPI] @TestRunNo, '2016-05-18'

Insert Into	test.CurrentCrossRef
Select		'2016-05-18' as TestDate
			, 'KPI/Position' as [Type]
			, 'Client' as Balance
			, 'Cash' as WalletName
			, BrandCode
			, AccountID
			, LedgerID
			, SourceOpeningBalance
			, TransactedAmount
			, TransactionCount
			, NetClosingBalance
			, NextSourceOpeningBalance
			, DayClosingDiscrepancy
			, OngoingClosingDiscrepancy
			, TotalDiscrepancy
			, '3.23'
From		#Errors

Truncate Table #Errors
Insert into #Errors
exec Test.[CrossRef_KPI] @TestRunNo, '2016-05-19'

Insert Into	test.CurrentCrossRef
Select		'2016-05-19' as TestDate
			, 'KPI/Position' as [Type]
			, 'Client' as Balance
			, 'Cash' as WalletName
			, BrandCode
			, AccountID
			, LedgerID
			, SourceOpeningBalance
			, TransactedAmount
			, TransactionCount
			, NetClosingBalance
			, NextSourceOpeningBalance
			, DayClosingDiscrepancy
			, OngoingClosingDiscrepancy
			, TotalDiscrepancy
			, '3.23'
From		#Errors

Truncate Table #Errors
Insert into #Errors
exec Test.[CrossRef_KPI] @TestRunNo, '2016-05-20'

Insert Into	test.CurrentCrossRef
Select		'2016-05-20' as TestDate
			, 'KPI/Position' as [Type]
			, 'Client' as Balance
			, 'Cash' as WalletName
			, BrandCode
			, AccountID
			, LedgerID
			, SourceOpeningBalance
			, TransactedAmount
			, TransactionCount
			, NetClosingBalance
			, NextSourceOpeningBalance
			, DayClosingDiscrepancy
			, OngoingClosingDiscrepancy
			, TotalDiscrepancy
			, '3.23'
From		#Errors

Truncate Table #Errors
Insert into #Errors
exec Test.[CrossRef_KPI] @TestRunNo, '2016-05-21'

Insert Into	test.CurrentCrossRef
Select		'2016-05-21' as TestDate
			, 'KPI/Position' as [Type]
			, 'Client' as Balance
			, 'Cash' as WalletName
			, BrandCode
			, AccountID
			, LedgerID
			, SourceOpeningBalance
			, TransactedAmount
			, TransactionCount
			, NetClosingBalance
			, NextSourceOpeningBalance
			, DayClosingDiscrepancy
			, OngoingClosingDiscrepancy
			, TotalDiscrepancy
			, '3.23'
From		#Errors

Truncate Table #Errors
Insert into #Errors
exec Test.[CrossRef_KPI] @TestRunNo, '2016-05-22'

Insert Into	test.CurrentCrossRef
Select		'2016-05-22' as TestDate
			, 'KPI/Position' as [Type]
			, 'Client' as Balance
			, 'Cash' as WalletName
			, BrandCode
			, AccountID
			, LedgerID
			, SourceOpeningBalance
			, TransactedAmount
			, TransactionCount
			, NetClosingBalance
			, NextSourceOpeningBalance
			, DayClosingDiscrepancy
			, OngoingClosingDiscrepancy
			, TotalDiscrepancy
			, '3.23'
From		#Errors


Declare @LastVersion Varchar(10)

If (Select Acquisition from [Control].[Acquisition]) = 'Small_Acquisition'
Begin	
	Set @LastVersion = (Select Max (VersionNumber) From test.SmallPreviousCrossRef)
	 
	Select	TestDate
			, [Type]
			, Balance
			, WalletName
			, BrandCode
			, AccountID
			, LedgerID
			, SourceOpeningBalance
			, TransactedAmount
			, TransactionCount
			, NetClosingBalance
			, NextSourceOpeningBalance
			, DayClosingDiscrepancy
			, OngoingClosingDiscrepancy
			, TotalDiscrepancy
	Into		#NewErrors
	From		test.CurrentCrossRef
	Where		[Type] = 'KPI/Position'
	Except
	Select	TestDate
			, [Type]
			, Balance
			, WalletName
			, BrandCode
			, AccountID
			, LedgerID
			, SourceOpeningBalance
			, TransactedAmount
			, TransactionCount
			, NetClosingBalance
			, NextSourceOpeningBalance
			, DayClosingDiscrepancy
			, OngoingClosingDiscrepancy
			, TotalDiscrepancy
	From		test.SmallPreviousCrossRef
	Where		[Type] = 'KPI/Position'
				and VersionNumber = @LastVersion

	Set @Counter = (Select Count(*) from #NewErrors)
End
Else
Begin 

	Set @LastVersion = (Select Max (VersionNumber) From test.PreviousCrossRef)

	Select	TestDate
			, [Type]
			, Balance
			, WalletName
			, BrandCode
			, AccountID
			, LedgerID
			, SourceOpeningBalance
			, TransactedAmount
			, TransactionCount
			, NetClosingBalance
			, NextSourceOpeningBalance
			, DayClosingDiscrepancy
			, OngoingClosingDiscrepancy
			, TotalDiscrepancy
	Into		#SmallNewErrors
	From		test.CurrentCrossRef
	except
	Select	TestDate
			, [Type]
			, Balance
			, WalletName
			, BrandCode
			, AccountID
			, LedgerID
			, SourceOpeningBalance
			, TransactedAmount
			, TransactionCount
			, NetClosingBalance
			, NextSourceOpeningBalance
			, DayClosingDiscrepancy
			, OngoingClosingDiscrepancy
			, TotalDiscrepancy
	From		test.PreviousCrossRef
	Where		[Type] = 'KPI/Position'
				and VersionNumber = @LastVersion

	Set @Counter = (Select Count(*) from #NewErrors)
End

If @Counter = 0
Begin
	Insert into test.tblResults ([RunID], [TestID], [TestName], [Status], [Notes]) Values (@TestRunNo, '154', 'CrossRef_KPI/Position 6Days', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults ([RunID], [TestID], [TestName], [Status], [Notes]) Values (@TestRunNo, '154', 'CrossRef_KPI/Position 6Days', 'Fail', 'There are new mismatches between KPI and Position')
End

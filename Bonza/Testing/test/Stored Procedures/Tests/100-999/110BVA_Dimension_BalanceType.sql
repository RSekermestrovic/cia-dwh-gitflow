﻿--Test Code
--exec Test.[110BVA_Dimension_BalanceType] 'ADHOC'
Create Procedure [Test].[110BVA_Dimension_BalanceType] @TestRunNo varchar(23)

as

IF 	 exists		(
					Select		*
					From		[$(Dimensional)].Dimension.BalanceType
					where		BalanceTypeKey = -1
								and BalanceTypeID = 'UNK'
								and BalanceTypeName = 'Unknown'
					)
Begin
	Insert into Test.tblResults Values (@TestRunNo, '110', 'BVA_BalanceType -1', 'Pass', Null)
End
Else
Begin
	Insert into Test.tblResults Values (@TestRunNo, '110', 'BVA_BalanceType -1', 'Fail', 'There is an issue with the -1 Key')
End

IF 	 exists		(
					Select		*
					From		[$(Dimensional)].Dimension.BalanceType
					where		BalanceTypeKey = 0
								and BalanceTypeID = 'N/A'
								and BalanceTypeName = 'N/A'
					)
Begin
	Insert into Test.tblResults Values (@TestRunNo, '110', 'BVA_BalanceType 0', 'Pass', Null)
End
Else
Begin
	Insert into Test.tblResults Values (@TestRunNo, '110', 'BVA_BalanceType 0', 'Fail', 'There is an issue with the 0 Key')
End

IF 	 exists		(
					Select		*
					From		[$(Dimensional)].Dimension.BalanceType
					where		BalanceTypeKey = 2
								and BalanceTypeID = 'CLI'
								and BalanceTypeName = 'Client'
					)
Begin
	Insert into Test.tblResults Values (@TestRunNo, '110', 'BVA_BalanceType 2', 'Pass', Null)
End
Else
Begin
	Insert into Test.tblResults Values (@TestRunNo, '110', 'BVA_BalanceType 2', 'Fail', 'There is an issue with the 2 Key')
End

IF 	 exists		(
					Select		*
					From		[$(Dimensional)].Dimension.BalanceType
					where		BalanceTypeKey = 3
								and BalanceTypeID = 'P&L'
								and BalanceTypeName = 'Profit & Loss'
					)
Begin
	Insert into Test.tblResults Values (@TestRunNo, '110', 'BVA_BalanceType 3', 'Pass', Null)
End
Else
Begin
	Insert into Test.tblResults Values (@TestRunNo, '110', 'BVA_BalanceType 3', 'Fail', 'There is an issue with the 3 Key')
End

IF 	 exists		(
					Select		*
					From		[$(Dimensional)].Dimension.BalanceType
					where		BalanceTypeKey = 4
								and BalanceTypeID = 'UNS'
								and BalanceTypeName = 'Hold'
					)
Begin
	Insert into Test.tblResults Values (@TestRunNo, '110', 'BVA_BalanceType 4', 'Pass', Null)
End
Else
Begin
	Insert into Test.tblResults Values (@TestRunNo, '110', 'BVA_BalanceType 4', 'Fail', 'There is an issue with the 4 Key')
End

IF 	 exists		(
					Select		*
					From		[$(Dimensional)].Dimension.BalanceType
					where		BalanceTypeKey = 5
								and BalanceTypeID = 'INT'
								and BalanceTypeName = 'Intercept'
					)
Begin
	Insert into Test.tblResults Values (@TestRunNo, '110', 'BVA_BalanceType 4', 'Pass', Null)
End
Else
Begin
	Insert into Test.tblResults Values (@TestRunNo, '110', 'BVA_BalanceType 4', 'Fail', 'There is an issue with the 4 Key')
End

IF 	 exists		(
					Select		*
					From		[$(Dimensional)].Dimension.BalanceType
					where		BalanceTypeKey = 7
								and BalanceTypeID = 'PWD'
								and BalanceTypeName = 'Pending Withdrawal'
					)
Begin
	Insert into Test.tblResults Values (@TestRunNo, '110', 'BVA_BalanceType 7', 'Pass', Null)
End
Else
Begin
	Insert into Test.tblResults Values (@TestRunNo, '110', 'BVA_BalanceType 7', 'Fail', 'There is an issue with the 7 Key')
End

IF 	 exists		(
					Select		*
					From		[$(Dimensional)].Dimension.BalanceType
					where		BalanceTypeKey = 8
								and BalanceTypeID = 'PDP'
								and BalanceTypeName = 'Pending Deposit'
					)
Begin
	Insert into Test.tblResults Values (@TestRunNo, '110', 'BVA_BalanceType 8', 'Pass', Null)
End
Else
Begin
	Insert into Test.tblResults Values (@TestRunNo, '110', 'BVA_BalanceType 8', 'Fail', 'There is an issue with the 8 Key')
End

IF 	 exists		(
					Select		*
					From		[$(Dimensional)].Dimension.BalanceType
					where		BalanceTypeKey = 9
								and BalanceTypeID = 'BBU'
								and BalanceTypeName = 'Betback Hold'
					)
Begin
	Insert into Test.tblResults Values (@TestRunNo, '110', 'BVA_BalanceType 9', 'Pass', Null)
End
Else
Begin
	Insert into Test.tblResults Values (@TestRunNo, '110', 'BVA_BalanceType 9', 'Fail', 'There is an issue with the 9 Key')
End

IF 	 exists		(
					Select		*
					From		[$(Dimensional)].Dimension.BalanceType
					where		BalanceTypeKey = 10
								and BalanceTypeID = 'BBK'
								and BalanceTypeName = 'Betback'
					)
Begin
	Insert into Test.tblResults Values (@TestRunNo, '110', 'BVA_BalanceType 10', 'Pass', Null)
End
Else
Begin
	Insert into Test.tblResults Values (@TestRunNo, '110', 'BVA_BalanceType 10', 'Fail', 'There is an issue with the 10 Key')
End
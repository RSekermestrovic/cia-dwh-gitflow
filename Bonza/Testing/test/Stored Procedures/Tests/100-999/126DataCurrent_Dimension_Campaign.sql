﻿--Test Code
--exec [Test].[126DataCurrent_Dimension_Campaign] 'ADHOC'
Create Procedure [Test].[126DataCurrent_Dimension_Campaign] @TestRunNo varchar(23)

as

Select	*
into #Dimension
From [$(Dimensional)].Dimension.Campaign

Declare @NumberOfRecords int
Set @NumberOfRecords = 
					(
					Select		count(*) as NumberOfRecords
					From		#Dimension
					)
If @NumberOfRecords >= 1009
Begin
	Insert into test.tblResults Values (@TestRunNo, '126', 'DataCurrent_Dimension_Campaign Record Count', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '126', 'DataCurrent_Dimension_Campaign Record Count', 'Fail', 'Expected at least 1009 records, ' + Cast (@NumberOfRecords as varchar(5)) + ' found.')
End

IF 					(
					Select		Min (CampaignKey) as Record
					From		#Dimension
					where		CampaignKey not in (0 ,-1)
					)
					= 1
Begin
	Insert into test.tblResults Values (@TestRunNo, '126.1', 'DataCurrent_Dimension_Campaign Record Count', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '126.1', 'DataCurrent_Dimension_Campaign Record Count', 'Fail', 'The lowest record key is not 1')
End




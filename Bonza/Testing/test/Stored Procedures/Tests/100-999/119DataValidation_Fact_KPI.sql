﻿--Test Code
--exec Test.[119DataValidation_Fact_KPI] 'ADHOC', '2016-05-04'
CREATE PROCEDURE [Test].[119DataValidation_Fact_KPI] @TestRunNo varchar(23), @FromDate datetime

AS

Declare @DayKey int
Declare @DayZoneKey1 char(10)
Declare @DayZoneKey2 char(10)
Declare @DayZone1 int
Declare @DayZone2 int

If @FromDate is not null
Begin
	Set @DayKey = (Select DayKey From [$(Dimensional)].Dimension.[Day] where daydate = @FromDate)
	set @DayZoneKey1 = Cast (@DayKey as char(8)) + '00'
	set @DayZoneKey2 = Cast (@DayKey as char(8)) + '01'
	set @DayZone1 = cast (@DayZoneKey1 as int)
	set @DayZone2 = cast (@DayZoneKey2 as int)
End

SELECT	top 10 KPI.*
Into	#KPI
FROM	[$(Dimensional)].[Fact].[KPI] as KPI
		inner join [$(Dimensional)].dimension.dayZone as DayZone on KPI.DayKey = DayZone.DayKey
Where	(
		DayZone.DayKey = @DayZone1
		or
		DayZone.DayKey = @DayZone2
		Or
		@FromDate is null
		)

--no tests required for Keys or metadata
--ContractKey
--DayKey
--AccountKey
--AccountStatusKey
--BetTypeKey
--LegTypeKey
--ChannelKey
--CampaignKey
--MarketKey
--EventKey
--WalletKey
--ClassKey
--UserKey
--InstrumentKey
--InPlayKey

--[AccountOpened]  No Test as Foreign Key
--[FirstDeposit] No Test as Foreign Key
--[FirstBet]  No Test as Foreign Key
--[FirstInPlayBet] No Test as Foreign Key
--[DepositsRequested]
If not exists	(
				SELECT	*
				FROM	#KPI
				Where	DepositsRequested is null
						or DepositsRequested < 0
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '119', 'DataValidation_Fact_KPI', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '119', 'DataValidation_Fact_KPI', 'Fail', 'Errors in the DepositsRequested column')
End

--[DepositsCompleted]
If not exists	(
				SELECT	*
				FROM	#KPI
				Where	DepositsCompleted is null
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '119.1', 'DataValidation_Fact_KPI', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '119.1', 'DataValidation_Fact_KPI', 'Fail', 'Errors in the DepositsCompleted column')
End

--[Promotions]
If not exists	(
				SELECT	*
				FROM	#KPI
				Where	Promotions is null
						or Promotions < 0
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '119.2', 'DataValidation_Fact_KPI', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '119.2', 'DataValidation_Fact_KPI', 'Fail', 'Errors in the Promotions column')
End

--[Transfers]
If not exists	(
				SELECT	*
				FROM	#KPI
				Where	Transfers is null
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '119.3', 'DataValidation_Fact_KPI', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '119.3', 'DataValidation_Fact_KPI', 'Fail', 'Errors in the Transfers column')
End

--[BetsPlaced]  No Test as Foreign Key
--[ContractsPlaced]  No Test as Foreign Key
--[BettorsPlaced]  No Test as Foreign Key
--[PlayDaysPlaced]  No Test as Foreign Key
--[PlayFiscalWeeksPlaced]  No Test as Foreign Key
--[PlayCalenderWeeksPlaced]  No Test as Foreign Key
--[PlayFiscalMonthsPlaced]  No Test as Foreign Key
--[PlayCalenderMonthsPlaced]  No Test as Foreign Key
--[Stakes]
If not exists	(
				SELECT	*
				FROM	#KPI
				Where	Stakes is null
				--stake is normally positive, but can be negative due to a reversal
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '119.4', 'DataValidation_Fact_KPI', 'Pass', null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-5549'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '119.4', 'DataValidation_Fact_KPI', 'Pass', 'CCIA-5549')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '119.4', 'DataValidation_Fact_KPI', 'Fail', 'Errors in the Stakes column')
	End
End

--[Winnings]
If not exists	(
				SELECT	*
				FROM	#KPI
				Where	Winnings is null
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '119.5', 'DataValidation_Fact_KPI', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '119.5', 'DataValidation_Fact_KPI', 'Fail', 'Errors in the Winnings column')
End

--[CashOut]
If not exists	(
				SELECT	*
				FROM	#KPI
				Where	CashOut is null
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '119.16', 'DataValidation_Fact_KPI', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '119.16', 'DataValidation_Fact_KPI', 'Fail', 'Errors in the CashOut column')
End

--[CashOutDifferential]
If not exists	(
				SELECT	*
				FROM	#KPI
				Where	CashOutDifferential is null
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '119.17', 'DataValidation_Fact_KPI', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '119.17', 'DataValidation_Fact_KPI', 'Fail', 'Errors in the CashOutDifferential column')
End

--[WithdrawalsRequested]
If not exists	(
				SELECT	*
				FROM	#KPI
				Where	WithdrawalsRequested is null
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '119.6', 'DataValidation_Fact_KPI', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '119.6', 'DataValidation_Fact_KPI', 'Fail', 'Errors in the WithdrawalsRequested column')
End

--[WithdrawalsCompleted]
If not exists	(
				SELECT	*
				FROM	#KPI
				Where	WithdrawalsCompleted is null
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '119.7', 'DataValidation_Fact_KPI', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '119.7', 'DataValidation_Fact_KPI', 'Fail', 'Errors in the WithdrawalsCompleted column')
End

--[Adjustments]
If not exists	(
				SELECT	*
				FROM	#KPI
				Where	Adjustments is null
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '119.8', 'DataValidation_Fact_KPI', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '119.8', 'DataValidation_Fact_KPI', 'Fail', 'Errors in the Adjustments column')
End

--[BetsResulted]  No Test as Foreign Key
--[ContractsResulted]  No Test as Foreign Key
--[BettorsResulted]  No Test as Foreign Key
--[PlayDaysResulted]  No Test as Foreign Key
--[PlayFiscalWeeksResulted]  No Test as Foreign Key
--[PlayCalenderWeeksResulted]  No Test as Foreign Key
--[PlayFiscalMonthsResulted]  No Test as Foreign Key
--[PlayCalenderMonthsResulted]  No Test as Foreign Key
--[Turnover]
If not exists	(
				SELECT	*
				FROM	#KPI
				Where	Turnover is null
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '119.9', 'DataValidation_Fact_KPI', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '119.9', 'DataValidation_Fact_KPI', 'Fail', 'Errors in the Turnover column')
End

--[GrossWin]
If not exists	(
				SELECT	*
				FROM	#KPI
				Where	GrossWin is null
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '119.10', 'DataValidation_Fact_KPI', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '119.10', 'DataValidation_Fact_KPI', 'Fail', 'Errors in the GrossWin column')
End

--[NetRevenue]
If not exists	(
				SELECT	*
				FROM	#KPI
				Where	NetRevenue is null
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '119.11', 'DataValidation_Fact_KPI', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '119.11', 'DataValidation_Fact_KPI', 'Fail', 'Errors in the NetRevenue column')
End

--[NetRevenueAdjustment]
If not exists	(
				SELECT	*
				FROM	#KPI
				Where	NetRevenueAdjustment is null
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '119.12', 'DataValidation_Fact_KPI', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '119.12', 'DataValidation_Fact_KPI', 'Fail', 'Errors in the NetRevenueAdjustment column')
End

--[BonusWinnings]
If not exists	(
				SELECT	*
				FROM	#KPI
				Where	BonusWinnings is null
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '119.13', 'DataValidation_Fact_KPI', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '119.13', 'DataValidation_Fact_KPI', 'Fail', 'Errors in the BonusWinnings column')
End

--[Betback]
If not exists	(
				SELECT	*
				FROM	#KPI
				Where	Betback is null
						or Betback < 0
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '119.14', 'DataValidation_Fact_KPI', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '119.14', 'DataValidation_Fact_KPI', 'Fail', 'Errors in the Betback column')
End

--[GrossWinAdjustment]
If not exists	(
				SELECT	*
				FROM	#KPI
				Where	GrossWinAdjustment is null
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '119.15', 'DataValidation_Fact_KPI', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '119.15', 'DataValidation_Fact_KPI', 'Fail', 'Errors in the GrossWinAdjustment column')
End

--StructureKey

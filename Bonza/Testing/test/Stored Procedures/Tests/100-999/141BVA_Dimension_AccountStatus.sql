﻿--Test Code
--exec Test.[141BVA_Dimension_AccountStatus] 'ADHOC', '2015-04-03'

Create Procedure Test.[141BVA_Dimension_AccountStatus] @TestRunNo varchar(23), @FromDate datetime

as

Declare @ErrorText varchar(100)
If @FromDate is not null
Begin
Set @ErrorText = ' since ' + Convert (varchar(20), @FromDate, 103)
End
else
Begin
Set @ErrorText = '' 
End

Select		* 
into		#AccountStatus
From		[$(Dimensional)].Dimension.AccountStatus
where		AccountKey not in (0,-1)
			and accounttype != 'Test'
			and ((FromDate > @FromDate)
					or (@FromDate is null))
			and ToDate = '9999-12-31'

--[AccountStatusKey] n/a
--[AccountKey] n/a
--[LedgerID]
If not exists(
				Select		AccountStatusKey
				From		#AccountStatus
				where		LedgerID < 1
			) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '141', 'BVA_Dimension_AccountStatus LedgerID', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '141', 'BVA_Dimension_AccountStatus LedgerID', 'Fail', 'There are records with an invalid LedgerID' + @ErrorText)
End

--[LedgerSource]
If not exists(
				Select		AccountStatusKey
				From		#AccountStatus
				where		LedgerSource is null or LedgerSource = ''
			) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.1', 'BVA_Dimension_AccountStatus LedgerSource', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.1', 'BVA_Dimension_AccountStatus LedgerSource', 'Fail', 'There are records with an invalid LedgerSource' + @ErrorText)
End

--[LedgerEnabled]
If not exists(
				Select		AccountStatusKey
				From		#AccountStatus
				where		LedgerEnabled not in ('No', 'Yes')
			) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.2', 'BVA_Dimension_AccountStatus LedgerEnabled', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.2', 'BVA_Dimension_AccountStatus LedgerEnabled', 'Fail', 'There are records with an invalid LedgerEnabled' + @ErrorText)
End

--[LedgerStatus]
If not exists(
				Select		AccountStatusKey
				From		#AccountStatus
				where		LedgerStatus not in ('A', 'B', 'C', 'D', 'U', 'N')
			) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.3', 'BVA_Dimension_AccountStatus LedgerStatus', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.3', 'BVA_Dimension_AccountStatus LedgerStatus', 'Fail', 'There are records with an invalid LedgerStatus' + @ErrorText)
End

--[AccountType]
If not exists(
				Select		AccountStatusKey
				From		#AccountStatus
				where		AccountType not in ('Client', 'Betback', 'Test')
			) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.4', 'BVA_Dimension_AccountStatus AccountType', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.4', 'BVA_Dimension_AccountStatus AccountType', 'Fail', 'There are records with an invalid AccountType' + @ErrorText)
End

--[InternetProfile]
If not exists(
				Select		AccountStatusKey
				From		#AccountStatus
				where		InternetProfile is null or InternetProfile = ''
			) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.5', 'BVA_Dimension_AccountStatus InternetProfile', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.5', 'BVA_Dimension_AccountStatus InternetProfile', 'Fail', 'There are records with an invalid InternetProfile' + @ErrorText)
End

--[VIP]
If not exists(
				Select		AccountStatusKey
				From		#AccountStatus
				where		VIP not in ('Yes', 'No')
			) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.6', 'BVA_Dimension_AccountStatus VIP', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.6', 'BVA_Dimension_AccountStatus VIP', 'Fail', 'There are records with an invalid VIP' + @ErrorText)
End

--[CompanyKey] n/a as Key
--[CreditLimit]
If not exists(
				Select		AccountStatusKey
				From		#AccountStatus
				where		CreditLimit < 0
			) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.7', 'BVA_Dimension_AccountStatus CreditLimit', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.7', 'BVA_Dimension_AccountStatus CreditLimit', 'Fail', 'There are records with an invalid CreditLimit' + @ErrorText)
End

--[MinBetAmount]
If not exists(
				Select		AccountStatusKey
				From		#AccountStatus
				where		MinBetAmount < 0
			) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.8', 'BVA_Dimension_AccountStatus MinBetAmount', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.8', 'BVA_Dimension_AccountStatus MinBetAmount', 'Fail', 'There are records with an invalid MinBetAmount' + @ErrorText)
End

--[MaxBetAmount]
If not exists(
				Select		AccountStatusKey
				From		#AccountStatus
				where		MaxBetAmount < 0
			) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.9', 'BVA_Dimension_AccountStatus MaxBetAmount', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.9', 'BVA_Dimension_AccountStatus MaxBetAmount', 'Fail', 'There are records with an invalid MaxBetAmount' + @ErrorText)
End

--[Introducer]
If not exists(
				Select		AccountStatusKey
				From		#AccountStatus
				where		Introducer is null or LedgerSource = ''
			) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.10', 'BVA_Dimension_AccountStatus Introducer', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.10', 'BVA_Dimension_AccountStatus Introducer', 'Fail', 'There are records with an invalid Introducer' + @ErrorText)
End

--[Handled]
If not exists(
				Select		AccountStatusKey
				From		#AccountStatus
				where		Handled not in ('Yes', 'No')
			) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.11', 'BVA_Dimension_AccountStatus Handled', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.11', 'BVA_Dimension_AccountStatus Handled', 'Fail', 'There are records with an invalid Handled' + @ErrorText)
End

--[PhoneColourDesc]
If not exists(
				Select		AccountStatusKey
				From		#AccountStatus
				where		PhoneColourDesc is null or PhoneColourDesc = ''
			) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.12', 'BVA_Dimension_AccountStatus PhoneColourDesc', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.12', 'BVA_Dimension_AccountStatus PhoneColourDesc', 'Fail', 'There are records with an invalid DisableDeposit' + @ErrorText)
End

--[DisableDeposit]
If not exists(
				Select		AccountStatusKey
				From		#AccountStatus
				where		DisableDeposit not in ('Yes', 'No')
			) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.13', 'BVA_Dimension_AccountStatus DisableDeposit', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.13', 'BVA_Dimension_AccountStatus DisableDeposit', 'Fail', 'There are records with an invalid DisableDeposit' + @ErrorText)
End

--[DisableWithdrawal]
If not exists(
				Select		AccountStatusKey
				From		#AccountStatus
				where		DisableWithdrawal not in ('Yes', 'No')
			) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.14', 'BVA_Dimension_AccountStatus DisableWithdrawal', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.14', 'BVA_Dimension_AccountStatus DisableWithdrawal', 'Fail', 'There are records with an invalid DisableWithdrawal' + @ErrorText)
End

--[AccountClosedByUserID]
IF not exists	(
				SELECT		*
				FROM		#AccountStatus
				Where		AccountClosedByUserID != 0 and AccountClosedDate is null
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.15', 'BVA_Dimension_AccountStatus AccountClosedByUserID', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.15', 'BVA_Dimension_AccountStatus TAccountClosedByUserID', 'Fail', 'Invalid record in AccountClosedByUserID')
End

--[AccountClosedByUserSource] 
IF not exists	(
				SELECT		*
				FROM		#AccountStatus
				Where		(AccountClosedByUserID != 0 and AccountClosedByUserSource = 'N/A')
							or (AccountClosedByUserID != 0 and AccountClosedByUserSource = '')
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.16', 'BVA_Dimension_AccountStatus AccountClosedByUserSource', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.16', 'BVA_Dimension_AccountStatus AccountClosedByUserSource', 'Fail', 'Invalid record in AccountClosedByUserSource')
End

--[AccountClosedByUserName]
IF not exists	(
				SELECT		*
				FROM		#AccountStatus
				Where		(AccountClosedByUserID = 0 and AccountClosedByUserName = 'N/A')
							or (AccountClosedByUserID != 0 and AccountClosedByUserName = '')
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.17', 'BVA_Dimension_AccountStatus AccountClosedByUserName', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-5763'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '141.17', 'BVA_Dimension_AccountStatus AccountClosedByUserName', 'Pass', 'CCIA-5763')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '141.17', 'BVA_Dimension_AccountStatus AccountClosedByUserName', 'Fail', 'Invalid record in AccountClosedByUserName')
	End
End

--[AccountClosedDate]
IF not exists	(
				SELECT		*
				FROM		#AccountStatus
				Where		(AccountClosedDate is null and AccountClosedByUserID != 0)
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.18', 'BVA_Dimension_AccountStatus AccountClosedDate', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.18', 'BVA_Dimension_AccountStatus AccountClosedDate', 'Fail', 'Invalid record in AccountClosedDate')
End

--[ReIntroducedBy]
IF not exists	(
				SELECT		*
				FROM		#AccountStatus
				Where		(ReIntroducedBy = 0 and ReIntroducedDate is not null)
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.19', 'BVA_Dimension_AccountStatus ReIntroducedBy', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-5764'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '141.19', 'BVA_Dimension_AccountStatus ReIntroducedBy', 'Pass', 'CCIA-5764')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '141.19', 'BVA_Dimension_AccountStatus ReIntroducedBy', 'Fail', 'Invalid record in ReIntroducedBy')
	End
End

--[ReIntroducedDate]
IF not exists	(
				SELECT		*
				FROM		#AccountStatus
				Where		(ReIntroducedBy != 0 and ReIntroducedDate is null)
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.20', 'BVA_Dimension_AccountStatus ReIntroducedDate', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-5766'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '141.20', 'BVA_Dimension_AccountStatus ReIntroducedDate', 'Pass', 'CCIA-5766')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '141.20', 'BVA_Dimension_AccountStatus ReIntroducedDate', 'Fail', 'Invalid record in ReIntroducedDate')
	End
End

--[AccountClosureReason]
IF not exists	(
				SELECT		*
				FROM		#AccountStatus
				Where		AccountClosedDate is not null
							and AccountClosureReason = 'N/A'
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.21', 'BVA_Dimension_AccountStatus AccountClosureReason', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-5900'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '141.21', 'BVA_Dimension_AccountStatus AccountClosureReason', 'Pass', 'CCIA-5900')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '141.21', 'BVA_Dimension_AccountStatus AccountClosureReason', 'Fail', 'Invalid record in AccountClosureReason')
	End
End

--[DepositLimit]
IF not exists	(
				SELECT		*
				FROM		#AccountStatus
				Where		DepositLimit < -1
							and LedgerID != 700602 --test account with destructive test data
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.22', 'BVA_Dimension_AccountStatus DepositLimit', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.22', 'BVA_Dimension_AccountStatus DepositLimit', 'Fail', 'Invalid record in DepositLimit')
End

--[LossLimit]
IF not exists	(
				SELECT		*
				FROM		#AccountStatus
				Where		LossLimit < -1
							and LedgerID != 700602 --test account with destructive test data
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.23', 'BVA_Dimension_AccountStatus LossLimit', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.23', 'BVA_Dimension_AccountStatus LossLimit', 'Fail', 'Invalid record in LossLimit')
End

--[LimitPeriod]
IF not exists	(
				SELECT		*
				FROM		#AccountStatus
				Where		LimitPeriod < -1
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.24', 'BVA_Dimension_AccountStatus LimitPeriod', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.24', 'BVA_Dimension_AccountStatus LimitPeriod', 'Fail', 'Invalid record in LimitPeriod')
End


--[LimitTimeStamp] n/a any date is possible, and even if limit period is 0, it could be when the date it changed back to this

--[ReceiveMarketingEmail]
IF not exists	(
				SELECT		*
				FROM		#AccountStatus
				Where		ReceiveMarketingEmail not in ('Yes', 'No')
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.27', 'BVA_Dimension_AccountStatus ReceiveMarketingEmail', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.27', 'BVA_Dimension_AccountStatus ReceiveMarketingEmail', 'Fail', 'Invalid record in ReceiveMarketingEmail')
End

--[ReceiveCompetitionEmail]
IF not exists	(
				SELECT		*
				FROM		#AccountStatus
				Where		ReceiveCompetitionEmail not in ('Yes', 'No')
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.28', 'BVA_Dimension_AccountStatus ReceiveCompetitionEmail', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.28', 'BVA_Dimension_AccountStatus ReceiveCompetitionEmail', 'Fail', 'Invalid record in ReceiveCompetitionEmail')
End

--[ReceiveSMS]
IF not exists	(
				SELECT		*
				FROM		#AccountStatus
				Where		ReceiveSMS not in ('Yes', 'No')
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.29', 'BVA_Dimension_AccountStatus ReceiveSMS', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.29', 'BVA_Dimension_AccountStatus ReceiveSMS', 'Fail', 'Invalid record in ReceiveSMS')
End

--[ReceivePostalMail]
IF not exists	(
				SELECT		*
				FROM		#AccountStatus
				Where		ReceiveSMS not in ('Yes', 'No')
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.30', 'BVA_Dimension_AccountStatus ReceivePostalMail', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.30', 'BVA_Dimension_AccountStatus ReceivePostalMail', 'Fail', 'Invalid record in ReceivePostalMail')
End

--[ReceiveFax]
IF not exists	(
				SELECT		*
				FROM		#AccountStatus
				Where		ReceiveFax not in ('Yes', 'No')
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.31', 'BVA_Dimension_AccountStatus ReceiveFax', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.31', 'BVA_Dimension_AccountStatus ReceiveFax', 'Fail', 'Invalid record in ReceiveFax')
End

--[CurrentVersion]
IF not exists	(
				SELECT		*
				FROM		#AccountStatus
				Where		(CurrentVersion = 'Yes' and ToDate < '9999-12-30 00:00:00.000')
							or (CurrentVersion = 'No' and ToDate >= '9999-12-30 00:00:00.000')
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.32', 'BVA_Dimension_AccountStatus CurrentVersion', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.32', 'BVA_Dimension_AccountStatus CurrentVersion', 'Fail', 'Invalid record in CurrentVersion')
End

--FirstDate FromDate
Declare @Counter int
Set @Counter = 	(
				Select		Count(*) as Counters
				From		(
							select		AccountKey, Min(FromDate) as FromDate, FirstDate
							from		[$(Dimensional)].Dimension.AccountStatus as Account
							where		AccountStatusKey not in (0, -1)
							Group by	AccountKey, FirstDate
							Having		Min(FromDate) != FirstDate
							) as Data
				)
If @Counter = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.33', 'BVA_Dimension_AccountStatus', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-7664'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '141.33', 'BVA_Dimension_AccountStatus', 'Pass', 'CCIA-7664')
	End
	Else
	Begin
			Insert into test.tblResults Values (@TestRunNo, '141.33', 'BVA_Dimension_AccountStatus', 'Fail', 'There are accounts which do not have a history from start date')
	End
End

--[FromDate] [ToDate] Type 2 Continuious
Set @Counter = 	(
				Select		Count(*) as Counters
				From		(
							select		AccountKey, ToDate
							from		[$(Dimensional)].Dimension.AccountStatus as Account
							where		AccountStatusKey not in (0, -1)
										and ToDate < '9999-12-30'
							Group by	AccountKey, ToDate
							except
							select		AccountKey, FromDate
							from		[$(Dimensional)].Dimension.AccountStatus as Account
							where		AccountStatusKey not in (0, -1)
										and FromDate != FirstDate
							Group by	AccountKey, FromDate
							) as Data
				)
If @Counter = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.34', 'BVA_Dimension_AccountStatus', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '141.34', 'BVA_Dimension_AccountStatus', 'Fail', 'FromDate - ToDates are not continuous')
End

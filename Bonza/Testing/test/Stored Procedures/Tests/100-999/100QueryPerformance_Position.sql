﻿--Test Code
--exec Test.[100QueryPerformance_Position] 'JF_5May_33'
Create Procedure [Test].[100QueryPerformance_Position] @TestRunNo varchar(23)

as

Declare @Time int

--Simple
exec [Test].[spExecuteMDX] @TestRunNo, 'Position', 'Simple', 10000,
	' Select * From OpenQuery([ANALYSISSERVICES],''
		SELECT 
		NON EMPTY
		{[Measures].[TurnoverPlaced],[Measures].[GrossWin]} on Columns,
		[Company].[CompanyName].[CompanyName] on rows
		FROM [Bonza]
		WHERE 
		([Day].[FiscalYearStartDate].&[2014-12-31T00:00:00])
	'')'


Set @Time = 	(
				Select		MSeconds
				From		[Test].[tblQueryPerformance]
				where		[RunID] = @TestRunNo
							and [TestID] = 'Position'
							and [TestName] = 'Simple'
				)
IF @Time <= 10000
Begin
	Insert into test.tblResults ([RunID], [TestID], [TestName], [Status], [Notes]) Values (@TestRunNo, '100', 'QueryPerformance_Position_Simple', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults ([RunID], [TestID], [TestName], [Status], [Notes]) Values (@TestRunNo, '100', 'QueryPerformance_Position_Simple', 'Fail', 'Time taken is ' + cast (@Time as varchar(100)) + ' msec and should be less than 10000')
End

--Medium
exec [Test].[spExecuteMDX] @TestRunNo, 'Position', 'Medium', 60000,
	' Select * From OpenQuery([ANALYSISSERVICES],''
		 WITH MEMBER [Measures].[NetValue] AS [Measures].[Max LifeTimeGrossWin] - [Measures].[Max LifeTimeTurnover]
         SELECT 
         NON EMPTY
         {[Measures].[Max LifeTimeGrossWin],[Measures].[Max LifeTimeTurnover]} on Columns,
         CROSSJOIN([Company].[BusinessUnitName].[BusinessUnitName],[Day].[FiscalWeekStartDate].MEMBERS) on rows
         FROM [Bonza]
         WHERE 
         {([Day].[FiscalMonthText].&[Mar-2015],[Company].[CompanyName].&[William Hill Australia])
		 , ([Day].[FiscalMonthText].&[Apr-2015],[Company].[CompanyName].&[William Hill Australia])}
	'')'


Set @Time = 	(
				Select		MSeconds
				From		[Test].[tblQueryPerformance]
				where		[RunID] = @TestRunNo
							and [TestID] = 'Position'
							and [TestName] = 'Medium'
				)
IF @Time <= 60000
Begin
	Insert into test.tblResults ([RunID], [TestID], [TestName], [Status], [Notes]) Values (@TestRunNo, '100.1', 'QueryPerformance_Position_Medium', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults ([RunID], [TestID], [TestName], [Status], [Notes]) Values (@TestRunNo, '100.1', 'QueryPerformance_Position_Medium', 'Fail', 'Time taken is ' + cast (@Time as varchar(100)) + ' msec and should be less than 60000')
End

--Complex
exec [Test].[spExecuteMDX] @TestRunNo, 'Position', 'Complex', 1200000,
	' Select * From OpenQuery([ANALYSISSERVICES],''
		 WITH MEMBER [Measures].[NetValue] AS [Measures].[Max LifeTimeGrossWin] - [Measures].[Max LifeTimeTurnover]
         SELECT 
         NON EMPTY
         {[Measures].[GrossWin],[Measures].[Max LifeTimeGrossWin],[Measures].[Max LifeTimeTurnover]} on Columns,
         CROSSJOIN([Company].[BusinessUnitName].[BusinessUnitName],[Day].[FiscalWeekStartDate].MEMBERS) on rows
         FROM [Bonza]
         WHERE 
         {([Day].[FiscalMonthText].&[Mar-2015],[Company].[CompanyName].&[William Hill Australia])
		 , ([Day].[FiscalMonthText].&[Apr-2015],[Company].[CompanyName].&[William Hill Australia])}
	'')'


Set @Time = 	(
				Select		MSeconds
				From		[Test].[tblQueryPerformance]
				where		[RunID] = @TestRunNo
							and [TestID] = 'Position'
							and [TestName] = 'Complex'
				)
IF @Time <= 1200000
Begin
	Insert into test.tblResults ([RunID], [TestID], [TestName], [Status], [Notes]) Values (@TestRunNo, '100.2', 'QueryPerformance_Position_Complex', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults ([RunID], [TestID], [TestName], [Status], [Notes]) Values (@TestRunNo, '100.2', 'QueryPerformance_Position_Complex', 'Fail', 'Time taken is ' + cast (@Time as varchar(100)) + ' msec and should be less than 1200000')
End

GO



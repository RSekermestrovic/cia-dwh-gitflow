﻿--test
--exec [Test].[135DataDuplicates_Dimension_Activity] 'ADHOC', null

CREATE PROCEDURE [Test].[135DataDuplicates_Dimension_Activity] @TestRunNo varchar(23), @FromDate datetime

as


If not exists(
		Select		Count(*)
		From		[$(Dimensional)].Dimension.Activity as Activity
		where		@FromDate is null
					or CreatedDate >= @FromDate
		Group by	HasBet, HasDeposited, HasLost, HasWon, HasWithdrawn
		Having		Count(*) > 1
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '135', 'DataDuplicates_Dimension_Activity', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '135', 'DataDuplicates_Dimension_Activity', 'Fail', 'Duplicates of Activity found')
End

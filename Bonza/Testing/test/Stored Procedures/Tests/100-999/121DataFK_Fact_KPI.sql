﻿--Test Code
--exec Test.[121DataFK_Fact_KPI] 'ADHOC', '2016-02-02'

Create Procedure [Test].[121DataFK_Fact_KPI] @TestRunNo varchar(23), @FromDate datetime

as

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

Select		KPI.*
into		#KPI
From		[$(Dimensional)].Fact.KPI as KPI with (nolock)
			inner join [$(Dimensional)].Dimension.DayZone as DayZone on KPI.DayKey = DayZone.DayKey
			inner join [$(Dimensional)].Dimension.[Day] as [Day] on DayZone.MasterDayKey = [Day].DayKey
where		[Day].DayDate = Cast (@FromDate as Date)
			or @FromDate is null	

Select		t.*, [Contract].BetId, [Contract].BetGroupID
into		#Transaction
From		[$(Dimensional)].Fact.[Transaction] as t with (nolock)
			inner join [$(Dimensional)].Dimension.DayZone as DayZone on t.DayKey = DayZone.DayKey
			inner join [$(Dimensional)].Dimension.[Day] as [Day] on DayZone.MasterDayKey = [Day].DayKey
			inner join [$(Dimensional)].Dimension.[Contract] as [Contract] on [Contract].ContractKey = t.ContractKey
where		[Day].DayDate = Cast (@FromDate as Date)
			or @FromDate is null	

--ContractKey
If not exists(
				Select		KPI.AccountKey
				From		#KPI as KPI
							left join [$(Dimensional)].Dimension.[Contract] as [Contract] on KPI.ContractKey = [Contract].ContractKey
				Where		[Contract].ContractKey is null	
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.0', 'DataFK_Fact_KPI Contract', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.0', 'DataFK_Fact_KPI Contract', 'Fail', 'Missing ContractKeys')
End

If not exists(
				Select		KPI.AccountKey
				From		#KPI as KPI
				Where		KPI.ContractKey = -1		
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.1', 'DataFK_Fact_KPI Contract', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.1', 'DataFK_Fact_KPI Contract', 'Fail', 'Missing ContractKeys')
End

--DayKey
If not exists(
				Select		KPI.DayKey
				From		[$(Dimensional)].Fact.KPI as KPI
							left join [$(Dimensional)].Dimension.[DayZone] as [DayZone] on KPI.DayKey = DayZone.DayKey
				Where		[DayZone].DayKey is null
							and (
								(KPI.CreatedDate >= @FromDate)
								or (@FromDate is null)
								)				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.2', 'DataFK_Fact_KPI Day', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.2', 'DataFK_Fact_KPI Day', 'Fail', 'Missing DayKeys')
End

If not exists(
				Select		KPI.DayKey
				From		[$(Dimensional)].Fact.KPI as KPI
				Where		KPI.DayKey = -1
							and (
								(KPI.CreatedDate >= @FromDate)
								or (@FromDate is null)
								)				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.3', 'DataFK_Fact_KPI Day', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-5445'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '121.3', 'DataFK_Fact_KPI Day', 'Pass', 'CCIA-5445')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '121.3', 'DataFK_Fact_KPI Day', 'Fail', 'Missing DayKeys')
	End
End

--AccountKey
If not exists(
				Select		KPI.AccountKey
				From		#KPI as KPI
							left join [$(Dimensional)].Dimension.Account as Account on KPI.AccountKey = Account.AccountKey
				Where		Account.AccountKey is null		
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.4', 'DataFK_Fact_KPI Account', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.4', 'DataFK_Fact_KPI Account', 'Fail', 'Missing AccountKeys')
End

If not exists(
				Select		KPI.AccountKey
				From		#KPI as KPI
				Where		KPI.AccountKey = -1	
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.5', 'DataFK_Fact_KPI Account', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.5', 'DataFK_Fact_KPI Account', 'Fail', 'Missing AccountKeys')
End

--AccountStatusKey
If not exists(
				Select		KPI.AccountStatusKey
				From		#KPI as KPI
							left join [$(Dimensional)].Dimension.AccountStatus as AccountStatus on KPI.AccountStatusKey = AccountStatus.AccountStatusKey
				Where		AccountStatus.AccountStatusKey is null			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.6', 'DataFK_Fact_KPI AccountStatus', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.6', 'DataFK_Fact_KPI AccountStatus', 'Fail', 'Missing AccountStatusKeys')
End

If not exists(
				Select		KPI.AccountStatusKey
				From		#KPI as KPI
				Where		KPI.AccountStatusKey = -1			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.7', 'DataFK_Fact_KPI AccountStatus', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-5428'
							and [Status] != 'Done'
				)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '121.7', 'DataFK_Fact_KPI AccountStatus', 'Pass', 'CCIA-5428')
	End
	Else
	Begin 
		Insert into test.tblResults Values (@TestRunNo, '121.7', 'DataFK_Fact_KPI AccountStatus', 'Fail', 'Missing AccountStatusKeys')
	End
End

--BetTypeKey
If not exists(
				Select		KPI.BetTypeKey
				From		#KPI as KPI
							left join [$(Dimensional)].Dimension.BetType as BetType on KPI.BetTypeKey = BetType.BetTypeKey
				Where		BetType.BetTypeKey is null			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.8', 'DataFK_Fact_KPI BetType', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.8', 'DataFK_Fact_KPI BetType', 'Fail', 'Missing BetTypeKeys')
End

If not exists(
				Select		KPI.BetTypeKey
				From		#KPI as KPI
				Where		KPI.BetTypeKey = -1				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.9', 'DataFK_Fact_KPI BetType', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.9', 'DataFK_Fact_KPI BetType', 'Fail', 'Missing BetTypeKeys')
End

--LegTypeKey
If not exists(
				Select		KPI.LegTypeKey
				From		#KPI as KPI
							left join [$(Dimensional)].Dimension.LegType as LegType on KPI.LegTypeKey = LegType.LegTypeKey
				Where		LegType.LegTypeKey is null			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.10', 'DataFK_Fact_KPI LegType', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.10', 'DataFK_Fact_KPI LegType', 'Fail', 'Missing LegTypeKeys')
End

If not exists(
				Select		KPI.LegTypeKey
				From		#KPI as KPI
				Where		KPI.LegTypeKey = -1			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.11', 'DataFK_Fact_KPI LegType', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.11', 'DataFK_Fact_KPI LegType', 'Fail', 'Missing LegTypeKeys')
End

--ChannelKey
If not exists(
				Select		KPI.ChannelKey
				From		#KPI as KPI
							left join [$(Dimensional)].Dimension.Channel as Channel on KPI.ChannelKey = Channel.ChannelKey
				Where		Channel.ChannelKey is null			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.12', 'DataFK_Fact_KPI Channel', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.12', 'DataFK_Fact_KPI Channel', 'Fail', 'Missing ChannelKeys')
End

If not exists(
				Select		*
				From		#KPI as KPI
				Where		KPI.ChannelKey = -1			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.13', 'DataFK_Fact_KPI Channel', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-5429'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '121.13', 'DataFK_Fact_KPI Channel', 'Pass', 'CCIA-5429')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '121.11', 'DataFK_Fact_KPI Channel', 'Fail', 'Missing ChannelKeys')
	End
End

--CampaignKey
If not exists(
				Select		KPI.CampaignKey
				From		#KPI as KPI
							left join [$(Dimensional)].Dimension.Campaign as Campaign on KPI.CampaignKey = Campaign.CampaignKey
				Where		Campaign.CampaignKey is null			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.14', 'DataFK_Fact_KPI Campaign', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.14', 'DataFK_Fact_KPI Campaign', 'Fail', 'Missing CampaignKeys')
End

If not exists(
				Select		KPI.CampaignKey
				From		#KPI as KPI
				Where		KPI.CampaignKey = -1		
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.15', 'DataFK_Fact_KPI Campaign', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.15', 'DataFK_Fact_KPI Campaign', 'Fail', 'Missing CampaignKeys')
End

--MarketKey
If not exists(
				Select		KPI.MarketKey
				From		#KPI as KPI
							left join [$(Dimensional)].Dimension.Market as Market on KPI.MarketKey = Market.MarketKey
				Where		Market.MarketKey is null			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.16', 'DataFK_Fact_KPI Market', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-5446'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '121.16', 'DataFK_Fact_KPI Market', 'Pass', 'CCIA-5446')
	End
	Else
	Begin 
		Insert into test.tblResults Values (@TestRunNo, '121.16', 'DataFK_Fact_KPI Market', 'Fail', 'Missing MarketKeys')
	End
End

If not exists(
				Select		KPI.MarketKey
				From		#KPI as KPI
				Where		KPI.MarketKey = -1			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.17', 'DataFK_Fact_KPI Market', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-5446'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '121.17', 'DataFK_Fact_KPI Market', 'Pass', 'CCIA-5446')
	End
	Else
	Begin 
		Insert into test.tblResults Values (@TestRunNo, '121.17', 'DataFK_Fact_KPI Market', 'Fail', 'Missing MarketKeys')
	End
End

--EventKey
If not exists(
				Select		KPI.EventKey
				From		#KPI as KPI
							left join [$(Dimensional)].Dimension.[Event] as [Event] on KPI.EventKey = [Event].EventKey
				Where		[Event].EventKey is null			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.18', 'DataFK_Fact_KPI Event', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.18', 'DataFK_Fact_KPI Event', 'Fail', 'Missing EventKeys')
End

If not exists(
				Select		KPI.EventKey
				From		#KPI as KPI
				Where		KPI.EventKey = -1			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.19', 'DataFK_Fact_KPI Event', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.19', 'DataFK_Fact_KPI Event', 'Fail', 'Missing EventKeys')
End

--ClassKey
If not exists(
				Select		KPI.ClassKey
				From		#KPI as KPI
							left join [$(Dimensional)].Dimension.Class as Class on KPI.ClassKey = Class.ClassKey
				Where		Class.ClassKey is null			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.22', 'DataFK_Fact_KPI Class', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.22', 'DataFK_Fact_KPI Class', 'Fail', 'Missing ClassKeys')
End

If not exists(
				Select		KPI.ClassKey
				From		#KPI as KPI
				Where		KPI.ClassKey = -1				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.23', 'DataFK_Fact_KPI Class', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.23', 'DataFK_Fact_KPI Class', 'Fail', 'Missing ClassKeys')
End

--UserKey
If not exists(
				Select		KPI.UserKey
				From		#KPI as KPI
							left join [$(Dimensional)].Dimension.[User] as [User] on KPI.UserKey = [User].UserKey
				Where		[User].UserKey is null				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.24', 'DataFK_Fact_KPI User', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.24', 'DataFK_Fact_KPI User', 'Fail', 'Missing UserKeys')
End

If not exists(
				Select		KPI.UserKey
				From		#KPI as KPI
				Where		KPI.UserKey = -1			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.25', 'DataFK_Fact_KPI User', 'Pass', NULL)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-5430'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '121.25', 'DataFK_Fact_KPI User', 'Pass', 'CCIA-5430')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '121.25', 'DataFK_Fact_KPI User', 'Fail', 'Missing UserKeys')
	End
End

--InstrumentKey
If not exists(
				Select		KPI.InstrumentKey
				From		#KPI as KPI
							left join [$(Dimensional)].Dimension.Instrument as Instrument on KPI.InstrumentKey = Instrument.InstrumentKey
				Where		Instrument.InstrumentKey is null				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.26', 'DataFK_Fact_KPI Instrument', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.26', 'DataFK_Fact_KPI Instrument', 'Fail', 'Missing InstrumentKeys')
End

If not exists(
				Select		KPI.InstrumentKey
				From		#KPI as KPI
				Where		KPI.InstrumentKey = -1			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.27', 'DataFK_Fact_KPI Instrument', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-5431'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '121.27', 'DataFK_Fact_KPI Instrument', 'Pass', 'CCIA-5431')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '121.27', 'DataFK_Fact_KPI Instrument', 'Fail', 'Missing InstrumentKeys')
	End
End

--InPlayKey
If not exists(
				Select		*
				From		#KPI as KPI
							left join [$(Dimensional)].Dimension.InPlay as InPlay on KPI.InPlayKey = InPlay.InPlayKey
				Where		InPlay.InPlayKey is null			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.28', 'DataFK_Fact_KPI InPlay', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-5772'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '121.28', 'DataFK_Fact_KPI InPlay', 'Pass', 'CCIA-5772')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '121.28', 'DataFK_Fact_KPI InPlay', 'Fail', 'Missing InPlayKeys')
	End
End

If not exists(
				Select		KPI.AccountKey
				From		#KPI as KPI
				Where		KPI.InPlayKey = -1				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.29', 'DataFK_Fact_KPI InPlay', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-5800'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '121.29', 'DataFK_Fact_KPI InPlay', 'Pass', 'CCIA-5800')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '121.29', 'DataFK_Fact_KPI InPlay', 'Fail', 'Missing InPlayKeys')
	End
End

--AccountOpened
If not exists(
				Select		KPI.AccountOpened
				From		#KPI as KPI
							left join [$(Dimensional)].Dimension.Account as Account on KPI.AccountOpened = Account.LedgerId
				Where		Account.LedgerId is null
							and KPI.AccountOpened is not null		
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.30', 'DataFK_Fact_KPI AccountOpened', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-5432'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '121.30', 'DataFK_Fact_KPI AccountOpened', 'Pass', 'CCIA-5432')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '121.30', 'DataFK_Fact_KPI AccountOpened', 'Fail', 'Missing AccountOpened')
	End
End

--FirstDeposit
If not exists(
				Select		*
				From		#KPI as KPI
							left join #Transaction as [Transaction] on KPI.FirstDeposit = [Transaction].TransactionId
				Where		[Transaction].TransactionId is null
							and KPI.FirstDeposit is not null			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.31', 'DataFK_Fact_KPI FirstDeposit', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.31', 'DataFK_Fact_KPI FirstDeposit', 'Fail', 'Missing FirstDeposit to TransactionID link')
End

If not exists(
				Select		KPI.AccountKey
				From		#KPI as KPI
				Where		KPI.FirstDeposit = -1			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.32', 'DataFK_Fact_KPI FirstDeposit', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.32', 'DataFK_Fact_KPI FirstDeposit', 'Fail', 'Missing FirstDepositKeys')
End

--FirstBet
If not exists(
				Select		KPI.AccountKey
				From		#KPI as KPI
							left join #Transaction as [Transaction] on KPI.FirstBet = [Transaction].BetId
				Where		[Transaction].BetId is null
							and KPI.FirstBet is not null			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.33', 'DataFK_Fact_KPI FirstBet', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.33', 'DataFK_Fact_KPI FirstBet', 'Fail', 'Missing FirstBet to TransactionID link')
End

If not exists(
				Select		KPI.AccountKey
				From		#KPI as KPI
				Where		KPI.FirstBet = -1				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.34', 'DataFK_Fact_KPI FirstBet', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.34', 'DataFK_Fact_KPI FirstBet', 'Fail', 'Missing FirstBetKeys')
End

--DepositsRequested n/a
--DepositsCompleted n/a
--Promotions n/a
--Transfers n/a

--BetsPlaced 
If not exists(
				Select		KPI.AccountKey
				From		#KPI as KPI
							left hash join #Transaction as [Transaction] on KPI.BetsPlaced = [Transaction].BetId
				Where		[Transaction].BetId is null
							and KPI.BetsPlaced is not null			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.35', 'DataFK_Fact_KPI BetsPlaced', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.35', 'DataFK_Fact_KPI BetsPlaced', 'Fail', 'Missing BetsPlaced to TransactionID link')
End

If not exists(
				Select		KPI.AccountKey
				From		#KPI as KPI
				Where		KPI.BetsPlaced = -1				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.36', 'DataFK_Fact_KPI BetsPlaced', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.36', 'DataFK_Fact_KPI BetsPlaced', 'Fail', 'Missing BetsPlacedKeys')
End

--ContractsPlaced
If not exists(
				Select		KPI.AccountKey
				From		#KPI as KPI
							left hash join #Transaction as [Transaction] on KPI.ContractsPlaced = [Transaction].BetGroupId
				Where		[Transaction].BetGroupId is null
							and KPI.ContractsPlaced is not null				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.37', 'DataFK_Fact_KPI ContractsPlaced', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.37', 'DataFK_Fact_KPI ContractsPlaced', 'Fail', 'Missing ContractsPlaced to TransactionID link')
End

If not exists(
				Select		KPI.AccountKey
				From		#KPI as KPI
				Where		KPI.ContractsPlaced = -1		
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.38', 'DataFK_Fact_KPI ContractsPlaced', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.38', 'DataFK_Fact_KPI ContractsPlaced', 'Fail', 'MissingContractsPlacedKeys')
End

--BettorsPlaced
If not exists(
				Select		*
				From		#KPI as KPI
							left join [$(Dimensional)].Dimension.Account as Account on KPI.BettorsPlaced = Account.LedgerID
				Where		KPI.BettorsPlaced is not null
							and Account.LedgerID is null				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.39', 'DataFK_Fact_KPI BettorsPlaced', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.39', 'DataFK_Fact_KPI BettorsPlaced', 'Fail', 'Missing BettorsPlaced to TransactionID link')
End

If not exists(
				Select		KPI.AccountKey
				From		#KPI as KPI
				Where		KPI.BettorsPlaced = -1				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.40', 'DataFK_Fact_KPI BettorsPlaced', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.40', 'DataFK_Fact_KPI BettorsPlaced', 'Fail', 'Missing BettorsPlacedKeys')
End

--PlayDaysPlaced
If not exists(
				Select		*
				From		#KPI as KPI
				Where		KPI.PlayDaysPlaced is not null
							and Cast (Left (DayKey, 8) as char(8)) + Cast (KPI.BettorsPlaced as varchar(10)) = KPI.PlayDaysPlaced				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.41', 'DataFK_Fact_KPI PlayDaysPlaced', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.41', 'DataFK_Fact_KPI PlayDaysPlaced', 'Fail', 'Missing PlayDaysPlaced link')
End

If not exists(
				Select		KPI.PlayDaysPlaced
				From		#KPI as KPI
				Where		KPI.PlayDaysPlaced = -1				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.42', 'DataFK_Fact_KPI PlayDaysPlaced', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.42', 'DataFK_Fact_KPI PlayDaysPlaced', 'Fail', 'Missing PlayDaysPlacedKeys')
End

--PlayFiscalWeeksPlaced
If not exists(
				Select		*
				From		#KPI as KPI
							left join [$(Dimensional)].Dimension.DayZone as DayZone on KPI.DayKey = DayZone.DayKey
							left join [$(Dimensional)].Dimension.[Day] as [Day] on DayZone.MasterDayKey = [Day].DayKey
				Where		KPI.PlayFiscalWeeksPlaced is not null
							and Cast ([Day].FiscalWeekKey as char(6)) + Cast (KPI.BettorsPlaced as varchar(10)) = KPI.PlayFiscalWeeksPlaced			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.43', 'DataFK_Fact_KPI PlayFiscalWeeksPlaced', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.43', 'DataFK_Fact_KPI PlayFiscalWeeksPlaced', 'Fail', 'Missing PlayFiscalWeeksPlaced link')
End

If not exists(
				Select		KPI.PlayFiscalWeeksPlaced
				From		#KPI as KPI
				Where		KPI.PlayFiscalWeeksPlaced = -1				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.44', 'DataFK_Fact_KPI PlayFiscalWeeksPlaced', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.44', 'DataFK_Fact_KPI PlayFiscalWeeksPlaced', 'Fail', 'Missing PlayFiscalWeeksPlacedKeys')
End

--PlayCalenderWeeksPlaced
If not exists(
				Select		*
				From		#KPI as KPI
							left join [$(Dimensional)].Dimension.DayZone as DayZone on KPI.DayKey = DayZone.DayKey
							left join [$(Dimensional)].Dimension.[Day] as [Day] on DayZone.MasterDayKey = [Day].DayKey
				Where		KPI.PlayCalenderWeeksPlaced is not null
							and Cast ([Day].WeekKey as char(6)) + Cast (KPI.BettorsPlaced as varchar(10)) = KPI.PlayCalenderWeeksPlaced				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.45', 'DataFK_Fact_KPI PlayCalenderWeeksPlaced', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.45', 'DataFK_Fact_KPI PlayCalenderWeeksPlaced', 'Fail', 'Missing PlayCalenderWeeksPlaced link')
End

If not exists(
				Select		KPI.PlayCalenderWeeksPlaced
				From		#KPI as KPI
				Where		KPI.PlayCalenderWeeksPlaced = -1			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.46', 'DataFK_Fact_KPI PlayCalenderWeeksPlaced', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.46', 'DataFK_Fact_KPI PlayCalenderWeeksPlaced', 'Fail', 'Missing PlayCalenderWeeksPlacedKeys')
End

--PlayFiscalMonthsPlaced
If not exists(
				Select		*
				From		#KPI as KPI
							left join [$(Dimensional)].Dimension.DayZone as DayZone on KPI.DayKey = DayZone.DayKey
							left join [$(Dimensional)].Dimension.[Day] as [Day] on DayZone.MasterDayKey = [Day].DayKey
				Where		KPI.PlayCalenderWeeksPlaced is not null
							and Cast ([Day].FiscalMonthKey as char(6)) + Cast (KPI.BettorsPlaced as varchar(10)) = KPI.PlayFiscalMonthsPlaced			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.47', 'DataFK_Fact_KPI PlayFiscalMonthsPlaced', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.47', 'DataFK_Fact_KPI PlayFiscalMonthsPlaced', 'Fail', 'Missing PlayFiscalMonthsPlaced link')
End

If not exists(
				Select		KPI.PlayFiscalMonthsPlaced
				From		#KPI as KPI
				Where		KPI.PlayFiscalMonthsPlaced = -1			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.48', 'DataFK_Fact_KPI PlayFiscalMonthsPlaced', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.48', 'DataFK_Fact_KPI PlayFiscalMonthsPlaced', 'Fail', 'Missing PlayFiscalMonthsPlaced Keys')
End

--PlayCalenderMonthsPlaced
If not exists(
				Select		*
				From		#KPI as KPI
							left join [$(Dimensional)].Dimension.DayZone as DayZone on KPI.DayKey = DayZone.DayKey
							left join [$(Dimensional)].Dimension.[Day] as [Day] on DayZone.MasterDayKey = [Day].DayKey
				Where		KPI.PlayCalenderWeeksPlaced is not null
							and Cast ([Day].MonthKey as char(6)) + Cast (KPI.BettorsPlaced as varchar(10)) = KPI.PlayCalenderMonthsPlaced			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.49', 'DataFK_Fact_KPI PlayCalenderMonthsPlaced', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.49', 'DataFK_Fact_KPI PlayCalenderMonthsPlaced', 'Fail', 'Missing PlayCalenderMonthsPlaced link')
End

If not exists(
				Select		KPI.PlayCalenderMonthsPlaced
				From		#KPI as KPI
				Where		KPI.PlayCalenderMonthsPlaced = -1			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.50', 'DataFK_Fact_KPI PlayCalenderMonthsPlaced', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.50', 'DataFK_Fact_KPI PlayCalenderMonthsPlaced', 'Fail', 'Missing PlayCalenderMonthsPlaced Keys')
End

--Stakes n/a
--Winnings  n/a

--BettorsCashedOut
If not exists(
				Select		*
				From		#KPI as KPI
							left join [$(Dimensional)].Dimension.Account as Account on KPI.BettorsCashedOut = Account.LedgerID
				Where		KPI.BettorsCashedOut is not null
							and Account.LedgerID is null				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.71', 'DataFK_Fact_KPI BettorsCashedOut', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.71', 'DataFK_Fact_KPI BettorsCashedOut', 'Fail', 'Missing BettorsCashedOut to LedgerID link')
End

If not exists(
				Select		KPI.BettorsCashedOut
				From		#KPI as KPI
				Where		KPI.BettorsCashedOut = -1				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.72', 'DataFK_Fact_KPI BettorsCashedOut', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.72', 'DataFK_Fact_KPI BettorsCashedOut', 'Fail', 'Missing BettorsCashedOut Key')
End

--BetsCashedOut
If not exists(
				Select		*
				From		#KPI as KPI
							left hash join #Transaction as [Transaction] on KPI.BetsCashedOut = [Transaction].BetId
				Where		[Transaction].BetId is null
							and KPI.BetsCashedOut is not null				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.73', 'DataFK_Fact_KPI BetsCashedOut', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.73', 'DataFK_Fact_KPI BetsCashedOut', 'Fail', 'Missing BetsCashedOut to BetID link')
End

If not exists(
				Select		KPI.BetsCashedOut
				From		#KPI as KPI
				Where		KPI.BetsCashedOut = -1				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.74', 'DataFK_Fact_KPI BetsCashedOut', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.74', 'DataFK_Fact_KPI BetsCashedOut', 'Fail', 'BetsCashedOut Keys')
End

--Cashout n/a
--CashoutDifferential n/a
--WithdrawalsRequested  n/a
--WithdrawalsCompleted  n/a 
--Adjustments  n/a

--BetsResulted
If not exists(
				Select		*
				From		#KPI as KPI
							left hash join #Transaction as [Transaction] on KPI.BetsResulted = [Transaction].BetId
				Where		[Transaction].BetId is null
							and KPI.BetsResulted is not null				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.51', 'DataFK_Fact_KPI BetsResulted', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-7515'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '121.51', 'DataFK_Fact_KPI BetsResulted', 'Pass', 'CCIA-5775')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '121.51', 'DataFK_Fact_KPI BetsResulted', 'Fail', 'Missing ContractsPlaced to BetID link')
	End
End

If not exists(
				Select		KPI.BetsResulted
				From		#KPI as KPI
				Where		KPI.BetsResulted = -1				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.52', 'DataFK_Fact_KPI BetsResulted', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.52', 'DataFK_Fact_KPI BetsResulted', 'Fail', 'BetsResulted Keys')
End

--ContractsResulted
If not exists(
				Select		*
				From		#KPI as KPI
							left hash join #Transaction as [Transaction] on KPI.ContractsResulted = [Transaction].BetGroupID
				Where		[Transaction].BetGroupID is null
							and KPI.ContractsResulted is not null				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.53', 'DataFK_Fact_KPI ContractsResulted', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-5775'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '121.53', 'DataFK_Fact_KPI ContractsResulted', 'Pass', 'CCIA-5775')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '121.53', 'DataFK_Fact_KPI ContractsResulted', 'Fail', 'Missing ContractsResulted to BetID link')
	End
End

If not exists(
				Select		KPI.ContractsResulted
				From		#KPI as KPI
				Where		KPI.ContractsResulted = -1				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.54', 'DataFK_Fact_KPI ContractsResulted', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.54', 'DataFK_Fact_KPI BetsResulted', 'Fail', 'BetsResulted Keys')
End

--BettorsResulted
If not exists(
				Select		*
				From		#KPI as KPI
							left join [$(Dimensional)].Dimension.Account as Account on KPI.BettorsResulted = Account.LedgerID
				Where		KPI.BettorsResulted is not null
							and Account.LedgerID is null				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.55', 'DataFK_Fact_KPI BettorsResulted', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.55', 'DataFK_Fact_KPI BettorsResulted', 'Fail', 'Missing BettorsResulted to LedgerID link')
End

If not exists(
				Select		KPI.BettorsResulted
				From		#KPI as KPI
				Where		KPI.BettorsResulted = -1				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.56', 'DataFK_Fact_KPI BettorsResulted', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.56', 'DataFK_Fact_KPI BettorsResulted', 'Fail', 'Missing BettorsResulted Key')
End

--PlayDaysResulted
If not exists(
				Select		*
				From		#KPI as KPI
							left join [$(Dimensional)].Dimension.DayZone as DayZone on KPI.DayKey = DayZone.DayKey
							left join [$(Dimensional)].Dimension.[Day] as [Day] on DayZone.MasterDayKey = [Day].DayKey
				Where		KPI.PlayDaysResulted is not null
							and Cast ([Day].DayKey as char(6)) + Cast (KPI.BettorsResulted as varchar(10)) = Cast (KPI.PlayDaysResulted as varchar(16))			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.57', 'DataFK_Fact_KPI PlayDaysResulted', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.57', 'DataFK_Fact_KPI PlayDaysResulted', 'Fail', 'Missing PlayDaysResulted link')
End

If not exists(
				Select		KPI.PlayDaysResulted
				From		#KPI as KPI
				Where		KPI.PlayDaysResulted = -1			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.58', 'DataFK_Fact_KPI PlayDaysResulted', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.58', 'DataFK_Fact_KPI PlayDaysResulted', 'Fail', 'Missing PlayDaysResulted Keys')
End

--PlayFiscalWeeksResulted
If not exists(
				Select		*
				From		#KPI as KPI
							left join [$(Dimensional)].Dimension.DayZone as DayZone on KPI.DayKey = DayZone.DayKey
							left join [$(Dimensional)].Dimension.[Day] as [Day] on DayZone.MasterDayKey = [Day].DayKey
				Where		KPI.PlayFiscalWeeksResulted is not null
							and Cast ([Day].FiscalWeekKey as char(6)) + Cast (KPI.BettorsResulted as varchar(10)) = Cast (KPI.PlayFiscalWeeksResulted as varchar(16))			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.59', 'DataFK_Fact_KPI PlayFiscalWeeksResulted', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.59', 'DataFK_Fact_KPI PlayFiscalWeeksResulted', 'Fail', 'Missing PlayFiscalWeeksResulted link')
End

If not exists(
				Select		KPI.PlayFiscalWeeksResulted
				From		#KPI as KPI
				Where		KPI.PlayFiscalWeeksResulted = -1				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.60', 'DataFK_Fact_KPI PlayFiscalWeeksResulted', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.60', 'DataFK_Fact_KPI PlayFiscalWeeksResulted', 'Fail', 'Missing PlayFiscalWeeksResulted Keys')
End

--PlayCalenderWeeksResulted
If not exists(
				Select		*
				From		#KPI as KPI
							left join [$(Dimensional)].Dimension.DayZone as DayZone on KPI.DayKey = DayZone.DayKey
							left join [$(Dimensional)].Dimension.[Day] as [Day] on DayZone.MasterDayKey = [Day].DayKey
				Where		KPI.PlayCalenderWeeksResulted is not null
							and Cast (KPI.BettorsResulted as varchar(10)) + Cast ([Day].WeekKey as char(6)) != Cast (KPI.PlayCalenderWeeksResulted as varchar(16))				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.61', 'DataFK_Fact_KPI PlayCalenderWeeksResulted', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-5939'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '121.61', 'DataFK_Fact_KPI PlayCalenderWeeksResulted', 'Pass', 'CCIA-5939')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '121.61', 'DataFK_Fact_KPI PlayCalenderWeeksResulted', 'Fail', 'Missing PlayCalenderWeeksResulted link')
	End
End

If not exists(
				Select		KPI.PlayCalenderWeeksResulted
				From		#KPI as KPI
				Where		KPI.PlayCalenderWeeksResulted = -1				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.62', 'DataFK_Fact_KPI PlayCalenderWeeksResulted', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.62', 'DataFK_Fact_KPI PlayCalenderWeeksResulted', 'Fail', 'Missing PlayCalenderWeeksResulted Keys')
End

--PlayFiscalMonthsResulted
If not exists(
				Select		*
				From		#KPI as KPI
							left join [$(Dimensional)].Dimension.DayZone as DayZone on KPI.DayKey = DayZone.DayKey
							left join [$(Dimensional)].Dimension.[Day] as [Day] on DayZone.MasterDayKey = [Day].DayKey
				Where		KPI.PlayFiscalMonthsResulted is not null
							and Cast ([Day].FiscalMonthKey as char(6)) + Cast (KPI.BettorsResulted as varchar(10)) = Cast (KPI.PlayFiscalMonthsResulted as varchar(16))			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.63', 'DataFK_Fact_KPI PlayFiscalMonthsResulted', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.63', 'DataFK_Fact_KPI PlayFiscalMonthsResulted', 'Fail', 'Missing PlayFiscalMonthsResulted link')
End

If not exists(
				Select		KPI.PlayFiscalMonthsResulted
				From		#KPI as KPI
				Where		KPI.PlayFiscalMonthsResulted = -1
							and (
								(KPI.CreatedDate >= @FromDate)
								or (@FromDate is null)
								)				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.64', 'DataFK_Fact_KPI PlayFiscalMonthsResulted', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.64', 'DataFK_Fact_KPI PlayFiscalMonthsResulted', 'Fail', 'Missing PlayFiscalMonthsResulted Keys')
End

--PlayCalenderMonthsResulted
If not exists(
				Select		*
				From		#KPI as KPI
							left join [$(Dimensional)].Dimension.DayZone as DayZone on KPI.DayKey = DayZone.DayKey
							left join [$(Dimensional)].Dimension.[Day] as [Day] on DayZone.MasterDayKey = [Day].DayKey
				Where		KPI.PlayCalenderMonthsResulted is not null
							and Cast ([Day].MonthKey as char(6)) + Cast (KPI.BettorsResulted as varchar(10)) = Cast (KPI.PlayCalenderMonthsResulted as varchar(16))			
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.65', 'DataFK_Fact_KPI PlayCalenderMonthsResulted', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.65', 'DataFK_Fact_KPI PlayCalenderMonthsResulted', 'Fail', 'Missing PlayCalenderMonthsResulted link')
End

If not exists(
				Select		KPI.PlayCalenderMonthsResulted
				From		#KPI as KPI
				Where		KPI.PlayCalenderMonthsResulted = -1				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.66', 'DataFK_Fact_KPI PlayCalenderMonthsResulted', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.66', 'DataFK_Fact_KPI PlayCalenderMonthsResulted', 'Fail', 'Missing PlayCalenderMonthsResulted Keys')
End

--Turnover n/a
--GrossWin n/a
--NetRevenue n/a
--NetRevenueAdjustment n/a
--BonusWinnings n/a
--Betback n/a
--GrossWinAdjustment n/a

--Metadata n/a
--CreatedDate
--CreatedBatchKey
--CreatedBy
--ModifiedDate
--ModifiedBatchKey
--ModifiedBy

--StructureKey
If not exists(
				Select		KPI.StructureKey
				From		#KPI as KPI
							left join [$(Dimensional)].Dimension.Structure as Structure on KPI.StructureKey = Structure.StructureKey
				Where		Structure.StructureKey is null				
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.67', 'DataFK_Fact_KPI Structure', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '121.67', 'DataFK_Fact_KPI Structure', 'Fail', 'Missing StructureKeys')
End

--If not exists(
--				Select		KPI.StructureKey
--				From		#KPI as KPI
--				Where		KPI.StructureKey = -1				
--			 ) 
--Begin
--	Insert into test.tblResults Values (@TestRunNo, '121.68', 'DataFK_Fact_KPI Structure', 'Pass', Null)
--End
--Else
--Begin
--	Insert into test.tblResults Values (@TestRunNo, '121.68', 'DataFK_Fact_KPI Structure', 'Fail', 'Missing StructureKeys')
--End

﻿--Test Code
--exec Test.[166WarehouseTaxonomy] 'ADHOC'
CREATE PROCEDURE [Test].[166WarehouseTaxonomy] @TestRunNo varchar(23)

as

If Not Exists	(
				select		*
				From		[$(Warehouse)].[Control].MissingTaxonomy
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '166', 'WarehouseTaxonomy', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '166', 'WarehouseTaxonomy', 'Fail', 'Fields are missing')
End

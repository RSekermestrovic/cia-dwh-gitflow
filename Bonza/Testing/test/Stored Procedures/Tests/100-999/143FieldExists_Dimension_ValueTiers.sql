--Test Code
--exec Test.[143FieldExists_Dimension_ValueTiers] 'ADHOC5'
Create Procedure Test.[143FieldExists_Dimension_ValueTiers] @TestRunNo varchar(23)

as

Declare @Database varchar(100)
Set @Database = '[$(Dimensional)]'

Declare @SQL varchar(max)

--CCIA-5564 metadata fields are missing
If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-5564'
							and [Status] != 'Done'
				) 
Begin
	Set @SQL = 'Select top 1 
			[TierKey],
			[TierNumber],
			[TierName],
			[FromDate],
			[ToDate]
				from ' + @Database + '.[Dimension].[ValueTiers]'

	Exec Test.spSelectValid @TestRunNo, '143','Structure - Dimension ValueTiers Table', @SQL

	Insert into test.tblResults Values (@TestRunNo, '143', 'Structure - Dimension ValueTiers Table', 'Pass', 'CCIA-5564')
End
Else
Begin
	Set @SQL = 'Select top 1 
			[TierKey],
			[TierNumber],
			[TierName],
			[FromDate],
			[ToDate],
			[FirstDate],
			[CreatedDate],
			[CreatedBatchKey],
			[CreatedBy],
			[ModifiedDate] ,
			[ModifiedBatchKey],
			[ModifiedBy] 
				from ' + @Database + '.[Dimension].[ValueTiers]'

	Exec Test.spSelectValid @TestRunNo, '143','Structure - Dimension ValueTiers Table', @SQL
End
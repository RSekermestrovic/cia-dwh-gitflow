﻿--Test Code
--exec [Test].[305DataDefault_Dimension_Communication] 'ADHOC'
Create Procedure [Test].[305DataDefault_Dimension_Communication] @TestRunNo varchar(23)

as
--CCIA-6650 this table does not have a zero value

If exists(
		Select		CommunicationKey
		From		[$(Dimensional)].[Dimension].Communication
		where		CommunicationKey =  -1
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '305.1', 'DataDefault_Dimension_Communication Table ID-1', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '305.1', 'DataDefault_Dimension_Communication Table ID-1', 'Fail', 'No ID -1 record found')
End

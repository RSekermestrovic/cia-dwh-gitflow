﻿--test
--exec [Test].[148DataDuplicates_Dimension_ValueTiers] 'ADHOC'

CREATE PROCEDURE [Test].[148DataDuplicates_Dimension_ValueTiers] @TestRunNo varchar(23)

as

If not exists(
		Select		Count(*)
		From		[$(Dimensional)].Dimension.ValueTiers as ValueTiers
		Group by	ValueTiers.TierNumber
		Having		Count(*) > 1
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '148', 'DataDuplicates_Dimension_ValueTiers', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '148', 'DataDuplicates_Dimension_ValueTiers', 'Fail', 'Duplicates of Tiers found')
End
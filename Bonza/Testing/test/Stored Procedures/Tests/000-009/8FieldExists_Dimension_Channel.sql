--Test Code
--exec Test.[8FieldExists_Dimension_Channel] 'ADHOC'
Create Procedure Test.[8FieldExists_Dimension_Channel] @TestRunNo varchar(23)

as

Declare @Database varchar(100)
Set @Database = '[$(Dimensional)]'

Declare @SQL varchar(max)
Set @SQL = 'Select top 1 
		[ChannelKey]
      ,[ChannelId]
      ,[ChannelDescription]
      ,[ChannelName]
      ,[ChannelTypeName]
      ,[CreatedDate]
      ,[CreatedBatchKey]
      ,[CreatedBy]
      ,[ModifiedDate]
      ,[ModifiedBatchKey]
      ,[ModifiedBy]
			from ' + @Database + '.[Dimension].[Channel]'

Exec Test.spSelectValid @TestRunNo, '8','Structure - Dimension Channel Table', @SQL

If		(
		Select		Count(*)
		From		[$(Dimensional)].sys.all_objects as [Objects]
					inner join [$(Dimensional)].sys.all_columns as [Columns] on [Objects].[object_id] = [Columns].[object_id]
					inner join [$(Dimensional)].sys.schemas as [schemas] on [Objects].[schema_id] = [schemas].[schema_id]
		where		[Objects].name = 'Channel'
					and [Objects].[type] = 'U'
					and [schemas].Name = 'Dimension'
		) = 11
Begin
	Insert into test.tblResults Values (@TestRunNo, '8.1', 'Structure - Dimension Channel Table', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '8.1', 'Structure - Dimension Channel Table', 'Fail', 'Wrong number of columns')
End

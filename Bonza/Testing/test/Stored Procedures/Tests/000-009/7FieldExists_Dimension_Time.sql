--Test Code
--exec Test.[7FieldExists_Dimension_Time] 'ADHOC'
Create Procedure Test.[7FieldExists_Dimension_Time] @TestRunNo varchar(23)

as

Declare @Database varchar(100)
Set @Database = '[$(Dimensional)]'
Declare @SQL varchar(max)
Set @SQL = 'Select top 1 
			[TimeKey]
		  ,[TimeID]
		  ,[TimeText]
		  ,[Time]
		  ,[TimeStartTime]
		  ,[TimeEndTime]
		  ,[TimeStartTimestamp]
		  ,[TimeEndTimestamp]
		  ,[TimeOfHourNumber]
		  ,[TimeOfDayNumber]
		  ,[HourKey]
		  ,[HourID]
		  ,[HourText]
		  ,[HourStartTime]
		  ,[HourEndTime]
		  ,[HourOfDayNumber]
		  ,[PeriodKey]
		  ,[PeriodText]
		  ,[FromDate]
		  ,[ToDate]
		  ,[FirstDate]
		  ,[CreatedDate]
		  ,[CreatedBatchKey]
		  ,[CreatedBy]
		  ,[ModifiedDate]
		  ,[ModifiedBatchKey]
		  ,[ModifiedBy]
			from ' + @Database + '.[Dimension].[Time]'

Exec Test.spSelectValid @TestRunNo, '7','Structure - Dimension Time Table', @SQL

If		(
		Select		Count(*)
		From		[$(Dimensional)].sys.all_objects as [Objects]
					inner join [$(Dimensional)].sys.all_columns as [Columns] on [Objects].[object_id] = [Columns].[object_id]
					inner join [$(Dimensional)].sys.schemas as [schemas] on [Objects].[schema_id] = [schemas].[schema_id]
		where		[Objects].name = 'Time'
					and [Objects].[type] = 'U'
					and [schemas].Name = 'Dimension'
		) = 27
Begin
	Insert into test.tblResults Values (@TestRunNo, '7.1', 'Structure - Dimension Time Table', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '7.1', 'Structure - Dimension Time Table', 'Fail', 'Wrong number of columns')
End


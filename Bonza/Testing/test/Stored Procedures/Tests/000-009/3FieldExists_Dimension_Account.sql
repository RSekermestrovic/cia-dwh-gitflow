--Test Code
--exec Test.[3FieldExists_Dimension_Account] 'ADHOC'
Create Procedure Test.[3FieldExists_Dimension_Account] @TestRunNo varchar(23)

as

Declare @Database varchar(100)
Set @Database = '[$(Dimensional)]'

Declare @SQL varchar(max)
Set @SQL = 'Select top 1 
		   [AccountKey]
		  ,[AccountID]
		  ,[AccountSource]
		  ,[PIN]
		  ,[CustomerID]
		  ,[CustomerSource]
		  ,[LedgerID]
		  ,[LedgerSource]
		  ,[LegacyLedgerID]
		  ,[LedgerTypeKey]
		  ,[LedgerTypeID]
		  ,[LedgerTypeSource]
		  ,[LedgerTypeName]
		  ,[LedgerGroupingName]
		  ,[AccountSequenceNumber]
		  ,[SourceTitle]
		  ,[SourceSurname]
		  ,[SourceMiddleName]
		  ,[SourceFirstName]
		  ,[SourceGender]
		  ,[Title]
		  ,[Surname]
		  ,[MiddleName]
		  ,[FirstName]
		  ,[DOB]
		  ,[Gender]
		  ,[CompanyKey]
		  ,[AccountTypeCode]
          ,[AccountType]
		  ,[AccountOpenedDayKey]
		  ,[AccountOpenedDate]
		  ,[AccountOpenedChannel]
		  ,[AddressType]
		  ,[SourceAddress1]
		  ,[SourceAddress2]
		  ,[SourceSuburb]
		  ,[SourceStateCode]
		  ,[SourceStateName]
		  ,[SourcePostCode]
		  ,[SourceCountryCode]
		  ,[SourceCountryName]
		  ,[Address1]
		  ,[Address2]
		  ,[Suburb]
		  ,[StateCode]
		  ,[StateName]
		  ,[PostCode]
		  ,[CountryCode]
		  ,[CountryName]
		  ,[GeographicalLocation]
		  ,[AustralianClient]
		  ,[SourcePhone]
		  ,[SourceMobile]
		  ,[SourceFax]
		  ,[SourceEmail]
		  ,[SourceAlternateEmail]
		  ,[Phone]
		  ,[Mobile]
		  ,[Fax]
		  ,[Email]
		  ,[AlternateEmail]
		  ,[StatementMethod]
		  ,[StatementFrequency]
		  ,[BTag2]
		  ,[AffiliateID]
		  ,[AffiliateName]
		  ,[SiteID]
		  ,[TrafficSource]
		  ,[RefURL]
		  ,[CampaignID]
		  ,[Keywords]
		  ,[UserName]
		  ,[BDM]
		  ,[VIPCode]
		  ,[VIP]
		  ,[FromDate]
		  ,[ToDate]
		  ,[FirstDate]
		  ,[CreatedDate]
		  ,[CreatedBatchKey]
		  ,[CreatedBy]
		  ,[ModifiedDate]
		  ,[ModifiedBatchKey]
		  ,[ModifiedBy]
			from ' + @Database + '.[Dimension].[Account]'

Exec Test.spSelectValid @TestRunNo, '3','Structure - Dimension Account Table', @SQL

If		(
		Select		Count(*)
		From		[$(Dimensional)].sys.all_objects as [Objects]
					inner join [$(Dimensional)].sys.all_columns as [Columns] on [Objects].[object_id] = [Columns].[object_id]
					inner join [$(Dimensional)].sys.schemas as [schemas] on [Objects].[schema_id] = [schemas].[schema_id]
		where		[Objects].name = 'Account'
					and [Objects].[type] = 'U'
					and [schemas].Name = 'Dimension'
		) = 85
Begin
	Insert into test.tblResults Values (@TestRunNo, '3.1', 'Structure - Dimension Account Table', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '3.1', 'Structure - Dimension Account Table', 'Fail', 'Wrong number of columns')
End

--CCIA-3308
If	(
	Select		DATETIME_PRECISION
	FROM		[$(Dimensional)].INFORMATION_SCHEMA.COLUMNS
	WHERE		TABLE_NAME = 'Account' and COLUMN_NAME = 'FirstDate'
	) = 3
Begin
	Insert into test.tblResults Values (@TestRunNo, '3.2', 'DataDefault_Dimension_Account CCIA-3308', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '3.2', 'DataDefault_Dimension_Account CCIA-3308', 'Fail', 'This should be a Datetime2(3) datatype')
End

--Test Code
--exec Test.[1TableExists_Star_Transaction] 'ADHOC2'
Create Procedure Test.[1TableExists_Star_Transaction] @TestRunNo varchar(23)

as

Declare @Database varchar(100)
Set @Database = '[$(Dimensional)]'

Insert into Test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('1.0','Structure - Transaction Table', 'Fact', 'Transaction')

Insert into Test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('1.1','Structure - Transaction Detail Table', 'Dimension', 'TransactionDetail')

Insert into Test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('1.2','Structure - BalanceType Table', 'Dimension', 'BalanceType')

Insert into Test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('1.3','Structure - Contract Table', 'Dimension', 'Contract')

Insert into Test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('1.4','Structure - Account Table', 'Dimension', 'Account')

Insert into Test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('1.5','Structure - Account Status Table', 'Dimension', 'AccountStatus')

Insert into Test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('1.6','Structure - Wallet Table', 'Dimension', 'Wallet')

Insert into Test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('1.7','Structure - Promotion Table', 'Dimension', 'Promotion')

Insert into Test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('1.8','Structure - Market Table', 'Dimension', 'Market')

Insert into Test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('1.9','Structure - Event Table', 'Dimension', 'Event')

Insert into Test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('1.10','Structure - Day Table', 'Dimension', 'Day')

Insert into Test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('1.11','Structure - Time Table', 'Dimension', 'Time')

Insert into Test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('1.12','Structure - Campaign Table', 'Dimension', 'Campaign')

Insert into Test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('1.13','Structure - Channel Table', 'Dimension', 'Channel')

Insert into Test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('1.14','Structure - PriceType Table', 'Dimension', 'PriceType')

Insert into Test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('1.15','Structure - BetType Table', 'Dimension', 'BetType')

Insert into Test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('1.16','Structure - LegType Table', 'Dimension', 'LegType')

Insert into Test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('1.17','Structure - Class Table', 'Dimension', 'Class')

Insert into Test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('1.18','Structure - User Table', 'Dimension', 'User')

Insert into Test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('1.19','Structure - Instrument Table', 'Dimension', 'Instrument')

Insert into Test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('1.20','Structure - Note Table', 'Dimension', 'Note')

Insert into Test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('1.21','Structure - InPlay Table', 'Dimension', 'InPlay')

Insert into test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('1.22','Structure - Structure Table', 'Dimension', 'Structure')
--Test Code
--exec Test.[9FieldExists_Dimension_User] 'ADHOC'
Create Procedure Test.[9FieldExists_Dimension_User] @TestRunNo varchar(23)

as

Declare @Database varchar(100)
Set @Database = '[$(Dimensional)]'

Declare @SQL varchar(max)
Set @SQL = 'Select top 1 
[UserKey]
      ,[UserId]
      ,[Source]
      ,[SystemName]
      ,[EmployeeKey]
      ,[Forename]
      ,[Surname]
      ,[PayrollNumber]
      ,[FromDate]
      ,[ToDate]
      ,[FirstDate]
      ,[CreatedDate]
      ,[CreatedBatchKey]
      ,[CreatedBy]
      ,[ModifiedDate]
      ,[ModifiedBatchKey]
      ,[ModifiedBy]
			from ' + @Database + '.[Dimension].[User]'

Exec Test.spSelectValid @TestRunNo, '9','Structure - Dimension User Table', @SQL

If		(
		Select		Count(*)
		From		[$(Dimensional)].sys.all_objects as [Objects]
					inner join [$(Dimensional)].sys.all_columns as [Columns] on [Objects].[object_id] = [Columns].[object_id]
					inner join [$(Dimensional)].sys.schemas as [schemas] on [Objects].[schema_id] = [schemas].[schema_id]
		where		[Objects].name = 'User'
					and [Objects].[type] = 'U'
					and [schemas].Name = 'Dimension'
		) = 17
Begin
	Insert into test.tblResults Values (@TestRunNo, '9.1', 'Structure - Dimension User Table', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '9.1', 'Structure - Dimension User Table', 'Fail', 'Wrong number of columns')
End
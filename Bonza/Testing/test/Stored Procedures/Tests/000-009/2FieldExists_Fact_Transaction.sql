--Test Code
--exec Test.[2FieldExists_Fact_Transaction] 'ADHOC'
Create Procedure Test.[2FieldExists_Fact_Transaction] @TestRunNo varchar(23)

as

Declare @Database varchar(100)
Set @Database = '[$(Dimensional)]'

Declare @SQL varchar(max)
Set @SQL = 'Select top 1 
			   [TransactionId]
			  ,[TransactionIdSource]
			  ,[TransactionDetailKey]
			  ,[BalanceTypeKey]
			  ,[ContractKey]
			  ,[AccountKey]
			  ,[AccountStatusKey]
			  ,[WalletKey]
			  ,[PromotionKey]
			  ,[MarketKey]
			  ,[EventKey]
			  ,[DayKey]
			  ,[MasterTransactionTimestamp]
			  ,[MasterTimeKey]
			  ,[AnalysisTransactionTimestamp]
			  ,[AnalysisTimeKey]
			  ,[CampaignKey]
			  ,[ChannelKey]
			  ,[LedgerID]
			  ,[TransactedAmount]
			  ,[PriceTypeKey]
			  ,[BetTypeKey]
			  ,[LegTypeKey]
			  ,[ClassKey]
			  ,[UserKey]
			  ,[InstrumentKey]
			  ,[NoteKey]
			  ,[InPlayKey]
			  ,[FromDate]
			  ,[CreatedDate]
			  ,[CreatedBatchKey]
			  ,[CreatedBy]
			  ,[StructureKey]
			from ' + @Database + '.[Fact].[Transaction]'

Exec Test.spSelectValid @TestRunNo, '2','Structure - Fact Transaction Table', @SQL

If		(
		Select		Count(*)
		From		[$(Dimensional)].sys.all_objects as [Objects]
					inner join [$(Dimensional)].sys.all_columns as [Columns] on [Objects].[object_id] = [Columns].[object_id]
					inner join [$(Dimensional)].sys.schemas as [schemas] on [Objects].[schema_id] = [schemas].[schema_id]
		where		[Objects].name = 'Transaction'
					and [Objects].[type] = 'U'
					and [schemas].Name = 'Fact'
		) = 33
Begin
	Insert into test.tblResults Values (@TestRunNo, '2.1', 'Structure - Fact Transaction Table', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '2.1', 'Structure - Fact Transaction Table', 'Fail', 'Wrong number of columns')
End



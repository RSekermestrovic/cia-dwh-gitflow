--Test Code
--exec Test.[5FieldExists_Dimension_Contract] 'ADHOC'
Create Procedure Test.[5FieldExists_Dimension_Contract] @TestRunNo varchar(23)

as

Declare @Database varchar(100)
Set @Database = '[$(Dimensional)]'

Declare @SQL varchar(max)
Set @SQL = 'Select top 1 
		   [ContractKey]
		  ,[BetGroupID]
		  ,[BetGroupIDSource]
		  ,[BetId]
		  ,[BetIdSource]
		  ,[LegId]
		  ,[LegIdSource]
		  ,[TransactionID]
		  ,[TransactionIDSource]
		  ,[FreeBetID]
		  ,[ExternalID]
		  ,[External1]
		  ,[External2]
		  ,[RequestID]
		  ,[WithdrawID]
		  ,[FromDate]
		  ,[ToDate]
		  ,[FirstDate]
		  ,[CreatedDate]
		  ,[CreatedBatchKey]
		  ,[CreatedBy]
		  ,[ModifiedDate]
		  ,[ModifiedBatchKey]
		  ,[ModifiedBy]
			from ' + @Database + '.[Dimension].[Contract]'

Exec Test.spSelectValid @TestRunNo, '5','Structure - Dimension Contract Table', @SQL

If		(
		Select		Count(*)
		From		[$(Dimensional)].sys.all_objects as [Objects]
					inner join [$(Dimensional)].sys.all_columns as [Columns] on [Objects].[object_id] = [Columns].[object_id]
					inner join [$(Dimensional)].sys.schemas as [schemas] on [Objects].[schema_id] = [schemas].[schema_id]
		where		[Objects].name = 'Contract'
					and [Objects].[type] = 'U'
					and [schemas].Name = 'Dimension'
		) = 24
Begin
	Insert into test.tblResults Values (@TestRunNo, '5.1', 'Structure - Dimension Contract Table', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '5.1', 'Structure - Dimension Contract Table', 'Fail', 'Wrong number of columns')
End

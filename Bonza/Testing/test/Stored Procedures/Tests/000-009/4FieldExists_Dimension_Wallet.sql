--Test Code
--exec Test.[4FieldExists_Dimension_Wallet] 'ADHOC'
Create Procedure Test.[4FieldExists_Dimension_Wallet] @TestRunNo varchar(23)

as

Declare @Database varchar(100)
Set @Database = '[$(Dimensional)]'

Declare @SQL varchar(max)
Set @SQL = 'Select top 1 
			   [WalletKey]
			  ,[WalletId]
			  ,[WalletName]
			  ,[FromDate]
			  ,[ToDate]
			  ,[FirstDate]
			  ,[CreatedDate]
			  ,[CreatedBatchKey]
			  ,[CreatedBy]
			  ,[ModifiedDate]
			  ,[ModifiedBatchKey]
			  ,[ModifiedBy]
			from ' + @Database + '.[Dimension].[Wallet]'

Exec Test.spSelectValid @TestRunNo, '4','Structure - Dimension Wallet Table', @SQL

If		(
		Select		Count(*)
		From		[$(Dimensional)].sys.all_objects as [Objects]
					inner join [$(Dimensional)].sys.all_columns as [Columns] on [Objects].[object_id] = [Columns].[object_id]
					inner join [$(Dimensional)].sys.schemas as [schemas] on [Objects].[schema_id] = [schemas].[schema_id]
		where		[Objects].name = 'Wallet'
					and [Objects].[type] = 'U'
					and [schemas].Name = 'Dimension'
		) = 12
Begin
	Insert into test.tblResults Values (@TestRunNo, '4.1', 'Structure - Dimension Wallet Table', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '4.1', 'Structure - Dimension Wallet Table', 'Fail', 'Wrong number of columns')
End
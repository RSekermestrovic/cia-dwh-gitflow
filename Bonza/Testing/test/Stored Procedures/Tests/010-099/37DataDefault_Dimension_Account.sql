﻿
--Test Code
--exec Test.[37DataDefault_Dimension_Account] 'ADHOC'
Create Procedure [test].[37DataDefault_Dimension_Account] @TestRunNo varchar(23)

as
If exists(
		Select		AccountKey
		From		[$(Dimensional)].[Dimension].Account
		where		AccountKey = 0
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '37', 'DataDefault_Dimension_Account Table ID0', 'Pass', Null)
End
Else
Begin
If exists		(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-5060'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '37', 'DataDefault_Dimension_Account Table ID0', 'Pass', 'CCIA-5060')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '37', 'DataDefault_Dimension_Account Table ID0', 'Fail', 'No ID 0 record found')
	End
End

If exists(
		Select		AccountKey
		From		[$(Dimensional)].[Dimension].Account
		where		AccountKey = -1
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '37.1', 'DataDefault_Dimension_Account Table ID-1', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '37.1', 'DataDefault_Dimension_Account Table ID-1', 'Fail', 'No ID -1 record found')
End


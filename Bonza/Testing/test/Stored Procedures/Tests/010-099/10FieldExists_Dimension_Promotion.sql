--Test Code
--exec Test.[10FieldExists_Dimension_Promotion] 'ADHOC5'
Create Procedure Test.[10FieldExists_Dimension_Promotion] @TestRunNo varchar(23)

as

Declare @Database varchar(100)
Set @Database = '[$(Dimensional)]'

--need to add fields when the table is created for the first time
Declare @SQL varchar(max)
Set @SQL = 'Select top 1 
[PromotionKey]
      ,[PromotionId]
      ,[Source]
      ,[PromotionType]
      ,[PromotionName]
      ,[OfferDate]
      ,[ExpiryDate]
      ,[FromDate]
      ,[ToDate]
      ,[FirstDate]
      ,[CreatedDate]
      ,[CreatedBatchKey]
      ,[CreatedBy]
      ,[ModifiedDate]
      ,[ModifiedBatchKey]
      ,[ModifiedBy]
			from ' + @Database + '.[Dimension].[Promotion]'

Exec Test.spSelectValid @TestRunNo, '10','Structure - Dimension Promotion Table', @SQL

If		(
		Select		Count(*)
		From		[$(Dimensional)].sys.all_objects as [Objects]
					inner join [$(Dimensional)].sys.all_columns as [Columns] on [Objects].[object_id] = [Columns].[object_id]
					inner join [$(Dimensional)].sys.schemas as [schemas] on [Objects].[schema_id] = [schemas].[schema_id]
		where		[Objects].name = 'Promotion'
					and [Objects].[type] = 'U'
					and [schemas].Name = 'Dimension'
		) = 16
Begin
	Insert into test.tblResults Values (@TestRunNo, '10.1', 'Structure - Dimension Promotion Table', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '10.1', 'Structure - Dimension Promotion Table', 'Fail', 'Wrong number of columns')
End

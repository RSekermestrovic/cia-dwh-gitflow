﻿--Test Code
--exec Test.[34DataDefault_Dimension_BetType] 'ADHOC'
Create Procedure Test.[34DataDefault_Dimension_BetType] @TestRunNo varchar(23)

as

If exists(
		Select		BetTypeKey
		From		[$(Dimensional)].[Dimension].[BetType]
		where		BetTypeKey = 0
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '34', 'DataDefault_Dimension_BetType Table ID0', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-5059'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '34', 'DataDefault_Dimension_BetType Table ID0', 'Pass', 'CCIA-5059')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '34', 'DataDefault_Dimension_BetType Table ID0', 'Fail', 'No ID 0 record found')
	End
End

If exists(
		Select		BetTypeKey
		From		[$(Dimensional)].[Dimension].[BetType]
		where		BetTypeKey = -1
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '34.1', 'DataDefault_Dimension_BetType Table ID-1', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '34.1', 'DataDefault_Dimension_BetType Table ID-1', 'Fail', 'No ID -1 record found')
End



﻿--Test Code
--exec Test.[50DataDuplicates_Dimension_Instrument] 'ADHOC', null

Create Procedure Test.[50DataDuplicates_Dimension_Instrument] @TestRunNo varchar(23), @FromDate datetime

as

Declare @ErrorText varchar(100)
If @FromDate is not null
Begin
Set @ErrorText = ' since ' + Convert (varchar(20), @FromDate, 103)
End
else
Begin
Set @ErrorText = '' 
End

If not exists(
				select		InstrumentId, Source
				From		[$(Dimensional)].Dimension.Instrument
				Group by	InstrumentId
							, Source
				Having count(*) > 1
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '50', 'DataDuplicates_Dimension_Instrument', 'Pass', Null)
End
Else
Begin
If exists		(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-5121'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '50', 'DataDuplicates_Dimension_Instrument', 'Pass', 'CCIA-5121')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '50', 'DataDuplicates_Dimension_Instrument', 'Fail', 'Duplicates of InstrumentID found' + @ErrorText)
	End
End


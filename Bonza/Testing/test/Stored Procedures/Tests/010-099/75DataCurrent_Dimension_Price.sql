﻿--Test Code
--exec Test.[75DataCurrent_Dimension_Price] 'ADHOC'
Create Procedure Test.[75DataCurrent_Dimension_Price] @TestRunNo varchar(23)

as
/*
Select	*
into #DimensionPrice
From [$(Dimensional)].[Dimension].[Price]

Declare @NumberOfRecords int
Set @NumberOfRecords = 
					(
					Select		count(*) as NumberOfRecords
					From		#DimensionPrice
					)


If @NumberOfRecords = 999
Begin
	Insert into test.tblResults Values (@TestRunNo, '75', 'DataCurrent_Dimension_Price Record Count', 'Pass', Null)
End
Else
Begin
	--CCIA-3317 the table is empty
	IF @NumberOfRecords = 0
		and	exists	(	Select		*
					From		test.tblDefects
					Where		DefectID = 'CCIA-3317'
								and Status != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '75', 'DataCurrent_Dimension_Price Record Count', 'Pass', 'CCIA-3317')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '75', 'DataCurrent_Dimension_Price Record Count', 'Fail', 'Expected 999 records, ' + Cast (@NumberOfRecords as varchar(5)) + ' found.')
	End
End

IF 					(
					Select		Min (PriceKey) as Record
					From		#DimensionPrice
					where		PriceKey not in (0 ,-1)
					)
					= 999
Begin
	Insert into test.tblResults Values (@TestRunNo, '75.1', 'DataCurrent_Dimension_Price Record Count', 'Pass', Null)
End
Else
Begin
	--CCIA-3317 the table is empty
	IF @NumberOfRecords = 0
		and	exists	(	Select		*
					From		test.tblDefects
					Where		DefectID = 'CCIA-3317'
								and Status != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '75.1', 'DataCurrent_Dimension_Price Record Count', 'Pass', 'CCIA-3317')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '75.1', 'DataCurrent_Dimension_Price Record Count', 'Fail', 'The lowest record key is not 999')
	End
End

IF 					(
					Select		Max (PriceKey) as Record
					From		#DimensionPrice
					where		PriceKey not in (0 ,-1)
					)
					= 999
Begin
	Insert into test.tblResults Values (@TestRunNo, '75.2', 'DataCurrent_Dimension_Price Record Count', 'Pass', Null)
End
Else
Begin
	--CCIA-3317 the table is empty
	IF @NumberOfRecords = 0
		and	exists	(	Select		*
					From		test.tblDefects
					Where		DefectID = 'CCIA-3317'
								and Status != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '75.2', 'DataCurrent_Dimension_Price Record Count', 'Pass', 'CCIA-3317')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '75.2', 'DataCurrent_Dimension_Price Record Count', 'Fail', 'The highest record key is not 999')
	End
End
*/

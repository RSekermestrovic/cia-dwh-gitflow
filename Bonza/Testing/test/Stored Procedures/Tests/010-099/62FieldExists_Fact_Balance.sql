--Test Code
--exec Test.[62FieldExists_Fact_Balance] 'ADHOC'
Create Procedure Test.[62FieldExists_Fact_Balance] @TestRunNo varchar(23)

as

Declare @Database varchar(100)
Set @Database = '[$(Dimensional)]'

Declare @SQL varchar(max)
Set @SQL = 'Select top 1 [AccountKey]
      ,[AccountStatusKey]
      ,[LedgerID]
      ,[LedgerSource]
      ,[DayKey]
      ,[BalanceTypeKey]
      ,[WalletKey]
      ,[OpeningBalance]
      ,[TotalTransactedAmount]
      ,[ClosingBalance]
      ,[CreatedDate]
      ,[CreatedBatchKey]
      ,[ModifiedDate]
      ,[ModifiedBatchKey]
	  ,StructureKey
			FROM		' + @Database + '.Fact.[Balance]'

Exec Test.spSelectValid @TestRunNo, '62','Structure - Fact Balance Table', @SQL

If		(
		Select		Count(*)
		From		[$(Dimensional)].sys.all_objects as [Objects]
					inner join [$(Dimensional)].sys.all_columns as [Columns] on [Objects].[object_id] = [Columns].[object_id]
					inner join [$(Dimensional)].sys.schemas as [schemas] on [Objects].[schema_id] = [schemas].[schema_id]
		where		[Objects].name = 'Balance'
					and [Objects].[type] = 'U'
					and [schemas].Name = 'Fact'
		) = 15
Begin
	Insert into test.tblResults Values (@TestRunNo, '62.1', 'Structure - Fact Balance Table', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '62.1', 'Structure - Fact Balance Table', 'Fail', 'Wrong number of columns')
End
﻿--Test Code
--exec Test.[84DataCurrent_Dimension_Note] 'ADHOC'
Create Procedure Test.[84DataCurrent_Dimension_Note] @TestRunNo varchar(23)

as

Declare	@BatchDate datetime2
Set	@BatchDate = (Select Max(BatchFromDate) From [$(Staging)].[Control].ETLController Where DimensionStatus = 1)

Declare @LastDate varchar(10)
Set		@LastDate = 
					(Select		max (ModifiedDate)
					From		[$(Dimensional)].[Dimension].[Note]
					)

If @LastDate >= @BatchDate or @LastDate >= DateAdd (HOUR,-12,getdate())
Begin
	Insert into test.tblResults Values (@TestRunNo, '84', 'DataCurrent_Dimension_Note Table', 'Pass', Null)
End
Else
Begin
	
	Insert into test.tblResults Values (@TestRunNo, '84', 'DataCurrent_Dimension_Note Table', 'Fail', 'The last update in the dimension is' + cast (@LastDate as varchar(10)) + ' expect an update each day')
End

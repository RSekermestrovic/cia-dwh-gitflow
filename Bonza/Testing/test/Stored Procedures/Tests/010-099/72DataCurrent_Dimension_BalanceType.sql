﻿--Test Code
--exec Test.[72DataCurrent_Dimension_BalanceType] 'ADHOC'
Create Procedure Test.[72DataCurrent_Dimension_BalanceType] @TestRunNo varchar(23)

as

Select	*
into #DimensionBalance
From [$(Dimensional)].[Dimension].[BalanceType]  

Declare @NumberOfRecords int
Set @NumberOfRecords = 
					(
					Select		count(*) as NumberOfRecords
					From		#DimensionBalance
					)
If @NumberOfRecords = 10
Begin
	Insert into test.tblResults Values (@TestRunNo, '72', 'DataCurrent_Dimension_BalanceType Record Count', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '72', 'DataCurrent_Dimension_BalanceType Record Count', 'Fail', 'Expected 10 records, ' + Cast (@NumberOfRecords as varchar(5)) + ' found.')
End

IF 					(
					Select		Min (BalanceTypeKey) as Record
					From		#DimensionBalance
					where		BalanceTypeKey not in (0 ,-1)
					)
					= 2
Begin
	Insert into test.tblResults Values (@TestRunNo, '72.1', 'DataCurrent_Dimension_BalanceType Record Count', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '72.1', 'DataCurrent_Dimension_BalanceType Record Count', 'Fail', 'The lowest record key is not 2')
End

IF 					(
					Select		Max (BalanceTypeKey) as Record
					From		#DimensionBalance
					where		BalanceTypeKey not in (0 ,-1)
					)
					= 10
Begin
	Insert into test.tblResults Values (@TestRunNo, '72.2', 'DataCurrent_Dimension_BalanceType Record Count', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '72.2', 'DataCurrent_Dimension_BalanceType Record Count', 'Fail', 'The highest record key is not 10')
End


﻿--Test Code
--exec Test.[77DataCurrent_Dimension_Activity] 'ADHOC'
Create Procedure Test.[77DataCurrent_Dimension_Activity] @TestRunNo varchar(23)

as

Select	*
into #DimensionActivity
From [$(Dimensional)].[Dimension].[Activity]

Declare @NumberOfRecords int
Set @NumberOfRecords = 
					(
					Select		count(*) as NumberOfRecords
					From		#DimensionActivity
					)
If @NumberOfRecords = 34
Begin
	Insert into test.tblResults Values (@TestRunNo, '77', 'DataCurrent_Dimension_Activity Record Count', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '77', 'DataCurrent_Dimension_Activity Record Count', 'Fail', 'Expected 34 records, ' + Cast (@NumberOfRecords as varchar(5)) + ' found.')
End

IF 					(
					Select		Min (ActivityKey) as Record
					From		#DimensionActivity
					where		ActivityKey not in (0 ,-1)
					)
					= 1
Begin
	Insert into test.tblResults Values (@TestRunNo, '77.1', 'DataCurrent_Dimension_Activity Record Count', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '77.1', 'DataCurrent_Dimension_Activity Record Count', 'Fail', 'The lowest record key is not 1')
End

IF 					(
					Select		Max (ActivityKey) as Record
					From		#DimensionActivity
					where		ActivityKey not in (0 ,-1)
					)
					= 32
Begin
	Insert into test.tblResults Values (@TestRunNo, '77.2', 'DataCurrent_Dimension_Activity Record Count', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '77.2', 'DataCurrent_Dimension_Activity Record Count', 'Fail', 'The highest record key is not 32')
End


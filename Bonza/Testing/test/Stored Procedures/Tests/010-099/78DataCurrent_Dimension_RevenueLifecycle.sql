﻿--Test Code
--exec Test.[78DataCurrent_Dimension_RevenueLifecycle] 'ADHOC'
Create Procedure Test.[78DataCurrent_Dimension_RevenueLifecycle] @TestRunNo varchar(23)

as

Select	*
into #DimensionLifecycle
From [$(Dimensional)].[Dimension].[RevenueLifecycle]

Declare @NumberOfRecords int
Set @NumberOfRecords = 
					(
					Select		count(*) as NumberOfRecords
					From		#DimensionLifecycle
					)
If @NumberOfRecords = 5
Begin
	Insert into test.tblResults Values (@TestRunNo, '78', 'DataCurrent_Dimension_RevenueLifecycle Record Count', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '78', 'DataCurrent_Dimension_RevenueLifecycle Record Count', 'Fail', 'Expected 5 records, ' + Cast (@NumberOfRecords as varchar(5)) + ' found.')
End

IF 					(
					Select		Min (RevenueLifecycleKey) as Record
					From		#DimensionLifecycle
					where		RevenueLifecycleKey not in (0 ,-1)
					)
					= 1
Begin
	Insert into test.tblResults Values (@TestRunNo, '78.1', 'DataCurrent_Dimension_RevenueLifecycle Record Count', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '78.1', 'DataCurrent_Dimension_RevenueLifecycle Record Count', 'Fail', 'The lowest record key is not 1')
End

IF 					(
					Select		Max (RevenueLifecycleKey) as Record
					From		#DimensionLifecycle
					where		RevenueLifecycleKey not in (0 ,-1)
					)
					= 3
Begin
	Insert into test.tblResults Values (@TestRunNo, '78.2', 'DataCurrent_Dimension_RevenueLifecycle Record Count', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '78.2', 'DataCurrent_Dimension_RevenueLifecycle Record Count', 'Fail', 'The highest record key is not 3')
End
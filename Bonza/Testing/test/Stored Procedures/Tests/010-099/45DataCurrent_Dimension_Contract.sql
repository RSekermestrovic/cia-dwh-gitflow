﻿--Test Code
--exec Test.[45DataCurrent_Dimension_Contract] 'ADHOC'
Create Procedure Test.[45DataCurrent_Dimension_Contract] @TestRunNo varchar(23)

as
Declare @LastBatch int
Set @LastBatch = (Select Max([BatchKey]) From [$(Staging)].[Control].[ETLController])

Declare @BatchFromDate datetime
Set @BatchFromDate = (
						Select	BatchFromDate
						From	[$(Staging)].[Control].[ETLController]
						Where	[BatchKey] = @LastBatch
					)
If exists (
			Select		*
			From		(Select		top 1 *
						 From		[$(Dimensional)].Dimension.[Contract] as [Contract]
						 order by	[ContractKey] desc
						 ) as Data
			Where		CreatedDate > @BatchFromDate

		 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '45', 'DataCurrent_Dimension_Contract Table', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '45', 'DataCurrent_Dimension_Contract Table', 'Fail', 'No modified records found in the last batch.')
End
	



﻿--Test Code
--exec Test.[96DataCount_Dimension_Account] 'ADHOC'

Create Procedure [Test].[96DataCount_Dimension_Account] @TestRunNo varchar(23)

as

Declare @LastBatch int
Set @LastBatch = (Select Max([BatchKey]) From [$(Staging)].[Control].[ETLController] where BatchStatus = 1)

Declare @FromDate datetime2
Set @FromDate =	(	Select	BatchToDate
					From	[$(Staging)].[Control].[ETLController]
					Where	[BatchKey] = @LastBatch
				)

Declare @Counter int

Select	distinct AccountID
into	#Source
From	[$(Acquisition)].[IntraBet].[tblAccounts] with (nolock)
where	ToDate >= '9999-01-01'
		and FromDate <= @FromDate

Select	distinct [LedgerId]	
into	#Target
From	[$(Dimensional)].Dimension.Account
where	AccountKey not in (0,-1)


If not exists	(Select		*
				 From		#Source
							left join	#Target on #Source.AccountID = #Target.[LedgerId]
				 where		#Target.[LedgerId] is null
				)
		 
Begin
	Insert into Test.tblResults Values (@TestRunNo, '96', 'DataCount_Dimension_Account', 'Pass', Null)
End
Else
Begin
	IF exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-5167'
							and [Status] != 'Done'
				)
	Begin
		Insert into Test.tblResults Values (@TestRunNo, '96', 'DataCount_Dimension_Account', 'Pass', 'CCIA-5167')
	End
	Else
	Begin
		Insert into Test.tblResults Values (@TestRunNo, '96', 'DataCount_Dimension_Account', 'Fail', 'There are accounts in the Acquisition DB that have not been copied to the DW')
	End
End

set @Counter = 	(Select		count(*)
				 From		#Target
							left join #Source on #Target.[LedgerId] = #Source.AccountID
				 where		#Source.AccountID is null
							and #Target.[LedgerId] != -1
				)
If @Counter = 0		 
Begin
	Insert into Test.tblResults Values (@TestRunNo, '96.1', 'DataCount_Dimension_Account', 'Pass', Null)
End
Else
Begin	
	--CCIA-3681
	IF exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-3681'
							and [Status] != 'Done'
				)
	Begin
		Insert into Test.tblResults Values (@TestRunNo, '96.1', 'DataCount_Dimension_Account', 'Pass', 'CCIA-3681')
	End
	else
	Begin
		Insert into Test.tblResults Values (@TestRunNo, '96.1', 'DataCount_Dimension_Account', 'Fail', 'There are accounts in the DW that have not come from the Acquisition DB')
	End
End

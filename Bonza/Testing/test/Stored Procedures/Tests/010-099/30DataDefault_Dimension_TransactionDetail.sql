﻿--Test Code
--exec Test.[30DataDefault_Dimension_TransactionDetail] 'ADHOC'
Create Procedure Test.[30DataDefault_Dimension_TransactionDetail] @TestRunNo varchar(23)

as

If exists(
		Select		TransactionDetailKey
		From		[$(Dimensional)].[Dimension].[TransactionDetail]
		where		TransactionDetailKey = 0
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '30', 'DataDefault_Dimension_TransactionDetail Table ID0', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '30', 'DataDefault_Dimension_TransactionDetail Table ID0', 'Fail', 'No ID 0 record found')
End

If exists(
		Select		TransactionDetailKey
		From		[$(Dimensional)].[Dimension].[TransactionDetail]
		where		TransactionDetailKey = -1
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '30.1', 'DataDefault_Dimension_TransactionDetail Table ID-1', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '30.1', 'DataDefault_Dimension_TransactionDetail Table ID-1', 'Fail', 'No ID -1 record found')
End



﻿--Test Code
--exec Test.[82DataCurrent_Dimension_User] 'ADHOC'
Create Procedure Test.[82DataCurrent_Dimension_User] @TestRunNo varchar(23)

as

Declare	@BatchDate datetime2
Set	@BatchDate = (Select Max(BatchFromDate) From [$(Staging)].[Control].ETLController Where DimensionStatus = 1)

Declare @LastDate varchar(10)
Set		@LastDate = 
					(Select		max (CreatedDate)
					From		[$(Dimensional)].[Dimension].[User]
					)

If @LastDate >= @BatchDate or @LastDate >= DateAdd (Day,-14,getdate())
Begin
	Insert into test.tblResults Values (@TestRunNo, '82', 'DataCurrent_Dimension_User Table', 'Pass', Null)
End
Else
Begin
	--CCIA-3313 only has default records
	IF		(Select		count(*)
			 From		[$(Dimensional)].[Dimension].[User]
			 where		UserKey not in (-1,0)
			) = 0
			and	exists	(	Select		*
							From		test.tblDefects
							Where		DefectID = 'CCIA-3313'
										and [Status] != 'Done'
						)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '82', 'DataCurrent_Dimension_User Table', 'Pass', 'Defect CCIA-3313')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '82', 'DataCurrent_Dimension_User Table', 'Fail', 'The last update in the dimension is ' + cast (@LastDate as varchar(10)) + ' expect an update every other week')
	End
End
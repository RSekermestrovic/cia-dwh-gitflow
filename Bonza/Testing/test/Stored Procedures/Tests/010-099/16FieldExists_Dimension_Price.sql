--Test Code
--exec Test.[16FieldExists_Dimension_Price] 'ADHOC'
Create Procedure Test.[16FieldExists_Dimension_Price] @TestRunNo varchar(23)

as

Declare @Database varchar(100)
Set @Database = '[$(Dimensional)]'

Declare @SQL varchar(max)
Set @SQL = 'select top 1
[PriceKey]
    ,[PriceText]
    ,[Price]
    ,[Probability]
    ,[PriceBandKey]
    ,[PriceBandName]
    ,[PriceBandLowerBound]
    ,[PriceBandUpperBound]
    ,[FromDate]
    ,[ToDate]
    ,[FirstDate]
    ,[CreatedDate]
    ,[CreatedBatchKey]
    ,[CreatedBy]
    ,[ModifiedDate]
    ,[ModifiedBatchKey]
    ,[ModifiedBy]
	FROM ' + @Database + '.[Dimension].[Price]'

Exec Test.spSelectValid @TestRunNo, '16','Structure - Dimension Price Table', @SQL

If		(
		Select		Count(*)
		From		[$(Dimensional)].sys.all_objects as [Objects]
					inner join [$(Dimensional)].sys.all_columns as [Columns] on [Objects].[object_id] = [Columns].[object_id]
					inner join [$(Dimensional)].sys.schemas as [schemas] on [Objects].[schema_id] = [schemas].[schema_id]
		where		[Objects].name = 'Price'
					and [Objects].[type] = 'U'
					and [schemas].Name = 'Dimension'
		) = 17
Begin
	Insert into test.tblResults Values (@TestRunNo, '16.1', 'Structure - Dimension Price Table', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '16.1', 'Structure - Dimension Price Table', 'Fail', 'Wrong number of columns')
End

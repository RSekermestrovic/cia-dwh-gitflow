﻿--Test Code
--exec Test.[60DataDuplicates_Dimension_Time] 'ADHOC'

Create Procedure Test.[60DataDuplicates_Dimension_Time] @TestRunNo varchar(23)

as

If not exists(
		Select		Time1.TimeKey
					, Time1.TimeID
					, Time2.TimeKey
					, Time2.TimeID
		From		[$(Dimensional)].Dimension.[Time] as Time1
		inner join	[$(Dimensional)].Dimension.[Time] as Time2 on Time1.TimeID = Time2.TimeID
		Where		Time1.TimeKey != Time2.TimeKey
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '60', 'DataDuplicates_Dimension_Time', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '60', 'DataDuplicates_Dimension_Time', 'Fail', 'Duplicates of TimeID found')
End


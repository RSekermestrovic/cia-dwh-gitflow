﻿--Test Code
--exec Test.[39DataCount_Dimension_Time] 'ADHOC'
Create Procedure Test.[39DataCount_Dimension_Time] @TestRunNo varchar(23)

as

If (
		Select		count(*) as CountAll
		From		[$(Dimensional)].[Dimension].[Time]
		) = 1442
Begin
	Insert into test.tblResults Values (@TestRunNo, '39', 'DataCount_Dimension_Time', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '39', 'DataCount_Dimension_Time', 'Fail', 'The number of records is incorrect, it should be 1442')
End



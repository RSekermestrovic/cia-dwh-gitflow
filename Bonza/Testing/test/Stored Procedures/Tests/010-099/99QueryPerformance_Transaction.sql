﻿--Test Code
--exec Test.[99QueryPerformance_Transaction] 'JF-5May_23'
Create Procedure [Test].[99QueryPerformance_Transaction] @TestRunNo varchar(23)

as

Declare @Time int

--Simple (1 star, 1 day)
exec [Test].[spExecuteMDX] @TestRunNo, 'Transaction', 'Simple', 10000,
	' Select * From OpenQuery([ANALYSISSERVICES],''
		SELECT 
		NON EMPTY
		{[Measures].[TransactionCount],[Measures].[TransactedAmount]} on Columns,
		[Company].[CompanyName].[CompanyName] on rows
		FROM [Bonza]
		WHERE 
		([Day].[DayDate].&[2015-03-03T00:00:00])
	'')'


Set @Time = 	(
				Select		MSeconds
				From		[Test].[tblQueryPerformance]
				where		[RunID] = @TestRunNo
							and [TestID] = 'Transaction'
							and [TestName] = 'Simple'
				)
IF @Time <= 10000
Begin
	Insert into test.tblResults ([RunID], [TestID], [TestName], [Status], [Notes]) Values (@TestRunNo, '99.1', 'QueryPerformance_Transaction_Simple', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults ([RunID], [TestID], [TestName], [Status], [Notes]) Values (@TestRunNo, '99.1', 'QueryPerformance_Transaction_Simple', 'Fail', 'Time taken is ' + cast (@Time as varchar(100)) + ' msec and should be less than 10000')
End

--Medium (1 star, 2 months)
exec [Test].[spExecuteMDX] @TestRunNo, 'Transaction', 'Medium', 60000,
	' Select * From OpenQuery([ANALYSISSERVICES],''
		WITH MEMBER [Measures].[AveBets] AS [Measures].[BetCount] / [Measures].[Bettors]
		SELECT 
		NON EMPTY
		{[Measures].[TransactionCount], [Measures].[TransactedAmount]} on Columns,
		CROSSJOIN([Company].[BusinessUnitName].[BusinessUnitName],[Day].[FiscalWeekStartDate].MEMBERS) on rows
		FROM [Bonza]
		WHERE 
		{([Day].[FiscalMonthText].&[Mar-2015],[Company].[CompanyName].&[William Hill Australia])
		, ([Day].[FiscalMonthText].&[Apr-2015],[Company].[CompanyName].&[William Hill Australia])}
	'')'


Set @Time = 	(
				Select		MSeconds
				From		[Test].[tblQueryPerformance]
				where		[RunID] = @TestRunNo
							and [TestID] = 'Transaction'
							and [TestName] = 'Medium'
				)
IF @Time <= 60000
Begin
	Insert into test.tblResults ([RunID], [TestID], [TestName], [Status], [Notes]) Values (@TestRunNo, '99.1', 'QueryPerformance_Transaction_Medium', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults ([RunID], [TestID], [TestName], [Status], [Notes]) Values (@TestRunNo, '99.1', 'QueryPerformance_Transaction_Medium', 'Fail', 'Time taken is ' + cast (@Time as varchar(100)) + ' msec and should be less than 60000')
End

--Complex (2 stars, 2 months)
exec [Test].[spExecuteMDX] @TestRunNo, 'Transaction', 'Complex', 1200000,
	' Select * From OpenQuery([ANALYSISSERVICES],''
		WITH MEMBER [Measures].[AveBets] AS [Measures].[BetCount] / [Measures].[Bettors]
		SELECT 
		NON EMPTY
		{[Measures].[TransactionCount], [Measures].[TransactedAmount],[Measures].[TotalTransactedAmount]} on Columns,
		CROSSJOIN([Company].[BusinessUnitName].[BusinessUnitName],[Day].[FiscalWeekStartDate].MEMBERS) on rows
		FROM [Bonza]
		WHERE 
		{([Day].[FiscalMonthText].&[Mar-2015],[Company].[CompanyName].&[William Hill Australia])
		, ([Day].[FiscalMonthText].&[Apr-2015],[Company].[CompanyName].&[William Hill Australia])}
	'')'


Set @Time = 	(
				Select		MSeconds
				From		[Test].[tblQueryPerformance]
				where		[RunID] = @TestRunNo
							and [TestID] = 'Transaction'
							and [TestName] = 'Complex'
				)
IF @Time <= 1200000
Begin
	Insert into test.tblResults ([RunID], [TestID], [TestName], [Status], [Notes]) Values (@TestRunNo, '99.2', 'QueryPerformance_Transaction_Complex', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults ([RunID], [TestID], [TestName], [Status], [Notes]) Values (@TestRunNo, '99.2', 'QueryPerformance_Transaction_Complex', 'Fail', 'Time taken is ' + cast (@Time as varchar(100)) + ' msec and should be less than 1200000')
End
GO



--Test Code
--exec Test.[69TableExists_Star_Bet] 'ADHOC'
Create Procedure Test.[69TableExists_Star_Bet] @TestRunNo varchar(23)

as

Declare @Database varchar(100)
Set @Database = '[$(Dimensional)]'

Insert into test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('69.0','Structure - Bet Table', 'Fact', 'Bet')

Insert into test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('69.7','Structure - Day Table', 'Dimension', 'Day')

Insert into test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('69.13','Structure - Time Table', 'Dimension', 'Time')

Insert into test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('69.12','Structure - Competitor Table', 'Dimension', 'Competitor') -- should be selection

Insert into test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('69.1','Structure - Price Table', 'Dimension', 'Price')

Insert into test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('69.2','Structure - Account Table', 'Dimension', 'Account')

Insert into test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('69.3','Structure - Wallet Table', 'Dimension', 'Wallet')

Insert into test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('69.4','Structure - Promotion Table', 'Dimension', 'Promotion')

Insert into test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('69.5','Structure - Channel Table', 'Dimension', 'Channel')

Insert into test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('69.6','Structure - Contract Table', 'Dimension', 'Contract')

Insert into test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('69.12','Structure - Market Table', 'Dimension', 'Market')

Insert into test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('69.11','Structure - PriceType Table', 'Dimension', 'PriceType')

Insert into test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('69.10','Structure - Class Table', 'Dimension', 'Class')

Insert into test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('69.9','Structure - BetType Table', 'Dimension', 'BetType')

Insert into test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('69.8','Structure - BetStatus Table', 'Dimension', 'BetStatus')



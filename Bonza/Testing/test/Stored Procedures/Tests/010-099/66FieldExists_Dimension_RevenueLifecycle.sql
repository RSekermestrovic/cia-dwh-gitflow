--Test Code
--exec Test.[66FieldExists_Dimension_RevenueLifecycle] 'ADHOC'
Create Procedure Test.[66FieldExists_Dimension_RevenueLifecycle] @TestRunNo varchar(23)

as

Declare @Database varchar(100)
Set @Database = '[$(Dimensional)]'

Declare @SQL varchar(max)
Set @SQL = 'Select top 1 [RevenueLifeCycleKey]
      ,[RevenueLifeCycle]
      --,[FromDate]
      --,[ToDate]
      --,[FirstDate]
      ,[CreatedDate]
      ,[CreatedBatchKey]
      ,[CreatedBy]
      ,[ModifiedDate]
      ,[ModifiedBatchKey]
      ,[ModifiedBy]
  FROM ' + @Database + '.[Dimension].[RevenueLifecycle]'

Exec Test.spSelectValid @TestRunNo, '66','Structure - Dimension RevenueLifecycle Table', @SQL

If		(
		Select		Count(*)
		From		[$(Dimensional)].sys.all_objects as [Objects]
					inner join [$(Dimensional)].sys.all_columns as [Columns] on [Objects].[object_id] = [Columns].[object_id]
					inner join [$(Dimensional)].sys.schemas as [schemas] on [Objects].[schema_id] = [schemas].[schema_id]
		where		[Objects].name = 'RevenueLifecycle'
					and [Objects].[type] = 'U'
					and [schemas].Name = 'Dimension'
		) = 11
Begin
	Insert into test.tblResults Values (@TestRunNo, '66.1', 'Structure - Dimension RevenueLifecycle Table', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-6167'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '66.1', 'Structure - Dimension RevenueLifecycle Table', 'Pass', 'CCIA-6167')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '66.1', 'Structure - Dimension RevenueLifecycle Table', 'Fail', 'Wrong number of columns')
	End
End
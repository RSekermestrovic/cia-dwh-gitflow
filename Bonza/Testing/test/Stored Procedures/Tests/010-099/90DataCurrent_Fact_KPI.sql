﻿--Test Code
--exec Test.[90DataCurrent_Fact_KPI] 'ADHOC'
Create Procedure Test.[90DataCurrent_Fact_KPI] @TestRunNo varchar(23)

as
/*
Declare @LastBatch int
Set @LastBatch = (Select Max([BatchKey]) From [$(Staging)].[Control].[ETLController] where BatchStatus = 1)

Select	*
into	#BatchDetails
From	[$(Staging)].[Control].[ETLController]
Where	[BatchKey] = @LastBatch

If	(
	Select	KPIFactRowsProcessed
	From	#BatchDetails
	) >0

Begin
	If  (
			(Select		max ([Day].DayDate)
			 From		[$(Dimensional)].[Fact].KPI as KPI
						inner join [$(Dimensional)].Dimension.[DayZone] as [DayZone] on KPI.DayKey = [DayZone].DayKey
						inner join [$(Dimensional)].Dimension.[Day] as [Day] on [DayZone].MasterDayKey = [Day].DayKey
			) >= 
			(Select		Cast (BatchFromDate as Date)
			 From		#BatchDetails
			)
		)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '90', 'DataCurrent_Fact_KPI Table', 'Pass', Null)
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '90', 'DataCurrent_Fact_KPI Table', 'Fail', 'The batch history says records were found, but none are in the table.')
	End
	

End

Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '90', 'DataCurrent_Fact_KPI Table', 'Fail', 'The batch history says no records were found in the source systems')
	End

*/
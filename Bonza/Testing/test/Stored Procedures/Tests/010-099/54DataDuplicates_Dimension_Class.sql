﻿--Test Code
--exec Test.[54DataDuplicates_Dimension_Class] 'ADHOC', null

Create Procedure Test.[54DataDuplicates_Dimension_Class] @TestRunNo varchar(23), @FromDate datetime

as
--CCIA-3316 Duplicates in the table

Declare @ErrorText varchar(100)
If @FromDate is not null
Begin
Set @ErrorText = ' since ' + Convert (varchar(20), @FromDate, 103)
End
else
Begin
Set @ErrorText = '' 
End


If not exists(
		Select		Class1.ClassKey
					, Class1.ClassID
					, Class1.FromDate
					, Class1.ToDate
					, Class2.ClassKey
					, Class2.ClassID
					, Class2.FromDate
					, Class2.ToDate
		From		[$(Dimensional)].Dimension.Class as Class1
		inner join	[$(Dimensional)].Dimension.Class as Class2 on Class1.ClassID = Class2.ClassID
		Where		Class1.ClassKey != Class2.ClassKey
					and 
						((Class1.FromDate = Class2.FromDate)
						Or (Class1.ToDate = Class2.ToDate))
					and (
						(Class1.FromDate > @FromDate)
						or (@FromDate is null)
						)
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '54', 'DataDuplicates_Dimension_Class', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '54', 'DataDuplicates_Dimension_Class', 'Fail', 'Duplicates of ClassID found' + @ErrorText)
End


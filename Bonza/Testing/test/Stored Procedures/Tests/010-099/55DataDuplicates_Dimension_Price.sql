﻿--Test Code
--exec Test.[55DataDuplicates_Dimension_Price] 'ADHOC'

Create Procedure Test.[55DataDuplicates_Dimension_Price] @TestRunNo varchar(23)

as
/*
--CCIA-3317 the table is empty

If not exists(
		Select		Price1.PriceKey
					, Price1.PriceText
					, Price2.PriceKey
					, Price2.PriceText
		From		[$(Dimensional)].Dimension.Price as Price1
		inner join	[$(Dimensional)].Dimension.Price as Price2 on Price1.PriceText = Price2.PriceText
		Where		Price1.PriceKey != Price2.PriceKey
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '55', 'DataDuplicates_Dimension_Price', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '55', 'DataDuplicates_Dimension_Price', 'Fail', 'Duplicates of PriceID found')
End
*/


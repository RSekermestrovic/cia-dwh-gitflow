﻿--Test Code
--exec Test.[76DataCurrent_Dimension_BetType] 'ADHOC'
Create Procedure Test.[76DataCurrent_Dimension_BetType] @TestRunNo varchar(23)

as

Select	*
into #DimensionBetType
From [$(Dimensional)].Dimension.[BetType]

Declare @NumberOfRecords int
Set @NumberOfRecords = 
					(
					Select		count(*) as NumberOfRecords
					From		#DimensionBetType
					)
If @NumberOfRecords >= 40
Begin
	Insert into test.tblResults Values (@TestRunNo, '76', 'DataCurrent_Dimension_BetType Record Count', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '76', 'DataCurrent_Dimension_BetType Record Count', 'Fail', 'Expected at least 40 records, ' + Cast (@NumberOfRecords as varchar(5)) + ' found.')
End

IF 					(
					Select		Min (BetTypeKey) as Record
					From		#DimensionBetType
					where		BetTypeKey not in (0 ,-1)
					)
					= 1
Begin
	Insert into test.tblResults Values (@TestRunNo, '76.1', 'DataCurrent_Dimension_BetType Record Count', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '76.1', 'DataCurrent_Dimension_BetType Record Count', 'Fail', 'The lowest record key is not 1')
End

IF 					(
					Select		Max (BetTypeKey) as Record
					From		#DimensionBetType
					where		BetTypeKey not in (0 ,-1)
					)
					>= 38
Begin
	Insert into test.tblResults Values (@TestRunNo, '76.2', 'DataCurrent_Dimension_BetType Record Count', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '76.2', 'DataCurrent_Dimension_BetType Record Count', 'Fail', 'The highest record key should be over 38')
End



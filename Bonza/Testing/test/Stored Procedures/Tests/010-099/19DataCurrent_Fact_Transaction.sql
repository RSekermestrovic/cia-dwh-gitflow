﻿--Test Code
--exec Test.[19DataCurrent_Fact_Transaction] 'ADHOC'
Create Procedure Test.[19DataCurrent_Fact_Transaction] @TestRunNo varchar(23)

as
Declare @LastBatch int
Set @LastBatch = (Select Max([BatchKey]) From [$(Staging)].[Control].[ETLController] where BatchStatus = 1)

Select	*
into	#BatchDetails
From	[$(Staging)].[Control].[ETLController]
Where	[BatchKey] = @LastBatch

If	(
	Select	TransactionRowsProcessed
	From	#BatchDetails
	) >0

Begin
	If  (
			(Select		top 1 [Day].DayDate
			 From		[$(Dimensional)].[Fact].[Transaction] as [Transaction]
						inner join [$(Dimensional)].Dimension.[DayZone] as [DayZone] on [Transaction].DayKey = DayZone.DayKey
						inner join [$(Dimensional)].Dimension.[Day] as [Day] on [DayZone].MasterDayKey = [Day].DayKey
			 order by	[Transaction].DayKey desc
			) >= 
			(Select		Cast (BatchFromDate as date)
			 From		#BatchDetails
			)
		)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '19', 'DataCurrent_Fact_Transaction Table', 'Pass', Null)
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '19', 'DataCurrent_Fact_Transaction Table', 'Fail', 'The batch history says records were found, but none are in the table.')
	End
	

End

Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '19', 'DataCurrent_Fact_Transaction Table', 'Fail', 'The batch history says no records were found in the source systems')
	End


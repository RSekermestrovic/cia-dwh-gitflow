﻿--Test Code
--exec Test.[24DataDefault_Dimension_Time] 'ADHOC'
Create Procedure Test.[24DataDefault_Dimension_Time] @TestRunNo varchar(23)

as

If exists(
		Select		TimeKey
		From		[$(Dimensional)].[Dimension].[Time]
		where		TimeKey = 0
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '24', 'DataDefault_Dimension_Time Table ID0', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '24', 'DataDefault_Dimension_Time Table ID0', 'Fail', 'No ID 0 record found')
End

If exists(
		Select		TimeKey
		From		[$(Dimensional)].[Dimension].[Time]
		where		TimeKey = -1
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '24.1', 'DataDefault_Dimension_Time Table ID-1', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '24.1', 'DataDefault_Dimension_Time Table ID-1', 'Fail', 'No ID -1 record found')
End



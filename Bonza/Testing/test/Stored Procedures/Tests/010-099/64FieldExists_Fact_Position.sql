--Test Code
--exec Test.[64FieldExists_Fact_Position] 'ADHOC'
Create Procedure Test.[64FieldExists_Fact_Position] @TestRunNo varchar(23)

as

Declare @Database varchar(100)
Set @Database = '[$(Dimensional)]'

Declare @SQL varchar(max)
Set @SQL = 'Select top 1 [BalanceTypeKey]
      ,[AccountKey]
      ,[AccountStatusKey]
      ,[LedgerID]
      ,[LedgerSource]
      ,[DayKey]
      ,[ActivityKey]
      ,[FirstDepositDayKey]
      ,[FirstBetDayKey]
      ,[FirstBetTypeKey]
      ,[FirstClassKey]
      ,[FirstChannelKey]
	  ,[LastDepositDayKey]
      ,[LastBetDayKey]
      ,[LastBetTypeKey]
      ,[LastClassKey]
      ,[LastChannelKey]
      ,[OpeningBalance]
      ,[TransactedAmount]
      ,[ClosingBalance]
	  ,[DaysSinceAccountOpened]
      ,[DaysSinceFirstDeposit]
      ,[DaysSinceFirstBet]
      ,[DaysSinceLastDeposit]
      ,[DayssinceLastBet]
      ,[LifetimeTurnover]
      ,[LifetimeGrossWin]
      ,[LifetimeBetCount]
      ,[LifetimeBonus]
      ,[LifetimeDeposits]
      ,[LifetimeWithdrawals]
      ,[TurnoverMTDCalender]
      ,[TurnoverMTDFiscal]
      ,[TierKey]
      ,[TierKeyFiscal]
	  ,[RevenueLifeCycleKey]
      ,[LifeStageKey]
      ,[PreviousLifeStageKey]
      ,[CreatedDate]
      ,[CreatedBatchKey]
      ,[CreatedBy]
      ,[ModifiedDate]
      ,[ModifiedBatchKey]
      ,[ModifiedBy]
	  , StructureKey
  FROM ' + @Database + '.[Fact].[Position]'

Exec Test.spSelectValid @TestRunNo, '64','Structure - Fact Position Table', @SQL

If		(
		Select		Count(*)
		From		[$(Dimensional)].sys.all_objects as [Objects]
					inner join [$(Dimensional)].sys.all_columns as [Columns] on [Objects].[object_id] = [Columns].[object_id]
					inner join [$(Dimensional)].sys.schemas as [schemas] on [Objects].[schema_id] = [schemas].[schema_id]
		where		[Objects].name = 'Position'
					and [Objects].[type] = 'U'
					and [schemas].Name = 'Fact'
		) = 45
Begin
	Insert into test.tblResults Values (@TestRunNo, '64.1', 'Structure - Fact Position Table', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '64.1', 'Structure - Fact Position Table', 'Fail', 'Wrong number of columns')
End


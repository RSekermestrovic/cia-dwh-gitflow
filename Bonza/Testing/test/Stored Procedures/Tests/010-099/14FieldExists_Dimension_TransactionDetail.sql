--Test Code
--exec Test.[14FieldExists_Dimension_TransactionDetail] 'ADHOC3'
Create Procedure Test.[14FieldExists_Dimension_TransactionDetail] @TestRunNo varchar(23)

as

Declare @Database varchar(100)
Set @Database = '[$(Dimensional)]'

Declare @SQL varchar(max)
Set @SQL = 'select top 1
[TransactionDetailKey]
      ,[TransactionDetailId]
      ,[TransactionTypeID]
      ,[TransactionType]
      ,[TransactionStatusID]
      ,[TransactionStatus]
      ,[TransactionMethodID]
      ,[TransactionMethod]
      ,[TransactionDirection]
      ,[TransactionSign]
      ,[FromDate]
      ,[ToDate]
      ,[FirstDate]
      ,[CreatedDate]
      ,[CreatedBatchKey]
      ,[CreatedBy]
      ,[ModifiedDate]
      ,[ModifiedBatchKey]
      ,[ModifiedBy]
  FROM ' + @Database + '.[Dimension].[TransactionDetail]'

Exec Test.spSelectValid @TestRunNo, '14','Structure - Dimension TransactionDetail Table', @SQL

If		(
		Select		Count(*)
		From		[$(Dimensional)].sys.all_objects as [Objects]
					inner join [$(Dimensional)].sys.all_columns as [Columns] on [Objects].[object_id] = [Columns].[object_id]
					inner join [$(Dimensional)].sys.schemas as [schemas] on [Objects].[schema_id] = [schemas].[schema_id]
		where		[Objects].name = 'TransactionDetail'
					and [Objects].[type] = 'U'
					and [schemas].Name = 'Dimension'
		) = 19
Begin
	Insert into test.tblResults Values (@TestRunNo, '14.1', 'Structure - Dimension TransactionDetail Table', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '14.1', 'Structure - Dimension TransactionDetail Table', 'Fail', 'Wrong number of columns')
End


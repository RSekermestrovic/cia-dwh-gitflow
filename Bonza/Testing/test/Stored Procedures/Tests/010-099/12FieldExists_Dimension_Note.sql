--Test Code
--exec Test.[12FieldExists_Dimension_Note] 'ADHOC'
Create Procedure Test.[12FieldExists_Dimension_Note] @TestRunNo varchar(23)

as

Declare @Database varchar(100)
Set @Database = '[$(Dimensional)]'

Declare @SQL varchar(max)
Set @SQL = 'select top 1
	   [NoteKey]
      ,[NoteId]
      ,[NoteSource]
      ,[Notes]
      ,[FromDate]
      ,[ToDate]
      ,[FirstDate]
      ,[CreatedDate]
      ,[CreatedBatchKey]
      ,[CreatedBy]
      ,[ModifiedDate]
      ,[ModifiedBatchKey]
      ,[ModifiedBy]
  FROM ' + @Database + '.[Dimension].[Note]'

Exec Test.spSelectValid @TestRunNo, '12','Structure - Dimension Note Table', @SQL

If		(
		Select		Count(*)
		From		[$(Dimensional)].sys.all_objects as [Objects]
					inner join [$(Dimensional)].sys.all_columns as [Columns] on [Objects].[object_id] = [Columns].[object_id]
					inner join [$(Dimensional)].sys.schemas as [schemas] on [Objects].[schema_id] = [schemas].[schema_id]
		where		[Objects].name = 'Note'
					and [Objects].[type] = 'U'
					and [schemas].Name = 'Dimension'
		) = 13
Begin
	Insert into test.tblResults Values (@TestRunNo, '12.1', 'Structure - Dimension Note Table', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '12.1', 'Structure - Dimension Note Table', 'Fail', 'Wrong number of columns')
End


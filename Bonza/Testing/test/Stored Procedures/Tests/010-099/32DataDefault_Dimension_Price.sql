﻿--Test Code
--exec Test.[32DataDefault_Dimension_Price] 'ADHOC'
Create Procedure Test.[32DataDefault_Dimension_Price] @TestRunNo varchar(23)

as
/*
If exists(
		Select		PriceKey
		From		[$(Dimensional)].[Dimension].[Price]
		where		PriceKey = 0
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '32', 'DataDefault_Dimension_Price Table ID0', 'Pass', Null)
End
Else
Begin
	--CCIA-3263 no -1,0 records
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-3263'
							and [status] = 'Open'
				)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '32', 'DataDefault_Dimension_Price Table ID0', 'Pass', 'Known Defect CCIA-3263')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '32', 'DataDefault_Dimension_Price Table ID0', 'Fail', 'No ID 0 record found')
	End
End


If exists(
		Select		PriceKey
		From		[$(Dimensional)].[Dimension].[Price]
		where		PriceKey = -1
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '32.1', 'DataDefault_Dimension_Price Table ID-1', 'Pass', Null)
End
Else
Begin
	--CCIA-3263 no -1,0 records
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-3263'
							and [status] = 'Open'
				)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '32', 'DataDefault_Dimension_Price Table ID-1', 'Pass', 'Known Defect CCIA-3263')
	End
	Else
	Begin
	Insert into test.tblResults Values (@TestRunNo, '32.1', 'DataDefault_Dimension_Price Table ID-1', 'Fail', 'No ID -1 record found')
	End
End
*/

Go

﻿--Test Code
--exec Test.[97BVA_Dimension_Day] 'ADHOC', null
--exec Test.[97BVA_Dimension_Day] 'ADHOC', '2015-04-30'

Create Procedure Test.[97BVA_Dimension_Day] @TestRunNo varchar(23), @FromDate datetime

as

Declare @ErrorText varchar(100)
If @FromDate is null
Begin
	set @FromDate = getdate()
End

Declare @FromDateD date
set @FromDateD = @FromDate

--Records up to the last batch date exist
If exists	(
			Select		Daydate
			From		[$(Dimensional)].Dimension.[Day]
			where		DayKey Not in (0, -1)
						and Daydate = @FromDateD
			) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '97', 'BVA_Dimension_Day', 'Pass', Null)
End
Else
Begin
	Set @ErrorText = (
					 Select		Cast (Max (Daydate) as varchar(100))
					 From		[$(Dimensional)].Dimension.[Day]
					 where		DayKey Not in (0, -1)
					 )
	Insert into test.tblResults Values (@TestRunNo, '97', 'BVA_Dimension_Day', 'Fail', 'There are records missing, the last date was ' + @ErrorText)
End

--There are no records past the last batch date
If not exists	(
				Select		Daydate
				From		[$(Dimensional)].Dimension.[Day]
				where		DayKey Not in (0, -1)
							and Daydate > DateAdd (Year, 2, getdate())
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '97.1', 'BVA_Dimension_Day', 'Pass', Null)
End
Else
Begin
	Set @ErrorText = (
					 Select		Cast (Max (Daydate) as varchar(100))
					 From		[$(Dimensional)].Dimension.[Day]
					 where		DayKey Not in (0, -1)
					 )
	Insert into test.tblResults Values (@TestRunNo, '97.1', 'BVA_Dimension_Day', 'Fail', 'There are additional records, the last date was ' + @ErrorText)
End

--Records up to the last batch date exist
If exists	(
			Select		MasterDayText
			From		[$(Dimensional)].Dimension.[DayZone]
			where		DayKey Not in (0, -1)
						and MasterDayText > Replace (Convert (Varchar(10),@FromDateD, 111),'/', '-')
			) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '97.2', 'BVA_Dimension_Day', 'Pass', Null)
End
Else
Begin
	Set @ErrorText = (
					 Select		Cast (Max (MasterDayText) as varchar(100))
					 From		[$(Dimensional)].Dimension.[DayZone]
					 where		DayKey Not in (0, -1)
								and MasterDayText != 'N/A'
					 )
	Insert into test.tblResults Values (@TestRunNo, '97.2', 'BVA_Dimension_Day', 'Fail', 'There are records missing, the last date was ' + @ErrorText)
End

--There are no records past today
If not exists	(
				Select		MasterDayText
				From		[$(Dimensional)].Dimension.[DayZone]
				where		DayKey Not in (0, -1)
							and MasterDayText != 'N/A'
							and MasterDayText > Replace (Convert (Varchar(10),DateAdd (Year, 2, getdate()), 111),'/', '-')
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '97.3', 'BVA_Dimension_DayZone', 'Pass', Null)
End
Else
Begin
	Set @ErrorText = (
					 Select		Cast (Max (MasterDayText) as varchar(100))
					 From		[$(Dimensional)].Dimension.[DayZone]
					 where		DayKey Not in (0, -1)
								and MasterDayText != 'N/A'
					 )
	Insert into test.tblResults Values (@TestRunNo, '97.3', 'BVA_Dimension_DayZone', 'Fail', 'There are additional records, the last date was ' + @ErrorText)
End




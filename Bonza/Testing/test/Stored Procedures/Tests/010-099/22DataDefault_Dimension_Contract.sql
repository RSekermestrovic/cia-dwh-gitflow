﻿--Test Code
--exec Test.[22DataDefault_Dimension_Contract] 'ADHOC'
Create Procedure Test.[22DataDefault_Dimension_Contract] @TestRunNo varchar(23)

as

If exists(
		Select		ContractKey
		From		[$(Dimensional)].Dimension.[Contract]
		where		ContractKey = 0
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '22', 'DataDefault_Dimension_Contract Table ID0', 'Pass', Null)
End
Else
Begin
	If  exists	(Select		DefectID
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-4503'
							and [Status] != 'Done'
				)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '22', 'DataDefault_Dimension_Contract Table ID0', 'Pass', 'CCIA-4503')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '22', 'DataDefault_Dimension_Contract Table ID0', 'Fail', 'No ID 0 record found')
	End
End

If exists(
		Select		ContractKey
		From		[$(Dimensional)].Dimension.[Contract]
		where		ContractKey = -1
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '22.1', 'DataDefault_Dimension_Contract Table ID-1', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '22.1', 'DataDefault_Dimension_Contract Table ID-1', 'Fail', 'No ID -1 record found')
End






﻿--Test Code
--exec Test.[21DataDefault_Dimension_Promotion] 'ADHOC'
Create Procedure Test.[21DataDefault_Dimension_Promotion] @TestRunNo varchar(23)

as

If exists(
		Select		PromotionId
		From		[$(Dimensional)].[Dimension].Promotion
		where		PromotionId = 0
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '21', 'DataDefault_Dimension_Promotion Table ID0', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '21', 'DataDefault_Dimension_Promotion Table ID0', 'Fail', 'No ID 0 record found')
End

If exists(
		Select		PromotionId
		From		[$(Dimensional)].[Dimension].Promotion
		where		PromotionId = -1
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '21.1', 'DataDefault_Dimension_Promotion Table ID-1', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '21.1', 'DataDefault_Dimension_Promotion Table ID-1', 'Fail', 'No ID -1 record found')
End






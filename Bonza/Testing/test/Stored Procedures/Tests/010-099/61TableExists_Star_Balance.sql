--Test Code
--exec Test.[61TableExists_Star_Balance] 'ADHOC'
Create Procedure Test.[61TableExists_Star_Balance] @TestRunNo varchar(23)

as

Declare @Database varchar(100)
Set @Database = '[$(Dimensional)]'

Insert into test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('61.0','Structure - Balance Table', 'Fact', 'Balance')

Insert into test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('61.1','Structure - Account Table', 'Dimension', 'Account')

Insert into test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('61.3','Structure - Day Table', 'Dimension', 'Day')

Insert into test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('61.4','Structure - BalanceType Table', 'Dimension', 'BalanceType')

Insert into test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('61.2','Structure - Wallet Table', 'Dimension', 'Wallet')

Insert into test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('61.3','Structure - Structure Table', 'Dimension', 'Structure')



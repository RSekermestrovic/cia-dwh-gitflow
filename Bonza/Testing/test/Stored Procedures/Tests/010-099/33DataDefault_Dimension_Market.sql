﻿--Test Code
--exec Test.[33DataDefault_Dimension_Market] 'ADHOC'
Create Procedure Test.[33DataDefault_Dimension_Market] @TestRunNo varchar(23)

as

If exists(
		Select		MarketKey
		From		[$(Dimensional)].[Dimension].[Market]
		where		MarketKey = 0
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '33', 'DataDefault_Dimension_Market Table ID0', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '33', 'DataDefault_Dimension_Market Table ID0', 'Fail', 'No ID 0 record found')
End

--CCIA-3264 No ID -1 record
If exists(
		Select		MarketKey
		From		[$(Dimensional)].[Dimension].[Market]
		where		MarketKey = -1
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '33.1', 'DataDefault_Dimension_Market Table ID-1', 'Pass', Null)
End
Else
Begin
If exists		(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-3264'
							and [Status] != 'Done'
				) 
	and exists (
		Select		MarketKey
		From		[$(Dimensional)].[Dimension].[Market]
		where		MarketKey = -99
		)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '33.1', 'DataDefault_Dimension_Market Table ID-1', 'Pass', 'CCIA-3264')
	End
	Else
	Begin  
		Insert into test.tblResults Values (@TestRunNo, '33.1', 'DataDefault_Dimension_Market Table ID-1', 'Fail', 'No ID -1 record found')
	End
End



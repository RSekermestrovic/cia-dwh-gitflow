﻿--Test Code
--exec Test.[89DataCurrent_Fact_Position] 'ADHOC'
Create Procedure Test.[89DataCurrent_Fact_Position] @TestRunNo varchar(23)

as
/*
Declare @LastBatch int
Set @LastBatch = (Select Max([BatchKey]) From [$(Staging)].[Control].[ETLController] where BatchStatus = 1)

Select	*
into	#BatchDetails
From	[$(Staging)].[Control].[ETLController]
Where	[BatchKey] = @LastBatch

If	(
	Select	PositionFactRowsProcessed
	From	#BatchDetails
	) >0

Begin
	If (
		Select		count(*) as NumberofRecords
		From		[$(Dimensional)].[Fact].Position as Position
					inner join [$(Dimensional)].Dimension.[DayZone] as [DayZone] on Position.DayKey = DayZone.DayKey
					inner join [$(Dimensional)].Dimension.[Day] as [Day] on DayZone.MasterDayKey = [Day].DayKey
					cross join #BatchDetails
		where		[Day].DayDate > #BatchDetails.BatchFromDate

	) > 0
	Begin
		Insert into test.tblResults Values (@TestRunNo, '89', 'DataCurrent_Fact_Position Table', 'Pass', Null)
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '89', 'DataCurrent_Fact_Position Table', 'Fail', 'The batch history differs from the records in the table.')
	End
	

End

Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '89', 'DataCurrent_Fact_Position Table', 'Fail', 'The batch history says no records were found in the source systems')
	End

*/
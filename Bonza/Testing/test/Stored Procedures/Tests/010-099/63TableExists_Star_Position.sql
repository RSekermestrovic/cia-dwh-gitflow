--Test Code
--exec Test.[63TableExists_Star_Position] 'ADHOC'
Create Procedure Test.[63TableExists_Star_Position] @TestRunNo varchar(23)

as

Declare @Database varchar(100)
Set @Database = '[$(Dimensional)]'

Insert into test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('63.0','Structure - Class Table', 'Fact', 'Position')

Insert into test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('63.1','Structure - BalanceType Table', 'Dimension', 'BalanceType')

Insert into test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('63.2','Structure - Account Table', 'Dimension', 'Account')

Insert into test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('63.3','Structure - AccountStatus Table', 'Dimension', 'AccountStatus')

Insert into test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('63.4','Structure - Day Table', 'Dimension', 'Day')

Insert into test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('63.5','Structure - Activity Table', 'Dimension', 'Activity')

Insert into test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('63.6','Structure - RevenueLifecycle Table', 'Dimension', 'RevenueLifecycle')

Insert into test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('63.7','Structure - BetType Table', 'Dimension', 'BetType')

Insert into test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('63.8','Structure - Class Table', 'Dimension', 'Class')

Insert into test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('63.9','Structure - Channel Table', 'Dimension', 'Channel')

Insert into test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('63.10','Structure - ValueTiers Table', 'Dimension', 'ValueTiers')

Insert into test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('63.11','Structure - LifeStage Table', 'Dimension', 'LifeStage')

Insert into test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('63.12','Structure - Structure Table', 'Dimension', 'Structure')



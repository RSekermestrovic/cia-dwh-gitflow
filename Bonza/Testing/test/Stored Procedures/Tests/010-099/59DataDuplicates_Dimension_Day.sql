﻿--Test Code
--exec Test.[59DataDuplicates_Dimension_Day] 'ADHOC'

Create Procedure Test.[59DataDuplicates_Dimension_Day] @TestRunNo varchar(23)

as

If not exists(
		Select		Count(*)
		From		[$(Dimensional)].Dimension.[Day] as [Day]
		Group by	DayDate
		Having		Count(*) > 1
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '59', 'DataDuplicates_Dimension_Day', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '59', 'DataDuplicates_Dimension_Day', 'Fail', 'Duplicates of DayDate found')
End

If not exists(
		Select		Count(*)
		From		[$(Dimensional)].Dimension.[DayZone] as [DayZone]
		Group by	MasterDayKey, AnalysisDayKey
		Having		Count(*) > 1
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '59.2', 'DataDuplicates_Dimension_DayZone', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '59.2', 'DataDuplicates_Dimension_DayZone', 'Fail', 'Duplicates of MasterDayKey/AnalysisDayKey found')
End


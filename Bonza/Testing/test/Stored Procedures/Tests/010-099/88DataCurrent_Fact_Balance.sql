﻿--Test Code
--exec Test.[88DataCurrent_Fact_Balance] 'ADHOC'
Create Procedure Test.[88DataCurrent_Fact_Balance] @TestRunNo varchar(23)

as
/*

Declare @LastBatch int
Set @LastBatch = (Select Max([BatchKey]) From [$(Staging)].[Control].[ETLController] where BatchStatus = 1)

Select	*
into	#BatchDetails
From	[$(Staging)].[Control].[ETLController]
Where	[BatchKey] = @LastBatch

If	(
	Select	BalanceFactRowsProcessed
	From	#BatchDetails
	) >0

Begin
	Declare @Counter int
	Set @Counter =  (
					Select		count(*) as NumberofRecords
					From		[$(Dimensional)].[Fact].[Balance] as [Balance] with (nolock)
					where		[Balance].DayKey in	(
													Select		[DayZone].DayKey
													From		[$(Dimensional)].Dimension.[Day] as [Day] with (nolock)
																inner join [$(Dimensional)].Dimension.[DayZone] as [DayZone] with (nolock) on [Day].DayKey = DayZone.MasterDayKey
													Where		[Day].DayDate > (select BatchFromDate from #BatchDetails)
																and [Day].DayKey not in (0,-1)
													)
					)
	If @Counter > 0
	Begin
		Insert into test.tblResults Values (@TestRunNo, '88', 'DataCurrent_Fact_Balance Table', 'Pass', Null)
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '88', 'DataCurrent_Fact_Balance Table', 'Fail', 'The batch history differs from the records in the table.')
	End
	

End

Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '88', 'DataCurrent_Fact_Balance Table', 'Fail', 'The batch history says no records were found in the source systems')
	End

*/

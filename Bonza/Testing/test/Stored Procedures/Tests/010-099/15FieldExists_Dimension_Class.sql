--Test Code
--exec Test.[15FieldExists_Dimension_Class] 'ADHOC'
Create Procedure Test.[15FieldExists_Dimension_Class] @TestRunNo varchar(23)

as

Declare @Database varchar(100)
Set @Database = '[$(Dimensional)]'

Declare @SQL varchar(max)
Set @SQL = 'select top 1
[ClassKey]
      ,[ClassId]
      ,[Source]
      ,[SourceClassName]
      ,[ClassCode]
      ,[ClassName]
      ,[ClassIcon]
      ,[SuperclassKey]
      ,[SuperclassName]
      ,[FromDate]
      ,[ToDate]
      ,[CreatedDate]
      ,[CreatedBatchKey]
      ,[CreatedBy]
      ,[ModifiedDate]
      ,[ModifiedBatchKey]
      ,[ModifiedBy]
  FROM ' + @Database + '.[Dimension].[Class]'

Exec Test.spSelectValid @TestRunNo, '15','Structure - Dimension Class Table', @SQL

If		(
		Select		Count(*)
		From		[$(Dimensional)].sys.all_objects as [Objects]
					inner join [$(Dimensional)].sys.all_columns as [Columns] on [Objects].[object_id] = [Columns].[object_id]
					inner join [$(Dimensional)].sys.schemas as [schemas] on [Objects].[schema_id] = [schemas].[schema_id]
		where		[Objects].name = 'Class'
					and [Objects].[type] = 'U'
					and [schemas].Name = 'Dimension'
		) = 17
Begin
	Insert into test.tblResults Values (@TestRunNo, '15.1', 'Structure - Dimension Class Table', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '15.1', 'Structure - Dimension Class Table', 'Fail', 'Wrong number of columns')
End

--Test Code
--exec Test.[67TableExists_Star_KPI] 'ADHOC'
Create Procedure Test.[67TableExists_Star_KPI] @TestRunNo varchar(23)

as

Declare @Database varchar(100)
Set @Database = '[$(Dimensional)]'

Insert into test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('67.0','Structure - KPI Table', 'Fact', 'KPI')

Insert into test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('67.1','Structure - Contract Table', 'Dimension', 'Contract')

Insert into test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('67.2','Structure - Day Table', 'Dimension', 'Day')

Insert into test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('67.3','Structure - Account Table', 'Dimension', 'Account')

Insert into test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('67.4','Structure - AccountStatus Table', 'Dimension', 'AccountStatus')

Insert into test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('67.5','Structure - BetType Table', 'Dimension', 'BetType')

Insert into test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('67.6','Structure - LegType Table', 'Dimension', 'LegType')

Insert into test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('67.7','Structure - Channel Table', 'Dimension', 'Channel')

Insert into test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('67.8','Structure - Wallet Table', 'Dimension', 'Campaign')

Insert into test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('67.9','Structure - Market Table', 'Dimension', 'Market')

Insert into test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('67.10','Structure - Wallet Table', 'Dimension', 'Wallet')

Insert into test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('67.11','Structure - Class Table', 'Dimension', 'Class')

Insert into test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('67.12','Structure - User Table', 'Dimension', 'User')

Insert into test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('67.13','Structure - Instrument Table', 'Dimension', 'Instrument')

Insert into test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('67.14','Structure - InPlay Table', 'Dimension', 'InPlay')

Insert into test.tblResults
Select @TestRunNo, * from Test.fnTableExists ('67.15','Structure - Structure Table', 'Dimension', 'Structure')









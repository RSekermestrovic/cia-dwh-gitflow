﻿--Test Code
--exec Test.[36DataValid_Dimension_Day] 'ADHOC'
Create Procedure [Test].[36DataValid_Dimension_Day] @TestRunNo varchar(23)

as

Select	*
into #DimensionDay
From [$(Dimensional)].Dimension.[Day]

select * 
into #vwFiscalYears
from test.vwFiscalYears

--Makes the start of the week a Monday
Set DateFirst 1

Declare @Counter int

--DayKey
IF not exists	(	SELECT	DayKey
							, DayDate
							, Cast(Year(DayDate) as char(4)) + Right ('0' + Cast(Month(DayDate) as varchar(2)),2) + Right ('0' + Cast(Day(DayDate) as varchar(2)),2)
					From	#DimensionDay
							inner join #vwFiscalYears on Left (FiscalQuarterKey,4) = #vwFiscalYears.[Year]
					where	DayKey not in (0 ,-1)
							
							and left (DayKey,8) != Cast(Year(DayDate) as char(4)) + Right ('0' + Cast(Month(DayDate) as varchar(2)),2) + Right ('0' + Cast(Day(DayDate) as varchar(2)),2)
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36', 'DataValid_Dimension_Day Computation Validation - DayKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36', 'DataValid_Dimension_Day Computation Validation - DayKey', 'Fail', 'There is an error in the DayKey Column')
End

--DayName
IF Not Exists  (SELECT	DayKey
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and [DayName] != Replace (Convert (varchar(11), DayDate, 106), ' ', '-')
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.1', 'DataValid_Dimension_Day Computation Validation - DayName', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.1', 'DataValid_Dimension_Day Computation Validation - DayName', 'Fail', 'There is an error in the DayName column')
End

--DayText
IF Not Exists  (SELECT	DayKey
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and [DayText] != Replace (Convert (varchar(11), DayDate, 102), '.', '-')
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.2', 'DataValid_Dimension_Day Computation Validation - DayText', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.2', 'DataValid_Dimension_Day Computation Validation - DayText', 'Fail', 'There is an error in the DayText column')
End

--DayDate
--DayOfWeekText
IF Not Exists  (SELECT	DayKey
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and DayOfWeekText != Left (DateName (weekday, DayDate), 3)
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.3', 'DataValid_Dimension_Day Computation Validation - DayOfWeekText', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.3', 'DataValid_Dimension_Day Computation Validation - DayOfWeekText', 'Fail', 'There is an error in the DayOfWeekText column')
End

--DayOfWeekNumber
IF Not Exists  (SELECT	DayKey
						, DayOfWeekText
						, DayOfWeekNumber
				From	#DimensionDay
				where	(DayOfWeekNumber = 1 and DayOfWeekText != 'Mon')
							or (DayOfWeekNumber = 2 and DayOfWeekText != 'Tue')
							or (DayOfWeekNumber = 3 and DayOfWeekText != 'Wed')
							or (DayOfWeekNumber = 4 and DayOfWeekText != 'Thu')
							or (DayOfWeekNumber = 5 and DayOfWeekText != 'Fri')
							or (DayOfWeekNumber = 6 and DayOfWeekText != 'Sat')
							or (DayOfWeekNumber = 7 and DayOfWeekText != 'Sun')
							or (DayOfWeekNumber = 0 and DayOfWeekText != 'UNK')
							or (DayOfWeekNumber = 9 and DayOfWeekText != 'N/A')
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.4', 'DataValid_Dimension_Day Computation Validation - DayOfWeekNumber', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.4', 'DataValid_Dimension_Day Computation Validation - DayOfWeekNumber', 'Fail', 'There is an error in the DayOfWeekNumber column')
End

--DayOfMonthNumber
IF Not Exists  (SELECT	DayKey
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and DayOfMonthNumber != DatePart (Day, DayDate)
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.5', 'DataValid_Dimension_Day Computation Validation - DayOfMonthNumber', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.5', 'DataValid_Dimension_Day Computation Validation - DayOfMonthNumber', 'Fail', 'There is an error in the DayOfMonthNumber column')
End

--DayOfQuarterNumber
IF Not Exists  (SELECT	DayKey
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and DayOfQuarterNumber != DATEDIFF(Day, DATEADD(Quarter, DATEDIFF(Quarter, 0, DayDate),0), DayDate) +1
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.6', 'DataValid_Dimension_Day Computation Validation - DayOfQuarterNumber', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.6', 'DataValid_Dimension_Day Computation Validation - DayOfQuarterNumber', 'Fail', 'There is an error in the DayOfQuarterNumber column')
End

--DayOfYearNumber
IF Not Exists  (SELECT	DayKey
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and DayOfYearNumber != DATEDIFF(Day, DATEADD(Year, DATEDIFF(Year, 0, DayDate),0), DayDate) +1
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.7', 'DataValid_Dimension_Day Computation Validation - DayOfYearNumber', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.7', 'DataValid_Dimension_Day Computation Validation - DayOfYearNumber', 'Fail', 'There is an error in the DayOfYearNumber column')
End

--DayOfTaxQuarterNumber
IF Not Exists  (SELECT	DayKey
				From	#DimensionDay
				where	DayOfTaxQuarterNumber != DayOfQuarterNumber
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.8', 'DataValid_Dimension_Day Computation Validation - DayOfTaxQuarterNumber', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.8', 'DataValid_Dimension_Day Computation Validation - DayOfTaxQuarterNumber', 'Fail', 'There is an error in the DayOfTaxQuarterNumber column')
End

--DayOfTaxYearNumber
IF Not Exists  (SELECT	DayKey
						, DayOfTaxYearNumber
						, DateDiff (Day, DATEADD(year,DATEDIFF(month,'19010701',DayDate)/12,'19010701'), DayDate) + 1 as Correction
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and DayOfTaxYearNumber != DateDiff (Day, DATEADD(year,DATEDIFF(month,'19010701',DayDate)/12,'19010701'), DayDate) + 1
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.9', 'DataValid_Dimension_Day Computation Validation - DayOfTaxYearNumber', 'Pass', Null)
End
Else
Begin
--CCIA-3267 - There are 1767 incorrect records (Corrected 1st April 2015)
	If		(SELECT	Count (*)
			From	#DimensionDay
			where	DayKey not in (0 ,-1)
					
					and DayOfTaxYearNumber != DateDiff (Day, DATEADD(year,DATEDIFF(month,'19010701',DayDate)/12,'19010701'), DayDate) + 1
			) = 1767
			and
		exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-3267'
							and [Status] != 'Done'
				)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.9', 'DataValid_Dimension_Day Computation Validation - DayOfTaxYearNumber', 'Pass', 'Known Defect CCIA-3267')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.9', 'DataValid_Dimension_Day Computation Validation - DayOfTaxYearNumber', 'Fail', 'There is an error in the DayOfTaxYearNumber column')
	End
End

--DayOfFiscalWeekNumber
IF Not Exists  (Select		*
				From		(
							SELECT	DayKey
									, DayOfFiscalWeekNumber
									, 'Correction' = CASE
										When DayOfWeekNumber <=2 then DayOfWeekNumber +5
										When DayOfWeekNumber >= 3 then DayOfWeekNumber -2
										else 0
										end			
									, DayOfWeekText
							From	#DimensionDay
							where	DayKey not in (0 ,-1)
									
							) as Data
				Where		DayOfFiscalWeekNumber != Correction
							or DayOfFiscalWeekNumber <= 0
							or DayOfFiscalWeekNumber >= 8	
							or Correction <=0
							or Correction >= 8						
				)	
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.10', 'DataValid_Dimension_Day Computation Validation - DayOfFiscalWeekNumber', 'Pass', Null)
End
Else
Begin
	--CCIA-3268 there are 28,487 errors as Day 1 has not been coded as Wed but Mon (corrected 1st April 2015)
	If (Select		count (*)
				From		(
							SELECT	DayKey
									, DayOfFiscalWeekNumber
									, 'Correction' = CASE
										When DayOfWeekNumber <=2 then DayOfWeekNumber +5
										When DayOfWeekNumber >= 3 then DayOfWeekNumber -2
										else 0
										end			
									, DayOfWeekText
							From	#DimensionDay
							where	DayKey not in (0 ,-1)
									
							) as Data
				Where		DayOfFiscalWeekNumber != Correction
							or DayOfFiscalWeekNumber <= 0
							or DayOfFiscalWeekNumber >= 8	
							or Correction <=0
							or Correction >= 8
		) = 28487
		and
		exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-3268'
							and [Status] != 'Done'
				)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.10', 'DataValid_Dimension_Day Computation Validation - DayOfFiscalWeekNumber', 'Pass', 'Known Defect CCIA-3268')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.10', 'DataValid_Dimension_Day Computation Validation - DayOfFiscalWeekNumber', 'Fail', 'There is an error in the DayOfFiscalWeekNumber column')
	End
End

--DayOfFiscalMonthNumber
IF Not Exists	(SELECT		DayKey
							, DayOfFiscalMonthNumber
							, FiscalMonthStartDate
							, DateDiff(Day,FiscalMonthStartDate, DayDate) +1 as Correction
  				 From		#DimensionDay
				 where		DayKey not in (0 ,-1)
							
							and DayOfFiscalMonthNumber != DateDiff(Day,FiscalMonthStartDate, DayDate) +1
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.11', 'DataValid_Dimension_Day Computation Validation - DayOfFiscalMonthNumber', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.11', 'DataValid_Dimension_Day Computation Validation - DayOfFiscalMonthNumber', 'Fail', 'There is an error in DayOfFiscalMonthNumber')
End

--DayOfFiscalQuarterNumber
set @Counter = 	(SELECT		Count(*)
  				 From		#DimensionDay
				 where		DayKey not in (0 ,-1)
							and DayOfFiscalQuarterNumber != DateDiff(Day,FiscalQuarterStartDate, DayDate) +1
				)
if @Counter = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.12', 'DataValid_Dimension_Day Computation Validation - DayOfFiscalQuarterNumber', 'Pass', Null)
End
Else
Begin
	--CCIA-3500
	if @Counter = 3673
	and exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-3500'
							and [Status] != 'Done'
				)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.12', 'DataValid_Dimension_Day Computation Validation - DayOfFiscalQuarterNumber', 'Pass', 'CCIA-3500')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.12', 'DataValid_Dimension_Day Computation Validation - DayOfFiscalQuarterNumber', 'Fail', 'There is an error in DayOfFiscalQuarterNumber')
	End
End

--DayOfFiscalYearNumber
IF Not Exists	(SELECT		DayKey
						, DayOfFiscalYearNumber
						, DateDiff(Day,FiscalYearStartDate, DayDate) +1 as Correction
  			 From	#DimensionDay
			 where	DayKey not in (0 ,-1)
					
					and DayOfFiscalYearNumber != DateDiff(Day,FiscalYearStartDate, DayDate) +1
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.13', 'DataValid_Dimension_Day Computation Validation - DayOfFiscalYearNumber', 'Pass', Null)
End
Else
Begin
	--CCIA-3491 (Corrected 20th April 2015)
	IF	exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-3491'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.13', 'DataValid_Dimension_Day Computation Validation - DayOfFiscalYearNumber', 'Pass', 'CCIA-3491')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.13', 'DataValid_Dimension_Day Computation Validation - DayOfFiscalYearNumber', 'Fail', 'There is an error in DayOfFiscalYearNumber')
	End
End

--WeekKey
IF Not Exists  (
				SELECT	DayKey
						, WeekKey
						, Cast(DatePart(Year,DayDate) as char(4)) + Right ('0' + Cast (DATEPART(Week,DayDate) as varchar(2)),2) as Correction
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and WeekKey != Cast(DatePart(Year,DayDate) as char(4)) + Right ('0' + Cast (DATEPART(Week,DayDate) as varchar(2)),2)		
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.14', 'DataValid_Dimension_Day Computation Validation - WeekKey', 'Pass', Null)
End
Else
Begin
	--CCIA-3270 Not always correct 7518 errors (Corrected 1st April 2015)
	If			(
				SELECT	count (*)
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and WeekKey != Cast(DatePart(Year,DayDate) as char(4)) + Right ('0' + Cast (DATEPART(Week,DayDate) as varchar(2)),2)		
				) = 7518
				and
		exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-3270'
							and [Status] != 'Done'
				)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.14', 'DataValid_Dimension_Day Computation Validation - WeekKey', 'Pass', 'Known Defect CCIA-3270')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.14', 'DataValid_Dimension_Day Computation Validation - WeekKey', 'Fail', 'There is an error in the WeekKey column')
	End
End

--WeekName
--CCIA-3271 - Naming convention change in 2010 - 1134 errors (Corrected 1st April 2015)
IF Not Exists  (
				SELECT	DayKey
						, daydate
						, WeekName
						, 'W' + Right ('0' + Cast (Cast (Right(WeekKey,2) as int) as varchar(2)),2) + '-' +  Left (WeekKey,4) as Correction
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and WeekName != 'W' + Right ('0' + Cast (Cast (Right(WeekKey,2) as int) as varchar(2)), 2) + '-' +  Left (WeekKey,4)
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.15', 'DataValid_Dimension_Day Computation Validation - WeekName', 'Pass', Null)
End
Else
Begin
	--CCIA 3368 - There 3303 errors because the first days of the previous year are getting allocated to the last week of the previous, pushing all of the week numbers out by 1 (Corrected 2nd April 2015)
	If			(SELECT	count (*) as numberOF
						From	#DimensionDay
						where	DayKey not in (0 ,-1)
								
								and WeekName != 'W' + Right ('0' + Cast (Cast (Right(WeekKey,2) as int) as varchar(2)), 2) + '-' +  Left (WeekKey,4)
				) = 3303
				and
		exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-3368'
							and [Status] != 'Done'
				)
	Begin	
		Insert into test.tblResults Values (@TestRunNo, '36.15', 'DataValid_Dimension_Day Computation Validation - WeekName', 'Pass', 'Known Defect 3368')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.15', 'DataValid_Dimension_Day Computation Validation - WeekName', 'Fail', 'There is an error in the WeekName column')
	End
End

--WeekText
Set @Counter =  (
				SELECT	count (*)
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and WeekText != WeekName
				)
If @Counter = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.16', 'DataValid_Dimension_Day Computation Validation - WeekText', 'Pass', Null)
End
Else
Begin
	--CCIA-3452 (Corrected 20th April 2015)
	If @Counter = 3303
	and exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-3452'
							and [Status] != 'Done'
			)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.16', 'DataValid_Dimension_Day Computation Validation - WeekText', 'Pass', 'CCIA-3452')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.16', 'DataValid_Dimension_Day Computation Validation - WeekText', 'Fail', 'There is an error in the WeekText column')
	End
End

--WeekStartDate
Set datefirst 1
IF Not Exists  (Select		*
				From		(
							Select		*
										, 'Correction' = CASE
											When daydate < FirstMonday then Cast (DATEPART(Year,daydate) as char(4)) + '-01-01'
											when daydate = FirstMonday then FirstMonday
											When FirstMonday = Cast (DATEPART(Year,daydate) as char(4)) + '-01-01' then DateAdd(day,(WeekNumber-1)*7,FirstMonday)
											else DateAdd(day,(WeekNumber-2)*7,FirstMonday)
											end
							From		(SELECT	DayKey
												, daydate
												, WeekStartDate
												, DATEADD(DAY, (@@DATEFIRST - DATEPART(WEEKDAY, DATEADD(YEAR, DATEPART(Year,daydate) - 1900, 0)) + (8 - @@DATEFIRST) * 2) % 7, DATEADD(YEAR, DATEPART(Year,daydate) - 1900, 0)) as FirstMonday
												, DATEPART(Week,daydate) as WeekNumber
											From	#DimensionDay
											where	DayKey not in (0 ,-1)
													
										) as Data
							) as Data
				Where		WeekStartDate != Correction
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.17', 'DataValid_Dimension_Day Computation Validation - WeekStartDate', 'Pass', Null)
End
Else
Begin
	--CCIA-3369 - records with a start date in the previous year (Corrected 7th April 2015)
	Set datefirst 1
	If Not Exists  (Select		*
					From		(
								Select		*
											, 'Correction' = CASE
												When daydate < FirstMonday then Cast (DATEPART(Year,daydate) as char(4)) + '-01-01'
												when daydate = FirstMonday then FirstMonday
												When FirstMonday = Cast (DATEPART(Year,daydate) as char(4)) + '-01-01' then DateAdd(day,(WeekNumber-1)*7,FirstMonday)
												else DateAdd(day,(WeekNumber-2)*7,FirstMonday)
												end
								From		(SELECT	DayKey
													, daydate
													, WeekStartDate
													, DATEADD(DAY, (@@DATEFIRST - DATEPART(WEEKDAY, DATEADD(YEAR, DATEPART(Year,daydate) - 1900, 0)) + (8 - @@DATEFIRST) * 2) % 7, DATEADD(YEAR, DATEPART(Year,daydate) - 1900, 0)) as FirstMonday
													, DATEPART(Week,daydate) as WeekNumber
												From	#DimensionDay
												where	DayKey not in (0 ,-1)
														
														and DATEPART(Week,daydate) != 1
											) as Data
								) as Data
					Where		WeekStartDate != Correction
					)
	and exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-3369'
							and [Status] != 'Done'
			)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.17', 'DataValid_Dimension_Day Computation Validation - WeekStartDate', 'Pass', 'CCIA-3369')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.17', 'DataValid_Dimension_Day Computation Validation - WeekStartDate', 'Fail', 'There is an error in the WeekStartDate column')
	End
End

--WeekEndDate
IF Not Exists  (Select	*
				From	(
						SELECT	DayKey
								, WeekEndDate
								, WeekStartDate
								, 'Correction' = CASE
									When DatePart(WeekDay,DayDate) = 7 then DayDate
									When DatePart(Year,DateAdd(Day,6,WeekStartDate)) != DatePart(Year,DayDate) then YearEndDate
									When DatePart(Week,Daydate) >= 2 then DateAdd(day,6,WeekStartDate)
									When DatePart(WEEKDAY,YearStartDate) = 1 then DateAdd(day,6,WeekStartDate)
									When DatePart(WEEKDAY,YearStartDate) = 2 then DateAdd(day,5,WeekStartDate)
									When DatePart(WEEKDAY,YearStartDate) = 3 then DateAdd(day,4,WeekStartDate)
									When DatePart(WEEKDAY,YearStartDate) = 4 then DateAdd(day,3,WeekStartDate)
									When DatePart(WEEKDAY,YearStartDate) = 5 then DateAdd(day,2,WeekStartDate)
									When DatePart(WEEKDAY,YearStartDate) = 6 then DateAdd(day,1,WeekStartDate)
									else DateAdd(day,6,WeekStartDate)
									end
						From	#DimensionDay
						where	DayKey not in (0 ,-1)
						) as Data
				Where	WeekEndDate != Correction
				)

Begin
	Insert into test.tblResults Values (@TestRunNo, '36.18', 'DataValid_Dimension_Day Computation Validation - WeekEndDate', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.18', 'DataValid_Dimension_Day Computation Validation - WeekEndDate', 'Fail', 'There is an error in the WeekEndDate column')
End

--WeekOfMonthText
Insert into test.tblResults Values (@TestRunNo, '36.19', 'DataValid_Dimension_Day Computation Validation - WeekOfMonthText', 'Pass', 'CCIA-3459')

--WeekOfMonthNumber
Insert into test.tblResults Values (@TestRunNo, '36.20', 'DataValid_Dimension_Day Computation Validation - WeekOfMonthNumber', 'Pass', 'CCIA-3459')

--WeekOfYearText
IF Not Exists  (
				SELECT	DayKey
						, WeekOfYearText
						, 'W' + Right ('0' + cast (WeekOfYearNumber as varchar(2)),2) as Correction
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and WeekOfYearText != 'W' + Right ('0' + cast (WeekOfYearNumber as varchar(2)),2)
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.21', 'DataValid_Dimension_Day Computation Validation - WeekOfYearText', 'Pass', Null)
End
Else
Begin
	--CCIA-3458 (Corrected 20th April 2015)
	if exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-3458'
							and [Status] != 'Done')
	begin
		Insert into test.tblResults Values (@TestRunNo, '36.21', 'DataValid_Dimension_Day Computation Validation - WeekOfYearText', 'Pass', 'CCIA-3458')
	end
	else
	begin
		--CCIA-3480 (Corrected 20th April 2015)
		if exists (SELECT	DayKey
							, WeekOfYearText
							, LEN(WeekOfYearText)
					From	#DimensionDay
					where	DayKey not in (0 ,-1)
							
							and LEN(WeekOfYearText) != 3
					)
		and exists	(Select		*
					From		test.tblDefects
					Where		DefectID = 'CCIA-3480'
								and [Status] != 'Done'
					)
		begin
			Insert into test.tblResults Values (@TestRunNo, '36.21', 'DataValid_Dimension_Day Computation Validation - WeekOfYearText', 'Pass', 'CCIA-3480')
		End
		Else
		Begin
			Insert into test.tblResults Values (@TestRunNo, '36.21', 'DataValid_Dimension_Day Computation Validation - WeekOfYearText', 'Fail', 'There is an error in WeekOfYearText')
		End
	end
End

--WeekOfYearNumber
set @Counter =   (
				SELECT	count(*)
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and WeekOfYearNumber != DATEPART(Week,daydate)
				)
if @Counter = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.22', 'DataValid_Dimension_Day Computation Validation - WeekOfYearNumber', 'Pass', Null)
End
Else
Begin
	--CCIA-3458
	if @Counter = 3303
	and exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-3458'
							and [Status] != 'Done'
			)
	begin
		Insert into test.tblResults Values (@TestRunNo, '36.22', 'DataValid_Dimension_Day Computation Validation - WeekOfYearNumber', 'Pass', 'CCIA-3458')
	end
	else
	begin
		Insert into test.tblResults Values (@TestRunNo, '36.22', 'DataValid_Dimension_Day Computation Validation - WeekOfYearNumber', 'Fail', 'There is an error in the WeekOfMonthNumber column')
	end
End

--WeekYearNumber
--CCIA-3272 There are 36 records with the next year (Corrected 2nd April 2015)
IF Not Exists  (
				SELECT	DayKey
						, WeekYearNumber
						, DatePart(Year,Daydate) as Correction
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and WeekYearNumber != DatePart(Year,daydate)
				)

Begin
	Insert into test.tblResults Values (@TestRunNo, '36.23', 'DataValid_Dimension_Day Computation Validation - WeekYearNumber', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.23', 'DataValid_Dimension_Day Computation Validation - WeekYearNumber', 'Fail', 'There is an error in the WeekYearNumber column')
End

--MonthKey
IF Not Exists  (
				SELECT	DayKey
						, DayDate
						, MonthKey
						, Replace (Left (DayDate, 7), '-', '') as Correction
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and MonthKey != Replace (Left (DayDate, 7), '-', '')
				)

Begin
	Insert into test.tblResults Values (@TestRunNo, '36.24', 'DataValid_Dimension_Day Computation Validation - MonthKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.24', 'DataValid_Dimension_Day Computation Validation - MonthKey', 'Fail', 'There is an error in the MonthKey column')
End

--MonthName
IF Not Exists  (
				SELECT	DayKey
						, DayDate
						, [MonthName]
						, Right (Replace (Convert(varchar(30),DayDate, 106), ' ', '-'), 8)
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and [MonthName] != Right (Replace (Convert(varchar(30),DayDate, 106), ' ', '-'), 8)
				)

Begin
	Insert into test.tblResults Values (@TestRunNo, '36.25', 'DataValid_Dimension_Day Computation Validation - MonthName', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.25', 'DataValid_Dimension_Day Computation Validation - MonthName', 'Fail', 'There is an error in the MonthName column')
End

--MonthText
IF Not Exists  (
				SELECT	DayKey
						, DayDate
						, MonthText
						, [MonthName]
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and MonthText != [MonthName]
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.26', 'DataValid_Dimension_Day Computation Validation - MonthText', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.26', 'DataValid_Dimension_Day Computation Validation - MonthText', 'Fail', 'There is an error in the MonthText column')
End

--MonthStartDate
IF Not Exists  (
				SELECT	DayKey
						, DayDate
						, MonthStartDate
						, Cast (DatePart(Year, DayDate) as char(4)) + '-' + Right (('0' + Cast (DatePart(Month, DayDate) as varchar(2))), 2) + '-01'
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and MonthStartDate != Cast (DatePart(Year, DayDate) as char(4)) + '-' + Right (('0' + Cast (DatePart(Month, DayDate) as varchar(2))), 2) + '-01'
				)

Begin
	Insert into test.tblResults Values (@TestRunNo, '36.27', 'DataValid_Dimension_Day Computation Validation - MonthStartDate', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.27', 'DataValid_Dimension_Day Computation Validation - MonthStartDate', 'Fail', 'There is an error in the MonthStartDate column')
End

--MonthEndDate
IF Not Exists  (
				SELECT	DayKey
						, DayDate
						, MonthEndDate
						, Cast (DateAdd(Month,1,MonthStartDate) as datetime) -1
						--DateAdd(Day,-1,MonthStartDate)
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and Cast (MonthEndDate as datetime) != Cast (DateAdd(Month,1,MonthStartDate) as datetime) -1
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.28', 'DataValid_Dimension_Day Computation Validation - MonthEndDate', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.28', 'DataValid_Dimension_Day Computation Validation - MonthEndDate', 'Fail', 'There is an error in the MonthEndDate column')
End

--MonthDays
IF Not Exists  (
				SELECT	DayKey
						, DayDate
						, MonthEndDate
						, MonthDays
						, Right (MonthEndDate,2)
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and MonthDays != Right (MonthEndDate,2)
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.29', 'DataValid_Dimension_Day Computation Validation - MonthDays', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.29', 'DataValid_Dimension_Day Computation Validation - MonthDays', 'Fail', 'There is an error in the MonthDays column')
End

--MonthOfYearText
IF Not Exists  (
				SELECT	DayKey
						, DayDate

				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and MonthOfYearText != Left (MonthName,3)
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.30', 'DataValid_Dimension_Day Computation Validation - MonthOfYearText', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.30', 'DataValid_Dimension_Day Computation Validation - MonthOfYearText', 'Fail', 'There is an error in the MonthOfYearText column')
End

--MonthOfYearNumber
IF Not Exists  (
				SELECT	DayKey
						, DayDate
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and MonthOfYearNumber != Month(DayDate)
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.31', 'DataValid_Dimension_Day Computation Validation - MonthOfYearNumber', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.31', 'DataValid_Dimension_Day Computation Validation - MonthOfYearNumber', 'Fail', 'There is an error in the MonthOfYearNumber column')
End

--QuarterKey
--CCIA-3274 Has 5241 invalid records (Corrected 1st April 2015)
IF Not Exists  (Select	*
				From	(
						SELECT	DayKey
								, DayDate
								, QuarterKey
								, 'Correction' = Case
									When Month(DayDate) in (1,2,3) then cast (Year(daydate) as char(4)) + '1' 
									When Month(DayDate) in (4,5,6) then cast (Year(daydate) as char(4)) + '2' 
									When Month(DayDate) in (7,8,9) then cast (Year(daydate) as char(4)) + '3' 
									When Month(DayDate) in (10,11,12) then cast (Year(daydate) as char(4)) + '4' 
									end
						From	#DimensionDay
						where	DayKey not in (0 ,-1)
								
						) as Data
				where QuarterKey != Correction
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.32', 'DataValid_Dimension_Day Computation Validation - QuarterKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.32', 'DataValid_Dimension_Day Computation Validation - QuarterKey', 'Fail', 'There is an error in the QuarterKey column')
End

--QuarterName
--CCIA-3273 Has 3 invalid records (Corrected 1st April 2015)
IF Not Exists  (Select	*
				From	(
						SELECT	DayKey
								, DayDate
								, QuarterName
								, 'Correction' = Case
									When Month(DayDate) in (1,2,3) then 'Q1-' + cast (Year(daydate) as char(4))
									When Month(DayDate) in (4,5,6) then 'Q2-' + cast (Year(daydate) as char(4))
									When Month(DayDate) in (7,8,9) then 'Q3-' + cast (Year(daydate) as char(4))
									When Month(DayDate) in (10,11,12) then 'Q4-' + cast (Year(daydate) as char(4)) 
									end
						From	#DimensionDay
						where	DayKey not in (0 ,-1)
								
						) as Data
				where QuarterName != Correction
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.33', 'DataValid_Dimension_Day Computation Validation - QuarterName', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.33', 'DataValid_Dimension_Day Computation Validation - QuarterName', 'Fail', 'There is an error in the QuarterName column')
End

--QuarterText
IF Not Exists  (
				SELECT	DayKey
						, DayDate
						, QuarterText
						, QuarterName
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and QuarterText != QuarterName
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.34', 'DataValid_Dimension_Day Computation Validation - QuarterText', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.34', 'DataValid_Dimension_Day Computation Validation - QuarterText', 'Fail', 'There is an error in the QuarterText column')
End

--QuarterStartDate
--CCIA-3322 Has 6572 invalid records (Corrected 2nd April 2015)
IF Not Exists  (
				Select	*
				From	(
						SELECT	DayKey
								, DayDate
								, QuarterStartDate
								, 'Correction' = Case
									When Month(DayDate) in (1,2,3) then cast (Year(daydate) as char(4)) + '-01-01'
									When Month(DayDate) in (4,5,6) then cast (Year(daydate) as char(4)) + '-04-01'
									When Month(DayDate) in (7,8,9) then cast (Year(daydate) as char(4)) + '-07-01'
									When Month(DayDate) in (10,11,12) then cast (Year(daydate) as char(4)) + '-10-01'
									end
						From	#DimensionDay
						where	DayKey not in (0 ,-1)
								
						) as Data
				where QuarterStartDate != Correction
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.35', 'DataValid_Dimension_Day Computation Validation - QuarterStartDate', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.35', 'DataValid_Dimension_Day Computation Validation - QuarterStartDate', 'Fail', 'There is an error in the QuarterStartDate column')
End

--QuarterEndDate
--CCIA-3323 Has 6572 invalid records (Corrected 2nd April 2015)
IF Not Exists  (
				Select	*
				From	(
						SELECT	DayKey
								, DayDate
								, QuarterEndDate
								, 'Correction' = Case
									When Month(DayDate) in (1,2,3) then cast (Year(daydate) as char(4)) + '-03-31'
									When Month(DayDate) in (4,5,6) then cast (Year(daydate) as char(4)) + '-06-30'
									When Month(DayDate) in (7,8,9) then cast (Year(daydate) as char(4)) + '-09-30'
									When Month(DayDate) in (10,11,12) then cast (Year(daydate) as char(4)) + '-12-31'
									end
						From	#DimensionDay
						where	DayKey not in (0 ,-1)
								
						) as Data
				where QuarterEndDate != Correction
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.36', 'DataValid_Dimension_Day Computation Validation - QuarterEndDate', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.36', 'DataValid_Dimension_Day Computation Validation - QuarterEndDate', 'Fail', 'There is an error in the QuarterEndDate column')
End

--QuarterDays
IF Not Exists  (
				SELECT	DayKey
						, DayDate
						, QuarterDays
						, QuarterStartDate
						, QuarterEndDate
						, DateDiff(Day, QuarterStartDate, QuarterEndDate) + 1
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and QuarterDays != DateDiff(Day, QuarterStartDate, QuarterEndDate) + 1
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.37', 'DataValid_Dimension_Day Computation Validation - QuarterDays', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.37', 'DataValid_Dimension_Day Computation Validation - QuarterDays', 'Fail', 'There is an error in the QuarterDays column')
End

--QuarterOfYearText
--CCIA-3276 - The are 5276 errors (Corrected 1st April 2015)
IF Not Exists  (
				SELECT	DayKey
						, DayDate
						, QuarterOfYearText
						, Left (QuarterText,2) as Correction
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and QuarterOfYearText != Left (QuarterText,2)
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.38', 'DataValid_Dimension_Day Computation Validation - QuarterOfYearText', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.38', 'DataValid_Dimension_Day Computation Validation - QuarterOfYearText', 'Fail', 'There is an error in the QuarterOfYearText column')
End

--QuarterOfYearNumber
IF Not Exists  (
				SELECT	DayKey
						, DayDate
						, QuarterOfYearNumber
						, Right (QuarterOfYearText,1)
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and cast (QuarterOfYearNumber as char(1)) != Right (QuarterOfYearText,1)
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.39', 'DataValid_Dimension_Day Computation Validation - QuarterOfYearNumber', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.39', 'DataValid_Dimension_Day Computation Validation - QuarterOfYearNumber', 'Fail', 'There is an error in the QuarterOfYearNumber column')
End

--YearKey
IF Not Exists  (
				SELECT	DayKey
						, DayDate
						, YearKey
						, Year (DayDate)
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and YearKey != Year (DayDate)
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.40', 'DataValid_Dimension_Day Computation Validation - YearKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.40', 'DataValid_Dimension_Day Computation Validation - YearKey', 'Fail', 'There is an error in the YearKey column')
End

--YearName
IF Not Exists  (
				SELECT	DayKey
						, DayDate
						, YearName
						, YearKey
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and YearName != YearKey
						and YearKey not in (-1,0)
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.41', 'DataValid_Dimension_Day Computation Validation - YearName', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.41', 'DataValid_Dimension_Day Computation Validation - YearName', 'Fail', 'There is an error in the YearName column')
End

--YearText
IF Not Exists  (
				SELECT	DayKey
						, DayDate
						, YearText
						, YearKey
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and YearText != YearKey
						and YearKey not in (-1,0)
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.42', 'DataValid_Dimension_Day Computation Validation - YearText', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.42', 'DataValid_Dimension_Day Computation Validation - YearText', 'Fail', 'There is an error in the YearText column')
End

--YearNumber
IF Not Exists  (
				SELECT	DayKey
						, DayDate
						, YearNumber
						, YearKey
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and YearNumber != YearKey
						and YearKey not in (-1,0)
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.43', 'DataValid_Dimension_Day Computation Validation - YearNumber', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.43', 'DataValid_Dimension_Day Computation Validation - YearNumber', 'Fail', 'There is an error in the YearNumber column')
End

--YearStartDate
IF Not Exists  (
				SELECT	DayKey
						, DayDate
						, YearStartDate
						, Cast (Year(DayDate) as char(4)) + '-01-01'
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and YearStartDate != Cast (Year(DayDate) as char(4)) + '-01-01'
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.44', 'DataValid_Dimension_Day Computation Validation - YearStartDate', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.44', 'DataValid_Dimension_Day Computation Validation - YearStartDate', 'Fail', 'There is an error in the YearStartDate column')
End

--YearEndDate
IF Not Exists  (
				SELECT	DayKey
						, DayDate
						, YearEndDate
						, Cast (Year(DayDate) as char(4)) + '-12-31'
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and YearEndDate != Cast (Year(DayDate) as char(4)) + '-12-31'
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.45', 'DataValid_Dimension_Day Computation Validation - YearEndDate', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.45', 'DataValid_Dimension_Day Computation Validation - YearEndDate', 'Fail', 'There is an error in the YearEndDate column')
End

--YearDays
IF Not Exists  (
				SELECT	DayKey
						, DayDate
						, YearDays
						, DATEDIFF(Day,YearStartDate,YearEndDate) +1
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and YearDays != DATEDIFF(Day,YearStartDate,YearEndDate) +1
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.46', 'DataValid_Dimension_Day Computation Validation - YearDays', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.46', 'DataValid_Dimension_Day Computation Validation - YearDays', 'Fail', 'There is an error in the YearDays column')
End

--TaxQuarterKey
--CCIA-3277 there are 5244 incorrect records (Corrected 2nd April 2015)
IF Not Exists  (
				Select	*
				From	(
						SELECT	DayKey
								, DayDate
								, TaxQuarterKey
								, 'Correction' = Case
									When Month(DayDate) in (1,2,3) then cast (Year(DayDate) -1 as char(4)) + '3'
									When Month(DayDate) in (4,5,6) then cast (Year(DayDate) -1 as char(4)) + '4'
									When Month(DayDate) in (7,8,9) then cast (Year(DayDate) as char(4)) + '1'
									When Month(DayDate) in (10,11,12) then cast (Year(DayDate) as char(4)) + '2'
									end
						From	#DimensionDay
						where	DayKey not in (0 ,-1)
								
						) as Data
				where TaxQuarterKey != Correction
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.47', 'DataValid_Dimension_Day Computation Validation - TaxQuarterKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.47', 'DataValid_Dimension_Day Computation Validation - TaxQuarterKey', 'Fail', 'There is an error in the TaxQuarterKey column')
End

--TaxQuarterName
--CCIA-3277 there are 5244 incorrect records
IF Not Exists  (
				Select	*
				From	(
						SELECT	DayKey
								, DayDate
								, TaxQuarterName
								, 'Q' = Case
									When Month(DayDate) in (1,2,3) then 'Q3-' + Right (cast (Year(DayDate) -1 as char(4)),2) + '/' + Right(cast (Year(DayDate) as char(4)),2) 
									When Month(DayDate) in (4,5,6) then 'Q4-' + Right (cast (Year(DayDate) -1 as char(4)),2) + '/' + Right(cast (Year(DayDate) as char(4)),2)
									When Month(DayDate) in (7,8,9) then 'Q1-' + Right (cast (Year(DayDate) as char(4)),2) + '/' + Right(cast (Year(DayDate) +1 as char(4)),2)
									When Month(DayDate) in (10,11,12) then 'Q2-' + Right (cast (Year(DayDate) as char(4)),2) + '/' + Right(cast (Year(DayDate) +1 as char(4)),2)
									end
						From	#DimensionDay
						where	DayKey not in (0 ,-1)
								
						) as Data
				where TaxQuarterName != Q
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.48', 'DataValid_Dimension_Day Computation Validation - TaxQuarterName', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.48', 'DataValid_Dimension_Day Computation Validation - TaxQuarterName', 'Fail', 'There is an error in the TaxQuarterName column')
End

--TaxQuarterText
--CCIA-3324 there are 5244 incorrect records (Corrected 2nd April 2015)
IF Not Exists  (
				SELECT	DayKey
						, DayDate
						, TaxQuarterText
						, TaxQuarterName
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and TaxQuarterText != TaxQuarterName
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.49', 'DataValid_Dimension_Day Computation Validation - TaxQuarterText', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.49', 'DataValid_Dimension_Day Computation Validation - TaxQuarterText', 'Fail', 'There is an error in the TaxQuarterText column')
End

--TaxQuarterStartDate
set @Counter =   (
				SELECT	count(*)
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						--the quarter start dates will always be the same date
						and TaxQuarterStartDate != QuarterStartDate
			)
If @Counter = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.50', 'DataValid_Dimension_Day Computation Validation - TaxQuarterStartDate', 'Pass', Null)
End
Else
Begin
	--CCIA-3453 (Corrected 20th April 2015)
	IF @Counter = 1
	and (SELECT	DayKey
		 From	#DimensionDay
		 where	DayKey not in (0 ,-1)
				
				and TaxQuarterStartDate != QuarterStartDate
		 ) = 2015123101
	and
	exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-3453'
							and [Status] != 'Done'
			)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.50', 'DataValid_Dimension_Day Computation Validation - TaxQuarterStartDate', 'Pass', 'CCIA-3453')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.50', 'DataValid_Dimension_Day Computation Validation - TaxQuarterStartDate', 'Fail', 'There is an error in the TaxQuarterStartDate column')
	End
End

--TaxQuarterEndDate
set @Counter = (
				SELECT	count (*)
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and TaxQuarterEndDate != QuarterEndDate
			)
If @Counter = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.51', 'DataValid_Dimension_Day Computation Validation - TaxQuarterEndDate', 'Pass', Null)
End
Else
Begin
--CCIA-3453
	IF @Counter = 1
	and (SELECT	DayKey
		 From	#DimensionDay
		 where	DayKey not in (0 ,-1)
				
				and TaxQuarterStartDate != QuarterStartDate
		 ) = 2015123101
	and
	exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-3453'
							and [Status] != 'Done'
			)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.51', 'DataValid_Dimension_Day Computation Validation - TaxQuarterEndDate', 'Pass', 'CCIA-3453')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.51', 'DataValid_Dimension_Day Computation Validation - TaxQuarterEndDate', 'Fail', 'There is an error in the TaxQuarterEndDate column')
	End
End

--TaxQuarterDays
IF Not Exists  (
				SELECT	DayKey
						, DayDate
						, TaxQuarterDays
						, QuarterDays
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and TaxQuarterDays != QuarterDays
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.52', 'DataValid_Dimension_Day Computation Validation - TaxQuarterDays', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.52', 'DataValid_Dimension_Day Computation Validation - TaxQuarterDays', 'Fail', 'There is an error in the TaxQuarterDays column')
End

--TaxQuarterOfYearText
IF Not Exists  (
				SELECT	DayKey
						, DayDate
						, TaxQuarterOfYearText
						, TaxQuarterText
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and TaxQuarterOfYearText != Left (TaxQuarterText,2)
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.53', 'DataValid_Dimension_Day Computation Validation - TaxQuarterOfYearText', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.53', 'DataValid_Dimension_Day Computation Validation - TaxQuarterOfYearText', 'Fail', 'There is an error in the TaxQuarterOfYearText column')
End

--TaxQuarterOfYearNumber
IF Not Exists  (
				SELECT	DayKey
						, DayDate
						, TaxQuarterOfYearText
						, TaxQuarterText
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						and cast (TaxQuarterOfYearNumber as varchar(10)) != Right (Left (TaxQuarterText,2),1)
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.54', 'DataValid_Dimension_Day Computation Validation - TaxQuarterOfYearNumber', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.54', 'DataValid_Dimension_Day Computation Validation - TaxQuarterOfYearNumber', 'Fail', 'There is an error in the TaxQuarterOfYearNumber column')

End

--TaxYearKey
--CCIA-3325 there are 1767 errors (Corrected 2nd April 2015)
IF Not Exists  (
				Select	*
				From	(
						SELECT	DayKey
								, DayDate
								, TaxYearKey
								, 'Q' = Case
									When Month(DayDate) in (1,2,3) then cast (Year(DayDate) -1 as char(4))
									When Month(DayDate) in (4,5,6) then cast (Year(DayDate) -1 as char(4))
									When Month(DayDate) in (7,8,9) then cast (Year(DayDate) as char(4))
									When Month(DayDate) in (10,11,12) then cast (Year(DayDate) as char(4))
									end
						From	#DimensionDay
						where	DayKey not in (0 ,-1)
								
						) as Data
				where TaxYearKey != Q
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.55', 'DataValid_Dimension_Day Computation Validation - TaxYearKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.55', 'DataValid_Dimension_Day Computation Validation - TaxYearKey', 'Fail', 'There is an error in the TaxYearKey column')
End

--TaxYearName
--CCIA-3279 there are 1767 errors (Corrected 2nd April 2015)
IF Not Exists  (
				Select	*
				From	(
						SELECT	DayKey
								, DayDate
								, TaxYearName
								, 'Correction' = Case
									When Month(DayDate) in (1,2,3) then Right (cast (Year(DayDate) -1 as char(4)),2) + '/' + Right(cast (Year(DayDate) as char(4)),2) 
									When Month(DayDate) in (4,5,6) then Right (cast (Year(DayDate) -1 as char(4)),2) + '/' + Right(cast (Year(DayDate) as char(4)),2)
									When Month(DayDate) in (7,8,9) then Right (cast (Year(DayDate) as char(4)),2) + '/' + Right(cast (Year(DayDate) +1 as char(4)),2)
									When Month(DayDate) in (10,11,12) then Right (cast (Year(DayDate) as char(4)),2) + '/' + Right(cast (Year(DayDate) +1 as char(4)),2)
									end
						From	#DimensionDay
						where	DayKey not in (0 ,-1)
								
						) as Data
				where TaxYearName != Correction
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.56', 'DataValid_Dimension_Day Computation Validation - TaxYearName', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.56', 'DataValid_Dimension_Day Computation Validation - TaxYearName', 'Fail', 'There is an error in the TaxYearName column')
End

--TaxYearText
IF Not Exists  (
				SELECT	DayKey
						, DayDate
						, TaxYearText
						, TaxYearName
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and TaxYearText != TaxYearName
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.57', 'DataValid_Dimension_Day Computation Validation - TaxYearText', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.57', 'DataValid_Dimension_Day Computation Validation - TaxYearText', 'Fail', 'There is an error in the TaxYearText column')
End

--TaxYearNumber
set @Counter =  (
				SELECT	count (*)
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and TaxYearNumber != TaxYearKey
			)
If @Counter = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.58', 'DataValid_Dimension_Day Computation Validation - TaxYearNumber', 'Pass', Null)
End
Else
Begin
	--CCIA-3456 (Corrected 20th April 2015)
	if @Counter = 1767
	and (SELECT	count(*)
		From	#DimensionDay
		where	DayKey not in (0 ,-1)
				
				and TaxYearStartDate != cast (TaxYearKey as char(4)) + '-07-01'
				and month(DayDate) != 7
		) = 0
	and	exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-3456'
							and [Status] != 'Done'
			)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.58', 'DataValid_Dimension_Day Computation Validation - TaxYearNumber', 'Pass', 'CCIA-3456')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.58', 'DataValid_Dimension_Day Computation Validation - TaxYearNumber', 'Fail', 'There is an error in the TaxYearNumber column')
	End
End

--TaxYearStartDate
Set @counter = (
				SELECT	count(*)
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and TaxYearStartDate != cast (TaxYearKey as char(4)) + '-07-01'
			)
if @Counter = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.59', 'DataValid_Dimension_Day Computation Validation - TaxYearStartDate', 'Pass', Null)
End
Else
Begin
	--CCIA-3456 (Corrected 17th April 2015)
	if @Counter = 1767
	and (SELECT	count(*)
		From	#DimensionDay
		where	DayKey not in (0 ,-1)
				
				and TaxYearStartDate != cast (TaxYearKey as char(4)) + '-07-01'
				and month(DayDate) != 7
		) = 0
	and	exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-3456'
							and [Status] != 'Done'
			)
	begin
		Insert into test.tblResults Values (@TestRunNo, '36.59', 'DataValid_Dimension_Day Computation Validation - TaxYearStartDate', 'Pass', 'CCIA-3456')
	end
	else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.59', 'DataValid_Dimension_Day Computation Validation - TaxYearStartDate', 'Fail', 'There is an error in the TaxYearStartDate column')
	End
End

--TaxYearEndDate
set @Counter =  (
				SELECT	count (*)
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and convert (varchar(10),TaxYearEndDate,20) != cast (TaxYearKey + 1 as char(4)) + '-06-30'
			)
If @Counter = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.60', 'DataValid_Dimension_Day Computation Validation - TaxYearEndDate', 'Pass', Null)
End
Else
Begin
	--CCIA-3456
	if @Counter = 1767
	and (SELECT	count(*)
		From	#DimensionDay
		where	DayKey not in (0 ,-1)
				
				and TaxYearStartDate != cast (TaxYearKey as char(4)) + '-07-01'
				and month(DayDate) != 7
		) = 0
	and	exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-3456'
							and [Status] != 'Done'
			)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.60', 'DataValid_Dimension_Day Computation Validation - TaxYearEndDate', 'Pass', 'CCIA-3456')
	End
	Else
	begin
	Insert into test.tblResults Values (@TestRunNo, '36.60', 'DataValid_Dimension_Day Computation Validation - TaxYearEndDate', 'Fail', 'There is an error in the TaxYearEndDate column')
	End
End

--TaxYearDays
set @counter =  (
				SELECT	count (*)
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and TaxYearDays != DateDiff(Day,TaxYearStartDate, TaxYearEndDate)+1
			)
IF @counter = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.61', 'DataValid_Dimension_Day Computation Validation - TaxYearDays', 'Pass', Null)
End
Else
Begin
	--CCIA-3539
	IF @counter = 30
	and exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-3539'
							and [Status] != 'Done'
			)
	begin
		Insert into test.tblResults Values (@TestRunNo, '36.61', 'DataValid_Dimension_Day Computation Validation - TaxYearDays', 'Pass', 'CCIA-3460')
	end
	else
	begin
		Insert into test.tblResults Values (@TestRunNo, '36.61', 'DataValid_Dimension_Day Computation Validation - TaxYearDays', 'Fail', 'There is an error in the TaxYearDays column')
	end
End

--FiscalWeekKey
--CCIA-3280 - Keys ending in 00 (Corrected 2nd April 2015)
IF Not Exists  (
				SELECT	DayKey
						, DayDate
						, FiscalWeekKey
						, FiscalWeekStartDate
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and (
							Right (FiscalWeekKey,2) = '00'
							or 
							Right (FiscalWeekKey,2) > '53'
							)					
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.62', 'DataValid_Dimension_Day Computation Validation - FiscalWeekKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.62', 'DataValid_Dimension_Day Computation Validation - FiscalWeekKey', 'Fail', 'There is an error in the FiscalWeekKey column')
End

Set @Counter =   (Select	Count (*)
				From	(
						SELECT	DayKey
								, DayDate
								, FiscalYearStartDate
								, FiscalWeekKey
								, 'Correction' = CASE
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 <= 7 then FiscalYearText + '01'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 8 and 14 then FiscalYearText + '02'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 15 and 21 then FiscalYearText + '03'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 22 and 28 then FiscalYearText + '04'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 29 and 35 then FiscalYearText + '05'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 36 and 42 then FiscalYearText + '06'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 43 and 49 then FiscalYearText + '07'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 50 and 56 then FiscalYearText + '08'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 57 and 63 then FiscalYearText + '09'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 64 and 70 then FiscalYearText + '10'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 71 and 77 then FiscalYearText + '11'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 78 and 84 then FiscalYearText + '12'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 85 and 91 then FiscalYearText + '13'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 92 and 98 then FiscalYearText + '14'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 99 and 105 then FiscalYearText + '15'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 106 and 112 then FiscalYearText + '16'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 113 and 119 then FiscalYearText + '17'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 120 and 126 then FiscalYearText + '18'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 127 and 133 then FiscalYearText + '19'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 134 and 140 then FiscalYearText + '20'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 141 and 147 then FiscalYearText + '21'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 148 and 154 then FiscalYearText + '22'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 155 and 161 then FiscalYearText + '23'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 162 and 168 then FiscalYearText + '24'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 169 and 175 then FiscalYearText + '25'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 176 and 182 then FiscalYearText + '26'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 183 and 189 then FiscalYearText + '27'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 190 and 196 then FiscalYearText + '28'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 197 and 203 then FiscalYearText + '29'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 204 and 210 then FiscalYearText + '30'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 211 and 217 then FiscalYearText + '31'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 218 and 224 then FiscalYearText + '32'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 225 and 231 then FiscalYearText + '33'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 232 and 238 then FiscalYearText + '34'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 239 and 245 then FiscalYearText + '35'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 246 and 252 then FiscalYearText + '36'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 253 and 259 then FiscalYearText + '37'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 260 and 266 then FiscalYearText + '38'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 267 and 273 then FiscalYearText + '39'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 274 and 280 then FiscalYearText + '40'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 281 and 287 then FiscalYearText + '41'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 288 and 294 then FiscalYearText + '42'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 295 and 301 then FiscalYearText + '43'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 302 and 308 then FiscalYearText + '44'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 309 and 315 then FiscalYearText + '45'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 316 and 322 then FiscalYearText + '46'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 323 and 329 then FiscalYearText + '47'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 330 and 336 then FiscalYearText + '48'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 337 and 343 then FiscalYearText + '49'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 344 and 350 then FiscalYearText + '50'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 351 and 357 then FiscalYearText + '51'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 358 and 364 then FiscalYearText + '52'
									else FiscalYearText + '53'
									end
						From	#DimensionDay
						where	DayKey not in (0 ,-1)
								
						) as Data
				Where	FiscalWeekKey != Correction
			)
If @Counter = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.62a', 'DataValid_Dimension_Day Computation Validation - FiscalWeekKey', 'Pass', Null)
End
Else
Begin
	--CCIA-3561
	If @Counter = 1123
	and exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-3561'
							and [Status] != 'Done'
				)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.62a', 'DataValid_Dimension_Day Computation Validation - FiscalWeekKey', 'Pass', 'CCIA-3561')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.62a', 'DataValid_Dimension_Day Computation Validation - FiscalWeekKey', 'Fail', 'There are errors in the FiscalWeekKey column')
	End
End

--FiscalWeekName
--CCIA-3326 - Keys ending in 00 have resulted in a W00 prefix (Corrected 1 April 2015)
set @Counter =  (
				SELECT	count(*)
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						and FiscalWeekName != 'W' + Cast (Right (FiscalWeekKey,2) as char(2)) + '-' + Cast (Left (FiscalWeekKey,4) as char(4))
			)
If @Counter = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.63', 'DataValid_Dimension_Day Computation Validation - FiscalWeekName', 'Pass', Null)
End
Else
Begin
	--CCIA-3482 (Corrected 20th April 2015)
	If @Counter = 16
	and	exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-3482'
							and [Status] != 'Done'
				)  
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.63', 'DataValid_Dimension_Day Computation Validation - FiscalWeekName', 'Pass', 'CCIA-3482')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.63', 'DataValid_Dimension_Day Computation Validation - FiscalWeekName', 'Fail', 'There is an error in the FiscalWeekName column')
	End
End

--FiscalWeekText
set @Counter = (
				SELECT	Count(*)
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						and FiscalWeekText != FiscalWeekName
			)
IF @Counter = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.64', 'DataValid_Dimension_Day Computation Validation - FiscalWeekText', 'Pass', Null)
End
Else
Begin
	--CCIA-3679
	IF @Counter = 1131
	and exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-3679'
							and [Status] != 'Done'
				)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.64', 'DataValid_Dimension_Day Computation Validation - FiscalWeekText', 'Pass', 'CCIA-3679')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.64', 'DataValid_Dimension_Day Computation Validation - FiscalWeekText', 'Fail', 'There is an error in the FiscalWeekText column')
	End
End

--FiscalWeekStartDate
IF Not Exists  (Select	*
				From	(SELECT	DayDate
								, FiscalWeekStartDate
								, 'Correction' = CASE
									When DatePart(WEEKDAY,daydate) = 1 then DateAdd(Day,-5,DayDate)
									When DatePart(WEEKDAY,daydate) = 2 then DateAdd(Day,-6,DayDate)
									When DatePart(WEEKDAY,daydate) = 3 then DayDate
									When DatePart(WEEKDAY,daydate) = 4 then DateAdd(Day,-1,DayDate)
									When DatePart(WEEKDAY,daydate) = 5 then DateAdd(Day,-2,DayDate)
									When DatePart(WEEKDAY,daydate) = 6 then DateAdd(Day,-3,DayDate)
									When DatePart(WEEKDAY,daydate) = 7 then DateAdd(Day,-4,DayDate)
									else 'ERROR'
									End
						From	#DimensionDay
						where	DayKey not in (0 ,-1)
								
						) as Data
				Where FiscalWeekStartDate != Correction
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.65', 'DataValid_Dimension_Day Computation Validation - FiscalWeekStartDate', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.65', 'DataValid_Dimension_Day Computation Validation - FiscalWeekStartDate', 'Fail', 'There is an error in the FiscalWeekStartDate column')
End

--FiscalWeekEndDate
IF Not Exists  (Select	*
				From	(SELECT	DayDate
								, FiscalWeekEndDate
								, 'Correction' = CASE
									When DatePart(WEEKDAY,daydate) = 1 then DateAdd(Day,1,DayDate)
									When DatePart(WEEKDAY,daydate) = 2 then DayDate
									When DatePart(WEEKDAY,daydate) = 3 then DateAdd(Day,6,DayDate)
									When DatePart(WEEKDAY,daydate) = 4 then DateAdd(Day,5,DayDate)
									When DatePart(WEEKDAY,daydate) = 5 then DateAdd(Day,4,DayDate)
									When DatePart(WEEKDAY,daydate) = 6 then DateAdd(Day,3,DayDate)
									When DatePart(WEEKDAY,daydate) = 7 then DateAdd(Day,2,DayDate)
									else 'ERROR'
									End
						From	#DimensionDay
						where	DayKey not in (0 ,-1)
								
						) as Data
				Where FiscalWeekEndDate != Correction
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.66', 'DataValid_Dimension_Day Computation Validation - FiscalWeekEndDate', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.66', 'DataValid_Dimension_Day Computation Validation - FiscalWeekEndDate', 'Fail', 'There is an error in the FiscalWeekEndDate column')
End

--FiscalWeekOfMonthText
IF Not Exists  (Select	*
				from	(SELECT	DayKey
								, DayDate
								, FiscalWeekOfMonthText
								, 'Correction' = case
									When right (FiscalWeekKey,2) = '01' then 'W1'
									When right (FiscalWeekKey,2) = '02' then 'W2'
									When right (FiscalWeekKey,2) = '03' then 'W3'
									When right (FiscalWeekKey,2) = '04' then 'W1'
									When right (FiscalWeekKey,2) = '05' then 'W2'
									When right (FiscalWeekKey,2) = '06' then 'W3'
									When right (FiscalWeekKey,2) = '07' then 'W1'
									When right (FiscalWeekKey,2) = '08' then 'W2'
									When right (FiscalWeekKey,2) = '09' then 'W3'
									When right (FiscalWeekKey,2) = '10' then 'W1'
									When right (FiscalWeekKey,2) = '11' then 'W2'
									When right (FiscalWeekKey,2) = '12' then 'W3'
									else 'W4'
									End
						From	#DimensionDay
						where	DayKey not in (0 ,-1)
								
						) as Data
				Where	FiscalWeekOfMonthText != Correction
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.67', 'DataValid_Dimension_Day Computation Validation - FiscalWeekOfMonthText', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.67', 'DataValid_Dimension_Day Computation Validation - FiscalWeekOfMonthText', 'Fail', 'There are errors in the FiscalWeekOfMonthText column')
End

--FiscalWeekOfMonthNumber
Set @Counter =   (
				SELECT	Count(*)
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						and FiscalWeekOfMonthNumber not in (0,9)
						and FiscalWeekOfMonthNumber != Right (FiscalWeekOfMonthText,1)
				)
If @Counter = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.68', 'DataValid_Dimension_Day Computation Validation - FiscalWeekOfMonthNumber', 'Pass', Null)
End
Else
Begin
	--CCIA-3695
	If @Counter = 0
	and exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-3695'
							and [Status] != 'Done'
				)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.68', 'DataValid_Dimension_Day Computation Validation - FiscalWeekOfMonthNumber', 'Pass', 'CCIA-3695')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.68', 'DataValid_Dimension_Day Computation Validation - FiscalWeekOfMonthNumber', 'Fail', 'There is an error in the FiscalWeekOfMonthNumber column')
	End
End

--FiscalWeekOfYearText
Set @Counter  = (
				SELECT	Count (*)
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						and FiscalWeekOfYearText != Replace (Left (FiscalWeekText,3),'W0', 'W')
			)
IF @Counter = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.69', 'DataValid_Dimension_Day Computation Validation - FiscalWeekOfYearText', 'Pass', Null)
End
Else
Begin
	--CCIA-3696
	IF @Counter = 1503
	and exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-3696'
							and [Status] != 'Done'
				)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.69', 'DataValid_Dimension_Day Computation Validation - FiscalWeekOfYearText', 'Pass', 'CCIA-3696')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.69', 'DataValid_Dimension_Day Computation Validation - FiscalWeekOfYearText', 'Fail', 'There is an error in the FiscalWeekOfYearText column')
	End
End

--FiscalWeekOfYearNumber
IF Not Exists  (
				SELECT	DayKey
						, DayDate
						, FiscalWeekOfYearNumber
						, FiscalWeekText
						, Replace (Replace (Left (FiscalWeekText,3),'W0', ''), 'W', '') as Correction
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						and FiscalWeekOfYearNumber != Replace (Replace (Left (FiscalWeekText,3),'W0', ''), 'W', '')
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.70', 'DataValid_Dimension_Day Computation Validation - FiscalWeekOfYearNumber', 'Pass', Null)
End
Else
Begin
	--CCIA-3281 
	Insert into test.tblResults Values (@TestRunNo, '36.70', 'DataValid_Dimension_Day Computation Validation - FiscalWeekOfYearNumber', 'Fail', 'There is an error in the FiscalWeekOfYearNumber column')
End

--FiscalMonthKey
set @Counter =  (Select	count (*)
				From	(
						SELECT	DayKey
								, DayDate
								, FiscalMonthKey
								, FiscalYearStartDate
								, 'Correction' = CASE
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 <= 28 then FiscalYearText + '01'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 29 and 56 then FiscalYearText + '02'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 57 and 91 then FiscalYearText + '03'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 92 and 119 then FiscalYearText + '04'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 120 and 147 then FiscalYearText + '05'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 148 and 182 then FiscalYearText + '06'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 183 and 210 then FiscalYearText + '07'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 211 and 238 then FiscalYearText + '08'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 239 and 273 then FiscalYearText + '09'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 274 and 301 then FiscalYearText + '10'
									When DateDiff(Day, FiscalYearStartDate, Daydate) +1 between 302 and 329 then FiscalYearText + '11'
									else FiscalYearText + '12'
									end
						From	#DimensionDay
						where	DayKey not in (0 ,-1)
								
						) as Data
				Where	FiscalMonthKey != Correction
			)
if @Counter = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.71', 'DataValid_Dimension_Day Computation Validation - FiscalMonthKey', 'Pass', Null)
End
Else
Begin
	--CCIA-3558
	if @Counter = 36
	and	exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-3558'
							and [Status] != 'Done'
				) 
	begin
		Insert into test.tblResults Values (@TestRunNo, '36.71', 'DataValid_Dimension_Day Computation Validation - FiscalMonthKey', 'Pass', 'CCIA-3457')
	end
	else
	begin
		Insert into test.tblResults Values (@TestRunNo, '36.71', 'DataValid_Dimension_Day Computation Validation - FiscalMonthKey', 'Fail', 'There are errors in the FiscalMonthKey column')
	end
End

--FiscalMonthName
set @Counter =   (
				SELECT	count (*)
						--, Replace (Right (Convert (varchar(11), Cast (Left (cast (FiscalMonthKey as char(6)), 4) + '-' + Right (Cast (FiscalMonthKey as char(6)),2) + '-01' as date),106),8),' ', '-') as Correction
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						and Cast (Left (cast (FiscalMonthKey as char(6)), 4) + '-' + Right (Cast (FiscalMonthKey as char(6)),2) + '-01' as date) != cast ('01-' + FiscalMonthName as date)
				)	
If @Counter = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.72', 'DataValid_Dimension_Day Computation Validation - FiscalMonthName', 'Pass', Null)
End
Else
Begin
	--CCIA-3563
	If @Counter = 614
	and exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-3563'
							and [Status] != 'Done'
				)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.72', 'DataValid_Dimension_Day Computation Validation - FiscalMonthName', 'Pass', 'CCIA-3563')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.72', 'DataValid_Dimension_Day Computation Validation - FiscalMonthName', 'Fail', 'There is an error in the FiscalMonthName column')
	End
End

--FiscalMonthText
IF Not Exists  (
				SELECT	DayKey
						, DayDate
						, FiscalMonthName
						, FiscalMonthText
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and FiscalMonthText != FiscalMonthName
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.73', 'DataValid_Dimension_Day Computation Validation - FiscalMonthText', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.73', 'DataValid_Dimension_Day Computation Validation - FiscalMonthText', 'Fail', 'There is an error in the FiscalMonthText column')
End

--FiscalMonthStartDate
set @Counter = 	(Select		Count (*)
				 From		(SELECT	DayDate
									, FiscalMonthStartDate
									, FiscalQuarterStartDate
									, FiscalQuarterKey
									, DateDiff(Day,FiscalQuarterStartDate,Daydate)+1 as DaysfromQStart
									, 'Correction' = CASE
										When DateDiff(Day,FiscalQuarterStartDate,Daydate)+1 <= 28 then FiscalQuarterStartDate
										When DateDiff(Day,FiscalQuarterStartDate,Daydate)+1 between 29 and 56 then DateAdd(Day,28,FiscalQuarterStartDate)									
										else DateAdd(Day,56,FiscalQuarterStartDate)
										end
							 From	#DimensionDay
							 where	DayKey not in (0 ,-1)							
							) as Data
				Where		FiscalMonthStartDate != Correction
				)
If @Counter = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.74', 'DataValid_Dimension_Day Computation Validation - FiscalMonthStartDate', 'Pass', Null)
End
Else
Begin
	--CCIA-3692
	IF @Counter = 18
	and exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-3692'
							and [Status] != 'Done'
				)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.74', 'DataValid_Dimension_Day Computation Validation - FiscalMonthStartDate', 'Pass', 'CCIA-3501')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.74', 'DataValid_Dimension_Day Computation Validation - FiscalMonthStartDate', 'Fail', 'There is an error in the FiscalMonthStartDate Column')
	End
End

--FiscalMonthEndDate
IF Not Exists  (Select	*
				From	(
						SELECT	DayKey
								, DayDate
								, FiscalMonthEndDate
								, FiscalMonthStartDate
								, FiscalMonthOfYearNumber
								, 'Correction' = CASE
									When FiscalMonthOfYearNumber = 12 then FiscalYearEndDate
									When FiscalMonthOfYearNumber in (3,6,9) then DateAdd(Day, 34, FiscalMonthStartDate)
									else DateAdd(Day, 27, FiscalMonthStartDate)
									End
						From	#DimensionDay
						where	DayKey not in (0 ,-1)
								
						) as Data
				Where FiscalMonthEndDate != Correction
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.75', 'DataValid_Dimension_Day Computation Validation - FiscalMonthEndDate', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.75', 'DataValid_Dimension_Day Computation Validation - FiscalMonthEndDate', 'Fail', 'There is an error in the FiscalMonthEndDate column')

End

--FiscalMonthDays
IF Not Exists  (
				SELECT	DayKey
						, DayDate
						, FiscalMonthDays
						, DateDiff(Day, FiscalMonthStartDate, FiscalMonthEndDate) +1
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and FiscalMonthDays != DateDiff(Day, FiscalMonthStartDate, FiscalMonthEndDate) +1
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.76', 'DataValid_Dimension_Day Computation Validation - FiscalMonthDays', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.76', 'DataValid_Dimension_Day Computation Validation - FiscalMonthDays', 'Fail', 'There is an error in the FiscalMonthDays column')
End

--FiscalMonthWeeks
set @Counter =   (
				SELECT	Count (*)
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and FiscalMonthWeeks != FiscalMonthDays / 7
			)
IF @Counter = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.77', 'DataValid_Dimension_Day Computation Validation - FiscalMonthWeeks', 'Pass', Null)
End
Else
Begin
	--CCIA-4042
	IF exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-4042'
							and [Status] != 'Done'
				)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.77', 'DataValid_Dimension_Day Computation Validation - FiscalMonthWeeks', 'Pass', 'CCIA-4042')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.77', 'DataValid_Dimension_Day Computation Validation - FiscalMonthWeeks', 'Fail', 'There is an error in the FiscalMonthWeeks column')
	End
End

--FiscalMonthOfYearText
IF Not Exists  (
				SELECT	DayKey
						, DayDate
						, FiscalMonthOfYearText
						, Left (FiscalMonthName,3)
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and FiscalMonthOfYearText != Left (FiscalMonthName,3)
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.78', 'DataValid_Dimension_Day Computation Validation - FiscalMonthOfYearText', 'Pass', Null)
End
Else
Begin
	IF	exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-3457'
							and [Status] != 'Done'
			)
	begin
		Insert into test.tblResults Values (@TestRunNo, '36.78', 'DataValid_Dimension_Day Computation Validation - FiscalMonthOfYearText', 'Pass', 'CCIA-3457')
	end
	begin
		Insert into test.tblResults Values (@TestRunNo, '36.78', 'DataValid_Dimension_Day Computation Validation - FiscalMonthOfYearText', 'Fail', 'There is an error in the FiscalMonthOfYearText column')
	End
End

--FiscalMonthOfYearNumber
IF Not Exists  (
				SELECT	DayKey
						, DayDate
						, FiscalMonthOfYearNumber
						, cast (Right (FiscalMonthKey,2) as int)
						, FiscalMonthKey
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and FiscalMonthOfYearNumber != cast (Right (FiscalMonthKey,2) as int)
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.79', 'DataValid_Dimension_Day Computation Validation - FiscalMonthOfYearNumber', 'Pass', Null)
End
Else
Begin
IF	exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-3457'
							and [Status] != 'Done'
			)
	begin
		Insert into test.tblResults Values (@TestRunNo, '36.79', 'DataValid_Dimension_Day Computation Validation - FiscalMonthOfYearNumber', 'Pass', 'CCIA-3457')
	end
	else
	begin
		Insert into test.tblResults Values (@TestRunNo, '36.79', 'DataValid_Dimension_Day Computation Validation - FiscalMonthOfYearNumber', 'Fail', 'There is an error in the FiscalMonthOfYearNumber column')
	End
End

--FiscalQuarterKey
set @Counter = 	(Select		count (*)
				 From		(SELECT	DayDate
									, FiscalQuarterKey
									, 'Correction' = CASE
										When DayDate < #vwFiscalYears.FiscalStart then cast (datepart(year,DayDate)-1 as char(4)) + '4'
										When DayDate > #vwFiscalYears.FiscalEnd then cast (datepart(year,DayDate)+1 as char(4)) + '1'
										When DayDate >= dateadd(day,273,#vwFiscalYears.FiscalStart) then #vwFiscalYears.[Year] + '4'
										When DayDate >= dateadd(day,182,#vwFiscalYears.FiscalStart) then #vwFiscalYears.[Year] + '3'
										When DayDate >= dateadd(day,91,#vwFiscalYears.FiscalStart) then #vwFiscalYears.[Year] + '2'
										else #vwFiscalYears.[Year] + '1'
										end
							 From	#DimensionDay
									inner join #vwFiscalYears on #DimensionDay.YearName = #vwFiscalYears.[Year]
							 where	DayKey not in (0 ,-1)
									
							) as Data
				Where		FiscalQuarterKey != Correction
				)
If @Counter = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.80', 'DataValid_Dimension_Day Computation Validation - FiscalQuarterKey', 'Pass', Null)
End
Else
Begin
	--CCIA-3534 
	IF @Counter = 18
	and exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-3534'
							and [Status] != 'Done'
			)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.80', 'DataValid_Dimension_Day Computation Validation - FiscalQuarterKey', 'Pass', 'CCIA-3461')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.80', 'DataValid_Dimension_Day Computation Validation - FiscalQuarterKey', 'Fail', 'There is an error in the FiscalQuarterKey Column')
	End
End

--FiscalQuarterName
set @counter =  (
				SELECT	count (*)						
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and FiscalQuarterName != 'Q' + cast (right (FiscalQuarterKey,1) as varchar(1)) + '-' + Left (FiscalQuarterKey,4)
			)
If @Counter = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.81', 'DataValid_Dimension_Day Computation Validation - FiscalQuarterName', 'Pass', Null)
End
Else
Begin
	--CCIA-3493 (Corrected 20th April 2015)
	IF @Counter = 198
	and	exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-3493'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.81', 'DataValid_Dimension_Day Computation Validation - FiscalQuarterName', 'Pass', 'CCIA-3493')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.81', 'DataValid_Dimension_Day Computation Validation - FiscalQuarterName', 'Fail', 'There is an error in the FiscalQuarterName column')
	End

End

--FiscalQuarterText
set @Counter =   (
					SELECT	Count(*)
					From	#DimensionDay
					where	DayKey not in (0 ,-1)
							and FiscalQuarterText != FiscalQuarterName
			)
IF @Counter = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.82', 'DataValid_Dimension_Day Computation Validation - FiscalQuarterText', 'Pass', Null)
End
Else
Begin
	--CCIA-3691
	IF @Counter = 18
	and exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-3691'
							and [Status] != 'Done'
				)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.82', 'DataValid_Dimension_Day Computation Validation - FiscalQuarterText', 'Pass', 'CCIA-3691')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.82', 'DataValid_Dimension_Day Computation Validation - FiscalQuarterText', 'Fail', 'There is an error in the FiscalQuarterText column')
	End
End

--FiscalQuarterStartDate & FiscalQuarterEndDate
IF Not Exists  (
				SELECT	distinct
						FiscalQuarterStartDate
						, FiscalQuarterEndDate
						, DateDiff (Day, FiscalQuarterStartDate, FiscalQuarterEndDate) +1
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and DateDiff (Day,FiscalQuarterStartDate, FiscalQuarterEndDate) +1 != 91
						AND Month(FiscalQuarterEndDate) not in (1, 12)
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.83', 'DataValid_Dimension_Day Computation Validation - FiscalQuarterStartDate/FiscalQuarterEndDate', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.83', 'DataValid_Dimension_Day Computation Validation - FiscalQuarterStartDate/FiscalQuarterEndDate', 'Fail', 'There is an error in the FiscalQuarterStartDate/FiscalQuarterEndDate column')
End

set @Counter = 	(Select		Count (*)
				 From		(SELECT	DayDate
									, FiscalQuarterStartDate
									, FiscalQuarterKey
									, 'Correction' = CASE
										When right (FiscalQuarterKey,1) = 4 then dateadd(day,273,#vwFiscalYears.FiscalStart)
										When right (FiscalQuarterKey,1) = 3 then dateadd(day,182,#vwFiscalYears.FiscalStart)
										When right (FiscalQuarterKey,1) = 2 then dateadd(day,91,#vwFiscalYears.FiscalStart)
										else #vwFiscalYears.FiscalStart
										end
							 From	#DimensionDay
									inner join #vwFiscalYears on Left (FiscalQuarterKey,4) = #vwFiscalYears.[Year]
							 where	DayKey not in (0 ,-1)
									
							) as Data
				Where		FiscalQuarterStartDate != Correction
				)
if @Counter = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.83a', 'DataValid_Dimension_Day Computation Validation - FiscalQuarterStartDate', 'Pass', Null)
End
Else
Begin
	--CCIA-3489 (Corrected 20th April 2015)
	If @Counter = 1516
	and	exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-3489'
							and [Status] != 'Done'
				)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.83a', 'DataValid_Dimension_Day Computation Validation - FiscalQuarterStartDate', 'Pass', 'CCIA-3489')
	End
	Else
	Begin 
		Insert into test.tblResults Values (@TestRunNo, '36.83a', 'DataValid_Dimension_Day Computation Validation - FiscalQuarterStartDate', 'Fail', 'There is an error in the FiscalQuarterStartDate Column')
	End
End

Set @Counter = 	(Select		count (*)
				 From		(SELECT	DayDate
									, FiscalQuarterEndDate
									, FiscalQuarterKey
									, 'Correction' = CASE
										When right (FiscalQuarterKey,1) = 3 then dateadd(day,272,#vwFiscalYears.FiscalStart)
										When right (FiscalQuarterKey,1) = 2 then dateadd(day,181,#vwFiscalYears.FiscalStart)
										When right (FiscalQuarterKey,1) = 1 then dateadd(day,90,#vwFiscalYears.FiscalStart)
										else #vwFiscalYears.FiscalEnd
										end
							 From	#DimensionDay
									inner join #vwFiscalYears on Left (FiscalQuarterKey,4) = #vwFiscalYears.[Year]
							 where	DayKey not in (0 ,-1)
									
							) as Data
				Where		FiscalQuarterEndDate != Correction
				)
If @Counter = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.83b', 'DataValid_Dimension_Day Computation Validation - FiscalQuarterEndDate', 'Pass', Null)
End
Else
Begin
	--CCIA-3488 (Corrected 20th April 2015)
	If @Counter = 1333
	and	exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-3488'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.83b', 'DataValid_Dimension_Day Computation Validation - FiscalQuarterEndDate', 'Pass', 'CCIA-3488')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.83b', 'DataValid_Dimension_Day Computation Validation - FiscalQuarterEndDate', 'Fail', 'There is an error in the FiscalQuarterEndDate Column')
	End
End

--FiscalQuarterDays
set @Counter =   (
					SELECT	count (*)
					From	#DimensionDay
					where	DayKey not in (0 ,-1)
							and FiscalQuarterDays != DateDiff (Day,FiscalQuarterStartDate, FiscalQuarterEndDate) +1 
				)
IF @Counter = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.84', 'DataValid_Dimension_Day Computation Validation - FiscalQuarterDays', 'Pass', Null)
End
Else
Begin
	--CCIA-3689
	if @Counter = 23
	and exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-3689'
							and [Status] != 'Done'
				)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.84', 'DataValid_Dimension_Day Computation Validation - FiscalQuarterDays', 'Pass', 'CCIA-3689')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.84', 'DataValid_Dimension_Day Computation Validation - FiscalQuarterDays', 'Fail', 'There is an error in the FiscalQuarterDays column')
	End
End

--FiscalQuarterWeeks
set @Counter = (
				SELECT	count (*)
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and FiscalQuarterWeeks != FiscalQuarterDays / 7
				)
if @Counter = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.85', 'DataValid_Dimension_Day Computation Validation - FiscalQuarterWeeks', 'Pass', Null)
End
Else
Begin
	--CCIA-3513 (Corrected 22nd April 2015)
	if @Counter = 86
	and exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-3513'
							and [Status] != 'Done'
				)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.85', 'DataValid_Dimension_Day Computation Validation - FiscalQuarterWeeks', 'Pass', 'CCIA-3513')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.85', 'DataValid_Dimension_Day Computation Validation - FiscalQuarterWeeks', 'Fail', 'There is an error in the FiscalQuarterWeeks column')
	End
End

--FiscalQuarterOfYearText
IF Not Exists  (
				SELECT	distinct
						DayDate
						, FiscalQuarterOFYearText
						, FiscalQuarterOFYearNumber
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and FiscalQuarterOFYearText != 'Q' + cast (FiscalQuarterOFYearNumber as char(1))
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.86', 'DataValid_Dimension_Day Computation Validation - FiscalQuarterOFYearText', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.86', 'DataValid_Dimension_Day Computation Validation - FiscalQuarterOFYearText', 'Fail', 'There is an error in the FiscalQuarterOFYearText column')
End

--FiscalQuarterOfYearNumber
set @Counter =   (
				SELECT	Count(*)
				From	#DimensionDay
				where	DayKey not in (0 ,-1)					
						and (
							FiscalQuarterOFYearNumber = 1 and Month (FiscalQuarterEndDate) not in (3,4)
							or 
							FiscalQuarterOFYearNumber = 2 and Month (FiscalQuarterEndDate) not in (6,7)
							or 
							FiscalQuarterOFYearNumber = 3 and Month (FiscalQuarterEndDate) not in (9,10)
							or 
							FiscalQuarterOFYearNumber = 4 and Month (FiscalQuarterEndDate) not in (12,1)
							)
				)
if @Counter = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.87', 'DataValid_Dimension_Day Computation Validation - FiscalQuarterOFYearNumber', 'Pass', Null)
End
Else
Begin
	--CCIA-3690
	if @Counter = 18
	and	exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-3690'
							and [Status] != 'Done' 
				)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.87', 'DataValid_Dimension_Day Computation Validation - FiscalQuarterOFYearNumber', 'Pass', 'CCIA-3690')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.87', 'DataValid_Dimension_Day Computation Validation - FiscalQuarterOFYearNumber', 'Fail', 'There is an error in the FiscalQuarterOFYearNumber column')
	End
End

--FiscalYearKey
Set @Counter = (Select	Count(*)
				From	(
						SELECT	DayKey
								, DayDate
								, FiscalYearKey
								, #vwFiscalYears.[Year] as Correction
						From	#DimensionDay
								Cross join #vwFiscalYears
						where	DayKey not in (0 ,-1)
								
								and #DimensionDay.DayDate >= #vwFiscalYears.FiscalStart
								and #DimensionDay.DayDate <= #vwFiscalYears.FiscalEnd
						) as Data
				Where	FiscalYearKey != Correction
				)
If @Counter = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.88', 'DataValid_Dimension_Day Computation Validation - FiscalYearKey', 'Pass', Null)
End
Else
Begin
	--CCIA-3446 there are errors pre 2002 (Corrected 20th April 2015)
	IF @Counter = 63
	and		(Select	Count(*)
				From	(
						SELECT	DayKey
								, DayDate
								, FiscalYearKey
								, #vwFiscalYears.[Year] as Correction
						From	#DimensionDay
								Cross join #vwFiscalYears
						where	DayKey not in (0 ,-1)
								
								and #DimensionDay.DayDate >= #vwFiscalYears.FiscalStart
								and #DimensionDay.DayDate <= #vwFiscalYears.FiscalEnd
								and DayDate > '2002-01-01'
						) as Data
				Where	FiscalYearKey != Correction
				) = 0
	and	exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-3446'
							and [Status] != 'Done'
				)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.88', 'DataValid_Dimension_Day Computation Validation - FiscalYearKey', 'Pass', 'CCIA-3446')
	End
	Else
	--CCIA-3537
	IF @Counter = 6
	and exists		(Select		*
						From		test.tblDefects
						Where		DefectID = 'CCIA-3537'
								and [Status] != 'Done'
					)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.88', 'DataValid_Dimension_Day Computation Validation - FiscalYearKey', 'Pass', 'CCIA-3537')
	End	
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.88', 'DataValid_Dimension_Day Computation Validation - FiscalYearKey', 'Fail', 'There is an error in the FiscalYearKey column')
	End
End

set @Counter = 	(Select		Count (*)
				 From		(SELECT	DayDate
									, FiscalYearKey
									, #vwFiscalYears.[Year] as Correction
							 From	#DimensionDay
									Cross join #vwFiscalYears
							 where	DayKey not in (0 ,-1)
									and DayDate >= #vwFiscalYears.FiscalStart
									and DayDate <= #vwFiscalYears.FiscalEnd
							) as Data
				 Where FiscalYearKey != Correction
				)
If @Counter = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.89', 'DataValid_Dimension_Day Computation Validation - FiscalYearKey', 'Pass', Null)
End
Else
Begin
--CCIA-3506 (Corrected 20th April 2015)
	IF @Counter = 6
	and	exists	(	Select		*
					From		test.tblDefects
					Where		DefectID = 'CCIA-3506'
								and [Status] != 'Done')
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.89', 'DataValid_Dimension_Day Computation Validation - FiscalYearKey', 'Pass', 'CCIA-3506')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.89', 'DataValid_Dimension_Day Computation Validation - FiscalYearKey', 'Fail', 'There is an error in the FiscalYearKey Column')
	End
End

--FiscalYearName
Set @Counter =  (
				SELECT	count (*)					
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and FiscalYearName != cast (FiscalYearKey as char(4))
				)
IF @Counter = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.90', 'DataValid_Dimension_Day Computation Validation - FiscalYearName', 'Pass', Null)
End
Else
Begin
	--CCIA-3553
	IF @Counter = 69
	and exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-3553'
							and [Status] != 'Done'
				)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.90', 'DataValid_Dimension_Day Computation Validation - FiscalYearName', 'Pass', 'CCIA-3553')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.90', 'DataValid_Dimension_Day Computation Validation - FiscalYearName', 'Fail', 'There is an error in the FiscalYearName column')
	End
End

--FiscalYearText
IF Not Exists  (
				SELECT	distinct
						DayDate
						, FiscalYearText
						, FiscalYearName
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and FiscalYearText != FiscalYearName
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.91', 'DataValid_Dimension_Day Computation Validation - FiscalYearText', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.91', 'DataValid_Dimension_Day Computation Validation - FiscalYearText', 'Fail', 'There is an error in the FiscalYearText column')
End

--FiscalYearNumber
Set @Counter =  (
				SELECT	Count(*)
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and FiscalYearNumber != FiscalYearKey
				)
If @Counter = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.92', 'DataValid_Dimension_Day Computation Validation - FiscalYearNumber', 'Pass', Null)
End
Else
Begin
	--CCIA-3554
	IF @Counter = 69
	and exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-3554'
							and [Status] != 'Done'
				)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.92', 'DataValid_Dimension_Day Computation Validation - FiscalYearNumber', 'Pass', 'CCIA-3554')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.92', 'DataValid_Dimension_Day Computation Validation - FiscalYearNumber', 'Fail', 'There is an error in the FiscalYearNumber column')
	End
End

--FiscalYearStartDate
Set @Counter = (select 	count (*)
				from	#DimensionDay
						inner join #vwFiscalYears on #DimensionDay.FiscalYearKey = #vwFiscalYears.[Year]
				Where	DayKey not in (0 ,-1)
						
						and FiscalYearStartDate != #vwFiscalYears.FiscalStart
				)
IF @Counter = 0 
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.93', 'DataValid_Dimension_Day Computation Validation - FiscalYearStartDate', 'Pass', Null)
End
Else
Begin
	--CCIA-3506 (Corrected 22nd April 2015)
	IF @Counter = 6
	and	exists	(	Select		*
					From		test.tblDefects
					Where		DefectID = 'CCIA-3506'
								and [Status] != 'Done')
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.93', 'DataValid_Dimension_Day Computation Validation - FiscalYearStartDate', 'Pass', 'CCIA-3506')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.93', 'DataValid_Dimension_Day Computation Validation - FiscalYearStartDate', 'Fail', 'There is an error in the FiscalYearStartDate column')
	End
End

Set @Counter = 	(SELECT	count (*)
				 From	#DimensionDay
						cross join #vwFiscalYears
				 where	DayKey not in (0 ,-1)
						and DayDate >= #vwFiscalYears.FiscalStart
						and DayDate <= #vwFiscalYears.FiscalEnd
						and #DimensionDay.FiscalYearStartDate != #vwFiscalYears.FiscalStart
				)
IF @Counter = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.93a', 'DataValid_Dimension_Day Computation Validation - FiscalYearStartDate', 'Pass', Null)
End
Else
Begin
	--CCIA-3538
	IF @Counter = 6
	and exists		(Select		*
						From		test.tblDefects
						Where		DefectID = 'CCIA-3538'
								and [Status] != 'Done'
					)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.93a', 'DataValid_Dimension_Day Computation Validation - FiscalYearStartDate', 'Pass', 'CCIA-3537')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.93a', 'DataValid_Dimension_Day Computation Validation - FiscalYearStartDate', 'Fail', 'There is an error in the FiscalYearStartDate Column')
	End
End

--FiscalYearEndDate
set @Counter = (select 	count (*)
				from	#DimensionDay
						inner join #vwFiscalYears on #DimensionDay.FiscalYearKey = #vwFiscalYears.[Year]
				Where	DayKey not in (0 ,-1)
						and FiscalYearEndDate != #vwFiscalYears.FiscalEnd
				)
IF @Counter = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.94', 'DataValid_Dimension_Day Computation Validation - FiscalYearEndDate', 'Pass', Null)
End
Else
Begin
	--CCIA-3555 
	if @Counter = 6
	and exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-3555'
							and [Status] != 'Done'
			)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.94', 'DataValid_Dimension_Day Computation Validation - FiscalYearEndDate', 'Pass', 'CCIA-3503')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.94', 'DataValid_Dimension_Day Computation Validation - FiscalYearEndDate', 'Fail', 'There is an error in the FiscalYearEndDate column')
	End
End

--FiscalYearDays
IF Not Exists  (SELECT	distinct
						DayDate
						, FiscalYearDays
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and FiscalYearDays not in (364,(364+7))					
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.95', 'DataValid_Dimension_Day Computation Validation - FiscalYearDays', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.95', 'DataValid_Dimension_Day Computation Validation - FiscalYearDays', 'Fail', 'There is an error in the FiscalYearDays column')
End

Set @Counter =   (SELECT	count (*)
				  From		#DimensionDay
				  where		DayKey not in (0 ,-1)
							and FiscalYearDays != DateDiff (Day, FiscalYearStartDate, FiscalYearEndDate) + 1				
				)
IF @Counter = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.95a', 'DataValid_Dimension_Day Computation Validation - FiscalYearDays', 'Pass', Null)
End
Else
Begin
	--CCIA-3495 (Corrected 20th April 2015)
	IF @Counter = 1984
	and	exists	(	Select		*
					From		test.tblDefects
					Where		DefectID = 'CCIA-3495'
								and [Status] != 'Done') 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.95a', 'DataValid_Dimension_Day Computation Validation - FiscalYearDays', 'Pass', 'CCIA-3495')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.95a', 'DataValid_Dimension_Day Computation Validation - FiscalYearDays', 'Fail', 'There is an error in the FiscalYearDays column')
	End
End

--FiscalYearWeeks
IF Not Exists  (SELECT	distinct
						DayDate
						, FiscalYearWeeks
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and FiscalYearWeeks not in (52,53)			
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.96', 'DataValid_Dimension_Day Computation Validation - FiscalYearWeeks', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.96', 'DataValid_Dimension_Day Computation Validation - FiscalYearWeeks', 'Fail', 'There is an error in the FiscalYearWeeks column')
End

IF Not Exists  (SELECT	distinct
						DayDate
						, FiscalYearWeeks
						, FiscalYearDays
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						
						and FiscalYearWeeks != 52
						and FiscalYearDays != 364
						and not (FiscalYearWeeks = 53 and FiscalYearDays = (364+7))
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.96a', 'DataValid_Dimension_Day Computation Validation - FiscalYearWeeks', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.96a', 'DataValid_Dimension_Day Computation Validation - FiscalYearWeeks', 'Fail', 'There is an error in the FiscalYearWeeks column')
End

--PreviousDayKey
IF Not Exists  (
				SELECT	DayKey
						, PreviousDayKey
						, Replace (DateAdd(Day, -1, DayDate), '-', '') as Correction
				From	#DimensionDay
				where	DayKey not in (0 ,-1)					
						and PreviousDayKey != Replace (DateAdd(Day, -1, DayDate), '-', '')
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.97', 'DataValid_Dimension_Day - PreviousDayKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.97', 'DataValid_Dimension_Day - PreviousDayKey', 'Fail', 'There is an error in the PreviousDayKey column')
End

--PreviousWeekKey
IF Not Exists  (
				SELECT	CurrentDay.DayKey
						, CurrentDay.PreviousWeekKey
						, PreviousDay.WeekKey as Correction
				From	#DimensionDay as CurrentDay
						inner join #DimensionDay as PreviousDay on DateAdd(Day, -1, CurrentDay.WeekStartDate) = PreviousDay.DayDate 
				where	CurrentDay.DayKey not in (0 ,-1)				
						and CurrentDay.PreviousWeekKey != PreviousDay.WeekKey
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.98', 'DataValid_Dimension_Day - PreviousWeekKey', 'Pass', Null)
End
Else
Begin
	--CCIA-4047
	IF exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-4047'
							and [Status] != 'Done'
				)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.98', 'DataValid_Dimension_Day - PreviousWeekKey', 'Pass', 'CCIA-4047')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.98', 'DataValid_Dimension_Day - PreviousWeekKey', 'Fail', 'There is an error in the PreviousWeekKey column')
	End
End

--PreviousWeekDayKey
IF Not Exists  (
				SELECT	DayKey
						, PreviousWeekDayKey
						, Replace (DateAdd(Day, -7, DayDate), '-', '') as Correction
				From	#DimensionDay
				where	DayKey not in (0 ,-1)					
						and PreviousWeekDayKey != Replace (DateAdd(Day, -7, DayDate), '-', '')
						and DayKey > 19900107
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.99', 'DataValid_Dimension_Day - PreviousWeekDayKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.99', 'DataValid_Dimension_Day - PreviousWeekDayKey', 'Fail', 'There is an error in the PreviousWeekDayKey column')
End

--PreviousMonthKey
IF Not Exists  (
				SELECT	CurrentDay.DayKey
						, CurrentDay.PreviousMonthKey
						, PreviousDay.MonthKey as Correction
				From	#DimensionDay as CurrentDay
						inner join #DimensionDay as PreviousDay on DateAdd(Day, -1, CurrentDay.MonthStartDate) = PreviousDay.DayDate 
				where	CurrentDay.DayKey not in (0 ,-1)				
						and CurrentDay.PreviousMonthKey != PreviousDay.MonthKey
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.100', 'DataValid_Dimension_Day - PreviousMonthKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.100', 'DataValid_Dimension_Day - PreviousMonthKey', 'Fail', 'There is an error in the PreviousMonthKey column')
End

--PreviousMonthDayKey
IF Not Exists  (
				SELECT	DayKey
						, PreviousMonthDayKey
						, Replace (DateAdd(Month, -1, DayDate), '-', '') as Correction
				From	#DimensionDay
				where	DayKey not in (0 ,-1)					
						and PreviousMonthDayKey != Replace (DateAdd(Month, -1, DayDate), '-', '')
						and DayKey > 19900131
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.101', 'DataValid_Dimension_Day - PreviousMonthDayKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.101', 'DataValid_Dimension_Day - PreviousMonthDayKey', 'Fail', 'There is an error in the PreviousMonthDayKey column')

End

--PreviousQuarterKey
IF Not Exists  (
				SELECT	CurrentDay.DayKey
						, CurrentDay.PreviousQuarterKey
						, PreviousDay.QuarterKey as Correction
				From	#DimensionDay as CurrentDay
						inner join #DimensionDay as PreviousDay on DateAdd(Day, -1, CurrentDay.QuarterStartDate) = PreviousDay.DayDate 
				where	CurrentDay.DayKey not in (0 ,-1)				
						and CurrentDay.PreviousQuarterKey != PreviousDay.QuarterKey
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.102', 'DataValid_Dimension_Day - PreviousQuarterKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.102', 'DataValid_Dimension_Day - PreviousQuarterKey', 'Fail', 'There is an error in the PreviousQuarterKey column')
End

--PreviousQuarterMonthKey
IF Not Exists  (Select	*
				From	
						(SELECT	CurrentDay.DayKey
								, CurrentDay.MonthOfYearNumber
								, CurrentDay.PreviousQuarterMonthKey
								, 'Correction' = CASE 
									When MonthOfYearNumber in (1,2,3)  then Cast (YearKey -1 as Char(4)) + Right ('0' + Cast ((MonthOfYearNumber + 9) as varchar(2)), 2)
									Else Cast (YearKey as Char(4)) + Right ('0' + Cast ((MonthOfYearNumber -3) as varchar(2)), 2)
									End	
						From	#DimensionDay as CurrentDay
						where	CurrentDay.DayKey not in (0 ,-1)
								and daykey > 19900331	
						) as Data
				Where 	PreviousQuarterMonthKey != 	Correction	
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.103', 'DataValid_Dimension_Day - PreviousQuarterMonthKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.103', 'DataValid_Dimension_Day - PreviousQuarterMonthKey', 'Fail', 'There is an error in the PreviousQuarterMonthKey column')
End

--PreviousQuarterDayKey
IF Not Exists  (
				SELECT	DayKey
						, PreviousMonthDayKey
						, Replace (DateAdd(Month, -3, DayDate), '-', '') as Correction
				From	#DimensionDay
				where	DayKey not in (0 ,-1)					
						and PreviousQuarterDayKey != Replace (DateAdd(Month, -3, DayDate), '-', '')
						and daykey > 19900331
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.104', 'DataValid_Dimension_Day - PreviousQuarterDayKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.104', 'DataValid_Dimension_Day - PreviousQuarterDayKey', 'Fail', 'There is an error in the PreviousQuarterDayKey column')
End

--PreviousYearKey
IF Not Exists  (
				SELECT	DayKey
						, PreviousYearKey
						, YearKey -1 as Correction
				From	#DimensionDay
				where	DayKey not in (0 ,-1)					
						and PreviousYearKey != YearKey -1 
						and daykey > 19901231
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.105', 'DataValid_Dimension_Day - PreviousYearKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.105', 'DataValid_Dimension_Day - PreviousYearKey', 'Fail', 'There is an error in the PreviousYearKey column')
End

--PreviousYearQuarterKey
IF Not Exists  (
				SELECT	DayKey
						, PreviousYearQuarterKey
						, Cast (YearKey -1 as char(4)) + Cast (QuarterofYearNumber as varchar(2)) as Correction
				From	#DimensionDay
				where	DayKey not in (0 ,-1)					
						and PreviousYearQuarterKey != Cast (YearKey -1 as char(4)) + Cast (QuarterofYearNumber as varchar(2))
						and daykey > 19901231
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.106', 'DataValid_Dimension_Day - PreviousYearQuarterKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.106', 'DataValid_Dimension_Day - PreviousYearQuarterKey', 'Fail', 'There is an error in the PreviousYearQuarterKey column')
End

--PreviousYearMonthKey
IF Not Exists  (
				SELECT	DayKey
						, PreviousYearMonthKey
						, Cast (YearKey -1 as char(4)) + Right ('0' + Cast (MonthofYearNumber as varchar(2)),2) as Correction
				From	#DimensionDay
				where	DayKey not in (0 ,-1)					
						and PreviousYearMonthKey != Cast (YearKey -1 as char(4)) + Right ('0' + Cast (MonthofYearNumber as varchar(2)),2)
						and daykey > 19901231
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.107', 'DataValid_Dimension_Day - PreviousYearMonthKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.107', 'DataValid_Dimension_Day - PreviousYearMonthKey', 'Fail', 'There is an error in the PreviousYearMonthKey column')
End

--PreviousYearDayKey
IF Not Exists  (
				SELECT	DayKey
						, PreviousYearDayKey
						, Replace (DateAdd(Year, -1, DayDate), '-', '') as Correction
				From	#DimensionDay
				where	DayKey not in (0 ,-1)					
						and PreviousYearDayKey != Replace (DateAdd(Year, -1, DayDate), '-', '')
						and daykey > 19901231
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.108', 'DataValid_Dimension_Day - PreviousYearDayKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.108', 'DataValid_Dimension_Day - PreviousYearDayKey', 'Fail', 'There is an error in the PreviousYearDayKey column')
End

--PreviousTaxQuarterKey
IF Not Exists  (
				SELECT	CurrentDay.DayKey
						, CurrentDay.PreviousTaxQuarterKey
						, PreviousDay.TaxQuarterKey as Correction
				From	#DimensionDay as CurrentDay
						inner join #DimensionDay as PreviousDay on DateAdd(Day, -1, CurrentDay.TaxQuarterStartDate) = PreviousDay.DayDate 
				where	CurrentDay.DayKey not in (0 ,-1)				
						and CurrentDay.PreviousTaxQuarterKey != PreviousDay.TaxQuarterKey
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.109', 'DataValid_Dimension_Day - PreviousTaxQuarterKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.109', 'DataValid_Dimension_Day - PreviousTaxQuarterKey', 'Fail', 'There is an error in the PreviousTaxQuarterKey column')
End

--PreviousTaxQuarterMonthKey
Set @Counter =   (Select	Count (*)
				 From	
						(SELECT	CurrentDay.DayKey
								, CurrentDay.MonthOfYearNumber
								, TaxYearKey
								, CurrentDay.PreviousTaxQuarterMonthKey
								, 'Correction' = CASE 
									When MonthOfYearNumber = 1  then Cast (TaxYearKey as Char(4)) + '04'
									When MonthOfYearNumber = 2  then Cast (TaxYearKey as Char(4)) + '05'
									When MonthOfYearNumber = 3  then Cast (TaxYearKey as Char(4)) + '06'
									When MonthOfYearNumber = 4  then Cast (TaxYearKey as Char(4)) + '07'
									When MonthOfYearNumber = 5  then Cast (TaxYearKey as Char(4)) + '08'
									When MonthOfYearNumber = 6  then Cast (TaxYearKey as Char(4)) + '09'
									When MonthOfYearNumber = 7  then Cast (TaxYearKey as Char(4)) + '10'
									When MonthOfYearNumber = 8  then Cast (TaxYearKey as Char(4)) + '11'
									When MonthOfYearNumber = 9  then Cast (TaxYearKey as Char(4)) + '12'
									When MonthOfYearNumber = 10  then Cast (TaxYearKey+1 as Char(4)) + '01'
									When MonthOfYearNumber = 11  then Cast (TaxYearKey+1 as Char(4)) + '02'
									Else Cast (TaxYearKey+1 as Char(4)) + '03'
									End	
						From	#DimensionDay as CurrentDay
						where	CurrentDay.DayKey not in (0 ,-1)
								and daykey > 19900331	
						) as Data
				Where 	PreviousTaxQuarterMonthKey != Correction	
			) 
If @Counter = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.110', 'DataValid_Dimension_Day - PreviousTaxQuarterMonthKey', 'Pass', Null)
End
Else
Begin
	--CCIA-7666
	IF exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-7666'
							and [Status] != 'Done'
				)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.110', 'DataValid_Dimension_Day - PreviousTaxQuarterMonthKey', 'Pass', 'CCIA-7666')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.110', 'DataValid_Dimension_Day - PreviousTaxQuarterMonthKey', 'Fail', 'There is an error in the PreviousTaxQuarterMonthKey column')
	End
End

--PreviousTaxQuarterDayKey
IF Not Exists  (
				SELECT	CurrentDay.DayKey
						, CurrentDay.PreviousTaxQuarterDayKey
						, PreviousDay.DayKey as Correction
				From	#DimensionDay as CurrentDay
						inner join #DimensionDay as PreviousDay on DateAdd(Month, -3, CurrentDay.DayDate) = PreviousDay.DayDate 
				where	CurrentDay.DayKey not in (0 ,-1)				
						and CurrentDay.PreviousTaxQuarterDayKey != PreviousDay.DayKey
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.111', 'DataValid_Dimension_Day - PreviousTaxQuarterDayKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.111', 'DataValid_Dimension_Day - PreviousTaxQuarterDayKey', 'Fail', 'There is an error in the PreviousTaxQuarterDayKey column')
End

--PreviousTaxYearKey
IF Not Exists  (
				SELECT	DayKey
						, PreviousTaxYearKey
						, TaxYearKey - 1 as Correction
				From	#DimensionDay
				where	DayKey not in (0 ,-1)	
						and daykey > 19901231					
						and PreviousTaxYearKey != TaxYearKey - 1
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.112', 'DataValid_Dimension_Day - PreviousTaxYearKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.112', 'DataValid_Dimension_Day - PreviousTaxYearKey', 'Fail', 'There is an error in the PreviousTaxYearKey column')
End

--PreviousTaxYearQuarterKey
IF Not Exists  (
				SELECT	DayKey
						, PreviousTaxYearQuarterKey
						, Cast (TaxYearKey - 1 as char(4)) + Right (Cast (TaxQuarterKey as char(5)),1) as Correction
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						and daykey > 19901231					
						and PreviousTaxYearQuarterKey != Cast (TaxYearKey - 1 as char(4)) + Right (Cast (TaxQuarterKey as char(5)),1)
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.113', 'DataValid_Dimension_Day - PreviousTaxYearQuarterKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.113', 'DataValid_Dimension_Day - PreviousTaxYearQuarterKey', 'Fail', 'There is an error in the PreviousTaxYearQuarterKey column')
End

--PreviousTaxYearMonthKey
IF Not Exists  (
				SELECT	DayKey
						, PreviousTaxYearMonthKey
						, Cast (YearKey - 1 as char(4)) + Right ('0' + Cast (DatePart(Month,DayDate) as varchar(3)),2)
				From	#DimensionDay
				where	DayKey not in (0 ,-1)	
						and daykey > 19901231				
						and PreviousTaxYearMonthKey != Cast (YearKey - 1 as char(4)) + Right ('0' + Cast (DatePart(Month,DayDate) as varchar(3)),2)
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.114', 'DataValid_Dimension_Day - PreviousTaxYearMonthKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.114', 'DataValid_Dimension_Day - PreviousTaxYearMonthKey', 'Fail', 'There is an error in the PreviousTaxYearMonthKey column')
End

--PreviousTaxYearDayKey
IF Not Exists  (
				SELECT	DayKey
						, PreviousTaxYearDayKey
						, Replace (DateAdd(Year, -1, DayDate), '-', '') as Correction
				From	#DimensionDay
				where	DayKey not in (0 ,-1)
						and daykey > 19901231					
						and PreviousTaxYearDayKey != Replace (DateAdd(Year, -1, DayDate), '-', '')
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.115', 'DataValid_Dimension_Day - PreviousTaxYearDayKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.115', 'DataValid_Dimension_Day - PreviousTaxYearDayKey', 'Fail', 'There is an error in the PreviousTaxYearDayKey column')
End

--PreviousFiscalWeekKey
Set @Counter =   (
				SELECT	Count (*)
				From	#DimensionDay as CurrentDay
						inner join #DimensionDay as PreviousDay on DateAdd(Day, -1, CurrentDay.FiscalWeekStartDate) = PreviousDay.DayDate 
				where	CurrentDay.DayKey not in (0 ,-1)				
						and CurrentDay.PreviousFiscalWeekKey != PreviousDay.FiscalWeekKey
			)
If @Counter = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.116', 'DataValid_Dimension_Day - PreviousFiscalWeekKey', 'Pass', Null)
End
Else
Begin
	--CCIA-7666
	IF exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-7666'
							and [Status] != 'Done'
				)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.116', 'DataValid_Dimension_Day - PreviousFiscalWeekKey', 'Pass', 'CCIA-7666')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.116', 'DataValid_Dimension_Day - PreviousFiscalWeekKey', 'Fail', 'There is an error in the PreviousFiscalWeekKey column')
	End
End

--PreviousFiscalWeekDayKey
IF Not Exists  (
				SELECT	DayKey
						, PreviousFiscalWeekDayKey
						, Replace (DateAdd(Day, -7, DayDate), '-', '') as Correction
				From	#DimensionDay
				where	DayKey not in (0 ,-1)					
						and PreviousFiscalWeekDayKey != Replace (DateAdd(Day, -7, DayDate), '-', '')
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.117', 'DataValid_Dimension_Day - PreviousFiscalWeekDayKey', 'Pass', Null)
End
Else
Begin
	--CCIA-7666
	IF exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-7666'
							and [Status] != 'Done'
				)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.117', 'DataValid_Dimension_Day - PreviousFiscalWeekDayKey', 'Pass', 'CCIA-7666')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.117', 'DataValid_Dimension_Day - PreviousFiscalWeekDayKey', 'Fail', 'There is an error in the PreviousFiscalWeekDayKey column')
	End
End

--PreviousFiscalMonthKey
IF Not Exists  (
				SELECT	CurrentDay.DayKey
						, CurrentDay.FiscalMonthKey
						, CurrentDay.FiscalMonthStartDate
						, CurrentDay.PreviousFiscalMonthKey
						, PreviousDay.FiscalMonthKey as Correction
				From	#DimensionDay as CurrentDay
						inner join #DimensionDay as PreviousDay on DateAdd(Day, -1, CurrentDay.FiscalMonthStartDate) = PreviousDay.Daydate
				where	CurrentDay.DayKey not in (0 ,-1)				
						and CurrentDay.PreviousFiscalMonthKey != PreviousDay.FiscalMonthKey
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.118', 'DataValid_Dimension_Day - PreviousFiscalMonthKey', 'Pass', Null)
End
Else
Begin
	--CCIA-7666
	IF exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-7666'
							and [Status] != 'Done'
				)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.118', 'DataValid_Dimension_Day - PreviousFiscalMonthKey', 'Pass', 'CCIA-7666')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.118', 'DataValid_Dimension_Day - PreviousFiscalMonthKey', 'Fail', 'There is an error in the PreviousFiscalMonthKey column')
	End
End

--PreviousFiscalMonthDayKey
IF Not Exists  (
				SELECT	DayKey
						, PreviousFiscalMonthDayKey
						, Replace (DateAdd(month, -1, DayDate), '-', '') as Correction
				From	#DimensionDay
				where	DayKey not in (0 ,-1)					
						and PreviousFiscalMonthDayKey != Replace (DateAdd(month, -1, DayDate), '-', '')
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.119', 'DataValid_Dimension_Day - PreviousFiscalMonthDayKey', 'Pass', Null)
End
Else
Begin
	--CCIA-7666
	IF exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-7666'
							and [Status] != 'Done'
				)
	Begin
	Insert into test.tblResults Values (@TestRunNo, '36.119', 'DataValid_Dimension_Day - PreviousFiscalMonthDayKey', 'Pass', 'CCIA-7666')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.119', 'DataValid_Dimension_Day - PreviousFiscalMonthDayKey', 'Fail', 'There is an error in the PreviousFiscalMonthDayKey column')
	End
End

--PreviousFiscalQuarterKey
IF Not Exists  (
				SELECT	CurrentDay.DayKey
						, CurrentDay.PreviousFiscalQuarterKey
						, PreviousDay.FiscalQuarterKey as Correction
				From	#DimensionDay as CurrentDay
						inner join #DimensionDay as PreviousDay on PreviousDay.DayDate = DateAdd(Day, -1, CurrentDay.FiscalQuarterStartDate) 
				where	CurrentDay.DayKey not in (0 ,-1)				
						and CurrentDay.PreviousFiscalQuarterKey != PreviousDay.FiscalQuarterKey
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.120', 'DataValid_Dimension_Day - PreviousFiscalQuarterKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.120', 'DataValid_Dimension_Day - PreviousFiscalQuarterKey', 'Fail', 'There is an error in the PreviousFiscalQuarterKey column')
End

--PreviousFiscalQuarterMonthKey
Set @Counter =  (Select	Count(*)
				From	(
						SELECT	DayKey
								, PreviousFiscalQuarterMonthKey
								, FiscalMonthKey
								, 'Correction' = CASE
									When Right (FiscalMonthKey, 2) <= 3 then Cast (FiscalYearKey -1 as char(4)) + Right ('0' + Cast (Right (FiscalMonthKey, 2) + 9 as varchar(2)),2)
									else Cast (FiscalYearKey as char(4)) + Right ('0' + Cast (Right (FiscalMonthKey, 2) - 3 as varchar(2)),2)
									end
						From	#DimensionDay
						where	DayKey not in (0 ,-1)					
						) as Data
				Where PreviousFiscalQuarterMonthKey != Correction
				)
IF @Counter = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.121', 'DataValid_Dimension_Day - PreviousFiscalQuarterMonthKey', 'Pass', Null)
End
Else
Begin
	--CCIA-7666
	IF exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-7666'
							and [Status] != 'Done'
				)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.121', 'DataValid_Dimension_Day - PreviousFiscalQuarterMonthKey', 'Pass', 'CCIA-7666')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.121', 'DataValid_Dimension_Day - PreviousFiscalQuarterMonthKey', 'Fail', 'There is an error in the PreviousFiscalQuarterMonthKey column')
	End
End

--PreviousFiscalQuarterDayKey
Set @Counter  =  (Select	Count(*)
				From	(
						SELECT	DayKey
								, FiscalYearDays
								, DayOfFiscalYearNumber
								, PreviousFiscalQuarterDayKey
								, 'Correction' = CASE
									When DayOfFiscalYearNumber  >= 365 then 0
									else Replace (DateAdd(Day,-91,daydate), '-', '')
									end
						From	#DimensionDay
						where	DayKey not in (0 ,-1)										
						) as Data		
				Where	PreviousFiscalQuarterDayKey != Correction
				)
If @Counter = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.122', 'DataValid_Dimension_Day - PreviousFiscalQuarterDayKey', 'Pass', Null)
End
Else
Begin
	--CCIA-7666
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-7666'
							and [Status] != 'Done'
				)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.122', 'DataValid_Dimension_Day - PreviousFiscalQuarterDayKey', 'Pass', 'CCIA-7666')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.122', 'DataValid_Dimension_Day - PreviousFiscalQuarterDayKey', 'Fail', 'There is an error in the PreviousFiscalQuarterDayKey column')
	End
End

--PreviousFiscalYearKey
IF Not Exists  (
				SELECT	DayKey
						, PreviousFiscalYearKey
						, FiscalYearKey +1 as Correction
				From	#DimensionDay
				where	DayKey not in (0 ,-1)					
						and PreviousFiscalYearKey != FiscalYearKey -1
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.123', 'DataValid_Dimension_Day - PreviousFiscalYearKey', 'Pass', Null)
End
Else
Begin
--CCIA-7666
	IF exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-7666'
							and [Status] != 'Done'
				)
	Begin
	Insert into test.tblResults Values (@TestRunNo, '36.123', 'DataValid_Dimension_Day - PreviousFiscalYearKey', 'Pass', 'CCIA-7666')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.123', 'DataValid_Dimension_Day - PreviousFiscalYearKey', 'Fail', 'There is an error in the PreviousFiscalYearKey column')
	End
End

--PreviousFiscalYearQuarterKey
IF Not Exists  (
				SELECT	DayKey
						, PreviousFiscalYearQuarterKey
						, Cast (FiscalYearKey -1 as char(4)) + Right (FiscalQuarterKey,1) as Correction
				From	#DimensionDay
				where	DayKey not in (0 ,-1)					
						and PreviousFiscalYearQuarterKey != Cast (FiscalYearKey -1 as char(4)) + Right (FiscalQuarterKey,1)
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.124', 'DataValid_Dimension_Day - PreviousFiscalYearQuarterKey', 'Pass', Null)
End
Else
Begin
--CCIA-7666
	IF exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-7666'
							and [Status] != 'Done'
				)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.124', 'DataValid_Dimension_Day - PreviousFiscalYearQuarterKey', 'Pass', 'CCIA-7666')
	End
	else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.124', 'DataValid_Dimension_Day - PreviousFiscalYearQuarterKey', 'Fail', 'There is an error in the PreviousFiscalYearQuarterKey column')
	End
End

--PreviousFiscalYearMonthKey
Set @Counter =   (
						SELECT	Count(*)
						From	#DimensionDay
						where	DayKey not in (0 ,-1)	
								and PreviousFiscalYearMonthKey != 	Cast (FiscalYearKey -1 as char(4)) + Cast (Right (FiscalMonthKey, 2) as char(2))			
				)
If @Counter = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.125', 'DataValid_Dimension_Day - PreviousFiscalYearMonthKey', 'Pass', Null)
End
Else
Begin
	--CCIA-7666
	IF exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-7666'
							and [Status] != 'Done'
				)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.125', 'DataValid_Dimension_Day - PreviousFiscalYearMonthKey', 'Pass', 'CCIA-7666')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.125', 'DataValid_Dimension_Day - PreviousFiscalYearMonthKey', 'Fail', 'There is an error in the PreviousFiscalYearMonthKey column')
	End
End

--PreviousFiscalYearDayKey
IF Not Exists  (
				SELECT	DayKey
						, PreviousFiscalYearDayKey
						, Replace (DateAdd(year, -1, DayDate), '-', '') as Correction
				From	#DimensionDay
				where	DayKey not in (0 ,-1)					
						and PreviousFiscalYearDayKey != Replace (DateAdd(year, -1, DayDate), '-', '')
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '36.126', 'DataValid_Dimension_Day - PreviousFiscalYearDayKey', 'Pass', Null)
End
Else
Begin
--CCIA-7666
	IF exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-7666'
							and [Status] != 'Done'
				)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.126', 'DataValid_Dimension_Day - PreviousFiscalYearDayKey', 'Pass', 'CCIA-7666')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '36.126', 'DataValid_Dimension_Day - PreviousFiscalYearDayKey', 'Fail', 'There is an error in the PreviousFiscalYearDayKey column')
	End
End

--NextDay etc has been hidden from the views and is being removed

--Metadata n/a
--CreatedDate
--CreatedBatchKey
--CreatedBy
--ModifiedDate
--ModifiedBatchKey
--ModifiedBy
GO



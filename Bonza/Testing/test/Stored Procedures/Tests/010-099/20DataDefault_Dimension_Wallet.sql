﻿--Test Code
--exec Test.[20DataDefault_Dimension_Wallet] 'ADHOC'
Create Procedure Test.[20DataDefault_Dimension_Wallet] @TestRunNo varchar(23)

as
If exists(
		Select		WalletKey
		From		[$(Dimensional)].[Dimension].Wallet
		where		WalletKey = 0
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '20', 'DataDefault_Dimension_Wallet Table ID0', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '20', 'DataDefault_Dimension_Wallet Table ID0', 'Fail', 'No ID 0 record found')
End

If exists(
		Select		WalletKey
		From		[$(Dimensional)].[Dimension].Wallet
		where		WalletKey = -1
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '20.1', 'DataDefault_Dimension_Wallet Table ID-1', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '20.1', 'DataDefault_Dimension_Wallet Table ID-1', 'Fail', 'No ID -1 record found')
End






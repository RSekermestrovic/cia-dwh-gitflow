﻿--Test Code
--exec Test.[92DataDefault_Dimension_RevenueLifecycle] 'ADHOC'
Create Procedure Test.[92DataDefault_Dimension_RevenueLifecycle] @TestRunNo varchar(23)

as

If exists(
		Select		RevenueLifecycleKey
		From		[$(Dimensional)].[Dimension].[RevenueLifecycle]
		where		RevenueLifecycleKey = 0
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '92', 'DataDefault_Dimension_RevenueLifecycle Table ID0', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '92', 'DataDefault_Dimension_Lifecycle Table ID0', 'Fail', 'No ID 0 record found')
End

If exists(
		Select		RevenueLifecycleKey
		From		[$(Dimensional)].[Dimension].[RevenueLifecycle]
		where		RevenueLifecycleKey = -1
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '92.1', 'DataDefault_Dimension_RevenueLifecycle Table ID-1', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '92.1', 'DataDefault_Dimension_RevenueLifecycle Table ID-1', 'Fail', 'No ID -1 record found')
End



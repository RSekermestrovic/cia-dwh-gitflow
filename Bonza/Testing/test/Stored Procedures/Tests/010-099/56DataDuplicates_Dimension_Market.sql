﻿--Test Code
--exec Test.[56DataDuplicates_Dimension_Market] 'ADHOC'

Create Procedure Test.[56DataDuplicates_Dimension_Market] @TestRunNo varchar(23), @FromDate datetime

as

Declare @ErrorText varchar(100)
If @FromDate is not null
Begin
Set @ErrorText = ' since ' + Convert (varchar(20), @FromDate, 103)
End
else
Begin
Set @ErrorText = '' 
End

declare @Counter int
Set @Counter = 
				(
				Select	Count (*)
				From	(
						Select		Market.MarketId
						From		[$(Dimensional)].Dimension.Market as Market
						where		(
										(Market.FromDate > @FromDate)
										or (@FromDate is null)
									)
									and marketkey not in (0, -1)
						Group by	Market.MarketId
						Having		Count(*) > 1
						) as Data			
				) 
If @Counter = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, '56', 'DataDuplicates_Dimension_Market', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '56', 'DataDuplicates_Dimension_Market', 'Fail', 'Duplicates of MarketID found' + @ErrorText)
End

﻿--Test Code
--exec Test.[85DataCurrent_Dimension_Class] 'ADHOC'
Create Procedure Test.[85DataCurrent_Dimension_Class] @TestRunNo varchar(23)

as

Declare	@BatchDate datetime2
Set	@BatchDate = (Select Max(BatchFromDate) From [$(Staging)].[Control].ETLController Where DimensionStatus = 1)

Declare @LastDate varchar(10)
Set		@LastDate = 
					(Select		max (ModifiedDate)
					From		[$(Dimensional)].[Dimension].[Class]
					)

If @LastDate >= @BatchDate or @LastDate >= '2015-10-23'
Begin
	Insert into test.tblResults Values (@TestRunNo, '85', 'DataCurrent_Dimension_Class Table', 'Pass', Null)
End
Else
Begin
	
	Insert into test.tblResults Values (@TestRunNo, '85', 'DataCurrent_Dimension_Class Table', 'Fail', 'The last update in the dimension is' + cast (@LastDate as varchar(10)) + ' expect an update every other month')
End
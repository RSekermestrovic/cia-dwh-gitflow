﻿--Test Code
--exec Test.[58DataDuplicates_Dimension_Account] 'ADHOC',  null

Create Procedure Test.[58DataDuplicates_Dimension_Account] @TestRunNo varchar(23), @FromDate datetime

as

Declare @ErrorText varchar(100)
If @FromDate is not null
Begin
Set @ErrorText = ' since ' + Convert (varchar(20), @FromDate, 103)
End
else
Begin
Set @ErrorText = '' 
End


If not exists	(Select		Count(*)
				From		[$(Dimensional)].Dimension.Account as Account
				where		@FromDate is null
							or CreatedDate >= @FromDate
				Group by	LedgerSource, LedgerID
				Having		Count(*) > 1
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '58', 'DataDuplicates_Dimension_Account', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '58', 'DataDuplicates_Dimension_Account', 'Fail', 'Duplicates of AccountID found' + @ErrorText)
End
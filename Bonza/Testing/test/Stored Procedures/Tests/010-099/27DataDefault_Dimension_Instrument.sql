﻿--Test Code
--exec Test.[27DataDefault_Dimension_Instrument] 'ADHOC'
Create Procedure Test.[27DataDefault_Dimension_Instrument] @TestRunNo varchar(23)

as

If exists(
		Select		InstrumentKey
		From		[$(Dimensional)].[Dimension].[Instrument]
		where		InstrumentKey = 0
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '27', 'DataDefault_Dimension_Instrument Table ID0', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '27', 'DataDefault_Dimension_Instrument Table ID0', 'Fail', 'No ID 0 record found')
End

If exists(
		Select		InstrumentKey
		From		[$(Dimensional)].[Dimension].[Instrument]
		where		InstrumentKey = -1
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '27.1', 'DataDefault_Dimension_Instrument Table ID-1', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '27.1', 'DataDefault_Dimension_Instrument Table ID-1', 'Fail', 'No ID -1 record found')
End



--Test Code
--exec Test.[18FieldExists_Dimension_BetType] 'ADHOC'
Create Procedure Test.[18FieldExists_Dimension_BetType] @TestRunNo varchar(23)

as

Declare @Database varchar(100)
Set @Database = '[$(Dimensional)]'

Declare @SQL varchar(max)
Set @SQL = 'select top 1
[BetTypeKey]
      ,[BetGroupType]
      ,[BetType]
      ,[BetSubType]
      ,[GroupingType]
      ,[LegType]
      ,[FromDate]
      ,[ToDate]
      ,[FirstDate]
      ,[CreatedDate]
      ,[CreatedBatchKey]
      ,[CreatedBy]
      ,[ModifiedDate]
      ,[ModifiedBatchKey]
      ,[ModifiedBy]
  FROM ' + @Database + '.[Dimension].[BetType]'

Exec Test.spSelectValid @TestRunNo, '18','Structure - Dimension BetType Table', @SQL

If		(
		Select		Count(*)
		From		[$(Dimensional)].sys.all_objects as [Objects]
					inner join [$(Dimensional)].sys.all_columns as [Columns] on [Objects].[object_id] = [Columns].[object_id]
					inner join [$(Dimensional)].sys.schemas as [schemas] on [Objects].[schema_id] = [schemas].[schema_id]
		where		[Objects].name = 'BetType'
					and [Objects].[type] = 'U'
					and [schemas].Name = 'Dimension'
		) = 15
Begin
	Insert into test.tblResults Values (@TestRunNo, '18.1', 'Structure - Dimension BetType Table', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '18.1', 'Structure - Dimension BetType Table', 'Fail', 'Wrong number of columns')
End

--Test Code
--exec Test.[17FieldExists_Dimension_Market] 'ADHOC'
Create Procedure Test.[17FieldExists_Dimension_Market] @TestRunNo varchar(23)

as

Declare @Database varchar(100)
Set @Database = '[$(Dimensional)]'

Declare @SQL varchar(max)
Set @SQL = 'select top 1
	  [MarketKey]
      ,[Source]
      ,[MarketId]
      ,[Market]
      ,[MarketType]
      ,[MarketGroup]
      ,[LevyRate]
      ,[Sport]
      ,[MarketOpen]
      ,[MarketOpenedDateTime]
      ,[MarketOpenedDayKey]
      ,[MarketOpenedVenueDateTime]
      ,[MarketClosed]
      ,[MarketClosedDateTime]
      ,[MarketClosedDayKey]
      ,[MarketClosedVenueDateTime]
      ,[MarketSettled]
      ,[MarketSettledDateTime]
      ,[MarketSettledDayKey]
      ,[MarketSettledVenueDateTime]
      ,[LiveMarket]
      ,[FutureMarket]
      ,[MaxWinnings]
      ,[MaxStake]
      ,[FromDate]
      ,[FirstDate]
      ,[CreatedDate]
      ,[CreatedBatchKey]
      ,[CreatedBy]
      ,[ModifiedDate]
      ,[ModifiedBatchKey]
      ,[ModifiedBy]
  FROM ' + @Database + '.[Dimension].[Market]'

Exec Test.spSelectValid @TestRunNo, '17','Structure - Dimension Market Table', @SQL

If		(
		Select		Count(*)
		From		[$(Dimensional)].sys.all_objects as [Objects]
					inner join [$(Dimensional)].sys.all_columns as [Columns] on [Objects].[object_id] = [Columns].[object_id]
					inner join [$(Dimensional)].sys.schemas as [schemas] on [Objects].[schema_id] = [schemas].[schema_id]
		where		[Objects].name = 'Market'
					and [Objects].[type] = 'U'
					and [schemas].Name = 'Dimension'
		) = 25
Begin
	Insert into test.tblResults Values (@TestRunNo, '17.1', 'Structure - Dimension Market Table', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '17.1', 'Structure - Dimension Market Table', 'Fail', 'Wrong number of columns')
End

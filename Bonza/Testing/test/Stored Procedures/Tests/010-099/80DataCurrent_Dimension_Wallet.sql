﻿--Test Code
--exec Test.[80DataCurrent_Dimension_Wallet] 'ADHOC'
Create Procedure Test.[80DataCurrent_Dimension_Wallet] @TestRunNo varchar(23)

as

--This dimension was changed to be a fixed type in 15.9

If not exists (Select		*
			   From			[$(Dimensional)].Dimension.Wallet
			   Where		CreatedBatchKey != 0
							or ModifiedBatchKey != 0
			  )

Begin
	Insert into test.tblResults Values (@TestRunNo, '80', 'DataCurrent_Dimension_Wallet Table', 'Pass', Null)
End
Else
Begin
	
	Insert into test.tblResults Values (@TestRunNo, '80', 'DataCurrent_Dimension_Wallet Table', 'Fail', 'Changes have been made to a fixed dimension post batch zero')
End


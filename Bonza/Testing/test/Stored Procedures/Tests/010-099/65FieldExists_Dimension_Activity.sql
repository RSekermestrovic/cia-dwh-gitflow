--Test Code
--exec Test.[65FieldExists_Dimension_Activity] 'ADHOC'
Create Procedure Test.[65FieldExists_Dimension_Activity] @TestRunNo varchar(23)

as

Declare @Database varchar(100)
Set @Database = '[$(Dimensional)]'

Declare @SQL varchar(max)
Set @SQL = 'Select top 1 [ActivityKey]
      ,[HasDeposited]
      ,[HasWithdrawn]
      ,[HasBet]
      ,[HasWon]
      ,[HasLost]
      ,[FromDate]
      ,[ToDate]
      ,[FirstDate]
      ,[CreatedDate]
      ,[CreatedBatchKey]
      ,[CreatedBy]
      ,[ModifiedDate]
      ,[ModifiedBatchKey]
      ,[ModifiedBy]
  FROM ' + @Database + '.[Dimension].[Activity]'

Exec Test.spSelectValid @TestRunNo, '65','Structure - Dimension Activity Table', @SQL

If		(
		Select		Count(*)
		From		[$(Dimensional)].sys.all_objects as [Objects]
					inner join [$(Dimensional)].sys.all_columns as [Columns] on [Objects].[object_id] = [Columns].[object_id]
					inner join [$(Dimensional)].sys.schemas as [schemas] on [Objects].[schema_id] = [schemas].[schema_id]
		where		[Objects].name = 'Activity'
					and [Objects].[type] = 'U'
					and [schemas].Name = 'Dimension'
		) = 15
Begin
	Insert into test.tblResults Values (@TestRunNo, '65.1', 'Structure - Dimension Activity Table', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '65.1', 'Structure - Dimension Activity Table', 'Fail', 'Wrong number of columns')
End

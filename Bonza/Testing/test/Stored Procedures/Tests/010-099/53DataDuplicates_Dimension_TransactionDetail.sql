﻿--Test Code
--exec Test.[53DataDuplicates_Dimension_TransactionDetail] 'ADHOC'

Create Procedure Test.[53DataDuplicates_Dimension_TransactionDetail] @TestRunNo varchar(23)

as


If not exists(
		Select		TransactionDetail1.TransactionDetailKey
					, TransactionDetail1.TransactionDetailID
					, TransactionDetail2.TransactionDetailKey
					, TransactionDetail2.TransactionDetailID
		From		[$(Dimensional)].Dimension.TransactionDetail as TransactionDetail1
		inner join	[$(Dimensional)].Dimension.TransactionDetail as TransactionDetail2 on TransactionDetail1.TransactionDetailID = TransactionDetail2.TransactionDetailID
		Where		TransactionDetail1.TransactionDetailKey != TransactionDetail2.TransactionDetailKey
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '53', 'DataDuplicates_Dimension_TransactionDetail', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '53', 'DataDuplicates_Dimension_TransactionDetail', 'Fail', 'Duplicates of TransactionDetailID found')
End


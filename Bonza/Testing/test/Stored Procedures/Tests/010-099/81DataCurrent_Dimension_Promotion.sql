﻿--Test Code
--exec Test.[81DataCurrent_Dimension_Promotion] 'ADHOC'
Create Procedure Test.[81DataCurrent_Dimension_Promotion] @TestRunNo varchar(23)

as

Declare @LastDate datetime2
Set		@LastDate = 
					(Select		max (CreatedDate)
					 From		[$(Dimensional)].[Dimension].Promotion
					)

If @LastDate >= DateAdd (DAY,-2,getdate())
Begin
	Insert into test.tblResults Values (@TestRunNo, '81', 'DataCurrent_Dimension_Promotion Table', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '81', 'DataCurrent_Dimension_Promotion Table', 'Fail', 'The last update in the dimension is ' + cast (@LastDate as varchar(10)) + ' expect an update each day')
End

﻿--Test Code
--exec Test.[74DataCurrent_Dimension_TransactionDetail] 'ADHOC'
Create Procedure Test.[74DataCurrent_Dimension_TransactionDetail] @TestRunNo varchar(23)

as

Select	*
into #DimensionTransactionDetail
From [$(Dimensional)].[Dimension].[TransactionDetail]

Declare @NumberOfRecords int
Set @NumberOfRecords = 
					(
					Select		count(*) as NumberOfRecords
					From		#DimensionTransactionDetail
					)
If @NumberOfRecords = 295
Begin
	Insert into test.tblResults Values (@TestRunNo, '74', 'DataCurrent_Dimension_TransactionDetail Record Count', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '74', 'DataCurrent_Dimension_TransactionDetail Record Count', 'Fail', 'Expected 295 records, ' + Cast (@NumberOfRecords as varchar(5)) + ' found.')
End

IF 					(
					Select		Min (TransactionDetailKey) as Record
					From		#DimensionTransactionDetail
					where		TransactionDetailKey not in (0 ,-1)
					)
					= 3
Begin
	Insert into test.tblResults Values (@TestRunNo, '74.1', 'DataCurrent_Dimension_TransactionDetail Record Count', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '74.1', 'DataCurrent_Dimension_TransactionDetail Record Count', 'Fail', 'The lowest record key is not 3')
End

IF 					(
					Select		Max (TransactionDetailKey) as Record
					From		#DimensionTransactionDetail
					where		TransactionDetailKey not in (0 ,-1)
					)
					= 329
Begin
	Insert into test.tblResults Values (@TestRunNo, '74.2', 'DataCurrent_Dimension_TransactionDetail Record Count', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '74.2', 'DataCurrent_Dimension_TransactionDetail Record Count', 'Fail', 'The highest record key is not 329')
End


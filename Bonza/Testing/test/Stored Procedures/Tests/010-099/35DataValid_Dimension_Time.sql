﻿--Test Code
--exec Test.[35DataValid_Dimension_Time] 'ADHOC'
Create Procedure [Test].[35DataValid_Dimension_Time] @TestRunNo varchar(23)

as

Select	*
into #DimensionTime
From [$(Dimensional)].[Dimension].[Time]

--TimeKey n/a as SequentialKey
--TimeID
--TimeText
IF Not Exists  (SELECT	[timekey]
				From	#DimensionTime
				where	timekey not in (0 ,-1)
						and TimeID != TimeText
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '35.1', 'DataValid_Dimension_Time Computation Validation - TimeText', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '35.1', 'DataValid_Dimension_Time Computation Validation - TimeText', 'Fail', 'There is an error in the TimeText column')
End

--Time
--TimeStartTime
IF Not Exists	(SELECT	[timekey]
				 From	#DimensionTime
				 where	timekey not in (0 ,-1)
						and [Time] != TimeStartTime
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '35.3', 'DataValid_Dimension_Time Computation Validation - TimeStartTime', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '35.3', 'DataValid_Dimension_Time Computation Validation - TimeStartTime', 'Fail', 'There is an error in the TimeStartTime column')
End

--TimeEndTime
IF Not Exists	(SELECT	[timekey]
				 From	#DimensionTime
				 where	timekey not in (0 ,-1)
						and DateAdd (SECOND,59,[Time]) != TimeEndTime
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '35.4', 'DataValid_Dimension_Time Computation Validation - TimeEndTime', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '35.4', 'DataValid_Dimension_Time Computation Validation - TimeEndTime', 'Fail', 'There is an error in the TimeEndTime column')
End

--TimeStartTimestamp
--TimeEndTimestamp
--TimeOfHourNumber
IF Not Exists	(SELECT	[timekey]
				 From	#DimensionTime
				 where	timekey not in (0 ,-1)
						and cast (right (TimeID,2) as int) +1 != timeofhournumber
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '35.7', 'DataValid_Dimension_Time Computation Validation - TimeOfHourNumber', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '35.7', 'DataValid_Dimension_Time Computation Validation - TimeOfHourNumber', 'Fail', 'There is an error in the TimeOfHourNumber column')
End

--TimeOfDayNumber
IF Not Exists	(SELECT	[timekey]
				 From	#DimensionTime
				 where	timekey not in (0 ,-1)
						and (cast (Left (TimeID,2) as int)) *60 + cast (right (TimeID,2) as int) +1 != timeofdaynumber
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '35.8', 'DataValid_Dimension_Time Computation Validation - TimeOfDayNumber', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '35.8', 'DataValid_Dimension_Time Computation Validation - TimeOfDayNumber', 'Fail', 'There is an error in the TimeOfDayNumber column')
End

--HourKey
--HourID
IF Not Exists	(SELECT	[timekey]
				 From	#DimensionTime
				 where	timekey not in (0 ,-1)
						and left (TimeID,2) != HourID
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '35.10', 'DataValid_Dimension_Time Computation Validation - HourID', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '35.10', 'DataValid_Dimension_Time Computation Validation - HourID', 'Fail', 'There is an error in the HourID column')
End

--HourText
IF Not Exists	(SELECT	[timekey]
				 From	#DimensionTime
				 where	timekey not in (0 ,-1)
						and left (TimeID,2) != HourText
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '35.11', 'DataValid_Dimension_Time Computation Validation - HourText', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '35.11', 'DataValid_Dimension_Time Computation Validation - HourText', 'Fail', 'There is an error in the HourText column')
End

--HourStartTime
IF Not Exists	(SELECT	[timekey]
				 From	#DimensionTime
				 where	timekey not in (0 ,-1)
						and 
							(Left (HourStartTime, 2) != Left (TimeID,2)
							 or Right (HourStartTime,6) != ':00:00'
							)
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '35.12', 'DataValid_Dimension_Time Computation Validation - HourStartTime', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '35.12', 'DataValid_Dimension_Time Computation Validation - HourStartTime', 'Fail', 'There is an error in the HourStartTime column')
End

--HourEndTime
IF Not Exists	(SELECT	[timekey]
				 From	#DimensionTime
				 where	timekey not in (0 ,-1)
						and 
							(Left (HourEndTime, 2) != Left (TimeID,2)
							 or Right (HourEndTime,6) != ':59:59'
							)
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '35.13', 'DataValid_Dimension_Time Computation Validation - HourEndTime', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '35.13', 'DataValid_Dimension_Time Computation Validation - HourEndTime', 'Fail', 'There is an error in the HourEndTime column')
End

--HourOfDayNumber
if not exists 	(
				Select		HourOfDayNumber
				From		#DimensionTime
				Where		DatePart (Hour, TimeStartTime) != HourOfDayNumber
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '35.14', 'DataValid_Dimension_Time Computation Validation - HourOfDayNumber', 'Pass', NULL)
End
else
Begin
	Insert into test.tblResults Values (@TestRunNo, '35.14', 'DataValid_Dimension_Time Computation Validation - HourOfDayNumber', 'Fail', 'Value incorrect')
End

--PeriodKey

IF Not Exists	(Select	[timekey]
				 From	(
						 SELECT	[timekey]
								, PeriodKey
								, TimeStartTime
								, 'Correction' = CASE
								When TimeStartTime Between '06:00:00' and '11:59:00' then 1
								When TimeStartTime Between '12:00:00' and '17:59:00' then 2
								When TimeStartTime Between '18:00:00' and '22:59:00' then 3
								When TimeStartTime Between '23:00:00' and '23:59:00' then 4
								When TimeStartTime Between '00:00:00' and '05:59:00' then 4
								Else 'ERROR'
								End
						 From	#DimensionTime
						 where	timekey not in (0 ,-1)
						) as Data
				Where PeriodKey != Correction
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '35.15', 'DataValid_Dimension_Time Computation Validation - PeriodKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '35.15', 'DataValid_Dimension_Time Computation Validation - PeriodKey', 'Fail', 'There is an error in the PeriodKey column')
End

--PeriodText
IF Not Exists	(SELECT	[timekey]
				 From	#DimensionTime
				 where	timekey not in (0 ,-1)
						and (
							(PeriodKey = 1 and [PeriodText] != 'Morning')
							or (PeriodKey = 2 and [PeriodText] != 'Afternoon')
							or (PeriodKey = 3 and [PeriodText] != 'Peak')
							or (PeriodKey = 4 and [PeriodText] != 'Midnight')
							or (PeriodKey >= 5)
							Or ([PeriodText] not in ('Morning', 'Afternoon', 'Peak', 'Midnight'))
							)
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '35.16', 'DataValid_Dimension_Time Computation Validation - [PeriodText]', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '35.16', 'DataValid_Dimension_Time Computation Validation - [PeriodText]', 'Fail', 'There is an error in the [PeriodText] column')
End

--FromDate n/a metadata
--ToDate n/a metadata
--FirstDate n/a metadata
--CreatedDate n/a metadata
--CreatedBatchKey n/a metadata
--CreatedBy n/a metadata
--ModifiedDate n/a metadata
--ModifiedBatchKey n/a metadata
--ModifiedBy n/a metadata
﻿--Test Code
--exec Test.[86DataCurrent_Dimension_Market] 'ADHOC'
Create Procedure Test.[86DataCurrent_Dimension_Market] @TestRunNo varchar(23)

as

Declare	@BatchDate datetime2
Set	@BatchDate = (Select Max(BatchFromDate) From [$(Staging)].[Control].ETLController Where DimensionStatus = 1)

Declare @LastDate varchar(10)
Set		@LastDate = 
					(Select		max (CreatedDate)
					From		[$(Dimensional)].[Dimension].[Market]
					)

If @LastDate >= @BatchDate or @LastDate >= DateAdd (HOUR,-12,getdate())
Begin
	Insert into test.tblResults Values (@TestRunNo, '86', 'DataCurrent_Dimension_Market Table', 'Pass', Null)
End
Else
Begin
	
	Insert into test.tblResults Values (@TestRunNo, '86', 'DataCurrent_Dimension_Market Table', 'Fail', 'The last update in the dimension is ' + cast (@LastDate as varchar(10)) + ' expect an update each day')
End



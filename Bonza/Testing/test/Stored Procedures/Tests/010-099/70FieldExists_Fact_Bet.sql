--Test Code
--exec Test.[70FieldExists_Fact_Bet] 'ADHOC'
Create Procedure Test.[70FieldExists_Fact_Bet] @TestRunNo varchar(23)

as

Declare @Database varchar(100)
Set @Database = '[$(Dimensional)]'

Declare @SQL varchar(max)
Set @SQL = 'Select top 1 [PlacedDayKey]
      ,[PlacedTimeKey]
      ,[SelectionKey]
      ,[PriceKey]
      ,[AccountKey]
      ,[WalletKey]
      ,[PromotionKey]
      ,[ChannelKey]
      ,[ContractKey]
      ,[MarketKey]
      ,[PriceTypeKey]
      ,[ClassKey]
      ,[BetTypeKey]
      ,[OriginalBetStatusKey]
      ,[OriginalSettledDayKey]
      ,[BetStatusKey]
      ,[SettledDayKey]
      ,[StakeOriginal]
      ,[StakeAdjustment]
      ,[Stake]
      ,[RefundOriginal]
      ,[RefundAdjustment]
      ,[Refund]
      ,[PayoutOriginal]
      ,[PayoutAdjustment]
      ,[Payout]
      ,[NumberOfSettlements]
      ,[EstimatedFees]
      ,[Fees]
      ,[CreatedDate]
      ,[CreatedBatchKey]
      ,[CreatedBy]
      ,[ModifiedDate]
      ,[ModifiedBatchKey]
      ,[ModifiedBy]
  FROM [' + @Database + '].[Fact].[Bet]'

Exec Test.spSelectValid @TestRunNo, '70','Structure - Fact Bet Table', @SQL

If		(
		Select		Count(*)
		From		[$(Dimensional)].sys.all_objects as [Objects]
					inner join [$(Dimensional)].sys.all_columns as [Columns] on [Objects].[object_id] = [Columns].[object_id]
					inner join [$(Dimensional)].sys.schemas as [schemas] on [Objects].[schema_id] = [schemas].[schema_id]
		where		[Objects].name = 'Bet'
					and [Objects].[type] = 'U'
					and [schemas].Name = 'Fact'
		) = 35
Begin
	Insert into test.tblResults Values (@TestRunNo, '70.1', 'Structure - Fact Bet Table', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '70.1', 'Structure - Fact Bet Table', 'Fail', 'Wrong number of columns')
End

﻿--Test Code
--exec Test.[CCIA-3708] 'ADHOC'
Create Procedure Test.[CCIA-3708] @TestRunNo varchar(23)

as

If not exists 	(
				  SELECT *
				  FROM [$(Dimensional)].[Dimension].[BetType]
				  where BetGroupType = 'Standard'
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, 'CCIA-3708a', 'CCIA-3708', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, 'CCIA-3708a', 'CCIA-3708', 'Fail', 'Standard appears in BetGroupType')
End

If exists 	(
			SELECT Count(*)  --should be > zero
			FROM [$(Dimensional)].[Dimension].[BetType]
			where BetGroupType = 'Straight'
			) 
Begin
	Insert into test.tblResults Values (@TestRunNo, 'CCIA-3708b', 'CCIA-3708', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, 'CCIA-3708b', 'CCIA-3708', 'Fail', 'Straight does not appear in BetGroupType')
End

If not exists 	(
				  SELECT Count(*)  --should be zero (error there is one)
				  FROM [$(Dimensional)].[Dimension].[BetType]
				  where GroupingType = 'Straight'
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, 'CCIA-3708c', 'CCIA-3708', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-5553'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, 'CCIA-3708c', 'CCIA-3708', 'Pass', 'CCIA-5553')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, 'CCIA-3708c', 'CCIA-3708', 'Fail', 'Straight appears in GroupingType')
	End
End

If exists 	(
			  SELECT Count(*)  --should be > zero
			  FROM [$(Dimensional)].[Dimension].[BetType]
			  where GroupingType = 'Standard'
			) 
Begin
	Insert into test.tblResults Values (@TestRunNo, 'CCIA-3708d', 'CCIA-3708', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, 'CCIA-3708d', 'CCIA-3708', 'Fail', 'Standard not in GroupingType')
End

If			 	(
				  SELECT Count(*)  --should be zero
				  FROM [$(Dimensional)].[Dimension].[BetType]
				  where BetGroupType = 'Roving'
				) = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, 'CCIA-3708e', 'CCIA-3708', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, 'CCIA-3708e', 'CCIA-3708', 'Fail', 'Roving in BetGroupType')
End

If		   	 (
			  SELECT Count(*)  --should be > zero
			  FROM [$(Dimensional)].[Dimension].[BetType]
			  where GroupingType = 'Roving'
		     ) = 0
Begin
	Insert into test.tblResults Values (@TestRunNo, 'CCIA-3708f', 'CCIA-3708', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-5649'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, 'CCIA-3708f', 'CCIA-3708', 'Pass', 'CCIA-5649')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, 'CCIA-3708f', 'CCIA-3708', 'Fail', 'Roving not in GroupingType')
	End
End
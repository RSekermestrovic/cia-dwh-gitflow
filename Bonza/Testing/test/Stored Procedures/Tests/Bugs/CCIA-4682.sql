﻿--Test Code
--exec Test.[CCIA-4682] 'ADHOC'
Create Procedure Test.[CCIA-4682] @TestRunNo varchar(23)

as

If not exists	(
				Select		*
				FROM [$(Staging)].INFORMATION_SCHEMA.COLUMNS
				WHERE TABLE_NAME = 'ETLController' AND COLUMN_NAME LIKE '%Cube%'
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, 'CCIA-4682', 'CCIA-4682', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, 'CCIA-4682', 'CCIA-4682', 'Fail', 'Cube columns have returned')
End
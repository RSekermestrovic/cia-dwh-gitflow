﻿CREATE PROCEDURE [Test].[CCIA-5072] @TestRunNo varchar(23)

as

If not exists(
		Select		Account.Accountkey
					, AccountStatus.FromDate
					, KPI.MasterTransactionTimestamp 
		From		[$(Staging)].Stage.KPI as KPI
					LEFT OUTER JOIN [$(Dimensional)].Dimension.[Contract] as [Contract] WITH (NOLOCK) ON ([Contract].LegID = KPI.LegID AND [Contract].LegIdSource=KPI.LegIdSource)
					LEFT OUTER JOIN [$(Dimensional)].Dimension.Account as Account WITH (NOLOCK) ON (Account.LedgerId = KPI.LedgerId AND Account.LedgerSource=KPI.LedgerSource)
					LEFT OUTER JOIN [$(Dimensional)].Dimension.AccountStatus as AccountStatus WITH (NOLOCK) ON (AccountStatus.LedgerId = KPI.LedgerId 
																								AND AccountStatus.LedgerSource=KPI.LedgerSource 
																								AND AccountStatus.FromDate <= KPI.MasterTransactionTimestamp 
																								AND AccountStatus.ToDate > KPI.MasterTransactionTimestamp)
		WHERE AccountStatus.FromDate > KPI.MasterTransactionTimestamp 
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, 'CCIA-5072', 'CCIA-5072', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, 'CCIA-5072', 'CCIA-5072', 'Fail', 'Transactions exist before the Account says it was opened')
End



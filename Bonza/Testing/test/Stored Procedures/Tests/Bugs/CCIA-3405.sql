﻿--Test Code
--exec Test.[CCIA-3405] 'ADHOC'
Create Procedure Test.[CCIA-3405] @TestRunNo varchar(23)

as


If not exists  (
				Select		*
				From		(
							 Select TransactionDetailId, BalanceTypeName, MasterTransactionTimestamp, [TransactionId], TransactedAmount
							 from [$(Dimensional)].Fact.[Transaction] as [Transaction]
							 inner join [$(Dimensional)].Dimension.TransactionDetail as TransactionDetail ON ([Transaction].TransactionDetailKey = TransactionDetail.TransactionDetailKey)
							 inner join [$(Dimensional)].Dimension.BalanceType as Balance ON (Balance.BalanceTypeKey = [Transaction].BalanceTypeKey)
							 where transactionid = 447428003
							 ) as Data
				Where		TransactedAmount = 0
			) 
Begin
	Insert into test.tblResults Values (@TestRunNo, 'CCIA-3405', 'CCIA-3405', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, 'CCIA-3405', 'CCIA-3405', 'Fail', 'Zero value transactions located')
End
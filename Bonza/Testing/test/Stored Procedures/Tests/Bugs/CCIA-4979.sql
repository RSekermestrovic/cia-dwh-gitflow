﻿--Test Code
--exec Test.[CCIA-4979] 'ADHOC'
Create Procedure Test.[CCIA-4979] @TestRunNo varchar(23)

as


If exists(
		Select		top 1 *
		From		[$(Dimensional)].[Control].[Version]
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, 'CCIA-4979', 'Dimensional DB', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, 'CCIA-4979', 'Dimensional DB', 'Fail', 'Do data has been found in the table')
End

If exists(
		Select		top 1 *
		From		[$(Acquisition)].[Control].[Version]
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, 'CCIA-4979.1', 'Acquisition DB', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, 'CCIA-4979.1', 'Acquisition DB', 'Fail', 'Do data has been found in the table')
End


﻿--Test Code
--exec Test.[CCIA-4690] 'ADHOC'
Create Procedure Test.[CCIA-4690] @TestRunNo varchar(23)

as


If not exists	(
				Select		*
				From		(
							Select		BatchA.BatchKey as BatchA
										, BatchB.BatchKey as BatchB
										, BatchA.BatchStartDate as BatchA_BatchStartDate
										, BatchA.BatchEndDate as BatchA_BatchEndDate
										, BatchA.AtomicStartDate
										, BatchB.BatchStartDate as BatchB_BatchStartDate
										, BatchB.BatchEndDate as BatchB_BatchEndDate
							From		[$(Staging)].[Control].ETLController as BatchA
										left join [$(Staging)].[Control].ETLController as BatchB on BatchA.BatchKey = (BatchB.BatchKey+1)
							) as Data
				Where		BatchA_BatchStartDate is not null
							and BatchA_BatchEndDate is not null
							and BatchB_BatchStartDate is not null
							and BatchB_BatchEndDate is not null
							and AtomicStartDate < BatchB_BatchEndDate
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, 'CCIA-4690', 'CCIA-4690', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, 'CCIA-4690', 'CCIA-4690', 'Fail', 'Atomic is running before the previous batch is complete')
End
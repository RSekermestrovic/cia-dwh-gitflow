﻿
CREATE View [test].[tblDefects]

as

--select * from [test].[tblDefects]

Select		[ID],
			JiraID as [DefectID],
			[Description],
			AssignedTo,
			RaisedBy,
			[Priority],
			[Status],
			Resolution
From		test.tblJira


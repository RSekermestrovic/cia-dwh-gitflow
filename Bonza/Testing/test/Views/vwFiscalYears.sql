﻿
--Test Code
--select * from test.vwFiscalYears

Create View Test.vwFiscalYears

as

Select		*
			, DateDiff(Day,FiscalStart, FiscalEnd)+1 as DaysInYear
			, DateDiff(Week,FiscalStart, FiscalEnd) as WeeksInYear
From		[$(Staging)].[Reference].FiscalYears as [tblFiscalYears]



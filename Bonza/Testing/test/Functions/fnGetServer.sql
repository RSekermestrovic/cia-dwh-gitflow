﻿--test
--select test.fnGetServer ()

Create Function [Test].[fnGetServer] ()

Returns varchar(100)

Begin
	Declare @Return varchar(100)
	Set @Return = (
				   Select data_source 
				   from sys.servers
				   where is_linked = 0
				   )
	Return @Return
end
GO
﻿--Test Code
--Select * from Test.fnTableExists (1,'Structure - Transaction Table', 'Fact', 'Transaction')

Create Function Test.fnTableExists (@ID varchar(100), @Name varchar(1000), @Schema varchar(100), @Table varchar(100))
Returns table

as
Return
(
select		@ID as TestID
			, @Name as TestName
			, 'Status' = Case	
				When count(*) = 1 then 'Pass'
				Else 'Fail'
				End
			, 'Notes' = CASE
				When count(*) = 1 then null
				When count(*) = 0 then 'The table was not found'
				else cast (count(*) as varchar(5)) + ' tables with the same name were located'
				end
from		[$(Dimensional)].sys.objects as [Objects]
			INNER JOIN [$(Dimensional)].sys.schemas as [Schemas] ON [Objects].schema_id = [Schemas].schema_id
where		[Objects].type = 'U'
			and [Objects].name = @Table
			and [Schemas].name = @Schema
)
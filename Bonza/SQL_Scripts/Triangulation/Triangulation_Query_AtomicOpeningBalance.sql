USE [AJ_Dimensional]
GO
/* Updated to use Version 2 */

DECLARE @FromDate DATETIME2(3) = '2015-04-06';
DECLARE @ToDate DATETIME2(3) = '2015-04-07';
DECLARE @BatchKey INT = 311;

SELECT *
FROM  (			   
		SELECT 
				DayDate, 
				BrandCode,
				Balance,
				ClientId,
				AccountId,
				MAX(SourceOpeningBalance) as SourceOpeningBalance,
				MAX(TransactedAmount) as TransactedAmount, 
				MAX(TransactionCount) as TransactedCount,
				FIRST_VALUE(SourceOpeningBalance) OVER (PARTITION BY BrandCode, Balance, AccountId ORDER BY DayDate) + MAX(TransactedAmount) NetClosingBalance,
				MAX(SourceOpeningBalance) + MAX(TransactedAmount) - LEAD(SourceOpeningBalance,1,0) OVER (PARTITION BY BrandCode, Balance, AccountId ORDER BY DayDate) DayClosingDiscrepancy,
				FIRST_VALUE(SourceOpeningBalance) OVER (PARTITION BY BrandCode, Balance, AccountId ORDER BY DayDate) + SUM(TransactedAmount) OVER (PARTITION BY BrandCode, Balance, AccountId ORDER BY DayDate RANGE BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) - LEAD(SourceOpeningBalance,1,0) OVER (PARTITION BY BrandCode, Balance, AccountId ORDER BY DayDate) TotalClosingDiscrepancy,
				LEAD(SourceOpeningBalance) OVER (PARTITION BY BrandCode, Balance, AccountId ORDER BY DayDate) NextSourceOpeningBalance,
				FIRST_VALUE(SourceOpeningBalance) OVER (PARTITION BY BrandCode, Balance, AccountId ORDER BY DayDate) + SUM(TransactedAmount) OVER (PARTITION BY BrandCode, Balance, AccountId) - FIRST_VALUE(SourceOpeningBalance) OVER (PARTITION BY BrandCode, Balance, AccountId ORDER BY DayDate DESC) TotalDiscrepancy,
				SUM(TransactedAmount) OVER (PARTITION BY BrandCode, Balance, AccountId ORDER BY DayDate RANGE BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) TotalTransacted
         FROM   (      
										SELECT 
										        DayDate,
												BrandCode,
                                                Balance,
                                                AccountId, 
                                                ClientId,                          
                                                --ISNULL(SUM(SourceOpeningBalance) ,0) SourceOpeningBalance,
												ISNULL(SUM(SourceOpeningBalance) OVER (PARTITION BY BrandCode, Balance, AccountId ORDER BY DayDate RANGE BETWEEN CURRENT ROW AND CURRENT ROW),0) SourceOpeningBalance,
                                                ISNULL(SUM(TransactedAmount),0) TransactedAmount,
												--SUM(TransactedAmount) OVER (PARTITION BY BrandCode, Balance, AccountId ORDER BY DayDate RANGE BETWEEN CURRENT ROW AND CURRENT ROW) TransactedAmount,
                                                SUM(CASE WHEN TransactedAmount <> 0 THEN 1 ELSE 0 END) TransactionCount
                                         FROM   (      SELECT CASE WHEN BrandCode = 'Unk' AND SourceAccountId NOT IN (777768,779522,783893,817235,825388,1187572,1250958) THEN 'Tote' ELSE BrandCode END BrandCode, 
                                                                           BalanceTypeName Balance,
                                                                           SourceAccountId AccountId,
                                                                           ClientId, 
                                                                           DayDate,
                                                                           NULL SourceOpeningBalance, 
                                                                           TransactedAmount
                                                              FROM Dimension.day d with (nolock)
																		   inner join dimension.dayzone dz with (nolock) on d.DayKey = dz.MasterDayKey
																		   inner join Fact.[Transaction] t with (nolock) on (t.DayKey = dz.DayKey)
                                                                           inner join Dimension.BalanceType b with (nolock) on (b.BalanceTypeKey = t.BalanceTypeKey)
                                                                           inner join Dimension.wallet w with (nolock) on (w.WalletKey = t.WalletKey)
                                                                           inner join Dimension.account a with (nolock) on (a.AccountKey = t.AccountKey)
                                                                           inner join Dimension.company c with (nolock) on (c.CompanyKey = a.CompanyKey)
                                                              where DayDate BETWEEN @FromDate AND @FromDate
                                                                           and BalanceTypeName IN ('Client')--,'Hold')
                                                                           and w.WalletName = 'Cash'
                                                                           and a.LedgerName NOT IN ('Write Offs','Vendor Splits','Vendor W.Offs/Other','Vendor Arrangements','Vendor 3rd Party Collection','Test','Betback') -- Added Test
                                                                           and a.AccountTypeName = 'Client'
                                                              UNION ALL
																SELECT 
																	CASE 
																		WHEN (BrandCode = 'Unk' OR BrandCode IS NULL) AND SourceAccountId NOT IN (777768,779522,783893,817235,825388,1187572,1250958) THEN 'Tote' 
																		ELSE BrandCode 
																	END as BrandCode, 
																	BalanceTypeName as Balance,
																	A.SourceAccountId as AccountId,
																	a.ClientId as ClientID, 
																	t.DayDate,
																	ClientBalance as SourceOpeningBalance, 
																	NULL as TransactedAmount
																FROM [$(Staging)].[Atomic].[OpeningBalance] t with (nolock)
																	inner join [$(Staging)].[Atomic].[Balance] as ab with (nolock) on t.AccountId = ab.AccountID and t.BatchKey = ab.BatchKey
																	inner join Dimension.BalanceType b with (nolock) on (b.BalanceTypeKey = 2)
																	inner join (SELECT
																					AccountID,
																					SourceAccountId,
																					ClientID,
																					CompanyKey,
																					LedgerName,
																					AccountTypeName,
																					ToDate
																				FROM Dimension.Account with (nolock)
																				WHERE ToDate > @FromDate
																				AND FromDate <= @FromDate
																				) as a on (a.SourceAccountID = t.AccountID)
																	left join Dimension.company c with (nolock) on (c.CompanyKey = a.CompanyKey and c.orgid = ab.OrgID)
																WHERE T.DayDate BETWEEN @FromDate AND @ToDate
																--AND B.BalanceName IN ('Client')--,'Hold')
																and a.LedgerName NOT IN ('Write Offs','Vendor Splits','Vendor W.Offs/Other','Vendor Arrangements','Vendor 3rd Party Collection','Test','Betback') -- Added Test
																AND A.AccountTypeName = 'Client'
																and t.FreeBet = 0
																--AND t.AccountEnabled = 1
                                         ) z 
										GROUP BY DayDate, BrandCode, Balance, AccountId, ClientId, SourceOpeningBalance--, TransactedAmount 
                           ) y
						   GROUP BY DayDate, BrandCode, Balance, AccountId, ClientId, SourceOpeningBalance, TransactedAmount
              ) x
WHERE NextSourceOpeningBalance IS NOT NULL
AND SourceOpeningBalance != NextSourceOpeningBalance     
AND DayClosingDiscrepancy <> 0
AND ABS(TotalDiscrepancy) <> 0
order by Balance, BrandCode, AccountId, clientid, DayDate
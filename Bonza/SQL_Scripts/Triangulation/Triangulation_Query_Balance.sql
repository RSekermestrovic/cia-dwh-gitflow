USE [AJ_Dimensional]
GO
/* Updated to use Version 2 */

DECLARE @FromDate DATETIME2(3) = '2015-04-02';
DECLARE @ToDate DATETIME2(3) = '2015-04-03';


-- Consistency check - total transacted amount = difference between source opening balances for consecutive days

SELECT	BrandCode,
		Balance,
		WalletName,
		DayDate,
		COUNT(*) NumberOfAccounts, 
		SUM(SourceOpeningBalance) SourceOpeningBalance,
		SUM(TransactedAmount) TransactedAmount, 
		SUM(TransactionCount) TransactedCount, 
		SUM(NetClosingBalance) NetClosingBalance,
		SUM(NextSourceOpeningBalance) NextSourceOpeningBalance,
		SUM(DayClosingDiscrepancy) DayClosingDiscrepancy,
		SUM(OngoingClosingDiscrepancy) OngoingClosingDiscrepancy,
		SUM(CASE DayClosingDiscrepancy WHEN 0 THEN 0 ELSE 1 END) AccountsWithNewDiscrepancy,
		SUM(CASE OngoingClosingDiscrepancy WHEN 0 THEN 0 ELSE 1 END) AccountsWithOngoingDiscrepancy
--select *
FROM	(	SELECT	Balance,
					WalletName,
					DayDate, 
					BrandCode,
					ClientId,
					AccountId,
					SourceOpeningBalance,
					TransactedAmount, 
					TransactionCount,
					SourceOpeningBalance + TransactedAmount NetClosingBalance,
					LEAD(SourceOpeningBalance) OVER (PARTITION BY BrandCode, Balance, WalletName, AccountId ORDER BY DayDate) NextSourceOpeningBalance,
					SourceOpeningBalance + TransactedAmount - LEAD(SourceOpeningBalance,1,0) OVER (PARTITION BY BrandCode, Balance, WalletName, AccountId ORDER BY DayDate) DayClosingDiscrepancy,
					FIRST_VALUE(SourceOpeningBalance) OVER (PARTITION BY BrandCode, Balance, WalletName, AccountId ORDER BY DayDate) + SUM(TransactedAmount) OVER (PARTITION BY BrandCode, Balance, WalletName, AccountId ORDER BY DayDate RANGE BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) - LEAD(SourceOpeningBalance,1,0) OVER (PARTITION BY BrandCode, Balance, WalletName, AccountId ORDER BY DayDate) OngoingClosingDiscrepancy,
					FIRST_VALUE(SourceOpeningBalance) OVER (PARTITION BY BrandCode, Balance, WalletName, AccountId ORDER BY DayDate) + SUM(TransactedAmount) OVER (PARTITION BY BrandCode, Balance, WalletName, AccountId) - FIRST_VALUE(SourceOpeningBalance) OVER (PARTITION BY BrandCode, Balance, WalletName, AccountId ORDER BY DayDate DESC) TotalDiscrepancy
			FROM	(	SELECT BrandCode,
								Balance,
								WalletName,
								AccountId, 
								ClientId,
								DayDate,
								ISNULL(SUM(SourceOpeningBalance) ,0) SourceOpeningBalance, 
								ISNULL(SUM(TransactedAmount),0) TransactedAmount,
								SUM(CASE WHEN TransactedAmount <> 0 THEN 1 ELSE 0 END) TransactionCount
						FROM	(	SELECT	CASE WHEN BrandCode = 'Unk' AND SourceAccountId NOT IN (777768,779522,783893,817235,825388,1187572,1250958) THEN 'Tote' ELSE BrandCode END BrandCode, 
											BalanceTypeName Balance,
											WalletName,
											SourceAccountId AccountId,
											ClientId, 
											DayDate,
											NULL SourceOpeningBalance, 
											TransactedAmount
									FROM   Dimension.day d with (nolock)
											inner join dimension.dayzone dz with (nolock) on d.DayKey = dz.MasterDayKey
											inner join Fact.[Transaction] t with (nolock) on (t.DayKey=dz.DayKey)
											inner join Dimension.BalanceType b with (nolock) on (b.BalanceTypeKey = t.BalanceTypeKey)
											inner join Dimension.wallet w with (nolock) on (w.WalletKey = t.WalletKey)
											inner join Dimension.account a with (nolock) on (a.AccountKey = t.AccountKey)
											inner join Dimension.company c with (nolock) on (c.CompanyKey = a.CompanyKey)
									where DayDate BETWEEN @FromDate AND @FromDate
											and BalanceTypeName IN ('Client')--,'Hold')
											and a.LedgerName NOT IN ('Write Offs','Vendor Splits','Vendor W.Offs/Other','Vendor Arrangements','Vendor 3rd Party Collection','Test','Betback') -- Added Test
											and a.AccountTypeName = 'Client'
 											and w.WalletName IN ('Cash','N/A')
									UNION ALL
									select	CASE WHEN BrandCode = 'Unk' AND SourceAccountId NOT IN (777768,779522,783893,817235,825388,1187572,1250958) THEN 'Tote' ELSE BrandCode END BrandCode, 
											BalanceTypeName Balance,
											WalletName,
											SourceAccountId AccountId,
											ClientId, 
											DayDate,
											OpeningBalance as [SourceOpeningBalance], 
											NULL TransactedAmount 
									from   Dimension.day d with (nolock)
											inner join dimension.dayzone dz with (nolock) on d.DayKey = dz.MasterDayKey
											inner join Fact.[Balance] t with (nolock) on (t.DayKey=dz.DayKey)
											inner join Dimension.BalanceType b with (nolock) on (b.BalanceTypeKey = t.BalanceTypeKey)
											inner join Dimension.account a with (nolock) on (a.AccountKey = t.AccountKey)
											inner join Dimension.company c with (nolock) on (c.CompanyKey = a.CompanyKey)
											inner join Dimension.wallet w with (nolock) on (w.WalletKey = t.WalletKey)
									where d.DayDate BETWEEN @FromDate AND @ToDate
											and BalanceTypeName IN ('Client')--,'Hold')
											and a.LedgerName NOT IN ('Write Offs','Vendor Splits','Vendor W.Offs/Other','Vendor Arrangements','Vendor 3rd Party Collection','Test','Betback') -- Added Test
											and a.AccountTypeName = 'Client'
											and w.WalletName IN ('Cash','N/A')
						) z 
					group by BrandCode, Balance, WalletName, DayDate, AccountId, ClientId
				) y
		) x
WHERE NextSourceOpeningBalance IS NOT NULL	
 and (DayClosingDiscrepancy <> 0 /*OR TotalClosingDiscrepancy <> 0*/)
 and TotalDiscrepancy <> 0
GROUP BY BrandCode, Balance, WalletName, DayDate
ORDER BY BrandCode, Balance, WalletName, DayDate


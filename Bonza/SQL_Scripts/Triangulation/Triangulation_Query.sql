USE [AJ_Dimensional]
GO
/* Updated to use Version 2 */

DECLARE @FromDate DATETIME2(3) = '2015-04-08';
DECLARE @ToDate DATETIME2(3) = '2015-04-09';

select *
FROM   (      SELECT Balance,
                                  DayDate, 
                                  BrandCode,
                                  ClientId,
                                  AccountId,
                                  SourceOpeningBalance,
                                  TransactedAmount, 
                                  TransactionCount,
                                  SourceOpeningBalance + TransactedAmount NetClosingBalance,
                                  SourceOpeningBalance + TransactedAmount - LEAD(SourceOpeningBalance,1,0) OVER (PARTITION BY BrandCode, Balance, AccountId ORDER BY DayDate) DayClosingDiscrepancy,
                                  FIRST_VALUE(SourceOpeningBalance) OVER (PARTITION BY BrandCode, Balance, AccountId ORDER BY DayDate) + SUM(TransactedAmount) OVER (PARTITION BY BrandCode, Balance, AccountId ORDER BY DayDate RANGE BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) - LEAD(SourceOpeningBalance,1,0) OVER (PARTITION BY BrandCode, Balance, AccountId ORDER BY DayDate) TotalClosingDiscrepancy,
                                  LEAD(SourceOpeningBalance) OVER (PARTITION BY BrandCode, Balance, AccountId ORDER BY DayDate) NextSourceOpeningBalance,
                                  FIRST_VALUE(SourceOpeningBalance) OVER (PARTITION BY BrandCode, Balance, AccountId ORDER BY DayDate) + SUM(TransactedAmount) OVER (PARTITION BY BrandCode, Balance, AccountId) - FIRST_VALUE(SourceOpeningBalance) OVER (PARTITION BY BrandCode, Balance, AccountId ORDER BY DayDate DESC) TotalDiscrepancy,
                                  SUM(TransactedAmount) OVER (PARTITION BY BrandCode, Balance, AccountId ORDER BY DayDate RANGE BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) TotalTransacted
                     FROM   (      SELECT BrandCode,
                                                       Balance,
                                                       AccountId, 
                                                       ClientId,
                                                       DayDate,
                                                       ISNULL(SUM(SourceOpeningBalance) ,0) SourceOpeningBalance, 
                                                       ISNULL(SUM(TransactedAmount),0) TransactedAmount,
                                                       SUM(CASE WHEN TransactedAmount <> 0 THEN 1 ELSE 0 END) TransactionCount
                                         FROM   (      SELECT CASE WHEN BrandCode = 'Unk' AND SourceAccountId NOT IN (777768,779522,783893,817235,825388,1187572,1250958) THEN 'Tote' ELSE BrandCode END BrandCode, 
                                                                           BalanceTypeName Balance,
                                                                           SourceAccountId AccountId,
                                                                           ClientId, 
                                                                           DayDate,
                                                                           NULL SourceOpeningBalance, 
                                                                           TransactedAmount
                                                               FROM Dimension.day d with (nolock)
																		   inner join dimension.dayzone dz with (nolock) on d.DayKey = dz.MasterDayKey
																		   inner join Fact.[Transaction] t with (nolock) on (t.DayKey = dz.DayKey)
                                                                           inner join Dimension.BalanceType b with (nolock) on (b.BalanceTypeKey = t.BalanceTypeKey)
                                                                           inner join Dimension.wallet w with (nolock) on (w.WalletKey = t.WalletKey)
                                                                           inner join Dimension.account a with (nolock) on (a.AccountKey = t.AccountKey)
                                                                           inner join Dimension.company c with (nolock) on (c.CompanyKey = a.CompanyKey)
                                                              where DayDate BETWEEN @FromDate AND @FromDate
                                                                           and BalanceTypeName IN ('Client')
                                                                           and w.WalletName IN ('Cash') --,'N/A')
                                                                           and a.LedgerName NOT IN ('Write Offs','Vendor Splits','Vendor W.Offs/Other','Vendor Arrangements','Vendor 3rd Party Collection','Test','Betback') -- Added Test
                                                                           and a.AccountTypeName = 'Client'
                                                              UNION ALL
                                                              select CASE WHEN BrandCode = 'Unk' AND SourceAccountId NOT IN (777768,779522,783893,817235,825388,1187572,1250958) THEN 'Tote' ELSE BrandCode END BrandCode, 
                                                                           BalanceTypeName Balance,
                                                                           SourceAccountId AccountId,
                                                                           ClientId, 
                                                                           DayDate,
                                                                           OpeningBalance as [SourceOpeningBalance], 
                                                                           NULL TransactedAmount
                                                               FROM Dimension.day d with (nolock)
																		   inner join dimension.dayzone dz with (nolock) on d.DayKey = dz.MasterDayKey
																		   inner join Fact.[Position] t with (nolock) on (t.DayKey = dz.DayKey)
                                                                           inner join Dimension.BalanceType b with (nolock) on (b.BalanceTypeKey = t.BalanceTypeKey)
                                                                           inner join Dimension.account a with (nolock) on (a.AccountKey = t.AccountKey)
                                                                           inner join Dimension.company c with (nolock) on (c.CompanyKey = a.CompanyKey)
                                                              where DayDate BETWEEN @FromDate AND @ToDate
                                                                           and BalanceTypeName IN ('Client')
                                                                           and a.LedgerName NOT IN ('Write Offs','Vendor Splits','Vendor W.Offs/Other','Vendor Arrangements','Vendor 3rd Party Collection','Test','Betback') -- Added Test
                                                                           and a.AccountTypeName = 'Client'
                                         ) z 
                                  group by BrandCode, Balance, DayDate, AccountId, ClientId
                           ) y
              ) x
WHERE NextSourceOpeningBalance IS NOT NULL      
and (DayClosingDiscrepancy <> 0 /*OR TotalClosingDiscrepancy <> 0*/)
and ABS(TotalDiscrepancy) <> 0
--GROUP BY OrgName, Balance, DayDate
order by Balance, BrandCode, AccountId, clientid, DayDate

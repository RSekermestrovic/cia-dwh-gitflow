/* Developer: Aaron Jackson
   Date: 06/08/2015
   Desc: Populate small acquisition database
*/

DBCC TRACEOFF (610, -1)

Begin Try
	drop table #AccountID
End Try
Begin Catch
End Catch

Begin Try
	drop table #ClientID
End Try
Begin Catch
End Catch

Begin Try
	drop table #BetID
End Try
Begin Catch
End Catch

Begin Try
	drop table #EventID
End Try
Begin Catch
End Catch

BEGIN TRY

	CREATE TABLE #AccountID( AccountID INT);

--100 random accountids
	INSERT INTO #AccountID
	SELECT 1155866 UNION ALL
	SELECT 980615 UNION ALL
	SELECT 743865 UNION ALL
	SELECT 1270037 UNION ALL
	SELECT 1269898 UNION ALL
	SELECT 1266785 UNION ALL
	SELECT 640841 UNION ALL
	SELECT 1181354 UNION ALL
	SELECT 1264137 UNION ALL
	SELECT 680880 UNION ALL
	SELECT 1268864 UNION ALL
	SELECT 719457 UNION ALL
	SELECT 243939 UNION ALL
	SELECT 1268839 UNION ALL
	SELECT 1265414 UNION ALL
	SELECT 1270005 UNION ALL
	SELECT 1270006 UNION ALL
	SELECT 756550 UNION ALL
	SELECT 1270007 UNION ALL
	SELECT 833279 UNION ALL
	SELECT 734814 UNION ALL
	SELECT 500549 UNION ALL
	SELECT 1096545 UNION ALL
	SELECT 1269986 UNION ALL
	SELECT 1269002 UNION ALL
	SELECT 824801 UNION ALL
	SELECT 1122418 UNION ALL
	SELECT 770009 UNION ALL
	SELECT 1199748 UNION ALL
	SELECT 148911 UNION ALL
	SELECT 148526 UNION ALL
	SELECT 64072 UNION ALL
	SELECT 1269118 UNION ALL
	SELECT 832726 UNION ALL
	SELECT 1269985 UNION ALL
	SELECT 1269672 UNION ALL
	SELECT 1200559 UNION ALL
	SELECT 788356 UNION ALL
	SELECT 1932 UNION ALL
	SELECT 1269502 UNION ALL
	SELECT 1264022 UNION ALL
	SELECT 138663 UNION ALL
	SELECT 751075 UNION ALL
	SELECT 703154 UNION ALL
	SELECT 1139767 UNION ALL
	SELECT 239871 UNION ALL
	SELECT 784008 UNION ALL
	SELECT 1268163 UNION ALL
	SELECT 61444 UNION ALL
	SELECT 98030 UNION ALL
	SELECT 1123471 UNION ALL
	SELECT 96630 UNION ALL
	SELECT 69834 UNION ALL
	SELECT 68379 UNION ALL
	SELECT 31073 UNION ALL
	SELECT 1149201 UNION ALL
	SELECT 1112381 UNION ALL
	SELECT 884569 UNION ALL
	SELECT 987083 UNION ALL
	SELECT 357497 UNION ALL
	SELECT 510778 UNION ALL
	SELECT 144333 UNION ALL
	SELECT 829583 UNION ALL
	SELECT 48391 UNION ALL
	SELECT 1269967 UNION ALL
	SELECT 11745 UNION ALL
	SELECT 756865 UNION ALL
	SELECT 56007 UNION ALL
	SELECT 76264 UNION ALL
	SELECT 85434 UNION ALL
	SELECT 60119 UNION ALL
	SELECT 53825 UNION ALL
	SELECT 60276 UNION ALL
	SELECT 54713 UNION ALL
	SELECT 69116 UNION ALL
	SELECT 138283 UNION ALL
	SELECT 1123274 UNION ALL
	SELECT 760527 UNION ALL
	SELECT 1005263 UNION ALL
	SELECT 47669 UNION ALL
	SELECT 83934 UNION ALL
	SELECT 853083 UNION ALL
	SELECT 508477 UNION ALL
	SELECT 1269704 UNION ALL
	SELECT 1269616 UNION ALL
	SELECT 1269224 UNION ALL
	SELECT 1269938 UNION ALL
	SELECT 1269794 UNION ALL
	SELECT 1269754 UNION ALL
	SELECT 232572 UNION ALL
	SELECT 620578 UNION ALL
	SELECT 1269971 UNION ALL
	SELECT 1157601 UNION ALL
	SELECT 950438 UNION ALL
	SELECT 143396 UNION ALL
	SELECT 778096 UNION ALL
	SELECT 1197283 UNION ALL
	SELECT 710821 UNION ALL
	SELECT 677781 UNION ALL
	SELECT 1173977

--Specific test cases
	INSERT INTO #AccountID
	SELECT 1283969 UNION ALL -- Account openend within the test period (on 2/4/15) - included to regression test issues with creating AccountStatus Type 2 dimension with correct FromDate to accommodate all transactions including those on the first day ;
	SELECT 250738 UNION ALL -- Transactions exist before the Account Opened timestamp due to latency
	SELECT 700365 UNION ALL -- Live test account


-- Triangulation Failures
	SELECT 4490 UNION ALL
	SELECT 9440 UNION ALL
	SELECT 22969 UNION ALL
	SELECT 50951 UNION ALL
	SELECT 55785 UNION ALL
	SELECT 60838 UNION ALL
	SELECT 69215 UNION ALL
	SELECT 87163 UNION ALL
	SELECT 90632 UNION ALL
	SELECT 91271 UNION ALL
	SELECT 92244 UNION ALL
	SELECT 95823 UNION ALL
	SELECT 115360 UNION ALL
	SELECT 122556 UNION ALL
	SELECT 131762 UNION ALL
	SELECT 131816 UNION ALL
	SELECT 158215 UNION ALL
	SELECT 175152 UNION ALL
	SELECT 190225 UNION ALL
	SELECT 190349 UNION ALL
	SELECT 208899 UNION ALL
	SELECT 224742 UNION ALL
	SELECT 226188 UNION ALL
	SELECT 231246 UNION ALL
	SELECT 232175 UNION ALL
	SELECT 240704 UNION ALL
	SELECT 241916 UNION ALL
	SELECT 243858 UNION ALL
	SELECT 249501 UNION ALL
	SELECT 250351 UNION ALL
	SELECT 260237 UNION ALL
	SELECT 261279 UNION ALL
	SELECT 269670 UNION ALL
	SELECT 274469 UNION ALL
	SELECT 280266 UNION ALL
	SELECT 282497 UNION ALL
	SELECT 283077 UNION ALL
	SELECT 284307 UNION ALL
	SELECT 287152 UNION ALL
	SELECT 290295 UNION ALL
	SELECT 290980 UNION ALL
	SELECT 291112 UNION ALL
	SELECT 291312 UNION ALL
	SELECT 301413 UNION ALL
	SELECT 314243 UNION ALL
	SELECT 319798 UNION ALL
	SELECT 366951 UNION ALL
	SELECT 408854 UNION ALL
	SELECT 462913 UNION ALL
	SELECT 479124 UNION ALL
	SELECT 530518 UNION ALL
	SELECT 650844 UNION ALL
	SELECT 679916 UNION ALL
	SELECT 681996 UNION ALL
	SELECT 698590 UNION ALL
	SELECT 705899 UNION ALL
	SELECT 711818 UNION ALL
	SELECT 717413 UNION ALL
	SELECT 728024 UNION ALL
	SELECT 783175 UNION ALL
	SELECT 823308 UNION ALL
	SELECT 824371 UNION ALL
	SELECT 879258 UNION ALL
	SELECT 941459 UNION ALL
	SELECT 984966 UNION ALL
	SELECT 1096791 UNION ALL
	SELECT 1108508 UNION ALL
	SELECT 1108946 UNION ALL
	SELECT 1110682 UNION ALL
	SELECT 1118341 UNION ALL
	SELECT 1119446 UNION ALL
	SELECT 1127092 UNION ALL
	SELECT 1129228 UNION ALL
	SELECT 1152468 UNION ALL
	SELECT 1154353 UNION ALL
	SELECT 1154647 UNION ALL
	SELECT 1164042 UNION ALL
	SELECT 1171003 UNION ALL
	SELECT 1173371 UNION ALL
	SELECT 1194248 UNION ALL
	SELECT 1200906 UNION ALL
	SELECT 1259634 UNION ALL
	SELECT 1265000 UNION ALL
	SELECT 1289574 UNION ALL
	SELECT 1290600 UNION ALL
	SELECT 1294291 UNION ALL
	SELECT 1300894 UNION ALL
	SELECT 1301727 UNION ALL
	SELECT 1303478 UNION ALL
	SELECT 1305647 UNION ALL
	SELECT 1312683 UNION ALL
	SELECT 1336938 UNION ALL
	SELECT 1343200 UNION ALL
	SELECT 1343451 UNION ALL
	SELECT 1345517 UNION ALL
	SELECT 1347883 UNION ALL
	SELECT 1348420 UNION ALL
	SELECT 1348754 UNION ALL
	SELECT 1351867 UNION ALL
	SELECT 1352462 UNION ALL
	SELECT 1355976 UNION ALL
	SELECT 1357505 UNION ALL
	SELECT 1357694 UNION ALL
	SELECT 1362267 UNION ALL
	SELECT 1363099 UNION ALL
	SELECT 1363156 UNION ALL
	SELECT 1363810 UNION ALL
	SELECT 1369182 UNION ALL
	SELECT 1369635 UNION ALL
	SELECT 1373545 UNION ALL
	SELECT 1375166 UNION ALL
	SELECT 1376540 UNION ALL
	SELECT 1376854 UNION ALL
	SELECT 1379844 UNION ALL
	SELECT 1380462 UNION ALL
	SELECT 1380534 UNION ALL
	SELECT 1382195 UNION ALL
	SELECT 1385530 UNION ALL
	SELECT 1386245 UNION ALL
	SELECT 1388230 UNION ALL
	SELECT 1397394 UNION ALL
	SELECT 1404963 UNION ALL
	SELECT 1405001 UNION ALL

	--For CashOut

	SELECT 827905 UNION ALL
	SELECT 1277686 UNION ALL
	SELECT 1343377 UNION ALL
	SELECT 1256911 UNION ALL
	SELECT 1451947 UNION ALL
	SELECT 24903 UNION ALL
	SELECT 1459473 UNION ALL
	SELECT 1339125 UNION ALL
	SELECT 17195 UNION ALL
	SELECT 1419170 UNION ALL
	SELECT 825642 UNION ALL
	SELECT 21222 UNION ALL
	SELECT 1269769 UNION ALL
	SELECT 1436341 UNION ALL
	SELECT 1370165 UNION ALL
	SELECT 1339228 UNION ALL
	SELECT 1278109 UNION ALL
	SELECT 1339285 UNION ALL
	SELECT 21221 UNION ALL
	SELECT 1459419


BEGIN /* CLIENT */

	Truncate table Small_Acquisition.IntraBet.tblAccounts
	INSERT INTO Small_Acquisition.IntraBet.tblAccounts WITH (TABLOCK)
	SELECT *
	FROM Acquisition.IntraBet.tblAccounts --ClientID
	WHERE AccountID IN (SELECT AccountID FROM #AccountID);

	SELECT DISTINCT ClientID INTO #ClientID FROM Small_Acquisition.IntraBet.tblAccounts;

	Truncate table Small_Acquisition.IntraBet.tblClientAccountClosures
	INSERT INTO Small_Acquisition.IntraBet.tblClientAccountClosures WITH (TABLOCK)
	SELECT *
	FROM Acquisition.IntraBet.tblClientAccountClosures
	WHERE ClientID IN (SELECT ClientID FROM #ClientID);

	Truncate table Small_Acquisition.IntraBet.tblClientLimit
	INSERT INTO Small_Acquisition.IntraBet.tblClientLimit WITH (TABLOCK)
	SELECT *
	FROM Acquisition.IntraBet.tblClientLimit
	WHERE ClientID IN (SELECT ClientID FROM #ClientID);

	Truncate table Small_Acquisition.IntraBet.tblSubscriptionCentre
	INSERT INTO Small_Acquisition.IntraBet.tblSubscriptionCentre WITH (TABLOCK)
	SELECT *
	FROM Acquisition.IntraBet.tblSubscriptionCentre
	WHERE ClientID IN (SELECT ClientID FROM #ClientID);

	Truncate table Small_Acquisition.IntraBet.tblClients
	INSERT INTO Small_Acquisition.IntraBet.tblClients WITH (TABLOCK)
	SELECT *
	FROM Acquisition.IntraBet.tblClients --ClientID
	WHERE ClientID IN (SELECT ClientID FROM #ClientID);

	Truncate table Small_Acquisition.IntraBet.tblCreditCardTransactions
	INSERT INTO Small_Acquisition.IntraBet.tblCreditCardTransactions WITH (TABLOCK)
	SELECT *
	FROM Acquisition.IntraBet.tblCreditCardTransactions -- ClientID, TransactionID
	WHERE ClientID IN (SELECT ClientID FROM #ClientID);

	Truncate table Small_Acquisition.IntraBet.tblClientInfo
	INSERT INTO Small_Acquisition.IntraBet.tblClientInfo WITH (TABLOCK)
	SELECT *
	FROM Acquisition.IntraBet.tblClientInfo --ClientID
	WHERE ClientID IN (SELECT ClientID FROM #ClientID);

	Truncate table Small_Acquisition.IntraBet.tblClientAddresses
	INSERT INTO Small_Acquisition.IntraBet.tblClientAddresses WITH (TABLOCK)
	SELECT *
	FROM Acquisition.IntraBet.tblClientAddresses --ClientID
	WHERE ClientID IN (SELECT ClientID FROM #ClientID);

	Truncate table Small_Acquisition.IntraBet.tblClientWithdrawRequest
	INSERT INTO Small_Acquisition.IntraBet.tblClientWithdrawRequest WITH (TABLOCK)
	SELECT *
	FROM Acquisition.IntraBet.tblClientWithdrawRequest --ClientID
	WHERE ClientID IN (SELECT ClientID FROM #ClientID);

	Truncate table Small_Acquisition.IntraBet.tblPayPalDEC
	INSERT INTO Small_Acquisition.IntraBet.tblPayPalDEC WITH (TABLOCK)
	SELECT *
	FROM Acquisition.IntraBet.tblPayPalDEC -- ClientID, TransactionID
	WHERE ClientID IN (SELECT ClientID FROM #ClientID);

	Truncate table Small_Acquisition.IntraBet.tblPayPalGEC
	INSERT INTO Small_Acquisition.IntraBet.tblPayPalGEC WITH (TABLOCK)
	SELECT *
	FROM Acquisition.IntraBet.tblPayPalGEC --ClientID
	WHERE ClientID IN (SELECT ClientID FROM #ClientID);

	Truncate table Small_Acquisition.IntraBet.tblClientBankDetails
	INSERT INTO Small_Acquisition.IntraBet.tblClientBankDetails WITH (TABLOCK)
	SELECT *
	FROM Acquisition.IntraBet.tblClientBankDetails --ClientID
	WHERE ClientID IN (SELECT ClientID FROM #ClientID);

	Truncate table Small_Acquisition.IntraBet.tblMoneybookersDepositResponse
	INSERT INTO Small_Acquisition.IntraBet.tblMoneybookersDepositResponse WITH (TABLOCK)
	SELECT *
	FROM Acquisition.IntraBet.tblMoneybookersDepositResponse -- ClientID
	WHERE ClientID IN (SELECT ClientID FROM #ClientID);

	Truncate table Small_Acquisition.IntraBet.tblClientBPayDetails 
	INSERT INTO Small_Acquisition.IntraBet.tblClientBPayDetails WITH (TABLOCK)
	SELECT *
	FROM Acquisition.IntraBet.tblClientBPayDetails -- ClientID
	WHERE ClientID IN (SELECT ClientID FROM #ClientID);

--**** Need to fix this to select via associate
	Truncate table Small_Acquisition.IntraBet.tblIntroducers 
	INSERT INTO Small_Acquisition.IntraBet.tblIntroducers  WITH (TABLOCK)
	SELECT *
	FROM Acquisition.IntraBet.tblIntroducers -- ClientID
	WHERE ClientID IN (SELECT ClientID FROM #ClientID);

	Truncate table Small_Acquisition.IntraBet.tblClientAffiliateInfo
	INSERT INTO Small_Acquisition.IntraBet.tblClientAffiliateInfo WITH (TABLOCK)
	SELECT *
	FROM Acquisition.IntraBet.tblClientAffiliateInfo
	WHERE ClientID IN (SELECT ClientID FROM #ClientID);

END

BEGIN /* BET */
	
	Truncate table Small_Acquisition.IntraBet.tblBets
	INSERT INTO Small_Acquisition.IntraBet.tblBets WITH (TABLOCK)
	SELECT *
	FROM Acquisition.IntraBet.tblBets
	WHERE ClientID IN (SELECT ClientID FROM Small_Acquisition.IntraBet.tblAccounts);

	SELECT DISTINCT BetID INTO #BetID FROM Small_Acquisition.IntraBet.tblBets

	Truncate table Small_Acquisition.IntraBet.tblCashoutBets
	INSERT INTO Small_Acquisition.IntraBet.tblCashoutBets WITH (TABLOCK)
	SELECT *
	FROM  [Acquisition].[IntraBet].[tblCashoutBets]
	WHERE BetID IN (SELECT BetID FROM #BetID);

	Truncate table Small_Acquisition.IntraBet.tblCancelledBets
	INSERT INTO Small_Acquisition.IntraBet.tblCancelledBets WITH (TABLOCK)
	SELECT *
	FROM Acquisition.IntraBet.tblCancelledBets
	WHERE BetID IN (SELECT BetID FROM #BetID);

	Truncate table Small_Acquisition.IntraBet.tblAllUps
	INSERT INTO Small_Acquisition.IntraBet.tblAllUps WITH (TABLOCK)
	SELECT *
	FROM Acquisition.IntraBet.tblAllUps
	WHERE BetID IN (SELECT BetID FROM #BetID);

	Truncate table Small_Acquisition.IntraBet.tblAllUpGroupBets
	INSERT INTO Small_Acquisition.IntraBet.tblAllUpGroupBets WITH (TABLOCK)
	SELECT *
	FROM Acquisition.IntraBet.tblAllUpGroupBets
	WHERE BetID IN (SELECT BetID FROM #BetID);

	Truncate table Small_Acquisition.IntraBet.tblExoticGroupBets
	INSERT INTO Small_Acquisition.IntraBet.tblExoticGroupBets WITH (TABLOCK)
	SELECT *
	FROM Acquisition.IntraBet.tblExoticGroupBets
	WHERE BetID IN (SELECT BetID FROM #BetID);

	Truncate table Small_Acquisition.IntraBet.tblGroupedBets
	INSERT INTO Small_Acquisition.IntraBet.tblGroupedBets WITH (TABLOCK)
	SELECT *
	FROM Acquisition.IntraBet.tblGroupedBets
	WHERE BetID IN (SELECT BetID FROM #BetID);

	Truncate table Small_Acquisition.IntraBet.tblPromotionalBets 
	INSERT INTO Small_Acquisition.IntraBet.tblPromotionalBets  WITH (TABLOCK)
	SELECT *
	FROM Acquisition.IntraBet.tblPromotionalBets -- ClientID, FreeBetID
	WHERE ClientID IN (SELECT ClientID FROM #ClientID)
	UNION
	SELECT *
	FROM Acquisition.IntraBet.tblPromotionalBets
	WHERE FreeBetId IN (1,2); -- Include 2 additional records required to prevent a benign defect becoming malignent (FreeBetId 1 and 2 are more recently used as flag values on tblBets than links to this table but require these records to still support the incorrect links to obtain correct results)

	Truncate table Small_Acquisition.IntraBet.tblBonusTransactions
	INSERT INTO Small_Acquisition.IntraBet.tblBonusTransactions WITH (TABLOCK)
	SELECT *
	FROM Acquisition.IntraBet.tblBonusTransactions
	WHERE ClientID IN (SELECT ClientID FROM #ClientID);
	
	Truncate table Small_Acquisition.IntraBet.tblDeletedTransactions
	INSERT INTO Small_Acquisition.IntraBet.tblDeletedTransactions WITH (TABLOCK)
	SELECT *
	FROM Acquisition.IntraBet.tblDeletedTransactions
	WHERE ClientID IN (SELECT ClientID FROM #ClientID);

END 

BEGIN /* TRANSACTION */
	
	Truncate table Small_Acquisition.IntraBet.tblTransactions
	INSERT INTO Small_Acquisition.IntraBet.tblTransactions WITH (TABLOCK)
	SELECT *
	FROM Acquisition.IntraBet.tblTransactions
	WHERE ClientID IN (SELECT ClientID FROM #ClientID);
	
	Truncate table Small_Acquisition.IntraBet.tblTransactionLogs
	INSERT INTO Small_Acquisition.IntraBet.tblTransactionLogs WITH (TABLOCK)
	SELECT *
	FROM Acquisition.IntraBet.tblTransactionLogs
	WHERE TransactionID IN (SELECT TransactionID FROM Small_Acquisition.IntraBet.tblTransactions);

END

BEGIN /* EVENT */

	SELECT DISTINCT EventId INTO #EventID FROM Small_Acquisition.IntraBet.tblBets;

	Truncate table Small_Acquisition.IntraBet.tblEvents
	INSERT INTO Small_Acquisition.IntraBet.tblEvents WITH (TABLOCK)
	SELECT *
	FROM Acquisition.IntraBet.tblEvents
	WHERE EventID IN (SELECT EventID FROM #EventID);

	WHILE @@ROWCOUNT > 0
	INSERT INTO Small_Acquisition.IntraBet.tblEvents WITH (TABLOCK)
	SELECT *
	FROM Acquisition.IntraBet.tblEvents
	WHERE EventID IN (SELECT m.MasterEventID FROM Small_Acquisition.IntraBet.tblEvents m LEFT JOIN Small_Acquisition.IntraBet.tblEvents e ON (e.EventID = m.MasterEventID) WHERE e.EventID IS NULL);

	Truncate table Small_Acquisition.IntraBet.tblCompetitors
	INSERT INTO Small_Acquisition.IntraBet.tblCompetitors WITH (TABLOCK)
	SELECT *
	FROM Acquisition.IntraBet.tblCompetitors
	WHERE EventID IN (SELECT EventID FROM #EventID);

	Truncate table Small_Acquisition.IntraBet.tblRacingResults
	INSERT INTO Small_Acquisition.IntraBet.tblRacingResults WITH (TABLOCK)
	SELECT *
	FROM Acquisition.IntraBet.tblRacingResults
	WHERE EventID IN (SELECT EventID FROM #EventID);

	Truncate table Small_Acquisition.PricingData.Race
	INSERT INTO Small_Acquisition.PricingData.Race WITH (TABLOCK)
	SELECT *
	FROM Acquisition.PricingData.Race
	WHERE IntrabetID IN (SELECT EventID FROM #EventID);

END

BEGIN /* EXACT TARGET */

       Truncate table Small_Acquisition.IntraBet.tblSubscriptionCentre
       INSERT INTO Small_Acquisition.IntraBet.tblSubscriptionCentre WITH (TABLOCK)
       SELECT *
       FROM Acquisition.IntraBet.tblSubscriptionCentre
       WHERE ClientID IN (SELECT ClientID FROM #ClientID);

       SELECT DISTINCT ExactTargetID INTO #ExactTargetID FROM Small_Acquisition.IntraBet.tblSubscriptionCentre;

       Truncate table Small_Acquisition.ExactTarget.Bounce
       INSERT INTO Small_Acquisition.ExactTarget.Bounce WITH (TABLOCK)
       SELECT *
       FROM Acquisition.ExactTarget.Bounce
       WHERE SubscriberKey IN (SELECT CONVERT(VARCHAR(500),ExactTargetID) FROM #ExactTargetID);

	   Truncate table Small_Acquisition.ExactTarget.Click
       INSERT INTO Small_Acquisition.ExactTarget.Click WITH (TABLOCK)
       SELECT *
       FROM Acquisition.ExactTarget.Click
       WHERE SubscriberKey IN (SELECT CONVERT(VARCHAR(500),ExactTargetID) FROM #ExactTargetID);

	   Truncate table Small_Acquisition.ExactTarget.Job
       INSERT INTO Small_Acquisition.ExactTarget.Job WITH (TABLOCK)
       SELECT *
       FROM Acquisition.ExactTarget.Job Where FromDate > '2016-04-05 00:00:00.000'

       INSERT INTO Small_Acquisition.ExactTarget.Job WITH (TABLOCK)
       SELECT *
       FROM Acquisition.ExactTarget.Job Where FromDate = '2015-11-05 00:00:00.000'

	   Truncate table Small_Acquisition.ExactTarget.[Open]
       INSERT INTO Small_Acquisition.ExactTarget.[Open] WITH (TABLOCK)
       SELECT *
       FROM Acquisition.ExactTarget.[Open]
       WHERE SubscriberKey IN (SELECT CONVERT(VARCHAR(500),ExactTargetID) FROM #ExactTargetID);

	   Truncate table Small_Acquisition.ExactTarget.[Sent]
       INSERT INTO Small_Acquisition.ExactTarget.[Sent] WITH (TABLOCK)
       SELECT *
       FROM Acquisition.ExactTarget.[Sent]
       WHERE SubscriberKey IN (SELECT CONVERT(VARCHAR(500),ExactTargetID) FROM #ExactTargetID);

	   Truncate table Small_Acquisition.ExactTarget.Subscriber
       INSERT INTO Small_Acquisition.ExactTarget.Subscriber WITH (TABLOCK)
       SELECT *
       FROM Acquisition.ExactTarget.Subscriber
       WHERE SubscriberKey IN (SELECT CONVERT(VARCHAR(500),ExactTargetID) FROM #ExactTargetID);

	   Truncate table Small_Acquisition.ExactTarget.Unsubscribe
       INSERT INTO Small_Acquisition.ExactTarget.Unsubscribe WITH (TABLOCK)
       SELECT *
       FROM Acquisition.ExactTarget.Unsubscribe
       WHERE SubscriberKey IN (SELECT CONVERT(VARCHAR(500),ExactTargetID) FROM #ExactTargetID);
	   
	   Truncate table Small_Acquisition.ExactTargetPush.PushReport
       INSERT INTO Small_Acquisition.ExactTargetPush.PushReport WITH (TABLOCK)
       SELECT *
       FROM Acquisition.ExactTargetPush.PushReport
       WHERE SubscriberKey IN (SELECT CONVERT(VARCHAR(500),ExactTargetID) FROM #ExactTargetID);

	   Select distinct DeviceID into #DeviceID from Small_Acquisition.ExactTargetPush.PushReport

	   Truncate table Small_Acquisition.ExactTargetPush.PushTag
       INSERT INTO Small_Acquisition.ExactTargetPush.PushTag WITH (TABLOCK)
       SELECT *
       FROM Acquisition.ExactTargetPush.PushTag
       WHERE DeviceID IN (SELECT DeviceID FROM #DeviceID);

	   Truncate table Small_Acquisition.ExactTargetPush.PushAddress
       INSERT INTO Small_Acquisition.ExactTargetPush.PushAddress WITH (TABLOCK)
       SELECT *
       FROM Acquisition.ExactTargetPush.PushAddress
       WHERE DeviceID IN (SELECT DeviceID FROM #DeviceID);
	   
	   Truncate table Small_Acquisition.ExactTargetPush.PushSummary
       INSERT INTO Small_Acquisition.ExactTargetPush.PushSummary WITH (TABLOCK)
       SELECT *
       FROM Acquisition.ExactTargetPush.PushSummary
       WHERE MessageName IN (SELECT distinct MessageName FROM Small_Acquisition.ExactTargetPush.PushReport);

	   Truncate table Small_Acquisition.ExactTargetSms.SmsReport
       INSERT INTO Small_Acquisition.ExactTargetSms.SmsReport WITH (TABLOCK)
       SELECT *
       FROM Acquisition.ExactTargetSms.SmsReport
       WHERE SubscriberKey IN (SELECT CONVERT(VARCHAR(500),ExactTargetID) FROM #ExactTargetID);

	   Truncate table Small_Acquisition.ExactTargetSms.SmsAddress
       INSERT INTO Small_Acquisition.ExactTargetSms.SmsAddress WITH (TABLOCK)
       SELECT *
       FROM Acquisition.ExactTargetSms.SmsAddress
       WHERE MobileNumber IN (SELECT MobileNumber FROM Small_Acquisition.ExactTargetSms.SmsReport);

	   Truncate table Small_Acquisition.ExactTargetSms.SmsSubscriber
       INSERT INTO Small_Acquisition.ExactTargetSms.SmsSubscriber WITH (TABLOCK)
       SELECT *
       FROM Acquisition.ExactTargetSms.SmsSubscriber
       WHERE MobileNumber IN (SELECT MobileNumber FROM Small_Acquisition.ExactTargetSms.SmsReport);

	   Truncate table Small_Acquisition.ExactTargetSms.LifecycleSmsPush
       INSERT INTO Small_Acquisition.ExactTargetSms.LifecycleSmsPush WITH (TABLOCK)
       SELECT *
       FROM Acquisition.ExactTargetSms.LifecycleSmsPush
       WHERE AssetID IN (SELECT AssetID FROM Small_Acquisition.ExactTargetSms.SmsReport)
	   or AssetID IN (SELECT AssetID FROM Small_Acquisition.ExactTargetPush.PushReport);

END


BEGIN /* LOOKUP */
	
	Truncate table Small_Acquisition.IntraBet.Latency
	INSERT INTO Small_Acquisition.IntraBet.Latency WITH (TABLOCK)
	SELECT *
	FROM Acquisition.IntraBet.Latency
	Where OriginalDate between '2016-05-01' and '2016-05-31';
	
	Truncate table Small_Acquisition.IntraBet.tblLUAllupGroups
	INSERT INTO Small_Acquisition.IntraBet.tblLUAllupGroups WITH (TABLOCK)
	SELECT *
	FROM Acquisition.IntraBet.tblLUAllupGroups;
	
	Truncate table Small_Acquisition.IntraBet.tblLUCountry
	INSERT INTO Small_Acquisition.IntraBet.tblLUCountry WITH (TABLOCK)
	SELECT *
	FROM Acquisition.IntraBet.tblLUCountry;
	
	Truncate table Small_Acquisition.IntraBet.tblLUEventTypes
	INSERT INTO Small_Acquisition.IntraBet.tblLUEventTypes WITH (TABLOCK)
	SELECT *
	FROM Acquisition.IntraBet.tblLUEventTypes;
	
	Truncate table Small_Acquisition.IntraBet.tblLUHeardAboutUs
	INSERT INTO Small_Acquisition.IntraBet.tblLUHeardAboutUs WITH (TABLOCK)
	SELECT *
	FROM Acquisition.IntraBet.tblLUHeardAboutUs;
	
	Truncate table Small_Acquisition.IntraBet.tblLULedgers
	INSERT INTO Small_Acquisition.IntraBet.tblLULedgers WITH (TABLOCK)
	SELECT *
	FROM Acquisition.IntraBet.tblLULedgers;
	
	Truncate table Small_Acquisition.IntraBet.tblLUStates
	INSERT INTO Small_Acquisition.IntraBet.tblLUStates WITH (TABLOCK)
	SELECT *
	FROM Acquisition.IntraBet.tblLUStates;
	
	Truncate table Small_Acquisition.IntraBet.tblLUVenues
	INSERT INTO Small_Acquisition.IntraBet.tblLUVenues WITH (TABLOCK)
	SELECT *
	FROM Acquisition.IntraBet.tblLUVenues;
	
	Truncate table Small_Acquisition.IntraBet.CompleteDate
	INSERT INTO Small_Acquisition.IntraBet.CompleteDate WITH (TABLOCK)
	SELECT *
	FROM Acquisition.IntraBet.CompleteDate;

	Truncate table Small_Acquisition.Intrabet.tblLUBetTypes
	INSERT INTO Small_Acquisition.Intrabet.tblLUBetTypes WITH (TABLOCK)
	SELECT *
	FROM Acquisition.Intrabet.tblLUBetTypes;

	Truncate table Small_Acquisition.Intrabet.tblLUBetTypeGroups
	INSERT INTO Small_Acquisition.Intrabet.tblLUBetTypeGroups WITH (TABLOCK)
	SELECT *
	FROM Acquisition.Intrabet.tblLUBetTypeGroups;

	Truncate table Small_Acquisition.IntraBet.tblUsers
	INSERT INTO Small_Acquisition.IntraBet.tblUsers WITH (TABLOCK)
	SELECT *
	FROM Acquisition.IntraBet.tblUsers;

	Truncate table Small_Acquisition.IntraBet.tblPromotions
	INSERT INTO Small_Acquisition.IntraBet.tblPromotions WITH (TABLOCK)
	SELECT *
	FROM Acquisition.IntraBet.tblPromotions;

	Truncate table Small_Acquisition.Intrabet.tblLUSubSportTypes
	INSERT INTO Small_Acquisition.Intrabet.tblLUSubSportTypes
	SELECT *
	FROM Acquisition.Intrabet.tblLUSubSportTypes;

	Truncate table Small_Acquisition.Intrabet.tblLUPhoneColour
	INSERT INTO Small_Acquisition.Intrabet.tblLUPhoneColour
	SELECT *
	FROM Acquisition.Intrabet.tblLUPhoneColour;

	Truncate table Small_Acquisition.Intrabet.tblLUDebtHandlers
	INSERT INTO Small_Acquisition.Intrabet.tblLUDebtHandlers
	SELECT *
	FROM Acquisition.Intrabet.tblLUDebtHandlers;



END

END TRY
BEGIN CATCH

	THROW;
END CATCH

DBCC TRACEON (610, -1)
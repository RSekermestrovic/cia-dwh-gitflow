	IF OBJECT_ID(N'tempdb..#TempSQL') IS NOT NULL
	DROP TABLE #TempSQL;

	CREATE TABLE #TempSQL
	(
		ID INT IDENTITY NOT NULL,
		SQLStatement VARCHAR(255) NOT NULL
	);

	INSERT INTO #TempSQL
	SELECT 
		  'USE [' + d.name + N']' + CHAR(13) + CHAR(10) 
		+ 'DBCC SHRINKFILE (N''' + mf.name + N''' , 0, TRUNCATEONLY);' 
		+ CHAR(13) + CHAR(10) as [SQLStatement]
	FROM sys.master_files mf 
	INNER JOIN sys.databases d ON mf.database_id = d.database_id 
	WHERE d.database_id > 4
	AND mf.file_id = 2;

	DECLARE @counter INT = 1;
	DECLARE @max INT = (SELECT MAX(ID) FROM #TempSQL);
	DECLARE @SqlStatement varchar(255);

	WHILE @counter < @max
	BEGIN

		SET @SqlStatement = (SELECT SqlStatement FROM #TempSQL WHERE ID = @counter);

		PRINT @SqlStatement;
		EXECUTE(@SqlStatement);

		SET @counter = @counter + 1;
	END
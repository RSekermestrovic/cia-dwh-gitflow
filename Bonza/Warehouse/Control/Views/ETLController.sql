﻿CREATE VIEW [Control].[ETLController]

AS 

SELECT		BatchKey
			, BatchFromDate
			, BatchToDate
			, DimensionStatus
			, TransactionStatus
			, KPIFactStatus as KPIStatus
			, PositionFactStatus as PositionStatus
			, BalanceFactStatus as BalanceStatus
			, BatchStatus
FROM		[Control].[ETLController_SYNONYM]

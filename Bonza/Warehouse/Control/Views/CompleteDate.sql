﻿CREATE VIEW [Control].[CompleteDate]
	AS 
SELECT [TransactionCompleteDate]
      ,[KPICompleteDate]
      ,[PositionCompleteDate]
      ,[BalanceCompleteDate]
  FROM [$(Dimensional)].[Control].[CompleteDate]

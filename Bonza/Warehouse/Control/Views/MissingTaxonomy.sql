﻿Create View [Control].MissingTaxonomy

as

select		[Schemas].[name] as [Schema]
			, [Objects].name as [Table]
			, [Columns].name as [Column]
			, Taxonomy.[DisplayName]
			, Taxonomy.[Description]
From		sys.all_objects as [Objects]
			inner join sys.schemas as [Schemas] on [Objects].[schema_id] = [Schemas].[schema_id]
			inner join sys.all_columns as [Columns] on [Objects].[object_id] = [Columns].[object_id]
			Left join [Control].Taxonomy as Taxonomy on [Columns].name = Taxonomy.[DisplayName]
where		[Objects].[type] = 'V'
			and [Schemas].[name] in ('Fact', 'Dimension')
			and [Columns].name not in (
										'ModifiedBatchKey' --this is just provided for the cube processing
									  )
			and Taxonomy.[Description] is null
union
select		[Schemas].[name] as [Schema]
			, [Objects].name as [Table]
			, [Objects].name as [Column]
			, Taxonomy.[DisplayName]
			, Taxonomy.[Description]
From		sys.all_objects as [Objects]
			inner join sys.schemas as [Schemas] on [Objects].[schema_id] = [Schemas].[schema_id]
			Left join [Control].Taxonomy as Taxonomy on [Objects].name = Taxonomy.[DisplayName]
where		[Objects].[type] = 'V'
			and [Schemas].[name] in ('Fact', 'Dimension')
			and Taxonomy.[Description] is null
go
CREATE VIEW [Control].[Versions]
	
AS 

Select		*
From
			(
			SELECT		[Database]
						, ID
						, VersionID
						, DeployedOn
						, DeployedBy
			FROM	 	[$(Dimensional)].[Control].[Version]
			) as Data
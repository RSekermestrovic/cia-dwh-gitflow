﻿CREATE view [Fact].[KPI] as
SELECT k.[AccountKey]
      ,[AccountStatusKey]
      ,[ContractKey]
      ,[InstrumentKey]
      ,[StructureKey]
      ,[BetTypeKey]
      ,[LegTypeKey]
      ,[ChannelKey]
	  ,[ActionChannelKey]
	  ,[InPlayKey]
      ,[ClassKey]
      ,[EventKey]
      ,[MarketKey]
      ,[CampaignKey]
      ,[UserKey]
      ,z.[MasterDayKey] [DayKey]
      ,z.[AnalysisDayKey] [SydneyDayKey]
      ,[AccountOpened] AS AccountsOpened --CCIA=5219
      --,[FirstDeposit] AS FirstTimeDepositors --CCIA=5219
	  ,CASE WHEN b.AccountKey IS NOT NULL THEN NULL ELSE [FirstDeposit] END AS FirstTimeDepositors
      --,[FirstBet] AS FirstTimeBettors --CCIA=5219
	  ,CASE WHEN c.AccountKey IS NOT NULL THEN NULL ELSE [FirstBet] END AS FirstTimeBettors
      ,[DepositsRequestedCount]
      ,[DepositsCompletedCount]
      ,[DepositsRequested]
      ,[DepositsCompleted]
      ,[WithdrawalsRequestedCount]
      ,[WithdrawalsCompletedCount]
      ,[WithdrawalsRequested]
      ,[WithdrawalsCompleted]
      ,[Transfers]
      ,[Promotions]
      ,[BetsPlaced]
      ,[BonusBetsPlaced]
      ,[ContractsPlaced]
      ,[BonusContractsPlaced]
      ,[BettorsPlaced]
      ,[BonusBettorsPlaced]
      ,[PlayDaysPlaced]
      ,[BonusPlayDaysPlaced]
      ,[PlayFiscalWeeksPlaced]
      ,[BonusPlayFiscalWeeksPlaced]
      ,[PlayCalenderWeeksPlaced] AS PlayWeeksPlaced
      ,[BonusPlayCalenderWeeksPlaced] AS BonusPlayWeeksPlaced
      ,[PlayFiscalMonthsPlaced]
      ,[BonusPlayFiscalMonthsPlaced]
      ,[PlayCalenderMonthsPlaced] AS PlayMonthsPlaced
      ,[BonusPlayCalenderMonthsPlaced] AS BonusPlayMonthsPlaced
      ,[Stakes] AS TurnoverPlaced--CCIA=5219
      ,[BonusStakes] AS BonusTurnoverPlaced
      ,[Winnings] AS Payout--CCIA=5219
      ,[BettorsCashedOut]
	  ,[BetsCashedOut]
      ,[Cashout]
	  ,[CashoutDifferential]
	  ,[Fees]
      ,[BetsResulted] AS BetsSettled --CCIA=5219
      ,[BonusBetsResulted] AS BonusBetsSettled
      ,[ContractsResulted] AS ContractsSettled
      ,[BonusContractsResulted] AS BonusContractsSettled
      ,[BettorsResulted] AS BettorsSettled --CCIA=5219
      ,[BonusBettorsResulted] AS BonusBettorsSettled
      ,[PlayDaysResulted] AS PlayDaysSettled
      ,[BonusPlayDaysResulted] AS BonusPlayDaysSettled
      ,[PlayFiscalWeeksResulted] AS PlayFiscalWeeksSettled
      ,[BonusPlayFiscalWeeksResulted] AS BonusPlayFiscalWeeksSettled
      ,[PlayCalenderWeeksResulted] AS PlayWeeksSettled
      ,[BonusPlayCalenderWeeksResulted] AS BonusPlayWeeksSettled
      ,[PlayFiscalMonthsResulted] AS PlayFiscalMonthsSettled
      ,[BonusPlayFiscalMonthsResulted] AS BonusPlayFiscalMonthsSettled
      ,[PlayCalenderMonthsResulted] AS PlayMonthsSettled
      ,[BonusPlayCalenderMonthsResulted] AS BonusPlayMonthsSettled
      ,[Turnover] AS TurnoverSettled--CCIA=5219
      ,[BonusTurnover] AS BonusTurnoverSettled
      ,[GrossWin]
      ,[GrossWinAdjustment] AS GrosswinResettledAdjustment --CCIA=5219
      ,[BonusWinnings] AS BonusGrosswin --CCIA=5219
      ,[Adjustments]
      ,[NetRevenue]
      ,[NetRevenueAdjustment]
      ,[Betback] AS BetbackGrosswin--CCIA=5219
      --,[CreatedDate]
      --,[CreatedBatchKey]
      --,[CreatedBy]
      --,[ModifiedDate]
      ,k.[ModifiedBatchKey]
      --,[ModifiedBy]
  FROM [$(Dimensional)].[Fact].[KPI] k WITH (NOLOCK)
		INNER JOIN [$(Dimensional)].[Dimension].[DayZone] z WITH (NOLOCK) ON (z.DayKey = k.DayKey)
		LEFT OUTER JOIN [dbo].[FirstDeposit] b WITH (NOLOCK) on k.AccountKey = b.AccountKey and k.FirstDeposit is not null and z.[MasterDayKey] <> b.FirstDepositDayKey
		LEFT OUTER JOIN [dbo].[FirstBet] c WITH (NOLOCK) on k.AccountKey = c.AccountKey and k.FirstBet is not null and z.[MasterDayKey] <> c.FirstBetDayKey



GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique internal data warehouse key for Account dimension - only to be used for joining fact and dimension tables' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'AccountKey'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique internal data warehouse key for AccountStatus dimension - only to be used for joining fact and dimension tables' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'AccountStatusKey'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique internal data warehouse key for Contract dimension - only to be used for joining fact and dimension tables' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'ContractKey'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique internal data warehouse key for Instrument dimension - only to be used for joining fact and dimension tables' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'InstrumentKey'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique internal data warehouse key for Structure dimension - only to be used for joining fact and dimension tables' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'StructureKey'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique internal data warehouse key for BetType dimension - only to be used for joining fact and dimension tables' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'BetTypeKey'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique internal data warehouse key for LegType dimension - only to be used for joining fact and dimension tables' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'LegTypeKey'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique internal data warehouse key for Channel dimension - only to be used for joining fact and dimension tables' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'ChannelKey'
GO

EXEC sys.sp_addextendedproperty @level2name=N'ActionChannelKey', @value=N'Unique internal data warehouse key for Channel dimension which was used for the last action applied to the cashflow (as opposed to the base ChannelKey which is always the initiating channel for the everall flow - e.g. ChannelKey is channel bet was placed via, ActionChannelKey could be channel bet was cashed out via) - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'KPI', @level1type=N'VIEW', @level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique internal data warehouse key for InPlay dimension - only to be used for joining fact and dimension tables' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'InPlayKey'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique internal data warehouse key for Class dimension - only to be used for joining fact and dimension tables' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'ClassKey'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique internal data warehouse key for Event dimension - only to be used for joining fact and dimension tables' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'EventKey'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique internal data warehouse key for Market dimension - only to be used for joining fact and dimension tables' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'MarketKey'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique internal data warehouse key for Campaign dimension - only to be used for joining fact and dimension tables' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'CampaignKey'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique internal data warehouse key for User dimension - only to be used for joining fact and dimension tables' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'UserKey'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique internal data warehouse key for Day dimension - only to be used for joining fact and dimension tables' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'DayKey'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique internal data warehouse key aliasing Day dimension to provide metrics based on days defined by Sydney time as opposed to the default of Darwin time - only to be used for joining fact and dimension tables' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'SydneyDayKey'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Acounts Opened - NOTE, this column is a non-specific identifier to facilitate COUNT DISTINCT functionality to obtain the metric and should not be used in any other way.' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'AccountsOpened'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of First Time Depositors by Deposit request - NOTE, this column is a non-specific identifier to facilitate COUNT DISTINCT functionality to obtain the metric and should not be used in any other way.' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'FirstTimeDepositors'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of First Time Bettors by bet placement - NOTE, this column is a non-specific identifier to facilitate COUNT DISTINCT functionality to obtain the metric and should not be used in any other way.' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'FirstTimeBettors'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of deposits requested - NOTE, this column is a non-specific identifier to facilitate COUNT DISTINCT functionality to obtain the metric and should not be used in any other way.' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'DepositsRequestedCount'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of deposits completed - NOTE, this column is a non-specific identifier to facilitate COUNT DISTINCT functionality to obtain the metric and should not be used in any other way.' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'DepositsCompletedCount'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount of deposits requested' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'DepositsRequested'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount of deposits completed' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'DepositsCompleted'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The number of withdrawals requested - NOTE, this column is a non-specific identifier to facilitate COUNT DISTINCT functionality to obtain the metric and should not be used in any other way.' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'WithdrawalsRequestedCount'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of withdrawals completed - NOTE, this column is a non-specific identifier to facilitate COUNT DISTINCT functionality to obtain the metric and should not be used in any other way.' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'WithdrawalsCompletedCount'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The net amount of withdrawals: Requested Amount minus those Cancelled, Declined, Reversed or Rejected' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'WithdrawalsRequested'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount of withdrawals completed' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'WithdrawalsCompleted'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of transfers in or out of client balance' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'Transfers'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount of cash bonus given to clients' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'Promotions'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Allows for a distinct count of the number of cash Bets Placed - NOTE, this column is a non-specific identifier to facilitate COUNT DISTINCT functionality to obtain the metric and should not be used in any other way.' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'BetsPlaced'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Allows for a distinct count of the number of bonus Bets Placed - NOTE, this column is a non-specific identifier to facilitate COUNT DISTINCT functionality to obtain the metric and should not be used in any other way.' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'BonusBetsPlaced'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Allows for a distinct count of the number of cash Bet Contracts Placed (counting full cover bets as one) - NOTE, this column is a non-specific identifier to facilitate COUNT DISTINCT functionality to obtain the metric and should not be used in any other way.' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'ContractsPlaced'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Allows for a distinct count of the number of bonus Bet Contracts Placed (counting full cover bets as one) - NOTE, this column is a non-specific identifier to facilitate COUNT DISTINCT functionality to obtain the metric and should not be used in any other way.' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'BonusContractsPlaced'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Allows for a distinct count of the number of cash bettors by placement - NOTE, this column is a non-specific identifier to facilitate COUNT DISTINCT functionality to obtain the metric and should not be used in any other way.' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'BettorsPlaced'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Allows for a distinct count of the number of bonus bettors by placement - NOTE, this column is a non-specific identifier to facilitate COUNT DISTINCT functionality to obtain the metric and should not be used in any other way.' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'BonusBettorsPlaced'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of active days based on cash bets placed per bettor. Must be used as a distinct count - NOTE, this column is a non-specific identifier to facilitate COUNT DISTINCT functionality to obtain the metric and should not be used in any other way.' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'PlayDaysPlaced'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of active days based on bonus bets placed per bettor. Must be used as a distinct count - NOTE, this column is a non-specific identifier to facilitate COUNT DISTINCT functionality to obtain the metric and should not be used in any other way.' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'BonusPlayDaysPlaced'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of active fiscal weeks based on cash bets placed per bettor. Must be used as a distinct count - NOTE, this column is a non-specific identifier to facilitate COUNT DISTINCT functionality to obtain the metric and should not be used in any other way.' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'PlayFiscalWeeksPlaced'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of active fiscal weeks based on bonus bets placed per bettor. Must be used as a distinct count - NOTE, this column is a non-specific identifier to facilitate COUNT DISTINCT functionality to obtain the metric and should not be used in any other way.' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'BonusPlayFiscalWeeksPlaced'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of active calendar weeks based on cash bets placed per bettor. Must be used as a distinct count - NOTE, this column is a non-specific identifier to facilitate COUNT DISTINCT functionality to obtain the metric and should not be used in any other way.' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'PlayWeeksPlaced'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of active calendar weeks based on bonus bets placed per bettor. Must be used as a distinct count - NOTE, this column is a non-specific identifier to facilitate COUNT DISTINCT functionality to obtain the metric and should not be used in any other way.' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'BonusPlayWeeksPlaced'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of active fiscal months based on cash bets placed per bettor. Must be used as a distinct count - NOTE, this column is a non-specific identifier to facilitate COUNT DISTINCT functionality to obtain the metric and should not be used in any other way.' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'PlayFiscalMonthsPlaced'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of active fiscal months based on bonus bets placed per bettor. Must be used as a distinct count - NOTE, this column is a non-specific identifier to facilitate COUNT DISTINCT functionality to obtain the metric and should not be used in any other way.' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'BonusPlayFiscalMonthsPlaced'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of active calendar months based on cash bets placed per bettor. Must be used as a distinct count - NOTE, this column is a non-specific identifier to facilitate COUNT DISTINCT functionality to obtain the metric and should not be used in any other way.' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'PlayMonthsPlaced'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of active calendar months based on bonus bets placed per bettor. Must be used as a distinct count - NOTE, this column is a non-specific identifier to facilitate COUNT DISTINCT functionality to obtain the metric and should not be used in any other way.' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'BonusPlayMonthsPlaced'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total stake value of cash bets at time they are placed' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'TurnoverPlaced'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total stake value of bonus bets at time they are placed' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'BonusTurnoverPlaced'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total amount paid out to the client after the bet is settled or cashed out' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'Payout'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Allows for a distinct count of the number of bettors who cashed out their bets - NOTE, this column is a non-specific identifier to facilitate COUNT DISTINCT functionality to obtain the metric and should not be used in any other way.' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'BettorsCashedOut'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Allows for a distinct count of the number of bets cashed out - NOTE, this column is a non-specific identifier to facilitate COUNT DISTINCT functionality to obtain the metric and should not be used in any other way.' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'BetsCashedOut'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount paid out on cashed out bets' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'Cashout'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The difference between the amount actually cashed out and the amount that would have had to be paid on the bet if it hadn''t cashed out' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'CashoutDifferential'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Allows for a distinct count of the number of cash Bets Settled - NOTE, this column is a non-specific identifier to facilitate COUNT DISTINCT functionality to obtain the metric and should not be used in any other way.' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'BetsSettled'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Allows for a distinct count of the number of bonus Bets Settled - NOTE, this column is a non-specific identifier to facilitate COUNT DISTINCT functionality to obtain the metric and should not be used in any other way.' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'BonusBetsSettled'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Allows for a distinct count of the number of cash Bet Contracts Settled (counting full cover bets as one) - NOTE, this column is a non-specific identifier to facilitate COUNT DISTINCT functionality to obtain the metric and should not be used in any other way.' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'ContractsSettled'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Allows for a distinct count of the number of bonus Bet Contracts Settled (counting full cover bets as one) - NOTE, this column is a non-specific identifier to facilitate COUNT DISTINCT functionality to obtain the metric and should not be used in any other way.' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'BonusContractsSettled'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Allows for a distinct count of the number of cash bettors by settlement - NOTE, this column is a non-specific identifier to facilitate COUNT DISTINCT functionality to obtain the metric and should not be used in any other way.' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'BettorsSettled'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Allows for a distinct count of the number of bonus bettors by settlement - NOTE, this column is a non-specific identifier to facilitate COUNT DISTINCT functionality to obtain the metric and should not be used in any other way.' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'BonusBettorsSettled'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of active days based on cash bets settled per bettor. Must be used as a distinct count - NOTE, this column is a non-specific identifier to facilitate COUNT DISTINCT functionality to obtain the metric and should not be used in any other way.' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'PlayDaysSettled'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of active days based on bonus bets settled per bettor. Must be used as a distinct count - NOTE, this column is a non-specific identifier to facilitate COUNT DISTINCT functionality to obtain the metric and should not be used in any other way.' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'BonusPlayDaysSettled'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of active fiscal weeks based on cash bets settled per bettor. Must be used as a distinct count - NOTE, this column is a non-specific identifier to facilitate COUNT DISTINCT functionality to obtain the metric and should not be used in any other way.' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'PlayFiscalWeeksSettled'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of active fiscal weeks based on bonus bets settled per bettor. Must be used as a distinct count - NOTE, this column is a non-specific identifier to facilitate COUNT DISTINCT functionality to obtain the metric and should not be used in any other way.' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'BonusPlayFiscalWeeksSettled'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of active calendar weeks based on cash bets settled per bettor. Must be used as a distinct count - NOTE, this column is a non-specific identifier to facilitate COUNT DISTINCT functionality to obtain the metric and should not be used in any other way.' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'PlayWeeksSettled'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of active calendar weeks based on bonus bets settled per bettor. Must be used as a distinct count - NOTE, this column is a non-specific identifier to facilitate COUNT DISTINCT functionality to obtain the metric and should not be used in any other way.' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'BonusPlayWeeksSettled'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of active fiscal months based on cash bets settled per bettor. Must be used as a distinct count - NOTE, this column is a non-specific identifier to facilitate COUNT DISTINCT functionality to obtain the metric and should not be used in any other way.' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'PlayFiscalMonthsSettled'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of active fiscal months based on bonus bets settled per bettor. Must be used as a distinct count - NOTE, this column is a non-specific identifier to facilitate COUNT DISTINCT functionality to obtain the metric and should not be used in any other way.' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'BonusPlayFiscalMonthsSettled'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of active calendar months based on cash bets settled per bettor. Must be used as a distinct count - NOTE, this column is a non-specific identifier to facilitate COUNT DISTINCT functionality to obtain the metric and should not be used in any other way.' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'PlayMonthsSettled'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of active calendar months based on bonus bets settled per bettor. Must be used as a distinct count - NOTE, this column is a non-specific identifier to facilitate COUNT DISTINCT functionality to obtain the metric and should not be used in any other way.' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'BonusPlayMonthsSettled'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total stake value of cash bets at time they are settled' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'TurnoverSettled'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total stake value of bonus bets at time they are settled' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'BonusTurnoverSettled'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The net financial gain (or loss if negative) to the company when a cash bet settles - equal to the bet turnover minus any payout' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'GrossWin'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Difference between the grosswin from initial settlement and the grosswin from resettled bet' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'GrosswinResettledAdjustment'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The net financial loss to the company when a bonus bet settles - equal to minus the amount of any payout on the bonus bet, otherwise zero, as the turnover amount on a bonus bet was never real money and therefore settlement of the turnover amount doesnt benefit the company in any way, only payout amounts contribute to a negative gross win whn present' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'BonusGrosswin'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount of any financial Adjustments that are not Bonuses' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'Adjustments'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Gross Win for cash bets less bonus bet payout (which is the negative value for BonusGrossWin)' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'NetRevenue'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The value of subsequent settlement adjustments after initial settlement on the net revenue figure (subtracting this figure from the NetRevenue figure will derive the Net Revenue at first settlement)' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'NetRevenueAdjustment'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cost of Betbacks when it is negative and Grosswin on Betbacks when it is positive' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI', @level2type=N'COLUMN',@level2name=N'BetbackGrosswin'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fact table defining cash movement metrics on the cashflow model. Grain = 1 record per Transaction, per BalanceType, per Wallet, per timestamp (any transaction will be double-entry in this model, i.e. minus the balance it is moving out of and plus the balance it is moving in to, further broken down by cash and bonus wallets)' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'KPI'
GO



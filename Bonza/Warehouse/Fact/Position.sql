﻿CREATE VIEW [Fact].[Position] AS
SELECT p.[AccountKey]
	  ,[AccountStatusKey]
      ,[BalanceTypeKey]
      ,[StructureKey]
      ,[ActivityKey]
	  ,[RevenueLifeCycleKey]
	  ,[LifeStageKey] AS [LifecycleKey]
	  ,[PreviousLifeStageKey] AS [PreviousLifecycleKey]
	  ,[TierKey] AS [ValueTierKey]
	  ,[TierKeyFiscal] AS [FiscalValueTierKey]
      ,z.[MasterDayKey] [DayKey]
      ,z.[AnalysisDayKey] [SydneyDayKey]
      --,fd.[MasterDayKey] [FirstDepositDayKey]
      --,fd.[AnalysisDayKey] [SydneyFirstDepositDayKey]
      --,fb.[MasterDayKey] [FirstBetDayKey]
      --,fb.[AnalysisDayKey] [SydneyFirstBetDayKey]
      --,[FirstBetTypeKey]
      --,[FirstClassKey] AS FirstBetClassKey --CCIA_6168
      --,[FirstChannelKey] AS FirstBetChannelKey --CCIA_6168
	  ,CASE WHEN afd.AccountKey IS NOT NULL THEN afd.FirstDepositDayKey ELSE fd.[MasterDayKey] END AS [FirstDepositDayKey]
      ,CASE WHEN afd.AccountKey IS NOT NULL THEN afd.FirstDepositDayKey ELSE fd.[AnalysisDayKey] END AS [SydneyFirstDepositDayKey]
      ,CASE WHEN afb.AccountKey IS NOT NULL THEN afb.FirstBetDayKey ELSE fb.[MasterDayKey] END AS [FirstBetDayKey]
      ,CASE WHEN afb.AccountKey IS NOT NULL THEN afb.FirstBetDayKey ELSE fb.[AnalysisDayKey] END AS [SydneyFirstBetDayKey]
      ,CASE WHEN afb.AccountKey IS NOT NULL THEN afb.FirstBetTypeKey ELSE p.FirstBetTypeKey END AS [FirstBetTypeKey]
      ,CASE WHEN afb.AccountKey IS NOT NULL THEN afb.FirstBetClassKey ELSE p.[FirstClassKey] END AS FirstBetClassKey --CCIA_6168
      ,CASE WHEN afb.AccountKey IS NOT NULL THEN afb.FirstBetChannelKey ELSE p.[FirstChannelKey] END AS FirstBetChannelKey --CCIA_6168
      ,ld.[MasterDayKey] [LastDepositDayKey]
      ,ld.[AnalysisDayKey] [SydneyLastDepositDayKey]
      ,lb.[MasterDayKey] [LastBetDayKey]
      ,lb.[AnalysisDayKey] [SydneyLastBetDayKey]
      ,[LastBetTypeKey]
      ,[LastClassKey] AS LastBetClassKey --CCIA_6168
      ,[LastChannelKey] AS LastBetChannelKey --CCIA_6168
      --,[LedgerID]
      --,[LedgerSource]
      ,[OpeningBalance]
      ,[TransactedAmount] AS [TotalTransactedAmount]
      ,[ClosingBalance]
	  ,[TurnoverMTDCalender] AS [MTDTurnover]
	  ,[TurnoverMTDFiscal] AS [FiscalMTDTurnover]
      ,[LifetimeTurnover]
      ,[LifetimeGrossWin]
	  ,[LifetimeBetCount]
	  ,[LifetimeBonus]
	  ,[LifetimeDeposits]
	  ,[LifetimeWithdrawals]
	  --,DaysSinceAccountOpened
	  --,DaysSinceFirstBet
	  --,DaysSinceFirstDeposit
	  --,DayssinceLastBet
	  --,DaysSinceLastDeposit
      --,[CreatedDate]
      --,[CreatedBatchKey]
      --,[ModifiedDate]
      ,p.[ModifiedBatchKey]
  FROM [$(Dimensional)].Fact.[Position] p WITH (NOLOCK)
		INNER JOIN [$(Dimensional)].[Dimension].[DayZone] z WITH (NOLOCK) ON (z.DayKey = p.DayKey)
		INNER JOIN [$(Dimensional)].[Dimension].[DayZone] fd WITH (NOLOCK) ON (fd.DayKey = p.FirstDepositDayKey)
		INNER JOIN [$(Dimensional)].[Dimension].[DayZone] fb WITH (NOLOCK) ON (fb.DayKey = p.FirstBetDayKey)
		INNER JOIN [$(Dimensional)].[Dimension].[DayZone] ld WITH (NOLOCK) ON (ld.DayKey = p.LastDepositDayKey)
		INNER JOIN [$(Dimensional)].[Dimension].[DayZone] lb WITH (NOLOCK) ON (lb.DayKey = p.LastBetDayKey)
		----NEW
		LEFT OUTER JOIN [dbo].[FirstBet] afb WITH (NOLOCK) ON (p.AccountKey = afb.AccountKey)
		LEFT OUTER JOIN [dbo].[FirstDeposit] afd WITH (NOLOCK) ON (p.AccountKey = afd.AccountKey)


GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique internal data warehouse key for Account dimension - only to be used for joining fact and dimension tables' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'Position', @level2type=N'COLUMN',@level2name=N'AccountKey'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique internal data warehouse key for AccountStatus dimension - only to be used for joining fact and dimension tables' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'Position', @level2type=N'COLUMN',@level2name=N'AccountStatusKey'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique internal data warehouse key for BalanceType dimension - only to be used for joining fact and dimension tables' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'Position', @level2type=N'COLUMN',@level2name=N'BalanceTypeKey'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique internal data warehouse key for Structure dimension - only to be used for joining fact and dimension tables' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'Position', @level2type=N'COLUMN',@level2name=N'StructureKey'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique internal data warehouse key for Activity dimension - only to be used for joining fact and dimension tables' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'Position', @level2type=N'COLUMN',@level2name=N'ActivityKey'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique internal data warehouse key for RevenueLifecycle dimension - only to be used for joining fact and dimension tables' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'Position', @level2type=N'COLUMN',@level2name=N'RevenueLifeCycleKey'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique internal data warehouse key for Lifecycle dimension - only to be used for joining fact and dimension tables' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'Position', @level2type=N'COLUMN',@level2name=N'LifecycleKey'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique internal data warehouse key aliasing Lifecycle dimension to link to clients previous stage in Lifecycle model - only to be used for joining fact and dimension tables' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'Position', @level2type=N'COLUMN',@level2name=N'PreviousLifecycleKey'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique internal data warehouse key for ValueTier dimension - only to be used for joining fact and dimension tables' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'Position', @level2type=N'COLUMN',@level2name=N'ValueTierKey'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique internal data warehouse key aliasing ValueTier dimension to link to a value tier aligned to fiscal periods as opposed to the default of calendar periods - only to be used for joining fact and dimension tables' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'Position', @level2type=N'COLUMN',@level2name=N'FiscalValueTierKey'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique internal data warehouse key for Day dimension - only to be used for joining fact and dimension tables' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'Position', @level2type=N'COLUMN',@level2name=N'DayKey'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique internal data warehouse key aliasing Day dimension to provide metrics based on days defined by Sydney time as opposed to the default of Darwin time - only to be used for joining fact and dimension tables' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'Position', @level2type=N'COLUMN',@level2name=N'SydneyDayKey'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique internal data warehouse key aliasing Day dimension to provide the day the client first deposited funds into their account - only to be used for joining fact and dimension tables' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'Position', @level2type=N'COLUMN',@level2name=N'FirstDepositDayKey'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique internal data warehouse key aliasing Day dimension to provide the day the client first deposited funds into their account defined by Sydney time as opposed to the default of Darwin time - only to be used for joining fact and dimension tables' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'Position', @level2type=N'COLUMN',@level2name=N'SydneyFirstDepositDayKey'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique internal data warehouse key aliasing Day dimension to provide the day the client first bet - only to be used for joining fact and dimension tables' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'Position', @level2type=N'COLUMN',@level2name=N'FirstBetDayKey'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique internal data warehouse key aliasing Day dimension to provide the day the client first bet defined by Sydney time as opposed to the default of Darwin time - only to be used for joining fact and dimension tables' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'Position', @level2type=N'COLUMN',@level2name=N'SydneyFirstBetDayKey'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique internal data warehouse key aliasing BetType dimension to provide the type of the client first bet - only to be used for joining fact and dimension tables' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'Position', @level2type=N'COLUMN',@level2name=N'FirstBetTypeKey'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique internal data warehouse key aliasing Class dimension to provide the class of the client first bet - only to be used for joining fact and dimension tables' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'Position', @level2type=N'COLUMN',@level2name=N'FirstBetClassKey'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique internal data warehouse key aliasing Channel dimension to provide the channel used for the client first bet - only to be used for joining fact and dimension tables' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'Position', @level2type=N'COLUMN',@level2name=N'FirstBetChannelKey'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique internal data warehouse key aliasing Day dimension to provide the day the client last deposited funds into their account - only to be used for joining fact and dimension tables' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'Position', @level2type=N'COLUMN',@level2name=N'LastDepositDayKey'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique internal data warehouse key aliasing Day dimension to provide the day the client last deposited funds into their account defined by Sydney time as opposed to the default of Darwin time - only to be used for joining fact and dimension tables' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'Position', @level2type=N'COLUMN',@level2name=N'SydneyLastDepositDayKey'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique internal data warehouse key aliasing Day dimension to provide the day the client last bet - only to be used for joining fact and dimension tables' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'Position', @level2type=N'COLUMN',@level2name=N'LastBetDayKey'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique internal data warehouse key aliasing Day dimension to provide the day the client last bet defined by Sydney time as opposed to the default of Darwin time - only to be used for joining fact and dimension tables' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'Position', @level2type=N'COLUMN',@level2name=N'SydneyLastBetDayKey'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique internal data warehouse key aliasing BetType dimension to provide the type of the client latest bet - only to be used for joining fact and dimension tables' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'Position', @level2type=N'COLUMN',@level2name=N'LastBetTypeKey'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique internal data warehouse key aliasing Class dimension to provide the class of the client latest bet - only to be used for joining fact and dimension tables' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'Position', @level2type=N'COLUMN',@level2name=N'LastBetClassKey'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique internal data warehouse key aliasing Channel dimension to provide the channel used for the client latest bet - only to be used for joining fact and dimension tables' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'Position', @level2type=N'COLUMN',@level2name=N'LastBetChannelKey'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Client balance amount at the start of the day' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'Position', @level2type=N'COLUMN',@level2name=N'OpeningBalance'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The net total transacted amount against the client balance for the entire day, or in the case of the current day, net total transacted amount as of the last time the Data Warehouse load processes were run' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'Position', @level2type=N'COLUMN',@level2name=N'TotalTransactedAmount'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Balance amount at the end of the day, or in the case of the current day, latest balance as of the last time the Data Warehouse load processes were run' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'Position', @level2type=N'COLUMN',@level2name=N'ClosingBalance'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total bet settlement turnover calendar month to date, as of the last time the Data Warehouse load processes ran' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'Position', @level2type=N'COLUMN',@level2name=N'MTDTurnover'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total bet settlement turnover fiscal month to date, as of the last time the Data Warehouse load processes ran' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'Position', @level2type=N'COLUMN',@level2name=N'FiscalMTDTurnover'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total bet settlement turnover for cash bets during the life of the client account ledger, as of the last time the Data Warehouse load processes ran' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'Position', @level2type=N'COLUMN',@level2name=N'LifetimeTurnover'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total bet settlement gross win (turnover minus payut) for cash bets during the life of the client account ledger, as of the last time the Data Warehouse load processes ran' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'Position', @level2type=N'COLUMN',@level2name=N'LifetimeGrossWin'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total number of cash and bonus bets placed during the life of the client account ledger, as of the last time the Data Warehouse load processes ran' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'Position', @level2type=N'COLUMN',@level2name=N'LifetimeBetCount'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total value of bonus payments during the life of the client account ledger, as of the last time the Data Warehouse load processes ran' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'Position', @level2type=N'COLUMN',@level2name=N'LifetimeBonus'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total value of deposits completed to the client balance during the life of the client account ledger, as of the last time the Data Warehouse load processes ran' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'Position', @level2type=N'COLUMN',@level2name=N'LifetimeDeposits'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total value of withdrawals completed from the client balance during the life of the client account ledger, as of the last time the Data Warehouse load processes ran' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'Position', @level2type=N'COLUMN',@level2name=N'LifetimeWithdrawals'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fact table defining daily client position in terms of lifetime and periodic events and balances. Grain = 1 record per Ledger, per Day' , @level0type=N'SCHEMA',@level0name=N'Fact', @level1type=N'VIEW',@level1name=N'Position'
GO



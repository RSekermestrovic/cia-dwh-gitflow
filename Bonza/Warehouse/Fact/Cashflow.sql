﻿Create view [Fact].[Cashflow] as
select [AccountKey]
	  ,[AccountStatusKey]
      ,[BalanceTypeKey]
      ,[WalletKey]
      ,[TransactionDetailKey]
      ,[ContractKey]
      ,[InstrumentKey]
      ,[StructureKey]
      ,[BetTypeKey]
      ,[LegTypeKey]
	  ,[PriceTypeKey]
      ,[ChannelKey]
      ,[ActionChannelKey]
	  ,CASE WHEN [BetTypeKey] = 0 THEN 0 ELSE [InPlayKey] END [InPlayKey] -- Note this logic is included to resolve an early issue with InPlay whereby it was set to 'No' for non-bet transactions rather than 'N/A', without having to reload the Cashflow (Transaction) fact. This logic is not replicated in the KPI fact because it has since been reloaded and the underlying issue resolved in the base table
      ,[ClassKey]
	  ,[EventKey]
      ,[MarketKey]
 	  ,[PromotionKey]
      ,[CampaignKey] 
      ,[UserKey]
	  ,CASE WHEN [NoteKey] = 0 THEN 0 ELSE [TransactionId] END as [NoteKey] -- CCIA-7063
      ,z.[MasterDayKey] [DayKey]
      ,z.[AnalysisDayKey] [SydneyDayKey]
      ,[MasterTimeKey] as [TimeKey] --CCIA-6170
      ,[AnalysisTimeKey] as [SydneyTimeKey] --CCIA-6170
      ,[MasterTransactionTimestamp] as [TransactionTimestamp] --CCIA-6170
      ,[AnalysisTransactionTimestamp] as [SydneyTransactionTimestamp] --CCIA-6170
      --,[LedgerID]
	  ,[TransactionId] AS [Transactions]
      --,[TransactionIdSource]
      ,[TransactedAmount]
      --,[FromDate]
      --,[CreatedDate]
      ,t.[CreatedBatchKey] as ModifiedBatchKey
      --,[CreatedBy]
  FROM [$(Dimensional)].Fact.[Transaction] t with (nolock)
		INNER JOIN [$(Dimensional)].[Dimension].[DayZone] z WITH (NOLOCK) ON (z.DayKey = t.DayKey)
  go

EXEC sys.sp_addextendedproperty @level1name=N'Cashflow', @value=N'Fact table defining cash movement metrics on the cashflow model. Grain = 1 record per Transaction, per BalanceType, per Wallet, per timestamp (any transaction will be double-entry in this model, i.e. minus the balance it is moving out of and plus the balance it is moving in to, further broken down by cash and bonus wallets)' ,@level1type=N'VIEW' ,@level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO

EXEC sys.sp_addextendedproperty @level2name=N'AccountKey', @value=N'Unique internal data warehouse key for Account dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Cashflow', @level1type=N'VIEW', @level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'AccountStatusKey', @value=N'Unique internal data warehouse key for AccountStatus dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Cashflow', @level1type=N'VIEW', @level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'BalanceTypeKey', @value=N'Unique internal data warehouse key for BalanceType dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Cashflow', @level1type=N'VIEW', @level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'WalletKey', @value=N'Unique internal data warehouse key for Wallet dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Cashflow', @level1type=N'VIEW', @level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'TransactionDetailKey', @value=N'Unique internal data warehouse key for TransactionDetail dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Cashflow', @level1type=N'VIEW', @level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'ContractKey', @value=N'Unique internal data warehouse key for Contract dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Cashflow', @level1type=N'VIEW', @level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'InstrumentKey', @value=N'Unique internal data warehouse key for Instrument dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Cashflow', @level1type=N'VIEW', @level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'StructureKey', @value=N'Unique internal data warehouse key for Structure dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Cashflow', @level1type=N'VIEW', @level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'BetTypeKey', @value=N'Unique internal data warehouse key for BetType dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Cashflow', @level1type=N'VIEW', @level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'LegTypeKey', @value=N'Unique internal data warehouse key for LegType dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Cashflow', @level1type=N'VIEW', @level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'PriceTypeKey', @value=N'Unique internal data warehouse key for PriceType dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Cashflow', @level1type=N'VIEW', @level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'ChannelKey', @value=N'Unique internal data warehouse key for Channel dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Cashflow', @level1type=N'VIEW', @level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'ActionChannelKey', @value=N'Unique internal data warehouse key for Channel dimension which was used for the last action applied to the cashflow (as opposed to the base ChannelKey which is always the initiating channel for the everall flow - e.g. ChannelKey is channel bet was placed via, ActionChannelKey could be channel bet was cashed out via) - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Cashflow', @level1type=N'VIEW', @level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'InPlayKey', @value=N'Unique internal data warehouse key for InPlay dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Cashflow', @level1type=N'VIEW', @level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'ClassKey', @value=N'Unique internal data warehouse key for Class dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Cashflow', @level1type=N'VIEW', @level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'EventKey', @value=N'Unique internal data warehouse key for Event dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Cashflow', @level1type=N'VIEW', @level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'MarketKey', @value=N'Unique internal data warehouse key for Market dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Cashflow', @level1type=N'VIEW', @level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'PromotionKey', @value=N'Unique internal data warehouse key for Promotion dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Cashflow', @level1type=N'VIEW', @level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'CampaignKey', @value=N'Unique internal data warehouse key for Campaign dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Cashflow', @level1type=N'VIEW', @level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'UserKey', @value=N'Unique internal data warehouse key for User dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Cashflow', @level1type=N'VIEW', @level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'NoteKey', @value=N'Unique internal data warehouse key for Note dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Cashflow', @level1type=N'VIEW', @level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'DayKey', @value=N'Unique internal data warehouse key for Day dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Cashflow', @level1type=N'VIEW', @level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'SydneyDayKey', @value=N'Unique internal data warehouse key aliasing Day dimension to provide metrics based on days defined by Sydney time as opposed to the default of Darwin time - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Cashflow', @level1type=N'VIEW', @level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'TimeKey', @value=N'Unique internal data warehouse key for Time dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Cashflow', @level1type=N'VIEW', @level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'SydneyTimeKey', @value=N'Unique internal data warehouse key aliasing Time dimension to provide metrics based on Sydney time as opposed to the default of Darwin time - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Cashflow', @level1type=N'VIEW', @level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'TransactionTimestamp', @value=N'Date and time, accurate to 1000th of a second (when available) of the transaction', @level2type=N'COLUMN' ,@level1name=N'Cashflow', @level1type=N'VIEW', @level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'SydneyTransactionTimestamp', @value=N'Date and time, accurate to 1000th of a second (when available) of the transaction based on Sydney time as opposed to the default of Darwin time', @level2type=N'COLUMN' ,@level1name=N'Cashflow', @level1type=N'VIEW', @level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'Transactions', @value=N'Allows for the application of a COUNT(DISTINCT) function to obtain the number of distinct transactions - NOTE, this column is a non-specific identifier to facilitate COUNT DISTINCT functionality to obtain the metric and should not be used in any other way.', @level2type=N'COLUMN' ,@level1name=N'Cashflow', @level1type=N'VIEW', @level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'TransactedAmount', @value=N'The amounted transacted against the balance, signed to reflect debit or credit of the balance', @level2type=N'COLUMN' ,@level1name=N'Cashflow', @level1type=N'VIEW', @level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO

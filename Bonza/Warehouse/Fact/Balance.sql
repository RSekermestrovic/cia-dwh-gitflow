﻿CREATE VIEW [Fact].[Balance] as
SELECT [AccountKey],
	  [AccountStatusKey]
      ,[BalanceTypeKey]
      ,[WalletKey]
      ,[StructureKey]
      ,z.[MasterDayKey] [DayKey]
      ,z.[AnalysisDayKey] [SydneyDayKey]
--      ,[LedgerID]
--      ,[LedgerSource]
      ,[OpeningBalance]
      ,[TotalTransactedAmount]
      ,[ClosingBalance]
      ,b.[ModifiedBatchKey]
  FROM [$(Dimensional)].[Fact].[Balance] b with (nolock)
		INNER JOIN [$(Dimensional)].[Dimension].[DayZone] z WITH (NOLOCK) ON (z.DayKey = b.DayKey)
GO

EXEC sys.sp_addextendedproperty @level1name=N'Balance', @value=N'Fact table defining Balance metrics on the cashflow model. Grain = 1 record per Ledger, per Wallet, per BalanceType, per Day' ,@level1type=N'VIEW' ,@level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO

EXEC sys.sp_addextendedproperty @level2name=N'AccountKey', @value=N'Unique internal data warehouse key for Account dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Balance', @level1type=N'VIEW', @level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'AccountStatusKey', @value=N'Unique internal data warehouse key for AccountStatus dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Balance', @level1type=N'VIEW', @level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'BalanceTypeKey', @value=N'Unique internal data warehouse key for BalanceType dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Balance', @level1type=N'VIEW', @level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'WalletKey', @value=N'Unique internal data warehouse key for Wallet dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Balance', @level1type=N'VIEW', @level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'StructureKey', @value=N'Unique internal data warehouse key for Structure dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Balance', @level1type=N'VIEW', @level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'DayKey', @value=N'Unique internal data warehouse key for Day dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Balance', @level1type=N'VIEW', @level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'SydneyDayKey', @value=N'Unique internal data warehouse key aliasing Day dimension to provide metrics based on days defined by Sydney time as opposed to the default of Darwin time - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Balance', @level1type=N'VIEW', @level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'OpeningBalance', @value=N'Balance amount at the start of the day', @level2type=N'COLUMN' ,@level1name=N'Balance', @level1type=N'VIEW', @level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'TotalTransactedAmount', @value=N'The net total transacted amount against the balance for the entire day, or in the case of the current day, net total transacted amount as of the last time the Data Warehouse load processes were run', @level2type=N'COLUMN' ,@level1name=N'Balance', @level1type=N'VIEW', @level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'ClosingBalance', @value=N'Balance amount at the end of the day, or in the case of the current day, latest balance as of the last time the Data Warehouse load processes were run', @level2type=N'COLUMN' ,@level1name=N'Balance', @level1type=N'VIEW', @level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO

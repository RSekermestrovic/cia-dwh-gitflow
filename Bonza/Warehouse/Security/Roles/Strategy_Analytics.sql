﻿CREATE ROLE [Strategy_Analytics]
go
Grant Select on schema :: Fact to [Strategy_Analytics]
Go
Grant Select on schema :: Dimension to [Strategy_Analytics]
Go

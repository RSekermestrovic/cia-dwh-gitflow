﻿CREATE ROLE [Commercial]
go
Grant Select on schema :: Fact to [Commercial]
Go
Grant Select on schema :: Dimension to [Commercial]
Go

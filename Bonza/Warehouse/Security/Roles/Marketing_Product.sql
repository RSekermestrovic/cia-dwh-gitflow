﻿CREATE ROLE [Marketing_Product]
go
Grant Select on schema :: Fact to [Marketing_Product]
Go
Grant Select on schema :: Dimension to [Marketing_Product]
Go

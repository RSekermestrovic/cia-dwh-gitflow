﻿CREATE ROLE [Finance_Compliance_Legal]
go
Grant Select on schema :: Fact to [Finance_Compliance_Legal]
Go
Grant Select on schema :: Dimension to [Finance_Compliance_Legal]
Go

﻿CREATE LOGIN SSISAdmin WITH PASSWORD=N'iXC7V0t4kLONgrfZntlvBdTaMUr3BG/pJsBp0KuJRu0=', DEFAULT_DATABASE=[$(DatabaseName)], DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF
Go
CREATE USER SSISAdmin FOR LOGIN SSISAdmin WITH DEFAULT_SCHEMA=[dbo]
Go
EXEC sp_addrolemember N'db_ddladmin', N'SSISAdmin'
Go
EXEC sp_addrolemember N'db_datawriter', N'SSISAdmin'
Go
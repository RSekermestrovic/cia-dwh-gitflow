﻿Create User [PROD\GRP-PROD-SC-DW_Strategy_Analytics] For Login [PROD\GRP-PROD-SC-DW_Strategy_Analytics]
GO
Grant CONNECT TO [PROD\GRP-PROD-SC-DW_Strategy_Analytics]
Go
EXEC sp_addrolemember 'Strategy_Analytics', [PROD\GRP-PROD-SC-DW_Strategy_Analytics]
Go
GRANT SHOWPLAN TO [PROD\GRP-PROD-SC-DW_Strategy_Analytics]
Go
GRANT Execute On dbo.Sp_WhoIsActive TO [PROD\GRP-PROD-SC-DW_Strategy_Analytics]
Go
﻿CREATE LOGIN [ssascuberead] WITH PASSWORD=N'iXC7V0t4kLONgrfZntlvBdTaMUr3BG/pJsBp0KuJRu0=', DEFAULT_DATABASE=[$(DatabaseName)], DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF
Go
CREATE USER [ssascuberead] FOR LOGIN [ssascuberead] WITH DEFAULT_SCHEMA=[dbo]
Go
EXEC sp_addrolemember N'db_datareader', N'ssascuberead'
Go
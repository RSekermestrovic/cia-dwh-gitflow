﻿CREATE LOGIN [ssascubeadmin] WITH PASSWORD=N'iXC7V0t4kLONgrfZntlvBdTaMUr3BG/pJsBp0KuJRu0=', DEFAULT_DATABASE=[$(DatabaseName)], DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF
Go
CREATE USER [ssascubeadmin] FOR LOGIN [ssascubeadmin] WITH DEFAULT_SCHEMA=[dbo]
Go
EXEC sp_addrolemember N'db_datareader', N'ssascubeadmin'
Go
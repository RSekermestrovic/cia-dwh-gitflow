﻿CREATE LOGIN [PROD\GRP-PROD-SC-DW_Commercial] FROM WINDOWS WITH DEFAULT_DATABASE=[$(DatabaseName)], DEFAULT_LANGUAGE=[us_english]
Go
Create User [PROD\GRP-PROD-SC-DW_Commercial] For Login [PROD\GRP-PROD-SC-DW_Commercial]
GO
Grant CONNECT TO [PROD\GRP-PROD-SC-DW_Commercial]
Go
EXEC sp_addrolemember 'Commercial', [PROD\GRP-PROD-SC-DW_Commercial]
Go
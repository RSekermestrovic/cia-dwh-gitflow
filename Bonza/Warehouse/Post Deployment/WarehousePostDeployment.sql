﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

/*Version Info */
Insert into [$(Dimensional)].[Control].[Version] values ('$(VersionNumber)', 'Warehouse', getdate(), SUser_Name())

--Synonyms
:r .\ETLController.sql

--User Access (CCIA-8077)
Use Master
GO
GRANT VIEW SERVER STATE TO [PROD\GRP-PROD-SC-DW_Strategy_Analytics]
Go

Use [$(DatabaseName)]

﻿CREATE VIEW [SnowFlake].[Account_ManagedBy]
AS
SELECT	DISTINCT
		COALESCE(NULLIF(BDM,'N/A'),NULLIF(VIP,'N/A'),'N/A') ManagedById,
		COALESCE(NULLIF(BDM,'N/A'),NULLIF(VIP,'N/A'),'N/A') ManagedBy,
		ISNULL(BDM,'N/A') BDM,
		ISNULL(VIP,'N/A') VIP,
		ISNULL(VIPCode,0) VIPCode
FROM	[$(Dimensional)].Dimension.Account
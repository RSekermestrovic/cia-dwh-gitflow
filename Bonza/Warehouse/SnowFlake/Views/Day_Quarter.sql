﻿


CREATE VIEW [SnowFlake].[Day_Quarter]
AS
SELECT DISTINCT cast(concat(YearNumber,QuarterOfYearNumber)as int) QuarterId
, QuarterName
, QuarterStartDate
, QuarterEndDate
, QuarterOfYearText QuarterOfYear
, QuarterOfYearNumber
, QuarterDays
, YearNumber YearId FROM [$(Dimensional)].Dimension.Day
  where daykey>=20000101
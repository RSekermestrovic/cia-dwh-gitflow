﻿


CREATE VIEW [SnowFlake].[Day_Week]
AS

SELECT DISTINCT case when weekofyearnumber<10 then cast(concat(yearnumber,'0',weekofyearnumber)as int)
 else cast(concat(yearnumber,weekofyearnumber)as int) end WeekId,
WeekName, WeekStartDate, WeekEndDate, WeekOfYearText WeekOfYear, WeekOfYearNumber
, cast(concat(substring(LastWeekName,5,4),SUBSTRING(LastWeekName,2,2)) as int) as LastWeekId 
, cast(concat(substring(LastYearSameWeekName,5,4),SUBSTRING(LastYearSameWeekName,2,2)) as int) as LastYearSameWeekId
FROM [$(Dimensional)].Dimension.Day
  where daykey>=20000101
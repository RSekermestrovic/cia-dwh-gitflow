﻿CREATE VIEW [SnowFlake].[Class_Superclass]
AS
SELECT DISTINCT SuperClassKey SuperClassId, SuperClassName SuperClass FROM [$(Dimensional)].Dimension.Class
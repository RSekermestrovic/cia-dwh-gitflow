﻿
CREATE VIEW [SnowFlake].[Account_Ledger]
AS
--SELECT	AccountKey,
--		LedgerID,
--		LegacyLedgerID,
--		LedgerSequenceNumber,
--		AccountId,
--		LedgerTypeID,
--		CASE WHEN LedgerSequenceNumber = 1 AND AccountTypeCode = 'C' AND	LedgerGrouping = 'Transactional' AND CompanyKey <> 100 THEN 'Y' ELSE 'N' END ClientAccount
--FROM	Dimensional.Dimension.Account

SELECT [AccountKey]
      ,[LedgerID]
      ,[LegacyLedgerID]
      ,[LedgerSequenceNumber]
      ,[AccountId]
      ,[LedgerTypeID]
      ,[ClientAccount]
  FROM [$(Dimensional)].[SnowFlake].[Account_Ledger]
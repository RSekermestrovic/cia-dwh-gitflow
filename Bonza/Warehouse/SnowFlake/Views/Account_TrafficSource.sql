﻿CREATE VIEW [SnowFlake].[Account_TrafficSource]
AS
SELECT  DISTINCT
		BTag2+'!'+TrafficSource+'!'+RefURL+'!'+CampaignID+'!'+Keywords TrafficSourceId,
		BTag2,
		AffiliateId,
		SiteId,
		TrafficSource,
		RefURL,
		CampaignID,
		Keywords
FROM	[$(Dimensional)].dimension.account
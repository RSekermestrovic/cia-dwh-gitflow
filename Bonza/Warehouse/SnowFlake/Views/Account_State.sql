﻿

CREATE VIEW [SnowFlake].[Account_State]
AS
SELECT	DISTINCT
		CASE WHEN Country = 'CHANNEL ISLANDS' THEN 'CI2' ELSE CountryCode END + StateCode StateId, 
		StateCode, 
		State,
		CASE WHEN Country = 'CHANNEL ISLANDS' THEN 'CI2' ELSE CountryCode END AS CountryId
FROM	[$(Dimensional)].Dimension.account
﻿CREATE VIEW [SnowFlake].[TransactionDetail_TransactionMethod]
AS
SELECT	DISTINCT
		[TransactionStatusCode]+[TransactionTypeCode]+[TransactionMethodCode] TransactionMethodId,
		[TransactionMethodCode],
		[TransactionMethod],
		[TransactionStatusCode]+[TransactionTypeCode] TransactionStatusId
FROM	[Dimension].[TransactionDetail]
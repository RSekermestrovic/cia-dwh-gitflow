﻿CREATE VIEW [SnowFlake].[Account_AccountType]
AS
SELECT	DISTINCT
		AccountTypeCode AccountTypeId,
		AccountTypeCode,
		AccountType
FROM	[$(Dimensional)].Dimension.account
﻿

CREATE VIEW [SnowFlake].[Channel_ChannelDetail]
AS
SELECT	DISTINCT 
		REPLACE(ChannelDescription,'  ',' ') ChannelDetailId, 
		REPLACE(ChannelDescription,'  ',' ') ChannelDetail,
		CASE 
			WHEN ChannelId = 8888 THEN 'iOS Push' 
			WHEN ChannelId = 8889 THEN 'Android Push' 
			WHEN ChannelId = 8887 THEN 'Unknown Push' 
			WHEN ChannelTypeName = 'Other Mobile' THEN 'Unknown Mobile' 
			WHEN ChannelTypeName = 'Internet' THEN 'Web' 
			WHEN ChannelTypeName = 'CMS' THEN 'Phone' 
			WHEN ChannelTypeName = 'Telebet' THEN 'Phone' 
			WHEN ChannelTypeName = 'Betback' THEN 'N/A' 
			WHEN ChannelTypeName = 'TAB' THEN 'N/A' 
			WHEN ChannelTypeName = 'TAB_Hedging' THEN 'N/A' 
			WHEN ChannelTypeName = 'Brokering' THEN 'N/A' 
			WHEN ChannelTypeName = 'Concierge' THEN 'Phone' 
			WHEN ChannelTypeName = 'Admin' THEN 'N/A' 
			WHEN ChannelTypeName = 'QuickSlip' THEN 'Web' 
			ELSE ChannelTypeName 
		END ChannelPlatformId
FROM	[$(Dimensional)].Dimension.Channel
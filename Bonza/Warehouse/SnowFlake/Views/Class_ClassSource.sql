﻿
CREATE VIEW [SnowFlake].[Class_ClassSource]
AS
SELECT	ClassKey, 
		CONVERT(Varchar,ClassId) + CONVERT(Varchar,ClassKey) SubClassId
FROM [$(Dimensional)].Dimension.Class
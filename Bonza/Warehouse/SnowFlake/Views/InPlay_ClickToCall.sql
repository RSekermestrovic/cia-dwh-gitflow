﻿CREATE VIEW [SnowFlake].[InPlay_ClickToCall]
AS
SELECT DISTINCT ClickToCall ClickToCallId, CASE ClickToCall WHEN 'Y' THEN 'Yes' WHEN 'N' THEN 'No' ELSE ClickToCall END ClickToCall, CASE ClickToCall WHEN 'Y' THEN 'Y' ELSE InPlay END InPlayId FROM [$(Dimensional)].Dimension.InPlay
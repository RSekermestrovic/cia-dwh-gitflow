﻿

CREATE VIEW [SnowFlake].[Day_FiscalWeek]
AS
SELECT DISTINCT case when fiscalweekofyearnumber<10 then cast(concat(fiscalyearnumber,'0',fiscalweekofyearnumber)as int)
 else cast(concat(fiscalyearnumber,fiscalweekofyearnumber)as int) end FiscalWeekId 
, FiscalWeekName FiscalWeek
, FiscalWeekStartDate
, FiscalWeekEndDate
, FiscalWeekOfMonthText FiscalWeekOfMonth
, FiscalWeekOfMonthNumber
, FiscalWeekOfYearText FiscalWeekOfYear
, FiscalWeekOfYearNumber
, case when FiscalMonthOfYearNumber<10 then cast(concat(FiscalYearNumber,'0',FiscalMonthOfYearNumber)as int)
 else cast(concat(FiscalYearNumber,FiscalMonthOfYearNumber)as int) end FiscalMonthId
, cast(concat(substring(LastFiscalWeekName,5,4),SUBSTRING(LastFiscalWeekName,2,2)) as int) as LastFiscalWeekId 
, cast(concat(substring(LastFiscalYearSameWeekName,5,4),SUBSTRING(LastFiscalYearSameWeekName,2,2)) as int) as LastFiscalYearSameWeekId 
FROM [$(Dimensional)].Dimension.Day
  where daykey>=20000101
﻿CREATE VIEW [Snowflake].[AccountStatus_AccountStatusSource]
AS 
SELECT	[AccountStatusKey],
		AccountStatusKey LedgerStatusId,
--		Introducer IntroducerId,
--		COALESCE(NULLIF(BDM,'N/A'),NULLIF(VIP,'N/A'),'N/A') ManagedById,
		[InternetProfile]+[PhoneColourDesc] ProfileId,
		[AccountStatusKey] LimitId,
		CASE WHEN [LimitPeriod] IS NULL THEN NULL WHEN [LimitPeriod] = -1 THEN NULL WHEN [LimitPeriod] = 0 THEN NULL ELSE [LimitTimestamp] END [LimitSetDateTime],
		[ReceiveMarketingEmail]+[ReceiveCompetitionEmail]+[ReceiveSMS]+[ReceivePostalMail]+[ReceiveFax] ConsentId,
		[BrandId],
		[BusinessUnitId],
		[FromDate],
		[ToDate],
		[CurrentVersion]
  FROM [$(Dimensional)].[Dimension].[AccountStatusCompany]
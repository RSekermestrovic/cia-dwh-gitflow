﻿CREATE VIEW [Snowflake].[AccountStatus_LedgerStatus]
AS 
SELECT	AccountKey LedgerStatusId,
		CASE LedgerStatus WHEN 'U' THEN 'Unk' WHEN 'A' THEN 'Yes' WHEN 'C' THEN 'No' WHEN 'D' THEN 'No' ELSE [LedgerEnabled] END LedgerEnabled,
		[LedgerStatus] AS [LedgerStatusCode],
		CASE [LedgerStatus] WHEN 'A' THEN 'Active' WHEN 'D' THEN 'Disabled' WHEN 'C' THEN 'Closed' ELSE 'Unknown' END AS [LedgerStatus],
		CASE [LedgerStatus] WHEN 'C' THEN [AccountClosedDate] WHEN 'D' THEN [AccountClosedDate] WHEN 'A' THEN NULL ELSE NULL END AS [LedgerStatusDate],
		CASE [LedgerStatus] WHEN 'C' THEN [AccountClosureReason] WHEN 'D' THEN [AccountClosureReason] WHEN 'A' THEN 'N/A' ELSE 'Unknown' END AS [LedgerStatusReason]
FROM	[$(Dimensional)].[Dimension].[AccountStatusCompany]
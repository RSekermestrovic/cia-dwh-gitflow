﻿CREATE VIEW [SnowFlake].[Company_BusinessUnit]
AS
SELECT DISTINCT BusinessUnitId, BusinessUnitCode, BusinessUnitName BusinessUnit, DivisionID  FROM [$(Dimensional)].Dimension.Company
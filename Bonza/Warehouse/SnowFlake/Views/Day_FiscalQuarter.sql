﻿
CREATE VIEW [SnowFlake].[Day_FiscalQuarter]
AS
SELECT DISTINCT cast(concat(FiscalYearNumber,FiscalQuarterOfYearNumber)as int) FiscalQuarterId
, FiscalQuarterName FiscalQuarter
, FiscalQuarterStartDate
, FiscalQuarterEndDate
, FiscalQuarterOfYearText FiscalQuarterOfYear
, FiscalQuarterOfYearNumber
, FiscalQuarterDays
, FiscalYearNumber FiscalYearId FROM [$(Dimensional)].Dimension.Day
  where daykey>=20000101
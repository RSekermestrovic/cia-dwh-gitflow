﻿

CREATE VIEW [SnowFlake].[Account_Customer]
AS
SELECT	DISTINCT
		CustomerID,
		Title,
		FirstName,
		MiddleName,
		Surname,
		DOB,
		Gender,
		Address1,
		Address2,
		Suburb,
		PostCode,
		Phone,
		Mobile,
		Fax,
		Email,
		CASE WHEN Country = 'CHANNEL ISLANDS' THEN 'CI2' ELSE CountryCode END + StateCode StateId
FROM	[$(Dimensional)].Dimension.Account
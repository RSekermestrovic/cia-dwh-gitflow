﻿


CREATE VIEW [SnowFlake].[Day_Day]
AS

SELECT DayKey, DayDate, DayOfWeekText DayOfWeekName, DayOfWeekNumber, DayOfMonthNumber, 
case when weekofyearnumber<10 then cast(concat(yearnumber,'0',weekofyearnumber)as int)
 else cast(concat(yearnumber,weekofyearnumber)as int) end WeekId,
case when MonthOfYearNumber<10 then cast(concat(yearnumber,'0',MonthOfYearNumber)as int)
 else cast(concat(yearnumber,MonthOfYearNumber)as int) end MonthId,
case when fiscalweekofyearnumber<10 then cast(concat(fiscalyearnumber,'0',fiscalweekofyearnumber)as int)
 else cast(concat(fiscalyearnumber,fiscalweekofyearnumber)as int) end FiscalWeekId 
, cast(convert(varchar(10),dateadd(dd,-1,daydate),112) as int) as YesterdayKey
, cast(convert(varchar(10),dateadd(dd,-7,daydate),112) as int) as LastWeekDayKey
, cast(convert(varchar(10),dateadd(mm,-1,daydate),112) as int) as LastMonthDayKey
, cast(convert(varchar(10),dateadd(mm,-12,daydate),112) as int) as LastYearDayKey
, cast(convert(varchar(10),LastYearSameDayDate,112) as int) as LastYearSameWeekDayKey
FROM [$(Dimensional)].Dimension.Day
  where daykey>=20000101
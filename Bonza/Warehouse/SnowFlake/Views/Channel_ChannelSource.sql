﻿
CREATE VIEW [SnowFlake].[Channel_ChannelSource]
AS
SELECT ChannelKey, ChannelId, REPLACE(ChannelDescription,'  ',' ') ChannelDetailId FROM [$(Dimensional)].Dimension.Channel
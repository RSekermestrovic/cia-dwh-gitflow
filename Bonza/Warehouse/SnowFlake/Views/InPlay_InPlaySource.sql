﻿
CREATE VIEW [SnowFlake].[InPlay_InPlaySource]
AS
SELECT	InPlayKey, 
		ClickToCall ClickToCallId,
		CashoutType StatusAtCashoutId,
		IsDoubleDown IsDoubleDownId,
		IsDoubledDown IsDoubledDownId
FROM	[$(Dimensional)].Dimension.InPlay
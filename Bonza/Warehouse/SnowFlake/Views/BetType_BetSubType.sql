﻿CREATE VIEW [SnowFlake].[BetType_BetSubType]
AS
SELECT	DISTINCT 
		BetSubTypeId, 
		BetSubTypeId BetSubType, 
		CASE 
			WHEN BetSubType = 'Single' THEN 'Single'
			WHEN BetType <> 'Unknown' THEN BetType 
			WHEN BetSubType = 'Unknown' THEN 'Unknown'
			WHEN BetSubType = 'Single' THEN 'Single'
			ELSE 'Multi'
		END BetTypeId 
FROM [$(Dimensional)].Dimension.BetType d INNER JOIN Snowflake.BetType_BetTypeSource s ON s.BetTypeKey = d.BetTypeKey
﻿
CREATE VIEW [SnowFlake].[Account_LedgerType]
AS
--SELECT	DISTINCT
--		LedgerTypeID, 
--		CASE LedgerType 
--			WHEN 'Unk' THEN 'Unknown' 
--			WHEN 'N/A' THEN 'No Ledger' 
--			ELSE ISNULL(LedgerType,'Unknown') 
--		END LedgerType, 
--		CASE LedgerType
--			WHEN 'N/A' THEN 'Financial' 
--			WHEN 'No Ledger' THEN 'Financial' 
--			WHEN 'Unknown' THEN 'Financial' 
--			ELSE ISNULL(LedgerGrouping,'Financial') 
--		END LedgerGrouping--, 
----		COUNT_BIG(*) z -- Compulsory analytical column to be allowed to create clustered index
--FROM	Dimensional.Dimension.Account

SELECT [LedgerTypeID]
      ,[LedgerType]
      ,[LedgerGrouping]
  FROM [$(Dimensional)].[SnowFlake].[Account_LedgerType]
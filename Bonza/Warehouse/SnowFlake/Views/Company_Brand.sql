﻿CREATE VIEW [SnowFlake].[Company_Brand]
AS
SELECT DISTINCT BrandId, BrandCode, BrandName Brand, OrgId, CompanyID  FROM [$(Dimensional)].Dimension.Company
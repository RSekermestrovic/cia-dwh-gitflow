﻿
CREATE VIEW [SnowFlake].[Day_FiscalMonth]
AS
SELECT DISTINCT case when FiscalMonthOfYearNumber<10 then cast(concat(FiscalYearNumber,'0',FiscalMonthOfYearNumber)as int)
 else cast(concat(FiscalYearNumber,FiscalMonthOfYearNumber)as int) end FiscalMonthId
, FiscalMonthName FiscalMonth
, FiscalMonthStartDate
, FiscalMonthEndDate
, FiscalMonthOfYearText FiscalMonthOfYear
, FiscalMonthOfYearNumber
, FiscalMonthDays
, cast(concat(FiscalYearNumber,FiscalQuarterOfYearNumber)as int) FiscalQuarterId 
FROM [$(Dimensional)].Dimension.Day
  where daykey>=20000101
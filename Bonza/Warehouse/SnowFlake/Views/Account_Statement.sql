﻿CREATE VIEW [SnowFlake].[Account_Statement]
AS
SELECT  DISTINCT
		CASE WHEN StatementFrequency = '-1' THEN 'Unknown' WHEN StatementMethod = 'Never' THEN 'Never' ELSE StatementFrequency END
		+CASE WHEN StatementMethod = 'Unk' THEN 'Unknown' WHEN StatementFrequency = 'Never' THEN 'Never' ELSE StatementMethod END StatementId,
		CASE WHEN StatementFrequency = '-1' THEN 'Unknown' WHEN StatementMethod = 'Never' THEN 'Never' ELSE StatementFrequency END StatementFrequency,
		CASE WHEN StatementMethod = 'Unk' THEN 'Unknown' WHEN StatementFrequency = 'Never' THEN 'Never' ELSE StatementMethod END StatementMethod
FROM	[$(Dimensional)].dimension.account
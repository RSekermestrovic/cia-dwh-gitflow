﻿CREATE VIEW [SnowFlake].[TransactionDetail_TransactionDetail]
AS
SELECT	[TransactionDetailKey],
		[TransactionDetailCode],
		[TransactionDirection],
		[TransactionSign],
		[TransactionStatusCode]+[TransactionTypeCode]+[TransactionMethodCode] TransactionMethodId
FROM	[Dimension].[TransactionDetail]
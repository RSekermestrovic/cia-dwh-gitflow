﻿CREATE VIEW [SnowFlake].[BetType_BetTypeSource]
AS
SELECT	BetTypeKey, 
		BetGroupType BetGroupTypeId, 
		CASE 
			WHEN BetType <> 'Exotic' AND BetSubType = 'Double' THEN '2 Leg' 
			WHEN BetType <> 'Exotic' AND BetSubType = 'Treble' THEN '3 Leg' 
			WHEN BetType <> 'Exotic' AND BetSubType = 'Quadrella' THEN '4 Leg' 
			WHEN BetSubType = 'Early Quadrella' THEN 'Quadrella' 
			WHEN BetSubType <> 'Unknown' THEN BetSubType 
			WHEN BetType = 'Single' THEN 'Single'
			WHEN BetType = 'Multi' THEN 'X Leg'
			WHEN BetType = 'Exotic' THEN 'Unknown Exotic'
			ELSE 'Unknown'
		END BetSubTypeId, 
		LegType PredictionTypeId, 
		CASE GroupingType WHEN 'Straight' THEN 'N/A' ELSE GroupingType END GroupingTypeId 
FROM	[$(Dimensional)].Dimension.BetType
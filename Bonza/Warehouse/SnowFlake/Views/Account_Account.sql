﻿
CREATE VIEW [SnowFlake].[Account_Account]
AS
--SELECT	DISTINCT
--		AccountId,
--		PIN,
--		ExactTargetID,
--		CustomerId,
--		AccountTypeCode AccountTypeId,
--		AccountOpenedDayKey,
--		BrandId,
--		BusinessUnitId,
--		COALESCE(NULLIF(BDM,'N/A'),NULLIF(VIP,'N/A'),'N/A') ManagedById,
--		BTag2+'!'+TrafficSource+'!'+RefURL+'!'+CampaignID+'!'+Keywords TrafficSourceId,
--		CASE WHEN StatementFrequency = '-1' THEN 'Unknown' WHEN StatementMethod = 'Never' THEN 'Never' ELSE StatementFrequency END
--		+CASE WHEN StatementMethod = 'Unk' THEN 'Unknown' WHEN StatementFrequency = 'Never' THEN 'Never' ELSE StatementMethod END StatementId
--FROM	Dimensional.Dimension.Account a
--		INNER JOIN Dimensional.Dimension.Company c ON c.CompanyKey = a.CompanyKey

SELECT [AccountId]
      ,[PIN]
      ,[ExactTargetID]
      ,[CustomerId]
      ,[AccountTypeId]
      ,[AccountOpenedDayKey]
      ,[BrandId]
      ,[BusinessUnitId]
      ,[ManagedById]
      ,[TrafficSourceId]
      ,[StatementId]
  FROM [$(Dimensional)].[SnowFlake].[Account_Account]
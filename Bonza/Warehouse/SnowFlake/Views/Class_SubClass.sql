﻿

CREATE VIEW [SnowFlake].[Class_SubClass]
AS
SELECT DISTINCT 
		CONVERT(Varchar,ClassId) + CONVERT(Varchar,ClassKey) SubClassId, 
		SourceClassName SubClass, 
		ClassCode + CONVERT(Varchar,SuperclassKey) ClassId 
FROM [$(Dimensional)].Dimension.Class
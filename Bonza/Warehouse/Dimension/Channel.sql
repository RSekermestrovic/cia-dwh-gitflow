﻿CREATE VIEW [Dimension].[Channel]
	AS SELECT [ChannelKey]
	  --,[ChannelId] surrogate id has no business context --CCIA-5791
      ,[ChannelName] AS [Channel] -- CCIA-7140
	  ,[ChannelDescription] AS [ChannelDetail] -- CCIA-7140
      ,[ChannelTypeName] AS [ChannelType] -- CCIA-7140
--Advanced Users
      --,[CreatedDate]
      --,[CreatedBatchKey]
      --,[CreatedBy]
      --,[ModifiedDate]
      ,[ModifiedBatchKey]
      --,[ModifiedBy]
  FROM [$(Dimensional)].[DImension].[Channel] with (nolock)
  Go

EXEC sys.sp_addextendedproperty @level1name=N'Channel', @value=N'Dimension providing a hierarchical decomposition of the communications channel used by the client to initiate the transaction.' ,@level1type=N'VIEW' ,@level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO

EXEC sys.sp_addextendedproperty @level2name=N'ChannelKey', @value=N'Unique internal data warehouse key for Channel dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Channel', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'Channel', @value=N'Name of the channel used in a transaction. Ex: iOS, Android, Phone, sbet.mobi etc.', @level2type=N'COLUMN' ,@level1name=N'Channel', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'ChannelDetail', @value=N'More detailed description of the channel such as brand/app', @level2type=N'COLUMN' ,@level1name=N'Channel', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'ChannelType', @value=N'Higher level channel type grouping, e.g. Mobile, Internet, Phone', @level2type=N'COLUMN' ,@level1name=N'Channel', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go

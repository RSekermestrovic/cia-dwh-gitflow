﻿Create View Dimension.Market

as

SELECT  [MarketKey]
-- Surrogate Keys --
		, o.[MasterDayKey] as [MarketOpenedDayKey]--CCIA-6682
		, c.[MasterDayKey] as [MarketClosedDayKey]--CCIA-6682
		, s.[MasterDayKey] as [MarketSettledDayKey]--CCIA-6682
-- Market Level --
		, [MarketId]
		--, [Source]
		, [Market]
		, [MarketOpen]
		, [MarketOpenedDateTime]
		--,[MarketOpenedVenueDateTime]
		, [MarketClosed]
		, [MarketClosedDateTime]
		--,[MarketClosedVenueDateTime]
		, [MarketSettled]
		, [MarketSettledDateTime]
		--,[MarketSettledVenueDateTime]
-- Market Type Level --
		, [MarketType]
		, [MarketGroup]
		, [LiveMarket] 
		, [FutureMarket] 
		, [MaxWinnings] 
		, [MaxStake]  
		, [LevyRate]
		--, [Sport]
--Advanced Users Only
      --,[FromDate]
      --,[FirstDate]
      --,[CreatedDate]
      --,[CreatedBatchKey]
      --,[CreatedBy]
      --,[ModifiedDate]
        , m.[ModifiedBatchKey]
      --,[ModifiedBy]
  FROM [$(Dimensional)].[Dimension].[Market] m with (nolock)
		INNER JOIN [$(Dimensional)].[Dimension].[DayZone] o WITH (NOLOCK) ON (o.DayKey = m.MarketOpenedDayKey)
		INNER JOIN [$(Dimensional)].[Dimension].[DayZone] c WITH (NOLOCK) ON (c.DayKey = m.MarketClosedDayKey)
		INNER JOIN [$(Dimensional)].[Dimension].[DayZone] s WITH (NOLOCK) ON (s.DayKey = m.MarketSettledDayKey)
  Go

EXEC sys.sp_addextendedproperty @level1name=N'Market', @value=N'Dimension documenting all Markets available to be bet on. This dimension covers details of the betting options; specifics about the event itself which is the subject of the Market are held in the Event dimension' ,@level1type=N'VIEW' ,@level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO

EXEC sys.sp_addextendedproperty @level2name=N'MarketKey', @value=N'Unique internal data warehouse key for Market dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Market', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'MarketOpenedDayKey', @value=N'Unique internal data warehouse key aliasing Day dimension to provide the day the market was opened for betting - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Market', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'MarketClosedDayKey', @value=N'Unique internal data warehouse key aliasing Day dimension to provide the day the market was last suspended from accepting any further bets - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Market', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'MarketSettledDayKey', @value=N'Unique internal data warehouse key aliasing Day dimension to provide the day all bets on the market were settled - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Market', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'MarketId', @value=N'External unique identifier of each market offered', @level2type=N'COLUMN' ,@level1name=N'Market', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'Market', @value=N'Name/description of the market', @level2type=N'COLUMN' ,@level1name=N'Market', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'MarketOpen', @value=N'Yes/ No flag to indicate whether the market is open for betting', @level2type=N'COLUMN' ,@level1name=N'Market', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'MarketOpenedDateTime', @value=N'Date/time the market was opened for betting', @level2type=N'COLUMN' ,@level1name=N'Market', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'MarketClosed', @value=N'Yes/ No flag to indicate whether the market is closed for betting', @level2type=N'COLUMN' ,@level1name=N'Market', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'MarketClosedDateTime', @value=N'Date/time the market was last suspended from accepting any further bets', @level2type=N'COLUMN' ,@level1name=N'Market', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'MarketSettled', @value=N'Yes/ No flag to indicate whether the market is settled', @level2type=N'COLUMN' ,@level1name=N'Market', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'MarketSettledDateTime', @value=N'Date/time by which all bets on the market were settled', @level2type=N'COLUMN' ,@level1name=N'Market', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'MarketType', @value=N'Classification of the type of markey being offered. i.e. there can be several markets, one for each match in the current NRL round, but the markets would all be of the same type, Head-to-Head', @level2type=N'COLUMN' ,@level1name=N'Market', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'MarketGroup', @value=N'A higher level grouping of the Market Type into groups of similar market types for analysis and reporting purposes', @level2type=N'COLUMN' ,@level1name=N'Market', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'LevyRate', @value=N'A sub-classification of markets according to the level of fees or levy they are assesed to attract based on criteria set by the governing body', @level2type=N'COLUMN' ,@level1name=N'Market', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'LiveMarket', @value=N'Yes/ No flag to indicate live bets are allowed on the market or not', @level2type=N'COLUMN' ,@level1name=N'Market', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FutureMarket', @value=N'Yes / No flag to indicate if the market is a future market or not as in Intrabet', @level2type=N'COLUMN' ,@level1name=N'Market', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'MaxWinnings', @value=N'Indicates the maximum winnings that can be won on a market', @level2type=N'COLUMN' ,@level1name=N'Market', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'MaxStake', @value=N'Maximum amount of stake that can be placed on a market', @level2type=N'COLUMN' ,@level1name=N'Market', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go


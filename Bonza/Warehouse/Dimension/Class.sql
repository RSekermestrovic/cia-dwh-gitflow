﻿Create view Dimension.Class AS
Select [ClassKey]
      ,[ClassId]
	  --,[Source] -- CCIA-7139 (Hide)
      ,[ClassCode]
      ,[ClassName] AS [Class] -- CCIA-7139
      --,[ClassIcon] --CCIA-5794
      --,[SuperclassKey] -- CCIA-7139 (Hide)
      ,CASE [SuperclassName] WHEN 'Sports' THEN 'S' WHEN 'Racing' THEN 'R' WHEN 'Novelty' THEN 'N' ELSE 'X' END AS [SuperclassCode] -- CCIA-7139
      ,[SuperclassName] AS [Superclass] -- CCIA-7139
--Advanced Users
      --,[FromDate]
      --,[ToDate]
      --,[CreatedDate]
      --,[CreatedBatchKey]
      --,[CreatedBy]
      --,[ModifiedDate]
      ,[ModifiedBatchKey]
      --,[ModifiedBy]
      ,[SourceClassName] AS [SourceClass] -- CCIA-7139
  FROM [$(Dimensional)].Dimension.[Class] with (nolock)
  Go

EXEC sys.sp_addextendedproperty @level1name=N'Class', @value=N'Dimension providing a hierarchical decomposition of the class of bet. i.e. Sport vs Racing and further break down into specific sport or type of racing' ,@level1type=N'VIEW' ,@level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO

EXEC sys.sp_addextendedproperty @level2name=N'ClassKey', @value=N'Unique internal data warehouse key for Class dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Class', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'ClassCode', @value=N'Unique character Code for each class/ Sport name - NOTE, this is the immutable value which should be used to code all conditional logic when selecting specific classes in preference to the presentation columns which could be subject to change', @level2type=N'COLUMN' ,@level1name=N'Class', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'ClassId', @value=N'Unique source system identifier for each class (Sport Name)', @level2type=N'COLUMN' ,@level1name=N'Class', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'Class', @value=N'Sport Name', @level2type=N'COLUMN' ,@level1name=N'Class', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'SourceClass', @value=N'Original uncleaned version of name verbatim from operational system', @level2type=N'COLUMN' ,@level1name=N'Class', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'SuperclassCode', @value=N'Unique single character Code for each superclass - NOTE, this is the immutable value which should be used to code all conditional logic when selecting specific superclasses in preference to the presentation columns which could be subject to change', @level2type=N'COLUMN' ,@level1name=N'Class', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'Superclass', @value=N'Name of the Sport type Ex: Sports, Racing, Novelty', @level2type=N'COLUMN' ,@level1name=N'Class', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go

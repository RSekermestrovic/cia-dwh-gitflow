CREATE VIEW [Dimension].[ClientAccount] AS
SELECT 
	  [AccountKey]

-- Surrogate Keys --
      ,[LedgerOpenedDayKey]
      ,[AccountOpenedDayKey]
      ,[AccountOpenedChannelKey]

-- Ledger Details --
      ,[LedgerID]
      ,[LegacyLedgerID]
      ,[LedgerSequenceNumber]
      ,[LedgerTypeID]
      ,[LedgerType]
      ,[LedgerOpenedDate]

-- Account Details --
	  ,[AccountID]
      ,[PIN]
	  ,[Username]
	  ,[ExactTargetId]
	  ,[AccountTypeCode]
	  ,[AccountType]

      ,[AccountOpenedDate]

	  ,[BDM]
	  ,[VIPCode]
	  ,[VIP]

      ,[BTag2]
      ,[AffiliateID]
      ,[Affiliate]
      ,[SiteID]
      ,[TrafficSource]
      ,[RefURL]
      ,[CampaignID]
      ,[Keywords]

      ,[StatementMethod]
      ,[StatementFrequency]

-- Customer Details --
      ,[CustomerID]
      ,[Title]
      ,[Surname]
      ,[FirstName]
      ,[MiddleName]
      ,[Gender]
      ,[DOB]
      ,[Address1]
      ,[Address2]
      ,[Suburb]
      ,[StateCode]
      ,[State]
      ,[PostCode]
      ,[CountryCode]
      ,[Country]
--      ,[GeographicalLocation] -- Makes table unusable over linked server, move to table of its own
      ,[AustralianClient]
      ,[Phone]
      ,[Mobile]
      ,[Fax]
      ,[Email]

-- Division Details --
      ,[Division]

-- Brand Details --
      ,[OrgId]
      ,[BrandCode]
      ,[Brand]

-- Business Unit Details --
      ,[BusinessUnitCode]
      ,[BusinessUnit]

--Advanced Users Only
      ,[ModifiedBatchKey]
      ,[AccountTitle]
      ,[AccountSurname]
      ,[AccountFirstName]
      ,[AccountMiddleName]
      ,[AccountGender]
      ,[AccountDOB]
      ,[HomeAddress1]
      ,[HomeAddress2]
      ,[HomeSuburb]
      ,[HomeStateCode]
      ,[HomeState]
      ,[HomePostCode]
      ,[HomeCountryCode]
      ,[HomeCountry]
      ,[HomePhone]
      ,[HomeMobile]
      ,[HomeFax]
      ,[HomeEmail]
      ,[HomeAlternateEmail]
      ,[PostalAddress1]
      ,[PostalAddress2]
      ,[PostalSuburb]
      ,[PostalStateCode]
      ,[PostalState]
      ,[PostalPostCode]
      ,[PostalCountryCode]
      ,[PostalCountry]
      ,[PostalPhone]
      ,[PostalMobile]
      ,[PostalFax]
      ,[PostalEmail]
      ,[PostalAlternateEmail]
      ,[BusinessAddress1]
      ,[BusinessAddress2]
      ,[BusinessSuburb]
      ,[BusinessStateCode]
      ,[BusinessState]
      ,[BusinessPostCode]
      ,[BusinessCountryCode]
      ,[BusinessCountry]
      ,[BusinessPhone]
      ,[BusinessMobile]
      ,[BusinessFax]
      ,[BusinessEmail]
      ,[BusinessAlternateEmail]
      ,[OtherAddress1]
      ,[OtherAddress2]
      ,[OtherSuburb]
      ,[OtherStateCode]
      ,[OtherState]
      ,[OtherPostCode]
      ,[OtherCountryCode]
      ,[OtherCountry]
      ,[OtherPhone]
      ,[OtherMobile]
      ,[OtherFax]
      ,[OtherEmail]
      ,[OtherAlternateEmail]
  FROM [Dimension].[Account] WITH (NOLOCK)
  WHERE AccountTypeCode = 'C'
		AND LedgerGrouping = 'Transactional'
		AND CompanyCode = 'WHA'

GO

EXEC sys.sp_addextendedproperty @level1name=N'ClientAccount', @value=N'Subset of the Account dimension focussing only on Transactional ledgers belonging to WHA client accounts (applies all the standard filters to the Account dimension required in 99% of standard reports)' ,@level1type=N'VIEW' ,@level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO

EXEC sys.sp_addextendedproperty @level2name=N'AccountId', @value=N'Unique source system identifier for the account concept. Known as ClientID in Intrabet', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'AccountFirstName', @value=N'FirstName as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'AccountGender', @value=N'Gender as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'AccountKey', @value=N'Unique internal data warehouse key for Account dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'AccountMiddleName', @value=N'MiddleName as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'AccountOpenedChannelKey', @value=N'Link to the Channel dimension for the client sign up channel', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'AccountOpenedDate', @value=N'Client sign up date/time', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'AccountOpenedDayKey', @value=N'Link to the Day dimension for the account sign up date', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'AccountSurname', @value=N'Surname as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'AccountTitle', @value=N'Title as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'Address1', @value=N'Client Address Line 1', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'Address2', @value=N'Client Address Line 2', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'Affiliate', @value=N'Name of the Affiliate through which the client signed up', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'AffiliateID', @value=N'AffiliateID through which a client signed up', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'AustralianClient', @value=N'Yes/No indicator as to whether client resides in Australia or not', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'BTag2', @value=N'A string indicating the SiteID and/or CreativeID and/or AffiliateID', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'BusinessAddress1', @value=N'BusinessAddress1 as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'BusinessAddress2', @value=N'BusinessAddress2 as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'BusinessAlternateEmail', @value=N'BusinessAlternateEmail as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'BusinessCountry', @value=N'BusinessCountryName as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'BusinessCountryCode', @value=N'BusinessCountryCode as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'BusinessEmail', @value=N'BusinessEmail as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'BusinessFax', @value=N'BusinessFax as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'BusinessMobile', @value=N'BusinessMobile as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'BusinessPhone', @value=N'BusinessPhone as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'BusinessPostCode', @value=N'BusinessPostCode as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'BusinessState', @value=N'BusinessStateName as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'BusinessStateCode', @value=N'BusinessStateCode as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'BusinessSuburb', @value=N'BusinessSuburb as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'CampaignID', @value=N'Unique identifier of the specific campaign through which the client is acquired', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'Country', @value=N'Country name where the client resides', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'CountryCode', @value=N'Short code of the country in which the client resides', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'CustomerID', @value=N'DCRMID unique identifier for each client. Currently contains ClientID as DCRMID is not yet available.', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'DOB', @value=N'Date of birth of the client', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'Email', @value=N'Client Email id', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'ExactTargetID', @value=N'Client ExactTarget unique identifier', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'Fax', @value=N'Client Fax number', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'FirstName', @value=N'FirstName of the client', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'Gender', @value=N'Gender of customer', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
--EXEC sys.sp_addextendedproperty @level2name=N'GeographicalLocation', @value=N'Geographical location of client address', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
EXEC sys.sp_addextendedproperty @level2name=N'HomeAddress1', @value=N'HomeAddress1 as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'HomeAddress2', @value=N'HomeAddress2 as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'HomeAlternateEmail', @value=N'HomeAlternateEmail as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'HomeCountry', @value=N'HomeCountryName as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'HomeCountryCode', @value=N'HomeCountryCode as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'HomeEmail', @value=N'HomeEmail as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'HomeFax', @value=N'HomeFax as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'HomeMobile', @value=N'HomeMobile as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'HomePhone', @value=N'HomePhone as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'HomePostCode', @value=N'HomePostCode as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'HomeState', @value=N'HomeStateName as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'HomeStateCode', @value=N'HomeStateCode as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'HomeSuburb', @value=N'HomeSuburb as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'Keywords', @value=N'Keywords used to search In the serach engine before signing up with WHA', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'LedgerID', @value=N'Unique identifier of each client account', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'LedgerOpenedDate', @value=N'Date/time ledger was opened underneath the account', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'LedgerOpenedDayKey', @value=N'Link to the Day dimension for the date the ledger was opened under the account', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'LedgerSequenceNumber', @value=N'Client AccountNumber as in Intrabet', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'LedgerType', @value=N'Financial role of the ledger. For ex: Cash only client, Full agent etc.', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'LedgerTypeID', @value=N'Unique Identifier for each Ledger type', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'LegacyLedgerID', @value=N'CB_Client_key. Account identifier in legacy database', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'MiddleName', @value=N'MiddleName of the client', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'Mobile', @value=N'Client mobile number', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'OtherAddress1', @value=N'OtherAddress1 as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'OtherAddress2', @value=N'OtherAddress2 as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'OtherAlternateEmail', @value=N'OtherAlternateEmail as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'OtherCountry', @value=N'OtherCountryName as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'OtherCountryCode', @value=N'OtherCountryCode as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'OtherEmail', @value=N'OtherEmail as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'OtherFax', @value=N'OtherFax as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'OtherMobile', @value=N'OtherMobile as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'OtherPhone', @value=N'OtherPhone as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'OtherPostCode', @value=N'OtherPostCode as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'OtherState', @value=N'OtherStateName as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'OtherStateCode', @value=N'OtherStateCode as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'OtherSuburb', @value=N'OtherSuburb as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'PostalAddress1', @value=N'PostalAddress1 as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'PostalAddress2', @value=N'PostalAddress2 as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'PostalAlternateEmail', @value=N'PostalAlternateEmail as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'PostalCountry', @value=N'PostalCountryName as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'PostalCountryCode', @value=N'PostalCountryCode as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'PostalEmail', @value=N'PostalEmail as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'PostalFax', @value=N'PostalFax as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'PostalMobile', @value=N'PostalMobile as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'PostalPhone', @value=N'PostalPhone as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'PostalPostCode', @value=N'PostalPostCode as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'PostalState', @value=N'PostalStateName as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'PostalStateCode', @value=N'PostalStateCode as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'PostalSuburb', @value=N'PostalSuburb as in Source database without any transformations', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'Phone', @value=N'Client phone number', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'PIN', @value=N'Client PIN visible to customer', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'PostCode', @value=N'PostCode of Client address', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'RefURL', @value=N'Web Reference URL of the affiliate', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'SiteID', @value=N'SiteID through which a client signed up', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'State', @value=N'State full name in Client address', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'StateCode', @value=N'Short code of State in Client address', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'StatementFrequency', @value=N'Frequency of the Account Statement that the client receives', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'StatementMethod', @value=N'The method in which the statement is delivered to the client. Ex: Mail, Email', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'Suburb', @value=N'Suburb in Client address', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'Surname', @value=N'Surname of the client', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'Title', @value=N'Title of customer like Mr, Miss,Ms, Mrs', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'TrafficSource', @value=N'Source of traffic through which client ended up signing up with WHA', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'Username', @value=N'Client username', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'BDM', @value=N'Name of the BDM responsible for handling the client. Some clients have been given the value of Legacy where their BDM has left, they have not been allocated to a new BDM, but they are still to be treat as a BDM', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'VIPCode', @value=N'Numeric indication of the VIP grouping the client belogs to - 0 = Not VIP, 1= VIP1, 2 = VIP2 - NOTE, this is the immutable column and conditions should be coded against this rather than the VIP column which might change for presentation purposes', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'VIP', @value=N'Text representation of the VIP grouping the client belogs to - VIP1, VIP 2 or N/A', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'BrandCode', @value=N'Short code of brand name. Ex: TWH,CBT,WHA,RWWA', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'Brand', @value=N'TomWaterhouse,Centerbet,SportingBet,RWWA', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'BusinessUnitCode', @value=N'Short code of business unit. Ex: TWH,SBA,CBT,BDM,RWWA', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'BusinessUnit', @value=N'Full Name of the business unit. Ex: Tom Waterhouse', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'Division', @value=N'Name of the diviion in the company. Ex: Digital,BDM,RWWA', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'OrgId', @value=N'Unique identifier of Brand name as in Intrabet', @level2type=N'COLUMN' ,@level1name=N'ClientAccount', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go

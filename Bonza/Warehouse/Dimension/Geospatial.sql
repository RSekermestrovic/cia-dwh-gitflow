CREATE VIEW [Dimension].[Geospatial] AS
SELECT 
	  [AccountKey]
      ,[GeographicalLocation]
-- Advanced Users Only --
      --,[FromDate] -- CCIA-7086 (Hide)
      --,[ToDate] -- CCIA-xxxx (Hide)
      --,[FirstDate]
      --,[CreatedDate]
      --,[CreatedBatchKey]
      --,[CreatedBy]
      --,[ModifiedDate]
      ,a.[ModifiedBatchKey]
      --,[ModifiedBy]
  FROM [$(Dimensional)].[Dimension].[AccountCompany] a WITH (NOLOCK, NOEXPAND)
GO

EXEC sys.sp_addextendedproperty @level1name=N'Geospatial', @value=N'Geospatial mapping information of Client location. Delivered seperate from main Account/ClientAccount dimension because geospatial data is more problematic when used with linked servers etc and would compromise the usability if included in the main dimensions' ,@level1type=N'VIEW' ,@level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO

EXEC sys.sp_addextendedproperty @level2name=N'AccountKey', @value=N'Unique internal data warehouse key for Account dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Geospatial', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'GeographicalLocation', @value=N'Geographical location of client address', @level2type=N'COLUMN' ,@level1name=N'Geospatial', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO



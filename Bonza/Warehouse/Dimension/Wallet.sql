﻿CREATE View [Dimension].[Wallet]

as

SELECT   [WalletKey],
    [WalletId] AS [WalletCode],
    [WalletName] AS [Wallet],
-- Advanced Users Only --
    --[FromDate],
    --[ToDate],
    --[FirstDate],
    --[CreatedDate],
    --[CreatedBatchKey],
    --[CreatedBy],
    --[ModifiedDate],
    [ModifiedBatchKey]
    --[ModifiedBy]
  FROM [$(Dimensional)].[Dimension].[Wallet] with (nolock)
  go

EXEC sys.sp_addextendedproperty @level1name=N'Wallet', @value=N'Dimension defining the type of money involved in cashflows - Cash or Bonus (artificial money given to clients to play with as part of promotions which can only be converted to Cash if the Bonus is used in a winning bet).' ,@level1type=N'VIEW' ,@level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO

EXEC sys.sp_addextendedproperty @level2name=N'WalletKey', @value=N'Unique internal data warehouse key for Wallet dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Wallet', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'WalletCode', @value=N'Unique identifier for each wallet name', @level2type=N'COLUMN' ,@level1name=N'Wallet', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'Wallet', @value=N'Name of the wallet. Ex: Cash, free bet, free winnings', @level2type=N'COLUMN' ,@level1name=N'Wallet', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
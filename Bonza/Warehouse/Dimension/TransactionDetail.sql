﻿CREATE View [Dimension].[TransactionDetail]

as 


SELECT [TransactionDetailKey]
      ,[TransactionDetailId] AS [TransactionDetailCode]
      ,[TransactionTypeID] AS [TransactionTypeCode]
      ,[TransactionType]
      ,[TransactionStatusID] AS [TransactionStatusCode]
      ,[TransactionStatus]
      ,[TransactionMethodID] AS [TransactionMethodCode]
      ,[TransactionMethod]
      ,[TransactionDirection]
      ,[TransactionSign]
-- Advanced Users Only --
      --,[FromDate]
      --,[ToDate]
      --,[FirstDate]
      --,[CreatedDate]
      --,[CreatedBatchKey]
      --,[CreatedBy]
      --,[ModifiedDate]
      ,[ModifiedBatchKey]
      --,[ModifiedBy]
  FROM [$(Dimensional)].[Dimension].[TransactionDetail] with (nolock)
GO

EXEC sys.sp_addextendedproperty @level1name=N'TransactionDetail', @value=N'Dimension covering the role of the transaction against the cashflow model.' ,@level1type=N'VIEW' ,@level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO

EXEC sys.sp_addextendedproperty @level2name=N'TransactionDetailKey', @value=N'Unique internal data warehouse key for TransactionDetail dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'TransactionDetail', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'TransactionDetailCode', @value=N'Unique identifier for each transaction type', @level2type=N'COLUMN' ,@level1name=N'TransactionDetail', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'TransactionDirection', @value=N'CR, DR to indicate whether transaction is credit or debit type', @level2type=N'COLUMN' ,@level1name=N'TransactionDetail', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'TransactionMethod', @value=N'Method used for a transaction. Ex: Cash, EFT, settlement', @level2type=N'COLUMN' ,@level1name=N'TransactionDetail', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'TransactionMethodCode', @value=N'Two-character short code representation of the transaction method - NOTE, this is the immutable column and conditions should be coded against this rather than the TransactionMethod column which might change for presentation purposes', @level2type=N'COLUMN' ,@level1name=N'TransactionDetail', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'TransactionSign', @value=N'+1 or -1 to indicate credit or debit transaction', @level2type=N'COLUMN' ,@level1name=N'TransactionDetail', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'TransactionStatus', @value=N'Status of the transaction. Ex: Complete, cancel etc.', @level2type=N'COLUMN' ,@level1name=N'TransactionDetail', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'TransactionStatusCode', @value=N'Two-character short code representation of the transaction status - NOTE, this is the immutable column and conditions should be coded against this rather than the TransactionStatus column which might change for presentation purposes', @level2type=N'COLUMN' ,@level1name=N'TransactionDetail', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'TransactionType', @value=N'Name of transaction type in text format. Ex: Bet, Deposit etc.', @level2type=N'COLUMN' ,@level1name=N'TransactionDetail', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'TransactionTypeCode', @value=N'Two-character short code representation of the transaction type - NOTE, this is the immutable column and conditions should be coded against this rather than the TransactionType column which might change for presentation purposes', @level2type=N'COLUMN' ,@level1name=N'TransactionDetail', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go


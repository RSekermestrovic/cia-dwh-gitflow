﻿CREATE View [Dimension].[Promotion]

as

SELECT [PromotionKey]
      ,[PromotionId]
      --,[Source]
      --,[PromotionType]
      ,[PromotionName] AS [Promotion]
      ,OfferDate
      ,ExpiryDate
--Advanced Users Only
      --,[FromDate]
      --,[ToDate]
      --,[FirstDate]
      --,[CreatedDate]
      --,[CreatedBatchKey]
      --,[CreatedBy]
      --,[ModifiedDate]
      ,[ModifiedBatchKey]
      --,[ModifiedBy]
  FROM [$(Dimensional)].[Dimension].[Promotion] with (nolock)
GO

EXEC sys.sp_addextendedproperty @level1name=N'Promotion', @value=N'Dimension storing all promotions offered to customers to link to triggering and/or resultant fact records' ,@level1type=N'VIEW' ,@level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO

EXEC sys.sp_addextendedproperty @level2name=N'PromotionKey', @value=N'Unique internal data warehouse key for Promotion dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Promotion', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'PromotionId', @value=N'Unique source system identifier for the Promotion', @level2type=N'COLUMN' ,@level1name=N'Promotion', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'Promotion', @value=N'The name/description of the promotion', @level2type=N'COLUMN' ,@level1name=N'Promotion', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'OfferDate', @value=N'The date on which the promotion is offered or first offered to clients', @level2type=N'COLUMN' ,@level1name=N'Promotion', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'ExpiryDate', @value=N'The day the offer benefits of the promotion must be claimed and used by or they expire and are recovered', @level2type=N'COLUMN' ,@level1name=N'Promotion', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go

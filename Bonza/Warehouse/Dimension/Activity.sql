﻿CREATE VIEW Dimension.[Activity] 
AS

SELECT [ActivityKey]
      ,[HasDeposited]
      ,[HasWithdrawn]
      ,[HasBet]
      ,[HasWon]
      ,[HasLost]
--Advanced Users
      --,[FromDate]
      --,[ToDate]
      --,[FirstDate]
      --,[CreatedDate]
      --,[CreatedBatchKey]
      --,[CreatedBy]
      --,[ModifiedDate]
      ,[ModifiedBatchKey]
      --,[ModifiedBy]
FROM [$(Dimensional)].[Dimension].[Activity] with (nolock)
Go

EXEC sys.sp_addextendedproperty @level1name=N'Activity', @value=N'Classifies potential activity which could have taken place into a series of Yes/No flags to allow for easy retrieval of active account/day combinations as the basis of further analysis' ,@level1type=N'VIEW' ,@level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO

EXEC sys.sp_addextendedproperty @level2name=N'ActivityKey', @value=N'Unique internal data warehouse key for Activity dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Activity', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'HasBet', @value=N'Indicates whether a client has placed a bet', @level2type=N'COLUMN' ,@level1name=N'Activity', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'HasDeposited', @value=N'Indicates whether a client has deposited', @level2type=N'COLUMN' ,@level1name=N'Activity', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'HasLost', @value=N'Indicates whether a client has lost a bet', @level2type=N'COLUMN' ,@level1name=N'Activity', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'HasWithdrawn', @value=N'Indicates whether a client has requested a withdrawal', @level2type=N'COLUMN' ,@level1name=N'Activity', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'HasWon', @value=N'Indicates whether a client has won a bet', @level2type=N'COLUMN' ,@level1name=N'Activity', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
﻿CREATE View [Dimension].[Day] 

as

Select [DayKey]
      ,[DayName] AS [Day] -- CCIA-7150
      ,[DayDate]
      ,[DayOfWeekText] AS [DayOfWeek] -- CCIA-7150
      ,[DayOfWeekNumber]
      ,[DayOfMonthNumber]
      ,[DayOfQuarterNumber]
      ,[DayOfYearNumber]
      ,[WeekName] AS [Week] -- CCIA-7150
      ,[WeekOfYearText] AS [WeekOfYear] -- CCIA-7150
      ,[WeekOfYearNumber]
      ,[WeekStartDate]
      ,[WeekEndDate]
      ,[MonthName] AS [Month] -- CCIA-7150
      ,[MonthOfYearText] AS [MonthOfYear] -- CCIA-7150
      ,[MonthOfYearNumber]
      ,[MonthStartDate]
      ,[MonthEndDate]
      ,[MonthDays]
      ,[QuarterName] AS [Quarter] -- CCIA-7150
      ,[QuarterStartDate]
      ,[QuarterEndDate]
      ,[QuarterDays]
      ,[QuarterOfYearText] AS [QuarterOfYear] -- CCIA-7150
      ,[QuarterOfYearNumber]
      ,[YearName] AS [Year] -- CCIA-7150
      ,[YearNumber]
      ,[YearStartDate]
      ,[YearEndDate]
      ,[YearDays]   
      ,[FiscalWeekName] AS [FiscalWeek] -- CCIA-7150
      ,[FiscalWeekStartDate]
      ,[FiscalWeekEndDate]
      ,[FiscalWeekOfMonthText] AS [FiscalWeekOfMonth] -- CCIA-7150
      ,[FiscalWeekOfMonthNumber]
      ,[FiscalWeekOfYearText] AS [FiscalWeekOfYear] -- CCIA-7150
      ,[FiscalWeekOfYearNumber]
      ,[FiscalMonthName] AS [FiscalMonth] -- CCIA-7150
      ,[FiscalMonthStartDate]
      ,[FiscalMonthEndDate]
      ,[FiscalMonthDays]
      ,[FiscalMonthWeeks]
      ,[FiscalMonthOfYearText] AS [FiscalMonthOfYear] -- CCIA-7150
      ,[FiscalMonthOfYearNumber]
      ,[FiscalQuarterName] AS [FiscalQuarter] --CCIA-5357
      ,[FiscalQuarterStartDate]
      ,[FiscalQuarterEndDate]
      ,[FiscalQuarterDays]
      ,[FiscalQuarterWeeks]
      ,[FiscalQuarterOfYearText] AS [FiscalQuarterOfYear] -- CCIA-7150
      ,[FiscalQuarterOfYearNumber]
      ,[FiscalYearName] AS [FiscalYear] --CCIA-5357
      ,[FiscalYearNumber]
      ,[FiscalYearStartDate]
      ,[FiscalYearEndDate]
      ,[FiscalYearDays]
      ,[FiscalYearWeeks]
--For Advanced Users
      --,[CreatedDate] -- CCIA-7150 (Hide)
      --,[CreatedBatchKey] -- CCIA-7150 (Hide)
      --,[CreatedBy] -- CCIA-7150 (Hide)
      --,[ModifiedDate] -- CCIA-7150 (Hide)
      ,[ModifiedBatchKey]
      --,[ModifiedBy] -- CCIA-7150 (Hide)
      --,[DayText] AS [DayId] -- CCIA-7150
	  ,[YesterdayName] AS [Yesterday]
	  ,[YesterdayDate]
	  ,[LastWeekName] AS [LastWeek]
	  ,[LastWeekSameDayName] AS [LastWeekSameDay]
	  ,[LastWeekSameDayDate]
	  ,[LastFiscalWeekName] AS [LastFiscalWeek]
	  ,[LastMonthName] AS [LastMonth]
	  ,[LastFiscalMonthName] AS [LastFiscalMonth]
	  ,[LastYearName] AS [LastYear]
	  ,[LastYearSameDayName] AS [LastYearSameDay]
	  ,[LastYearSameDayDate] 
	  ,[LastYearSameWeekName] AS [LastYearSameWeek]
	  ,[LastYearSameMonthName] AS [LastYearSameMonth]
	  ,[LastFiscalYearName] AS [LastFiscalYear]
	  ,[LastFiscalYearSameWeekName] AS [LastFiscalYearSameWeek]
	  ,[LastFiscalYearSameMonthName] AS [LastFiscalYearSameMonth]

-- NOT REQUIRED -- CCIA-7150
      --,[DayText] AS [DayCode]
      --,[PreviousDayKey]
      --,[NextDayKey]
      --,[DayOfFiscalWeekNumber]
      --,[DayOfFiscalMonthNumber]
      --,[DayOfFiscalQuarterNumber]
      --,[DayOfFiscalYearNumber]
      --,[WeekKey]
      --,[WeekText] AS [WeekCode]
      --,[WeekYearNumber]
      --,[WeekOfMonthText] CCIA-3459
      --,[WeekOfMonthNumber] CCIA-3459
      --,[PreviousWeekKey]
      --,[PreviousWeekDayKey]
      --,[NextWeekKey] CCIA-4603
      --,[NextWeekDayKey]
      --,[MonthKey]
      --,[MonthText] AS [MonthCode]
      --,[PreviousMonthKey]
      --,[PreviousMonthDayKey]
      --,[NextMonthKey]
      --,[NextMonthDayKey]
      --,[QuarterKey]
      --,[QuarterText] AS [QuarterCode]
      --,[PreviousQuarterKey]
      --,[PreviousQuarterMonthKey]
      --,[PreviousQuarterDayKey]
      --,[NextQuarterKey]
      --,[NextQuarterMonthKey]
      --,[NextQuarterDayKey]
      --,[YearKey]
      --,[YearText] AS [YearCode]
      --,[PreviousYearKey]
      --,[PreviousYearQuarterKey]
      --,[PreviousYearMonthKey]
      --,[PreviousYearDayKey]
      --,[NextYearKey]
      --,[NextYearQuarterKey]
      --,[NextYearMonthKey]
      --,[NextYearDayKey]
      --,[FiscalWeekKey]
      --,[FiscalWeekText] AS [FiscalWeekCode]
      --,[PreviousFiscalWeekKey] CCIA-4603
      --,[PreviousFiscalWeekDayKey] CCIA-4603
      --,[NextFiscalWeekKey] CCIA-4603
      --,[NextFiscalWeekDayKey]
      --,[FiscalMonthKey]
      --,[FiscalMonthText] AS [FiscalMonthCode]
      --,[PreviousFiscalMonthKey] CCIA-4603
      --,[PreviousFiscalMonthDayKey] CCIA-4603
      --,[NextFiscalMonthKey]
      --,[NextFiscalMonthDayKey] CCIA-4603
      --,[FiscalQuarterKey]
      --,[FiscalQuarterText] AS [FiscalQuarterCode] --CCIA-5357
      --,[PreviousFiscalQuarterKey]
      --,[PreviousFiscalQuarterMonthKey] CCIA-4603
      --,[PreviousFiscalQuarterDayKey] CCIA-4603
      --,[NextFiscalQuarterKey]
      --,[NextFiscalQuarterMonthKey]
      --,[NextFiscalQuarterDayKey] CCIA-4603
      --,[FiscalYearKey]
      --,[FiscalYearText] AS [FiscalYearCode] --CCIA-5357
      --,[PreviousFiscalYearKey] CCIA-4603
      --,[PreviousFiscalYearQuarterKey] CCIA-4603
      --,[PreviousFiscalYearMonthKey] CCIA-4603
      --,[PreviousFiscalYearDayKey] CCIA-4603
      --,[NextFiscalYearKey]
      --,[NextFiscalYearQuarterKey]
      --,[NextFiscalYearMonthKey] CCIA-4069
      --,[NextFiscalYearDayKey]
      --,[DayOfTaxQuarterNumber]
      --,[DayOfTaxYearNumber]
	  --,[TaxQuarterKey]
	  --,[PreviousTaxQuarterKey]
      --,[PreviousTaxQuarterMonthKey] CCIA-4603
      --,[PreviousTaxQuarterDayKey]
	  --,[NextTaxQuarterKey]
      --,[NextTaxQuarterMonthKey] CCIA-4603
      --,[NextTaxQuarterDayKey]
      --,[TaxQuarterText] AS [TaxQuarterCode]
      --,[TaxQuarterName] AS [TaxQuarter]
      --,[TaxQuarterStartDate]
      --,[TaxQuarterEndDate]
      --,[TaxQuarterDays]
      --,[TaxQuarterOfYearText] AS [TaxQuarterOfYear]
      --,[TaxQuarterOfYearNumber]
      --,[TaxYearKey]
      --,[PreviousTaxYearKey]
      --,[PreviousTaxYearQuarterKey]
      --,[PreviousTaxYearMonthKey]
      --,[PreviousTaxYearDayKey]
      --,[NextTaxYearKey]
      --,[NextTaxYearQuarterKey] CCIA-4603
      --,[NextTaxYearMonthKey] CCIA-4603
      --,[NextTaxYearDayKey]
      --,[TaxYearText] AS [TaxYearCode]
      --,[TaxYearName] AS [TaxYear]
      --,[TaxYearNumber]
      --,[TaxYearStartDate]
      --,[TaxYearEndDate]
      --,[TaxYearDays]
  FROM [$(Dimensional)].[Dimension].[Day] with (nolock)
  Go

EXEC sys.sp_addextendedproperty @level1name=N'Day', @value=N'Dimension defining each day and the hierarchy of fiscal and calendar periods it belongs to in addition to its relationship the preceding equivalent periods such as same day previous week/year, etc' ,@level1type=N'VIEW' ,@level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO

EXEC sys.sp_addextendedproperty @level2name=N'DayKey', @value=N'Unique internal data warehouse key for Communication dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'Day', @value=N'Day date in display text format. Ex: 01-Jan-90', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'DayDate', @value=N'Day date in database date format', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'DayOfWeek', @value=N'Weekday of the day, e.g. Mon, Tue, Wed, etc.', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'DayOfWeekNumber', @value=N'Day of the week as an incremental number', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'DayOfMonthNumber', @value=N'Day of the month in numeric format', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'DayOfQuarterNumber', @value=N'Day of the quarter in numeric format', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'DayOfYearNumber', @value=N'Day of the year in numeric format', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'Week', @value=N'Short code of the week number and the year number. Ex: W01-1990', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'WeekOfYear', @value=N'Short code of week number of the year. Ex: W1', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'WeekOfYearNumber', @value=N'Number of the Week in a Year. Ex: 1', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'WeekStartDate', @value=N'First Day of the week in date format. Ex: 7/01/1990', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'WeekEndDate', @value=N'Last Day of the week in date format. Ex: 7/01/1990', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'Month', @value=N'Short code of the month and Year nuber.Ex: Jan-90', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'MonthOfYear', @value=N'Short code of the month name in text format. Ex: Jan', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'MonthOfYearNumber', @value=N'Number of month in a year. Ex: 1', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'MonthStartDate', @value=N'First Day of the month in date format. Ex: 1/01/1990', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'MonthEndDate', @value=N'Last Day of the month in date format. Ex: 31/01/1990', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'MonthDays', @value=N'Number of days in a month. Ex: 31', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'Quarter', @value=N'Short code of Quarter number of the year and the year number. Ex: Q1-1990', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'QuarterStartDate', @value=N'First day of the Quarter in date format. Ex: 1/01/1990', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'QuarterEndDate', @value=N'Last day of the Quarter in date format. Ex: 31/03/1990', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'QuarterDays', @value=N'Number of days in a quarter. Ex: 90', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'QuarterOfYear', @value=N'Short code of the number of the quarter in a year. Ex: Q1', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'QuarterOfYearNumber', @value=N'Number of quarter of a year. Ex: 1', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'Year', @value=N'Number of the year in character format. Allows to add character data to it on reports. Ex: 1990', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'YearNumber', @value=N'Number of the year in numeric format. Numeric format can be used for sorting records. Ex: 1990', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'YearStartDate', @value=N'First day of the year in date format. Ex: 1/01/1990', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'YearEndDate', @value=N'Last day of the year in date format. Ex: 1/01/1990', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'YearDays', @value=N'Number of days in a year. Ex: 365 or 366', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FiscalWeek', @value=N'Short code of the number of the fiscal week and year number. Ex: W52-1989', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FiscalWeekStartDate', @value=N'First day of the fiscal week in date format. Ex: 27/12/1989', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FiscalWeekEndDate', @value=N'Last day of the fiscal week in date format. Ex: 2/01/1990', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FiscalWeekOfMonth', @value=N'Short Code for the number of fiscal week in a month. Ex: W1', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FiscalWeekOfMonthNumber', @value=N'Number of fiscal week in a month. Ex: 1', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FiscalWeekOfYear', @value=N'Short code for number of fiscal week in a year Ex: W52', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FiscalWeekOfYearNumber', @value=N'Number of fiscal week in a year. Ex: 52', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FiscalMonth', @value=N'Short code for fiscal month in text and year number. Ex: Jan-90', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FiscalMonthStartDate', @value=N'First Day of the fiscal month in date format. Ex: 1/01/1990', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FiscalMonthEndDate', @value=N'Last Day of the fiscal month in date format. Ex: 31/01/1990', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FiscalMonthDays', @value=N'Number of days in a fiscal month. Ex: 31', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FiscalMonthWeeks', @value=N'Number of weeks in a fiscal month. Ex: 4', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FiscalMonthOfYear', @value=N'Short code of the month name in text format. Ex: Jan', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FiscalMonthOfYearNumber', @value=N'Number of month in a fiscal year. Ex: 1', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FiscalQuarter', @value=N'Short code of Quarter number of the fiscal year and the fiscal year number. Ex: Q1-1990', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FiscalQuarterStartDate', @value=N'First day of the fiscal Quarter in date format. Ex: 31/03/1990', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FiscalQuarterEndDate', @value=N'Last day of the fiscal Quarter in date format. Ex: 31/03/1990', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FiscalQuarterDays', @value=N'Number of days in a fiscal quarter. Ex: 90', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FiscalQuarterWeeks', @value=N'Number of weeks in a fiscal quarter. Ex: 13', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FiscalQuarterOfYear', @value=N'Short code of the number of the fiscal quarter in a year. Ex: Q1', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FiscalQuarterOfYearNumber', @value=N'Number of fiscal quarter of a year. Ex: 1', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FiscalYear', @value=N'Number of the fiscal year in character format. Allows to add character data to it on reports. Ex: 1990', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FiscalYearNumber', @value=N'Number of the fiscal year in numeric format. Numeric format can be used for sorting records. Ex: 1990', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FiscalYearStartDate', @value=N'First day of the fiscal year in date format. Ex: 1/01/1990', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FiscalYearEndDate', @value=N'Last day of the fiscal year in date format. Ex: 31/12/1990', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FiscalYearDays', @value=N'Number of days in a fiscal year. Ex: 365', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FiscalYearWeeks', @value=N'Number of weeks in a fiscal year. Ex: 48', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'Yesterday', @value=N'Previous day - join to [Day] column to get Day dimension record for yesterday', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'YesterdayDate', @value=N'Previous day in database date format', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'LastWeek', @value=N'Previous week - join to [Week] column to get all Day dimension records for previous week', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'LastWeekSameDay', @value=N'Same day of the previous week - join to [day] column to get Day dimension record for same day previous week, i.e. Monday to Monday, Tuesday to Tuesday, etc', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'LastWeekSameDayDate', @value=N'Same day of the previous week in database date format', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'LastFiscalWeek', @value=N'Previous fiscal week - join to [FiscalWeek] column to get all Day dimension records for previous fiscal week', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'LastMonth', @value=N'Previous calendar month - join to [Month] column to get all Day dimension records for previous calendar month', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'LastFiscalMonth', @value=N'Previous fiscal month - join to [FiscalMonth] column to get all Day dimension records for previous fiscal month', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'LastYear', @value=N'Previous year - join to [Year] column to get all Day dimension records for previous year', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'LastYearSameDay', @value=N'Same weekday of the previous year - join to [Day] column to get Day dimension record for same weekday previous year (minus 364 days), i.e. Monday to Monday, Tuesday to Tuesday, etc', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'LastYearSameDayDate', @value=N'Same weekday of the previous year in database date format', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'LastYearSameWeek', @value=N'Same week of the previous year - join to [Week] column to get all Day dimension records for same week previous year, i.e. the week that the same weekday previous year using minus 364 days logic would belong to', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'LastYearSameMonth', @value=N'Same month of the previous year - join to [Month] column to get all Day dimension records for same month previous year (minus 364 days), i.e. Monday to Monday, Tuesday to Tuesday, etc', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'LastFiscalYear', @value=N'Previous fiscal year - join to [FiscalYear] column to get all Day dimension records for previous fiscal year', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'LastFiscalYearSameWeek', @value=N'Same fiscal week of the previous fiscal year - join to [FiscalWeek] column to get all Day dimension records for same fiscal week previous year', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'LastFiscalYearSameMonth', @value=N'Same fiscal month of the previous fiscal year - join to [FiscalMonth] column to get all Day dimension records for same fiscal month previous year', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go

-- NOT REQUIRED
--EXEC sys.sp_addextendedproperty @level2name=N'DayCode', @value=N'Immutable text version of date - NOTE, this is the column conditions should be coded against rather than the Day column which might change for presentation purposes', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--Go
--EXEC sys.sp_addextendedproperty @level2name=N'PreviousDayKey', @value=N'Unique internal data warehouse key of the previous calendar day - join to DayKey to get previous record', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
--EXEC sys.sp_addextendedproperty @level2name=N'NextDayKey', @value=N'Unique internal data warehouse key of the next calendar day - join to DayKey to get next record', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
--EXEC sys.sp_addextendedproperty @level2name=N'DayOfFiscalWeekNumber', @value=N'Day of the Fiscal week in numeric format. Ex: 6', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--Go
--EXEC sys.sp_addextendedproperty @level2name=N'DayOfFiscalMonthNumber', @value=N'Day of the Fiscal month in numeric format. Ex: 6', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--Go
--EXEC sys.sp_addextendedproperty @level2name=N'DayOfFiscalQuarterNumber', @value=N'Day of the fiscal quarter in numeric format. Ex: 1', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--Go
--EXEC sys.sp_addextendedproperty @level2name=N'DayOfFiscalYearNumber', @value=N'Day of the fiscal year in numeric format. Ex: 1', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--Go
--EXEC sys.sp_addextendedproperty @level2name=N'WeekKey', @value=N'Unique internal data warehouse key for Week - only to be used for joining to other day dimension records to implement next/previous logic', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
--EXEC sys.sp_addextendedproperty @level2name=N'WeekCode', @value=N'Immutable text version of week - NOTE, this is the column conditions should be coded against rather than the Week column which might change for presentation purposes', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--Go
--EXEC sys.sp_addextendedproperty @level2name=N'WeekYearNumber', @value=N'Year number of a selected week. Ex: 1990', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--Go
--EXEC sys.sp_addextendedproperty @level2name=N'PreviousWeekKey', @value=N'Unique internal data warehouse key of the previous Week - join to WeekKey to get previous record', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
--EXEC sys.sp_addextendedproperty @level2name=N'PreviousWeekDayKey', @value=N'Unique internal data warehouse key of the equivalent day in the previous week - join to DayKey to get previous record', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
--EXEC sys.sp_addextendedproperty @level2name=N'NextWeekKey', @value=N'Unique internal data warehouse key of the next Week - join to WeekKey to get next record', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
--EXEC sys.sp_addextendedproperty @level2name=N'NextWeekDayKey', @value=N'Unique internal data warehouse key of the equivalent day in the next week - join to DayKey to get next record', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
--EXEC sys.sp_addextendedproperty @level2name=N'MonthKey', @value=N'Unique internal data warehouse key for Month - only to be used for joining to other day dimension records to implement next/previous logic', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
--EXEC sys.sp_addextendedproperty @level2name=N'MonthCode', @value=N'Immutable text version of month - NOTE, this is the column conditions should be coded against rather than the Month column which might change for presentation purposes', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--Go
--EXEC sys.sp_addextendedproperty @level2name=N'PreviousMonthKey', @value=N'Unique internal data warehouse key of the previous Month - join to MonthKey to get previous record', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
--EXEC sys.sp_addextendedproperty @level2name=N'PreviousMonthDayKey', @value=N'Unique internal data warehouse key of the equivalent day in the previous month - join to DayKey to get previous record', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
--EXEC sys.sp_addextendedproperty @level2name=N'NextMonthKey', @value=N'Unique internal data warehouse key of the next Month - join to MonthKey to get next record', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
--EXEC sys.sp_addextendedproperty @level2name=N'NextMonthDayKey', @value=N'Unique internal data warehouse key of the equivalent day in the next month - join to DayKey to get next record', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
--EXEC sys.sp_addextendedproperty @level2name=N'QuarterKey', @value=N'Unique internal data warehouse key for Quarter - only to be used for joining to other day dimension records to implement next/previous logic', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
--EXEC sys.sp_addextendedproperty @level2name=N'QuarterCode', @value=N'Immutable text version of quarter - NOTE, this is the column conditions should be coded against rather than the Quarter column which might change for presentation purposes', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--Go
--EXEC sys.sp_addextendedproperty @level2name=N'PreviousQuarterKey', @value=N'Unique internal data warehouse key of the previous Quarter - join to QuarterKey to get previous record', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
--EXEC sys.sp_addextendedproperty @level2name=N'PreviousQuarterMonthKey', @value=N'Unique internal data warehouse key of the equivalent month in the previous quarter - join to MonthKey to get previous record', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
--EXEC sys.sp_addextendedproperty @level2name=N'PreviousQuarterDayKey', @value=N'Unique internal data warehouse key of the equivalent day in the previous quarter - join to DayKey to get previous record', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
--EXEC sys.sp_addextendedproperty @level2name=N'NextQuarterKey', @value=N'Unique internal data warehouse key of the next Quarter - join to QuarterKey to get next record', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
--EXEC sys.sp_addextendedproperty @level2name=N'NextQuarterMonthKey', @value=N'Unique internal data warehouse key of the equivalent month in the next quarter - join to MonthKey to get next record', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
--EXEC sys.sp_addextendedproperty @level2name=N'NextQuarterDayKey', @value=N'Unique internal data warehouse key of the equivalent day in the next quarter - join to DayKey to get next record', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
--EXEC sys.sp_addextendedproperty @level2name=N'YearKey', @value=N'Unique internal data warehouse key for Year - only to be used for joining to other day dimension records to implement next/previous logic', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
--EXEC sys.sp_addextendedproperty @level2name=N'YearCode', @value=N'Immutable text version of year - NOTE, this is the column conditions should be coded against rather than the Year column which might change for presentation purposes', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--Go
--EXEC sys.sp_addextendedproperty @level2name=N'PreviousYearKey', @value=N'Unique internal data warehouse key of the previous Year - join to YearKey to get previous record', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
--EXEC sys.sp_addextendedproperty @level2name=N'PreviousYearQuarterKey', @value=N'Unique internal data warehouse key of the equivalent quarter in the previous year - join to QuarterKey to get previous record', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
--EXEC sys.sp_addextendedproperty @level2name=N'PreviousYearMonthKey', @value=N'Unique internal data warehouse key of the equivalent month in the previous year - join to MonthKey to get previous record', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
--EXEC sys.sp_addextendedproperty @level2name=N'PreviousYearDayKey', @value=N'Unique internal data warehouse key of the equivalent day in the previous year - join to DayKey to get previous record', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
--EXEC sys.sp_addextendedproperty @level2name=N'NextYearKey', @value=N'Unique internal data warehouse key of the next Year - join to YearKey to get next record', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
--EXEC sys.sp_addextendedproperty @level2name=N'NextYearQuarterKey', @value=N'Unique internal data warehouse key of the equivalent quarter in the next year - join to QuarterKey to get next record', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
--EXEC sys.sp_addextendedproperty @level2name=N'NextYearMonthKey', @value=N'Unique internal data warehouse key of the equivalent month in the next year - join to MonthKey to get next record', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
--EXEC sys.sp_addextendedproperty @level2name=N'NextYearDayKey', @value=N'Unique internal data warehouse key of the equivalent day in the next year - join to DayKey to get next record', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
--EXEC sys.sp_addextendedproperty @level2name=N'FiscalWeekKey', @value=N'Unique internal data warehouse key for Fiscal Week - only to be used for joining to other day dimension records to implement next/previous logic', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
--EXEC sys.sp_addextendedproperty @level2name=N'FiscalWeekCode', @value=N'Immutable text version of fiscal week - NOTE, this is the column conditions should be coded against rather than the FiscalWeek column which might change for presentation purposes', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--Go
--EXEC sys.sp_addextendedproperty @level2name=N'PreviousFiscalWeekDayKey', @value=N'Unique internal data warehouse key of the equivalent day in the previous fiscal week - join to DayKey to get previous record', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
--EXEC sys.sp_addextendedproperty @level2name=N'NextFiscalWeekDayKey', @value=N'Unique internal data warehouse key of the equivalent day in the next fiscal week - join to DayKey to get next record', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
--EXEC sys.sp_addextendedproperty @level2name=N'FiscalMonthKey', @value=N'Unique internal data warehouse key for Fiscal Month - only to be used for joining to other day dimension records to implement next/previous logic', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
--EXEC sys.sp_addextendedproperty @level2name=N'FiscalMonthCode', @value=N'Immutable text version of fiscal month - NOTE, this is the column conditions should be coded against rather than the FiscalMonth column which might change for presentation purposes', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--Go
--EXEC sys.sp_addextendedproperty @level2name=N'PreviousFiscalMonthKey', @value=N'Unique internal data warehouse key of the previous fiscal month - join to FiscalMonthKey to get previous record', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
--EXEC sys.sp_addextendedproperty @level2name=N'NextFiscalMonthKey', @value=N'Unique internal data warehouse key of the next fiscal month - join to FiscalMonthKey to get next record', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
--EXEC sys.sp_addextendedproperty @level2name=N'FiscalQuarterKey', @value=N'Unique internal data warehouse key for Fiscal Quarter - only to be used for joining to other day dimension records to implement next/previous logic', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
--EXEC sys.sp_addextendedproperty @level2name=N'FiscalQuarterCode', @value=N'Immutable text version of fiscal quarter - NOTE, this is the column conditions should be coded against rather than the FiscalQuarter column which might change for presentation purposes', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--Go
--EXEC sys.sp_addextendedproperty @level2name=N'PreviousFiscalQuarterKey', @value=N'Unique internal data warehouse key of the previous fiscal quarter - join to FiscalQuarterKey to get previous record', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
--EXEC sys.sp_addextendedproperty @level2name=N'PreviousFiscalQuarterMonthKey', @value=N'Unique internal data warehouse key of the equivalent month in the previous fiscal quarter - join to FiscalMonthKey to get previous record', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
--EXEC sys.sp_addextendedproperty @level2name=N'NextFiscalQuarterKey', @value=N'Unique internal data warehouse key of the next fiscal quarter - join to FiscalQuarterKey to get next record', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
--EXEC sys.sp_addextendedproperty @level2name=N'NextFiscalQuarterMonthKey', @value=N'Unique internal data warehouse key of the equivalent month in the next fiscal quarter - join to FiscalMonthKey to get next record', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
--EXEC sys.sp_addextendedproperty @level2name=N'FiscalYearKey', @value=N'Unique internal data warehouse key for Fiscal Year - only to be used for joining to other day dimension records to implement next/previous logic', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
--EXEC sys.sp_addextendedproperty @level2name=N'FiscalYearCode', @value=N'Immutable text version of fiscal year - NOTE, this is the column conditions should be coded against rather than the FiscalYear column which might change for presentation purposes', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--Go
--EXEC sys.sp_addextendedproperty @level2name=N'PreviousFiscalYearKey', @value=N'Unique internal data warehouse key of the previous fiscal year - join to FiscalYearKey to get previous record', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
--EXEC sys.sp_addextendedproperty @level2name=N'PreviousFiscalYearQuarterKey', @value=N'Unique internal data warehouse key of the equivalent quarter in the previous fiscal year - join to FiscalQuarterKey to get previous record', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
--EXEC sys.sp_addextendedproperty @level2name=N'NextFiscalYearKey', @value=N'Unique internal data warehouse key of the next fiscal year - join to FiscalYearKey to get next record', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
--EXEC sys.sp_addextendedproperty @level2name=N'NextFiscalYearQuarterKey', @value=N'Unique internal data warehouse key of the equivalent quarter in the next fiscal year - join to FiscalQuarterKey to get next record', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
--EXEC sys.sp_addextendedproperty @level2name=N'NextFiscalYearDayKey', @value=N'Unique internal data warehouse key of the equivalent day in the next fiscal year - join to DayKey to get next record', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
--EXEC sys.sp_addextendedproperty @level2name=N'DayOfTaxQuarterNumber', @value=N'Number of the day of a tax year quarter. Ex: 1', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--Go
--EXEC sys.sp_addextendedproperty @level2name=N'DayOfTaxYearNumber', @value=N'Number of the day in a tax year. Ex: 185', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--Go
--EXEC sys.sp_addextendedproperty @level2name=N'TaxQuarter', @value=N'Short code for the tax quarter number and the calendar years that the tax year relates to. Ex: Q3-89/90', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--Go
--EXEC sys.sp_addextendedproperty @level2name=N'TaxQuarterStartDate', @value=N'First day of the Quarter in date format. Ex: 1/01/1990', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--Go
--EXEC sys.sp_addextendedproperty @level2name=N'TaxQuarterEndDate', @value=N'Last day of the Quarter in date format. Ex: 31/03/1990', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--Go
--EXEC sys.sp_addextendedproperty @level2name=N'TaxQuarterDays', @value=N'Number of days in the tax quarter. Ex: 90', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--Go
--EXEC sys.sp_addextendedproperty @level2name=N'TaxQuarterOfYear', @value=N'Short code of the number of quarter in a tax year. Ex: Q3', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--Go
--EXEC sys.sp_addextendedproperty @level2name=N'TaxQuarterOfYearNumber', @value=N'Number of quarter in a tax year. Ex: 3', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--Go
--EXEC sys.sp_addextendedproperty @level2name=N'TaxYear', @value=N'Number of the Tax year in character format. Allows to add character data to it on reports. Ex: 1990', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--Go
--EXEC sys.sp_addextendedproperty @level2name=N'TaxYearNumber', @value=N'Number of the Tax year in numeric format. Numeric format can be used for sorting records. Ex: 1990', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--Go
--EXEC sys.sp_addextendedproperty @level2name=N'TaxYearStartDate', @value=N'First day of the tax year in date format. Ex: 1/01/1990', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--Go
--EXEC sys.sp_addextendedproperty @level2name=N'TaxYearEndDate', @value=N'Last day of the tax year in date format. Ex: 31/12/1990', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--Go
--EXEC sys.sp_addextendedproperty @level2name=N'TaxYearDays', @value=N'Number of days in a tax year. Ex: 365', @level2type=N'COLUMN' ,@level1name=N'Day', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--Go

﻿CREATE View [Dimension].[LegType]

as

SELECT [LegTypeKey]
      ,[LegTypeName] AS [LegType]
      ,[LegNumber]
      ,[NumberOfLegs]
--Advanced Users Only
      --,[FromDate]
      --,[ToDate]
      --,[FirstDate]
      --,[CreatedDate]
      --,[CreatedBatchKey]
      --,[CreatedBy]
      --,[ModifiedDate]
      ,[ModifiedBatchKey]
      --,[ModifiedBy]
  FROM [$(Dimensional)].[Dimension].[LegType] with (nolock)
  Go

EXEC sys.sp_addextendedproperty @level1name=N'LegType', @value=N'Dimension defining the leg type relative to the bet in terms of leg number of total number of legs, etc' ,@level1type=N'VIEW' ,@level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO

EXEC sys.sp_addextendedproperty @level2name=N'LegTypeKey', @value=N'Unique internal data warehouse key for In-Play dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'LegType', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'LegType', @value=N'Description of leg type. Ex: 1 of 50, 3 of 12 etc.', @level2type=N'COLUMN' ,@level1name=N'LegType', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'LegNumber', @value=N'Number of leg in a bet from 1 to 100', @level2type=N'COLUMN' ,@level1name=N'LegType', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'NumberOfLegs', @value=N'Total number of legs in a particular bet', @level2type=N'COLUMN' ,@level1name=N'LegType', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go


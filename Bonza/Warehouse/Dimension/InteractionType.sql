﻿Create view Dimension.InteractionType AS
SELECT [InteractiontypeKey]
      --,[InteractionTypeId] --CCIA-7266
      --,[InteractionTypeSource] --CCIA-7268
      ,[InteractionTypeName] as [InteractionType]
--Advanced Users Only
      --,[CreatedDate]
      --,[CreatedBatchKey]
      --,[CreatedBy]
      --,[ModifiedDate]
      ,[ModifiedBatchKey]
      --,[ModifiedBy]
  FROM [$(Dimensional)].[Dimension].[InteractionType] WITH (NOLOCK)
  Go

EXEC sys.sp_addextendedproperty @level1name=N'InteractionType', @value=N'Dimension documenting and action used to achieve a contact, such as Open (email), sent, Bounce, etc' ,@level1type=N'VIEW' ,@level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO

EXEC sys.sp_addextendedproperty @level2name=N'InteractionTypeKey', @value=N'Unique internal data warehouse key for InstrumentType dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'InteractionType', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
--EXEC sys.sp_addextendedproperty @name=N'InteractionTypeId', @value=N'Own value to identify interaction type', @level0type=N'SCHEMA',@level0name=N'Dimension', @level1type=N'VIEW',@level1name=N'InteractionType', @level2type=N'Column',@level2name=N'InteractionTypeId'
--Go
--EXEC sys.sp_addextendedproperty @name=N'InteractionTypeSource', @value=N'Source of Interaction type', @level0type=N'SCHEMA',@level0name=N'Dimension', @level1type=N'VIEW',@level1name=N'InteractionType', @level2type=N'Column',@level2name=N'InteractionTypeSource'
--Go
EXEC sys.sp_addextendedproperty @level2name=N'InteractionType', @value=N'Name of Interaction type, i.e. Bounce, sent, click etc', @level2type=N'COLUMN' ,@level1name=N'InteractionType', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
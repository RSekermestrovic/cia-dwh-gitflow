CREATE PROC [Utility].[Sp_GetRowcountsByBatch]
(
	@BatchKey INT
)
AS
BEGIN
/*
	Developer: Aaron Jackson
	Date: 03/06/2015
	Desc: Returns row counts for a given batch by table
	Exec: EXEC [Utility].[Sp_GetRowcountsByBatch] @BatchKey = 306;
*/

	SET NOCOUNT ON;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT @@servername as [Server], '[Dimension].[Activity]' as [Table], CreatedBatchKey, COUNT(*) as [Rows]
	FROM [Dimension].[Activity]
	WHERE CreatedBatchKey = @BatchKey
	GROUP BY CreatedBatchKey
	UNION ALL
	SELECT @@servername as [Server], '[Dimension].[Account]' as [Table], CreatedBatchKey, COUNT(*) as [Rows]
	FROM [Dimension].[Account]
	WHERE CreatedBatchKey = @BatchKey
	GROUP BY CreatedBatchKey
	UNION ALL
	SELECT @@servername as [Server], '[Dimension].[Day]' as [Table], NULL, COUNT(*) as [Rows]
	FROM [Dimension].[Day]
	WHERE CreatedBatchKey = @BatchKey
	GROUP BY CreatedBatchKey
	UNION ALL
	SELECT @@servername as [Server], '[Dimension].[DayZone]' as [Table], NULL, COUNT(*) as [Rows]
	FROM [Dimension].[DayZone]
	UNION ALL
	SELECT @@servername as [Server], '[Dimension].[Company]' as [Table], CreatedBatchKey, COUNT(*) as [Rows]
	FROM [Dimension].[Company]
	WHERE CreatedBatchKey = @BatchKey
	GROUP BY CreatedBatchKey
	UNION ALL
	SELECT @@servername as [Server], '[Dimension].[BalanceType]' as [Table], CreatedBatchKey, COUNT(*) as [Rows]
	FROM [Dimension].[BalanceType]
	WHERE CreatedBatchKey = @BatchKey
	GROUP BY CreatedBatchKey
	UNION ALL
	SELECT @@servername as [Server], '[Dimension].[Campaign]' as [Table], CreatedBatchKey, COUNT(*) as [Rows]
	FROM [Dimension].[Campaign]
	WHERE CreatedBatchKey = @BatchKey
	GROUP BY CreatedBatchKey
	UNION ALL
	SELECT @@servername as [Server], '[Dimension].[Channel]' as [Table], CreatedBatchKey, COUNT(*) as [Rows]
	FROM [Dimension].[Channel]
	WHERE CreatedBatchKey = @BatchKey
	GROUP BY CreatedBatchKey
	UNION ALL
	SELECT @@servername as [Server], '[Dimension].[Contract]' as [Table], CreatedBatchKey, COUNT(*) as [Rows]
	FROM [Dimension].[Contract]
	WHERE CreatedBatchKey = @BatchKey
	GROUP BY CreatedBatchKey
	UNION ALL
	SELECT @@servername as [Server], '[Dimension].[BetType]' as [Table], CreatedBatchKey, COUNT(*) as [Rows]
	FROM [Dimension].[BetType]
	WHERE CreatedBatchKey = @BatchKey
	GROUP BY CreatedBatchKey
	UNION ALL
	SELECT @@servername as [Server], '[Dimension].[Class]' as [Table],  NULL, COUNT(*) as [Rows]
	FROM [Dimension].[Class]	
	WHERE CreatedBatchKey = @BatchKey
	UNION ALL
	SELECT @@servername as [Server], '[Dimension].[Instrument]' as [Table], CreatedBatchKey, COUNT(*) as [Rows]
	FROM [Dimension].[Instrument]
	WHERE CreatedBatchKey = @BatchKey
	GROUP BY CreatedBatchKey
	UNION ALL
	SELECT @@servername as [Server], '[Dimension].[LegType]' as [Table], CreatedBatchKey, COUNT(*) as [Rows]
	FROM [Dimension].[LegType]
	WHERE CreatedBatchKey = @BatchKey
	GROUP BY CreatedBatchKey
	UNION ALL
	SELECT @@servername as [Server], '[Dimension].[Market]' as [Table], CreatedBatchKey, COUNT(*) as [Rows]
	FROM [Dimension].[Market]
	WHERE CreatedBatchKey = @BatchKey
	GROUP BY CreatedBatchKey
	UNION ALL
	SELECT @@servername as [Server], '[Dimension].[Promotion]' as [Table], CreatedBatchKey, COUNT(*) as [Rows]
	FROM [Dimension].[Promotion]
	WHERE CreatedBatchKey = @BatchKey
	GROUP BY CreatedBatchKey
	UNION ALL
	SELECT @@servername as [Server], '[Dimension].[Time]' as [Table], CreatedBatchKey, COUNT(*) as [Rows]
	FROM [Dimension].[Time]
	WHERE CreatedBatchKey = @BatchKey
	GROUP BY CreatedBatchKey
	UNION ALL
	SELECT @@servername as [Server], '[Dimension].[TransactionDetail]' as [Table], CreatedBatchKey, COUNT(*) as [Rows]
	FROM [Dimension].[TransactionDetail]
	WHERE CreatedBatchKey = @BatchKey
	GROUP BY CreatedBatchKey
	UNION ALL
	SELECT @@servername as [Server], '[Dimension].[User]' as [Table], CreatedBatchKey, COUNT(*) as [Rows]
	FROM [Dimension].[User]
	WHERE CreatedBatchKey = @BatchKey
	GROUP BY CreatedBatchKey
	UNION ALL
	SELECT @@servername as [Server], '[Dimension].[Wallet]' as [Table], CreatedBatchKey, COUNT(*) as [Rows]
	FROM [Dimension].[Wallet]
	WHERE CreatedBatchKey = @BatchKey
	GROUP BY CreatedBatchKey
	UNION ALL
	SELECT @@servername as [Server], '[Fact].[Transaction]' as [Table], CreatedBatchKey, COUNT(*) as [Rows]
	FROM [Fact].[Transaction]
	WHERE CreatedBatchKey = @BatchKey
	GROUP BY CreatedBatchKey
	UNION ALL
	SELECT @@servername as [Server], '[Fact].[Position]' as [Table], CreatedBatchKey, COUNT(*) as [Rows]
	FROM [Fact].[Position]
	WHERE CreatedBatchKey = @BatchKey
	GROUP BY CreatedBatchKey
	UNION ALL
	SELECT @@servername as [Server], '[Fact].[KPI]' as [Table], CreatedBatchKey, COUNT(*) as [Rows]
	FROM [Fact].[KPI]
	WHERE CreatedBatchKey = @BatchKey
	GROUP BY CreatedBatchKey
	ORDER BY [Table]

END
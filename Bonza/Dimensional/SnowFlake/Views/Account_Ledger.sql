﻿CREATE VIEW [SnowFlake].[Account_Ledger] WITH SCHEMABINDING
AS
SELECT	AccountKey,
		LedgerID,
		LegacyLedgerID,
		LedgerSequenceNumber,
		AccountId,
		LedgerTypeID,
		CASE WHEN LedgerSequenceNumber = 1 AND AccountTypeCode = 'C' AND	LedgerGrouping = 'Transactional' AND CompanyKey <> 100 THEN 'Y' ELSE 'N' END ClientAccount
FROM	Dimension.Account

GO

CREATE UNIQUE CLUSTERED INDEX [CI_AccountKey(A)] ON [SnowFlake].[Account_Ledger]
(
	[AccountKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


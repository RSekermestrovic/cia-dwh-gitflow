﻿CREATE VIEW [SnowFlake].[Event_VenueType] WITH SCHEMABINDING
AS

Select	CASE 
			WHEN VenueType  IS NULL THEN 'UNK'
			ELSE VenueType
		END AS VenueTypeId,
		CASE 
			WHEN VenueType  IS NULL THEN 'UNK'
			ELSE VenueType
		END AS VenueType,
		COUNT_BIG(*) z
From [Dimension].[Event]
GROUP BY
	CASE 
			WHEN VenueType  IS NULL THEN 'UNK'
			ELSE VenueType
		END,
		CASE 
			WHEN VenueType  IS NULL THEN 'UNK'
			ELSE VenueType
		END



GO

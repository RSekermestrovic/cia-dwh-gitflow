﻿CREATE VIEW [SnowFlake].[Event_VenueCountry] WITH SCHEMABINDING
AS

Select	--Distinct		
		CASE 
			WHEN VenueCountryCode IS NULL THEN 'XX'
			ELSE VenueCountryCode
		END	AS VenueCountryId,
		CASE 
			WHEN VenueCountryCode IS NULL THEN 'XX'
			ELSE VenueCountryCode
		END	AS VenueCountryCode,
		CASE 
			WHEN VenueCountry = 'UNK' THEN 'UNKNOWN'
			WHEN VenueCountry IS NULL THEN 'UNKNOWN'
			ELSE VenueCountry 
		END AS VenueCountry,
		COUNT_BIG(*) z
From Dimension.[Event] NOLOCK
Group By CASE 
			WHEN VenueCountryCode IS NULL THEN 'XX'
			ELSE VenueCountryCode
		END,
		CASE 
			WHEN VenueCountryCode IS NULL THEN 'XX'
			ELSE VenueCountryCode
		END,
		CASE 
			WHEN VenueCountry = 'UNK' THEN 'UNKNOWN'
			WHEN VenueCountry IS NULL THEN 'UNKNOWN'
			ELSE VenueCountry 
		END

GO


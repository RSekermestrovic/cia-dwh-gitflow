﻿CREATE VIEW [SnowFlake].[Event_VenueState] WITH SCHEMABINDING
AS

Select	
		CASE 
			WHEN VenueCountryCode IS NULL THEN VenueStateCode + 'XX'
			ELSE VenueStateCode + VenueCountryCode
		END	AS VenueStateId,
		VenueStateCode as VenueStateCode,
		CASE 
			WHEN VenueState = 'UNK' THEN 'Unknown'
			ELSE VenueState
		END AS VenueState,
		CASE 
			WHEN VenueCountryCode IS NULL THEN 'XX'
			ELSE VenueCountryCode
		END	AS VenueCountryId,
		COUNT_BIG(*) z
From Dimension.[Event]
GROUP BY
CASE 
			WHEN VenueCountryCode IS NULL THEN VenueStateCode + 'XX'
			ELSE VenueStateCode + VenueCountryCode
		END,
		VenueStateCode,
		CASE 
			WHEN VenueState = 'UNK' THEN 'Unknown'
			ELSE VenueState
		END ,
		CASE 
			WHEN VenueCountryCode IS NULL THEN 'XX'
			ELSE VenueCountryCode
		END




GO

﻿CREATE PROCEDURE [SnowFlake].[sp_EventRound]

AS

BEGIN

SELECT	DISTINCT
		CASE
			WHEN Competition IS NULL or Competition = '' THEN 'Unknown' + CONVERT(Varchar,IsNull(CompetitionStartDate,'1900-01-01')) + CONVERT(Varchar,IsNull(CompetitionEndDate,'1900-01-01')) + CONVERT(Varchar,c.ClassId) + (CASE WHEN [Round] IS NULL OR [Round] = 'Unknown' THEN 'UNK' ELSE [Round] END)
			ELSE Competition + CONVERT(Varchar,IsNull(CompetitionStartDate,'1900-01-01')) + CONVERT(Varchar,IsNull(CompetitionEndDate,'1900-01-01')) + CONVERT(Varchar,c.ClassId) + (CASE WHEN [Round] IS NULL OR [Round] = 'Unknown' THEN 'UNK' ELSE [Round] END)
		END AS RoundID,
		CASE 
			WHEN [Round] IS NULL OR [Round] = 'Unknown' THEN 'UNK'
			ELSE [Round]
		END AS [Round],
		IsNull(RoundSequence,0) RoundSequence,
		CASE
			WHEN Competition IS NULL or Competition = '' THEN 'Unknown' + CONVERT(Varchar,IsNull(CompetitionStartDate,'1900-01-01')) + CONVERT(Varchar,IsNull(CompetitionEndDate,'1900-01-01')) + CONVERT(Varchar,IsNull(c.ClassId,-1))
			ELSE Competition + CONVERT(Varchar,IsNull(CompetitionStartDate,'1900-01-01')) + CONVERT(Varchar,IsNull(CompetitionEndDate,'1900-01-01')) + CONVERT(Varchar,IsNull(c.ClassId,-1))
		END AS CompetitionID
INTO #EventRound
FROM [Dimension].[Event] a
INNER JOIN [Dimension].[Class] c ON c.ClassKey = ISNULL(a.ClassKey, -1)

INSERT INTO [SnowFlake].[Event_Round]
Select a.* 
From #EventRound a
LEFT OUTER JOIN [SnowFlake].[Event_Round] b ON a.RoundID = b.RoundID
Where b.RoundID IS NULL

END

GO

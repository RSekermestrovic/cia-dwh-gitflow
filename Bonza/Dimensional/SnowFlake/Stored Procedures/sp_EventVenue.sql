﻿CREATE PROCEDURE [SnowFlake].[sp_EventVenue]

AS

BEGIN
 
 Select 
	Distinct
	CASE 
		WHEN VenueCountryCode IS NULL THEN CONVERT(Varchar,IsNull(SourceVenueId,-1)) +  VenueStateCode + 'XX' + (CASE WHEN VenueType IS NULL THEN 'UNK' ELSE VenueType END)
		ELSE CONVERT(Varchar,IsNull(SourceVenueId,-1)) + VenueStateCode + VenueCountryCode + (CASE WHEN VenueType IS NULL THEN 'UNK' ELSE VenueType END)
	END	AS VenueId,
	CASE 
		WHEN SourceVenueId IS NULL THEN 'UNK'
		WHEN Venue = 'France-St. Galmier' THEN 'Saint Galmier'
		WHEN Venue = 'Kilerri' THEN 'Killeri'
		WHEN Venue = 'SANDOWN' and VenueCountryCode = 'AUS' THEN 'Sandown Hillside'
		WHEN Venue = 'YARRA GLEN' and VenueCountryCode = 'AUS' THEN 'YARRA VALLEY'
		ELSE Venue
	END AS Venue, 
	CASE 
		WHEN VenueCountryCode IS NULL THEN VenueStateCode + 'XX'
		ELSE VenueStateCode + VenueCountryCode
	END	AS VenueStateId,
	CASE 
		WHEN VenueType  IS NULL THEN 'UNK'
		ELSE VenueType
	END AS VenueTypeId
INTO #EventVenue
From [Dimension].[Event]


INSERT INTO [SnowFlake].[Event_Venue]
Select a.*
From #EventVenue a
LEFT OUTER JOIN [SnowFlake].[Event_Venue] b on a.VenueId = b.VenueId
Where b.VenueId Is NULL

END

GO
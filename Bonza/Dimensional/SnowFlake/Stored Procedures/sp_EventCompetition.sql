﻿CREATE PROCEDURE [SnowFlake].[sp_EventCompetition]

AS

BEGIN

SELECT	DISTINCT
		CASE
			WHEN Competition IS NULL or Competition = '' THEN 'Unknown' + CONVERT(Varchar,IsNull(CompetitionStartDate,'1900-01-01')) + CONVERT(Varchar,IsNull(CompetitionEndDate,'1900-01-01')) + CONVERT(Varchar,c.ClassId)
			ELSE Competition + CONVERT(Varchar,IsNull(CompetitionStartDate,'1900-01-01')) + CONVERT(Varchar,IsNull(CompetitionEndDate,'1900-01-01')) + CONVERT(Varchar,c.ClassId)
		END AS CompetitionID,
		CASE
			WHEN Competition IS NULL or Competition = '' THEN 'Unknown'
			ELSE Competition
		END AS Competition,
		CASE 
			WHEN CompetitionSeason IS NULL THEN 'Unknown'
			ELSE CompetitionSeason
		END AS CompetitionSeason,
		CASE
			WHEN CompetitionStartDate IS NULL THEN '1900-01-01'
			ELSE CompetitionStartDate
		END AS CompetitionStartDate,
		CASE
			WHEN CompetitionEndDate IS NULL THEN '1900-01-01'
			ELSE CompetitionEndDate
		END AS CompetitionEndDate,
		CONVERT(Varchar,c.ClassId) + CONVERT(Varchar,c.ClassKey) SubClassId
INTO #EventComp
FROM [Dimension].[Event] e
INNER JOIN [Dimension].[Class] c ON c.ClassKey = ISNULL(e.ClassKey, -1)


INSERT INTO [SnowFlake].[Event_Competition]
Select a.* 
From #EventComp a
LEFT OUTER JOIN [SnowFlake].[Event_Competition] b on a.CompetitionID = b.CompetitionID
Where b.CompetitionID is null

END

GO
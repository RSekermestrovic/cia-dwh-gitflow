﻿CREATE PROCEDURE [SnowFlake].[sp_EventEvent]

AS
BEGIN

Select	CONVERT(Varchar(20),EventId) + '-' + CONVERT(Varchar(20),c.ClassId) as EventId,
		IsNull([Event], SourceEvent) as [Event],
		ScheduledDateTime as EventDateTime,
		Grade,
		Distance,
		PrizePool,
		ScheduledDayKey/100 as EventDayKey,
		CASE
			WHEN Competition IS NULL or Competition = '' THEN 'Unknown' + CONVERT(Varchar,IsNull(CompetitionStartDate,'1900-01-01')) + CONVERT(Varchar,IsNull(CompetitionEndDate,'1900-01-01')) + CONVERT(Varchar,c.ClassId) + (CASE WHEN [Round] IS NULL OR [Round] = 'Unknown' THEN 'UNK' ELSE [Round] END)
			ELSE Competition + CONVERT(Varchar,IsNull(CompetitionStartDate,'1900-01-01')) + CONVERT(Varchar,IsNull(CompetitionEndDate,'1900-01-01')) + CONVERT(Varchar,c.ClassId) + (CASE WHEN [Round] IS NULL OR [Round] = 'Unknown' THEN 'UNK' ELSE [Round] END)
		END AS RoundID,
		CASE 
			WHEN VenueCountryCode IS NULL THEN CONVERT(Varchar,IsNull(SourceVenueId,-1)) +  VenueStateCode + 'XX' + (CASE WHEN VenueType IS NULL THEN 'UNK' ELSE VenueType END)
			ELSE CONVERT(Varchar,IsNull(SourceVenueId,-1)) + VenueStateCode + VenueCountryCode + (CASE WHEN VenueType IS NULL THEN 'UNK' ELSE VenueType END)
		END	AS VenueId,
		CONVERT(Varchar,c.ClassId) + CONVERT(Varchar,c.ClassKey) SubClassId
INTO #EventEvent
FROM [Dimension].[Event] e
INNER JOIN [Dimension].[Class] c ON c.ClassKey = ISNULL(e.ClassKey, -1)


INSERT INTO [SnowFlake].[Event_Event]
SELECT a.*
From #EventEvent a
LEFT OUTER JOIN [SnowFlake].[Event_Event] b on a.EventId = b.EventId
Where b.EventID Is NULL

END


GO
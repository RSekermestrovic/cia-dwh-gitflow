﻿CREATE TABLE [SnowFlake].[Event_Competition](
	[CompetitionID] [varchar](345) NULL,
	[Competition] [varchar](255) NULL,
	[CompetitionSeason] [varchar](32) NULL,
	[CompetitionStartDate] [date] NULL,
	[CompetitionEndDate] [date] NULL,
	[SubClassId] [varchar](60) NOT NULL
) ON [PRIMARY]

GO

CREATE CLUSTERED COLUMNSTORE INDEX [CC_Competition] ON [SnowFlake].[Event_Competition] WITH (DROP_EXISTING = OFF) ON [PRIMARY]
GO
﻿CREATE TABLE [SnowFlake].[Event_Event](
	[EventId] [varchar](50) NOT NULL,
	[Event] [varchar](255) NULL,
	[EventDateTime] [datetime] NULL,
	[Grade] [varchar](255) NULL,
	[Distance] [varchar](10) NULL,
	[PrizePool] [int] NULL,
	[EventDayKey] [int] NULL,
	[RoundID] [varchar](600) NULL,
	[VenueId] [varchar](136) NULL,
	[SubClassId] [varchar](60) NOT NULL
) ON [PRIMARY]

GO

CREATE CLUSTERED COLUMNSTORE INDEX [CC_Event] ON [SnowFlake].[Event_Event] WITH (DROP_EXISTING = OFF) ON [PRIMARY]
GO

﻿/*
ALTER DATABASE [$(DatabaseName)]
	ADD LOG FILE 
	(
		NAME=[Dimensional_log], 
		FILENAME='$(DimensionalLogPath)$(DatabaseName)_Primary.ldf', 
		SIZE=20000 MB, 
		FILEGROWTH=10%
	)
*/
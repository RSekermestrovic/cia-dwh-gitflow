﻿/*
ALTER DATABASE [$(DatabaseName)]
	ADD FILE
	(
		NAME=Dimensional, 
		FILENAME='$(DimensionalDataPath)$(DatabaseName)_Primary.mdf', 
		SIZE=40000 MB, 
		FILEGROWTH=10%
	)
	
*/

﻿CREATE PROCEDURE [Control].[Sp_RemotePartitioning]
(	
	@PartitionFunction	varchar(128),	-- Mandatory - Name of partition function for which partitions are to be split/merged
	@PartitionScheme	varchar(128),	-- Mandatory - Name of partition scheme for which partitions are to be split/merged
	@Value				varchar(100),	-- Mandatory - The partitioning value at which the partitions are to be split/mergd
	@Action				varchar(5)		-- Mandatory - The desired partition management action - either 'SPLIT' OR 'MERGE'
	)
AS

	SET NOCOUNT ON 

	DECLARE	@SQL varchar(MAX)

BEGIN

	IF @Action = 'SPLIT' SET @SQL = 'ALTER PARTITION SCHEME '+ @PartitionScheme +' NEXT USED [PRIMARY] ALTER PARTITION FUNCTION '+@PartitionFunction+'() SPLIT RANGE ('+@Value+')'
	IF @Action = 'MERGE' SET @SQL = 'ALTER PARTITION FUNCTION '+@PartitionFunction+'() MERGE RANGE ('+CONVERT(varchar,@Value)+')'

	EXEC(@SQL)
END
﻿CREATE TABLE [Control].[CompleteDate] (
    [TransactionCompleteDate] DATETIME2 (7) NULL,
    [KPICompleteDate]         DATETIME2 (7) NULL,
    [PositionCompleteDate]    DATETIME2 (7) NULL,
	[BalanceCompleteDate]	  DATETIME2 (7) NULL,
	[ExactTargetEmailCompleteDate] DATETIME2(7)  NULL,
	[ExactTargetSmsCompleteDate] DATETIME2(7)  NULL,
	[ExactTargetPushCompleteDate] DATETIME2(7)  NULL
);




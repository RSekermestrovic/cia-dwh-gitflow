﻿CREATE TABLE [Control].[ETLController]
(
	BatchKey int
	, BatchFromDate datetime2
	, BatchToDate datetime2
	, DimensionStatus int
	, TransactionStatus int
	, [KPIFactStatus] int
	, [PositionFactStatus] int
	, [BalanceFactStatus] int
	, BatchStatus int
)

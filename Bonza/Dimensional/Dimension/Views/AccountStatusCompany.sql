CREATE VIEW [Dimension].[AccountStatusCompany] WITH SCHEMABINDING AS
SELECT [AccountStatusKey]
      ,[AccountKey]
      ,[LedgerID]
      ,[LedgerSource]
      ,[LedgerEnabled]
      ,[LedgerStatus]
      ,[AccountType]
      ,[InternetProfile]
      ,[VIP]
      ,a.[CompanyKey]
      ,[CreditLimit]
      ,[MinBetAmount]
      ,[MaxBetAmount]
      ,[Introducer]
      ,[Handled]
      ,[PhoneColourDesc]
      ,[DisableDeposit]
      ,[DisableWithdrawal]
      ,[AccountClosedByUserID]
      ,[AccountClosedByUserSource]
      ,[AccountClosedByUserName]
      ,[AccountClosedDate]
      ,[ReIntroducedBy]
      ,[ReIntroducedDate]
      ,[AccountClosureReason]
      ,[DepositLimit]
      ,[LossLimit]
      ,[LimitPeriod]
      ,[LimitTimeStamp]
      ,[ReceiveMarketingEmail]
      ,[ReceiveCompetitionEmail]
      ,[ReceiveSMS]
      ,[ReceivePostalMail]
      ,[ReceiveFax]
      ,[CurrentVersion]
      ,a.[FromDate]
      ,a.[ToDate]
      ,a.[FirstDate]
      ,a.[CreatedDate]
      ,a.[CreatedBatchKey]
      ,a.[CreatedBy]
      ,a.[ModifiedDate]
      ,a.[ModifiedBatchKey]
      ,a.[ModifiedBy]
      ,[OrgId]
      ,[DivisionId]
      ,[DivisionName]
      ,[BrandId]
      ,[BrandCode]
      ,[BrandName]
      ,[BusinessUnitId]
      ,[BusinessUnitCode]
      ,[BusinessUnitName]
      ,[CompanyID]
      ,[CompanyCode]
      ,[CompanyName]
  FROM [Dimension].[AccountStatus] a
		INNER JOIN [Dimension].[Company] c ON (c.CompanyKey = a.CompanyKey)
GO

CREATE UNIQUE CLUSTERED INDEX [CI_AccountStatusCompany] ON [Dimension].[AccountStatusCompany]
(
	[AccountStatusKey] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [NI_AccountStatusComapny_NK]
    ON [Dimension].[AccountStatusCompany]([LedgerId] ASC, [LedgerSource] ASC, [FromDate] ASC, [ToDate] ASC)
    INCLUDE([AccountStatusKey]);
GO


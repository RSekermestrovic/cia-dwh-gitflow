Create Procedure [Dimension].sp_InitialPopInstrument

as

IF not exists (Select	[InstrumentKey] from [Dimension].[Instrument])
Begin
Begin Transaction
Set IDENTITY_INSERT [Dimension].[Instrument] ON 

INSERT [Dimension].[Instrument] ([InstrumentKey], [InstrumentID], [Source], [AccountNumber], [SafeAccountNumber], [AccountName], [BSB], [ExpiryDate], [Verified], [VerifyAmount], [InstrumentTypeKey], [InstrumentType], [ProviderKey], [ProviderName], [FromDate], [ToDate], [FirstDate], [CreatedDate], [CreatedBatchKey], [CreatedBy], [ModifiedDate], [ModifiedBatchKey], [ModifiedBy]) VALUES (-1, -1, N'UNK', N'UNK', N'UNK', N'UNK', N'UNK', CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), N'UNK', NULL, -1, N'UNK', -1, N'UNK', CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), CAST(N'9999-12-31 00:00:00.0000000' AS DateTime2), CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), CAST(N'2014-12-17 09:43:32.5230000' AS DateTime2), 1, N'sp_InitialPopInstrument', CAST(N'2014-12-17 09:43:32.5230000' AS DateTime2), 1, N'sp_InitialPopInstrument')
INSERT [Dimension].[Instrument] ([InstrumentKey], [InstrumentID], [Source], [AccountNumber], [SafeAccountNumber], [AccountName], [BSB], [ExpiryDate], [Verified], [VerifyAmount], [InstrumentTypeKey], [InstrumentType], [ProviderKey], [ProviderName], [FromDate], [ToDate], [FirstDate], [CreatedDate], [CreatedBatchKey], [CreatedBy], [ModifiedDate], [ModifiedBatchKey], [ModifiedBy]) VALUES (0, 0, N'N/A', N'N/A', N'N/A', N'N/A', N'N/A', CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), N'N/A', NULL, -1, N'N/A', -1, N'N/A', CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), CAST(N'9999-12-31 00:00:00.0000000' AS DateTime2), CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), CAST(N'2014-12-17 09:43:32.5230000' AS DateTime2), 1, N'sp_InitialPopInstrument', CAST(N'2014-12-17 09:43:32.5230000' AS DateTime2), 1, N'sp_InitialPopInstrument')
Set IDENTITY_INSERT [Dimension].[Instrument] OFF 

IF @@ERROR = 0
Begin
	Commit Transaction
End
Else
Begin
	Rollback Transaction
End
End
Create Procedure [Dimension].sp_InitialPopContract

as

IF not exists (Select	[ContractKey] from [Dimension].[Contract] where [ContractKey] = -1)
Begin
Begin Transaction
SET IDENTITY_INSERT [Dimension].[Contract] ON 

INSERT INTO [Dimension].[Contract]
           ([ContractKey]
		   ,[BetGroupID]
           ,[BetGroupIDSource]
           ,[BetId]
           ,[BetIdSource]
           ,[LegId]
           ,[LegIdSource]
           ,[FreeBetID]
           ,[ExternalID]
           ,[External1]
           ,[External2]
           ,[FromDate]
           ,[ToDate]
           ,[FirstDate]
           ,[CreatedDate]
           ,[CreatedBatchKey]
           ,[CreatedBy]
           ,[ModifiedDate]
           ,[ModifiedBatchKey]
           ,[ModifiedBy])
Values (-1,	-1,	'UNK',	-1,	'UNK',	-1,	'UNK',	0,	NULL,	NULL,	NULL,	'1900-01-01 00:00:00',	'9999-12-31 00:00:00',	'1900-01-01 00:00:00',	getdate(),	0,	'sp_InitialPopContract',	getdate(),	0,	'sp_InitialPopContract')
, (0,	-1,	'N/A',	-1,	'N/A',	-1,	'N/A',	0,	NULL,	NULL,	NULL,	'1900-01-01 00:00:00',	'9999-12-31 00:00:00',	'1900-01-01 00:00:00',	getdate(),	0,	'sp_InitialPopContract',	getdate(),	0,	'sp_InitialPopContract')

SET IDENTITY_INSERT [Dimension].[Contract] OFF
IF @@ERROR = 0
Begin
	Commit Transaction
End
Else
Begin
	Rollback Transaction
End
End
﻿Create Procedure [Dimension].sp_InitialPopRevenueLifeCycle

as

IF not exists (Select	[RevenueLifeCycleKey] from [Dimension].[RevenueLifeCycle])
Begin
Begin Transaction

INSERT [Dimension].[RevenueLifeCycle] ([RevenueLifeCycleKey], [RevenueLifeCycle], [CreatedDate], [CreatedBatchKey], [CreatedBy], [ModifiedDate], [ModifiedBatchKey], [ModifiedBy]) VALUES (-1, N'Unknown', CAST(N'2015-11-27 11:26:18.7800000' AS DateTime2), 0, N'sp_InitialPopLifestage', CAST(N'2015-11-27 11:26:18.7800000' AS DateTime2), 0, N'sp_InitialPopRevenueLifeCycle')
INSERT [Dimension].[RevenueLifeCycle] ([RevenueLifeCycleKey], [RevenueLifeCycle], [CreatedDate], [CreatedBatchKey], [CreatedBy], [ModifiedDate], [ModifiedBatchKey], [ModifiedBy]) VALUES (0, N'N/A', CAST(N'2015-11-27 11:26:18.7800000' AS DateTime2), 0, N'sp_InitialPopLifestage', CAST(N'2015-11-27 11:26:18.7800000' AS DateTime2), 0, N'sp_InitialPopRevenueLifeCycle')
INSERT [Dimension].[RevenueLifeCycle] ([RevenueLifeCycleKey], [RevenueLifeCycle], [CreatedDate], [CreatedBatchKey], [CreatedBy], [ModifiedDate], [ModifiedBatchKey], [ModifiedBy]) VALUES (1, N'FTB', CAST(N'2015-11-27 11:26:18.7800000' AS DateTime2), 0, N'sp_InitialPopLifestage', CAST(N'2015-11-27 11:26:18.7800000' AS DateTime2), 0, N'sp_InitialPopRevenueLifeCycle')
INSERT [Dimension].[RevenueLifeCycle] ([RevenueLifeCycleKey], [RevenueLifeCycle], [CreatedDate], [CreatedBatchKey], [CreatedBy], [ModifiedDate], [ModifiedBatchKey], [ModifiedBy]) VALUES (2, N'Retained', CAST(N'2015-11-27 11:26:18.7800000' AS DateTime2), 0, N'sp_InitialPopLifestage', CAST(N'2015-11-27 11:26:18.7800000' AS DateTime2), 0, N'sp_InitialPopRevenueLifeCycle')
INSERT [Dimension].[RevenueLifeCycle] ([RevenueLifeCycleKey], [RevenueLifeCycle], [CreatedDate], [CreatedBatchKey], [CreatedBy], [ModifiedDate], [ModifiedBatchKey], [ModifiedBy]) VALUES (3, N'Reactivated', CAST(N'2015-11-27 11:26:18.7800000' AS DateTime2), 0, N'sp_InitialPopLifestage', CAST(N'2015-11-27 11:26:18.7800000' AS DateTime2), 0, N'sp_InitialPopRevenueLifeCycle')

IF @@ERROR = 0
Begin
	Commit Transaction
End
Else
Begin
	Rollback Transaction
End
End
﻿Create Procedure [Dimension].sp_InitialPopInPlay

as

IF not exists (Select	[InPlayKey] from [Dimension].[InPlay])
Begin
Begin Transaction

INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (-1, -1, N'Unknown', N'Unknown', N'Unk', N'Unknown', 0, 0, 'N/A')

INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (0, 0, N'N/A', N'N/A', N'No', N'N/A', 0, 0, 'N/A')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (1, 0, N'N', N'N', N'No', N'N/A', 0, 0, 'N/A')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (2, 0, N'Y', N'N', N'No', N'N/A', 0, 0, 'N/A')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (3, 0, N'N', N'Y', N'No', N'N/A', 0, 0, 'N/A')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (4, 0, N'Y', N'Y', N'No', N'N/A', 0, 0, 'N/A')

INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (5, 1, N'N/A', N'N/A', N'Yes', N'PreGame', 0, 0, 'N/A')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (6, 1, N'N', N'N', N'Yes', N'PreGame', 0, 0, 'N/A')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (7, 1, N'Y', N'N', N'Yes', N'PreGame', 0, 0, 'N/A')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (8, 1, N'N', N'Y', N'Yes', N'PreGame', 0, 0, 'N/A')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (9, 1, N'Y', N'Y', N'Yes', N'PreGame', 0, 0, 'N/A')

INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (10, 2, N'N/A', N'N/A', N'Yes', N'InPlay', 0, 0, 'N/A')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (11, 2, N'N', N'N', N'Yes', N'InPlay', 0, 0, 'N/A')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (12, 2, N'Y', N'N', N'Yes', N'InPlay', 0, 0, 'N/A')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (13, 2, N'N', N'Y', N'Yes', N'InPlay', 0, 0, 'N/A')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (14, 2, N'Y', N'Y', N'Yes', N'InPlay', 0, 0, 'N/A')

INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (15, 3, N'N/A', N'N/A', N'Yes', N'InPlay', 0, 0, 'N/A')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (16, 3, N'N', N'N', N'Yes', N'InPlay', 0, 0, 'N/A')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (17, 3, N'Y', N'N', N'Yes', N'InPlay', 0, 0, 'N/A')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (18, 3, N'N', N'Y', N'Yes', N'InPlay', 0, 0, 'N/A')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (19, 3, N'Y', N'Y', N'Yes', N'InPlay', 0, 0, 'N/A')

INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (20, 4, N'N/A', N'N/A', N'Yes', N'Settled', 0, 0, 'N/A')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (21, 4, N'N', N'N', N'Yes', N'Settled', 0, 0, 'N/A')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (22, 4, N'Y', N'N', N'Yes', N'Settled', 0, 0, 'N/A')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (23, 4, N'N', N'Y', N'Yes', N'Settled', 0, 0, 'N/A')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (24, 4, N'Y', N'Y', N'Yes', N'Settled', 0, 0, 'N/A')

INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (25, 5, N'N/A', N'N/A', N'Yes', N'Multi Max', 0, 0, 'N/A')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (26, 5, N'N', N'N', N'Yes', N'Multi Max', 0, 0, 'N/A')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (27, 5, N'Y', N'N', N'Yes', N'Multi Max', 0, 0, 'N/A')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (28, 5, N'N', N'Y', N'Yes', N'Multi Max', 0, 0, 'N/A')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (29, 5, N'Y', N'Y', N'Yes', N'Multi Max', 0, 0, 'N/A')

INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (30, 0, N'N/A', N'N/A', N'No', N'N/A', 1, 0, 'Initial')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (31, 0, N'N', N'N', N'No', N'N/A', 1, 0, 'Initial')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (32, 0, N'Y', N'N', N'No', N'N/A', 1, 0, 'Initial')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (33, 0, N'N', N'Y', N'No', N'N/A', 1, 0, 'Initial')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (34, 0, N'Y', N'Y', N'No', N'N/A', 1, 0, 'Initial')

INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (35, 1, N'N/A', N'N/A', N'Yes', N'PreGame', 1, 0, 'Initial')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (36, 1, N'N', N'N', N'Yes', N'PreGame', 1, 0, 'Initial')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (37, 1, N'Y', N'N', N'Yes', N'PreGame', 1, 0, 'Initial')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (38, 1, N'N', N'Y', N'Yes', N'PreGame', 1, 0, 'Initial')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (39, 1, N'Y', N'Y', N'Yes', N'PreGame', 1, 0, 'Initial')

INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (40, 2, N'N/A', N'N/A', N'Yes', N'InPlay', 1, 0, 'Initial')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (41, 2, N'N', N'N', N'Yes', N'InPlay', 1, 0, 'Initial')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (42, 2, N'Y', N'N', N'Yes', N'InPlay', 1, 0, 'Initial')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (43, 2, N'N', N'Y', N'Yes', N'InPlay', 1, 0, 'Initial')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (44, 2, N'Y', N'Y', N'Yes', N'InPlay', 1, 0, 'Initial')

INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (45, 3, N'N/A', N'N/A', N'Yes', N'InPlay', 1, 0, 'Initial')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (46, 3, N'N', N'N', N'Yes', N'InPlay', 1, 0, 'Initial')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (47, 3, N'Y', N'N', N'Yes', N'InPlay', 1, 0, 'Initial')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (48, 3, N'N', N'Y', N'Yes', N'InPlay', 1, 0, 'Initial')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (49, 3, N'Y', N'Y', N'Yes', N'InPlay', 1, 0, 'Initial')

INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (50, 4, N'N/A', N'N/A', N'Yes', N'Settled', 1, 0, 'Initial')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (51, 4, N'N', N'N', N'Yes', N'Settled', 1, 0, 'Initial')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (52, 4, N'Y', N'N', N'Yes', N'Settled', 1, 0, 'Initial')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (53, 4, N'N', N'Y', N'Yes', N'Settled', 1, 0, 'Initial')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (54, 4, N'Y', N'Y', N'Yes', N'Settled', 1, 0, 'Initial')

INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (55, 5, N'N/A', N'N/A', N'Yes', N'Multi Max', 1, 0, 'Initial')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (56, 5, N'N', N'N', N'Yes', N'Multi Max', 1, 0, 'Initial')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (57, 5, N'Y', N'N', N'Yes', N'Multi Max', 1, 0, 'Initial')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (58, 5, N'N', N'Y', N'Yes', N'Multi Max', 1, 0, 'Initial')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (59, 5, N'Y', N'Y', N'Yes', N'Multi Max', 1, 0, 'Initial')

INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (60, 0, N'N/A', N'N/A', N'No', N'N/A', 0, 1, 'DoubledDown')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (61, 0, N'N', N'N', N'No', N'N/A', 0, 1, 'DoubledDown')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (62, 0, N'Y', N'N', N'No', N'N/A', 0, 1, 'DoubledDown')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (63, 0, N'N', N'Y', N'No', N'N/A', 0, 1, 'DoubledDown')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (64, 0, N'Y', N'Y', N'No', N'N/A', 0, 1, 'DoubledDown')

INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (65, 1, N'N/A', N'N/A', N'Yes', N'PreGame', 0, 1, 'DoubledDown')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (66, 1, N'N', N'N', N'Yes', N'PreGame', 0, 1, 'DoubledDown')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (67, 1, N'Y', N'N', N'Yes', N'PreGame', 0, 1, 'DoubledDown')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (68, 1, N'N', N'Y', N'Yes', N'PreGame', 0, 1, 'DoubledDown')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (69, 1, N'Y', N'Y', N'Yes', N'PreGame', 0, 1, 'DoubledDown')

INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (70, 2, N'N/A', N'N/A', N'Yes', N'InPlay', 0, 1, 'DoubledDown')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (71, 2, N'N', N'N', N'Yes', N'InPlay', 0, 1, 'DoubledDown')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (72, 2, N'Y', N'N', N'Yes', N'InPlay', 0, 1, 'DoubledDown')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (73, 2, N'N', N'Y', N'Yes', N'InPlay', 0, 1, 'DoubledDown')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (74, 2, N'Y', N'Y', N'Yes', N'InPlay', 0, 1, 'DoubledDown')

INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (75, 3, N'N/A', N'N/A', N'Yes', N'InPlay', 0, 1, 'DoubledDown')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (76, 3, N'N', N'N', N'Yes', N'InPlay', 0, 1, 'DoubledDown')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (77, 3, N'Y', N'N', N'Yes', N'InPlay', 0, 1, 'DoubledDown')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (78, 3, N'N', N'Y', N'Yes', N'InPlay', 0, 1, 'DoubledDown')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (79, 3, N'Y', N'Y', N'Yes', N'InPlay', 0, 1, 'DoubledDown')

INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (80, 4, N'N/A', N'N/A', N'Yes', N'Settled', 0, 1, 'DoubledDown')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (81, 4, N'N', N'N', N'Yes', N'Settled', 0, 1, 'DoubledDown')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (82, 4, N'Y', N'N', N'Yes', N'Settled', 0, 1, 'DoubledDown')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (83, 4, N'N', N'Y', N'Yes', N'Settled', 0, 1, 'DoubledDown')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (84, 4, N'Y', N'Y', N'Yes', N'Settled', 0, 1, 'DoubledDown')

INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (85, 5, N'N/A', N'N/A', N'Yes', N'Multi Max', 0, 1, 'DoubledDown')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (86, 5, N'N', N'N', N'Yes', N'Multi Max', 0, 1, 'DoubledDown')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (87, 5, N'Y', N'N', N'Yes', N'Multi Max', 0, 1, 'DoubledDown')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (88, 5, N'N', N'Y', N'Yes', N'Multi Max', 0, 1, 'DoubledDown')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (89, 5, N'Y', N'Y', N'Yes', N'Multi Max', 0, 1, 'DoubledDown')

INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (90, 0, N'N/A', N'N/A', N'No', N'N/A', 1, 1, 'DoubledDown')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (91, 0, N'N', N'N', N'No', N'N/A', 1, 1, 'DoubledDown')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (92, 0, N'Y', N'N', N'No', N'N/A', 1, 1, 'DoubledDown')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (93, 0, N'N', N'Y', N'No', N'N/A', 1, 1, 'DoubledDown')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (94, 0, N'Y', N'Y', N'No', N'N/A', 1, 1, 'DoubledDown')

INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (95, 1, N'N/A', N'N/A', N'Yes', N'PreGame', 1, 1, 'DoubledDown')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (96, 1, N'N', N'N', N'Yes', N'PreGame', 1, 1, 'DoubledDown')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (97, 1, N'Y', N'N', N'Yes', N'PreGame', 1, 1, 'DoubledDown')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (98, 1, N'N', N'Y', N'Yes', N'PreGame', 1, 1, 'DoubledDown')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (99, 1, N'Y', N'Y', N'Yes', N'PreGame', 1, 1, 'DoubledDown')

INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (100, 2, N'N/A', N'N/A', N'Yes', N'InPlay', 1, 1, 'DoubledDown')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (101, 2, N'N', N'N', N'Yes', N'InPlay', 1, 1, 'DoubledDown')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (102, 2, N'Y', N'N', N'Yes', N'InPlay', 1, 1, 'DoubledDown')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (103, 2, N'N', N'Y', N'Yes', N'InPlay', 1, 1, 'DoubledDown')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (104, 2, N'Y', N'Y', N'Yes', N'InPlay', 1, 1, 'DoubledDown')

INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (105, 3, N'N/A', N'N/A', N'Yes', N'InPlay', 1, 1, 'DoubledDown')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (106, 3, N'N', N'N', N'Yes', N'InPlay', 1, 1, 'DoubledDown')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (107, 3, N'Y', N'N', N'Yes', N'InPlay', 1, 1, 'DoubledDown')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (108, 3, N'N', N'Y', N'Yes', N'InPlay', 1, 1, 'DoubledDown')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (109, 3, N'Y', N'Y', N'Yes', N'InPlay', 1, 1, 'DoubledDown')

INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (110, 4, N'N/A', N'N/A', N'Yes', N'Settled', 1, 1, 'DoubledDown')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (111, 4, N'N', N'N', N'Yes', N'Settled', 1, 1, 'DoubledDown')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (112, 4, N'Y', N'N', N'Yes', N'Settled', 1, 1, 'DoubledDown')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (113, 4, N'N', N'Y', N'Yes', N'Settled', 1, 1, 'DoubledDown')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (114, 4, N'Y', N'Y', N'Yes', N'Settled', 1, 1, 'DoubledDown')

INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (115, 5, N'N/A', N'N/A', N'Yes', N'Multi Max', 1, 1, 'DoubledDown')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (116, 5, N'N', N'N', N'Yes', N'Multi Max', 1, 1, 'DoubledDown')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (117, 5, N'Y', N'N', N'Yes', N'Multi Max', 1, 1, 'DoubledDown')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (118, 5, N'N', N'Y', N'Yes', N'Multi Max', 1, 1, 'DoubledDown')
INSERT [Dimension].[InPlay] ([InPlayKey], [CashoutType], [InPlay], [ClickToCall], [CashedOut], [StatusAtCashout], [IsDoubleDown], [IsDoubledDown], [DoubleDown]) VALUES (119, 5, N'Y', N'Y', N'Yes', N'Multi Max', 1, 1, 'DoubledDown')

IF @@ERROR = 0
Begin
	Commit Transaction
End
Else
Begin
	Rollback Transaction
End
End
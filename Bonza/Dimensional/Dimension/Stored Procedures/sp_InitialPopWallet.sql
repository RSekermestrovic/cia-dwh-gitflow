CREATE PROCEDURE [Dimension].[sp_InitialPopWallet]
AS
BEGIN

	IF NOT EXISTS (SELECT [WalletKey] FROM [Dimension].[Wallet])
	BEGIN
		
		SET XACT_ABORT ON;
		
		BEGIN TRANSACTION

		SET IDENTITY_INSERT [Dimension].[Wallet] ON 

		INSERT INTO [Dimension].[Wallet]
		(
			 [WalletKey]
			,[WalletId]
			,[WalletName]
			,[FromDate]
			,[ToDate]
			,[FirstDate]
			,[CreatedDate]
			,[CreatedBatchKey]
			,[CreatedBy]
			,[ModifiedDate]
			,[ModifiedBatchKey]
			,[ModifiedBy]
		)
		SELECT 1,  'C', 'Cash', GETDATE(), '31-Dec-9999', GETDATE(), GETDATE(), 0, 'sp_InitialPopWallet', GETDATE(), 0, 'sp_InitialPopWallet'
		UNION ALL
		SELECT 2,  'B', 'Bonus', GETDATE(), '31-Dec-9999', GETDATE(), GETDATE(), 0, 'sp_InitialPopWallet', GETDATE(), 0, 'sp_InitialPopWallet'
		UNION ALL
		SELECT -1, 'U', 'Unknown', GETDATE(), '31-Dec-9999', GETDATE(), GETDATE(), 0, 'sp_InitialPopWallet', GETDATE(), 0, 'sp_InitialPopWallet'
		UNION ALL 
		SELECT 0, 'N', 'N/A', GETDATE(), '31-Dec-9999', GETDATE(), GETDATE(), 0, 'sp_InitialPopWallet', GETDATE(), 0, 'sp_InitialPopWallet';

		SET IDENTITY_INSERT [Dimension].[Wallet] OFF;

		COMMIT;

	END

END
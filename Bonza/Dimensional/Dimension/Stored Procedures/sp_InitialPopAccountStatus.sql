﻿CREATE PROCEDURE [Dimension].[sp_InitialPopAccountStatus]
	AS
	IF not exists (Select	[AccountStatusKey] from [Dimension].[AccountStatus])
Begin
Begin Transaction
	
	INSERT [Dimension].[AccountStatus] ([AccountStatusKey],	[AccountKey],	[LedgerID],	[LedgerSource] ,	[LedgerEnabled] ,	[LedgerStatus],	[AccountType] ,	[InternetProfile] ,	[VIP] , [CompanyKey],
	[CreditLimit] ,	[MinBetAmount],	[MaxBetAmount] ,	[Introducer],	[Handled] ,	[PhoneColourDesc] ,	[DisableDeposit] ,	[DisableWithdrawal] ,	[AccountClosedByUserID],	[AccountClosedByUserSource],
	[AccountClosedByUserName] ,	[AccountClosedDate] ,	[ReIntroducedBy],	[ReIntroducedDate],	AccountClosureReason ,	[DepositLimit] ,	[LossLimit],	[LimitPeriod],	[LimitTimeStamp] ,
	[ReceiveMarketingEmail] ,	[ReceiveCompetitionEmail],	[ReceiveSMS] ,	[ReceivePostalMail] ,	[ReceiveFax] ,	[CurrentVersion],	[FromDate] ,	[ToDate] ,	[FirstDate] ,
	[CreatedDate] ,	[CreatedBatchKey],	[CreatedBy] ,	[ModifiedDate] ,	[ModifiedBatchKey],	[ModifiedBy] ) 	
	VALUES (-1, -1, -1, N'Unk', N'Unk',N'U', N'Unk',N'Unk',N'Unk',-1,
	0,0,0, N'Unk', N'Unk',N'Unk',N'Unk',N'Unk',-1,N'Unk',
	N'Unk',  CAST(N'1900-01-01 00:00:00.000' AS DateTime),-1,CAST(N'1900-01-01 00:00:00.000' AS DateTime),N'Unk',0,0,-1, CAST(N'1900-01-01 00:00:00.000' AS DateTime),
	N'Unk', N'Unk',N'Unk',N'Unk',N'Unk',N'Unk',	CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2),CAST(N'9999-12-31 00:00:00.0000000' AS DateTime2),CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2),
	CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2),	1,N'sp_InitialPopAccountStatus', CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2),1,N'sp_InitialPopAccountStatus'),
	(0, 0, 0, N'N/A', N'N/A',N'U', N'N/A',N'N/A',N'N/A',0,
	0,0,0, N'N/A', N'N/A',N'N/A',N'N/A',N'N/A',0,N'N/A',
	N'N/A',  CAST(N'1900-01-01 00:00:00.000' AS DateTime),0,CAST(N'1900-01-01 00:00:00.000' AS DateTime),N'N/A',0,0,0, CAST(N'1900-01-01 00:00:00.000' AS DateTime),
	N'N/A', N'N/A',N'N/A',N'N/A',N'N/A',N'N/A',	CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2),CAST(N'9999-12-31 00:00:00.0000000' AS DateTime2),CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2),
	CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2),	1,N'sp_InitialPopAccountStatus', CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2),1,N'sp_InitialPopAccountStatus')
	
	IF @@ERROR = 0
Begin
	Commit Transaction
End
Else
Begin
	Rollback Transaction
End
End

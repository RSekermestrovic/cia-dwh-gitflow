Create Procedure [Dimension].sp_InitialPopUser

as

IF not exists (Select	[UserKey] from [Dimension].[User])
Begin
Begin Transaction
SET IDENTITY_INSERT [Dimension].[User] ON 

INSERT [Dimension].[User] ([UserKey], [UserId], [Source], [SystemName], [EmployeeKey], [Forename], [Surname], [PayrollNumber], [FromDate], [ToDate], [FirstDate], [CreatedDate], [CreatedBatchKey], [CreatedBy], [ModifiedDate], [ModifiedBatchKey], [ModifiedBy]) 
					VALUES (-1,		    N'Unknown',   N'Unk',        N'Unk',				-1,	   N'Unk',    N'Unk',          N'Unk', CAST(N'2014-04-03 15:03:27.0000000' AS DateTime2), CAST(N'2014-04-03 15:03:27.0000000' AS DateTime2), CAST(N'2014-04-03 15:03:27.0000000' AS DateTime2), CAST(N'2014-04-03 15:03:27.4170000' AS DateTime2), 1, N'sp_InitialPopUser', CAST(N'2014-04-03 15:03:27.4170000' AS DateTime2), 1, N'sp_InitialPopUser')
INSERT [Dimension].[User] ([UserKey], [UserId], [Source], [SystemName], [EmployeeKey], [Forename], [Surname], [PayrollNumber], [FromDate], [ToDate], [FirstDate], [CreatedDate], [CreatedBatchKey], [CreatedBy], [ModifiedDate], [ModifiedBatchKey], [ModifiedBy]) 
				   VALUES (0,			  N'N/A',   N'N/A',       N'N/A',             0,     N'N/A',    N'N/A',          N'N/A', CAST(N'2014-04-03 15:03:56.0000000' AS DateTime2), CAST(N'2014-04-03 15:03:56.0000000' AS DateTime2), CAST(N'2014-04-03 15:03:56.0000000' AS DateTime2), CAST(N'2014-04-03 15:03:56.0900000' AS DateTime2), 1, N'sp_InitialPopUser', CAST(N'2014-04-03 15:03:56.0900000' AS DateTime2), 1, N'sp_InitialPopUser')

SET IDENTITY_INSERT [Dimension].[User] OFF
IF @@ERROR = 0
Begin
	Commit Transaction
End
Else
Begin
	Rollback Transaction
End
End

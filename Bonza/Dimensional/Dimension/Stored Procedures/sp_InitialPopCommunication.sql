﻿Create Procedure [Dimension].sp_InitialPopCommunication

as

IF not exists (Select	[CommunicationKey] from [Dimension].[Communication])
Begin
Begin Transaction

SET IDENTITY_INSERT [Dimension].[Communication] ON 

INSERT [Dimension].[Communication] ([CommunicationKey], [CommunicationId], [CommunicationSource], [Title], [Details], [SubDetails], [CreatedDate], [ModifiedDate], [CreatedBatchKey], [CreatedBy], [ModifiedBatchKey], [ModifiedBy]) VALUES (-1, -1, N'ET', N'N/A', N'N/A', N'N/A', CAST(N'2015-10-01 00:00:00.0000000' AS DateTime2), CAST(N'2015-10-01 00:00:00.0000000' AS DateTime2), 0, N'sp_InitialPopCommunication', 0, N'sp_InitialPopCommunication')
INSERT [Dimension].[Communication] ([CommunicationKey], [CommunicationId], [CommunicationSource], [Title], [Details], [SubDetails], [CreatedDate], [ModifiedDate], [CreatedBatchKey], [CreatedBy], [ModifiedBatchKey], [ModifiedBy]) VALUES (0, 0, N'Unk', N'Unknown', N'Unknown', N'Unknown', CAST(N'2015-10-01 00:00:00.0000000' AS DateTime2), CAST(N'2015-10-01 00:00:00.0000000' AS DateTime2), 0, N'sp_InitialPopCommunication', 0, N'sp_InitialPopCommunication')

SET IDENTITY_INSERT [Dimension].[Communication] OFF

IF @@ERROR = 0
Begin
	Commit Transaction
End
Else
Begin
	Rollback Transaction
End
End
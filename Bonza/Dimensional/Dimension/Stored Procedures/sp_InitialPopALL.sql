﻿CREATE PROCEDURE [Dimension].[sp_InitialPopALL]
AS
BEGIN

-- USAGE: EXEC [Dimension].[sp_InitialPopALL]

	SET XACT_ABORT ON;
	BEGIN TRAN

	/* Fixed Dimensions */

	PRINT ('Dimension.Activity')
	exec Dimension.sp_InitialPopActivity

	PRINT ('Dimension.BalanceType')
	exec Dimension.sp_InitialPopBalanceType

	PRINT ('Dimension.BetStatus')
	exec Dimension.sp_InitialPopBetStatus

	PRINT ('Dimension.BetType')
	exec Dimension.sp_InitialPopBetType

	PRINT ('Dimension.Campaign')
	exec Dimension.sp_InitialPopCampaign

	PRINT ('Dimension.Channel')
	exec Dimension.sp_InitialPopChannel

	PRINT ('Dimension.Class')
	exec Dimension.sp_InitialPopClass

	PRINT ('Dimension.Company')
	exec Dimension.sp_InitialPopCompany

	PRINT ('Dimension.Instrument')
	exec Dimension.sp_InitialPopInstrument

	PRINT ('Dimension.LegType')
	exec Dimension.sp_InitialPopLegType

	PRINT ('Dimension.Market')
	exec Dimension.sp_InitialPopMarket

	PRINT ('Dimension.Time')
	exec Dimension.sp_InitialPopTime

	PRINT ('Dimension.TransactionDetail')
	exec Dimension.sp_InitialPopTransactionDetail

	PRINT ('Dimension.User')
	exec Dimension.sp_InitialPopUser

	PRINT ('Dimension.Wallet') 
	exec Dimension.sp_InitialPopWallet

	/* Dynamic Dimension Default Members */

	PRINT ('Dimension.Account')
	exec Dimension.sp_InitialPopAccount -- this is a SCD

	PRINT ('Dimension.AccountStatus')
	exec Dimension.sp_InitialPopAccountStatus

	PRINT ('Dimension.Event')
	exec Dimension.sp_InitialPopEvent

	PRINT ('Dimension.Day')
	exec Dimension.sp_InitialPopDay

	PRINT ('Dimension.DayZone')
	exec Dimension.sp_InitialPopDayZone

	PRINT ('Dimension.Promotion') 
	exec Dimension.sp_InitialPopPromotion -- this is a SCD

	PRINT ('Dimension.Contract') 
	exec Dimension.sp_InitialPopContract 

	PRINT ('Dimension.PriceType') 
	exec Dimension.sp_InitialPopPriceType 

	PRINT ('Dimension.ValueTiers') 
	exec Dimension.sp_InitialPopValueTiers

	PRINT ('Dimension.InPlay')
	exec Dimension.sp_InitialPopInPlay

	PRINT ('Dimension.LifeStage')
	exec Dimension.sp_InitialPopLifeStage

	PRINT ('Dimension.RevenueLifeCycle')
	exec Dimension.sp_InitialPopRevenueLifeCycle

	PRINT ('Dimension.Market') 
	exec Dimension.sp_InitialPopMarket

	PRINT ('Dimension.Note') 
	exec Dimension.sp_InitialPopNote

	PRINT ('Dimension.Communication') 
	exec Dimension.sp_InitialPopCommunication

	PRINT ('Dimension.InteractionType') 
	exec Dimension.sp_InitialPopInteractionType

	COMMIT;

END


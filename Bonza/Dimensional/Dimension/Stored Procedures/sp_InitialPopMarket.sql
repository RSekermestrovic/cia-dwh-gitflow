Create Procedure [Dimension].[sp_InitialPopMarket]

as

IF not exists (Select [MarketKey] from [Dimension].[Market])
Begin
Begin Transaction
SET IDENTITY_INSERT [Dimension].[Market] ON 

INSERT [Dimension].[Market]  
		([MarketKey],[Source],[MarketId],[Market],[MarketType],[MarketGroup], [MarketClosed], [MarketClosedDateTime], [MarketClosedVenueDateTime], [MarketOpen], 
		[MarketOpenedDatetime], [MarketOpenedVenueDateTime], [MarketSettled], [MarketSettledDateTime], [MarketSettledVenueDateTime],
		[MarketSettledDayKey], [MarketOpenedDayKey], [MarketClosedDayKey], [LiveMarket], [FutureMarket], [MaxWinnings], 
		[MaxStake], [LevyRate],[Sport],[FromDate],[FirstDate],[CreatedDate],[CreatedBatchKey],[CreatedBy],[ModifiedDate],[ModifiedBatchKey],[ModifiedBy]) 
SELECT -1, 'UNK', -1, 'Unknown', 'UNK', 'UNK', 'UNK', CAST(N'1900-01-01 00:00:00.000' AS DateTime), CAST(N'1900-01-01 00:00:00.000' AS DateTime), 'UNK', 
	   CAST(N'1900-01-01 00:00:00.000' AS DateTime), CAST(N'1900-01-01 00:00:00.000' AS DateTime), 'UNK', CAST(N'1900-01-01 00:00:00.000' AS DateTime), CAST(N'1900-01-01 00:00:00.000' AS DateTime), 
	   -1, -1, -1, 'UNK', 'UNK', -1, 
	   -1, 'UNK', 'UNK', GETDATE(), GETDATE(), GETDATE(), 0, 'sp_InitialPopMarket', GETDATE(), 0, 'sp_InitialPopMarket'
UNION ALL
SELECT 0, 'N/A', 0, 'N/A', 'N/A', 'N/A', 'N/A', CAST(N'1900-01-01 00:00:00.000' AS DateTime), CAST(N'1900-01-01 00:00:00.000' AS DateTime), 'N/A', 
	   CAST(N'1900-01-01 00:00:00.000' AS DateTime), CAST(N'1900-01-01 00:00:00.000' AS DateTime), 'N/A', CAST(N'1900-01-01 00:00:00.000' AS DateTime), CAST(N'1900-01-01 00:00:00.000' AS DateTime), 
	   0, 0, 0, 'N/A', 'N/A', 0, 
	   0, 'N/A', 'N/A', GETDATE(), GETDATE(), GETDATE(), 0, 'sp_InitialPopMarket', GETDATE(), 0, 'sp_InitialPopMarket'

SET IDENTITY_INSERT [Dimension].[Market] OFF
IF @@ERROR = 0
Begin
	Commit Transaction
End
Else
Begin
	Rollback Transaction
End
End
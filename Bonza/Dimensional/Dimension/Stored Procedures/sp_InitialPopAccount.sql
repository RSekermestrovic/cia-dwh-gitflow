Create Procedure [Dimension].sp_InitialPopAccount

as

IF not exists (Select	[AccountKey] from [Dimension].[Account] where AccountKey = -1)
Begin
Begin Transaction

	SET IDENTITY_INSERT [Dimension].[Account] ON 
	 
	INSERT [Dimension].[Account] 
	(	[AccountKey],
		-- Surrogate Keys --
		[CompanyKey], [LedgerOpenedDayKey], [AccountOpenedDayKey], [AccountOpenedChannelKey],
		-- Ledger Details --
		[LedgerID], [LedgerSource], [LegacyLedgerID], [LedgerSequenceNumber], [LedgerTypeID], [LedgerType], [LedgerGrouping], [LedgerOpenedDate], [LedgerOpenedDateOrigin],
		-- Account Details --
		[AccountID], [AccountSource], [PIN], [UserName], [ExactTargetID], [AccountFlags], [AccountTypeCode], [AccountType], [AccountOpenedDate],
		[BTag2], [AffiliateID], [AffiliateName], [SiteID], [TrafficSource], [RefURL], [CampaignID], [Keywords], [StatementMethod], [StatementFrequency],
		-- Customer Details --
		[CustomerID], [CustomerSource], [Title], [Surname], [FirstName], [MiddleName], [Gender], [DOB],
		[Address1], [Address2], [Suburb], [StateCode], [State], [PostCode], [CountryCode], [Country], [GeographicalLocation], [AustralianClient],
		[Phone], [Mobile], [Fax], [Email],
		-- Advanced
		[AccountTitle], [AccountSurname], [AccountFirstName], [AccountMiddleName], [AccountGender], [AccountDOB], 
		[HasHomeAddress], [HomeAddress1], [HomeAddress2], [HomeSuburb], [HomeStateCode], [HomeState], [HomePostCode], [HomeCountryCode], [HomeCountry],
		[HomePhone], [HomeMobile], [HomeFax], [HomeEmail], [HomeAlternateEmail],
		[HasPostalAddress], [PostalAddress1], [PostalAddress2], [PostalSuburb], [PostalStateCode], [PostalState], [PostalPostCode], [PostalCountryCode], [PostalCountry],
		[PostalPhone], [PostalMobile], [PostalFax], [PostalEmail], [PostalAlternateEmail],
		[HasBusinessAddress], [BusinessAddress1], [BusinessAddress2], [BusinessSuburb], [BusinessStateCode], [BusinessState], [BusinessPostCode], [BusinessCountryCode], [BusinessCountry],
		[BusinessPhone], [BusinessMobile], [BusinessFax], [BusinessEmail], [BusinessAlternateEmail],
		[HasOtherAddress], [OtherAddress1], [OtherAddress2], [OtherSuburb], [OtherStateCode], [OtherState], [OtherPostCode], [OtherCountryCode], [OtherCountry],
		[OtherPhone], [OtherMobile], [OtherFax], [OtherEmail], [OtherAlternateEmail],
		[SourceBTag2],[SourceTrafficSource], [SourceRefURL], [SourceCampaignID], [SourceKeywords],
		-- Metadata --
		[FirstDate], [FromDate], [ToDate], [CreatedDate], [CreatedBatchKey], [CreatedBy], [ModifiedDate], [ModifiedBatchKey], [ModifiedBy]
	)
	VALUES
	(	-1,
		-- Surrogate Keys --
		-1, -1, -1, -1,
		-- Ledger Details --
		-1, 'UNK', -1, -1, -1, 'Unknown', 'Unknown', NULL, 'Unknown',
		-- Account Details --
		-1, 'UNK', -1, 'Unknown', 'Unknown', -1, 'U', 'Unknown', NULL,
		'Unknown', 'Unknown', 'Unknown', 'Unknown', 'Unknown', 'Unknown', 'Unknown', 'Unknown', 'Unknown', 'Unknown',
		-- Customer Details --
		-1, 'UNK', '', '', '', '', 'U', NULL,
		'', '', 'Unknown', 'UNK', 'Unknown', 'Unknown', 'UNK', 'Unknown', NULL, 'Unk',
		'Unknown', 'Unknown', 'Unknown', 'Unknown',
		-- Advanced
		'', '', '', '', 'U', NULL, 
		0, '', '', 'Unknown', 'UNK', 'Unknown', 'Unknown', 'UNK', 'Unknown',
		'Unknown', 'Unknown', 'Unknown', 'Unknown', 'Unknown',
		0, '', '', 'Unknown', 'UNK', 'Unknown', 'Unknown', 'UNK', 'Unknown',
		'Unknown', 'Unknown', 'Unknown', 'Unknown', 'Unknown',
		0, '', '', 'Unknown', 'UNK', 'Unknown', 'Unknown', 'UNK', 'Unknown',
		'Unknown', 'Unknown', 'Unknown', 'Unknown', 'Unknown',
		0, '', '', 'Unknown', 'UNK', 'Unknown', 'Unknown', 'UNK', 'Unknown',
		'Unknown', 'Unknown', 'Unknown', 'Unknown', 'Unknown',
		'Unknown', 'Unknown', 'Unknown', 'Unknown', 'Unknown',
		-- Metadata --
		'1900-01-01', '1900-01-01', '9999-12-31', GETDATE(), 0, 'sp_InitialPopAccount', GETDATE(), 0, 'sp_InitialPopAccount'
	),
	(	0,
		-- Surrogate Keys --
		0, 0, 0, 0,
		-- Ledger Details --
		0, 'N/A', 0, 0, 0, 'N/A', 'N/A', NULL, 'N/A',
		-- Account Details --
		0, 'N/A', 0, 'N/A', 'N/A', 0, 'X', 'N/A', NULL,
		'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A',
		-- Customer Details --
		0, 'N/A', '', '', '', '', 'U', NULL,
		'', '', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'N/A',
		'N/A', 'N/A', 'N/A', 'N/A',
		-- Advanced
		'', '', '', '', 'U', NULL, 
		0, '', '', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A',
		'N/A', 'N/A', 'N/A', 'N/A', 'N/A',
		0, '', '', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A',
		'N/A', 'N/A', 'N/A', 'N/A', 'N/A',
		0, '', '', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A',
		'N/A', 'N/A', 'N/A', 'N/A', 'N/A',
		0, '', '', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A',
		'N/A', 'N/A', 'N/A', 'N/A', 'N/A',
		'N/A', 'N/A', 'N/A', 'N/A', 'N/A',
		-- Metadata --
		'1900-01-01', '1900-01-01', '9999-12-31', GETDATE(), 0, 'sp_InitialPopAccount', GETDATE(), 0, 'sp_InitialPopAccount'
	)
	
	SET IDENTITY_INSERT [Dimension].[Account] OFF 
IF @@ERROR = 0
Begin
	Commit Transaction
End
Else
Begin
	Rollback Transaction
End
End

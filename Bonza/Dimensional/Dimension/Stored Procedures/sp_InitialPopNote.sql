Create Procedure [Dimension].sp_InitialPopNote

as

IF not exists (Select [NoteKey] from [Dimension].[Note])
Begin
Begin Transaction

SET IDENTITY_INSERT [Dimension].[Note] ON 

INSERT [Dimension].[Note]  ([NoteKey],[NoteId],[NoteSource],[Notes],[FromDate],[ToDate],[FirstDate],[CreatedDate],[CreatedBatchKey],[CreatedBy],[ModifiedDate],[ModifiedBatchKey],[ModifiedBy]) 
SELECT -1, -1, 'UNK', 'UNK', GETDATE(), '9999-12-31 00:00:00.000', GETDATE(), GETDATE(), 0, 'sp_InitialPopNote', GETDATE(), 0, 'sp_InitialPopNote'
UNION ALL
SELECT  0,  0, 'N/A', 'N/A', GETDATE(), '9999-12-31 00:00:00.000', GETDATE(), GETDATE(), 0, 'sp_InitialPopNote', GETDATE(), 0, 'sp_InitialPopNote'

SET IDENTITY_INSERT [Dimension].[Note] OFF

IF @@ERROR = 0
Begin
	Commit Transaction
End
Else
Begin
	Rollback Transaction
End
End

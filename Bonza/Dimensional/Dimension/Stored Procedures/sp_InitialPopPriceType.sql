CREATE PROCEDURE [Dimension].[sp_InitialPopPriceType]
AS
BEGIN

	IF NOT EXISTS (SELECT [PriceTypeKey] FROM [Dimension].[PriceType])
	BEGIN
		
		SET XACT_ABORT ON;
		
		BEGIN TRANSACTION

		SET IDENTITY_INSERT [Dimension].[PriceType] ON 

		INSERT INTO [Dimension].[PriceType]
		(
			[PriceTypeKey]
		   ,[PriceTypeID]
		   ,[Source]
		   ,[PriceTypeName]
		   ,[FromDate]
		   ,[ToDate]
		   ,[FirstDate]
		   ,[CreatedDate]
		   ,[CreatedBatchKey]
		   ,[CreatedBy]
		   ,[ModifiedDate]
		   ,[ModifiedBatchKey]
		   ,[ModifiedBy]
		)
		SELECT -1, -1, 'UNK', 'Unknown',  GETDATE(), '31-Dec-9999', GETDATE(), GETDATE(), 0, 'sp_InitialPopPriceType', GETDATE(), 0, 'sp_InitialPopPriceType'
		UNION ALL 
		SELECT  0,  0, 'N/A',     'N/A',  GETDATE(), '31-Dec-9999', GETDATE(), GETDATE(), 0, 'sp_InitialPopPriceType', GETDATE(), 0, 'sp_InitialPopPriceType';

		SET IDENTITY_INSERT [Dimension].[PriceType] OFF;

		COMMIT;

	END

END
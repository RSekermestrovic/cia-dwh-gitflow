﻿Create Procedure [Dimension].sp_InitialPopLifeStage

as

IF not exists (Select	[LifeStageKey] from [Dimension].[Lifestage])
Begin
Begin Transaction

INSERT [Dimension].[Lifestage] ([LifeStageKey], [LifeStage], [CreatedDate], [CreatedBatchKey], [CreatedBy], [ModifiedDate], [ModifiedBatchKey], [ModifiedBy]) VALUES (-1, N'Unknown', CAST(N'2015-11-27 11:26:09.4970000' AS DateTime2), 0, N'sp_InitialPopLifestage', CAST(N'2015-11-27 11:26:09.4970000' AS DateTime2), 0, N'sp_InitialPopLifestage')
INSERT [Dimension].[Lifestage] ([LifeStageKey], [LifeStage], [CreatedDate], [CreatedBatchKey], [CreatedBy], [ModifiedDate], [ModifiedBatchKey], [ModifiedBy]) VALUES (0, N'N/A', CAST(N'2015-11-27 11:26:09.4970000' AS DateTime2), 0, N'sp_InitialPopLifestage', CAST(N'2015-11-27 11:26:09.4970000' AS DateTime2), 0, N'sp_InitialPopLifestage')
INSERT [Dimension].[Lifestage] ([LifeStageKey], [LifeStage], [CreatedDate], [CreatedBatchKey], [CreatedBy], [ModifiedDate], [ModifiedBatchKey], [ModifiedBy]) VALUES (1, N'Acquisition', CAST(N'2015-11-27 11:26:09.4970000' AS DateTime2), 0, N'sp_InitialPopLifestage', CAST(N'2015-11-27 11:26:09.4970000' AS DateTime2), 0, N'sp_InitialPopLifestage')
INSERT [Dimension].[Lifestage] ([LifeStageKey], [LifeStage], [CreatedDate], [CreatedBatchKey], [CreatedBy], [ModifiedDate], [ModifiedBatchKey], [ModifiedBy]) VALUES (2, N'Convert', CAST(N'2015-11-27 11:26:09.4970000' AS DateTime2), 0, N'sp_InitialPopLifestage', CAST(N'2015-11-27 11:26:09.4970000' AS DateTime2), 0, N'sp_InitialPopLifestage')
INSERT [Dimension].[Lifestage] ([LifeStageKey], [LifeStage], [CreatedDate], [CreatedBatchKey], [CreatedBy], [ModifiedDate], [ModifiedBatchKey], [ModifiedBy]) VALUES (3, N'On Boarding', CAST(N'2015-11-27 11:26:09.4970000' AS DateTime2), 0, N'sp_InitialPopLifestage', CAST(N'2015-11-27 11:26:09.4970000' AS DateTime2), 0, N'sp_InitialPopLifestage')
INSERT [Dimension].[Lifestage] ([LifeStageKey], [LifeStage], [CreatedDate], [CreatedBatchKey], [CreatedBy], [ModifiedDate], [ModifiedBatchKey], [ModifiedBy]) VALUES (4, N'Retain', CAST(N'2015-11-27 11:26:09.4970000' AS DateTime2), 0, N'sp_InitialPopLifestage', CAST(N'2015-11-27 11:26:09.4970000' AS DateTime2), 0, N'sp_InitialPopLifestage')
INSERT [Dimension].[Lifestage] ([LifeStageKey], [LifeStage], [CreatedDate], [CreatedBatchKey], [CreatedBy], [ModifiedDate], [ModifiedBatchKey], [ModifiedBy]) VALUES (5, N'Win back', CAST(N'2015-11-27 11:26:09.4970000' AS DateTime2), 0, N'sp_InitialPopLifestage', CAST(N'2015-11-27 11:26:09.4970000' AS DateTime2), 0, N'sp_InitialPopLifestage')
INSERT [Dimension].[Lifestage] ([LifeStageKey], [LifeStage], [CreatedDate], [CreatedBatchKey], [CreatedBy], [ModifiedDate], [ModifiedBatchKey], [ModifiedBy]) VALUES (6, N'Re-Activate', CAST(N'2015-11-27 11:26:09.4970000' AS DateTime2), 0, N'sp_InitialPopLifestage', CAST(N'2015-11-27 11:26:09.4970000' AS DateTime2), 0, N'sp_InitialPopLifestage')
INSERT [Dimension].[Lifestage] ([LifeStageKey], [LifeStage], [CreatedDate], [CreatedBatchKey], [CreatedBy], [ModifiedDate], [ModifiedBatchKey], [ModifiedBy]) VALUES (7, N'Out Of Play', CAST(N'2015-11-27 11:26:09.4970000' AS DateTime2), 0, N'sp_InitialPopLifestage', CAST(N'2015-11-27 11:26:09.4970000' AS DateTime2), 0, N'sp_InitialPopLifestage')


IF @@ERROR = 0
Begin
	Commit Transaction
End
Else
Begin
	Rollback Transaction
End
End
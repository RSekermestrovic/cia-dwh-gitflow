﻿
CREATE PROCEDURE [Dimension].[sp_InitialPopPromotion]
AS
BEGIN

	IF NOT EXISTS (Select [PromotionKey] from [Dimension].[Promotion])
	BEGIN
		SET XACT_ABORT ON;
		
		BEGIN TRANSACTION

		SET IDENTITY_INSERT [Dimension].[Promotion] ON 

		INSERT [Dimension].[Promotion] 
		(      [PromotionKey]
			  ,[PromotionId]
			  ,[Source]
			  ,[PromotionType]
			  ,[PromotionName]
			  ,[OfferDate]
			  ,[ExpiryDate]
			  ,[FromDate]
			  ,[ToDate]
			  ,[FirstDate]
			  ,[CreatedDate]
			  ,[CreatedBatchKey]
			  ,[CreatedBy]
			  ,[ModifiedDate]
			  ,[ModifiedBatchKey]
			  ,[ModifiedBy]
		) 
				  SELECT -3, -1,     N'IBB', N'Free Winnings', N'Promotion', CAST(N'1900-01-01' AS Date), CAST(N'1900-01-01' AS Date), GETDATE(), '31-Dec-9999', GETDATE(), GETDATE(), 0, 'sp_InitialPopPromotion', GETDATE(), 0, 'sp_InitialPopPromotion'
		UNION ALL SELECT -1, -1,     N'N/A', N'Unknown',	   N'Unknown', CAST(N'1900-01-01' AS Date), CAST(N'1900-01-01' AS Date), GETDATE(), '31-Dec-9999', GETDATE(), GETDATE(), 0, 'sp_InitialPopPromotion', GETDATE(), 0, 'sp_InitialPopPromotion'
		UNION ALL SELECT  0,  0,     N'N/A', N'Cash',		   N'N/A', CAST(N'1900-01-01' AS Date), CAST(N'1900-01-01' AS Date), GETDATE(), '31-Dec-9999', GETDATE(), GETDATE(), 0, 'sp_InitialPopPromotion', GETDATE(), 0, 'sp_InitialPopPromotion'
		UNION ALL SELECT -4, 999999, N'IBB', N'Free Bet',	   N'Unknown Voucher Expiry', CAST(N'1900-01-01' AS Date), CAST(N'1900-01-01' AS Date), GETDATE(), '31-Dec-9999', GETDATE(), GETDATE(), 0, 'sp_InitialPopPromotion', GETDATE(), 0, 'sp_InitialPopPromotion'
		UNION ALL SELECT -5, 999999, N'IBF', N'Free Bet',	   N'Unknown Voucher Expiry', CAST(N'1900-01-01' AS Date), CAST(N'1900-01-01' AS Date), GETDATE(), '31-Dec-9999', GETDATE(), GETDATE(), 0, 'sp_InitialPopPromotion', GETDATE(), 0, 'sp_InitialPopPromotion'
		UNION ALL SELECT -6, 999999, N'IBP', N'Free Bet',	  N'Unknown Voucher Expiry', CAST(N'1900-01-01' AS Date), CAST(N'1900-01-01' AS Date),  GETDATE(), '31-Dec-9999', GETDATE(), GETDATE(), 0, 'sp_InitialPopPromotion', GETDATE(), 0, 'sp_InitialPopPromotion'

		SET IDENTITY_INSERT [Dimension].[Promotion] OFF

		COMMIT;

	END

END
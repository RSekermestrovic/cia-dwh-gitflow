﻿Create Procedure [Dimension].sp_InitialPopValueTiers

as

IF not exists (Select	[TierKey] from [Dimension].[ValueTiers])
Begin
Begin Transaction


INSERT [Dimension].[ValueTiers] ([TierKey], [TierNumber], [TierName], [FromDate], [ToDate]) VALUES (-1, -1, N'Unk', CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), CAST(N'9999-12-31 00:00:00.0000000' AS DateTime2))
INSERT [Dimension].[ValueTiers] ([TierKey], [TierNumber], [TierName], [FromDate], [ToDate]) VALUES (0, 0, N'0', CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), CAST(N'9999-12-31 00:00:00.0000000' AS DateTime2))
INSERT [Dimension].[ValueTiers] ([TierKey], [TierNumber], [TierName], [FromDate], [ToDate]) VALUES (1, 1, N'>= 1000000', CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), CAST(N'9999-12-31 00:00:00.0000000' AS DateTime2))
INSERT [Dimension].[ValueTiers] ([TierKey], [TierNumber], [TierName], [FromDate], [ToDate]) VALUES (2, 2, N'500000 <= X < 1000000', CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), CAST(N'9999-12-31 00:00:00.0000000' AS DateTime2))
INSERT [Dimension].[ValueTiers] ([TierKey], [TierNumber], [TierName], [FromDate], [ToDate]) VALUES (3, 3, N'200000 <= X < 500000', CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), CAST(N'9999-12-31 00:00:00.0000000' AS DateTime2))
INSERT [Dimension].[ValueTiers] ([TierKey], [TierNumber], [TierName], [FromDate], [ToDate]) VALUES (4, 4, N'50000 <= X < 200000', CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), CAST(N'9999-12-31 00:00:00.0000000' AS DateTime2))
INSERT [Dimension].[ValueTiers] ([TierKey], [TierNumber], [TierName], [FromDate], [ToDate]) VALUES (5, 5, N'5000 <= X < 50000', CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), CAST(N'9999-12-31 00:00:00.0000000' AS DateTime2))
INSERT [Dimension].[ValueTiers] ([TierKey], [TierNumber], [TierName], [FromDate], [ToDate]) VALUES (6, 6, N'1000 <= X < 5000', CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), CAST(N'9999-12-31 00:00:00.0000000' AS DateTime2))
INSERT [Dimension].[ValueTiers] ([TierKey], [TierNumber], [TierName], [FromDate], [ToDate]) VALUES (7, 7, N'500 <= X < 1000', CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), CAST(N'9999-12-31 00:00:00.0000000' AS DateTime2))
INSERT [Dimension].[ValueTiers] ([TierKey], [TierNumber], [TierName], [FromDate], [ToDate]) VALUES (8, 8, N'<500', CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), CAST(N'9999-12-31 00:00:00.0000000' AS DateTime2))


IF @@ERROR = 0
Begin
	Commit Transaction
End
Else
Begin
	Rollback Transaction
End
End
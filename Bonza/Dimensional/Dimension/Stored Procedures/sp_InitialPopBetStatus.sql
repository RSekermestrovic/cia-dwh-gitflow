Create Procedure [Dimension].sp_InitialPopBetStatus

as

IF not exists (Select [StatusKey] from [Dimension].[BetStatus])
Begin
Begin Transaction

SET IDENTITY_INSERT [Dimension].[BetStatus] ON 

INSERT INTO [Dimension].[BetStatus] ([StatusKey]
      ,[StatusCode]
      ,[StatusName]
      ,[OutcomeCode]
      ,[OutcomeName]
      ,[FromDate]
      ,[ToDate]
      ,[FirstDate]
      ,[CreatedDate]
      ,[CreatedBatchKey]
      ,[CreatedBy]
      ,[ModifiedDate]
      ,[ModifiedBatchKey]
      ,[ModifiedBy]) 
          SELECT -1, 'U', 'Unknown', 'U', 'Unknown', CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), CAST(N'9999-12-31 00:00:00.0000000' AS DateTime2), CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), 0, 'ETL', CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), 0, 'ETL'
UNION ALL SELECT  0, 'N', 'N/A',	 'N', 'N/A',	 CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), CAST(N'9999-12-31 00:00:00.0000000' AS DateTime2), CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), 0, 'ETL', CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), 0, 'ETL'
UNION ALL SELECT  1, 'A', 'Accept',  'P', 'Pending', CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), CAST(N'9999-12-31 00:00:00.0000000' AS DateTime2), CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), 0, 'ETL', CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), 0, 'ETL'
UNION ALL SELECT  2, 'C', 'Cancel',  'C', 'Cancel',  CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), CAST(N'9999-12-31 00:00:00.0000000' AS DateTime2), CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), 0, 'ETL', CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), 0, 'ETL'
UNION ALL SELECT  3, 'D', 'Decline', 'C', 'Cancel',  CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), CAST(N'9999-12-31 00:00:00.0000000' AS DateTime2), CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), 0, 'ETL', CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), 0, 'ETL'
UNION ALL SELECT  4, 'L', 'Loss',	 'L', 'Loss',	 CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), CAST(N'9999-12-31 00:00:00.0000000' AS DateTime2), CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), 0, 'ETL', CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), 0, 'ETL'
UNION ALL SELECT  5, 'R', 'Request', 'P', 'Pending', CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), CAST(N'9999-12-31 00:00:00.0000000' AS DateTime2), CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), 0, 'ETL', CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), 0, 'ETL'
UNION ALL SELECT  6, 'S', 'Scratch', 'C', 'Cancel',  CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), CAST(N'9999-12-31 00:00:00.0000000' AS DateTime2), CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), 0, 'ETL', CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), 0, 'ETL'
UNION ALL SELECT  7, 'W', 'Win',	 'W', 'Win',	 CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), CAST(N'9999-12-31 00:00:00.0000000' AS DateTime2), CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), 0, 'ETL', CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), 0, 'ETL'

SET IDENTITY_INSERT [Dimension].[BetStatus] OFF
IF @@ERROR = 0
Begin
	Commit Transaction
End
Else
Begin
	Rollback Transaction
End
End  
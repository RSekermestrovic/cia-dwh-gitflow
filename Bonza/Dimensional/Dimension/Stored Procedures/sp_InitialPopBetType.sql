Create Procedure [Dimension].sp_InitialPopBetType

as

IF not exists (Select	[BetTypeKey] from [Dimension].[BetType])
Begin
Begin Transaction

SET IDENTITY_INSERT [Dimension].[BetType] ON 

INSERT [Dimension].[BetType] ([BetTypeKey], [BetGroupType], [BetType], [BetSubType], [GroupingType], [LegType], [FromDate], [ToDate], [FirstDate], [CreatedDate], [CreatedBatchKey], [CreatedBy], [ModifiedDate], [ModifiedBatchKey], [ModifiedBy]) VALUES (-1, N'Unknown', N'Unknown', N'Unknown', N'Unknown', N'Unknown', CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), CAST(N'9999-12-31 00:00:00.0000000' AS DateTime2), CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), 0, N'ETL', CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), 0, N'ETL')
INSERT [Dimension].[BetType] ([BetTypeKey], [BetGroupType], [BetType], [BetSubType], [GroupingType], [LegType], [FromDate], [ToDate], [FirstDate], [CreatedDate], [CreatedBatchKey], [CreatedBy], [ModifiedDate], [ModifiedBatchKey], [ModifiedBy]) VALUES (0, N'N/A', N'N/A', N'N/A', N'N/A', N'N/A', CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), CAST(N'9999-12-31 00:00:00.0000000' AS DateTime2), CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), 0, N'ETL', CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2), 0, N'ETL')

SET IDENTITY_INSERT [Dimension].[BetType] OFF
IF @@ERROR = 0
Begin
	Commit Transaction
End
Else
Begin
	Rollback Transaction
End
End
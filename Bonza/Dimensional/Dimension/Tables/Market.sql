﻿CREATE TABLE [Dimension].[Market] (
    [MarketKey]        INT           IDENTITY (1, 1) NOT NULL,
    [Source]           CHAR (3)      NOT NULL,
    [MarketId]         INT           NOT NULL,
    [Market]	       VARCHAR (255) NOT NULL,
	[MarketType]       VARCHAR (255) NULL,
    [MarketGroup]      VARCHAR (255) NULL,
	[LevyRate]         VARCHAR (30)  NULL,
    [Sport]            VARCHAR (50)  NULL,
    [MarketOpen]					  VARCHAR(10)	     NULL,
    [MarketOpenedDateTime]            DATETIME           NULL,
    [MarketOpenedDayKey]              INT                NULL,
    [MarketOpenedVenueDateTime]       DATETIME           NULL,
	[MarketClosed]					  VARCHAR(10)	     NULL,
    [MarketClosedDateTime]            DATETIME           NULL,
    [MarketClosedDayKey]              INT                NULL,
    [MarketClosedVenueDateTime]       DATETIME           NULL,
	[MarketSettled]					  VARCHAR(10)		 NULL,
    [MarketSettledDateTime]           DATETIME           NULL,
    [MarketSettledDayKey]             INT                NULL,
    [MarketSettledVenueDateTime]      DATETIME           NULL,
    [LiveMarket]	   VARCHAR (3)   NULL,
	[FutureMarket]     VARCHAR (3)  NULL,
	[MaxWinnings]      INT		    NULL,
	[MaxStake]         INT		    NULL,
    [FromDate]         DATETIME2 (3) NOT NULL,
    [FirstDate]        DATETIME2 (3) NOT NULL,
    [CreatedDate]      DATETIME2 (7) NOT NULL,
    [CreatedBatchKey]  INT           NOT NULL,
    [CreatedBy]        VARCHAR (100) NOT NULL,
    [ModifiedDate]     DATETIME2 (7) NOT NULL,
    [ModifiedBatchKey] INT           NOT NULL,
    [ModifiedBy]       VARCHAR (100) NOT NULL,
    CONSTRAINT [CI-PK-MarketKey] PRIMARY KEY CLUSTERED ([MarketKey] ASC) WITH (DATA_COMPRESSION = ROW)
);
GO
CREATE UNIQUE NONCLUSTERED INDEX [NI-NK-Market]
    ON [Dimension].[Market]([MarketId] ASC, [Source] ASC)
    INCLUDE([MarketKey]) WITH (DATA_COMPRESSION = ROW);
GO
CREATE NONCLUSTERED INDEX [NI_Market_ModifiedBatchKey]
    ON [Dimension].[Market]([ModifiedBatchKey] ASC) 
	INCLUDE (MarketId) WITH (DATA_COMPRESSION = ROW);
GO

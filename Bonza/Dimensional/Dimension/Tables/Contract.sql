﻿CREATE TABLE [Dimension].[Contract] (
	[ContractKey] [int] IDENTITY(1,1) NOT NULL,
	[BetGroupID] [bigint] NULL,
	[BetGroupIDSource] [char](3) NULL,
	[BetId] [bigint] NULL,
	[BetIdSource] [char](3) NULL,
	[LegId] [bigint] NULL,
	[LegIdSource] [char](3) NULL,
	[TransactionID] [bigint] NULL,
	[TransactionIDSource] [char](3) NULL,
	[FreeBetID] [bigint] NULL,
	[ExternalID] [varchar](200) NULL,
	[External1] [varchar](200) NULL,
	[External2] [varchar](200) NULL,
	[RequestID] [int] NULL,
	[WithdrawID] [int] NULL,
	[FromDate] [datetime2](0) NOT NULL,
	[ToDate] [datetime2](0) NOT NULL,
	[FirstDate] [datetime2](0) NOT NULL,
	[CreatedDate] [datetime2](7) NULL,
	[CreatedBatchKey] [int] NOT NULL,
	[CreatedBy] [varchar](32) NOT NULL,
	[ModifiedDate] [datetime2](7) NOT NULL,
	[ModifiedBatchKey] [int] NOT NULL,
	[ModifiedBy] [varchar](32) NOT NULL,
    CONSTRAINT [CI-PK-ContractKey] PRIMARY KEY CLUSTERED ([ContractKey] ASC) WITH (DATA_COMPRESSION = ROW)
);


GO
CREATE NONCLUSTERED INDEX [NCI-BetID]
    ON [Dimension].[Contract]([BetId] ASC, [BetIdSource] ASC)
    INCLUDE([ContractKey]) WITH (DATA_COMPRESSION = ROW);


GO
CREATE UNIQUE NONCLUSTERED INDEX [NCI-NaturalKeys]
    ON [Dimension].[Contract]([LegId] ASC, [LegIdSource] ASC)
    INCLUDE([ContractKey]) WITH (DATA_COMPRESSION = ROW);


GO
CREATE NONCLUSTERED INDEX [NCI-ExternalID]
    ON [Dimension].[Contract]([ExternalID] ASC)
    INCLUDE([ContractKey]) WITH (DATA_COMPRESSION = ROW);


﻿CREATE TABLE [Dimension].[Promotion] (
    [PromotionKey]     INT       IDENTITY (1, 1) NOT NULL,
    [PromotionId]      INT            NOT NULL,
    [Source]           CHAR (3)       NOT NULL,
    [PromotionType]    VARCHAR (32)   NOT NULL,
    [PromotionName]    VARCHAR (1000) NOT NULL,
    [OfferDate]        DATE           NOT NULL,
    [ExpiryDate]       DATE           NOT NULL,
    [FromDate]         DATETIME2 (0)  NOT NULL,
    [ToDate]           DATETIME2 (0)  NOT NULL,
    [FirstDate]        DATETIME2 (0)  NOT NULL,
    [CreatedDate]      DATETIME2 (7)  NOT NULL,
    [CreatedBatchKey]  INT            NOT NULL,
    [CreatedBy]        VARCHAR (32)   NOT NULL,
    [ModifiedDate]     DATETIME2 (7)  NOT NULL,
    [ModifiedBatchKey] INT            NOT NULL,
    [ModifiedBy]       VARCHAR (32)   NOT NULL,
    CONSTRAINT [CI-PK-PromotionKey] PRIMARY KEY CLUSTERED ([PromotionKey] ASC)
);




﻿CREATE TABLE [Dimension].[AccountStatus](
	[AccountStatusKey] [int] NOT NULL,
	[AccountKey] [int] NOT NULL,
	[LedgerID] [int] NOT NULL,
	[LedgerSource] [char](3) NOT NULL,
	[LedgerEnabled] [varchar](10) NULL,
	[LedgerStatus] [char](1) NULL,
	[AccountType] [varchar](30) NULL,
	[InternetProfile] [varchar](30) NULL,
	[VIP] [varchar](10) NULL,
	[CompanyKey] [int] NOT NULL,
	[CreditLimit] [money] NULL,
	[MinBetAmount] [money] NULL,
	[MaxBetAmount] [money] NULL,
	[Introducer] [varchar](50) NULL,
	[Handled] [varchar](10) NULL,
	[PhoneColourDesc] [varchar](30) NULL,
	[DisableDeposit] [varchar](10) NULL,
	[DisableWithdrawal] [varchar](10) NULL,
	[AccountClosedByUserID] [VARCHAR](32) NOT NULL,
	[AccountClosedByUserSource] [char](3) NOT NULL,
	[AccountClosedByUserName] [varchar](30) NOT NULL,
	[AccountClosedDate] [smalldatetime] NULL,
	[ReIntroducedBy] [int] NULL,
	[ReIntroducedDate] [smalldatetime] NULL,
	[AccountClosureReason] [varchar](1000) NULL,
	[DepositLimit] [money] NULL,
	[LossLimit] [money] NULL,
	[LimitPeriod] [int] NULL,
	[LimitTimeStamp] [datetime] NULL,
	[ReceiveMarketingEmail] [varchar](10) NULL,
	[ReceiveCompetitionEmail] [varchar](10) NULL,
	[ReceiveSMS] [varchar](10) NULL,
	[ReceivePostalMail] [varchar](10) NULL,
	[ReceiveFax] [varchar](10) NULL,
	[CurrentVersion] [varchar](3) NOT NULL,
	[FromDate] [datetime2](3) NOT NULL,
	[ToDate] [datetime2](3) NULL,
	[FirstDate] [datetime2](3) NULL,
	[CreatedDate] [datetime2](7) NULL,
	[CreatedBatchKey] [int] NULL,
	[CreatedBy] [varchar](32) NULL,
	[ModifiedDate] [datetime2](7) NULL,
	[ModifiedBatchKey] [int] NULL,
	[ModifiedBy] [varchar](32) NULL,
	CONSTRAINT [CI_AccountStatus_PK] PRIMARY KEY CLUSTERED ([AccountStatusKey] ASC) WITH (DATA_COMPRESSION = ROW)
)
GO

CREATE UNIQUE NONCLUSTERED INDEX [NI_AccountStatus_NK]
    ON [Dimension].[AccountStatus]([LedgerId] ASC, [LedgerSource] ASC, [FromDate] ASC, [ToDate] ASC)
    INCLUDE([AccountStatusKey]);
GO


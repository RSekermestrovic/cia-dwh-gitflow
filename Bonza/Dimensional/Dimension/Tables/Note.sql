﻿CREATE TABLE [Dimension].[Note](
	[NoteKey] [int] IDENTITY(1,1) NOT NULL,
	[NoteId] [bigint] NOT NULL,
	[NoteSource] [Char](3) NOT NULL,
	[Notes] [varchar](4000) NOT NULL,
	[FromDate]          DATETIME2 (3) NOT NULL,
    [ToDate]            DATETIME2 (3) NOT NULL,
    [FirstDate]         DATETIME2 (3) NOT NULL,
    [CreatedDate]       DATETIME2 (7) NOT NULL,
    [CreatedBatchKey]   INT           NOT NULL,
    [CreatedBy]         VARCHAR (32)  NOT NULL,
    [ModifiedDate]      DATETIME2 (7) NOT NULL,
    [ModifiedBatchKey]  INT           NOT NULL,
    [ModifiedBy]        VARCHAR (32)  NOT NULL,
    CONSTRAINT [CI-PK-NoteKey] PRIMARY KEY CLUSTERED ([NoteId] ASC) WITH (DATA_COMPRESSION = ROW)
)
GO


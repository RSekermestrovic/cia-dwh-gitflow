﻿CREATE TABLE [Dimension].[PriceType] (
    [PriceTypeKey]             INT           IDENTITY (1, 1) NOT NULL,
    [PriceTypeID]			   INT			 NOT NULL,
	[Source]				   CHAR(3)		 NOT NULL,
	[PriceTypeName]			   VARCHAR(255)  NOT NULL,
    [FromDate]                 DATETIME2 (3) NOT NULL,
    [ToDate]                   DATETIME2 (3) NOT NULL,
    [FirstDate]                DATETIME2 (3) NOT NULL,
    [CreatedDate]              DATETIME2 (3) DEFAULT (getdate()) NOT NULL,
    [CreatedBatchKey]          INT           NOT NULL,
    [CreatedBy]                VARCHAR (32)  DEFAULT (suser_sname()) NOT NULL,
    [ModifiedDate]             DATETIME2 (3) NOT NULL,
    [ModifiedBatchKey]         INT           NOT NULL,
    [ModifiedBy]               VARCHAR (32)  NOT NULL,
    CONSTRAINT [CI-PK-PriceTypeKey] PRIMARY KEY NONCLUSTERED ([PriceTypeKey] ASC)
);


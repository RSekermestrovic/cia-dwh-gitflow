﻿CREATE TABLE [Dimension].[BetLeg] (
    [ContractKey]     INT        NOT NULL,
    [LegNumber]       TINYINT    NOT NULL,
    [NumberOfLegs]    TINYINT    NOT NULL,
    [WeightingFactor] FLOAT (53) NOT NULL,
    [BetTypeKey]      INT        NOT NULL,
    [ClassKey]        INT        NOT NULL,
	[FromDate] [datetime2](0) NOT NULL,
	[ToDate] [datetime2](0) NOT NULL,
	[FirstDate] [datetime2](0) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBatchKey] [int] NOT NULL,
	[CreatedBy] [varchar](32) NOT NULL,
	[ModifiedDate] [datetime2](7) NOT NULL,
	[ModifiedBatchKey] [int] NOT NULL,
	[ModifiedBy] [varchar](32) NOT NULL,
    CONSTRAINT [BetLeg_PK] PRIMARY KEY NONCLUSTERED ([ContractKey] ASC, [LegNumber] ASC)
)
WITH (DATA_COMPRESSION = ROW);


﻿CREATE TABLE [Dimension].[Communication](
	[CommunicationKey] [int] IDENTITY(1,1) NOT NULL,
	[CommunicationId] [varchar](200) NULL,
	[CommunicationSource] [varchar](5) NULL,
	[Title] [varchar](200) NULL,
	[Details] [varchar](max) NULL,
	[SubDetails] [varchar](max) NULL,
	[CreatedDate] [datetime2](7) NULL,
	[ModifiedDate] [datetime2](7) NULL,
	[CreatedBatchKey] [int] NULL,
	[CreatedBy] [varchar](32) NULL,
	[ModifiedBatchKey] [int] NULL,
	[ModifiedBy] [varchar](32) NULL,
 CONSTRAINT [PK_Communication] PRIMARY KEY CLUSTERED 
(
	[CommunicationKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]





CREATE TABLE [Dimension].[Account](
	[AccountKey] [int] IDENTITY (1,1) NOT NULL,

-- Surrogate Keys --
	[CompanyKey] [int] NOT NULL,
	[LedgerOpenedDayKey] [int] NOT NULL,
	[AccountOpenedDayKey] [int] NOT NULL,
	[AccountOpenedChannelKey] [int] NOT NULL,

-- Ledger Details --
	[LedgerID] [int] NOT NULL,
	[LedgerSource] [char](3) NOT NULL,
	[LegacyLedgerID] [int] NULL,
	[LedgerSequenceNumber] [int] NULL,
	[LedgerTypeID] [int] NOT NULL,
	[LedgerType] [varchar](50) NULL,
	[LedgerGrouping] [varchar](30) NULL,
	[LedgerOpenedDate] [datetime] NULL,
	[LedgerOpenedDateOrigin] [varchar](20) NULL,

-- Account Details --
	[AccountID] [int] NOT NULL,
	[AccountSource] [char](3) NOT NULL,
	[PIN] [int] NULL,
	[UserName] [varchar](64) NULL,
	[ExactTargetID] [varchar](40) NULL,
	[AccountFlags] [int] NULL,
	[AccountTypeCode] [char] (1) NULL,
	[AccountType] [varchar] (10) NULL,

	[AccountOpenedDate] [datetime] NULL,
	[BDM] [varchar](50) NULL,
	[VIPCode] [int] NULL,
	[VIP] [varchar](50) NULL,

	[BTag2] [varchar](100) NULL,
	[AffiliateID] [varchar](30) NULL,
	[AffiliateName] [varchar](100) NULL,
	[SiteID] [varchar](10) NULL,
	[TrafficSource] [varchar](50) NULL,
	[RefURL] [varchar](500) NULL,
	[CampaignID] [varchar](50) NULL,
	[Keywords] [varchar](500) NULL,

	[StatementMethod] [varchar](20) NULL,
	[StatementFrequency] [varchar](20) NULL,

-- Customer Details --
	[CustomerID] [int] NOT NULL,
	[CustomerSource] [char](3) NOT NULL,
	[Title] [varchar](10) NULL,
	[Surname] [varchar](60) NULL,
	[FirstName] [varchar](60) NULL,
	[MiddleName] [varchar](60) NULL,
	[Gender] [char](1) NULL,
	[DOB] [date] NULL,
	[Address1] [varchar](60) NULL,
	[Address2] [varchar](60) NULL,
	[Suburb] [varchar](50) NULL,
	[StateCode] [varchar](3) NULL,
	[State] [varchar](30) NULL,
	[PostCode] [varchar](7) NULL,
	[CountryCode] [varchar](3) NULL,
	[Country] [varchar](40) NULL,
	[GeographicalLocation] [geography] NULL,
	[AustralianClient] [varchar](3) NULL,
	[Phone] [varchar](20) NULL,
	[Mobile] [varchar](20) NULL,
	[Fax] [varchar](20) NULL,
	[Email] [varchar](100) NULL,

-- Advanced
	[AccountTitle] [varchar](10) NULL,
	[AccountSurname] [varchar](60) NULL,
	[AccountFirstName] [varchar](60) NULL,
	[AccountMiddleName] [varchar](60) NULL,
	[AccountGender] [char](1) NULL,
	[AccountDOB] [date] NULL,
	[HasHomeAddress] [tinyint] NOT NULL,
	[HomeAddress1] [varchar](60) NULL,
	[HomeAddress2] [varchar](60) NULL,
	[HomeSuburb] [varchar](50) NULL,
	[HomeStateCode] [varchar](3) NULL,
	[HomeState] [varchar](30) NULL,
	[HomePostCode] [varchar](7) NULL,
	[HomeCountryCode] [varchar](3) NULL,
	[HomeCountry] [varchar](40) NULL,
	[HomePhone] [varchar](20) NULL,
	[HomeMobile] [varchar](20) NULL,
	[HomeFax] [varchar](20) NULL,
	[HomeEmail] [varchar](100) NULL,
	[HomeAlternateEmail] [varchar](100) NULL,
	[HasPostalAddress] [tinyint] NOT NULL,
	[PostalAddress1] [varchar](60) NULL,
	[PostalAddress2] [varchar](60) NULL,
	[PostalSuburb] [varchar](50) NULL,
	[PostalStateCode] [varchar](3) NULL,
	[PostalState] [varchar](30) NULL,
	[PostalPostCode] [varchar](7) NULL,
	[PostalCountryCode] [varchar](3) NULL,
	[PostalCountry] [varchar](40) NULL,
	[PostalPhone] [varchar](20) NULL,
	[PostalMobile] [varchar](20) NULL,
	[PostalFax] [varchar](20) NULL,
	[PostalEmail] [varchar](100) NULL,
	[PostalAlternateEmail] [varchar](100) NULL,
	[HasBusinessAddress] [tinyint] NOT NULL,
	[BusinessAddress1] [varchar](60) NULL,
	[BusinessAddress2] [varchar](60) NULL,
	[BusinessSuburb] [varchar](50) NULL,
	[BusinessStateCode] [varchar](3) NULL,
	[BusinessState] [varchar](30) NULL,
	[BusinessPostCode] [varchar](7) NULL,
	[BusinessCountryCode] [varchar](3) NULL,
	[BusinessCountry] [varchar](40) NULL,
	[BusinessPhone] [varchar](20) NULL,
	[BusinessMobile] [varchar](20) NULL,
	[BusinessFax] [varchar](20) NULL,
	[BusinessEmail] [varchar](100) NULL,
	[BusinessAlternateEmail] [varchar](100) NULL,
	[HasOtherAddress] [tinyint] NOT NULL,
	[OtherAddress1] [varchar](60) NULL,
	[OtherAddress2] [varchar](60) NULL,
	[OtherSuburb] [varchar](50) NULL,
	[OtherStateCode] [varchar](3) NULL,
	[OtherState] [varchar](30) NULL,
	[OtherPostCode] [varchar](7) NULL,
	[OtherCountryCode] [varchar](3) NULL,
	[OtherCountry] [varchar](40) NULL,
	[OtherPhone] [varchar](20) NULL,
	[OtherMobile] [varchar](20) NULL,
	[OtherFax] [varchar](20) NULL,
	[OtherEmail] [varchar](100) NULL,
	[OtherAlternateEmail] [varchar](100) NULL,

	[SourceBTag2] [varchar](100) NULL,
	[SourceTrafficSource] [varchar](50) NULL,
	[SourceRefURL] [varchar](500) NULL,
	[SourceCampaignID] [varchar](50) NULL,
	[SourceKeywords] [varchar](500) NULL,

-- Metadata --
	[FirstDate] [datetime2](3) NULL,
	[FromDate] [datetime2](3) NULL,
	[ToDate] [datetime2](3) NULL,
	[CreatedDate] [datetime2](7) NULL,
	[CreatedBatchKey] [int] NULL,
	[CreatedBy] [varchar](32) NULL,
	[ModifiedDate] [datetime2](7) NULL,
	[ModifiedBatchKey] [int] NULL,
	[ModifiedBy] [varchar](32) NULL,
	CONSTRAINT [CI_Account_PK] PRIMARY KEY CLUSTERED ([AccountKey] ASC) WITH (DATA_COMPRESSION = ROW)
)
GO

CREATE UNIQUE NONCLUSTERED INDEX [NI_Account_NK]
    ON [Dimension].[Account]([LedgerId] ASC, [LedgerSource] ASC)
    INCLUDE([AccountKey]);
GO

CREATE NONCLUSTERED INDEX [NI_Account_LedgerType] 
	ON [Dimension].[Account] ([LedgerTypeID] ASC, [LedgerSource] ASC)
GO

CREATE NONCLUSTERED INDEX [NI_Account_Customer] 
	ON [Dimension].[Account] ([CustomerID] ASC,	[CustomerSource] ASC)
GO

CREATE NONCLUSTERED INDEX [NI_Account_CompanyKey]
	ON [Dimension].[Account] ([CompanyKey] ASC)
GO

CREATE NONCLUSTERED INDEX [NI_Account_LedgerOpenedDayKey]
	ON [Dimension].[Account] ([LedgerOpenedDayKey] ASC)
GO

CREATE NONCLUSTERED INDEX [NI_Account_AccountOpenedDayKey]
	ON [Dimension].[Account] ([AccountOpenedDayKey] ASC)
GO

CREATE NONCLUSTERED INDEX [NI_Account_AccountOpenedChannelKey]
	ON [Dimension].[Account] ([AccountOpenedChannelKey] ASC)
GO

CREATE NONCLUSTERED INDEX [NI_Account_AccountId_AccountSource] 
	ON [Dimension].[Account] ([AccountID] ASC, [AccountSource] ASC)
	INCLUDE ([AccountKey],[LedgerSequenceNumber])
GO

CREATE NONCLUSTERED INDEX [NI_Account_ModifiedBatchKey] 
	ON [Dimension].[Account] ([ModifiedBatchKey] ASC)
GO

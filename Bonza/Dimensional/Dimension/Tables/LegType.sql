﻿CREATE TABLE [Dimension].[LegType] (
    [LegTypeKey]       INT           IDENTITY (1, 1) NOT NULL,
    [LegTypeName]      VARCHAR (32)  NOT NULL,
    [LegNumber]        SMALLINT      NOT NULL,
    [NumberOfLegs]     SMALLINT      NOT NULL,
    [FromDate]         DATETIME2 (0) NOT NULL,
    [ToDate]           DATETIME2 (0) NOT NULL,
    [FirstDate]        DATETIME2 (0) NOT NULL,
    [CreatedDate]      DATETIME2 (7) NOT NULL,
    [CreatedBatchKey]  INT           NOT NULL,
    [CreatedBy]        VARCHAR (32)  NOT NULL,
    [ModifiedDate]     DATETIME2 (7) NOT NULL,
    [ModifiedBatchKey] INT           NOT NULL,
    [ModifiedBy]       VARCHAR (32)  NOT NULL,
    CONSTRAINT [CI-PK-LegTypeKey] PRIMARY KEY CLUSTERED ([LegTypeKey] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [NCI-NaturalKeys]
    ON [Dimension].[LegType]([NumberOfLegs] ASC, [LegNumber] ASC)
    INCLUDE([LegTypeKey]);


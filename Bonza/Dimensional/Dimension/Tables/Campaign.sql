﻿CREATE TABLE [Dimension].[Campaign] (
    [CampaignKey]        INT           IDENTITY (1, 1) NOT NULL,
    [CampaignId]         INT           NOT NULL,
    [CampaignName]       VARCHAR (32)  NOT NULL,
    [CampaignComment]    VARCHAR (100) NOT NULL,
    [CampaignActive]     VARCHAR (3)   NOT NULL,
    [CreatedDayKey]      INT           NOT NULL,
    [CampaignTypeKey]    INT           NOT NULL,
    [CampaignTypeId]     INT           NOT NULL,
    [CampaignTypeName]   VARCHAR (32)  NOT NULL,
    [CampaignTypeActive] VARCHAR (3)   NOT NULL,
    [FromDate]           DATETIME2 (0) NOT NULL,
    [ToDate]             DATETIME2 (0) NOT NULL,
    [FirstDate]          DATETIME2 (0) NOT NULL,
    [CreatedDate]        DATETIME2 (7) NOT NULL,
    [CreatedBatchKey]    INT           NOT NULL,
    [CreatedBy]          VARCHAR (32)  NOT NULL,
    [ModifiedDate]       DATETIME2 (7) NOT NULL,
    [ModifiedBatchKey]   INT           NOT NULL,
    [ModifiedBy]         VARCHAR (32)  NOT NULL,
    CONSTRAINT [Campaign_PK] PRIMARY KEY CLUSTERED ([CampaignKey] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [NCI-NaturalKeys]
    ON [Dimension].[Campaign]([CampaignId] ASC)
    INCLUDE([CampaignKey]);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Standard internal data warehouse surrogate key', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Campaign', @level2type = N'COLUMN', @level2name = N'CampaignKey';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Unique numeric Id for campaign used by business', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Campaign', @level2type = N'COLUMN', @level2name = N'CampaignId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Generic short form of any type of name for standard succinct report formats', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Campaign', @level2type = N'COLUMN', @level2name = N'CampaignName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Generic verbose form of any name long enough to hold any reasonable value', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Campaign', @level2type = N'COLUMN', @level2name = N'CampaignComment';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Explicit YES and NO values to implement a Boolean data type in a user friendly form.', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Campaign', @level2type = N'COLUMN', @level2name = N'CampaignActive';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Standard internal data warehouse surrogate key', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Campaign', @level2type = N'COLUMN', @level2name = N'CreatedDayKey';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Standard internal data warehouse surrogate key', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Campaign', @level2type = N'COLUMN', @level2name = N'CampaignTypeKey';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numeric identifier of campaign type', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Campaign', @level2type = N'COLUMN', @level2name = N'CampaignTypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Generic short form of any type of name for standard succinct report formats', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Campaign', @level2type = N'COLUMN', @level2name = N'CampaignTypeName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Explicit YES and NO values to implement a Boolean data type in a user friendly form.', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Campaign', @level2type = N'COLUMN', @level2name = N'CampaignTypeActive';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The source system date from which this version of data for the natural key was first encountered.', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Campaign', @level2type = N'COLUMN', @level2name = N'FromDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The source system date from which this version of data for the natural key was deprecated.', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Campaign', @level2type = N'COLUMN', @level2name = N'ToDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The source system date from which the natural key of this record was first encountered.', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Campaign', @level2type = N'COLUMN', @level2name = N'FirstDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The date the record was first created', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Campaign', @level2type = N'COLUMN', @level2name = N'CreatedDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The key of the ETL batch which first created the record', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Campaign', @level2type = N'COLUMN', @level2name = N'CreatedBatchKey';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The user or process which first created the record', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Campaign', @level2type = N'COLUMN', @level2name = N'CreatedBy';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The date the record was last modified', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Campaign', @level2type = N'COLUMN', @level2name = N'ModifiedDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The key of the ETL batch which last modified the record', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Campaign', @level2type = N'COLUMN', @level2name = N'ModifiedBatchKey';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The user or porcess which last modified the record', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Campaign', @level2type = N'COLUMN', @level2name = N'ModifiedBy';


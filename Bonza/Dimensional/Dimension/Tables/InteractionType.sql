﻿
CREATE TABLE [Dimension].[InteractionType](
	[InteractiontypeKey] [int] IDENTITY(1,1) NOT NULL,
	[InteractionTypeId] [varchar](20) NULL,
	[InteractionTypeSource] [varchar](5) NULL,
	[InteractionTypeName] [varchar](38) NULL,
	[CreatedDate] [datetime2](7) NULL,
	[CreatedBatchKey] [int] NULL,
	[CreatedBy] [varchar](32) NULL,
	[ModifiedDate] [datetime2](7) NULL,
	[ModifiedBatchKey] [int] NULL,
	[ModifiedBy] [varchar](32) NULL,
 CONSTRAINT [PK_InteractionType] PRIMARY KEY CLUSTERED 
(
	[InteractiontypeKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


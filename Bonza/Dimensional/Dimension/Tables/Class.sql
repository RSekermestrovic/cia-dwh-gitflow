﻿CREATE TABLE [Dimension].[Class] (
    [ClassKey]        SMALLINT      IDENTITY (1, 1) NOT NULL,
    [ClassId]         SMALLINT      NOT NULL,
    [Source]          CHAR (3)      NOT NULL,
    [SourceClassName] VARCHAR (32)  NOT NULL,
    [ClassCode]       VARCHAR (4)   NOT NULL,
    [ClassName]       VARCHAR (32)  NOT NULL,
    [ClassIcon]       IMAGE         NULL,
    [SuperclassKey]   INT           NOT NULL,
    [SuperclassName]  VARCHAR (32)  NOT NULL,
    [FromDate]        DATETIME2 (3) NOT NULL,
    [ToDate]          DATETIME2 (3) NOT NULL,
	[CreatedDate]      DATETIME2 (7) NOT NULL,
    [CreatedBatchKey]  INT           NOT NULL,
    [CreatedBy]        VARCHAR (32)  NOT NULL,
    [ModifiedDate]     DATETIME2 (7) NOT NULL,
    [ModifiedBatchKey] INT           NOT NULL,
    [ModifiedBy]       VARCHAR (32)  NOT NULL,
    CONSTRAINT [CI-PK-ClassKey] PRIMARY KEY CLUSTERED ([ClassKey] ASC)
);
GO

CREATE UNIQUE NONCLUSTERED INDEX [NCI-NaturalKeys]
    ON [Dimension].[Class]([ClassId] ASC, [Source] ASC, [FromDate] ASC, [ToDate] ASC)
    INCLUDE([ClassKey]);
GO


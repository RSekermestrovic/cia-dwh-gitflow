﻿CREATE TABLE [Dimension].[BetStatus] (
    [StatusKey]        INT           IDENTITY (1, 1) NOT NULL,
    [StatusCode]       CHAR (1)      NOT NULL,
    [StatusName]       VARCHAR (32)  NOT NULL,
    [OutcomeCode]      CHAR (1)      NOT NULL,
    [OutcomeName]      VARCHAR (32)  NOT NULL,
    [FromDate]         DATETIME2 (0) NOT NULL,
    [ToDate]           DATETIME2 (0) NOT NULL,
    [FirstDate]        DATETIME2 (0) NOT NULL,
    [CreatedDate]     DATETIME2 (7) NOT NULL,
    [CreatedBatchKey]  INT           NOT NULL,
    [CreatedBy]        VARCHAR (32)  NOT NULL,
    [ModifiedDate]     DATETIME2 (7) NOT NULL,
    [ModifiedBatchKey] INT           NOT NULL,
    [ModifiedBy]       VARCHAR (32)  NOT NULL,
    CONSTRAINT [BetStatus_PK] PRIMARY KEY NONCLUSTERED ([StatusKey] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Defines the current status of a bet along its lifecycle.

e.g. Placed, Cancelled, Settled', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'BetStatus';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Standard internal data warehouse surrogate key', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'BetStatus', @level2type = N'COLUMN', @level2name = N'StatusKey';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Standard single character reference code value used as default codification form for low cardinality data sets.

e.g. P = Placed, C = Cancelled, S = Settled', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'BetStatus', @level2type = N'COLUMN', @level2name = N'StatusCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Generic short form of any type of name for standard succinct report formats

e.g. Placed, Cancelled, Settled', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'BetStatus', @level2type = N'COLUMN', @level2name = N'StatusName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Standard single character reference code value used as default codification form for low cardinality data sets

Quick refrence to the bets ultimate outcome from the clients perspective

e.g. W = Win, L = Loss, P = Pending, C = Cancelled', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'BetStatus', @level2type = N'COLUMN', @level2name = N'OutcomeCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Generic short form of any type of name for standard succinct report formats

Refrence to the bets ultimate outcome from the clients perspective

e.g. Win, Loss, Pending, Cancelled', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'BetStatus', @level2type = N'COLUMN', @level2name = N'OutcomeName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The source system date from which this version of data for the natural key was first encountered.', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'BetStatus', @level2type = N'COLUMN', @level2name = N'FromDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The source system date from which this version of data for the natural key was deprecated.', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'BetStatus', @level2type = N'COLUMN', @level2name = N'ToDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The source system date from which the natural key of this record was first encountered.', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'BetStatus', @level2type = N'COLUMN', @level2name = N'FirstDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The date the record was first created', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'BetStatus', @level2type = N'COLUMN', @level2name = N'CreatedDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The key of the ETL batch which first created the record', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'BetStatus', @level2type = N'COLUMN', @level2name = N'CreatedBatchKey';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The user or process which first created the record', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'BetStatus', @level2type = N'COLUMN', @level2name = N'CreatedBy';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The date the record was last modified', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'BetStatus', @level2type = N'COLUMN', @level2name = N'ModifiedDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The key of the ETL batch which last modified the record', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'BetStatus', @level2type = N'COLUMN', @level2name = N'ModifiedBatchKey';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The user or porcess which last modified the record', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'BetStatus', @level2type = N'COLUMN', @level2name = N'ModifiedBy';


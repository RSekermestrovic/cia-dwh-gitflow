﻿CREATE TABLE [Dimension].[RevenueLifeCycle](
	[RevenueLifeCycleKey] [int] NOT NULL,
	[RevenueLifeCycle] [varchar](20) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBatchKey] [int] NOT NULL,
	[CreatedBy] [varchar](32) NOT NULL,
	[ModifiedDate] [datetime2](7) NOT NULL,
	[ModifiedBatchKey] [int] NOT NULL,
	[ModifiedBy] [varchar](32) NOT NULL,
    CONSTRAINT [CI-PK-RevenueLifeCycle] PRIMARY KEY CLUSTERED ([RevenueLifeCycleKey] ASC)
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [NCI-NaturalKeys]
    ON [Dimension].[RevenueLifeCycle]([RevenueLifeCycle] ASC)
    INCLUDE([RevenueLifeCycleKey]);
GO


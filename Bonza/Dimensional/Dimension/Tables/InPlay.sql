﻿CREATE TABLE [Dimension].[InPlay](
	[InPlayKey] [int] NOT NULL,
	[CashoutType] [int] NOT NULL,
	[InPlay] [varchar](20) NOT NULL,
	[ClickToCall] [varchar](20) NOT NULL,
	[CashedOut] [char](3) NOT NULL,
	[StatusAtCashout] [varchar](20) NOT NULL,
	[IsDoubleDown] [bit] NOT NULL,
	[IsDoubledDown] [bit] NOT NULL,
	[DoubleDown] [varchar](20) NOT NULL
) 
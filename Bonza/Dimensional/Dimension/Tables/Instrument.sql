﻿CREATE TABLE [Dimension].[Instrument] (
    [InstrumentKey]     INT           IDENTITY (1, 1) NOT NULL,
    [InstrumentID]      VARCHAR (50)  NOT NULL,
    [Source]            CHAR (3)      NOT NULL,
    [AccountNumber]     VARCHAR (32)  NULL,
    [SafeAccountNumber] VARCHAR (32)  NULL,
    [AccountName]       VARCHAR (100) NULL,
    [BSB]               VARCHAR (32)  NULL,
    [ExpiryDate]        DATETIME2 (0) NULL,
    [Verified]          VARCHAR (3)   NULL,
    [VerifyAmount]      MONEY         NULL,
    [InstrumentTypeKey] INT           NOT NULL,
    [InstrumentType]    VARCHAR (15)  NOT NULL,
    [ProviderKey]       INT           NULL,
    [ProviderName]      VARCHAR (100) NULL,
    [FromDate]          DATETIME2 (0) NOT NULL,
    [ToDate]            DATETIME2 (0) NOT NULL,
    [FirstDate]         DATETIME2 (0) NOT NULL,
    [CreatedDate]       DATETIME2 (7) NOT NULL,
    [CreatedBatchKey]   INT           NOT NULL,
    [CreatedBy]         VARCHAR (32)  NOT NULL,
    [ModifiedDate]      DATETIME2 (7) NOT NULL,
    [ModifiedBatchKey]  INT           NOT NULL,
    [ModifiedBy]        VARCHAR (32)  NOT NULL,
    CONSTRAINT [Instrument_PK] PRIMARY KEY CLUSTERED ([InstrumentKey] ASC)
);




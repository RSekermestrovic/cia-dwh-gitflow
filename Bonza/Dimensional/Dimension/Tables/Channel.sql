﻿CREATE TABLE [Dimension].[Channel](
	[ChannelKey] [int] IDENTITY(1,1) NOT NULL,
	[ChannelId] [smallint] NULL,
	[ChannelDescription] [varchar](200) NULL,
	[ChannelName] [varchar](50) NULL,
	[ChannelTypeName] [varchar](50) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBatchKey] [int] NOT NULL,
	[CreatedBy] [varchar](20) NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[ModifiedBatchKey] [int] NOT NULL,
	[ModifiedBy] [varchar](20) NOT NULL,
    CONSTRAINT [CI-PK-ChannelKey] PRIMARY KEY CLUSTERED ([ChannelKey] ASC)
);
GO

CREATE UNIQUE NONCLUSTERED INDEX [NCI-NaturalKeys]
    ON [Dimension].[Channel]([ChannelId] ASC)
    INCLUDE([ChannelKey]);
GO

﻿CREATE TABLE [Dimension].[BalanceType] (
	[BalanceTypeKey] [smallint] IDENTITY(1,1) NOT NULL,
	[BalanceTypeID] [char](3) NOT NULL,
	[BalanceTypeName] [varchar](32) NOT NULL,
	[FromDate] [datetime2](0) NOT NULL,
	[ToDate] [datetime2](0) NOT NULL,
	[FirstDate] [datetime2](0) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBatchKey] [int] NOT NULL,
	[CreatedBy] [varchar](32) NOT NULL,
	[ModifiedDate] [datetime2](7) NOT NULL,
	[ModifiedBatchKey] [int] NOT NULL,
	[ModifiedBy] [varchar](32) NOT NULL,
    CONSTRAINT [BalanceType_PK] PRIMARY KEY NONCLUSTERED ([BalanceTypeKey] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [NCI-NaturalKeys]
    ON [Dimension].[BalanceType]([BalanceTypeName] ASC)
    INCLUDE([BalanceTypeKey]);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Defines a logical financial balance held in one of TomWaterhouse.com''s bank accounts', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'BalanceType';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Standard internal data warehouse surrogate key', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'BalanceType', @level2type = N'COLUMN', @level2name = N'BalanceTypeKey';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Generic short form of any type of name for standard succinct report formats', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'BalanceType', @level2type = N'COLUMN', @level2name = N'BalanceTypeName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The source system date from which this version of data for the natural key was first encountered.', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'BalanceType', @level2type = N'COLUMN', @level2name = N'FromDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The source system date from which this version of data for the natural key was deprecated.', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'BalanceType', @level2type = N'COLUMN', @level2name = N'ToDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The source system date from which the natural key of this record was first encountered.', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'BalanceType', @level2type = N'COLUMN', @level2name = N'FirstDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The date the record was first created', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'BalanceType', @level2type = N'COLUMN', @level2name = N'CreatedDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The key of the ETL batch which first created the record', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'BalanceType', @level2type = N'COLUMN', @level2name = N'CreatedBatchKey';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The user or process which first created the record', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'BalanceType', @level2type = N'COLUMN', @level2name = N'CreatedBy';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The date the record was last modified', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'BalanceType', @level2type = N'COLUMN', @level2name = N'ModifiedDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The key of the ETL batch which last modified the record', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'BalanceType', @level2type = N'COLUMN', @level2name = N'ModifiedBatchKey';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The user or porcess which last modified the record', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'BalanceType', @level2type = N'COLUMN', @level2name = N'ModifiedBy';


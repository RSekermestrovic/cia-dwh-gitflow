﻿CREATE TABLE [Dimension].[Lifestage](
	[LifeStageKey] [int] NOT NULL,
	[LifeStage] [varchar](20) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBatchKey] [int] NOT NULL,
	[CreatedBy] [varchar](32) NOT NULL,
	[ModifiedDate] [datetime2](7) NOT NULL,
	[ModifiedBatchKey] [int] NOT NULL,
	[ModifiedBy] [varchar](32) NOT NULL,
    CONSTRAINT [CI-PK-LifeStage] PRIMARY KEY CLUSTERED ([LifeStageKey] ASC)
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [NCI-NaturalKeys]
    ON [Dimension].[LifeStage]([LifeStage] ASC)
    INCLUDE([LifeStageKey]);
GO


﻿CREATE TABLE [Dimension].[Event] (
    [EventKey]                        INT                IDENTITY (1, 1) NOT NULL,
    [Source]                          CHAR (3)           NOT NULL,
	[SourceEventId]                   INT                NOT NULL,
    [EventId]                         INT                NOT NULL,
    [SourceEvent]					  VARCHAR (255)      NULL,
	[SourceEventType]				  VARCHAR (32)		 NULL,
    [ParentEventId]					  INT                NULL,
    [ParentEvent]					  VARCHAR (255)      NULL,
    [GrandparentEventId]			  INT                NULL,
    [GrandparentEvent]				  VARCHAR (255)      NULL,
    [GreatGrandparentEventId]		  INT                NULL,
    [GreatGrandparentEvent]			  VARCHAR (255)      NULL,
    [SourceEventDate]                 DATETIME           NULL,
    [ClassID]                         INT                NOT NULL,
    [ClassKey]                        SMALLINT           NULL,
	[SourceRaceNumber]				  INT				 NULL,
    [Event]                           VARCHAR (255)      NULL,
	[Round]                           VARCHAR (255)      NULL,
    [RoundSequence]                   SMALLINT           NULL,
    [Competition]                     VARCHAR (255)      NULL,
    [CompetitionSeason]               VARCHAR (32)       NULL,
    [CompetitionStartDate]            DATE               NULL,
    [CompetitionEndDate]              DATE               NULL,
    [Grade]                           VARCHAR (255)      NULL,
    [Distance]                        VARCHAR (10)       NULL,
	[SourcePrizePool]                 INT                NULL,
    [PrizePool]                       INT                NULL,
	[SourceTrackCondition]            VARCHAR (32)       NULL,
    [SourceEventWeather]              VARCHAR (32)       NULL,
    [TrackCondition]                  VARCHAR (32)       NULL,
    [EventWeather]                    VARCHAR (32)       NULL,
	[EventAbandoned]				  VARCHAR (10)	     NULL,
	[EventAbandonedDateTime]		  DATETIME2 (3)		 NULL,
	[EventAbandonedDayKey]            INT                NULL,
	[LiveVideoStreamed]			      VARCHAR (10)	     NULL,
	[LiveScoreBoard]				  VARCHAR (10)		 NULL,
    [ScheduledDateTime]               DATETIME           NULL,
    [ScheduledDayKey]                 INT                NULL,
    [ScheduledVenueDateTime]          DATETIME           NULL,
    [ResultedDateTime]                DATETIME           NULL,
    [ResultedDayKey]                  INT                NULL,
    [ResultedVenueDateTime]           DATETIME           NULL,
	[SourceVenueId]		              INT                NULL,
    [SourceVenue]			          VARCHAR (255)      NULL,	
	[SourceVenueType]                 VARCHAR (10)       NULL,
	[SourceVenueEventType]			  INT				 NULL,
	[SourceVenueEventTypeName]		  VARCHAR (255)		 NULL,
    [SourceVenueStateCode]            CHAR (3)           NULL,
    [SourceVenueState]                VARCHAR (32)       NULL,
    [SourceVenueCountryCode]          CHAR (3)           NULL,
    [SourceVenueCountry]              VARCHAR (32)       NULL,
    [VenueId]		                  INT                NULL,
    [Venue]                           VARCHAR (100)      NULL,
    [VenueType]                       VARCHAR (100)      NULL,
    [VenueStateCode]                  VARCHAR (3)        NULL,
    [VenueState]                      VARCHAR (32)       NULL,
    [VenueCountryCode]                VARCHAR (3)        NULL,
    [VenueCountry]                    VARCHAR (32)       NULL,
	[SourceSubSportType]			  VARCHAR (50)		 NULL,
	[SourceRaceGroup]				  VARCHAR (50)		 NULL,
	[SourceRaceClass]				  VARCHAR (200)		 NULL,
	[SourceHybridPricingTemplate]	  VARCHAR (100)		 NULL,
	[SourceWeightType]				  VARCHAR (100)		 NULL,
	[SubSportType]					  VARCHAR (50)		 NULL,
	[RaceGroup]						  VARCHAR (50)		 NULL,
	[RaceClass]						  VARCHAR (200)		 NULL,
	[HybridPricingTemplate]			  VARCHAR (100)		 NULL,
	[WeightType]					  VARCHAR (100)		 NULL,
	[SourceDistance]				  INT				 NULL,
	[SourceEventAbandoned]			  INT			     NULL,	
	[SourceLiveStreamPerformId]		  VARCHAR (50)		 NULL,
    [ColumnLock]                      BIGINT             NOT NULL,
    [FromDate]                        DATETIME2 (3)      NOT NULL,
    [FirstDate]                       DATETIME2 (3)      NOT NULL,
    [CreatedDate]                     DATETIME2 (7)      NOT NULL,
    [CreatedBatchKey]                 INT                NOT NULL,
    [CreatedBy]                       VARCHAR (100)      NOT NULL,
    [ModifiedDate]                    DATETIME2 (7)      NOT NULL,
    [ModifiedBatchKey]                INT                NOT NULL,
    [ModifiedBy]                      VARCHAR (100)      NOT NULL,
    CONSTRAINT [CI-PK-EventKey] PRIMARY KEY CLUSTERED ([EventKey] ASC) WITH (DATA_COMPRESSION = ROW)
);
GO
CREATE NONCLUSTERED INDEX [NI_Event_ModifiedBatchKey]
    ON [Dimension].[Event]([ModifiedBatchKey] ASC) 
	INCLUDE (EventId, [Competition]) WITH (DATA_COMPRESSION = ROW);
GO
CREATE UNIQUE NONCLUSTERED INDEX [NI-NK-Event]
    ON [Dimension].[Event]([EventId] ASC, [Source] ASC)
    INCLUDE([EventKey]) WITH (DATA_COMPRESSION = ROW);
GO
CREATE NONCLUSTERED INDEX [NI_Event_ScheduledDayKey] ON [Dimension].[Event]
(
	[ScheduledDayKey] ASC
) INCLUDE (EventKey) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NI_Event_ResultedDayKey] ON [Dimension].[Event]
(
	[ResultedDayKey] ASC
) INCLUDE (EventKey) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NI_Event_AbandonedDayKey] ON [Dimension].[Event]
(
	[EventAbandonedDayKey] ASC
) INCLUDE (EventKey) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NI_Event_ClassKey] ON [Dimension].[Event]
(
	[ClassKey] ASC
) INCLUDE (EventKey) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
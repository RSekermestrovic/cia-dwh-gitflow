﻿CREATE TABLE [Dimension].[Time] (
    [TimeKey]            INT           NOT NULL,
    [TimeID]           VARCHAR (5)   NOT NULL,
    [TimeText]           VARCHAR (7)   NOT NULL,
    [Time]               TIME (0)      NOT NULL,
    [TimeStartTime]      TIME (0)      NOT NULL,
    [TimeEndTime]        TIME (0)      NOT NULL,
    [TimeStartTimestamp] DATETIME2 (7) NOT NULL,
    [TimeEndTimestamp]   DATETIME2 (7) NOT NULL,
    [TimeOfHourNumber]   SMALLINT      NOT NULL,
    [TimeOfDayNumber]    SMALLINT      NOT NULL,
    [HourKey]            INT           NOT NULL,
    [HourID]           VARCHAR (3)   NOT NULL,
    [HourText]           VARCHAR (7)   NOT NULL,
    [HourStartTime]      TIME (0)      NOT NULL,
    [HourEndTime]        TIME (0)      NOT NULL,
    [HourOfDayNumber]    TINYINT       NOT NULL,
    [PeriodKey]          INT           NOT NULL,
    [PeriodText]         VARCHAR (32)  NOT NULL,
    [FromDate]           DATETIME2 (0) NOT NULL,
    [ToDate]             DATETIME2 (0) NOT NULL,
    [FirstDate]          DATETIME2 (0) NOT NULL,
    [CreatedDate]        DATETIME2 (7) NOT NULL,
    [CreatedBatchKey]    INT           NOT NULL,
    [CreatedBy]          VARCHAR (32)  NOT NULL,
    [ModifiedDate]       DATETIME2 (7) NOT NULL,
    [ModifiedBatchKey]   INT           NOT NULL,
    [ModifiedBy]         VARCHAR (32)  NOT NULL,
    CONSTRAINT [Time_PK] PRIMARY KEY NONCLUSTERED ([TimeKey] ASC)
);


GO
CREATE NONCLUSTERED INDEX [NC_Time_TimeText]
    ON [Dimension].[Time]([TimeID] ASC)
    INCLUDE([TimeKey]);


GO

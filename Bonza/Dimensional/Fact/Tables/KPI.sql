CREATE TABLE [Fact].[KPI](
	[ContractKey] [int] NOT NULL,
	[DayKey] [int] NOT NULL,
	[AccountKey] [int] NOT NULL,
	[AccountStatusKey] [int] NOT NULL,
	[BetTypeKey] [smallint] NOT NULL,
	[LegTypeKey] [smallint] NOT NULL,
	[ChannelKey] [smallint] NOT NULL,
	[ActionChannelKey] [smallint] NOT NULL,
	[CampaignKey] [smallint] NOT NULL,
	[MarketKey] [int] NOT NULL,
	[EventKey] [int] NOT NULL,
	[ClassKey] [smallint] NOT NULL,
	[UserKey] [smallint] NOT NULL,
	[InstrumentKey] [int] NOT NULL,
	[InPlayKey] [int] NOT NULL,
	[AccountOpened] [int] NULL,
	[FirstDeposit] [int] NULL,
	[FirstBet] [int] NULL,
	[DepositsRequested] [money] NOT NULL,
	[DepositsCompleted] [money] NOT NULL,
	[DepositsRequestedCount] [bigint] NULL,
	[DepositsCompletedCount] [bigint] NULL,
	[Promotions] [money] NOT NULL,
	[Transfers] [money] NOT NULL,
	[BetsPlaced] [int] NULL,
	[BonusBetsPlaced] [int] NULL,
	[ContractsPlaced] [int] NULL,
	[BonusContractsPlaced] [int] NULL,
	[BettorsPlaced] [int] NULL,
	[BonusBettorsPlaced] [int] NULL,
	[PlayDaysPlaced] [bigint] NULL,
	[BonusPlayDaysPlaced] [bigint] NULL,
	[PlayFiscalWeeksPlaced] [bigint] NULL,
	[BonusPlayFiscalWeeksPlaced] [bigint] NULL,
	[PlayCalenderWeeksPlaced] [bigint] NULL,
	[BonusPlayCalenderWeeksPlaced] [bigint] NULL,
	[PlayFiscalMonthsPlaced] [bigint] NULL,
	[BonusPlayFiscalMonthsPlaced] [bigint] NULL,
	[PlayCalenderMonthsPlaced] [bigint] NULL,
	[BonusPlayCalenderMonthsPlaced] [bigint] NULL,
	[Stakes] [money] NOT NULL,
	[BonusStakes] [money] NULL,
	[Winnings] [money] NOT NULL,
	[BettorsCashedOut] [int] NULL,
	[BetsCashedOut] [int] NULL,
	[Cashout] [money] NOT NULL,
	[CashoutDifferential] [money] NOT NULL,
	[Fees] [money] NOT NULL,
	[WithdrawalsRequested] [money] NOT NULL,
	[WithdrawalsCompleted] [money] NOT NULL,
	[WithdrawalsRequestedCount] [bigint] NULL,
	[WithdrawalsCompletedCount] [bigint] NULL,
	[Adjustments] [money] NOT NULL,
	[BetsResulted] [int] NULL,
	[BonusBetsResulted] [int] NULL,
	[ContractsResulted] [int] NULL,
	[BonusContractsResulted] [int] NULL,
	[BettorsResulted] [int] NULL,
	[BonusBettorsResulted] [int] NULL,
	[PlayDaysResulted] [bigint] NULL,
	[BonusPlayDaysResulted] [bigint] NULL,
	[PlayFiscalWeeksResulted] [bigint] NULL,
	[BonusPlayFiscalWeeksResulted] [bigint] NULL,
	[PlayCalenderWeeksResulted] [bigint] NULL,
	[BonusPlayCalenderWeeksResulted] [bigint] NULL,
	[PlayFiscalMonthsResulted] [bigint] NULL,
	[BonusPlayFiscalMonthsResulted] [bigint] NULL,
	[PlayCalenderMonthsResulted] [bigint] NULL,
	[BonusPlayCalenderMonthsResulted] [bigint] NULL,
	[Turnover] [money] NOT NULL,
	[BonusTurnover] [money] NULL,
	[GrossWin] [money] NOT NULL,
	[NetRevenue] [money] NOT NULL,
	[NetRevenueAdjustment] [money] NULL,
	[BonusWinnings] [money] NOT NULL,
	[Betback] [money] NOT NULL,
	[GrossWinAdjustment] [money] NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBatchKey] [int] NOT NULL,
	[CreatedBy] [varchar](32) NOT NULL,
	[ModifiedDate] [datetime2](7) NOT NULL,
	[ModifiedBatchKey] [int] NOT NULL,
	[ModifiedBy] [varchar](32) NOT NULL,
	[StructureKey]  AS ([accountkey]*	case [InPlayKey] 
											when (2) then (-1) when (4) then (-1) when (7) then (-1) when (9) then (-1) 
											when (12) then (-1) when (14) then (-1) when (17) then (-1) when (19) then (-1) 
											when (22) then (-1) when (24) then (-1) when (27) then (-1) when (29) then (-1) 
											else (1) 
										end)
) ON [par_sch_int_Dimensional] ([DayKey])
GO

CREATE UNIQUE NONCLUSTERED INDEX [NI_KPI_PK]
ON [Fact].[KPI](
	[DayKey] ASC,
	[ContractKey] ASC,
	[AccountKey] ASC,
	[AccountStatusKey] ASC,
	[BetTypeKey] ASC,
	[LegTypeKey] ASC,
	[MarketKey] ASC,
	[EventKey] ASC,
	[ClassKey] ASC,
	[ChannelKey] ASC,
	[ActionChannelKey] ASC,
	[CampaignKey] ASC,
	[UserKey] ASC,
	[InstrumentKey] ASC,
	[InPlayKey] ASC) 
GO

CREATE NONCLUSTERED INDEX [NI_KPI_MarketKey]
ON [Fact].[KPI] ([MarketKey] ASC)
GO

CREATE NONCLUSTERED INDEX [NI_KPI_DayKey] 
ON [Fact].[KPI] ([DayKey] ASC)
GO

CREATE NONCLUSTERED INDEX [NI_KPI_StructureKey] 
ON [Fact].[KPI] ([StructureKey] ASC)
GO

CREATE NONCLUSTERED INDEX [NI_KPI_AccountKey] 
ON [Fact].[KPI] ([AccountKey] ASC)
GO

CREATE NONCLUSTERED INDEX [NI_KPI_ContractKey] 
ON [Fact].[KPI] ([ContractKey] ASC)
GO


CREATE TABLE [Fact].[Balance](
	[AccountKey] [int] NOT NULL,
	[AccountStatusKey] [int] NOT NULL,
	[LedgerID] [bigint] NOT NULL,
	[LedgerSource] [varchar](4) NOT NULL,
	[DayKey] [int] NOT NULL,
	[BalanceTypeKey] [smallint] NOT NULL,
	[WalletKey] [smallint] NOT NULL,
	[OpeningBalance] [money] NOT NULL,
	[TotalTransactedAmount] [money] NOT NULL,
	[ClosingBalance] [money] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBatchKey] [int] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[ModifiedBatchKey] [int] NOT NULL,
	StructureKey AS (accountkey), 
 CONSTRAINT [PK_Balance] PRIMARY KEY CLUSTERED 
(
	[DayKey] ASC,
	[AccountKey] ASC,
	[BalanceTypeKey] ASC,
	[WalletKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) on [par_sch_int_Dimensional] ([DayKey])
);

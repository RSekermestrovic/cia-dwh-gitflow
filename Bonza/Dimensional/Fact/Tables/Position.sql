﻿CREATE TABLE [Fact].[Position] (
   [BalanceTypeKey] [smallint] NOT NULL,
	[AccountKey] [int] NOT NULL,
	[AccountStatusKey] [int] NOT NULL,
	[LedgerID] [bigint] NOT NULL,
	[LedgerSource] [char](3) NOT NULL,
	[DayKey] [int] NOT NULL,
	[ActivityKey] [smallint] NOT NULL,
	[FirstDepositDayKey] [int] NOT NULL,
	[FirstBetDayKey] [int] NOT NULL,
	[FirstBetTypeKey] [smallint] NOT NULL,
	[FirstClassKey] [smallint] NOT NULL,
	[FirstChannelKey] [smallint] NOT NULL,
	[LastDepositDayKey] [int] NULL,
	[LastBetDayKey] [int] NOT NULL,
	[LastBetTypeKey] [smallint] NOT NULL,
	[LastClassKey] [smallint] NOT NULL,
	[LastChannelKey] [smallint] NOT NULL,
	[OpeningBalance] [money] NOT NULL,
	[TransactedAmount] [money] NOT NULL,
	[ClosingBalance] [money] NOT NULL,
	[DaysSinceAccountOpened] [int] NULL,
	[DaysSinceFirstDeposit] [int] NULL,
	[DaysSinceFirstBet] [int] NULL,
	[DaysSinceLastDeposit] [int] NULL,
	[DayssinceLastBet] [int] NULL,
	[LifetimeTurnover] [money] NOT NULL,
	[LifetimeGrossWin] [money] NOT NULL,
	[LifetimeBetCount] [int] NOT NULL,
	[LifetimeBonus] [money] NOT NULL,
	[LifetimeDeposits] [money] NOT NULL,
	[LifetimeWithdrawals] [money] NOT NULL,
	[TurnoverMTDCalender] [money] NULL,
	[TurnoverMTDFiscal] [money] NULL,
	[TierKey] [int] NULL,
	[TierKeyFiscal] [int] NULL,
	[RevenueLifeCycleKey] [int] NULL,
	[LifeStageKey] [int] NULL,
	[PreviousLifeStageKey] [int] NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBatchKey] [int] NOT NULL,
	[CreatedBy] [varchar](32) NULL,
	[ModifiedDate] [datetime2](7) NOT NULL,
	[ModifiedBatchKey] [int] NOT NULL,
	[ModifiedBy] [varchar](32) NULL,
	StructureKey AS (accountkey)
)
GO

CREATE UNIQUE NONCLUSTERED INDEX [Position_PK]
    ON [Fact].[Position]
(
	[DayKey] ASC, 
	[AccountKey] ASC
)
GO

CREATE NONCLUSTERED INDEX [NI_Position_AccountKey] 
	ON [Fact].[Position]
(
	[AccountKey] ASC
)
GO

CREATE NONCLUSTERED INDEX [NI_Position_DayKey] 
	ON [Fact].[Position]
(
	[DayKey] ASC
)
GO

	
﻿

CREATE PROC [Audit].[Sp_TableSizeMonitor]
AS
BEGIN
/*
	Developer: Aaron Jackson
	Date: 31/03/2015
	Desc: Take a snapshot of table size for later analysis
*/

	SET NOCOUNT ON;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE @TableName VARCHAR(max); --For storing values in the cursor
	DECLARE @LastRow INT;
	DECLARE @SnapshotDate DATETIME = GETDATE();

	--A procedure level temp table to store the results

	CREATE TABLE #TempTable
	(
		RowID [int] IDENTITY(1,1) NOT NULL,
		DatabaseName varchar(100),
		SchemaName varchar(100),
		TableName varchar(100) NOT NULL,
		RecordCount varchar(100),
		ReservedStorage_KB varchar(50),
		DataStorage_KB varchar(50),
		IndexStorage_KB varchar(50),
		UnusedStorage_KB varchar(50),
		PRIMARY KEY(RowID)
	);

	--Cursor to get the name of all user tables from the sysobjects listing
	DECLARE tableCursor CURSOR
	FOR 
		select TABLE_SCHEMA + '.' + TABLE_NAME
		from information_schema.tables
	FOR READ ONLY

	--Open the cursor
	OPEN tableCursor

	--Get the first table name from the cursor
	FETCH NEXT FROM tableCursor INTO @TableName

	--Loop until the cursor was not able to fetch
	WHILE (@@Fetch_Status >= 0)
	BEGIN
		--Dump the results of the sp_spaceused query to the temp table
		INSERT  #TempTable (TableName, RecordCount, ReservedStorage_KB, DataStorage_KB, IndexStorage_KB, UnusedStorage_KB)
			EXEC sp_spaceused @TableName;

		SET @LastRow = @@IDENTITY;

		UPDATE #TempTable
		SET SchemaName = SUBSTRING(@TableName, 0, CHARINDEX('.', @TableName)),
			DatabaseName = DB_NAME()
		WHERE RowID = @LastRow;

		--Get the next table name
		FETCH NEXT FROM tableCursor INTO @TableName
	END

	--Get rid of the cursor
	CLOSE tableCursor
	DEALLOCATE tableCursor;

	--Store records in audit table
	INSERT INTO [Audit].[TableSize]
	(
	   [SnapshotDate]
      ,[DatabaseName]
      ,[SchemaName]
      ,[TableName]
      ,[RecordCount]
      ,[ReservedStorage_KB]
      ,[DataStorage_KB]
      ,[IndexStorage_KB]
      ,[UnusedStorage_KB]
	)
	SELECT
		@SnapshotDate,
		DatabaseName,
		SchemaName,
		TableName,
		RecordCount,
		CONVERT(FLOAT,LTRIM(RTRIM(REPLACE(ReservedStorage_KB,'KB','')))),
		CONVERT(FLOAT,LTRIM(RTRIM(REPLACE(DataStorage_KB,'KB','')))),
		CONVERT(FLOAT,LTRIM(RTRIM(REPLACE(IndexStorage_KB,'KB','')))),
		CONVERT(FLOAT,LTRIM(RTRIM(REPLACE(UnusedStorage_KB,'KB',''))))
	FROM #TempTable;


END
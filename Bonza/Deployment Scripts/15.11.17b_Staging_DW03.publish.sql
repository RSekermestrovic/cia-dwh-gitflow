﻿/*
Deployment script for Staging

This code was generated by a tool.
Changes to this file may cause incorrect behavior and will be lost if
the code is regenerated.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar Acquisition "Acquisition"
:setvar Dimensional "Dimensional"
:setvar DimensionalDataPath "G:\SQLData\"
:setvar DimensionalLogPath "K:\SQLLogs\"
:setvar JobPrefix "DW_"
:setvar ServerName "EQ3WNPRDBDW03"
:setvar SSISDB "SSISDB"
:setvar Staging "Staging"
:setvar VersionNumber "15.11.17"
:setvar DatabaseName "Staging"
:setvar DefaultFilePrefix "Staging"
:setvar DefaultDataPath "D:\SQLData\"
:setvar DefaultLogPath "H:\SQLLogs\"

GO
:on error exit
GO
/*
Detect SQLCMD mode and disable script execution if SQLCMD mode is not supported.
To re-enable the script after enabling SQLCMD mode, execute the following:
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'SQLCMD mode must be enabled to successfully execute this script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
PRINT N'Disabling all DDL triggers...'
GO
DISABLE TRIGGER ALL ON DATABASE
GO
PRINT N'Dropping [Control].[LiveETLController]...';


GO
DROP SYNONYM [Control].[LiveETLController];


GO
PRINT N'Creating [Control].[LiveETLController]...';


GO
CREATE SYNONYM [Control].[LiveETLController] FOR [Control].ETLController;


GO
PRINT N'Altering [Cleanse].[SP_Event_Venue]...';


GO
ALTER proc [Cleanse].[SP_Event_Venue]
(
	@BatchKey int = 0,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @RowCount INT;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

BEGIN TRY

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Event_Venue', 'Update', 'Start';

	UPDATE	x
		SET	SourceStateCode = x.StateCode, 
			StateCode = x.NewStateCode, 
			SourceStateName = x.StateName,
			StateName = CASE x.NewStateCode
							WHEN 'N/A' THEN 'Outside Australia' 
							WHEN 'ACT' THEN 'Australian Capital Territory' 
							WHEN 'NSW' THEN 'New South Wales' 
							WHEN 'VIC' THEN 'Victoria' 
							WHEN 'QLD' THEN 'Queensland' 
							WHEN 'SA' THEN 'South Australia' 
							WHEN 'WA' THEN 'Western Australia' 
							WHEN 'TAS' THEN 'Tasmania' 
							WHEN 'NT' THEN 'Northern Territories'
							ELSE 'Unknown'
						END,
			SourceCountryCode = x.CountryCode, 
			CountryCode = c.Country3Code,
			SourceCountryName = x.CountryName, 
			CountryName = x.NewCountryName
	FROM	(	SELECT	EventKey,
						SourceStateCode,
						StateCode,
						CASE
							WHEN SourceVenueId = 3784 THEN 'NT'
							WHEN SourceVenueId = 3787 THEN 'WA'
							WHEN SourceVenueId = 3822 THEN 'N/A'
							WHEN CountryName IN ('UNK','UNKNOWN') THEN 'UNK'
							WHEN CountryName <> 'Australia' THEN 'N/A'
							WHEN StateCode = '0' THEN 'N/A'
							WHEN StateCode = '1' THEN 'ACT'
							WHEN StateCode = '2' THEN 'NSW'
							WHEN StateCode = '3' THEN 'VIC'
							WHEN StateCode = '4' THEN 'QLD'
							WHEN StateCode = '5' THEN 'SA'
							WHEN StateCode = '6' THEN 'WA'
							WHEN StateCode = '7' THEN 'TAS'
							WHEN StateCode = '8' THEN 'NT'
							WHEN StateCode = '9' THEN 'UNK'
							WHEN StateCode IS NULL THEN 'UNK'
							ELSE StateCode
						END NewStateCode,
						SourceStateName,
						StateName,
						SourceCountryCode,
						CountryCode,
						SourceCountryName,
						CountryName,
						CASE
							WHEN SourceVenueId = 3784 THEN 'AUSTRALIA'
							WHEN SourceVenueId = 3787 THEN 'AUSTRALIA'
							WHEN SourceVenueId = 3822 THEN 'SWEDEN'
							WHEN CountryName = 'AUSTRALIA' AND StateCode = '0' AND SourceGrandparentEventName LIKE '%New Zealand' THEN 'NEW ZEALAND'
							WHEN CountryName = 'AUSTRALIA' AND StateCode = '0' AND SourceGrandparentEventName LIKE '%Singapore' THEN 'SINGAPORE'
							WHEN CountryName = 'AUSTRALIA' AND StateCode = '0' AND SourceGrandparentEventName LIKE '%Sweden' THEN 'SWEDEN'
							WHEN CountryName = 'AUSTRALIA' AND StateCode = '0' AND SourceGrandparentEventName LIKE '%United States' THEN 'UNITED STATES'
							WHEN CountryName = 'AUSTRALIA' AND StateCode = '0' THEN 'UNITED KINGDOM'
							WHEN CountryName = 'UNK' THEN 'UNKNOWN'
							ELSE ISNULL(CountryName, 'UNKNOWN')
						END NewCountryName
				FROM	[$(Dimensional)].Dimension.Event
				WHERE	EventKey NOT IN (0,-1)
						AND ModifiedBatchKey = CASE @BatchKey WHEN 0 THEN ModifiedBatchKey ELSE @BatchKey END 
			) x
			LEFT JOIN Reference.Country c ON (c.CountryName = x.NewCountryName)
	WHERE	ISNULL(x.StateCode,'!') <> x.NewStateCode
			OR ISNULL(x.CountryName,'!') <> x.NewCountryName

	UPDATE	x
		SET	ScheduledDayKey = DayKey,
			ModifiedBatchKey =  @BatchKey,
			ModifiedBy = 'Sp_Event_Venue',
			ModifiedDate = getdate()
	FROM	(
			Select		EventKey
						, ScheduledDayKey
						, ScheduledDateTime
						, [DayZone].DayKey
						, [Event].ModifiedBatchKey
						, [Event].ModifiedBy
						, [Event].ModifiedDate
			FROM		[$(Dimensional)].Dimension.[Event] as [Event]
						left join [$(Dimensional)].Dimension.[DayZone] as [DayZone] on CONVERT(VARCHAR(11),CONVERT(DATE,[Event].ScheduledDateTime)) = [DayZone].MasterDayText
																						and [Event].ScheduledDateTime >= [DayZone].FromDate
																						and [Event].ScheduledDateTime < [DayZone].ToDate
			WHERE		EventKey NOT IN (0,-1)
						and ScheduledDayKey = -1
			) as X
	Where	DayKey not in (0, -1)

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Event_Venue', NULL, 'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Event_Venue', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
GO
PRINT N'Refreshing [Cleanse].[Sp_Cleanse_ALL]...';


GO
EXECUTE sp_refreshsqlmodule N'[Cleanse].[Sp_Cleanse_ALL]';


GO
PRINT N'Refreshing [Control].[ETLDimension]...';


GO
EXECUTE sp_refreshsqlmodule N'[Control].[ETLDimension]';


GO
PRINT N'Refreshing [Utility].[sp_ColdStart]...';


GO
EXECUTE sp_refreshsqlmodule N'[Utility].[sp_ColdStart]';


GO
PRINT N'Refreshing [utility].[sp_ColdStart_TestData]...';


GO
EXECUTE sp_refreshsqlmodule N'[utility].[sp_ColdStart_TestData]';


GO
/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

/*Version Info */
Insert into [$(Dimensional)].[Control].[Version] values ('$(VersionNumber)', 'Staging', getdate(), SUser_Name())

/* Static Data Inserts*/
Exec Reference.Sp_InitialPopAll

--Synonyms
DROP SYNONYM [Control].[LiveETLController]
GO

CREATE SYNONYM [Control].[LiveETLController] FOR [EQ3WNPRDBDW01].[Staging].[Control].[ETLController]
GO

--Operators
Begin Try EXEC msdb.dbo.sp_delete_operator @name=N'BI Notify' End Try Begin Catch End Catch
GO
EXEC msdb.dbo.sp_add_operator @name=N'BI Notify', 
		@enabled=1, 
		@weekday_pager_start_time=90000, 
		@weekday_pager_end_time=180000, 
		@saturday_pager_start_time=90000, 
		@saturday_pager_end_time=180000, 
		@sunday_pager_start_time=90000, 
		@sunday_pager_end_time=180000, 
		@pager_days=0, 
		@email_address=N'BI.Service@Williamhill.com.au', 
		@category_name=N'[Uncategorized]'
GO


Begin Try EXEC msdb.dbo.sp_delete_operator @name=N'BI Alert' End Try Begin Catch End Catch
GO
EXEC msdb.dbo.sp_add_operator @name=N'BI Alert', 
		@enabled=1, 
		@weekday_pager_start_time=90000, 
		@weekday_pager_end_time=180000, 
		@saturday_pager_start_time=90000, 
		@saturday_pager_end_time=180000, 
		@sunday_pager_start_time=90000, 
		@sunday_pager_end_time=180000, 
		@pager_days=0, 
		@email_address=N'BI.Alert@Williamhill.com.au', 
		@category_name=N'[Uncategorized]'
GO



use [$(Staging)]
GO

GO
PRINT N'Reenabling DDL triggers...'
GO
ENABLE TRIGGER [trg_DDLChangeTrigger] ON DATABASE
GO
PRINT N'Update complete.';


GO

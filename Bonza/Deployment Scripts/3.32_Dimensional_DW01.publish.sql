﻿/*
Deployment script for Dimensional

This code was generated by a tool.
Changes to this file may cause incorrect behavior and will be lost if
the code is regenerated.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar VersionNumber "3.32"
:setvar DatabaseName "Dimensional"
:setvar DefaultFilePrefix "Dimensional"
:setvar DefaultDataPath "E:\SQLData\"
:setvar DefaultLogPath "I:\SQLLogs\"

GO
:on error exit
GO
/*
Detect SQLCMD mode and disable script execution if SQLCMD mode is not supported.
To re-enable the script after enabling SQLCMD mode, execute the following:
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'SQLCMD mode must be enabled to successfully execute this script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
PRINT N'Disabling all DDL triggers...'
GO
DISABLE TRIGGER ALL ON DATABASE
GO

PRINT N'Starting rebuilding table [Dimension].[Market]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [Dimension].[tmp_ms_xx_Market] (
    [MarketKey]              INT           IDENTITY (1, 1) NOT NULL,
    [Source]                 CHAR (3)      NOT NULL,
    [MarketId]               INT           NOT NULL,
    [MarketName]             VARCHAR (255) NOT NULL,
    [OfferedDateTime]        DATETIME      NULL,
    [OfferedDayKey]          INT           NULL,
    [OfferedLocalDateTime]   DATETIME      NULL,
    [SuspendedDateTime]      DATETIME      NULL,
    [SuspendedDayKey]        INT           NULL,
    [SuspendedLocalDateTime] DATETIME      NULL,
    [SettledDateTime]        DATETIME      NULL,
    [SettledDayKey]          INT           NULL,
    [SettledLocalDateTime]   DATETIME      NULL,
    [MarketType]             VARCHAR (255) NULL,
    [MarketGroup]            VARCHAR (255) NULL,
    [LiveMarket]             VARCHAR (3)   NULL,
    [FutureMarket]           VARCHAR (3)   NULL,
    [MaxWinnings]            INT           NULL,
    [MaxStake]               INT           NULL,
    [LevyRate]               VARCHAR (30)  NULL,
    [Sport]                  VARCHAR (50)  NULL,
    [FromDate]               DATETIME2 (3) NOT NULL,
    [FirstDate]              DATETIME2 (3) NOT NULL,
    [CreatedDate]            DATETIME2 (7) NOT NULL,
    [CreatedBatchKey]        INT           NOT NULL,
    [CreatedBy]              VARCHAR (100) NOT NULL,
    [ModifiedDate]           DATETIME2 (7) NOT NULL,
    [ModifiedBatchKey]       INT           NOT NULL,
    [ModifiedBy]             VARCHAR (100) NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_CI-PK-MarketKey] PRIMARY KEY CLUSTERED ([MarketKey] ASC) WITH (DATA_COMPRESSION = ROW)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [Dimension].[Market])
    BEGIN
        SET IDENTITY_INSERT [Dimension].[tmp_ms_xx_Market] ON;
        INSERT INTO [Dimension].[tmp_ms_xx_Market] ([MarketKey], [Source], [MarketId], [MarketName], [OfferedDateTime], [OfferedDayKey], [OfferedLocalDateTime], [SuspendedDateTime], [SuspendedDayKey], [SuspendedLocalDateTime], [SettledDateTime], [SettledDayKey], [SettledLocalDateTime], [MarketType], [MarketGroup], [LevyRate], [Sport], [FromDate], [FirstDate], [CreatedDate], [CreatedBatchKey], [CreatedBy], [ModifiedDate], [ModifiedBatchKey], [ModifiedBy])
        SELECT   [MarketKey],
                 [Source],
                 [MarketId],
                 [MarketName],
                 [OfferedDateTime],
                 [OfferedDayKey],
                 [OfferedLocalDateTime],
                 [SuspendedDateTime],
                 [SuspendedDayKey],
                 [SuspendedLocalDateTime],
                 [SettledDateTime],
                 [SettledDayKey],
                 [SettledLocalDateTime],
                 [MarketType],
                 [MarketGroup],
                 [LevyRate],
                 [Sport],
                 [FromDate],
                 [FirstDate],
                 [CreatedDate],
                 [CreatedBatchKey],
                 [CreatedBy],
                 [ModifiedDate],
                 [ModifiedBatchKey],
                 [ModifiedBy]
        FROM     [Dimension].[Market]
        ORDER BY [MarketKey] ASC;
        SET IDENTITY_INSERT [Dimension].[tmp_ms_xx_Market] OFF;
    END

DROP TABLE [Dimension].[Market];

EXECUTE sp_rename N'[Dimension].[tmp_ms_xx_Market]', N'Market';

EXECUTE sp_rename N'[Dimension].[tmp_ms_xx_constraint_CI-PK-MarketKey]', N'CI-PK-MarketKey', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
PRINT N'Creating [Dimension].[Market].[NI-NK-Market]...';


GO
CREATE UNIQUE NONCLUSTERED INDEX [NI-NK-Market]
    ON [Dimension].[Market]([MarketId] ASC, [Source] ASC)
    INCLUDE([MarketKey]) WITH (DATA_COMPRESSION = ROW);


GO
PRINT N'Creating [Dimension].[Market].[NI_Market_ModifiedBatchKey]...';


GO
CREATE NONCLUSTERED INDEX [NI_Market_ModifiedBatchKey]
    ON [Dimension].[Market]([ModifiedBatchKey] ASC)
    INCLUDE([MarketId]) WITH (DATA_COMPRESSION = ROW);


GO
PRINT N'Altering [Dimension].[sp_InitialPopMarket]...';


GO
ALTER Procedure [Dimension].[sp_InitialPopMarket]

as

IF not exists (Select [MarketKey] from [Dimension].[Market])
Begin
Begin Transaction
SET IDENTITY_INSERT [Dimension].[Market] ON 

INSERT [Dimension].[Market]  ([MarketKey],[Source],[MarketId],[MarketName],[MarketType],[MarketGroup], [LiveMarket], [FutureMarket], [MaxWinnings], [MaxStake], [LevyRate],[Sport],[FromDate],[FirstDate],[CreatedDate],[CreatedBatchKey],[CreatedBy],[ModifiedDate],[ModifiedBatchKey],[ModifiedBy]) 
SELECT -1, 'UNK', -1, 'Unknown', 'UNK', 'UNK', 'UNK', 'UNK', -1, -1, 'UNK', 'UNK', GETDATE(), GETDATE(), GETDATE(), 0, 'sp_InitialPopMarket', GETDATE(), 0, 'sp_InitialPopMarket'
UNION ALL
SELECT 0, 'N/A', 0, 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 0, 0, 'N/A', 'N/A', GETDATE(), GETDATE(), GETDATE(), 0, 'sp_InitialPopMarket', GETDATE(), 0, 'sp_InitialPopMarket'

SET IDENTITY_INSERT [Dimension].[Market] OFF
IF @@ERROR = 0
Begin
	Commit Transaction
End
Else
Begin
	Rollback Transaction
End
End
GO
PRINT N'Creating Permission...';


GO
GRANT SELECT
    ON SCHEMA::[Control] TO [Reporting];


GO
PRINT N'Refreshing [Utility].[Sp_GetRowcountsByBatch]...';


GO
EXECUTE sp_refreshsqlmodule N'[Utility].[Sp_GetRowcountsByBatch]';


GO
PRINT N'Refreshing [Dimension].[sp_InitialPopALL]...';


GO
EXECUTE sp_refreshsqlmodule N'[Dimension].[sp_InitialPopALL]';


GO
/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

/*Version Info */
Insert into [Control].[Version] values ('$(VersionNumber)', 'Dimensional', getdate(), SUser_Name())

/* Static Dimension Inserts */
Exec Dimension.sp_InitialPopALL


GO

GO
PRINT N'Reenabling DDL triggers...'
GO
ENABLE TRIGGER [trg_DDLChangeTrigger] ON DATABASE
GO
PRINT N'Update complete.';


GO

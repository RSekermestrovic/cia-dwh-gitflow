Update S
	Set S.Promotions = T.Promotions,
		S.BonusWinnings = T.BonusWinnings
FROM [Dimensional].Fact.[KPI] S
INNER JOIN staging.stage.IncorrectBonus T
ON	S.ContractKey=T.ContractKey AND S.DayKey=T.DayKey AND S.AccountKey=T.AccountKey AND S.BetTypeKey=T.BetTypeKey and S.AccountStatusKey=T.AccountStatusKey
	AND S.LegTypeKey=T.LegTypeKey AND S.MarketKey=T.MarketKey AND S.WalletKey=T.WalletKey AND S.ClassKey=T.ClassKey AND S.ChannelKey = T.ChannelKey
	AND S.CampaignKey = T.CampaignKey AND S.UserKey = T.UserKey AND S.InstrumentKey = T.InstrumentKey AND S.EventKey = T.EventKey




INSERT INTO [Fact].[KPI]
Select s.[ContractKey]
      ,s.[DayKey]
      ,s.[AccountKey]
      ,s.[AccountStatusKey]
      ,s.[BetTypeKey]
      ,s.[LegTypeKey]
      ,s.[ChannelKey]
      ,s.[CampaignKey]
      ,s.[MarketKey]
      ,s.[EventKey]
      ,s.[WalletKey]
      ,s.[ClassKey]
      ,s.[UserKey]
      ,s.[InstrumentKey]
      ,0 as [InPlayKey]
      ,NULL AS [AccountOpened]
      ,NULL AS [FirstDeposit]
      ,NULL AS [FirstBet]
      ,0 AS [DepositsRequested]
      ,0 AS [DepositsCompleted]
      ,s.[Promotions]
      ,0 AS [Transfers]
      ,NULL AS [BetsPlaced]
      ,NULL AS [ContractsPlaced]
      ,NULL AS [BettorsPlaced]
      ,NULL AS [PlayDaysPlaced]
      ,NULL AS [PlayFiscalWeeksPlaced]
      ,NULL AS [PlayCalenderWeeksPlaced]
      ,NULL AS [PlayFiscalMonthsPlaced]
      ,NULL AS [PlayCalenderMonthsPlaced]
      ,0 AS [Stakes]
      ,0 AS [Winnings]
      ,0 AS [WithdrawalsRequested]
      ,0 AS [WithdrawalsCompleted]
      ,s.[Adjustments]
      ,NULL AS [BetsResulted]
      ,NULL AS [ContractsResulted]
      ,NULL AS [BettorsResulted]
      ,NULL AS [PlayDaysResulted]
      ,NULL AS [PlayFiscalWeeksResulted]
      ,NULL AS [PlayCalenderWeeksResulted]
      ,NULL AS [PlayFiscalMonthsResulted]
      ,NULL AS [PlayCalenderMonthsResulted]
      ,0 AS [Turnover]
      ,0 AS [GrossWin]
      ,0 AS [NetRevenue]
      ,0 AS [NetRevenueAdjustment]
      ,s.[BonusWinnings]
      ,0 AS [Betback]
      ,0 AS [GrossWinAdjustment]
      ,GetDate() AS [CreatedDate]
      ,0 AS [CreatedBatchKey]
      ,'Manual Update' AS [CreatedBy]
      ,GetDate() AS [ModifiedDate]
      ,0 AS [ModifiedBatchKey]
      ,'Manual Update' AS [ModifiedBy]
FROM staging.stage.IncorrectBonus S
LEFT OUTER JOIN [Dimensional].Fact.[KPI]  T
ON	S.ContractKey=T.ContractKey AND S.DayKey=T.DayKey AND S.AccountKey=T.AccountKey AND S.BetTypeKey=T.BetTypeKey and S.AccountStatusKey=T.AccountStatusKey
	AND S.LegTypeKey=T.LegTypeKey AND S.MarketKey=T.MarketKey AND S.WalletKey=T.WalletKey AND S.ClassKey=T.ClassKey AND S.ChannelKey = T.ChannelKey
	AND S.CampaignKey = T.CampaignKey AND S.UserKey = T.UserKey AND S.InstrumentKey = T.InstrumentKey AND S.EventKey = T.EventKey
	Where T.AccountKey IS NULL --and S.DayKey IN (2015031200,2015031201)
-----------------------------------------------------------
------------------INSERT MISSING KPIs----------------------
INSERT INTO Dimensional.fact.KPI
Select a.* 
From [Stage].[MissingKPIAccounts] a
LEFT OUTER JOIN Dimensional.fact.KPI b on a.ContractKey = b.ContractKey and a.DayKey = b.DayKey and a.AccountKey = b.AccountKey
Where b.AccountKey is null

-----------------------------------------------------------
-----------------------------------------------------------
------UPDATE FROM DATES
Update b
Set b.Fromdate = b.AccountOpeneddate
from [Stage].[MissingLedgersOpensInKPI] a
INNER JOIN Stage.Account b on a.LedgerId = b.LedgerId and a.BatchKey = b.BatchKey

-----------------------------------------------------------
-----------------------------------------------------------
-----REMOVE INCORRECT FTBs
Update S
Set S.FirstBet = null
FROM [Dimensional].Fact.[KPI] S
INNER JOIN Stage.IncorrectFTB T
ON	S.ContractKey=T.ContractKey AND S.DayKey=T.DayKey AND S.AccountKey=T.AccountKey AND S.BetTypeKey=T.BetTypeKey and S.AccountStatusKey=T.AccountStatusKey
	AND S.LegTypeKey=T.LegTypeKey AND S.MarketKey=T.MarketKey AND S.WalletKey=T.WalletKey AND S.ClassKey=T.ClassKey AND S.ChannelKey = T.ChannelKey
	AND S.CampaignKey = T.CampaignKey AND S.UserKey = T.UserKey AND S.InstrumentKey = T.InstrumentKey AND S.EventKey = T.EventKey


-----------------------------------------------------------
-----------------------------------------------------------
-----REMOVE INCORRECT FTDs
--FTDs seems ok...



-----------------------------------------------------------
-----------------------------------------------------------
-----Update Withdrawals

Update S
	Set S.WithdrawalsCompleted = CASE WHEN T.AccountKey IS NOT NULL THEN T.WithdrawalsCompleted ELSE 0 END
FROM [Dimensional].Fact.[KPI] S
LEFT OUTER JOIN staging.stage.IncorrectWithdrawsCompleted T
ON	S.ContractKey=T.ContractKey AND S.DayKey=T.DayKey AND S.AccountKey=T.AccountKey AND S.BetTypeKey=T.BetTypeKey and S.AccountStatusKey=T.AccountStatusKey
	AND S.LegTypeKey=T.LegTypeKey AND S.MarketKey=T.MarketKey AND S.WalletKey=T.WalletKey AND S.ClassKey=T.ClassKey AND S.ChannelKey = T.ChannelKey
	AND S.CampaignKey = T.CampaignKey AND S.UserKey = T.UserKey AND S.InstrumentKey = T.InstrumentKey AND S.EventKey = T.EventKey
	Where S.WithdrawalsCompleted <> CASE WHEN T.AccountKey IS NOT NULL THEN T.WithdrawalsCompleted ELSE 0 END
﻿/*
Deployment script for Staging

This code was generated by a tool.
Changes to this file may cause incorrect behavior and will be lost if
the code is regenerated.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar Acquisition "Acquisition"
:setvar Dimensional "Dimensional"
:setvar JobIntervalSeconds "60"
:setvar JobPrefix "DW_"
:setvar ServerName "EQ3WNPRDBDW01"
:setvar VersionNumber "3.38"
:setvar DatabaseName "Staging"
:setvar DefaultFilePrefix "Staging"
:setvar DefaultDataPath "E:\SQLData\"
:setvar DefaultLogPath "I:\SQLLogs\"

GO
:on error exit
GO
/*
Detect SQLCMD mode and disable script execution if SQLCMD mode is not supported.
To re-enable the script after enabling SQLCMD mode, execute the following:
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'SQLCMD mode must be enabled to successfully execute this script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
/*
 Pre-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be executed before the build script.	
 Use SQLCMD syntax to include a file in the pre-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the pre-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/
SET NOCOUNT ON
--Plan Guides
--DROP all "BULK" Plan Guides referencing stored procedures in case any of the stored procedures is being redeployed
BEGIN
	PRINT N'Dropping Plan Guides referencing Stored Procedures...';
	
	DECLARE	@i int,
			@SQL varchar(MAX)

	BEGIN TRY DROP TABLE #PlanGuides END TRY BEGIN CATCH END CATCH

	SELECT	ROW_NUMBER() OVER (ORDER BY name DESC) i,
			'sp_control_plan_guide ''DROP'', ''' + name + '''' sql
	INTO	#PlanGuides
	FROM	sys.plan_guides
	WHERE	scope_type_desc = 'OBJECT'
			AND name LIKE '%BULK%'

	SET @i = @@ROWCOUNT

	WHILE @i > 0
		BEGIN
			SELECT	@SQL = sql FROM	#PlanGuides	WHERE i = @i
			EXEC(@SQL)
			SET @i = @i - 1	
		END
END
GO


SET NOCOUNT OFF
GO

GO
PRINT N'Dropping [Control].[LiveETLController]...';


GO
DROP SYNONYM [Control].[LiveETLController];


GO
PRINT N'Creating [Control].[LiveETLController]...';


GO
CREATE SYNONYM [Control].[LiveETLController] FOR [Control].ETLController;


GO
PRINT N'Altering [Intrabet].[Sp_Instrument_Paypal]...';


GO
ALTER PROC [Intrabet].[Sp_Instrument_Paypal]
(
	@BatchKey	int,
	@FromDate	datetime2(3),
	@ToDate		datetime2(3),
	@RowsProcessed	int = 0 OUTPUT
)
AS
BEGIN
	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
	DECLARE @Reload varchar(6) = CASE @FromDate WHEN '1900-01-01' THEN 'RELOAD' ELSE NULL END
	DECLARE @RowCount int
	DECLARE @i int
	
	SET TRANSACTION ISOLATION LEVEL SNAPSHOT;

BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------ Initialize Temp Tables ---------------------
	IF OBJECT_ID('TempDB..#Candidates','U') IS NOT NULL BEGIN DROP TABLE #Candidates END;

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Get Paypals','Start', @Reload

	BEGIN TRANSACTION Instrument;
	
	SELECT ID,trnType,MAX(TimestampPPL) TimestampPPL,MAX(fromDate) fromDate  
	INTO #Candidates
	FROM (

			----------- tblPaypalGEc --------------------------
			SELECT	ID,trnType,FromDate,TimeStamp as TimestampPPL
			FROM (	
					SELECT PayerID AS ID,'PPL' trnType,fromDate,TimeStamp 
					FROM [$(Acquisition)].Intrabet.tblPaypalGEc pp
					WHERE FromDate > @FromDate AND FromDate <= @ToDate AND ToDate > @FromDate AND PayerID is not null and PayerID <>'n/a'		
			UNION ALL
					SELECT PayerID AS ID,'PPL' trnType,ToDate ,TimeStamp
					FROM [$(Acquisition)].Intrabet.tblPaypalGEc 
					WHERE	ToDate > @FromDate AND ToDate <= @ToDate AND PayerID is not null and PayerID <>'n/a'
			) x
			GROUP BY ID,trnType,FromDate,TimeStamp
			HAVING COUNT(*) <> 2

		 ) x
	GROUP BY ID,trnType
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'InsertIntoStaging','Start', @Reload, @RowsProcessed = @@ROWCOUNT

	INSERT	into Stage.Instrument (BatchKey,InstrumentId,Source,AccountNumber,SafeAccountNumber,AccountName,BSB,ExpiryDate,Verified,VerifyAmount,InstrumentType,ProviderName,FromDate)
	SELECT	
			@BatchKey																			AS BatchKey, 
			C.id																				AS instrumentID,
			ISNULL(C.trnType, 'Unknown')														AS Source, 
			ISNULL(Pg.Token,'Unknown')															AS AccountNumber,
			ISNULL(Pg.Token,'Unknown')															AS SafeAccountNumber,
			ISNULL(Pg.Payer,'Unknown')															AS AccountName,	 		
			'N/A'																				AS BSB,
			NULL																				AS ExpiryDate,
			Case When pg.Payerstatus='verified' 
			     Then 'Yes'
				 Else 'No' End																	AS Verified,
			NULL																				AS VerifiedAmount,
			'PayPal'																			AS InstrumentType,
			'N/A'																				AS ProviderName,
			c.FromDate																			AS FromDate		
	FROM #Candidates C  with (nolock)
	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblPayPalGEC Pg WITH (NOLOCK) ON 
					C.ID COLLATE SQL_Latin1_General_CP1_CI_AS=Cast(Pg.PayerID  as varchar) 
					and c.TimestampPPL=Pg.[Timestamp]
					AND Pg.FromDate <= c.FromDate AND Pg.ToDate > C.FromDate

	OPTION (RECOMPILE, FORCE ORDER, LOOP JOIN)

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount
								
	COMMIT TRANSACTION Instrument;

	EXEC [Control].Sp_Log @BatchKey, @Me, NULL, 'Success', @RowsProcessed = @RowsProcessed

	UPDATE [Control].[ConfigurationItems] Set Value = 'N' Where ConfigurationItem = 'Intrabet.Sp_Instrument_Paypal'

END TRY
BEGIN CATCH

	ROLLBACK TRANSACTION Instrument;
	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Instrument_Paypal', NULL, 'Failed', @ErrorMessage;
	RAISERROR(@ErrorMessage,16,1);
	THROW;

END CATCH
																					   																					
END
GO
PRINT N'Altering [trg_DDLChangeTrigger]...';


GO
ENABLE TRIGGER [trg_DDLChangeTrigger]
    ON DATABASE;


GO
/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

SET NOCOUNT ON

--Plan Guides
PRINT N'Adding Plan Guides...';
EXEC sp_create_plan_guide
@name = N'BULK:Intrabet-sp_Account:CombinedChanges(ThresholdDays=10)',
@stmt = N'
	SELECT	AccountId, ClientId, AccountNumber, MAX(FirstDate) FirstDate, MAX(FromDate) FromDate, MAX(NewLedger) NewLedger
	INTO	#Candidates
	FROM	(	SELECT	AccountId, ClientId, AccountNumber, FirstDate, FromDate, NewLedger
				FROM	#Accounts
				UNION ALL
				SELECT	a.AccountId, a.ClientId, a.AccountNumber, c.FirstDate, c.FromDate, 0 NewLedger
				FROM	#Clients c
						INNER JOIN [$(Acquisition)].IntraBet.tblAccounts a ON (a.ClientID = c.ClientID AND a.FromDate <= c.FromDate AND a.ToDate > c.Fromdate)
			) x
	GROUP BY AccountId, ClientId, AccountNumber
	OPTION (RECOMPILE, TABLE HINT(a, FORCESEEK))
', 
@type = N'OBJECT',
@module_or_batch = 'Intrabet.sp_Account',
@params = NULL,
@hints = N'OPTION (RECOMPILE)'

EXEC sp_create_plan_guide
@name = N'BULK:Intrabet-sp_Account:MissingOpenDate(ThresholdDays=10)',
@stmt = N'
	SELECT	c.AccountId, c.ClientID, c.AccountNumber, c.FromDate
	INTO	#MissingOpenDate
	FROM	#Candidates c
			LEFT JOIN [$(Acquisition)].[IntraBet].tblClientInfo i ON i.ClientID = c.ClientID AND i.FromDate <= c.FromDate AND i.ToDate > c.FromDate
	WHERE	c.AccountNumber > 1 OR i.StartDate = CONVERT(datetime,''1900-01-01'') OR i.StartDate IS NULL
	OPTION (RECOMPILE, TABLE HINT(i, FORCESEEK))
', 
@type = N'OBJECT',
@module_or_batch = 'Intrabet.sp_Account',
@params = NULL,
@hints = N'OPTION (RECOMPILE)'

EXEC sp_create_plan_guide
@name = N'BULK:Intrabet-sp_Account:DerivedOpenDate(ThresholdDays=10)',
@stmt = N'
	SELECT	c.AccountId, MIN(ISNULL(t.TransactionDate,a.FromDate)) AccountOpenedDate, CASE COUNT(t.ClientId) WHEN 0 THEN ''Account'' ELSE ''Transaction'' END Origin
	INTO	#DerivedOpenDate
	FROM	#MissingOpenDate c
			INNER JOIN [$(Acquisition)].IntraBet.tblAccounts a ON (a.AccountID = c.AccountID)
			LEFT JOIN [$(Acquisition)].IntraBet.tblTransactions t ON (t.ClientID = c.ClientID AND t.AccountNumber = c.AccountNumber AND t.FromDate = CONVERT(datetime2(3),''2013-12-19'') AND a.FromDate = CONVERT(datetime2(3),''2013-12-19''))
	GROUP BY c.AccountID
	OPTION (RECOMPILE, TABLE HINT(a, FORCESEEK), TABLE HINT(t, FORCESEEK))
', 
@type = N'OBJECT',
@module_or_batch = 'Intrabet.sp_Account',
@params = NULL,
@hints = N'OPTION (RECOMPILE)'

EXEC sp_create_plan_guide
@name = N'BULK:Intrabet-sp_Account:InsertIntoStaging(ThresholdDays=10)',
@stmt = N'
	INSERT	Stage.Account
		(	BatchKey, 
			LedgerID, LedgerSource, LedgerSequenceNumber, LegacyLedgerID, LedgerTypeID, LedgerType, LedgerOpenedDate, LedgerOpenedDayText, LedgerOpenedDateOrigin,
			AccountID, AccountSource, PIN, UserName, ExactTargetID, AccountFlags,
			AccountOpenedChannelId, SourceBTag2, SourceTrafficSource, SourceRefURL, SourceCampaignId, SourceKeywords,
			StatementMethod, StatementFrequency,
			AccountTitle, AccountSurname, AccountFirstName, AccountMiddleName, AccountGender, AccountDOB,
			HasHomeAddress, HomeAddress1, HomeAddress2, HomeSuburb, HomeStateCode, HomeState, HomePostCode, HomeCountryCode, HomeCountry,
			HomePhone, HomeMobile, HomeFax, HomeEmail, HomeAlternateEmail, 
			HasPostalAddress, PostalAddress1, PostalAddress2, PostalSuburb, PostalStateCode, PostalState, PostalPostCode, PostalCountryCode, PostalCountry,
			PostalPhone, PostalMobile, PostalFax, PostalEmail, PostalAlternateEmail,
			HasBusinessAddress, BusinessAddress1, BusinessAddress2, BusinessSuburb, BusinessStateCode, BusinessState, BusinessPostCode, BusinessCountryCode, BusinessCountry,
			BusinessPhone, BusinessMobile, BusinessFax, BusinessEmail, BusinessAlternateEmail,
			HasOtherAddress, OtherAddress1, OtherAddress2, OtherSuburb, OtherStateCode, OtherState, OtherPostCode, OtherCountryCode, OtherCountry,
			OtherPhone, OtherMobile, OtherFax, OtherEmail, OtherAlternateEmail,
			FirstDate, FromDate
		)
	SELECT	@BatchKey BatchKey,
			x.AccountID LedgerId,
			''IAA'' as LedgerSource,
			x.AccountNumber LedgerSequenceNumber,
			CASE ISNULL(C.CB_Client_key,0) WHEN 0 THEN x.AccountID ELSE c.CB_Client_key END LegacyLedgerID,
			a.Ledger LedgerTypeID,
			ISNULL(l.LedgerDescription,''Unknown'') LedgerType,
			ISNULL(dd.AccountOpenedDate, ci.StartDate) LedgerOpenedDate,
			CONVERT(char(10),CONVERT(date,ISNULL(dd.AccountOpenedDate, ci.StartDate))) LedgerOpenedDayText,
			ISNULL(dd.Origin, ''Client'') LedgerOpenedDateOrigin,
			x.ClientID AccountID,
			''IAC'' AccountSource,
			c.PIN,
			c.Username,
			ISNULL(CONVERT(varchar(36), sc.exactTargetID),''N/A'') ExactTargetID,
			c.ClientType AccountFlags,
			ISNULL(ci.Channel, -1) AccountOpenedChannelId,
			cai.BTag2 SourceBTag2,
			cai.TrafficSource SourceTrafficSource,
			cai.RefURL SourceRefURL,
			cai.CampaignID SourceCampaignID,
			cai.Keywords SourceKeywords,
			ISNULL(sm.[Description], ''Unknown'') StatementMethod,
			ISNULL(sf.[Description], ''Unknown'') StatementFrequency,
			c.Title AccountTitle,
			c.Surname AccountSurname,
			c.FirstName AccountFirstName,
			c.MiddleName AccountMiddleName,
			c.Gender AccountGender,
			c.DOB AccountDOB,
			CASE WHEN ha.AddressType IS NOT NULL THEN 1 ELSE 0 END HasHomeAddress,
			ha.Address1 HomeAddress1,
			ha.Address2 HomeAddress2,
			ha.Suburb HomeSuburb,
			ha.State HomeStateCode,
			hs.StateName HomeState,
			ha.PostCode HomePostCode,
			hc.CountryAbbr HomeCountryCode,
			hc.CountryName HomeCountry,
			ha.Phone HomePhone,
			ha.Mobile HomeMobile,
			ha.Fax HomeFax,
			ha.Email HomeEmail,
			ha.altEmail HomeAlternateEmail,
			CASE WHEN pa.AddressType IS NOT NULL THEN 1 ELSE 0 END HasPostalAddress,
			pa.Address1 PostalAddress1,
			pa.Address2 PostalAddress2,
			pa.Suburb PostalSuburb,
			pa.State PostalStateCode,
			ps.StateName PostalState,
			pa.PostCode PostalPostCode,
			pc.CountryAbbr PostalCountryCode,
			pc.CountryName PostalCountry,
			pa.Phone PostalPhone,
			pa.Mobile PostalMobile,
			pa.Fax PostalFax,
			pa.Email PostalEmail,
			pa.altEmail PostalAlternateEmail,
			CASE WHEN ba.AddressType IS NOT NULL THEN 1 ELSE 0 END HasBusinessAddress,
			ba.Address1 BusinessAddress1,
			ba.Address2 BusinessAddress2,
			ba.Suburb BusinessSuburb,
			ba.State BusinessStateCode,
			bs.StateName BusinessState,
			ba.PostCode BusinessPostCode,
			bc.CountryAbbr BusinessCountryCode,
			bc.CountryName BusinessCountry,
			ba.Phone BusinessPhone,
			ba.Mobile BusinessMobile,
			ba.Fax BusinessFax,
			ba.Email BusinessEmail,
			ba.altEmail BusinessAlternateEmail,
			CASE WHEN oa.AddressType IS NOT NULL THEN 1 ELSE 0 END HasOtherAddress,
			oa.Address1 OtherAddress1,
			oa.Address2 OtherAddress2,
			oa.Suburb OtherSuburb,
			oa.State OtherStateCode,
			os.StateName OtherState,
			oa.PostCode OtherPostCode,
			oc.CountryAbbr OtherCountryCode,
			oc.CountryName OtherCountry,
			oa.Phone OtherPhone,
			oa.Mobile OtherMobile,
			oa.Fax OtherFax,
			oa.Email OtherEmail,
			oa.altEmail OtherAlternateEmail,
			ISNULL(l1.CorrectedDate,x.FirstDate) FirstDate,
			ISNULL(l2.CorrectedDate,x.FromDate) FromDate
	FROM	#Candidates x
			INNER JOIN [$(Acquisition)].IntraBet.tblAccounts a ON a.AccountID = x.AccountID AND a.FromDate <= x.FromDate AND a.ToDate > x.FromDate
			LEFT JOIN #DerivedOpenDate dd ON dd.AccountID = x.AccountID
			LEFT JOIN [$(Acquisition)].IntraBet.tblLULedgers l ON l.Ledger = a.Ledger AND l.FromDate <= x.FromDate AND l.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].IntraBet.tblClients c ON c.ClientID = x.ClientID AND c.FromDate <= x.FromDate AND c.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].IntraBet.tblClientInfo ci ON ci.ClientID = x.ClientID AND ci.FromDate <= x.FromDate AND ci.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].IntraBet.tblSubscriptionCentre sc ON sc.clientID = x.ClientID AND sc.FromDate <= x.FromDate AND sc.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].IntraBet.tblClientAffiliateInfo cai ON cai.clientID = x.ClientID AND cai.FromDate <= x.FromDate AND cai.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].IntraBet.tblLUStatementMethods sm ON sm.StatementMethod = ci.StatementMethod AND sm.FromDate <= x.FromDate AND sm.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].IntraBet.tblLUStatementFrequency sf ON sf.StatementFrequency = ci.StatementFrequency AND sf.FromDate <= x.FromDate AND sf.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].[IntraBet].tblClientAddresses ha ON ha.ClientID = x.ClientID AND ha.FromDate <= x.FromDate AND ha.ToDate > x.FromDate AND ha.AddressType=1
			LEFT JOIN [$(Acquisition)].[IntraBet].tblLUStates hs ON hs.Code = ha.StateId AND hs.FromDate <= x.FromDate AND hs.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].[IntraBet].tblLUCountry hc ON hc.Country = ha.Country AND hc.FromDate <= x.FromDate AND hc.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].[IntraBet].tblClientAddresses pa ON pa.ClientID = x.ClientID AND pa.FromDate <= x.FromDate AND pa.ToDate > x.FromDate AND pa.AddressType=2
			LEFT JOIN [$(Acquisition)].[IntraBet].tblLUStates ps ON ps.Code = pa.StateId AND ps.FromDate <= x.FromDate AND ps.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].[IntraBet].tblLUCountry pc ON pc.Country = pa.Country AND pc.FromDate <= x.FromDate AND pc.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].[IntraBet].tblClientAddresses ba ON ba.ClientID = x.ClientID AND ba.FromDate <= x.FromDate AND ba.ToDate > x.FromDate AND ba.AddressType=3
			LEFT JOIN [$(Acquisition)].[IntraBet].tblLUStates bs ON bs.Code = ba.StateId AND bs.FromDate <= x.FromDate AND bs.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].[IntraBet].tblLUCountry bc ON bc.Country = ba.Country AND bc.FromDate <= x.FromDate AND bc.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].[IntraBet].tblClientAddresses oa ON oa.ClientID = x.ClientID AND oa.FromDate <= x.FromDate AND oa.ToDate > x.FromDate AND oa.AddressType=4
			LEFT JOIN [$(Acquisition)].[IntraBet].tblLUStates os ON os.Code = oa.StateId AND os.FromDate <= x.FromDate AND os.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].[IntraBet].tblLUCountry oc ON oc.Country = oa.Country AND oc.FromDate <= x.FromDate AND oc.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].Intrabet.Latency  l1 WITH(FORCESEEK)  on l1.OriginalDate = x.FirstDate --AND l1.OriginalDate >= @FromDate
			LEFT JOIN [$(Acquisition)].Intrabet.Latency  l2 WITH(FORCESEEK)  on l2.OriginalDate = x.FromDate --AND l2.OriginalDate >= @FromDate
	OPTION (RECOMPILE, FORCE ORDER, LOOP JOIN) --TABLE HINT(c, FORCESEEK), TABLE HINT(ci, FORCESEEK), TABLE HINT(cai, FORCESEEK), TABLE HINT(sc, FORCESEEK), TABLE HINT(ha, FORCESEEK), TABLE HINT(pa, FORCESEEK), TABLE HINT(ba, FORCESEEK), TABLE HINT(oa, FORCESEEK))
', 
@type = N'OBJECT',
@module_or_batch = 'Intrabet.sp_Account',
@params = NULL,
@hints = N'OPTION (RECOMPILE)'



EXEC sp_create_plan_guide
@name = N'BULK:Intrabet-sp_Event:InsertIntoStaging(ThresholdDays=10)',
@stmt = N'
	INSERT	into Stage.Event (BatchKey, Source, SourceEventId, SourceEvent, SourceEventType, ParentEventId, ParentEvent, 
							GrandparentEventId, GrandparentEvent, GreatGrandparentEventId, GreatGrandparentEvent,
							SourceEventDate, ResultedDateTime, SourceRaceNumber, SourceVenueId, SourceVenue, SourceVenueType,SourceVenueEventType, SourceVenueEventTypeName, SourceVenueStateCode,
							SourceVenueState, SourceVenueCountryCode, SourceVenueCountry, ClassID, ClassSource, SourceSubSportType
							, SourceRaceGroup, SourceRaceClass, SourceHybridPricingTemplate, SourcePrizePool, SourceWeightType, SourceDistance, SourceEventAbandoned, EventAbandonedDateTime, SourceLiveStreamPerformId, SourceTrackCondition, SourceEventWeather, FromDate)
	SELECT	@BatchKey BatchKey, 
			''IEI'' Source, 
			e.EventId SourceEventId, 
			e.EventName SourceEvent, 
			et.description SourceEventType,
			ep.EventId SourceParentEventId, 
			ep.EventName SourceParentEvent, 
			egp.EventId SourceGrandparentEventId, 
			egp.EventName SourceGrandparentEvent, 
			eggp.EventId SourceGreatGrandparentEventId, 
			eggp.EventName SourceGreatGrandparentEvent,
			e.EventDate SourceEventDate, 
			R.ResultedDate ResultedDateTime,
			e.RaceNumber SourceRaceNumber,
			v.VenueID SourceVenueId,
			v.VenueName SourceVenue,
			v.VenueType SourceVenueType, 
			v.EventType SourceVenueEventType,
			et2.Description SourceVenueEventTypeName,
			v.stateCode as SourceVenuestateCode, 
			s.StateName as SourceVenueState,
			v.CountryCode as SourceVenueCountryCode, 
			ct.CountryName as SourceVenueCountry,
			e.EventType ClassID, 
			''IBT'' ClassSource,
			sst.[Description] as SourceSubSportType,
			ri.RaceGroup as SourceRaceGroup,
			ri.RaceClass as SourceRaceClass,
			ri.HybridPricingTemplate as SourceHybridPricingTemplate,
			ri.TotalPrizeMoney as SourcePrizePool,
			ri.WeightType as SourceWeightType,
			ri.Distance as SourceDistance,
			e.IsAbandonedEvent SourceEventAbandoned,
			A.AbandonedDate EventAbandonedDateTime,
			e.LiveStreamPerformId,
			ISNULL(tc.Track,tc2.Track) SourceTrackCondition,
			ISNULL(tc.Weather,tc2.Weather) SourceEventWeather,
			c.FromDate
	FROM	#Candidate c 
			INNER JOIN [$(Acquisition)].IntraBet.tblEvents e  ON (e.EventID = c.EventId AND e.FromDate <= c.FromDate AND e.ToDate > c.FromDate)
			LEFT JOIN [$(Acquisition)].IntraBet.tblEvents ep  ON (ep.EventID = e.MasterEventId AND ep.FromDate <= c.FromDate AND ep.ToDate > c.FromDate)
			LEFT JOIN [$(Acquisition)].IntraBet.tblEvents egp  ON (egp.EventID = ep.MasterEventId AND egp.FromDate <= c.FromDate AND egp.ToDate > c.FromDate)
			LEFT JOIN [$(Acquisition)].IntraBet.tblEvents eggp  ON (eggp.EventID = egp.MasterEventId AND eggp.FromDate <= c.FromDate AND eggp.ToDate > c.FromDate)
			left join [$(Acquisition)].[Intrabet].[tblLUEventTypes] et 
			on et.eventtype = e.eventtype and (et.FromDate<=c.FromDate and et.ToDate>c.FromDate)
			left outer join [$(Acquisition)].[IntraBet].[tblLUVenues] v 
			on v.venueid = e.venueid and (v.FromDate<=c.FromDate and v.ToDate>c.FromDate)
			left outer join [$(Acquisition)].[IntraBet].[tblLUEventTypes] et2 
			on v.EventType = et2.EventType and (et2.FromDate<=v.FromDate and et2.ToDate>v.FromDate)
			left outer join [$(Acquisition)].[IntraBet].[tblLUStates] s 
			on v.StateCode = s.Code and (s.FromDate<=c.FromDate and s.ToDate>c.FromDate)
			left outer join [$(Acquisition)].[IntraBet].[tblLUCountry] ct 
			on v.CountryCode = ct.Country and (ct.FromDate<=c.FromDate and ct.ToDate>c.FromDate)
			LEFT OUTER JOIN [$(Acquisition)].[IntraBet].[tblLUSubSportTypes] sst on e.SportSubType = sst.SportSubType and e.EventType = sst.EventType and sst.ToDate = ''9999-12-31 00:00:00.000''
			LEFT OUTER JOIN #RaceInfo ri on c.EventId = ri.IntrabetID
			LEFT OUTER JOIN #Resulted R on R.EventID=C.EventId
			LEFT OUTER JOIN [$(Acquisition)].IntraBet.tblTrackConditions tc on e.VenueID=tc.VenueID and ISNULL(v.EventType,e.EventType) = tc.EventType and c.FromDate>=tc.FromDate and c.FromDate<tc.ToDate
			LEFT OUTER JOIN [$(Acquisition)].IntraBet.tblTrackConditions tc2 on e.VenueID=tc2.VenueID and c.FromDate>=tc2.FromDate and c.FromDate<tc2.ToDate and tc.VenueID is null
			LEFT JOIN #Abandoned A on c.EventId=A.EventID
where isnull(e.IsMasterEvent,0)<>1
OPTION(RECOMPILE, TABLE HINT(e, FORCESEEK), TABLE HINT(ep, FORCESEEK), TABLE HINT(egp, FORCESEEK), TABLE HINT(eggp, FORCESEEK))
', 
@type = N'OBJECT',
@module_or_batch = 'Intrabet.sp_Event',
@params = NULL,
@hints = N'OPTION (RECOMPILE)'

EXEC sp_create_plan_guide @name = N'[BULK:Intrabet-sp_Instrument_Paypal:InsertIntoStaging(ThresholdDays=10)]', @stmt = N'
	INSERT	into Stage.Instrument (BatchKey,InstrumentId,Source,AccountNumber,SafeAccountNumber,AccountName,BSB,ExpiryDate,Verified,VerifyAmount,InstrumentType,ProviderName,FromDate)
	SELECT	
			@BatchKey																			AS BatchKey, 
			C.id																				AS instrumentID,
			ISNULL(C.trnType, ''Unknown'')														AS Source, 
			ISNULL(Pg.Token,''Unknown'')															AS AccountNumber,
			ISNULL(Pg.Token,''Unknown'')															AS SafeAccountNumber,
			ISNULL(Pg.Payer,''Unknown'')															AS AccountName,	 		
			''N/A''																				AS BSB,
			NULL																				AS ExpiryDate,
			Case When pg.Payerstatus=''verified'' 
			     Then ''Yes''
				 Else ''No'' End																	AS Verified,
			NULL																				AS VerifiedAmount,
			''PayPal''																			AS InstrumentType,
			''N/A''																				AS ProviderName,
			c.FromDate																			AS FromDate		
	FROM #Candidates C  with (nolock)
	LEFT OUTER JOIN [Acquisition].Intrabet.tblPayPalGEC Pg WITH (NOLOCK) ON 
					C.ID COLLATE SQL_Latin1_General_CP1_CI_AS=Cast(Pg.PayerID  as varchar) 
					and c.TimestampPPL=Pg.[Timestamp]
					AND Pg.FromDate <= c.FromDate AND Pg.ToDate > C.FromDate

	OPTION (RECOMPILE, FORCE ORDER, LOOP JOIN)
', @type = N'OBJECT', @module_or_batch = N'[Intrabet].[Sp_Instrument_Paypal]', @hints = N'OPTION (RECOMPILE)'

EXEC sp_create_plan_guide @name = N'[BULK:Intrabet-sp_Instrument_MoneyBookers:InsertIntoStaging(ThresholdDays=10)]', @stmt = N'
	INSERT	into Stage.Instrument (BatchKey,InstrumentId,Source,AccountNumber,SafeAccountNumber,AccountName,BSB,ExpiryDate,Verified,VerifyAmount,InstrumentType,ProviderName,FromDate)
	SELECT	
			@BatchKey																			AS BatchKey, 
			C.id																				AS instrumentID,
			ISNULL(C.TrnType,''Unknown'') 														AS Source, 
			''N/A''																				AS AccountNumber,
			''N/A''																				AS SafeAccountNumber,
			''N/A''																				AS AccountName,		
			''N/A''																				AS BSB,
			 NULL																				AS ExpiryDate,
			''U/K''																				AS Verified,
			NULL																				AS VerifiedAmount,
			''MoneyBookers''																		AS InstrumentType,
			 ''N/A''																				AS ProviderName,
			c.FromDate																			AS FromDate		

	FROM #Candidates C  with (nolock)
	LEFT OUTER JOIN [Acquisition].Intrabet.tblMoneybookersdepositresponse B WITH (NOLOCK)	ON 
					C.ID =Cast(B.ID as varchar) 
					AND B.FromDate <= c.FromDate AND B.ToDate > C.FromDate

	OPTION (RECOMPILE, FORCE ORDER, LOOP JOIN)
', @type = N'OBJECT', @module_or_batch = N'[Intrabet].[Sp_Instrument_MoneyBookers]', @hints = N'OPTION (RECOMPILE)'


EXEC sp_create_plan_guide @name = N'[BULK:Intrabet-sp_Instrument_EFT:InsertIntoStaging(ThresholdDays=10)]', @stmt = N'
	INSERT	into Stage.Instrument (BatchKey,InstrumentId,Source,AccountNumber,SafeAccountNumber,AccountName,BSB,ExpiryDate,Verified,VerifyAmount,InstrumentType,ProviderName,FromDate)
	SELECT	
			@BatchKey																			AS BatchKey, 
			C.id																				AS instrumentID,
			ISNULL(C.TrnType,''Unknown'')															AS Source, 
			ISNULL(BD.AccountNumber,''Unknown'')													AS AccountNumber,
			ISNULL(BD.AccountNumber,''Unknown'')													AS SafeAccountNumber,
			ISNULL(BD.AccountName,''Unknown'')													AS AccountName,		
			ISNULL(bd.BankBsb,''Unknown'')														AS BSB,
			NULL																				AS ExpiryDate,
			''N/A''																				AS Verified,
			NULL																				AS VerifiedAmount,
			ISNULL(C.TrnType,''Unknown'')															AS InstrumentType,
			ISNULL(bd.BankName,''Unknown'')														AS ProviderName,
			c.FromDate																			AS FromDate	
				
	FROM #Candidates C  with (nolock)
	LEFT OUTER JOIN [Acquisition].Intrabet.tblClientBankDetails BD WITH (NOLOCK) ON 					
					C.ID =Cast(Bd.BankID as varchar)
					AND BD.FromDate <= c.FromDate AND BD.ToDate > C.FromDate
						
	OPTION (RECOMPILE, FORCE ORDER, LOOP JOIN)
', @type = N'OBJECT', @module_or_batch = N'[Intrabet].[Sp_Instrument_EFT]', @hints = N'OPTION (RECOMPILE)'

EXEC sp_create_plan_guide @name = N'[BULK:Intrabet-sp_Instrument_Bpay:InsertIntoStaging(ThresholdDays=10)]', @stmt = N'
	INSERT	into Stage.Instrument (BatchKey,InstrumentId,Source,AccountNumber,SafeAccountNumber,AccountName,BSB,ExpiryDate,Verified,VerifyAmount,InstrumentType,ProviderName,FromDate)
	SELECT	
			@BatchKey						  AS BatchKey, 
			C.id							  AS instrumentID,
			ISNULL(C.trnType, ''Unknown'')		  AS Source, 
			ISNULL(Bp.RefNumber,''Unknown'')	  AS AccountNumber,
			ISNULL(Bp.RefNumber,''Unknown'')	  AS SafeAccountNumber,
			''N/A''						  AS AccountName,		
			ISNULL(Bp.BillerCode,''Unknown'')	  AS BSB,
			NULL							  AS ExpiryDate,
			''U/K''						  AS Verified,
			NULL							  AS VerifiedAmount,
			''BPAY''						  AS InstrumentType,
			''Unknown''						  AS ProviderName,
			c.FromDate					  AS FromDate		

	FROM #Candidates C  with (nolock)
	LEFT OUTER JOIN [Acquisition].Intrabet.tblClientBpayDetails BP WITH (NOLOCK) ON 
					C.ID =Cast(Bp.BPayID as varchar) 
					AND Bp.FromDate <= c.FromDate AND Bp.ToDate > C.FromDate

	OPTION (RECOMPILE, FORCE ORDER, LOOP JOIN)
', @type = N'OBJECT', @module_or_batch = N'[Intrabet].[Sp_Instrument_Bpay]', @hints = N'OPTION (RECOMPILE)'

/* Static Data Inserts*/
PRINT N'Static Data Inserts...';
Exec Reference.Sp_InitialPopAll

--Synonyms
PRINT N'Updating Synonyms...';
DROP SYNONYM [Control].[LiveETLController]
GO

CREATE SYNONYM [Control].[LiveETLController] FOR [EQ3WNPRDBDW01].[Staging].[Control].[ETLController]
GO

--Operators
PRINT N'Operators...';
Begin Try EXEC msdb.dbo.sp_delete_operator @name=N'BI Notify' End Try Begin Catch End Catch
GO
EXEC msdb.dbo.sp_add_operator @name=N'BI Notify', 
		@enabled=1, 
		@weekday_pager_start_time=90000, 
		@weekday_pager_end_time=180000, 
		@saturday_pager_start_time=90000, 
		@saturday_pager_end_time=180000, 
		@sunday_pager_start_time=90000, 
		@sunday_pager_end_time=180000, 
		@pager_days=0, 
		@email_address=N'BI.Service@Williamhill.com.au', 
		@category_name=N'[Uncategorized]'
GO


Begin Try EXEC msdb.dbo.sp_delete_operator @name=N'BI Alert' End Try Begin Catch End Catch
GO
EXEC msdb.dbo.sp_add_operator @name=N'BI Alert', 
		@enabled=1, 
		@weekday_pager_start_time=90000, 
		@weekday_pager_end_time=180000, 
		@saturday_pager_start_time=90000, 
		@saturday_pager_end_time=180000, 
		@sunday_pager_start_time=90000, 
		@sunday_pager_end_time=180000, 
		@pager_days=0, 
		@email_address=N'BI.Alert@Williamhill.com.au', 
		@category_name=N'[Uncategorized]'
GO


SET NOCOUNT OFF

/*Version Info */
PRINT N'Setting Version Info...';
Insert into [$(Dimensional)].[Control].[Version] values ('$(VersionNumber)', 'Staging', getdate(), SUser_Name())

use [$(DatabaseName)]
GO

GO
PRINT N'Update complete.';




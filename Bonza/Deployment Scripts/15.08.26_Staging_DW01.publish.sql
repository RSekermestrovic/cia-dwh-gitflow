﻿/*
Deployment script for Staging

This code was generated by a tool.
Changes to this file may cause incorrect behavior and will be lost if
the code is regenerated.
*/

/*
Disabled ETLController Job
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar Acquisition "Acquisition"
:setvar Dimensional "Dimensional"
:setvar DimensionalDataPath "G:\SQLData\"
:setvar DimensionalLogPath "K:\SQLLogs\"
:setvar JobPrefix "DW_"
:setvar ServerName "EQ3WNPRDBDW01"
:setvar SSISDB "SSISDB"
:setvar Staging "Staging"
:setvar VersionNumber "15.08.26"
:setvar DatabaseName "Staging"
:setvar DefaultFilePrefix "Staging"
:setvar DefaultDataPath "E:\SQLData\"
:setvar DefaultLogPath "I:\SQLLogs\"

GO
:on error exit
GO
/*
Detect SQLCMD mode and disable script execution if SQLCMD mode is not supported.
To re-enable the script after enabling SQLCMD mode, execute the following:
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'SQLCMD mode must be enabled to successfully execute this script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
PRINT N'Disabling all DDL triggers...'
GO
DISABLE TRIGGER ALL ON DATABASE
GO
PRINT N'Dropping [Control].[LiveETLController]...';


GO
DROP SYNONYM [Control].[LiveETLController];


GO
PRINT N'Creating [ExactTarget]...';


GO
CREATE SCHEMA [ExactTarget]
    AUTHORIZATION [dbo];


GO
PRINT N'Starting rebuilding table [Control].[ETLController]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [Control].[tmp_ms_xx_ETLController] (
    [BatchKey]                     BIGINT          IDENTITY (1, 1) NOT NULL,
    [BatchStatus]                  SMALLINT        NULL,
    [BatchFromDate]                DATETIME2 (3)   NULL,
    [BatchToDate]                  DATETIME2 (3)   NULL,
    [BatchStartDate]               DATETIME2 (3)   NULL,
    [BatchEndDate]                 DATETIME2 (3)   NULL,
    [AtomicStatus]                 SMALLINT        NULL,
    [AtomicStartDate]              DATETIME2 (3)   NULL,
    [AtomicEndDate]                DATETIME2 (3)   NULL,
    [AtomicRowsProcessed]          INT             NULL,
    [StagingStatus]                SMALLINT        NULL,
    [StagingStartDate]             DATETIME2 (3)   NULL,
    [StagingEndDate]               DATETIME2 (3)   NULL,
    [StagingRowsProcessed]         INT             NULL,
    [RecursiveStatus]              SMALLINT        NULL,
    [RecursiveStartDate]           DATETIME2 (3)   NULL,
    [RecursiveEndDate]             DATETIME2 (3)   NULL,
    [RecursiveRowsProcessed]       INT             NULL,
    [DimensionStatus]              SMALLINT        NULL,
    [DimensionStartDate]           DATETIME2 (3)   NULL,
    [DimensionEndDate]             DATETIME2 (3)   NULL,
    [DimensionRowsProcessed]       INT             NULL,
    [TransactionStatus]            SMALLINT        NULL,
    [TransactionStartDate]         DATETIME2 (3)   NULL,
    [TransactionEndDate]           DATETIME2 (3)   NULL,
    [TransactionRowsProcessed]     INT             NULL,
    [KPIFactStatus]                SMALLINT        NULL,
    [KPIFactStartDate]             DATETIME2 (3)   NULL,
    [KPIFactEndDate]               DATETIME2 (3)   NULL,
    [KPIFactRowsProcessed]         INT             NULL,
    [PositionStagingStatus]        SMALLINT        NULL,
    [PositionStagingStartDate]     DATETIME2 (3)   NULL,
    [PositionStagingEndDate]       DATETIME2 (3)   NULL,
    [PositionStagingRowsProcessed] INT             NULL,
    [PositionFactStatus]           SMALLINT        NULL,
    [PositionFactStartDate]        DATETIME2 (3)   NULL,
    [PositionFactEndDate]          DATETIME2 (3)   NULL,
    [PositionFactRowsProcessed]    INT             NULL,
    [BalanceFactStatus]            SMALLINT        NULL,
    [BalanceFactStartDate]         DATETIME2 (3)   NULL,
    [BalanceFactEndDate]           DATETIME2 (3)   NULL,
    [BalanceFactRowsProcessed]     INT             NULL,
    [ContactStagingStatus]         INT             NULL,
    [ContactStagingStartDate]      DATETIME2 (3)   NULL,
    [ContactStagingEndDate]        DATETIME2 (3)   NULL,
    [ContactStagingRowsProcessed]  INT             NULL,
    [ContactFactStatus]            INT             NULL,
    [ContactFactStartDate]         DATETIME2 (3)   NULL,
    [ContactFactEndDate]           DATETIME2 (3)   NULL,
    [ContactFactRowsProcessed]     INT             NULL,
    [ErrorMessage]                 NVARCHAR (4000) NULL
);

CREATE UNIQUE CLUSTERED INDEX [tmp_ms_xx_index_IX_CI_ETLController_BatchKey]
    ON [Control].[tmp_ms_xx_ETLController]([BatchKey] DESC);

IF EXISTS (SELECT TOP 1 1 
           FROM   [Control].[ETLController])
    BEGIN
        SET IDENTITY_INSERT [Control].[tmp_ms_xx_ETLController] ON;
        INSERT INTO [Control].[tmp_ms_xx_ETLController] ([BatchKey], [BatchStatus], [BatchFromDate], [BatchToDate], [BatchStartDate], [BatchEndDate], [AtomicStatus], [AtomicStartDate], [AtomicEndDate], [AtomicRowsProcessed], [StagingStatus], [StagingStartDate], [StagingEndDate], [StagingRowsProcessed], [RecursiveStatus], [RecursiveStartDate], [RecursiveEndDate], [RecursiveRowsProcessed], [DimensionStatus], [DimensionStartDate], [DimensionEndDate], [DimensionRowsProcessed], [TransactionStatus], [TransactionStartDate], [TransactionEndDate], [TransactionRowsProcessed], [KPIFactStatus], [KPIFactStartDate], [KPIFactEndDate], [KPIFactRowsProcessed], [PositionStagingStatus], [PositionStagingStartDate], [PositionStagingEndDate], [PositionStagingRowsProcessed], [PositionFactStatus], [PositionFactStartDate], [PositionFactEndDate], [PositionFactRowsProcessed], [BalanceFactStatus], [BalanceFactStartDate], [BalanceFactEndDate], [BalanceFactRowsProcessed], [ErrorMessage])
        SELECT   [BatchKey],
                 [BatchStatus],
                 [BatchFromDate],
                 [BatchToDate],
                 [BatchStartDate],
                 [BatchEndDate],
                 [AtomicStatus],
                 [AtomicStartDate],
                 [AtomicEndDate],
                 [AtomicRowsProcessed],
                 [StagingStatus],
                 [StagingStartDate],
                 [StagingEndDate],
                 [StagingRowsProcessed],
                 [RecursiveStatus],
                 [RecursiveStartDate],
                 [RecursiveEndDate],
                 [RecursiveRowsProcessed],
                 [DimensionStatus],
                 [DimensionStartDate],
                 [DimensionEndDate],
                 [DimensionRowsProcessed],
                 [TransactionStatus],
                 [TransactionStartDate],
                 [TransactionEndDate],
                 [TransactionRowsProcessed],
                 [KPIFactStatus],
                 [KPIFactStartDate],
                 [KPIFactEndDate],
                 [KPIFactRowsProcessed],
                 [PositionStagingStatus],
                 [PositionStagingStartDate],
                 [PositionStagingEndDate],
                 [PositionStagingRowsProcessed],
                 [PositionFactStatus],
                 [PositionFactStartDate],
                 [PositionFactEndDate],
                 [PositionFactRowsProcessed],
                 [BalanceFactStatus],
                 [BalanceFactStartDate],
                 [BalanceFactEndDate],
                 [BalanceFactRowsProcessed],
                 [ErrorMessage]
        FROM     [Control].[ETLController]
        ORDER BY [BatchKey] DESC;
        SET IDENTITY_INSERT [Control].[tmp_ms_xx_ETLController] OFF;
    END

DROP TABLE [Control].[ETLController];

EXECUTE sp_rename N'[Control].[tmp_ms_xx_ETLController]', N'ETLController';

EXECUTE sp_rename N'[Control].[ETLController].[tmp_ms_xx_index_IX_CI_ETLController_BatchKey]', N'IX_CI_ETLController_BatchKey', N'INDEX';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
PRINT N'Creating [Stage].[Communication]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
CREATE TABLE [Stage].[Communication] (
    [BatchKey]            INT           NOT NULL,
    [CommunicationId]     INT           NULL,
    [CommunicationSource] VARCHAR (5)   NULL,
    [Title]               VARCHAR (200) NULL,
    [Details]             VARCHAR (MAX) NULL,
    [SubDetails]          VARCHAR (MAX) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY];


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
PRINT N'Creating [Stage].[Contact]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
CREATE TABLE [Stage].[Contact] (
    [BatchKey]               INT            NULL,
    [AccountId]              BIGINT         NULL,
    [AccountSource]          VARCHAR (3)    NOT NULL,
    [JobID]                  BIGINT         NULL,
    [Story]                  VARCHAR (3)    NOT NULL,
    [ChannelName]            VARCHAR (32)   NOT NULL,
    [PromotionId]            INT            NOT NULL,
    [BetType]                VARCHAR (3)    NOT NULL,
    [BetGroupType]           VARCHAR (3)    NOT NULL,
    [BetSubType]             VARCHAR (3)    NOT NULL,
    [GroupingType]           VARCHAR (3)    NOT NULL,
    [Legtype]                VARCHAR (3)    NOT NULL,
    [classId]                INT            NOT NULL,
    [CommunicationCompanyId] INT            NOT NULL,
    [EventId]                INT            NOT NULL,
    [InteractionTypeId]      VARCHAR (30)   NULL,
    [InteractionTypeSource]  VARCHAR (2)    NOT NULL,
    [EventDate]              DATETIME       NULL,
    [EventTime]              TIME (7)       NULL,
    [Details]                NVARCHAR (900) NULL
) ON [PRIMARY];


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
PRINT N'Creating [Control].[LiveETLController]...';


GO
CREATE SYNONYM [Control].[LiveETLController] FOR [Control].ETLController;


GO
PRINT N'Creating [ExactTarget].[Sp_Contact]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
CREATE PROC [ExactTarget].[Sp_Contact]
(
	@FromDate	datetime2(3),
	@ToDate		datetime2(3),
	@BatchKey	int
)
WITH RECOMPILE
AS 

BEGIN TRY

EXEC [Control].Sp_Log @BatchKey,'Sp_Contact','InsertIntoStaging','Start'

--Declare @fromdate DateTime2(3), @BatchKey INT
--Set @fromdate = '2015-10-29 00:00:00.000'
--Set @BatchKey = 1

INSERT INTO Stage.Contact
---EMAIL SENT---
SELECT 
	Distinct
	@BatchKey BatchKey,
	b.clientID AccountId,
	'IAC' AccountSource,
	a.JobID,
	'N/A' Story,
	'Email' ChannelName,
	0 PromotionId,
	'N/A' BetType,
	'N/A' BetGroupType,
	'N/A' BetSubType,
	'N/A' GroupingType,
	'N/A' Legtype,
	0 classId,
	0 CommunicationCompanyId,
	0 EventId,
	'SNT' InteractionTypeId,
	'ET' InteractionTypeSource,
	DATEADD(minute,930,a.EventDate) as EventDate, --14.5 hour difference between Indianopolis and Darwin
	CONVERT(time,DATEADD(minute,930,a.EventDate)) EventTime,
	'' Details
  FROM [$(Acquisition)].[ExactTarget].[Sent] a
  INNER JOIN [$(Acquisition)].IntraBet.tblSubscriptionCentre b on a.SubscriberKey = b.exactTargetID
  Where --b.ToDate = '9999-12-31' and 
  LEN(a.SubscriberKey) > 20 and a.SubscriberKey not like '%@%'
  and a.FromDate >= @FromDate and a.FromDate < @ToDate

UNION ALL

---EMAIL CLICK---
SELECT 
	Distinct
	@BatchKey BatchKey,
	b.clientID AccountId,
	'IAC' AccountSource,
	a.JobID,
	'N/A' Story,
	'Email' ChannelName,
	0 PromotionId,
	'N/A' BetType,
	'N/A' BetGroupType,
	'N/A' BetSubType,
	'N/A' GroupingType,
	'N/A' Legtype,
	0 classId,
	0 CommunicationCompanyId,
	0 EventId,
	'CLIC' InteractionTypeId,
	'ET' InteractionTypeSource,
	DATEADD(minute,930,a.EventDate) as EventDate, --14.5 hour difference between Indianopolis and Darwin
	CONVERT(time,DATEADD(minute,930,a.EventDate)) EventTime,
	URL Details
  FROM [$(Acquisition)].[ExactTarget].[Click] a
  INNER JOIN [$(Acquisition)].IntraBet.tblSubscriptionCentre b on a.SubscriberKey = b.exactTargetID
  Where --b.ToDate = '9999-12-31' and 
  LEN(a.SubscriberKey) > 20 and a.SubscriberKey not like '%@%'
  and a.FromDate >= @FromDate and a.FromDate < @ToDate

UNION ALL

---EMAIL OPEN---
SELECT 
	Distinct
	@BatchKey BatchKey,
	b.clientID AccountId,
	'IAC' AccountSource,
	a.JobID,
	'N/A' Story,
	'Email' ChannelName,
	0 PromotionId,
	'N/A' BetType,
	'N/A' BetGroupType,
	'N/A' BetSubType,
	'N/A' GroupingType,
	'N/A' Legtype,
	0 classId,
	0 CommunicationCompanyId,
	0 EventId,
	'OPN' InteractionTypeId,
	'ET' InteractionTypeSource,
	DATEADD(minute,930,a.EventDate) as EventDate, --14.5 hour difference between Indianopolis and Darwin
	CONVERT(time,DATEADD(minute,930,a.EventDate)) EventTime,
	'' Details
  FROM [$(Acquisition)].[ExactTarget].[Open] a
  INNER JOIN [$(Acquisition)].IntraBet.tblSubscriptionCentre b on a.SubscriberKey = b.exactTargetID
  Where --b.ToDate = '9999-12-31' and 
  LEN(a.SubscriberKey) > 20 and a.SubscriberKey not like '%@%'
  and a.FromDate >= @FromDate and a.FromDate < @ToDate

UNION ALL

---EMAIL UNSUBSCRIBE---
SELECT 
	Distinct
	@BatchKey BatchKey,
	b.clientID AccountId,
	'IAC' AccountSource,
	a.JobID,
	'N/A' Story,
	'Email' ChannelName,
	0 PromotionId,
	'N/A' BetType,
	'N/A' BetGroupType,
	'N/A' BetSubType,
	'N/A' GroupingType,
	'N/A' Legtype,
	0 classId,
	0 CommunicationCompanyId,
	0 EventId,
	'UNS' InteractionTypeId,
	'ET' InteractionTypeSource,
	DATEADD(minute,930,a.EventDate) as EventDate, --14.5 hour difference between Indianopolis and Darwin
	CONVERT(time,DATEADD(minute,930,a.EventDate)) EventTime,
	'' Details
  FROM [$(Acquisition)].[ExactTarget].[Unsubscribe] a
  INNER JOIN [$(Acquisition)].IntraBet.tblSubscriptionCentre b on a.SubscriberKey = b.exactTargetID
  Where --b.ToDate = '9999-12-31' and 
  LEN(a.SubscriberKey) > 20 and a.SubscriberKey not like '%@%'
  and a.FromDate >= @FromDate and a.FromDate < @ToDate

UNION ALL

---EMAIL BOUNCE---
SELECT 
	Distinct
	@BatchKey BatchKey,
	b.clientID AccountId,
	'IAC' AccountSource,
	a.JobID,
	'N/A' Story,
	'Email' ChannelName,
	0 PromotionId,
	'N/A' BetType,
	'N/A' BetGroupType,
	'N/A' BetSubType,
	'N/A' GroupingType,
	'N/A' Legtype,
	0 classId,
	0 CommunicationCompanyId,
	0 EventId,
	CONVERT(varchar,a.BounceCategoryID) InteractionTypeId,
	'ET' InteractionTypeSource,
	DATEADD(minute,930,a.EventDate) as EventDate, --14.5 hour difference between Indianopolis and Darwin
	CONVERT(time,DATEADD(minute,930,a.EventDate)) EventTime,
	BounceSubcategory Details
  FROM [$(Acquisition)].[ExactTarget].[Bounce] a
  INNER JOIN [$(Acquisition)].IntraBet.tblSubscriptionCentre b on a.SubscriberKey = b.exactTargetID
  Where --b.ToDate = '9999-12-31' and 
  LEN(a.SubscriberKey) > 20 and a.SubscriberKey not like '%@%'
  and a.FromDate >= @FromDate and a.FromDate < @ToDate



END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Contact', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
PRINT N'Creating [ExactTarget].[Sp_Communication]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
CREATE PROC [ExactTarget].[Sp_Communication]
(
	@FromDate	datetime2(3),
	@ToDate		datetime2(3),
	@BatchKey	int
)
WITH RECOMPILE
AS 

BEGIN TRY

EXEC [Control].Sp_Log @BatchKey,'Sp_Communication','InsertIntoStaging','Start'

INSERT INTO [Stage].[Communication]
Select	Distinct
		@BatchKey BatchKey,
		JobID,
		'ET' as Source,
		EmailName as Title,
		EmailSubject as Details,
		null as SubDetails
From [$(Acquisition)].ExactTarget.Job
Where FromDate >= @FromDate and FromDate < @ToDate

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Communication', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
PRINT N'Creating [Control].[ETLContactStaging]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
CREATE PROCEDURE [Control].[ETLContactStaging]
	(
		@BatchKey INT = NULL
	)

AS 

BEGIN

Declare @FromDate dateTime2(3)
Declare @ToDate DateTime2(3)
Declare @ETLatestTodate DateTime2(3)
Declare @ETLoadedDate Datetime2(3)

Select	@ETLatestTodate = IsNull(MAX(ToDate),CONVERT(DateTime2(3),'1900-01-01 00:00:00.000'))
From [$(Acquisition)].[ExactTarget].[CompleteDate]
	
		BEGIN TRY
			IF ISNUMERIC(@BatchKey)<>1																									
				SELECT @BatchKey=MIN(BatchKey) FROM [Control].[ETLController] WHERE DimensionStatus =1 AND ISNULL(ContactStagingStatus,0)=0
			
			IF EXISTS ( SELECT 1 FROM [Control].[ETLController] WHERE DimensionStatus = 1 AND ContactStagingStatus IN (-1,-2,-3) 
					AND ErrorMessage LIKE '%Deadlock%')																					
			BEGIN
				SELECT 	@BatchKey=MIN(BatchKey) 
				FROM [Control].[ETLController] 
				WHERE DimensionStatus=1 AND ContactStagingStatus IN(-1,-2,-3) AND ErrorMessage LIKE '%Deadlock%'				
				

				DELETE FROM Stage.Contact WHERE BatchKey=@BatchKey
				DELETE FROM Stage.Communication WHERE BatchKey=@BatchKey

				UPDATE [Control].[ETLController]																			
					SET ContactStagingStatus		=NULL,
						ContactStagingStartDate	=NULL,
						ContactStagingEndDate	=NULL,
						ErrorMessage		=NULL 
					WHERE BatchKey=	@BatchKey
									
				EXEC msdb.dbo.sp_send_dbmail																						
					@profile_name	= 'DataWareHouse_Auto_Mails',
					@recipients		= 'DW.Service@WilliamHill.com.au',
					@body			= 'Contact Staging ETL Deadlocked, process has been restarted, check ETL log/ETLController for details',
					@subject		= 'Contact Staging ETL Deadlocked, process has been restarted, check ETL log/ETLController for details'
			END


			IF ISNUMERIC(@BatchKey)=1 
			BEGIN 

			Select @ETLoadedDate = CONVERT(DateTime,CONVERT(date,IsNull(BatchFromDate,'1900-01-01 00:00:00.000')))
			FROM [Control].[ETLController]
			Where BatchKey = @BatchKey

			IF @ETLatestTodate >= @ETLoadedDate	

				BEGIN

					SELECT @FromDate= BatchFromDate FROM [Control].[ETLController] WHERE BatchKey=@BatchKey
					SELECT @ToDate= BatchToDate FROM [Control].[ETLController] WHERE BatchKey=@BatchKey
	
					UPDATE [Control].[ETLController] SET ContactStagingStartDate=GETDATE() WHERE BatchKey=@BatchKey				

					EXEC [ExactTarget].[Sp_Communication] @FromDate, @ToDate, @BatchKey
					EXEC [ExactTarget].[Sp_Contact] @FromDate, @ToDate, @BatchKey	
				
					--INSERT INTO [$(Acquisition)].ExactTarget.Latency
					--Select MAX(FromDate), MAX(FromDate) From Stage.Contact Where BatchKey = @BatchKey											

					UPDATE [Control].[ETLController] SET ContactStagingEndDate=GETDATE() WHERE BatchKey=@BatchKey					
					UPDATE [Control].[ETLController] SET ContactStagingStatus=1 WHERE BatchKey=@BatchKey							

					UPDATE [Control].[ETLController]																				
					SET ContactstagingRowsProcessed=(SELECT COUNT(*) FROM STAGE.Contact WITH(NOLOCK) WHERE BatchKey=@BatchKey) 
					WHERE BatchKey=@BatchKey
				
				END
			
			END 	


		END TRY
		
		BEGIN CATCH
			UPDATE [Control].[ETLController] SET ContactStagingEndDate=GETDATE() WHERE BatchKey=@BatchKey						
			UPDATE [Control].[ETLController] SET ContactStagingStatus=ISNULL(ContactStagingStatus,0)-1 WHERE BatchKey=@BatchKey;	
																																		
			UPDATE [Control].[ETLController] SET ErrorMessage = ERROR_MESSAGE() WHERE BatchKey=@BatchKey;
			THROW;
		END CATCH

END
GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
PRINT N'Creating [Dimension].[Sp_ContactFactLoad]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
CREATE PROC [Dimension].[Sp_ContactFactLoad]
(
	@BatchKey	int
)
WITH RECOMPILE
AS 

BEGIN TRY

EXEC [Control].Sp_Log @BatchKey,'Sp_ContactFactLoad','InsertIntoDimension','Start'

Select 
	IsNull(DAK.AccountKey,-1) AccountKey,
	a.JobID CommunicationId,
	IsNull(d.CommunicationKey,-1) CommunicationKey,
	IsNull(s.StoryKey,-1) StoryKey,
	IsNull(ch.ChannelKey,-1) ChannelKey,
	IsNull(DBT.BettypeKey,-1) BettypeKey,
	IsNull(cl.classKey,-1) classKey,
	IsNull(co.CompanyKey,-1) CommunicationCompanyKey,
	a.EventId,
	IsNull(it.InteractionTypeKey,-1) InteractionTypeKey,
	IsNull(DZ.DayKey,-1) InteractionDayKey,
	ISNULL(TMD.TimeKey,-1) AS InteractionTimeKeyMaster,
	ISNULL(TAD.TimeKey,-1) AS InteractionTimeKeyAnalysis,
	a.Details 
INTO #PreLoad
From [Stage].[Contact] a
LEFT OUTER JOIN [$(Dimensional)].Dimension.ACCOUNT	DAK  WITH(NOLOCK) ON a.AccountId=DAK.ClientId AND CONVERT(DATETIME2(3),a.EventDate) >=DAK.FROMDATE AND CONVERT(DATETIME2(3),a.EventDate) <DAK.TODATE
LEFT OUTER JOIN [$(Dimensional)].[Dimension].[Communication] d on a.JobId = d.CommunicationId
LEFT OUTER JOIN Reference.TimeZone	TZ   WITH(NOLOCK) ON a.EventDate>=TZ.FromDate AND a.EventDate<TZ.ToDate AND TZ.TimeZone='Analysis'
LEFT OUTER JOIN [$(Dimensional)].Dimension.[DayZone] DZ WITH(NOLOCK) ON CONVERT(VARCHAR(11),CONVERT(DATE,a.EventDate))=DZ.MasterDayText AND a.EventDate>=DZ.FromDate AND a.EventDate<DZ.ToDate
LEFT OUTER JOIN [$(Dimensional)].Dimension.[TIME] TMD  WITH(NOLOCK) ON CONVERT(VARCHAR(5),CONVERT(TIME,a.EventDate))=TMD.TimeText
LEFT OUTER JOIN [$(Dimensional)].Dimension.[TIME] TAD  WITH(NOLOCK) ON CONVERT(VARCHAR(5),CONVERT(TIME,DATEADD(MI,TZ.OffsetMinutes,a.EventDate)))=TAD.TimeText
LEFT OUTER JOIN [$(Dimensional)].Dimension.InteractionType it on a.InteractionTypeId = it.InteractionTypeId
LEFT OUTER JOIN [$(Dimensional)].Dimension.Story s on a.Story = s.Story
LEFT OUTER JOIN [$(Dimensional)].Dimension.Channel ch on a.ChannelName = ch.ChannelName
LEFT OUTER JOIN [$(Dimensional)].Dimension.BetType DBT on a.BetType=DBT.BetType AND a.LegType=DBT.LegType AND a.BetGroupType=DBT.BetGroupType AND a.BetSubType=DBT.BetSubType AND a.GroupingType=DBT.GroupingType
LEFT OUTER JOIN [$(Dimensional)].Dimension.Class cl on a.ClassId = cl.ClassId
LEFT OUTER JOIN [$(Dimensional)].Dimension.Company co on a.CommunicationCompanyId = co.CompanyID
Where a.BatchKey = @BatchKey
--and e.ToDate = '9999-12-31'
OPTION (RECOMPILE)

INSERT INTO [$(Dimensional)].[Fact].[Contact]
           ([AccountKey]
		   ,[CommunicationId]
           ,[CommunicationKey]
           ,[StoryKey]
           ,[ChannelKey]
           ,[BetTypeKey]
           ,[ClassKey]
           ,[CommunicationCompanyKey]
           ,[InteractionTypeKey]
           ,[InteractionDayKey]
           ,[InteractionTimeKeyMaster]
		   ,[InteractionTimeKeyAnalysis]
           ,[Details]
		   ,[MarketKey]
		   ,[CreatedDate]
		   ,[CreatedBatchKey]
		   ,[ModifiedDate]
		   ,[ModifiedBatchKey]
		   )
Select	
	AccountKey,
	CommunicationId,
	CommunicationKey,
	StoryKey,
	ChannelKey,
	BetTypeKey,
	a.ClassKey,
	CommunicationCompanyKey,
	InteractionTypeKey,
	InteractionDayKey,
	InteractionTimeKeyMaster,
	InteractionTimeKeyAnalysis,
	a.Details,
	IsNull(ev.MarketKey, -1) MarketKey,
	GetDate(),
	@BatchKey,
	GetDate(),
	@BatchKey
From #PreLoad a
LEFT OUTER JOIN [$(Dimensional)].Dimension.[Market] ev on a.EventId = ev.MarketId
OPTION (RECOMPILE)

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_ContactFactLoad', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
PRINT N'Creating [Dimension].[Sp_DimCommunication]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
CREATE PROC [Dimension].[Sp_DimCommunication]
(
	@BatchKey	int
)
WITH RECOMPILE
AS 

BEGIN TRY

EXEC [Control].Sp_Log @BatchKey,'Sp_DimCommunication','InsertIntoDimension','Start'

INSERT INTO [$(Dimensional)].[Dimension].[Communication]
Select
		a.CommunicationId,
		a.CommunicationSource,
		a.Title,
		a.Details,
		a.SubDetails,
		GetDate() CreatedDate,
		GetDate() ModifiedDate,
		@BatchKey createdBatchKey,
		'Sp_DimCommunication' CreatedBy,
		@BatchKey ModifiedBatchKey,
		'Sp_DimCommunication' ModifiedBy 
from [Stage].[Communication] a
LEFT OUTER JOIN [$(Dimensional)].[Dimension].[Communication] b on a.CommunicationId = b.CommunicationId
Where BatchKey = @BatchKey
and b.CommunicationId is null

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_DimCommunication', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
PRINT N'Creating [Control].[ETLContactFact]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
CREATE PROCEDURE [Control].[ETLContactFact]
	(
		@BatchKey	int	= NULL
	)
AS 

BEGIN
	
		BEGIN TRY

			IF ISNUMERIC(@BatchKey)<>1																								
				SELECT @BatchKey=MIN(BatchKey) FROM [Control].[ETLController] WHERE ContactStagingStatus=1 AND ISNULL(ContactFactStatus,0)=0
			
			IF ISNUMERIC(@BatchKey)=1																								
			BEGIN
	
				UPDATE [Control].[ETLController] SET ContactFactStartDate=GETDATE() WHERE BatchKey=@BatchKey	

				EXEC [Dimension].[Sp_DimCommunication] @BatchKey
				EXEC [Dimension].[Sp_ContactFactLoad] @BatchKey
							
				UPDATE [Control].[ETLController] SET ContactFactEndDate=GETDATE() WHERE BatchKey=@BatchKey
				UPDATE [Control].[ETLController] SET ContactFactStatus=1			 WHERE BatchKey=@BatchKey
				
				UPDATE [Control].[ETLController]
				SET ContactFactRowsProcessed=(SELECT COUNT(*) FROM [$(Dimensional)].FACT.Contact WITH(NOLOCK) WHERE CreatedBatchKey=@BatchKey) 
				WHERE BatchKey=@BatchKey
				
				DECLARE @CompleteDate	DATETIME2
				Set @CompleteDate =	(
									 SELECT MAX(EventDate) FROM [Stage].[Contact] Where BatchKey = @BatchKey
									)
				
				UPDATE STATISTICS [$(Dimensional)].FACT.Contact																	

				UPDATE [$(Dimensional)].Control.CompleteDate											
				SET ExactTargetCompleteDate=ISNULL(@CompleteDate,PositionCompleteDate)
				
			END

		END TRY
		
		BEGIN CATCH
			UPDATE [Control].[ETLController] SET ContactFactEndDate=GETDATE()	WHERE BatchKey=@BatchKey	
			UPDATE [Control].[ETLController] SET ContactFactStatus=-1			WHERE BatchKey=@BatchKey;
			UPDATE [Control].[ETLController] SET ErrorMessage = ERROR_MESSAGE() WHERE BatchKey=@BatchKey;
			THROW;
		END CATCH

		
END
GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
PRINT N'Creating Permission...';


GO
GRANT SELECT
    ON OBJECT::[Control].[ETLController] TO [Reporting];


GO
PRINT N'Refreshing [Audit].[Sp_ETLHealthCheck]...';


GO
EXECUTE sp_refreshsqlmodule N'[Audit].[Sp_ETLHealthCheck]';


GO
PRINT N'Refreshing [Control].[ETLBalanceFact]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
EXECUTE sp_refreshsqlmodule N'[Control].[ETLBalanceFact]';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
PRINT N'Refreshing [Control].[ETLDimension]...';


GO
EXECUTE sp_refreshsqlmodule N'[Control].[ETLDimension]';


GO
PRINT N'Refreshing [Control].[ETLFinished]...';


GO
EXECUTE sp_refreshsqlmodule N'[Control].[ETLFinished]';


GO
PRINT N'Refreshing [Control].[ETLKPI]...';


GO
EXECUTE sp_refreshsqlmodule N'[Control].[ETLKPI]';


GO
PRINT N'Refreshing [Control].[ETLPositionFact]...';


GO
EXECUTE sp_refreshsqlmodule N'[Control].[ETLPositionFact]';


GO
PRINT N'Refreshing [Control].[ETLPositionStaging]...';


GO
EXECUTE sp_refreshsqlmodule N'[Control].[ETLPositionStaging]';


GO
PRINT N'Refreshing [Control].[ETLTransaction]...';


GO
EXECUTE sp_refreshsqlmodule N'[Control].[ETLTransaction]';


GO
PRINT N'Refreshing [Control].[Sp_Partitioning_Batch]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
EXECUTE sp_refreshsqlmodule N'[Control].[Sp_Partitioning_Batch]';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
PRINT N'Refreshing [Control].[Sp_Partitioning_Day]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
EXECUTE sp_refreshsqlmodule N'[Control].[Sp_Partitioning_Day]';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
PRINT N'Refreshing [Intrabet].[Sp_AtomicOpeningBalances]...';


GO
EXECUTE sp_refreshsqlmodule N'[Intrabet].[Sp_AtomicOpeningBalances]';


GO
PRINT N'Refreshing [Intrabet].[Sp_Rec_BalanceFactCheck]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
EXECUTE sp_refreshsqlmodule N'[Intrabet].[Sp_Rec_BalanceFactCheck]';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
PRINT N'Refreshing [Intrabet].[Sp_TransactionBets]...';


GO
EXECUTE sp_refreshsqlmodule N'[Intrabet].[Sp_TransactionBets]';


GO
PRINT N'Refreshing [Utility].[Sp_PurgeData]...';


GO
EXECUTE sp_refreshsqlmodule N'[Utility].[Sp_PurgeData]';


GO
PRINT N'Refreshing [Control].[ETLAtomic]...';


GO
EXECUTE sp_refreshsqlmodule N'[Control].[ETLAtomic]';


GO
PRINT N'Refreshing [Control].[ETLControllerMain]...';


GO
EXECUTE sp_refreshsqlmodule N'[Control].[ETLControllerMain]';


GO
PRINT N'Refreshing [Control].[ETLRecursive]...';


GO
EXECUTE sp_refreshsqlmodule N'[Control].[ETLRecursive]';


GO
PRINT N'Refreshing [Control].[ETLStaging]...';


GO
EXECUTE sp_refreshsqlmodule N'[Control].[ETLStaging]';


GO
PRINT N'Refreshing [Utility].[sp_ColdStart]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
EXECUTE sp_refreshsqlmodule N'[Utility].[sp_ColdStart]';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

/*Version Info */
Insert into [Control].[Version] values ('$(VersionNumber)', getdate(), SUser_Name())

/* Static Data Inserts*/
Exec Reference.Sp_InitialPopAll

--Synonyms
DROP SYNONYM [Control].[LiveETLController]
GO

CREATE SYNONYM [Control].[LiveETLController] FOR [EQ3DBS05].[Staging].[Control].[ETLController]
GO

/* SQL Server Agent Jobs */

USE [msdb]
GO

/****** Object:  Job [ED_ETLContactFact]    Script Date: 12/11/2015 9:27:39 AM ******/
BEGIN TRANSACTION
Declare @JobName varchar(1000)
Set @JobName = '$(JobPrefix)' + 'ETLContactFact'
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [ETL]    Script Date: 12/11/2015 9:27:39 AM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'ETL' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'ETL'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=@JobName, 
		@enabled=0, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'ETL', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [RUN PROC]    Script Date: 12/11/2015 9:27:39 AM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'RUN PROC', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXEC [Control].[ETLContactFact]', 
		@database_name=[$(Staging)], 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'ContactFact', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=4, 
		@freq_subday_interval=1, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20140922, 
		@active_end_date=99991231, 
		@active_start_time=0, 
		@active_end_time=235959, 
		@schedule_uid=N'f09dde08-22cb-4ab7-9928-fc5b0fcca735'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:

GO



USE [msdb]
GO

/****** Object:  Job [ED_ETLContactStaging]    Script Date: 11/11/2015 6:28:48 PM ******/
BEGIN TRANSACTION
Declare @JobName varchar(1000)
Set @JobName = '$(JobPrefix)' + 'ETLContactStaging'
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [ETL]    Script Date: 11/11/2015 6:28:48 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'ETL' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'ETL'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=@JobName, 
		@enabled=0, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'ETL', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [EXEC PROC]    Script Date: 11/11/2015 6:28:48 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'EXEC PROC', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXEC [Control].[ETLContactStaging]', 
		@database_name=[$(Staging)], 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'ContactStaging', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=4, 
		@freq_subday_interval=1, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20140922, 
		@active_end_date=99991231, 
		@active_start_time=0, 
		@active_end_time=235959, 
		@schedule_uid=N'f09dde08-22cb-4ab7-9928-fc5b0fcca735'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:

GO




use [$(Staging)]
GO

GO
PRINT N'Reenabling DDL triggers...'
GO
ENABLE TRIGGER [trg_DDLChangeTrigger] ON DATABASE
GO
PRINT N'Update complete.';


GO


/*
Enable ETLController Job
*/
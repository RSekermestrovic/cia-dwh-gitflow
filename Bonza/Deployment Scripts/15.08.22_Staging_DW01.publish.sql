﻿/*
Deployment script for Staging

This code was generated by a tool.
Changes to this file may cause incorrect behavior and will be lost if
the code is regenerated.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar Acquisition "Acquisition"
:setvar Dimensional "Dimensional"
:setvar DimensionalDataPath "G:\SQLData\"
:setvar DimensionalLogPath "K:\SQLLogs\"
:setvar JobPrefix "DW_"
:setvar ServerName "EQ3WNPRDBDW01"
:setvar SSISDB "SSISDB"
:setvar Staging "Staging"
:setvar VersionNumber "15.08.22"
:setvar DatabaseName "Staging"
:setvar DefaultFilePrefix "Staging"
:setvar DefaultDataPath "E:\SQLData\"
:setvar DefaultLogPath "I:\SQLLogs\"

GO
:on error exit
GO
/*
Detect SQLCMD mode and disable script execution if SQLCMD mode is not supported.
To re-enable the script after enabling SQLCMD mode, execute the following:
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'SQLCMD mode must be enabled to successfully execute this script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
PRINT N'Disabling all DDL triggers...'
GO
DISABLE TRIGGER ALL ON DATABASE
GO
PRINT N'Dropping [Control].[LiveETLController]...';


GO
DROP SYNONYM [Control].[LiveETLController];


GO
PRINT N'Creating [Control].[LiveETLController]...';


GO
CREATE SYNONYM [Control].[LiveETLController] FOR [Control].ETLController;


GO
PRINT N'Altering [Intrabet].[Sp_FreeBetCredits]...';


GO
ALTER PROC [Intrabet].[Sp_FreeBetCredits]
(
	@BatchKey	INT,
	@FromDate	DATETIME2(3),
	@ToDate		DATETIME2(3)
)
WITH RECOMPILE
AS 
BEGIN
BEGIN TRY

	SET TRANSACTION ISOLATION LEVEL SNAPSHOT

	EXEC [Control].Sp_Log	@BatchKey,'Sp_FreeBetCredits','LoadIntoStaging','Start'

		INSERT INTO Stage.[Transaction] (BatchKey,TransactionId,TransactionIdSource,BalanceTypeID,TransactionDetailId,BetGroupID,BetGroupIDSource,BetId,BetIdSource,LegId,LegIdSource,FreeBetID,ExternalID,External1,
		External2,UserID,InstrumentID,InstrumentIDSource,AccountId,AccountIDSource,WalletId,WalletIDSource,MasterTransactionTimestamp,MasterDayText,MasterTimeText,ChannelName,CampaignId,TransactedAmount,BetGroupType,
		BetTypeName,BetSubTypeName,LegBetTypeName,GroupingType,ClassId,ClassIdSource,EventID,EventIDSource,LegNumber,NumberOfLegs,ExistsInDim,Conditional,MappingType,MappingId,FromDate,DateTimeUpdated,RequestID,WithdrawID)

		SELECT   DISTINCT   
			@BATCHKEY										AS BatchKey
			,x.TransactionId								AS TransactionID
			,X.Source										AS TransactionIDSource
			,m.BalanceTypeID								AS BalanceTypeID				-- Changed from BalanceName on 6Mar15
			,m.TransactionDetailId							AS TransactionDetailID
			,X.BetId										AS BetGroupID
			,X.Source										AS BetGroupIDSource
			,X.BetId										AS BetID
			,X.Source										AS BetIDSource
			,X.BetId										AS LegID		
			,X.Source										AS LegIDSource
			,ISNULL(x.WalletId,-1)							AS FreeBetID
			,'N/A'											AS ExternalID
			,'N/A'											AS ExternalID1
			,'N/A'											AS ExternalID2
			,ISNULL(dt.UserCode,ISNULL(tl.UserCode,ISNULL(B.UserCode,-4))) AS userID
			,0												AS InstrumentID
			,'N/A'											AS InstrumentIDSource
			,COALESCE(D.AccountId,-1)						AS AccountId
			,'IAA'											AS AccountIdSource
			,ISNULL(x.WalletId,-1)							AS WalletId
			,X.Source										AS WalletIdSource
			,ISNULL(x.TransactionTimestamp,'1900-01-01')							AS MasterTransactionTimestamp
			,CONVERT(varchar(10),ISNULL(x.TransactionTimestamp,'1900-01-01'),120)	AS MasterDayText
			,CONVERT(varchar(5),ISNULL(x.TransactionTimestamp,'1900-01-01'),108)	AS MasterTimeText
			,'N/A'											AS ChannelName
			,x.CampaignId									AS CampaignId
			,x.TransactedAmount * m.Multiplier				AS TransactedAmount
			,'N/A'											AS BetGroupType
			,'N/A'											AS BetTypeName
			,'N/A'											AS BetSubTypeName
			,'N/A'											AS LegBetTypeName
			,'N/A'											AS GroupingType
			,0												AS ClassID
			,'IBT'											AS ClassIDSource
			,0												AS EventID
			,'N/A'											AS EventIDSource
			,0												AS LegNumber
			,0												AS NumberOfLegs
			,0												AS ExistsInDim
			,0												AS Conditional
			,90												AS MappingType
			,5000											AS MappingID
			,x.FromDate										AS FromDate
			,0												AS UpdateDateTime
			,NULL											AS RequestID
			,NULL											AS WithdrawID
		FROM   (

			-------------- FREEBETS BEFORE JUN 2012 --------------------

				------------ VCI --------------------
				-- First Record of the FreeBETID
				-- FromDate of the record is between the Period of concern
				-- Dont include FreeBETID's where PromotionID IS NULL
				-------------------------------------

				SELECT 
					'+' Direction
					,- B.FreeBetID * 10 BetId
					,- B.FreeBetID * 10 TransactionId
					,B.ClientID
					,COALESCE(P.PromotionID,B.FreeBetID,-1) AS WalletId												-- NULL handled Later
					,0 CampaignId
					,B.Value TransactedAmount
					,B.IssueDate TransactionTimestamp
					,B.FromDate
					,CASE WHEN P.PromotionID IS NOT NULL THEN 'IBP' ELSE 'IBF' END AS SOURCE						-- Case added Later
				FROM [$(Acquisition)].IntraBet.tblPromotionalBets B
				INNER JOIN [$(Acquisition)].IntraBet.tblPromotions P
					ON B.PromotionID=P.PromotionID AND P.FromDate<=B.FromDate AND P.ToDate>B.FromDate
				LEFT OUTER JOIN [$(Acquisition)].IntraBet.tblPromotionalBets C
					ON B.FreeBetID=C.FreeBetID AND P.FromDate=C.ToDate
				WHERE B.FromDate>=@FromDate AND B.FromDate<@ToDate
		--		AND B.PromotionID IS NOT NULL																		-- Removed Later
				AND C.FreeBetID IS NULL

				UNION ALL

				------------ VEI --------------------
				-- FromDate of the record is between the Period of concern
				-- BetUsed=1 but there in no entry of the Bet in the TblBets, hence should be an anomaly, classified as expiry.
				-- Dont include FreeBETID's where PromotionID IS NULL
				-------------------------------------

				SELECT 
					'-' Direction
					,- B.FreeBetID * 10 BetId
					,- B.FreeBetID * 10 TransactionId
					,B.ClientID
					,COALESCE(P.PromotionID,B.FreeBetID,-1) AS WalletId
					,0 CampaignId
					,B.Value TransactedAmount
					,B.ExpiryDate TransactionTimestamp
					,B.FromDate
					,CASE WHEN P.PromotionID IS NOT NULL THEN 'IBP' ELSE 'IBF' END AS SOURCE
				FROM [$(Acquisition)].IntraBet.tblPromotionalBets B
				INNER JOIN [$(Acquisition)].IntraBet.tblPromotions P
					ON B.PromotionID=P.PromotionID AND P.FromDate<=B.FromDate AND P.ToDate>B.FromDate
				LEFT OUTER JOIN [$(Acquisition)].IntraBet.tblBets C
					ON B.FreeBetID=C.FreeBetID 
				WHERE B.FromDate>=@FromDate AND B.FromDate<@ToDate
		--		AND B.PromotionID IS NOT NULL -- Removed Later
				AND B.BetUsed=1
				AND C.FreeBetID IS NULL

				------------ VEI --------------------
				-- FromDate of the record is between the Period of concern
				-- BetUsed=0 and Expirydate is less than or equal to FromDate AND Bet has not been placed at a later date for the same FreeBetID
				-- Dont include FreeBETID's where PromotionID IS NULL
				-------------------------------------

				UNION ALL

				SELECT 
					'-' Direction
					,- B.FreeBetID * 10 BetId
					,- B.FreeBetID * 10 TransactionId
					,B.ClientID
					,COALESCE(P.PromotionID,B.FreeBetID,-1) AS WalletId
					,0 CampaignId
					,B.Value TransactedAmount
					,B.ExpiryDate TransactionTimestamp
					,B.FromDate
					,CASE WHEN P.PromotionID IS NOT NULL THEN 'IBP' ELSE 'IBF' END AS SOURCE
				FROM [$(Acquisition)].IntraBet.tblPromotionalBets B
				INNER JOIN [$(Acquisition)].IntraBet.tblPromotions P
					ON B.PromotionID=P.PromotionID AND P.FromDate<=B.FromDate AND P.ToDate>B.FromDate
				WHERE B.FreeBetID NOT IN (SELECT FreeBetID FROM [$(Acquisition)].IntraBet.tblPromotionalBets WHERE BetUsed=1 GROUP BY FreeBetID)
				AND B.FromDate>=@FromDate AND B.FromDate<@ToDate
		--		AND B.PromotionID IS NOT NULL -- Removed Later
				AND B.BetUsed=0
				AND B.ExpiryDate<=B.FromDate
				
				---------------- FREEBETS AFTER SEP 2011 -----------------------------
		
				----------------BOTH VCI AND VEI ----------------------

				UNION ALL

		-- Approved Transactions (Including before delete transaction)


					SELECT 
						CASE WHEN t.Amount<0 THEN '-' ELSE '+' END AS Direction
						,-t.BonusTransID *10 BetID
						,-t.BonusTransID *10 TransactionID
						,t.ClientID
						,CASE WHEN t.Amount <0 THEN NULL ELSE COALESCE(NULLIF(t.PromotionID,0),t.BTransCode,-1) END AS WalletId
						,0 CampaignId
						,Abs(t.Amount) TransactedAmount
						,t.TransactionDate TransactionTimestamp
						,t.FromDate
						,CASE WHEN NULLIF(t.PromotionID,0) IS NOT NULL THEN 'IBP' ELSE 'IBB' END AS SOURCE
					FROM [$(Acquisition)].IntraBet.tblBonusTransactions t
					LEFT OUTER JOIN [$(Acquisition)].IntraBet.tblBonusTransactions ta	ON t.BonusTransID=ta.BonusTransID and t.ToDate=ta.FromDate			-- Is the last record for a CDC Change
					WHERE t.BTransCode <>-4																												-- Dont Include Bets (-4)
					AND NOT (t.BTransCode =15 AND t.BETID IS NOT NULL)																				-- Include Bonus Bets cancelled (15) only if betid is NULL (bet cancel logic takes care if BetID is not null)
					AND t.FromDate>@FromDate AND t.FromDate<=@ToDate																					-- FromDate between the ETL Run Period
					AND ta.BonusTransID IS NULL																											-- Not a CDC Change

				-- DELETED Transactions (Only after delete Transaction)
			
					UNION ALL

					SELECT 
						CASE WHEN t.Amount<0 THEN 'd-' ELSE 'd+' END AS Direction
						,-t.BonusTransID *10 BetID
						,-t.BonusTransID *10 TransactionID
						,t.ClientID
						,CASE WHEN t.Amount <0 THEN NULL ELSE COALESCE(NULLIF(t.PromotionID,0),t.BTransCode,-1) END AS WalletId
						,0 CampaignId
						,Abs(t.Amount) TransactedAmount
						,ISNULL(LT.CorrectedDate,t.todate) TransactionTimestamp																			-- ToDate of the delete record is when the Transaction was deleted.
						,ISNULL(LT.CorrectedDate,t.todate)																								-- ToDate of the delete record is when the Transaction was deleted.
						,CASE WHEN NULLIF(t.PromotionID,0) IS NOT NULL THEN 'IBP' ELSE 'IBB' END AS SOURCE
					FROM [$(Acquisition)].IntraBet.tblBonusTransactions t
					LEFT OUTER JOIN [$(Acquisition)].IntraBet.tblBonusTransactions ta	ON t.BonusTransID=ta.BonusTransID and t.ToDate=ta.FromDate			-- Is the last record, genuine delete record
					LEFT OUTER JOIN [$(Acquisition)].IntraBet.Latency LT WITH(NOLOCK) ON t.ToDate=LT.OriginalDate											-- Join to Latency Table, on ToDate of the deleted record
					WHERE t.BTransCode <>-4																												-- Dont Include Bets (-4)
					AND NOT (t.BTransCode =15 AND t.BETID IS NOT NULL)																				-- Include Bonus Bets cancelled (15) --only if betid is NULL (bet cancel logic takes care if BetID is not null)
					AND t.ToDate>@FromDate AND t.ToDate<=@ToDate																						-- ToDate between the ETL Run Period (genuine delete record)
					AND ta.BonusTransID IS NULL																											-- Not a CDC Change


			)X
		INNER JOIN (SELECT '+' Direction, 'VCI-' TransactionDetailId, 'P&L' BalanceTypeID, -1 Multiplier			-- Changed from BalanceName on 6Mar15 (All below)
					UNION ALL
					SELECT '+' Direction, 'VCI+' TransactionDetailId, 'CLI' BalanceTypeID, 1 Multiplier
					UNION ALL
					SELECT '-' Direction, 'VEI-' TransactionDetailId, 'CLI' BalanceTypeID, -1 Multiplier
					UNION ALL
					SELECT '-' Direction, 'VEI+' TransactionDetailId, 'P&L' BalanceTypeID, 1 Multiplier
					UNION ALL
					SELECT 'd+' Direction, 'VCI-' TransactionDetailId, 'CLI' BalanceTypeID, -1 Multiplier
					UNION ALL
					SELECT 'd+' Direction, 'VCI+' TransactionDetailId, 'P&L' BalanceTypeID, 1 Multiplier
					UNION ALL
					SELECT 'd-' Direction, 'VEI-' TransactionDetailId, 'P&L' BalanceTypeID, -1 Multiplier
					UNION ALL
					SELECT 'd-' Direction, 'VEI+' TransactionDetailId, 'CLI' BalanceTypeID, 1 Multiplier
				   ) M 
			ON (M.Direction = X.Direction)
		LEFT OUTER JOIN [$(Acquisition)].IntraBet.tblClients C	WITH(NOLOCK)				ON X.ClientID=C.ClientID AND C.ToDate='9999-12-31 00:00:00.000'
		LEFT OUTER JOIN [$(Acquisition)].IntraBet.tblAccounts D	WITH(NOLOCK)				ON X.ClientID=D.ClientID AND D.ToDate='9999-12-31 00:00:00.000'
		LEFT OUTER JOIN [$(Acquisition)].IntraBet.tblDeletedTransactions dt	WITH(NOLOCK)	ON x.TransactionID=Dt.TransactionID and x.FromDate=dt.FromDate
		LEFT OUTER JOIN [$(Acquisition)].IntraBet.tblTransactionLogs tl	WITH(NOLOCK)		ON x.TransactionID=tl.TransactionID and x.FromDate=tl.FromDate
		LEFT OUTER JOIN [$(Acquisition)].IntraBet.tblBets	B  	WITH(NOLOCK)				ON x.TransactionID=B.BetID and x.FromDate=B.FromDate

	EXEC [Control].Sp_Log @BatchKey, 'Sp_FreeBetCredits', NULL, 'Success'
END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_FreeBetCredits', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

END
GO
PRINT N'Refreshing [Control].[ETLStaging]...';


GO
EXECUTE sp_refreshsqlmodule N'[Control].[ETLStaging]';


GO
/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

/*Version Info */
Insert into [Control].[Version] values ('$(VersionNumber)', getdate(), SUser_Name())

/* Static Data Inserts*/
Exec Reference.Sp_InitialPopAll

--Synonyms
DROP SYNONYM [Control].[LiveETLController]
GO

CREATE SYNONYM [Control].[LiveETLController] FOR [EQ3DBS05].[Staging].[Control].[ETLController]
GO



use [$(Staging)]
GO

GO
PRINT N'Reenabling DDL triggers...'
GO
ENABLE TRIGGER [trg_DDLChangeTrigger] ON DATABASE
GO
PRINT N'Update complete.';


GO

﻿/*
Deployment script for Warehouse

This code was generated by a tool.
Changes to this file may cause incorrect behavior and will be lost if
the code is regenerated.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar Dimensional "Dimensional"
:setvar DimensionalDataPath "E:\SQLData\"
:setvar DimensionalLogPath "I:\SQLLogs\"
:setvar Staging "Staging"
:setvar VersionNumber "15.11.10"
:setvar DatabaseName "Warehouse"
:setvar DefaultFilePrefix "Warehouse"
:setvar DefaultDataPath "D:\SQLData\"
:setvar DefaultLogPath "H:\SQLLogs\"

GO
:on error exit
GO
/*
Detect SQLCMD mode and disable script execution if SQLCMD mode is not supported.
To re-enable the script after enabling SQLCMD mode, execute the following:
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'SQLCMD mode must be enabled to successfully execute this script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
PRINT N'Dropping [Control].[ETLController_SYNONYM]...';


GO
DROP SYNONYM [Control].[ETLController_SYNONYM];


GO
PRINT N'Creating [Control].[ETLController_SYNONYM]...';


GO
CREATE SYNONYM [Control].[ETLController_SYNONYM] FOR [$(Dimensional)].[Control].[ETLController];


GO
PRINT N'Altering [Dimension].[Account]...';


GO
ALTER VIEW [Dimension].[Account] AS
SELECT 
	  [AccountKey]
	  , [AccountID]
      ,[PIN]
      ,[CustomerKey]
      ,[CustomerID]
      ,[LedgerID]
      ,[LegacyLedgerID]
      ,[LedgerTypeKey]
      ,[LedgerTypeID]
      ,[LedgerTypeName]
      ,[LedgerGroupingName]
      ,[AccountSequenceNumber] as [LedgerSequenceNumber] --CCIA-6277
      ,[SourceTitle]
      ,[SourceSurname]
      ,[SourceMiddleName]
      ,[SourceFirstName]
      ,[SourceGender]
      ,[Title]
      ,[Surname]
      ,[MiddleName]
      ,[FirstName]
      ,[DOB]
      ,[Gender]
      ,[CompanyKey]
      ,[AccountOpenedDayKey]
      ,[AccountOpenedDate]
      ,[AccountOpenedChannel]
      ,[AddressType]
      ,[SourceAddress1]
      ,[SourceAddress2]
      ,[SourceSuburb]
      ,[SourceStateCode]
      ,[SourceStateName]
      ,[SourcePostCode]
      ,[SourceCountryCode]
      ,[SourceCountryName]
      ,[Address1]
      ,[Address2]
      ,[Suburb]
      ,[StateCode]
      ,[StateName]
      ,[PostCode]
      ,[CountryCode]
      ,[CountryName]
      ,[GeographicalLocation]
      ,[AustralianClient]
      ,[SourcePhone]
      ,[SourceMobile]
      ,[SourceFax]
      ,[SourceEmail]
      ,[SourceAlternateEmail]
      ,[Phone]
      ,[Mobile]
      ,[Fax]
      ,[Email]
      ,[AlternateEmail]
      ,[StatementMethod]
      ,[StatementFrequency]
      ,[BTag2]
      ,[AffiliateID]
      ,[AffiliateName]
      ,[SiteID]
      ,[TrafficSource]
      ,[RefURL]
      ,[CampaignID]
      ,[Keywords]
      ,[FromDate]
      ,[ToDate]
      --,[FirstDate]
      --,[CreatedDate]
      --,[CreatedBatchKey]
      --,[CreatedBy]
      --,[ModifiedDate]
      ,[ModifiedBatchKey]
      --,[ModifiedBy]
	  --Advanced Users Only
		,'AccountSource' = CASE
			When [control].InRole ('Strategy_Analytics') = 1 then AccountSource
			Else ''
			end
	  ,'CustomerSource' = CASE
			When [control].InRole ('Strategy_Analytics') = 1 then CustomerSource
			Else ''
			end
	  ,'LedgerSource' = CASE
			When [control].InRole ('Strategy_Analytics') = 1 then LedgerSource
			Else ''
			end
	  ,'LedgerTypeSource' = CASE
			When [control].InRole ('Strategy_Analytics') = 1 then LedgerTypeSource
			Else ''
			end
  FROM [$(Dimensional)].[Dimension].[Account] with (nolock)
GO
PRINT N'Altering [Dimension].[AccountStatus]...';


GO
ALTER VIEW [Dimension].[AccountStatus]
	AS SELECT 
	[AccountStatusKey]
      ,[AccountKey]
      ,[LedgerID]
      --,[LedgerSource] --CCIA-5786
      ,[LedgerEnabled]
      ,[LedgerStatus] as [LedgerStatusCode] --temp rename till v15.12
      ,[AccountType]
      ,[InternetProfile]
      ,[VIP]
	  ,[CompanyKey]
      ,[CreditLimit]
      ,[MinBetAmount]
      ,[MaxBetAmount]
      ,[Introducer]
      ,[Handled]
      ,[PhoneColourDesc]
      ,[DisableDeposit]
      ,[DisableWithdrawal]
      ,[AccountClosedByUserID]
      --,[AccountClosedByUserSource] --CCIA-5786
      ,[AccountClosedByUserName]
      ,[AccountClosedDate]
      ,[ReIntroducedBy]
      ,[ReIntroducedDate]
      ,[AccountClosureReason]
      ,[DepositLimit]
      ,[LossLimit]
      ,[LimitPeriod]
      ,[LimitTimeStamp]
      ,[ReceiveMarketingEmail]
      ,[ReceiveCompetitionEmail]
      ,[ReceiveSMS]
      ,[ReceivePostalMail]
      ,[ReceiveFax]
      ,[CurrentVersion]
      --,[FromDate]
      --,[ToDate]
      --,[FirstDate]
      --,[CreatedDate]
      --,[CreatedBatchKey]
      --,[CreatedBy]
      --,[ModifiedDate]
      ,[ModifiedBatchKey]
      --,[ModifiedBy]
  FROM [$(Dimensional)].[Dimension].[AccountStatus] with (nolock)
GO
PRINT N'Altering [Dimension].[Activity]...';


GO
ALTER VIEW Dimension.[Activity] 
AS

SELECT [ActivityKey]
      ,[HasDeposited]
      ,[HasWithdrawn]
      ,[HasBet]
      ,[HasWon]
      ,[HasLost]
      --,[FromDate]
      --,[ToDate]
      --,[FirstDate]
      --,[CreatedDate]
      --,[CreatedBatchKey]
      --,[CreatedBy]
      --,[ModifiedDate]
      ,[ModifiedBatchKey]
      --,[ModifiedBy]
FROM [$(Dimensional)].[Dimension].[Activity] with (nolock)
GO
PRINT N'Altering [Dimension].[BalanceType]...';


GO
ALTER view [Dimension].[BalanceType]
AS
SELECT [BalanceTypeKey]
      ,[BalanceTypeID]
      ,[BalanceTypeName]
      --,[FromDate]
      --,[ToDate]
      --,[FirstDate]
      --,[CreatedDate]
      --,[CreatedBatchKey]
      --,[CreatedBy]
      --,[ModifiedDate]
      ,[ModifiedBatchKey]
      --,[ModifiedBy]
  FROM [$(Dimensional)].[Dimension].[BalanceType] with (nolock)
GO
PRINT N'Altering [Dimension].[BetType]...';


GO
ALTER VIEW [Dimension].[BetType]

AS 

SELECT [BetTypeKey]
      ,[BetGroupType]
      ,[BetType]
      ,[BetSubType]
      ,[GroupingType]
      ,[LegType] as PredictionType --CCIA-5789
      --,[FromDate]
      --,[ToDate]
      --,[FirstDate]
      --,[CreatedDate]
      --,[CreatedBatchKey]
      --,[CreatedBy]
      --,[ModifiedDate]
      ,[ModifiedBatchKey]
      --,[ModifiedBy]
  FROM [$(Dimensional)].[Dimension].[BetType] with (nolock)
GO
PRINT N'Altering [Dimension].[Channel]...';


GO
ALTER VIEW [Dimension].[Channel]
	AS SELECT [ChannelKey]
	  --,[ChannelId] surrogate id has no business context --CCIA-5791
	  , ChannelDescription
      ,[ChannelName]
      ,[ChannelTypeName]
      --,[CreatedDate]
      --,[CreatedBatchKey]
      --,[CreatedBy]
      --,[ModifiedDate]
      ,[ModifiedBatchKey]
      --,[ModifiedBy]
  FROM [$(Dimensional)].[DImension].[Channel] with (nolock)
GO
PRINT N'Altering [Dimension].[Class]...';


GO
ALTER view Dimension.Class AS
Select [ClassKey]
      ,[ClassId]
      ,[SourceClassName]
      ,[ClassCode]
      ,[ClassName]
      --,[ClassIcon] --CCIA-5794
      ,[SuperclassKey]
      ,[SuperclassName]
	  , [ModifiedBatchKey]
      --,[FromDate]
      --,[ToDate]
	  --Advanced Users
	  ,'Source' = CASE
			When [control].InRole ('Strategy_Analytics') = 1 then Source
			Else ''
			end
  FROM [$(Dimensional)].Dimension.[Class] with (nolock)
GO
PRINT N'Altering [Dimension].[Company]...';


GO
ALTER view Dimension.Company as
select [CompanyKey]
      ,[OrgId]
      ,[DivisionId]
      ,[DivisionName]
      ,[BrandId]
      ,[BrandCode]
      ,[BrandName]
      ,[BusinessUnitId]
      ,[BusinessUnitCode]
      ,[BusinessUnitName]
      ,[CompanyID]
      ,[CompanyCode]
      ,[CompanyName]
      --,[FromDate]
      --,[ToDate]
      --,[FirstDate]
      --,[CreatedDate]
      --,[CreatedBatchKey]
      --,[CreatedBy]
      --,[ModifiedDate]
      ,[ModifiedBatchKey]
      --,[ModifiedBy]
  FROM [$(Dimensional)].Dimension.[Company] with (nolock)
GO
PRINT N'Altering [Dimension].[Contract]...';


GO
ALTER view [Dimension].[Contract] as
select [ContractKey]
      ,[BetGroupID]   
      ,[BetId]      
      ,[LegId]      
      ,[TransactionID]
      ,[FreeBetID]
      ,[ExternalID]
      ,[External1]
      ,[External2]
      ,[RequestID]
      ,[WithdrawID]
	  --Advanced Users Only
	  ,[BetGroupIDSource]
	  ,[LegIdSource]
	  ,[BetIdSource]
	  ,[TransactionIDSource]
      --,[FromDate]
      --,[ToDate]
      --,[FirstDate]
      --,[CreatedDate]
      --,[CreatedBatchKey]
      --,[CreatedBy]
      --,[ModifiedDate]
      ,[ModifiedBatchKey]
      --,[ModifiedBy]
  FROM [$(Dimensional)].Dimension.[Contract] with (nolock)
GO
PRINT N'Altering [Dimension].[Day]...';


GO
ALTER View [Dimension].[Day] 

as

Select [DayKey]
      ,[DayName] as [DayText] --CCIA-5357
      ,[DayText] as DayID --CCIA-5357
      ,[DayDate]
      ,[DayOfWeekText]
      ,[DayOfWeekNumber]
      ,[DayOfMonthNumber]
      ,[DayOfQuarterNumber]
      ,[DayOfYearNumber]
      ,[DayOfTaxQuarterNumber]
      ,[DayOfTaxYearNumber]
      ,[DayOfFiscalWeekNumber]
      ,[DayOfFiscalMonthNumber]
      ,[DayOfFiscalQuarterNumber]
      ,[DayOfFiscalYearNumber]
      ,[WeekKey]
      ,[WeekName] as [WeekText] --CCIA-5357
      ,[WeekText] as WeekID --CCIA-5357
      ,[WeekStartDate]
      ,[WeekEndDate]
      --,[WeekOfMonthText] CCIA-3459
      --,[WeekOfMonthNumber] CCIA-3459
      ,[WeekOfYearText]
      ,[WeekOfYearNumber]
      ,[WeekYearNumber]
      ,[MonthKey]
      ,[MonthName] as [MonthText] --CCIA-5357
      ,[MonthText] as MonthID --CCIA-5357
      ,[MonthStartDate]
      ,[MonthEndDate]
      ,[MonthDays]
      ,[MonthOfYearText]
      ,[MonthOfYearNumber]
      ,[QuarterKey]
      ,[QuarterName] as [QuarterText] --CCIA-5357
      ,[QuarterText] as QuarterID --CCIA-5357
      ,[QuarterStartDate]
      ,[QuarterEndDate]
      ,[QuarterDays]
      ,[QuarterOfYearText]
      ,[QuarterOfYearNumber]
      ,[YearKey]
      ,[YearName] as [YearText] --CCIA-5357
      ,[YearText] as YearID --CCIA-5357
      ,[YearNumber]
      ,[YearStartDate]
      ,[YearEndDate]
      ,[YearDays]   
      ,[PreviousDayKey]
      ,[PreviousWeekKey]
      ,[PreviousWeekDayKey]
      ,[PreviousMonthKey]
      ,[PreviousMonthDayKey]
      ,[PreviousQuarterKey]
      ,[PreviousQuarterMonthKey]
      ,[PreviousQuarterDayKey]
      ,[PreviousYearKey]
      ,[PreviousYearQuarterKey]
      ,[PreviousYearMonthKey]
      ,[PreviousYearDayKey]
      ,[NextDayKey]
      --,[NextWeekKey] CCIA-4603
      ,[NextWeekDayKey]
      ,[NextMonthKey]
      ,[NextMonthDayKey]
      ,[NextQuarterKey]
      ,[NextQuarterMonthKey]
      ,[NextQuarterDayKey]
      ,[NextYearKey]
      ,[NextYearQuarterKey]
      ,[NextYearMonthKey]
      ,[NextYearDayKey]
	  --For Advanced Users
	  ,[TaxQuarterKey]
      ,[TaxQuarterName] as [TaxQuarterText] --CCIA-5357
      ,[TaxQuarterText] as [TaxQuarterID] --CCIA-5357
      ,[TaxQuarterStartDate]
      ,[TaxQuarterEndDate]
      ,[TaxQuarterDays]
      ,[TaxQuarterOfYearText]
      ,[TaxQuarterOfYearNumber]
      ,[TaxYearKey]
      ,[TaxYearName] as [TaxYearText] --CCIA-5357
      ,[TaxYearText] as TaxYearID --CCIA-5357
      ,[TaxYearNumber]
      ,[TaxYearStartDate]
      ,[TaxYearEndDate]
      ,[TaxYearDays]
      ,[FiscalWeekKey]
      ,[FiscalWeekName] as [FiscalWeekText] --CCIA-5357
      ,[FiscalWeekText] as [FiscalWeekID] --CCIA-5357
      ,[FiscalWeekStartDate]
      ,[FiscalWeekEndDate]
      ,[FiscalWeekOfMonthText]
      ,[FiscalWeekOfMonthNumber]
      ,[FiscalWeekOfYearText]
      ,[FiscalWeekOfYearNumber]
      ,[FiscalMonthKey]
      ,[FiscalMonthName] as [FiscalMonthText] --CCIA-5357
      ,[FiscalMonthText] as [FiscalMonthID] --CCIA-5357
      ,[FiscalMonthStartDate]
      ,[FiscalMonthEndDate]
      ,[FiscalMonthDays]
      ,[FiscalMonthWeeks]
      ,[FiscalMonthOfYearText]
      ,[FiscalMonthOfYearNumber]
      ,[FiscalQuarterKey]
      ,[FiscalQuarterName] as [FiscalQuarterText] --CCIA-5357
      ,[FiscalQuarterText] as [FiscalQuarterID] --CCIA-5357
      ,[FiscalQuarterStartDate]
      ,[FiscalQuarterEndDate]
      ,[FiscalQuarterDays]
      ,[FiscalQuarterWeeks]
      ,[FiscalQuarterOfYearText]
      ,[FiscalQuarterOfYearNumber]
      ,[FiscalYearKey]
      ,[FiscalYearName] as [FiscalYearText] --CCIA-5357
      ,[FiscalYearText] as [FiscalYearID] --CCIA-5357
      ,[FiscalYearNumber]
      ,[FiscalYearStartDate]
      ,[FiscalYearEndDate]
      ,[FiscalYearDays]
      ,[FiscalYearWeeks]
	  ,[PreviousTaxQuarterKey]
      --,[PreviousTaxQuarterMonthKey] CCIA-4603
      ,[PreviousTaxQuarterDayKey]
      ,[PreviousTaxYearKey]
      ,[PreviousTaxYearQuarterKey]
      ,[PreviousTaxYearMonthKey]
      ,[PreviousTaxYearDayKey]
      --,[PreviousFiscalWeekKey] CCIA-4603
      --,[PreviousFiscalWeekDayKey] CCIA-4603
      --,[PreviousFiscalMonthKey] CCIA-4603
      --,[PreviousFiscalMonthDayKey] CCIA-4603
      ,[PreviousFiscalQuarterKey]
      --,[PreviousFiscalQuarterMonthKey] CCIA-4603
      --,[PreviousFiscalQuarterDayKey] CCIA-4603
      --,[PreviousFiscalYearKey] CCIA-4603
      --,[PreviousFiscalYearQuarterKey] CCIA-4603
      --,[PreviousFiscalYearMonthKey] CCIA-4603
      --,[PreviousFiscalYearDayKey] CCIA-4603
	  ,[NextTaxQuarterKey]
      --,[NextTaxQuarterMonthKey] CCIA-4603
      ,[NextTaxQuarterDayKey]
      ,[NextTaxYearKey]
      --,[NextTaxYearQuarterKey] CCIA-4603
      --,[NextTaxYearMonthKey] CCIA-4603
      ,[NextTaxYearDayKey]
      --,[NextFiscalWeekKey] CCIA-4603
      ,[NextFiscalWeekDayKey]
      ,[NextFiscalMonthKey]
      --,[NextFiscalMonthDayKey] CCIA-4603
      ,[NextFiscalQuarterKey]
      ,[NextFiscalQuarterMonthKey]
      --,[NextFiscalQuarterDayKey] CCIA-4603
      ,[NextFiscalYearKey]
      ,[NextFiscalYearQuarterKey]
      --,[NextFiscalYearMonthKey] CCIA-4069
      --,[NextFiscalYearDayKey] CCIA-4069
	  ,ModifiedBatchKey
  FROM [$(Dimensional)].[Dimension].[Day] with (nolock)
GO
PRINT N'Altering [Dimension].[DayZone]...';


GO


ALTER View [Dimension].[DayZone]

as 

SELECT [DayKey]
      ,[MasterDayKey]
      ,[AnalysisDayKey] as SydneyDayKey --CCIA-5796
      ,[MasterDayText]
      ,[AnalysisDayText] as SydneyDayText --CCIA-5796
	  ,[ModifiedBatchKey]
      --,[FromDate]
      --,[ToDate]
  FROM [$(Dimensional)].[Dimension].[DayZone] with (nolock)
GO
PRINT N'Altering [Dimension].[Event]...';


GO
ALTER VIEW [Dimension].[Event]

as

Select [EventKey]     
      ,[EventId]
      ,[ClassID]
      ,[MarketName]
      ,[OfferedDateTime]
      ,[OfferedDayKey]
      ,[OfferedLocalDateTime]
      ,[SuspendedDateTime]
      ,[SuspendedDayKey]
      ,[SuspendedLocalDateTime]
      ,[SettledDateTime]
      ,[SettledDayKey]
      ,[SettledLocalDateTime]
      ,[EventName]
      ,[EventDescription]
      ,[EventType]
      ,[Grade]
      ,[Distance]
      ,[PrizePool]
      ,[Track]
      ,[Weather]
      ,[ScheduledDateTime]
      ,[ScheduledDayKey]
      ,[ScheduledLocalDateTime]
      ,[ResultedDateTime]
      ,[ResultedDayKey]
      ,[ResultedLocalDateTime]
      ,[WinningTime]
      ,[RoundName]
      ,[RoundSequence]
      ,[CompetitionName]
      ,[CompetitionSeason]
      ,[CompetitionStartDate]
      ,[CompetitionEndDate]
      ,[ClassKey]
      ,[VenueName]
      ,[VenueDescription]
      ,[StateCode]
      ,[StateName]
      ,[CountryCode]
      ,[CountryName]
      --,[ColumnLock] this field is only used to identify manual amendments have taken place
	  --Advanced Users Only
	  ,[Source]
	  ,[SourceMarketName]
      ,[SourceEventId]
      ,[SourceEventName]
      ,[SourceParentEventId]
      ,[SourceParentEventName]
      ,[SourceGrandparentEventId]
      ,[SourceGrandparentEventName]
      ,[SourceGreatGrandparentEventId]
      ,[SourceGreatGrandparentEventName]
      ,[SourceEventType]
      ,[SourceEventDate]
      ,[SourceVenueId]
      ,[SourceVenueName]
      --,[FromDate]
      --,[FirstDate]
      --,[CreatedDate]
      --,[CreatedBatchKey]
      --,[CreatedBy]
      --,[ModifiedDate]
      ,[ModifiedBatchKey]
      --,[ModifiedBy]
      ,[SourceSubSportType]
  FROM [$(Dimensional)].[Dimension].[Event] with (nolock)
GO
PRINT N'Altering [Dimension].[InPlay]...';


GO
ALTER VIEW Dimension.InPlay
AS

SELECT [InPlayKey]
      ,[InPlay]
      ,[ClickToCall]
  FROM [$(Dimensional)].[Dimension].[InPlay] with (nolock)
GO
PRINT N'Altering [Dimension].[Instrument]...';


GO
ALTER View [Dimension].[Instrument]

as

SELECT [InstrumentKey]
      ,[InstrumentID]
      ,'PaymentMethodCode' = CASE
			When [control].InRole ('Finance_Compliance_Legal') = 1 then [Source]
			else '***'
			End
      ,'AccountNumber' = CASE
			when [control].InRole ('Finance_Compliance_Legal') = 1 then [AccountNumber]
			else SafeAccountNumber
			End
      ,'AccountName' = CASE
			When [control].InRole ('Finance_Compliance_Legal') = 1 then [AccountName]
			else '***'
			End
      ,'BSB' = CASE
			When [control].InRole ('Finance_Compliance_Legal') = 1 then [BSB]
			else ''
			End
      ,'Verified' = CASE
			When [control].InRole ('Finance_Compliance_Legal') = 1 then [Verified]
			Else '***'
			end
      ,[InstrumentTypeKey] as [PaymentMethodKey]
      ,[InstrumentType] as [PaymentMethod]
      ,[ProviderKey]
      ,'ProviderName' = CASE
			When [control].InRole ('Finance_Compliance_Legal') = 1 then [ProviderName]
			else '***'
			end
		--Advanced Users Only
		,'ExpiryDate' = CASE
			When [control].InRole ('Finance_Compliance_Legal') = 1 then ExpiryDate
			Else '***'
			end
		,'VerifyAmount' = CASE
			When [control].InRole ('Finance_Compliance_Legal') = 1 then VerifyAmount
			Else '***'
			end
      --,[FromDate]
      --,[ToDate]
      --,[FirstDate]
      --,[CreatedDate]
      --,[CreatedBatchKey]
      --,[CreatedBy]
      --,[ModifiedDate]
      ,[ModifiedBatchKey]
      --,[ModifiedBy]
  FROM [$(Dimensional)].[Dimension].[Instrument] with (nolock)
GO
PRINT N'Altering [Dimension].[LegType]...';


GO
ALTER View [Dimension].[LegType]

as

SELECT [LegTypeKey]
      ,[LegTypeName]
      ,[LegNumber]
      ,[NumberOfLegs]
      --,[FromDate]
      --,[ToDate]
      --,[FirstDate]
      --,[CreatedDate]
      --,[CreatedBatchKey]
      --,[CreatedBy]
      --,[ModifiedDate]
      ,[ModifiedBatchKey]
      --,[ModifiedBy]
  FROM [$(Dimensional)].[Dimension].[LegType] with (nolock)
GO
PRINT N'Altering [Dimension].[Market]...';


GO
ALTER View Dimension.Market

as

SELECT  [MarketKey]
		, [Source]
		, [MarketId]
		, [MarketName]
		, [MarketType]
		, [MarketGroup]
		, [LevyRate]
		, [Sport]
      --,[FromDate]
      --,[FirstDate]
      --,[CreatedDate]
      --,[CreatedBatchKey]
      --,[CreatedBy]
      --,[ModifiedDate]
        , [ModifiedBatchKey]
      --,[ModifiedBy]
  FROM [$(Dimensional)].[Dimension].[Market] with (nolock)
GO
PRINT N'Altering [Dimension].[Note]...';


GO
ALTER View [Dimension].[Note]

as


SELECT [NoteKey]
      ,[NoteId]
      ,[Notes]
	  --Advanced Users Only
	  , [NoteSource]
      --,[FromDate]
      --,[ToDate]
      --,[FirstDate]
      --,[CreatedDate]
      --,[CreatedBatchKey]
      --,[CreatedBy]
      --,[ModifiedDate]
      ,[ModifiedBatchKey]
      --,[ModifiedBy]
  FROM [$(Dimensional)].[Dimension].[Note] with (nolock)
GO
PRINT N'Altering [Dimension].[Promotion]...';


GO
ALTER View [Dimension].[Promotion]

as

SELECT [PromotionKey]
      ,[PromotionId]
      ,[Source]
      ,[PromotionType]
      ,[PromotionName]
      ,OfferDate
      ,ExpiryDate
      --,[FromDate]
      --,[ToDate]
      --,[FirstDate]
      --,[CreatedDate]
      --,[CreatedBatchKey]
      --,[CreatedBy]
      --,[ModifiedDate]
      ,[ModifiedBatchKey]
      --,[ModifiedBy]
  FROM [$(Dimensional)].[Dimension].[Promotion] with (nolock)
GO
PRINT N'Altering [Dimension].[Time]...';


GO

ALTER View Dimension.[Time]

as 

SELECT [TimeKey]
      ,TimeText
      ,[Time]
      ,[TimeOfHourNumber]
      ,[TimeOfDayNumber]
      ,HourText
      ,[HourOfDayNumber]
      ,[PeriodText]
	  --Advanced Users Only
      ,[TimeStartTime]
      ,[TimeEndTime]
      ,[TimeStartTimestamp]
      ,[TimeEndTimestamp]
	  ,[HourStartTime]
      ,[HourEndTime]
	  ,TimeID
	  ,[HourKey]
	  ,HourID
	  ,[PeriodKey]
      --,[FromDate]
      --,[ToDate]
      --,[FirstDate]
      --,[CreatedDate]
      --,[CreatedBatchKey]
      --,[CreatedBy]
      --,[ModifiedDate]
      ,[ModifiedBatchKey]
      --,[ModifiedBy]
  FROM [$(Dimensional)].[Dimension].[Time] with (nolock)
GO
PRINT N'Altering [Dimension].[TransactionDetail]...';


GO
ALTER View [Dimension].[TransactionDetail]

as 


SELECT [TransactionDetailKey]
      ,[TransactionDetailId]
      ,[TransactionTypeID]
      ,[TransactionType]
      ,[TransactionStatusID]
      ,[TransactionStatus]
      ,[TransactionMethodID]
      ,[TransactionMethod]
      ,[TransactionDirection]
      ,[TransactionSign]
      --,[FromDate]
      --,[ToDate]
      --,[FirstDate]
      --,[CreatedDate]
      --,[CreatedBatchKey]
      --,[CreatedBy]
      --,[ModifiedDate]
      ,[ModifiedBatchKey]
      --,[ModifiedBy]
  FROM [$(Dimensional)].[Dimension].[TransactionDetail] with (nolock)
GO
PRINT N'Altering [Dimension].[User]...';


GO
ALTER View [Dimension].[User]

as

SELECT [UserKey]
      ,[UserId]
      ,[SystemName]
      ,[EmployeeKey]
      ,[Forename]
      ,[Surname]
      --,[PayrollNumber] CCIA-4477
	  --For Advanced Users
	  ,[Source]
      --,[FromDate]
      --,[ToDate]
      --,[FirstDate]
      --,[CreatedDate]
      --,[CreatedBatchKey]
      --,[CreatedBy]
      --,[ModifiedDate]
      ,[ModifiedBatchKey]
      --,[ModifiedBy]
  FROM [$(Dimensional)].[Dimension].[User] with (nolock)
GO
PRINT N'Altering [Dimension].[ValueTiers]...';


GO
ALTER VIEW [Dimension].[ValueTiers]

as

Select		[TierKey]
			,[TierNumber]
			,[TierName]
			--,[FromDate]
			--,[ToDate]
From		[$(Dimensional)].Dimension.ValueTiers with (nolock)
GO
PRINT N'Altering [Dimension].[Wallet]...';


GO
ALTER View [Dimension].[Wallet]

as

SELECT   [WalletKey],
    [WalletId],
    [WalletName],
    --[FromDate],
    --[ToDate],
    --[FirstDate],
    --[CreatedDate],
    --[CreatedBatchKey],
    --[CreatedBy],
    --[ModifiedDate],
    [ModifiedBatchKey]
    --[ModifiedBy]
  FROM [$(Dimensional)].[Dimension].[Wallet] with (nolock)
GO
PRINT N'Altering [Fact].[Balance]...';


GO
ALTER VIEW [Fact].[Balance] as
SELECT [AccountKey],
	  [AccountStatusKey]
      ,[LedgerID]
      ,[LedgerSource]
      ,[DayKey]
      ,[BalanceTypeKey]
      ,[WalletKey]
      ,[OpeningBalance]
      ,[TotalTransactedAmount]
      ,[ClosingBalance]
      ,[ModifiedBatchKey]
  FROM [$(Dimensional)].[Fact].[Balance] with (nolock)
GO
PRINT N'Altering [Fact].[KPI]...';


GO
ALTER view [Fact].[KPI] as
select [ContractKey]
      ,[DayKey]
      ,[AccountKey]
	  ,[AccountStatusKey]
      ,[BetTypeKey]
      ,[LegTypeKey]
      ,[ChannelKey]
      ,[CampaignKey]
      ,[MarketKey]
	  ,[EventKey]
      ,[WalletKey]
      ,[ClassKey]
      ,[UserKey]
      ,[InstrumentKey]
	  ,[InPlayKey]
      ,[AccountOpened] as AccountsOpened --CCIA=5219
	  ,[FirstDeposit] as FirstTimeDepositors --CCIA=5219
      ,[FirstBet] as FirstTimeBettors --CCIA=5219
      ,[DepositsRequested]
      ,[DepositsCompleted]
      ,[Promotions]
      ,[Transfers]
      ,[BetsPlaced]
      ,[ContractsPlaced]
      ,[BettorsPlaced]
	  ,[PlayDaysPlaced]
	  ,[PlayFiscalWeeksPlaced]
      ,[PlayCalenderWeeksPlaced]
      ,[PlayFiscalMonthsPlaced]
      ,[PlayCalenderMonthsPlaced]
      ,[Stakes] as TurnoverPlaced--CCIA=5219
      ,[Winnings] as Payout--CCIA=5219
      ,[WithdrawalsRequested]
      ,[WithdrawalsCompleted]
      ,[Adjustments]
      ,[BetsResulted] as BetsSettled --CCIA=5219
      ,[ContractsResulted]
      ,[BettorsResulted] as BettorsSettled --CCIA=5219
	  ,[PlayDaysResulted]
	  ,[PlayFiscalWeeksResulted]
      ,[PlayCalenderWeeksResulted]
      ,[PlayFiscalMonthsResulted]
      ,[PlayCalenderMonthsResulted]
      ,[Turnover] as TurnoverSettled--CCIA=5219
      ,[GrossWin]
      ,[NetRevenue]
	  ,[NetRevenueAdjustment]
      --,[BonusWinnings] as BonusGrosswin --CCIA=5219
      ,[Betback] as BetbackGrosswin--CCIA=5219
      ,[GrossWinAdjustment] as GrosswinResettledAdjustment --CCIA=5219
      --,[CreatedDate]
      --,[CreatedBatchKey]
      --,[CreatedBy]
      --,[ModifiedDate]
      ,[ModifiedBatchKey]
      --,[ModifiedBy]
  FROM [$(Dimensional)].Fact.[KPI] with (nolock)
GO
PRINT N'Altering [Fact].[Position]...';


GO
ALTER view [Fact].[Position] as
select [BalanceTypeKey]
      ,[AccountKey]
	  ,[AccountStatusKey]
      ,[LedgerID]
      ,[LedgerSource]
      ,[DayKey]
      ,[ActivityKey]
      ,[FirstDepositDayKey]
      ,[FirstBetDayKey]
      ,[FirstBetTypeKey]
      ,[FirstClassKey] as FirstBetClassKey --CCIA_6168
      ,[FirstChannelKey] as FirstBetChannelKey --CCIA_6168
      ,[LastBetDayKey]
      ,[LastBetTypeKey]
      ,[LastClassKey] as LastBetClassKey --CCIA_6168
      ,[LastChannelKey] as LastBetChannelKey --CCIA_6168
      ,[OpeningBalance]
      ,[ClosingBalance]
      ,[TransactedAmount]
      ,[LifetimeTurnover]
      ,[LifetimeGrossWin]
	  ,[LifetimeBetCount]
	  ,[LifetimeBonus]
	  ,[LifetimeDeposits]
	  ,[LifetimeWithdrawals]
	  ,[TurnoverMTDCalender]
	  ,[TurnoverMTDFiscal]
	  ,[TierKey]
	  ,[TierKeyFiscal]
	  ,DaysSinceAccountOpened
	  ,DaysSinceFirstBet
	  ,DaysSinceFirstDeposit
	  ,DayssinceLastBet
	  ,DaysSinceLastDeposit
	  ,RevenueLifeCycleKey
	  ,PreviousLifeStageKey
	  ,LifeStageKey
	  ,LastDepositDayKey

      --,[CreatedDate]
      --,[CreatedBatchKey]
      --,[ModifiedDate]
      ,[ModifiedBatchKey]
  FROM [$(Dimensional)].Fact.[Position] with (nolock)
GO
PRINT N'Altering [Fact].[Transaction]...';


GO
ALTER view [Fact].[Transaction] as
select [TransactionId]
      ,[TransactionIdSource]
      ,[TransactionDetailKey]
      ,[BalanceTypeKey]
      ,[ContractKey]
      ,[AccountKey]
	  ,[AccountStatusKey]
      ,[WalletKey]
	  ,[PromotionKey]
      ,[MarketKey]
	  ,[EventKey]
      ,[DayKey]
      ,[MasterTransactionTimestamp] as [DarwinTransactionTimestamp] --CCIA-6170
      ,[MasterTimeKey] as [DarwinTimeKey] --CCIA-6170
      ,[AnalysisTransactionTimestamp] as [SydneyTransactionTimestamp] --CCIA-6170
      ,[AnalysisTimeKey] as [SydneyTimeKey] --CCIA-6170
      ,[CampaignKey] 
      ,[ChannelKey]
      ,[LedgerID]
      ,[TransactedAmount]
      ,[BetTypeKey]
      ,[LegTypeKey]
	  ,[PriceTypeKey]
      ,[ClassKey]
      ,[UserKey]
      ,[InstrumentKey]
	  ,[NoteKey]
	  ,'InPlayKey' = CASE
		When [BetTypeKey] = 0 then 0
		Else [InPlayKey]
		End
      --,[FromDate]
      --,[CreatedDate]
      ,[CreatedBatchKey] as ModifiedBatchKey
      --,[CreatedBy]
  FROM [$(Dimensional)].Fact.[Transaction] with (nolock)
GO
PRINT N'Creating [Dimension].[LifeStage]...';


GO
CREATE VIEW Dimension.LifeStage
AS

SELECT		[LifeStageKey],
			[LifeStage]
FROM		[$(Dimensional)].[Dimension].LifeStage with (nolock)
GO
PRINT N'Creating [Dimension].[RevenueLifeCycle]...';


GO
CREATE VIEW [Dimension].[RevenueLifeCycle]

AS

SELECT		[RevenueLifeCycleKey]
			,[RevenueLifeCycle]
FROM		[$(Dimensional)].[Dimension].[RevenueLifeCycle] with (nolock)
GO
PRINT N'Creating [Dimension].[LifeStage].[LifeStage]...';


GO
EXECUTE sp_addextendedproperty @name = N'LifeStage', @value = N'Marketing grouping based on the time since the client signed-up or last bet', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'VIEW', @level1name = N'LifeStage';


GO
PRINT N'Creating [Dimension].[RevenueLifeCycle].[RevenueLifeCycle]...';


GO
EXECUTE sp_addextendedproperty @name = N'RevenueLifeCycle', @value = N'Revenue grouping based on the time since the client last had a settled bet', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'VIEW', @level1name = N'RevenueLifeCycle';


GO
PRINT N'Creating [Fact].[Position].[LifeStageKey]...';


GO
EXECUTE sp_addextendedproperty @name = N'LifeStageKey', @value = N'Link to the LifeStage dimension for the current day', @level0type = N'SCHEMA', @level0name = N'Fact', @level1type = N'VIEW', @level1name = N'Position';


GO
PRINT N'Creating [Fact].[Position].[PreviousLifeStageKey]...';


GO
EXECUTE sp_addextendedproperty @name = N'PreviousLifeStageKey', @value = N'Link to the LifeStage dimension that was valid on the previous day', @level0type = N'SCHEMA', @level0name = N'Fact', @level1type = N'VIEW', @level1name = N'Position';


GO
PRINT N'Creating [Fact].[Position].[RevenueLifeCycleKey]...';


GO
EXECUTE sp_addextendedproperty @name = N'RevenueLifeCycleKey', @value = N'Link to the RevenueLifeCycle dimension for the current month', @level0type = N'SCHEMA', @level0name = N'Fact', @level1type = N'VIEW', @level1name = N'Position';


GO
/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

/*Version Info */
Insert into [$(Dimensional)].[Control].[Version] values ('$(VersionNumber)', 'Warehouse', getdate(), SUser_Name())

--Synonyms
DROP SYNONYM [Control].[ETLController_SYNONYM]
GO

Create SYNONYM [Control].[ETLController_SYNONYM] FOR [$(Staging)].[Control].[ETLController]
GO

GO

GO
PRINT N'Update complete.';


GO

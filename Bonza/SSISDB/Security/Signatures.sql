﻿ADD SIGNATURE TO OBJECT::[internal].[check_is_role]
    BY CERTIFICATE [MS_SQLISSigningCertificate] WITH SIGNATURE = 0x1D224B8B2DF642B4B10D77077E12256B9D562E10F567F78F7669D8DECD06C2788581D97A36693E42996DDA515ED0B54EEA5444766C9973A8D4688BD832882B4C334B40F74C9074CA8917128AB2C1BEFCE9091AED560691BEFA422AE3AC11405040B3864A2E59D8A5CF9DFCCB62C72584E4F7600CD649600D4D1036D049E21651;


GO
ADD SIGNATURE TO OBJECT::[internal].[deploy_project_internal]
    BY CERTIFICATE [MS_SQLISSigningCertificate] WITH SIGNATURE = 0x574E5B9BCD6D13D63ECF0EFE03B9CEED142A760096EA511388117D058FEE44621127BB2E6ED29E7C15A6FBCDF144D89C4ECC1CF37769FAD75F93F72D9C8810EB7744318FB77D8CCE4DF02ED67A6F891BCE833E2397543E8B6FE8E04B83A2E8AA5595BCBD64C79C992F130055B6B362F98BED11C30FDA4A935C437569EA211365;


GO
ADD SIGNATURE TO OBJECT::[internal].[set_system_informations]
    BY CERTIFICATE [MS_SQLISSigningCertificate] WITH SIGNATURE = 0x830AB24EB07B6BAD0E0477F48EAF6012B15CBDB9EB6C74D7D3858705A213A755363C6045EC8C7F9F1A4FED531E0FFABB9433ECE1D60E3FFE4E4A11E555ED7FDD5F5F63F01831E85B9BDD0803DD83F719FBDEC204714DC89FA8AA70CF0110AA0E7834DF9F3BE296814816D84ADAA1CE3D9A4C86899ED9934A1B0670AADF67FE36;


GO
ADD SIGNATURE TO OBJECT::[internal].[start_execution_internal]
    BY CERTIFICATE [MS_SQLISSigningCertificate] WITH SIGNATURE = 0xCF2453B9713DB1F9505AE318E6E5CB03175DDD4AEC013A79FA59CDF4782C5CF75C5CBBF32B2008AF39D9A362BD08C9726A1DA70549764824FD35A95E375EB68A6E65B0ADC3E9A4970F413D436EB228586D333C44FFB7AA416B873370C97C38441FC20963748C130E0097A0BB40399E7249284FFADD06F0A4E3F9C5DFF91D3598;


GO
ADD SIGNATURE TO OBJECT::[internal].[stop_operation_internal]
    BY CERTIFICATE [MS_SQLISSigningCertificate] WITH SIGNATURE = 0x17903F53BC9C3BD9D121CF5AF82B5A322F76DE3305EF05BC277A38DA39EE6260960A960F45470F20ADD3153ED8A4B796FC5E83ADD4DDF93AD16DF218017189F9024C3B122D976791DFDE6B780D5F1E5C8CE113E79D113C276CF4A1D40081BECDFB026C7DDE9E9378DCA800966E64508021ECF10E19F1DD4B566BF00473CDD320;


GO
ADD SIGNATURE TO OBJECT::[internal].[validate_package_internal]
    BY CERTIFICATE [MS_SQLISSigningCertificate] WITH SIGNATURE = 0x130E0C7B82E20EADA9BF6E64B7AD02691DE6B33564D8901E66878ED54A73957CF82DD7525626EE50E62ADE1310312949C64085A9B29D742C87C503EAE3C211D70D8BE05E1BF2540CDD11D409F0A2EA7E4CCEBDDF0B60670036527B0BF7183C3F23D41815B8AF1107B7D6C51DFEC5D91077D72B69FFAB95D5B2C359413C9C5E1C;


GO
ADD SIGNATURE TO OBJECT::[internal].[validate_project_internal]
    BY CERTIFICATE [MS_SQLISSigningCertificate] WITH SIGNATURE = 0x8869A9599564AFD010567BBAD26F29B8FC29798894C7267ABD29F630DBED89D60AEC448D0406DD104CBDBBDB0C167992666C862AB8E2649ADB81367A70816D4D421174125A4863F5C62A339CCF5688D1AE0C66C58D1ED5CD414D6655E9CCC42CE342C53BE3AD35BEA6ACDCD48EC7914945925ACE4D7853A4D05BFA811960948F;


GO
ADD SIGNATURE TO OBJECT::[internal].[check_schema_version_internal]
    BY CERTIFICATE [MS_SQLISSigningCertificate] WITH SIGNATURE = 0xB3C000C414DEFB60B6800DDDD09AA9E1706D22109DA0BA880A49583C9D5E23549E6DE5A3D2A3FC5375B9C1CCA1481BFC00BF3195453CF3CAFF2F5D167E5FDBAE5EB5A562B0012F8416E5C91A8AF385F2D11A68C145945A6B2FC522C9BEB4EE04F13450A0E593F3753651F0B385BEA616B9200C7E0774BF5EE6582DE9A49C7A64;


GO
ADD SIGNATURE TO OBJECT::[internal].[get_database_principals]
    BY CERTIFICATE [MS_SQLISSigningCertificate] WITH SIGNATURE = 0x24B2FD85772423558BBFABCE0AA67684129A854BE59A85F2B593009ABFA6D1114611C72415711698D3CC73145613525623B01595C1AF0CE784B9C4F16C1D2D992B53705C066F67399CF6755DFAF728B38ED01819EE8856221DDF02F7CD9DC2F7822D2A6E57C45F2879BE00C04B79EB342C4B6E6552EFC7417083FC2FB04CE64C;


GO
ADD SIGNATURE TO OBJECT::[internal].[get_space_used]
    BY CERTIFICATE [MS_SQLISSigningCertificate] WITH SIGNATURE = 0x79D804B68B3F2E7860F40ADDC4ABBF3BFE57166171C7D2B95958150BCEB7FFEC3C742D5C4221F51CC5730A86F9B5D85C2406F22FD3C369E4B0F098ED6029CDA7DFC070CBE1EC3C6DE34EA99423BE35D14901A01EF25F19A61CC52A6FA5E06E3675DC7F1F526745793FFC4EEF0203C6556CA2CAF4453C9227BC6824F7C0065C91;


GO
ADD SIGNATURE TO OBJECT::[internal].[get_principal_id_by_sid]
    BY CERTIFICATE [MS_SQLISSigningCertificate] WITH SIGNATURE = 0x25E3039323999854719A6566A82A2CB738D3804A652E17F264AA4039C57288645C45595FDF7F8FBC61F94D6257965ADAA7710B3DAED6E635B827DC9B654D6CA9A3592D9E573A3A0B13AFD63F5949C965358C3C7D452E0564934459EE4A6CA6F77A5A8DD42E97D45160D6CC114B65C7AC57FBC41BE35AB4B8CB4FD89049689B62;


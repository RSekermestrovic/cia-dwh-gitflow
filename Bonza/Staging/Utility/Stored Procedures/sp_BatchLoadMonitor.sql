﻿CREATE Procedure utility.[sp_BatchLoadMonitor]
as
SELECT A.[LogID]
      ,A.[BatchKey]
         ,B.ProcessName
      ,A.[Procedure]
      ,A.[Step]
      ,A.[Status]
      ,A.[StartDateTime]
      ,A.[EndDateTime]
         ,DATEDIFF(s,A.StartDateTime,ISNULL(A.EndDateTime,GETDATE())) as [Elapsed_s]
      ,A.[ErrorMessage]
	  ,A.[RowsProcessed]
  	  , B.SequenceID
FROM [Control].[Log] AS A
  LEFT JOIN [Control].[ETLProcessMapping] AS B on A.[Procedure]=B.ProcedureName
  ORDER BY a.LogID DESC


﻿CREATE PROCEDURE utility.[sp_ColdStart_TestData]

AS

exec utility.[sp_ColdStart] '2016-05-15 00:00:56.777', '2016-05-16 00:00:58.327', 1, 1


SET IDENTITY_INSERT [Control].[ETLController] ON												-- Switch ON Identity_Insert	

INSERT INTO [Control].[ETLController] (	BatchKey,BatchStatus,BatchFromDate,BatchToDate,BatchStartDate)
SELECT 1, 0, '2016-05-15 00:00:56.777', '2016-05-16 00:00:58.327', GETDATE()

INSERT INTO [Control].[ETLController] (	BatchKey,BatchStatus,BatchFromDate,BatchToDate,BatchStartDate)
SELECT 2, 0, '2016-05-16 00:00:58.327', '2016-05-17 00:00:58.327', GETDATE()

INSERT INTO [Control].[ETLController] (	BatchKey,BatchStatus,BatchFromDate,BatchToDate,BatchStartDate)
SELECT 3, 0, '2016-05-17 00:00:58.327', '2016-05-18 00:00:58.327', GETDATE()

INSERT INTO [Control].[ETLController] (	BatchKey,BatchStatus,BatchFromDate,BatchToDate,BatchStartDate)
SELECT 4, 0, '2016-05-18 00:00:58.327', '2016-05-19 00:00:58.327', GETDATE()

INSERT INTO [Control].[ETLController] (	BatchKey,BatchStatus,BatchFromDate,BatchToDate,BatchStartDate)
SELECT 5, 0, '2016-05-19 00:00:58.327', '2016-05-20 00:00:58.327', GETDATE()

INSERT INTO [Control].[ETLController] (	BatchKey,BatchStatus,BatchFromDate,BatchToDate,BatchStartDate)
SELECT 6, 0, '2016-05-20 00:00:58.327', '2016-05-21 00:00:58.327', GETDATE()

INSERT INTO [Control].[ETLController] (	BatchKey,BatchStatus,BatchFromDate,BatchToDate,BatchStartDate)
SELECT 7, 0, '2016-05-21 00:00:58.327', '2016-05-22 00:00:58.327', GETDATE()

INSERT INTO [Control].[ETLController] (	BatchKey,BatchStatus,BatchFromDate,BatchToDate,BatchStartDate)
SELECT 8, 0, '2016-05-22 00:00:58.327', '2016-05-23 00:00:58.327', GETDATE()
	
SET IDENTITY_INSERT [Control].[ETLController] OFF

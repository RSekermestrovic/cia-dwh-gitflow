﻿


CREATE PROC [Utility].[Sp_GetAtomicRowcountsByBatch]
	@BatchKey INT
AS
BEGIN
/*
	Developer: Aaron Jackson
	Date: 03/06/2015
	Desc: Returns row counts for a given batch by table
	Exec: EXEC [Utility].[Sp_GetAtomicRowcountsByBatch] @BatchKey = 0;
*/

	SET NOCOUNT ON;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT @@servername as [Server], '[Atomic].[Bet]' as [Table], BatchKey, COUNT(*) as [Rows]
	FROM [Atomic].[Bet]
	WHERE BatchKey = @BatchKey
	GROUP BY BatchKey
	UNION ALL
	SELECT @@servername as [Server], '[Atomic].[OpeningBalance]', BatchKey, COUNT(*) as [Rows]
	FROM [Atomic].[OpeningBalance]
	WHERE BatchKey = @BatchKey
	GROUP BY BatchKey
	UNION ALL
	SELECT @@servername as [Server], '[Atomic].[PendingWithdrawal]', BatchKey, COUNT(*) as [Rows]
	FROM [Atomic].[PendingWithdrawal]
	WHERE BatchKey = @BatchKey
	GROUP BY BatchKey
	UNION ALL
	SELECT @@servername as [Server], '[Atomic].[Unsettled]', BatchKey, COUNT(*) as [Rows]
	FROM [Atomic].[Unsettled]
	WHERE BatchKey = @BatchKey
	GROUP BY BatchKey
	ORDER BY [Table]

END
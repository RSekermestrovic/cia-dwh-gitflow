﻿


CREATE PROC [Utility].[Sp_RevokeBatch]
	@AreYouSure CHAR(1) = 'N', -- Leave the safety catch on, this must be Y for anything to be purged
	@BatchKey int
AS
BEGIN
/*
	Developer: Aaron Jackson
	Date: 13/05/2015
	Desc: Remove all non-default data. Good for cleansing test environments.
	Exec: [Utility].[Sp_RevokeBatch] @AreYouSure = 'Y'
*/

	SET NOCOUNT ON;

	IF @AreYouSure = 'Y'
	BEGIN

	PRINT 'This is not yet implemented'

	-- Switch data out to switch table
	-- truncate table
	-- switch partition back in?

	END
	ELSE
		PRINT '@AreYouSure was set to ''N''. No data was purged';

END
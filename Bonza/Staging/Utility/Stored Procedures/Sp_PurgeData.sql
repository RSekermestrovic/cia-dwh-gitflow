﻿

CREATE PROC [Utility].[Sp_PurgeData]
	@AreYouSure CHAR(1) = 'N', -- Leave the safety catch on, this must be Y for anything to be purged
	@Schema VARCHAR(255) = 'Atomic, Stage, Centrebet', -- Comma separated list of schemas to truncate
	@IncludeControl CHAR(1) = 'N' -- default is no
AS
BEGIN
/*
	Developer: Aaron Jackson
	Date: 13/05/2015
	Desc: Remove all non-default data. Good for cleansing test environments.
	Exec: EXEC [Utility].[Sp_PurgeData] @AreYouSure = 'Y', @Schema = 'Atomic,Stage', @IncludeControl = 'Y';
*/

	SET NOCOUNT ON;

	IF @AreYouSure = 'Y'
	BEGIN

		DECLARE @SQL NVARCHAR(255), @TableName VARCHAR(255);

		IF @IncludeControl = 'Y'
		BEGIN
			TRUNCATE TABLE [Control].[ETLController];
			PRINT 'TRUNCATE TABLE [Control].[ETLController]'
			TRUNCATE TABLE [Control].[Log];
			PRINT 'TRUNCATE TABLE [Control].[Log]'
		END

		DECLARE tableCursor CURSOR
		FOR 
			SELECT '[' + TABLE_SCHEMA + '].[' + TABLE_NAME + ']'
			FROM information_schema.tables
			WHERE TABLE_SCHEMA IN (SELECT Value FROM [Utility].[SplitString](@Schema))
		FOR READ ONLY;

		--Open the cursor
		OPEN tableCursor

		--Get the first table name from the cursor
		FETCH NEXT FROM tableCursor INTO @TableName

		--Loop until the cursor was not able to fetch
		WHILE (@@Fetch_Status >= 0)
		BEGIN
			SET @SQL = 'TRUNCATE TABLE ' + @TableName;
			PRINT @SQL;
			EXEC sp_executesql @SQL;
			FETCH NEXT FROM tableCursor INTO @TableName
		END

		--Get rid of the cursor
		CLOSE tableCursor
		DEALLOCATE tableCursor;

	END
	ELSE
		PRINT '@AreYouSure was set to ''N''. No data was purged';

END
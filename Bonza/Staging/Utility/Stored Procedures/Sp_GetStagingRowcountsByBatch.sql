﻿


CREATE PROC [Utility].[Sp_GetStagingRowcountsByBatch]
	@BatchKey INT
AS
BEGIN
/*
	Developer: Aaron Jackson
	Date: 03/06/2015
	Desc: Returns row counts for a given batch by table
	Exec: EXEC [Utility].[Sp_GetStagingRowcountsByBatch] @BatchKey = 0;
*/

	SET NOCOUNT ON;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT @@servername as [Server], '[Stage].[Account]' as [Table], BatchKey, COUNT(*) as [Rows]
	FROM [Stage].[Account]
	WHERE BatchKey = @BatchKey
	GROUP BY BatchKey
	UNION ALL
	SELECT @@servername as [Server], '[Stage].[Activity]' as [Table], BatchKey, COUNT(*) as [Rows]
	FROM [Stage].[Activity]
	WHERE BatchKey = @BatchKey
	GROUP BY BatchKey
	UNION ALL
	SELECT @@servername as [Server], '[Stage].[Balance]' as [Table], BatchKey, COUNT(*) as [Rows]
	FROM [Stage].[Balance]
	WHERE BatchKey = @BatchKey
	GROUP BY BatchKey
	UNION ALL
	SELECT @@servername as [Server], '[Stage].[BetLeg]' as [Table], BatchKey, COUNT(*) as [Rows]
	FROM [Stage].[BetLeg]
	WHERE BatchKey = @BatchKey
	GROUP BY BatchKey
	UNION ALL
	SELECT @@servername as [Server], '[Stage].[BetStatus]' as [Table], BatchKey, COUNT(*) as [Rows]
	FROM [Stage].[BetStatus]
	WHERE BatchKey = @BatchKey
	GROUP BY BatchKey
	UNION ALL
	SELECT @@servername as [Server], '[Stage].[BetType]' as [Table], BatchKey, COUNT(*) as [Rows]
	FROM [Stage].[BetType]
	WHERE BatchKey = @BatchKey
	GROUP BY BatchKey
	UNION ALL
	SELECT @@servername as [Server], '[Stage].[Campaign]' as [Table], BatchKey, COUNT(*) as [Rows]
	FROM [Stage].[Campaign]
	WHERE BatchKey = @BatchKey
	GROUP BY BatchKey
	UNION ALL
	SELECT @@servername as [Server], '[Stage].[Channel]' as [Table], BatchKey, COUNT(*) as [Rows]
	FROM [Stage].[Channel]
	WHERE BatchKey = @BatchKey
	GROUP BY BatchKey
	UNION ALL
	--SELECT @@servername as [Server], '[Stage].[Day]' as [Table], BatchKey, COUNT(*) as [Rows]
	--FROM [Stage].[Day]
	--WHERE BatchKey = @BatchKey
	--GROUP BY BatchKey
	--UNION ALL
	SELECT @@servername as [Server], '[Stage].[Instrument]' as [Table], BatchKey, COUNT(*) as [Rows]
	FROM [Stage].[Instrument]
	WHERE BatchKey = @BatchKey
	GROUP BY BatchKey
	UNION ALL
	SELECT @@servername as [Server], '[Stage].[Leg]' as [Table], BatchKey, COUNT(*) as [Rows]
	FROM [Stage].[Leg]
	WHERE BatchKey = @BatchKey
	GROUP BY BatchKey
	UNION ALL
	SELECT @@servername as [Server], '[Stage].[LegType]' as [Table], BatchKey, COUNT(*) as [Rows]
	FROM [Stage].[LegType]
	WHERE BatchKey = @BatchKey
	GROUP BY BatchKey
	UNION ALL
	SELECT @@servername as [Server], '[Stage].[Limit]' as [Table], BatchKey, COUNT(*) as [Rows]
	FROM [Stage].[Limit]
	WHERE BatchKey = @BatchKey
	GROUP BY BatchKey
	UNION ALL
	SELECT @@servername as [Server], '[Stage].[LocalDay]' as [Table], BatchKey, COUNT(*) as [Rows]
	FROM [Stage].[LocalDay]
	WHERE BatchKey = @BatchKey
	GROUP BY BatchKey
	UNION ALL
	SELECT @@servername as [Server], '[Stage].[Event]' as [Table], BatchKey, COUNT(*) as [Rows]
	FROM [Stage].[Event]
	WHERE BatchKey = @BatchKey
	GROUP BY BatchKey
	UNION ALL
	SELECT @@servername as [Server], '[Stage].[Note]' as [Table], BatchKey, COUNT(*) as [Rows]
	FROM [Stage].[Note]
	WHERE BatchKey = @BatchKey
	GROUP BY BatchKey
	UNION ALL
	SELECT @@servername as [Server], '[Stage].[Part]' as [Table], BatchKey, COUNT(*) as [Rows]
	FROM [Stage].[Part]
	WHERE BatchKey = @BatchKey
	GROUP BY BatchKey
	UNION ALL
	SELECT @@servername as [Server], '[Stage].[Position]' as [Table], BatchKey, COUNT(*) as [Rows]
	FROM [Stage].[Position]
	WHERE BatchKey = @BatchKey
	GROUP BY BatchKey
	UNION ALL
	SELECT @@servername as [Server], '[Stage].[Price]' as [Table], BatchKey, COUNT(*) as [Rows]
	FROM [Stage].[Price]
	WHERE BatchKey = @BatchKey
	GROUP BY BatchKey
	UNION ALL
	SELECT @@servername as [Server], '[Stage].[Risk]' as [Table], BatchKey, COUNT(*) as [Rows]
	FROM [Stage].[Risk]
	WHERE BatchKey = @BatchKey
	GROUP BY BatchKey
	UNION ALL
	SELECT @@servername as [Server], '[Stage].[Selection]' as [Table], BatchKey, COUNT(*) as [Rows]
	FROM [Stage].[Selection]
	WHERE BatchKey = @BatchKey
	GROUP BY BatchKey
	UNION ALL
	SELECT @@servername as [Server], '[Stage].[Time]' as [Table], BatchKey, COUNT(*) as [Rows]
	FROM [Stage].[Time]
	WHERE BatchKey = @BatchKey
	GROUP BY BatchKey
	UNION ALL
	SELECT @@servername as [Server], '[Stage].[Transaction]' as [Table], BatchKey, COUNT(*) as [Rows]
	FROM [Stage].[Transaction]
	WHERE BatchKey = @BatchKey
	GROUP BY BatchKey
	UNION ALL
	SELECT @@servername as [Server], '[Stage].[TransactionDetail]' as [Table], BatchKey, COUNT(*) as [Rows]
	FROM [Stage].[TransactionDetail]
	WHERE BatchKey = @BatchKey
	GROUP BY BatchKey
	UNION ALL
	SELECT @@servername as [Server], '[Stage].[User]' as [Table], BatchKey, COUNT(*) as [Rows]
	FROM [Stage].[User]
	WHERE BatchKey = @BatchKey
	GROUP BY BatchKey
	ORDER BY [Table]

END
﻿CREATE TABLE [Audit].[TableSize] (
    [SnapshotDate]       DATETIME      NOT NULL,
    [DatabaseName]       VARCHAR (100) NOT NULL,
    [SchemaName]         VARCHAR (100) NOT NULL,
    [TableName]          VARCHAR (100) NOT NULL,
    [RecordCount]        BIGINT        NULL,
    [ReservedStorage_KB] FLOAT (53)    NULL,
    [DataStorage_KB]     FLOAT (53)    NULL,
    [IndexStorage_KB]    FLOAT (53)    NULL,
    [UnusedStorage_KB]   FLOAT (53)    NULL,
    PRIMARY KEY CLUSTERED ([SnapshotDate] ASC, [DatabaseName] ASC, [SchemaName] ASC, [TableName] ASC)
);


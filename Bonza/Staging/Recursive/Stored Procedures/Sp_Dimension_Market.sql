﻿/*
	Developer: Aaron Jackson
	Date: 24/08/2015
	Desc: Search for dimensions that are missing from dimension table, but exist in fact table. Insert stub records into dimension table.
		
	Test: EXEC [Recursive].[Sp_Dimension_Market] @BatchKey = 305;
*/

CREATE PROC [Recursive].[Sp_Dimension_Market]
(
	@BatchKey	INT,
	@RowsProcessed	INT = 0 OUTPUT
)
AS 
BEGIN TRY

	DECLARE @ProcName VARCHAR(255) = (SELECT OBJECT_NAME(@@PROCID));

	DECLARE @RowCount int

	--SET STATISTICS IO ON;
	--SET STATISTICS TIME ON;

	SET ANSI_NULLS ON;
	SET QUOTED_IDENTIFIER ON;
	SET NOCOUNT ON;

	EXEC [Control].Sp_Log @BatchKey, @ProcName, 'Get Missing Keys','Start';
	
	-- Create new records based on -1 record
	SELECT
		 @BatchKey as [BatchKey] 
		,[Source]
		,[MarketId]
		,[Market]
		,[FromDate]
    INTO #DefaultUnknown
    FROM [$(Dimensional)].[Dimension].[Market]
    WHERE MarketKey = -1;

	-- Get list of natural keys from stage.transaction
	-- check natural keys exist in dimension

	SELECT DISTINCT
		S.MarketID,
		S.MarketIDSource AS Source,
		S.FromDate
	INTO #Dim
	FROM Stage.[Transaction] S
	LEFT OUTER JOIN [$(Dimensional)].Dimension.Market DM ON S.MarketID = DM.MarketID AND S.MarketIDSource = DM.Source
	WHERE S.ExistsInDim = 0
	AND DM.MarketKey IS NULL
	AND S.BatchKey = @BatchKey
	OPTION(RECOMPILE);

	EXEC [Control].Sp_Log @BatchKey, @ProcName, 'Insert into Staging','Start', @RowsProcessed = @@ROWCOUNT;

	-- insert missing keys into staging.account table
	INSERT INTO Stage.Market ([BatchKey] 
		,[Source]
		,[MarketId]
		,[Market]
		,[FromDate]) 
	SELECT DISTINCT
		 @BatchKey as [BatchKey]
		,D.[Source]
		,D.[MarketId]
		,U.[Market]
		,U.[FromDate]
	FROM #Dim as D
	LEFT JOIN Stage.Market as S on S.MarketId = D.MarketId AND S.Source = D.Source AND S.BatchKey = @BatchKey
	CROSS APPLY (SELECT * FROM #DefaultUnknown) as U
	WHERE S.MarketId IS NULL
	OPTION(RECOMPILE);

	SET @RowCount  = @@ROWCOUNT;
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey, @ProcName, NULL,'Success', @RowsProcessed = @RowCount;
	
END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, @ProcName, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
﻿/*
	Developer: Aaron Jackson
	Date: 24/08/2015
	Desc: Search for dimensions that are missing from dimension table, but exist in fact table. Insert stub records into dimension table.
		
	Test: EXEC [Recursive].[Sp_Dimension_Campaign] @BatchKey = 305;
*/

CREATE PROC [Recursive].[Sp_Dimension_Campaign]
(
	@BatchKey	INT,
	@RowsProcessed	INT = 0 OUTPUT
)
AS 
BEGIN TRY

	DECLARE @ProcName VARCHAR(255) = (SELECT OBJECT_NAME(@@PROCID));

	DECLARE @RowCount int

	--SET STATISTICS IO ON;
	--SET STATISTICS TIME ON;

	SET ANSI_NULLS ON;
	SET QUOTED_IDENTIFIER ON;
	SET NOCOUNT ON;

	EXEC [Control].Sp_Log @BatchKey, @ProcName, 'Get Missing Keys','Start';
	
	-- Get list of natural keys from stage.transaction
	-- check natural keys exist in dimension

	-- Create new records based on -1 record
	SELECT
		 @BatchKey as [BatchKey] 
		,[CampaignId]
		,[CampaignName]
		,[CampaignComment]
		,[CampaignActive]
		,[CampaignTypeId]
		,[CampaignTypeName]
		,[CampaignTypeActive]
    INTO #DefaultUnknown
    FROM [$(Dimensional)].[Dimension].[Campaign]
    WHERE CampaignKey = -1;

	SELECT DISTINCT
		S.CampaignId,
		--S.CampaignIdSource,
		S.FromDate
	INTO #Dim
	FROM Stage.[Transaction] S
	LEFT OUTER JOIN [$(Dimensional)].Dimension.Campaign	DCK ON S.CampaignId = DCK.CampaignID
	WHERE S.ExistsInDim = 0
	AND S.CampaignId <> -1
	AND DCK.CampaignKey IS NULL
	AND S.BatchKey = @BatchKey
	OPTION(RECOMPILE);

	EXEC [Control].Sp_Log @BatchKey, @ProcName, 'Insert into Staging','Start', @RowsProcessed = @@ROWCOUNT;

	-- insert missing keys into staging.account table
	INSERT INTO Stage.Campaign ([BatchKey] 
		,[CampaignId]
		,[CampaignName]
		,[CampaignComment]
		,[CampaignActive]
		,[CampaignTypeId]
		,[CampaignTypeName]
		,[CampaignTypeActive]) 
	SELECT
		 U.[BatchKey] 
		,D.[CampaignId]
		,U.[CampaignName]
		,U.[CampaignComment]
		,U.[CampaignActive]
		,U.[CampaignTypeId]
		,U.[CampaignTypeName]
		,U.[CampaignTypeActive]
	FROM #Dim as D
	LEFT JOIN Stage.Campaign as S on S.CampaignId = D.CampaignId AND S.BatchKey = @BatchKey
	CROSS APPLY (SELECT * FROM #DefaultUnknown) as U
	WHERE S.CampaignId IS NULL
	OPTION(RECOMPILE);

	SET @RowCount  = @@ROWCOUNT;
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey, @ProcName, NULL,'Success', @RowsProcessed = @RowCount;
	
END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, @ProcName, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
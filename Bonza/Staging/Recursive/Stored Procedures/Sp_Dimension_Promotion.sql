﻿/*
	Developer: Aaron Jackson
	Date: 24/08/2015
	Desc: Search for dimensions that are missing from dimension table, but exist in fact table. Insert stub records into dimension table.
		
	Test: EXEC [Recursive].[Sp_Dimension_Promotion] @BatchKey = 305;
*/

CREATE PROC [Recursive].[Sp_Dimension_Promotion]
(
	@BatchKey	INT,
	@RowsProcessed	INT = 0 OUTPUT
)
AS 
BEGIN TRY

	DECLARE @ProcName VARCHAR(255) = (SELECT OBJECT_NAME(@@PROCID));

	DECLARE @RowCount int

	--SET STATISTICS IO ON;
	--SET STATISTICS TIME ON;

	SET ANSI_NULLS ON;
	SET QUOTED_IDENTIFIER ON;
	SET NOCOUNT ON;

	EXEC [Control].Sp_Log @BatchKey, @ProcName, 'Get Missing Keys','Start';
	
	-- Create new records based on -1 record
	SELECT
		 @BatchKey as [BatchKey] 
		,[PromotionKey]
		,[PromotionId]
		,[Source]
		,[PromotionType]
		,[PromotionName]
		,[OfferDate]
		,[ExpiryDate]
		,[FromDate]
    INTO #DefaultUnknown
    FROM [$(Dimensional)].[Dimension].[Promotion]
    WHERE PromotionKey = -1;

	-- Get list of natural keys from stage.transaction
	-- check natural keys exist in dimension

	SELECT DISTINCT
		S.PromotionId,
		S.PromotionIDSource,
		S.FromDate
	INTO #Dim
	FROM Stage.[Transaction] S
	LEFT OUTER JOIN [$(Dimensional)].Dimension.Promotion DPK ON S.PromotionId = DPK.PromotionId AND S.PromotionIDSource = DPK.Source
	WHERE S.ExistsInDim = 0
	AND s.PromotionId <> -1
	AND DPK.PromotionKey IS NULL
	AND S.BatchKey = @BatchKey
	OPTION(RECOMPILE);

	EXEC [Control].Sp_Log @BatchKey, @ProcName, 'Insert into Staging','Start', @RowsProcessed = @@ROWCOUNT;

	-- insert missing keys into staging.account table
	INSERT INTO Stage.Promotion ([BatchKey]
	  ,[PromotionId]
      ,[PromotionType]
      ,[PromotionName]
      ,[OfferDate]
      ,[ExpiryDate]
      ,[Source]) 
	SELECT DISTINCT
		@BatchKey as [BatchKey]
		,D.[PromotionId]
		,U.[PromotionType]
		,U.[PromotionName]
		,U.[OfferDate]
		,U.[ExpiryDate]
		,D.[PromotionIDSource]
	FROM #Dim as D
	LEFT JOIN Stage.Promotion as S on S.PromotionId = D.PromotionId AND S.Source = D.PromotionIDSource AND S.BatchKey = @BatchKey
	CROSS APPLY (SELECT * FROM #DefaultUnknown) as U
	WHERE S.PromotionId IS NULL
	OPTION(RECOMPILE);

	SET @RowCount  = @@ROWCOUNT;
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey, @ProcName, NULL,'Success', @RowsProcessed = @RowCount;
	
END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, @ProcName, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
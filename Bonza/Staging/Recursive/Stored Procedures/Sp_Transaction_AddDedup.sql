﻿CREATE PROC [Recursive].[Sp_Transaction_AddDedup]
(
	@BatchKey	INT
)
AS 

BEGIN
/*
	Developer: Joseph George
	Date: Unknown
	Desc: Find unbalanced withdrawals in the transaction data set

	Previous Changed By: Aaron Jackson
	Previous Changed Date: 02/03/2015
	Previous Change Desc: Tidy up code, remove hardcoding, performance tweaks
	Added CTE to replace subquery

	Last Changed By: Edward Chen
	Last Changed Date: 06/JUL/2015
	Last Change Desc: Remove TransactionDetailID Dependency
	
	Test: EXEC [Intrabet].[Sp_Rec_AddDedup] @BatchKey = 258;
	Test: EXEC [PreProd].[Sp_Rec_AddDedup] @BatchKey = 258;
*/
	DECLARE @ProcName VARCHAR(255) = (SELECT OBJECT_NAME(@@PROCID));

	--SET STATISTICS IO ON;
	--SET STATISTICS TIME ON;

	SET ANSI_NULLS ON;
	SET QUOTED_IDENTIFIER ON;
	SET NOCOUNT ON;
		
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
		
	IF OBJECT_ID('tempdb..#Dim') IS NOT NULL BEGIN DROP TABLE #Dim END;

	BEGIN TRY

		DECLARE @RowsProcessed INT, @RowsAffected INT

		-- *** Note all of this should be superfluous once the other staging processes are debugged thoroughly

		------------------------------- REMOVE DUPLICATES within staging ------------------------------------------------

		EXEC [Control].Sp_Log @BatchKey, @ProcName, 'StagingDuplicates','Start';

		UPDATE	s			
		SET		ExistsInDim = 19
		FROM	(	SELECT	BatchKey, ExistsInDim,
							ROW_NUMBER() OVER(PARTITION BY TransactionID, TransactionIDSource, MasterTransactionTimestamp, TransactionTypeID, TransactionStatusID, TransactionMethodID, TransactionDirection, BalanceTypeID, LegNumber ORDER BY  A.TransactionID,A.TransactionIdSource) RowNum 
					FROM [Stage].[Transaction] A
					WHERE BatchKey = @BatchKey AND ExistsInDim = 0																					
				) s
		WHERE	RowNum > 1
				AND BatchKey = @BatchKey
		OPTION (RECOMPILE);

		SET @RowsProcessed = @@ROWCOUNT;
		SET @RowsAffected = @RowsProcessed;

		------------------------------- Compare Staging and Dimension to Remove Duplicates  -----------------------------------------

		EXEC [Control].Sp_Log @BatchKey, @ProcName, 'CacheDim', 'Start', @RowsProcessed = @RowsProcessed;

		SELECT	t.TransactionID, t.TransactionIdSource, b.BalanceTypeID, d.TransactionTypeID, d.TransactionStatusID, d.TransactionMethodID, d.TransactionDirection, l.LegNumber, l.NumberOfLegs, t.MasterTransactionTimestamp
		INTO	#Dim
		FROM	(	SELECT	DISTINCT TransactionId, TransactionIdSource 
					FROM	[Stage].[Transaction] 
					WHERE	BatchKey = @BatchKey AND ExistsInDim = 0
				) s
				INNER LOOP JOIN [$(Dimensional)].[Fact].[Transaction] t ON t.TransactionId = s.TransactionId AND t.TransactionIdSource = s.TransactionIdSource	AND t.CreatedBatchKey < @BatchKey 
				INNER JOIN [$(Dimensional)].[Dimension].TransactionDetail d ON d.TransactionDetailKey=t.TransactionDetailKey
				INNER JOIN [$(Dimensional)].[Dimension].LegType l ON l.LegTypeKey=t.LegTypeKey
				INNER JOIN [$(Dimensional)].[Dimension].BalanceType b ON b.BalanceTypeKey=t.BalanceTypeKey
		OPTION (RECOMPILE);


		EXEC [Control].Sp_Log @BatchKey, @ProcName, 'CompareStagingAndDim', 'Start', @RowsProcessed = @@ROWCOUNT;

		UPDATE	s
		SET		ExistsInDim = 20
		FROM	[Stage].[Transaction] s
				INNER HASH JOIN #Dim d ON (d.TransactionId = s.TransactionId AND d.TransactionIdSource = s.TransactionIdSource
										AND d.TransactionTypeID = s.TransactionTypeID AND d.TransactionStatusID = s.TransactionStatusID
										AND d.TransactionMethodID = s.TransactionMethodID AND d.TransactionDirection = s.TransactionDirection AND d.BalanceTypeID = s.BalanceTypeID AND d.LegNumber = s.LegNumber AND d.NumberOfLegs = s.NumberOfLegs
										AND d.MasterTransactionTimestamp = s.MasterTransactionTimestamp)
		WHERE	s.BatchKey = @BatchKey AND s.ExistsInDim = 0
		OPTION (RECOMPILE);

		SET @RowsProcessed = @@ROWCOUNT;
		SET @RowsAffected = @RowsAffected + @RowsProcessed;

		------------------------------- Finished  -----------------------------------------
		EXEC [Control].Sp_Log	@BatchKey, @ProcName, NULL, 'Success', @RowsProcessed = @RowsProcessed;
		
		DROP TABLE #Dim
	
	END TRY
	BEGIN CATCH

		THROW;

		DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

		EXEC [Control].Sp_Log @BatchKey, @ProcName, NULL, 'Failed', @ErrorMessage;

	END CATCH

END

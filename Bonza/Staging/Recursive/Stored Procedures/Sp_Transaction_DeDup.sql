﻿CREATE PROC [Recursive].[Sp_Transaction_DeDup]
(
	@BatchKey	INT
)
AS 

BEGIN
/*
	Developer: Joseph George
	Date: Unknown
	Desc: Deduplicate data (which data?)

	Previous Changed By: Richard Hudson
	Previous Changed Date: 5/4/2015
	Previous Change Desc: Simplification and Permance Improvement

	Last Changed By: Edward Chen
	Last Changed Date: 6/JUL/2015
	Last Change Desc: Remove TransactionDetailID Dependency
	
	Test: EXEC [Intrabet].[Sp_Rec_DeDup] @BatchKey = x

*/
	DECLARE @ProcName VARCHAR(255) = (SELECT OBJECT_NAME(@@PROCID));

	--SET STATISTICS IO ON;
	--SET STATISTICS TIME ON;

	SET ANSI_NULLS ON;
	SET QUOTED_IDENTIFIER ON;
	SET NOCOUNT ON;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
		
	IF OBJECT_ID('tempdb..#Dim') IS NOT NULL BEGIN DROP TABLE #Dim END;

	BEGIN TRY

		DECLARE @RowsProcessed INT, @RowsAffected INT

		------------------------------- IGNORE CASES --------------------------------

		EXEC [Control].Sp_Log @BatchKey, @ProcName, 'IgnoreCases','Start';

		UPDATE	s
		SET		s.ExistsInDim = 1
		FROM	Stage.[Transaction] s
		WHERE	s.Conditional = -1
				AND s.BatchKey = @BatchKey
		OPTION (RECOMPILE);

		SET @RowsProcessed = @@ROWCOUNT;
		SET @RowsAffected = @RowsProcessed;

		------------------------------- DONT MOVE Transactions that dont affect any Balances ------------------------------------------

		EXEC [Control].Sp_Log @BatchKey, @ProcName, 'NoBalanceAffect','Start', @RowsProcessed = @RowsProcessed;

		UPDATE	s
		SET		s.ExistsInDim = 2
		FROM	Stage.[Transaction] s
		WHERE	s.BalanceTypeID = ''
				AND s.BatchKey = @BatchKey
		OPTION (RECOMPILE);

		SET @RowsProcessed = @@ROWCOUNT;
		SET @RowsAffected = @RowsAffected + @RowsProcessed;

		------------------------------- REMOVE DUPLICATES DUE TO MULTIPLE Mapping Types ------------------------------------------------

		EXEC [Control].Sp_Log @BatchKey, @ProcName, 'MultipleMappingType','Start', @RowsProcessed = @RowsProcessed;

		UPDATE	s			
		SET		ExistsInDim = 3
		FROM	(	SELECT	BatchKey, ExistsInDim,
							ROW_NUMBER() OVER(PARTITION BY TransactionID, TransactionIDSource, MasterTransactionTimestamp, TransactionTypeID,TransactionStatusID,TransactionMethodID,TransactionDirection, BalanceTypeID, LegNumber ORDER BY FromDate) RowNum 
					FROM Stage.[Transaction] A
					WHERE ExistsInDim = 0 AND BatchKey = @BatchKey																					
				) s
		WHERE	RowNum > 1
				AND BatchKey = @BatchKey
		OPTION (RECOMPILE);

		SET @RowsProcessed = @@ROWCOUNT;
		SET @RowsAffected = @RowsAffected + @RowsProcessed;

		------------------------------- Compare Staging and Dimension to Remove Duplicates  -----------------------------------------

		EXEC [Control].Sp_Log @BatchKey, @ProcName, 'CacheDim', 'Start', @RowsProcessed = @RowsProcessed;

		SELECT	t.TransactionID, t.TransactionIdSource, b.BalanceTypeID, d.TransactionTypeID, d.TransactionStatusID, d.TransactionMethodID, d.TransactionDirection, l.LegNumber, l.NumberOfLegs, t.MasterTransactionTimestamp
		INTO	#Dim
		FROM	(	SELECT	DISTINCT TransactionId, TransactionIdSource 
					FROM	[Stage].[Transaction] 
					WHERE	BatchKey = @BatchKey AND ExistsInDim = 0
				) s
				INNER LOOP JOIN [$(Dimensional)].[Fact].[Transaction] t on t.TransactionId = s.TransactionId AND t.TransactionIdSource = s.TransactionIdSource	AND t.CreatedBatchKey < @BatchKey 
				INNER JOIN [$(Dimensional)].Dimension.TransactionDetail d ON d.TransactionDetailKey=t.TransactionDetailKey
				INNER JOIN [$(Dimensional)].Dimension.LegType l ON l.LegTypeKey=t.LegTypeKey
				INNER JOIN [$(Dimensional)].Dimension.BalanceType b ON b.BalanceTypeKey=t.BalanceTypeKey
		OPTION (RECOMPILE);


		EXEC [Control].Sp_Log @BatchKey, @ProcName, 'CompareStagingAndDim', 'Start', @RowsProcessed = @@ROWCOUNT;

		UPDATE	s
		SET		ExistsInDim = 4
		FROM	Stage.[Transaction] s
				INNER HASH JOIN #Dim d ON (d.TransactionId = s.TransactionId AND d.TransactionIdSource = s.TransactionIdSource
										AND d.TransactionTypeID = s.TransactionTypeID AND d.TransactionStatusID = s.TransactionStatusID
										AND d.TransactionMethodID = s.TransactionMethodID AND d.TransactionDirection = s.TransactionDirection AND d.BalanceTypeID = s.BalanceTypeID AND d.LegNumber = s.LegNumber AND d.NumberOfLegs = s.NumberOfLegs
										AND (d.MasterTransactionTimestamp = s.MasterTransactionTimestamp 
											OR 
											(d.TransactionTypeID = 'WI' and d.TransactionStatusID NOT IN ('CO','RJ','RV'))
											OR
											(d.TransactionTypeID = 'BT' and d.TransactionStatusID = 'AC' and d.TransactionMethodID = 'SY')
											)) -- SPECIFIC LOGIC FOR Withdrawals Ignoring MasterTransactionTimestamp
		WHERE	s.BatchKey = @BatchKey AND s.ExistsInDim = 0
		OPTION (RECOMPILE);

		SET @RowsProcessed = @@ROWCOUNT;
		SET @RowsAffected = @RowsAffected + @RowsProcessed;

		------------------------------- Finished  -----------------------------------------
		EXEC [Control].Sp_Log	@BatchKey, @ProcName, NULL, 'Success', @RowsProcessed = @RowsProcessed;
		
		DROP TABLE #Dim
	
	END TRY
	BEGIN CATCH

		THROW;

	END CATCH
END

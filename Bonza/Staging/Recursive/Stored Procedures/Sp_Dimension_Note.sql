﻿/*
	Developer: Aaron Jackson
	Date: 24/08/2015
	Desc: Search for dimensions that are missing from dimension table, but exist in fact table. Insert stub records into dimension table.
		
	Test: EXEC [Recursive].[Sp_Dimension_Note] @BatchKey = 305;
*/


-- Does note need a recursive? I think the answer is no

CREATE PROC [Recursive].[Sp_Dimension_Note]
(
	@BatchKey	INT,
	@RowsProcessed	INT = 0 OUTPUT
)
AS 
BEGIN TRY

	DECLARE @ProcName VARCHAR(255) = (SELECT OBJECT_NAME(@@PROCID));

	DECLARE @RowCount int

	--SET STATISTICS IO ON;
	--SET STATISTICS TIME ON;

	SET ANSI_NULLS ON;
	SET QUOTED_IDENTIFIER ON;
	SET NOCOUNT ON;

	EXEC [Control].Sp_Log @BatchKey, @ProcName, 'Get Missing Keys','Start';
	
	-- Create new records based on -1 record
	SELECT
		 @BatchKey as [BatchKey] 
		,[NoteId]
		,[NoteSource]
		,[Notes]
		,[FromDate]
    INTO #DefaultUnknown
    FROM [$(Dimensional)].[Dimension].[Note]
    WHERE NoteKey = -1;

	-- Get list of natural keys from stage.transaction
	-- check natural keys exist in dimension

	SELECT DISTINCT
		S.TransactionId AS NoteID,
		--S.NoteIdSource,
		S.FromDate
	INTO #Dim
	FROM Stage.[Transaction] S
	LEFT OUTER JOIN [$(Dimensional)].Dimension.Note DN ON S.TransactionId = DN.NoteID 
	WHERE S.ExistsInDim = 0
	AND DN.NoteKey IS NULL
	AND S.BatchKey = @BatchKey
	OPTION(RECOMPILE);

	EXEC [Control].Sp_Log @BatchKey, @ProcName, 'Insert into Staging','Start', @RowsProcessed = @@ROWCOUNT;

	-- insert missing keys into staging.account table
	INSERT INTO Stage.Note ([BatchKey]
	  ,[NoteId]
      ,[NoteSource]
      ,[Notes]
      ,[FromDate]) 
	SELECT DISTINCT
		 @BatchKey as [BatchKey]
		,D.[NoteId]
		,U.[NoteSource]
		,U.[Notes]
		,U.[FromDate]
	FROM #Dim as D
	LEFT JOIN Stage.Note as S on S.NoteID = D.NoteID AND S.BatchKey = @BatchKey
	CROSS APPLY (SELECT * FROM #DefaultUnknown) as U
	WHERE S.NoteID IS NULL
	OPTION(RECOMPILE);

	SET @RowCount  = @@ROWCOUNT;
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey, @ProcName, NULL,'Success', @RowsProcessed = @RowCount;
	
END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, @ProcName, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
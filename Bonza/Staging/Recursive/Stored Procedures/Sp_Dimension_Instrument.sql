﻿/*
	Developer: Aaron Jackson
	Date: 24/08/2015
	Desc: Search for dimensions that are missing from dimension table, but exist in fact table. Insert stub records into dimension table.
		
	Test: EXEC [Recursive].[Sp_Dimension_Instrument] @BatchKey = 305;
*/

CREATE PROC [Recursive].[Sp_Dimension_Instrument]
(
	@BatchKey	INT,
	@RowsProcessed	INT = 0 OUTPUT
)
AS 
BEGIN TRY

	DECLARE @ProcName VARCHAR(255) = (SELECT OBJECT_NAME(@@PROCID));

	DECLARE @RowCount int

	--SET STATISTICS IO ON;
	--SET STATISTICS TIME ON;

	SET ANSI_NULLS ON;
	SET QUOTED_IDENTIFIER ON;
	SET NOCOUNT ON;

	EXEC [Control].Sp_Log @BatchKey, @ProcName, 'Get Missing Keys','Start';
	
	-- Create new records based on -1 record
	SELECT
		 @BatchKey as [BatchKey] 
		,[InstrumentId]
		,[Source]
		,[AccountNumber]
		,[SafeAccountNumber]
		,[AccountName]
		,[BSB]
		,[ExpiryDate]
		,[Verified]
		,[VerifyAmount]
		,[InstrumentType]
		,[ProviderName]
		,[FromDate]
    INTO #DefaultUnknown
    FROM [$(Dimensional)].[Dimension].[Instrument]
    WHERE InstrumentKey = -1;

	-- Get list of natural keys from stage.transaction
	-- check natural keys exist in dimension

	SELECT DISTINCT
		S.InstrumentID,
		S.InstrumentIDSource AS Source,
		S.FromDate
	INTO #Dim
	FROM Stage.[Transaction] S
	LEFT OUTER JOIN [$(Dimensional)].Dimension.Instrument DIK ON S.InstrumentID = DIK.InstrumentID AND S.InstrumentIDSource = DIK.Source
	WHERE S.ExistsInDim = 0
	AND	S.InstrumentId <> '-1'
	AND DIK.InstrumentKey IS NULL
	AND S.BatchKey = @BatchKey
	OPTION(RECOMPILE);

	EXEC [Control].Sp_Log @BatchKey, @ProcName, 'Insert into Staging','Start', @RowsProcessed = @@ROWCOUNT;

	-- insert missing keys into staging.account table
	INSERT INTO Stage.Instrument ([BatchKey]
		,[InstrumentId]
		,[Source]
		,[AccountNumber]
		,[SafeAccountNumber]
		,[AccountName]
		,[BSB]
		,[ExpiryDate]
		,[Verified]
		,[VerifyAmount]
		,[InstrumentType]
		,[ProviderName]
		,[FromDate]) 
	SELECT DISTINCT
		 U.[BatchKey]
		,D.[InstrumentId]
		,D.[Source]
		,U.[AccountNumber]
		,U.[SafeAccountNumber]
		,U.[AccountName]
		,U.[BSB]
		,U.[ExpiryDate]
		,U.[Verified]
		,U.[VerifyAmount]
		,U.[InstrumentType]
		,U.[ProviderName]
		,U.[FromDate]
	FROM #Dim as D
	LEFT JOIN Stage.Instrument as S on S.InstrumentId = D.InstrumentId AND S.Source = D.Source AND S.BatchKey = @BatchKey
	CROSS APPLY (SELECT * FROM #DefaultUnknown) as U
	WHERE S.InstrumentId IS NULL
	OPTION(RECOMPILE);

	SET @RowCount  = @@ROWCOUNT;
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey, @ProcName, NULL,'Success', @RowsProcessed = @RowCount;
	
END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, @ProcName, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
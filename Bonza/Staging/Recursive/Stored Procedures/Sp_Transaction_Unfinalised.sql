CREATE PROC [Recursive].[Sp_Transaction_Unfinalised]
(
	@BatchKey	INT
)
AS
BEGIN TRY
/*
	Developer: Joseph George
	Date: Unknown
	Desc: Unfinalised

	Last Changed By: Edward Chen
	Last Changed Date: 6/JUL/2015
	Last Change Desc: Remove TransactionDetailID Dependency
*/

-- EXEC [Intrabet].[Sp_Rec_Unfinalised] @BatchKey = 306; 


	DECLARE @ProcName VARCHAR(255) = (SELECT OBJECT_NAME(@@PROCID));

	--SET STATISTICS IO ON;
	--SET STATISTICS TIME ON;

	--DECLARE @BatchKey INT = 278;

	SET ANSI_NULLS ON;
	SET QUOTED_IDENTIFIER ON;
	SET NOCOUNT ON;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
		
	BEGIN TRY DROP TABLE #Pool							END TRY BEGIN CATCH END CATCH
	BEGIN TRY DROP TABLE #UnfinaliseCandidates			END TRY BEGIN CATCH END CATCH
	BEGIN TRY DROP TABLE #PartialCancellationCandidates	END TRY BEGIN CATCH END CATCH
	BEGIN TRY DROP TABLE #UnfinaliseDates				END TRY BEGIN CATCH END CATCH
	BEGIN TRY DROP TABLE #Adjustments					END TRY BEGIN CATCH END CATCH

	

	DECLARE @RowsProcessed INT, @RowsAffected INT

	------------------------------- Create a pool of all staged transactions and any related earlier transactions from Dim  -----------------------------------------

	EXEC [Control].Sp_Log @BatchKey, @ProcName, 'StageAndDimPool', 'Start';

	SELECT DISTINCT BetId, BetIdSource
	INTO #StgBets 
	FROM [Stage].[Transaction] 
	WHERE BatchKey = @BatchKey AND ExistsInDim = 0
	OPTION (RECOMPILE);

	SELECT  Src, TransactionId, TransactionIdSource, BalanceTypeId, TransactionTypeID, TransactionStatusID, TransactionMethodID, TransactionDirection,
			BetGroupID, BetGroupIDSource, BetId, BetIdSource, LegId, LegIdSource, FreeBetID, ExternalID, External1, External2,
			UserId, UserIdSource, InstrumentId, InstrumentIdSource, LedgerID, LedgerSource, WalletId, PromotionId, PromotionIdSource,
			MasterTransactionTimestamp, MasterDayText, MasterTimeText,
			Channel, ActionChannel, CampaignId, TransactedAmount, BetGroupType, BetTypeName, BetSubTypeName, LegBetTypeName, GroupingType, 
			PriceTypeID, PriceTypeSource, ClassId, ClassIdSource, EventID, EventIDSource, MarketId, MarketIdSource,
			LegNumber, NumberOfLegs, FromDate, InPlay, ClickToCall, CashoutType, IsDoubleDown, IsDoubledDown
	INTO	#Pool
	FROM	(	SELECT  'S' Src, TransactionId, TransactionIdSource, BalanceTypeId, TransactionTypeID, TransactionStatusID, TransactionMethodID, TransactionDirection,
						BetGroupID, BetGroupIDSource, BetId, BetIdSource, LegId, LegIdSource, FreeBetID, ExternalID, External1, External2,
						UserId, UserIdSource, InstrumentId, InstrumentIdSource, LedgerID, LedgerSource, WalletId, PromotionId, PromotionIdSource,
						MasterTransactionTimestamp, MasterDayText, MasterTimeText,
						Channel, ActionChannel, CampaignId, TransactedAmount, BetGroupType, BetTypeName, BetSubTypeName, LegBetTypeName, GroupingType, 
						PriceTypeID, PriceTypeSource, ClassId, ClassIdSource, EventID, EventIDSource, MarketId, MarketIdSource,
						LegNumber, NumberOfLegs, FromDate, InPlay, ClickToCall, CashoutType, IsDoubleDown, IsDoubledDown
				FROM	[Stage].[Transaction] 
				WHERE	BatchKey = @BatchKey AND ExistsInDim = 0
				UNION ALL
				SELECT  'D' Src, t.TransactionId, t.TransactionIdSource, b.BalanceTypeId, td.TransactionTypeID, td.TransactionStatusID, td.TransactionMethodID, td.TransactionDirection,
						c.BetGroupID, c.BetGroupIDSource, c.BetId, c.BetIdSource, c.LegId, c.LegIdSource, c.FreeBetID, c.ExternalID, c.External1, c.External2,
						u.UserId, u.Source, CONVERT(VARCHAR(50), i.InstrumentId), i.Source InstrumentIdSource, a.LedgerID, a.LedgerSource, w.WalletId, p.PromotionId, p.Source PromotionIdSource,
						t.MasterTransactionTimestamp, d.MasterDayText MasterDayText, ti.TimeText MasterTimeText,
						ch.ChannelId, ac.ChannelId, ca.CampaignId, t.TransactedAmount, bt.BetGroupType, bt.BetType BetTypeName, bt.BetSubType BetSubTypeName, bt.LegType LegBetTypeName, bt.GroupingType, 
						pt.PriceTypeKey, pt.Source, cl.ClassId, cl.Source ClassIdSource, e.EventID, e.Source EventIdSource, m.MarketId, m.Source MarketIdSource,
						L.LegNumber,L.NumberOfLegs, t.FromDate, ip.InPlay, ip.ClickToCall, ip.CashoutType, ip.IsDoubleDown, ip.IsDoubledDown
				FROM	#StgBets s  
						INNER LOOP JOIN [$(Dimensional)].Dimension.[Contract] c ON (c.BETID = s.BETID AND c.BetIdSource = s.BetIdSource AND c.CreatedBatchKey < @BatchKey)
						INNER LOOP JOIN [$(Dimensional)].Fact.[Transaction] t ON (t.ContractKey = c.ContractKey AND t.CreatedBatchKey < @BatchKey)
						INNER LOOP JOIN [$(Dimensional)].Dimension.DayZone d ON (d.DayKey = t.DayKey)
						INNER LOOP JOIN [$(Dimensional)].Dimension.[Time] ti ON (ti.TimeKey = t.MasterTimeKey)
						INNER LOOP JOIN [$(Dimensional)].Dimension.TransactionDetail td ON (td.TransactionDetailKey = t.TransactionDetailKey)
						INNER LOOP JOIN [$(Dimensional)].Dimension.BalanceType b ON (b.BalanceTypeKey = t.BalanceTypeKey)
						INNER LOOP JOIN [$(Dimensional)].Dimension.Channel ch ON (ch.ChannelKey = t.ChannelKey)
						INNER LOOP JOIN [$(Dimensional)].Dimension.Channel ac ON (ac.ChannelKey = t.ActionChannelKey)
						INNER LOOP JOIN [$(Dimensional)].Dimension.Campaign ca ON (ca.CampaignKey = t.CampaignKey)
						INNER LOOP JOIN [$(Dimensional)].Dimension.Account a ON (a.AccountKey = t.AccountKey)
						INNER LOOP JOIN [$(Dimensional)].Dimension.Wallet w ON (w.WalletKey = t.WalletKey)
						INNER LOOP JOIN [$(Dimensional)].Dimension.Promotion p ON (t.PromotionKey = p.PromotionKey)
						INNER LOOP JOIN [$(Dimensional)].Dimension.BetType bt ON (bt.BetTypeKey = t.BetTypeKey)
						INNER LOOP JOIN [$(Dimensional)].Dimension.PriceType pt ON (pt.PriceTypeKey = t.PriceTypeKey)
						INNER LOOP JOIN [$(Dimensional)].Dimension.Market m ON (m.MarketKey = t.MarketKey)
						INNER LOOP JOIN [$(Dimensional)].Dimension.[User] u ON (u.UserKey = t.UserKey)
						INNER LOOP JOIN [$(Dimensional)].Dimension.Instrument i ON (i.InstrumentKey = t.InstrumentKey)
						INNER LOOP JOIN [$(Dimensional)].Dimension.LegType l ON (l.LegTypeKey = t.LegTypeKey)
						INNER LOOP JOIN [$(Dimensional)].Dimension.Class cl ON (cl.ClassKey = t.ClassKey)
						INNER LOOP JOIN [$(Dimensional)].Dimension.[Event] e ON (e.EventKey = t.EventKey)
						INNER LOOP JOIN [$(Dimensional)].Dimension.[InPlay] ip ON (ip.InPlayKey = t.InPlayKey)
			) x
	OPTION (RECOMPILE);

	------------------------------- Reduce the Stg/Dim pool to a subset of unfinalise candidates  -----------------------------------------

	EXEC [Control].Sp_Log @BatchKey, @ProcName, 'UnfinaliseCandidates', 'Start', @RowsProcessed = @@ROWCOUNT;

	SELECT	DISTINCT p.BetId, p.BetIdSource, p.TransactionTypeID, p.TransactionStatusID, p.TransactionMethodID, p.TransactionDirection, p.FromDate, p.BalanceTypeId, p.MasterTransactionTimestamp, p.LegNumber, p.TransactedAmount, --I'm sure this DISTINCT could be removed for greater efficiency (should never have duplicates to this degree across both Stage and Dim and neither can have duplicates on their own).... but not going to risk it...!!
			DENSE_RANK() OVER(PARTITION BY p.BetId,p.BetIdSource ORDER BY p.MasterTransactionTimestamp DESC,p.FromDate DESC) AS DATE_RANK
	INTO	#UnfinaliseCandidates
	FROM	#Pool p
			-- There must be at least two bet settlement type transaction steps in the pool for a bet to be considered for unfinalise
			LEFT JOIN	(	SELECT	BETID,BetIdSource
							FROM 	(	SELECT	BETID,BetIdSource,TransactionStatusID,MASTERTRANSACTIONTIMESTAMP 
										FROM	#Pool
										WHERE	(TransactionTypeID = 'BT' and TransactionStatusID IN ('CN','LO','WN','CO'))
										GROUP BY BETID,BetIdSource,TransactionStatusID,MASTERTRANSACTIONTIMESTAMP
									) X
							GROUP BY BetId,BetIdSource
							HAVING	COUNT(*) < 2
						) x ON (x.BetId = p.BetId AND x.BetIdSource = p.BetIdSource)
			-- Check for multiple requests. ** Worried about this - aren't there are an equal number of valid scenarios with multiple requests where we would still want to unfinalise? 
			LEFT JOIN	(	SELECT	BetId, BetIdSource
							FROM	#Pool
							WHERE	(TransactionTypeID = 'BT' and TransactionStatusID IN ('RE'))
							GROUP BY BetId,BetIdSource
							HAVING	COUNT(DISTINCT MasterTransactionTimestamp) > 1
						) y ON (y.BetId = p.BetId AND y.BetIdSource = p.BetIdSource AND p.TransactionTypeID = 'BT' and p.TransactionStatusID IN ('CN'))
			-- Where First Transaction is a settlement, delete the 'final' settlement from the list of candidates. ** But why then DateRank = 2....This looks very dodgy - like a reactive solution to an isloted encountered problem
			LEFT JOIN	(	SELECT	BetId, BetIdSource, MasterTransactionTimestamp, FromDate
							FROM 	(	SELECT	BetId, BetIdSource, MasterTransactionTimestamp, FromDate,TransactionTypeID,
												DENSE_RANK() OVER(PARTITION BY BetId, BetIdSource ORDER BY MasterTransactionTimestamp, FromDate) DateRank,
												FIRST_VALUE(TransactionStatusID) OVER(PARTITION BY BetId, BetIdSource ORDER BY MasterTransactionTimestamp, FromDate) FirstTransactionDetailId-- Commented out as can't see the use of it (EC)
										FROM	#Pool
									) x
							WHERE	DateRank = 2 AND (TransactionTypeID = 'BT' and FirstTransactionDetailId IN ('CN','LO','WN','CO'))
						) z ON (z.BetId = p.BetId AND z.BetIdSource = p.BetIdSource AND z.MasterTransactionTimestamp = p.MasterTransactionTimestamp AND z.FromDate = p.FromDate)
	WHERE	(p.TransactionTypeID = 'BT' and p.TransactionStatusID IN ('AC','CN','LO','WN','CO'))
			AND x.BetId IS NULL
			AND y.BetId IS NULL
			AND z.BetId IS NULL
	OPTION (RECOMPILE);
	
	------------------------------- Pick out partitial cancellations which can complicate matters  -----------------------------------------

	EXEC [Control].Sp_Log @BatchKey, @ProcName, 'PartialCancellation', 'Start', @RowsProcessed = @@ROWCOUNT;

	SELECT	A.BETID, A.BETIDSOURCE, A.TransactionStatusID
	INTO	#PartialCancellationCandidates
	FROM	#UnfinaliseCandidates A 
			INNER JOIN #UnfinaliseCandidates B ON A.BetId=B.BetId AND A.BetIdSource=B.BetIdSource AND A.DATE_RANK<B.DATE_RANK
	WHERE	A.BalanceTypeId = 'UNS' 
			AND (A.TransactionTypeID = 'BT' and A.TransactionStatusID IN ('CN'))
			AND A.DATE_RANK > 1	
			AND B.BalanceTypeId = 'UNS' 
			AND (B.TransactionTypeID = 'BT' and B.TransactionStatusID IN ('AC'))
	GROUP BY A.BETID, A.BETIDSOURCE, A.TransactionStatusID
	HAVING SUM(ABS(A.TransactedAmount)) <> SUM(ABS(B.TransactedAmount)) -- Bet Cancel Amount is different from Bet Accept Amount - Means partial Cancellation
	OPTION (RECOMPILE);

	------------------------------- And finally identify the required unfinalise transactions and dates  -----------------------------------------

	EXEC [Control].Sp_Log @BatchKey, @ProcName, 'UnfinaliseDates', 'Start', @RowsProcessed = @@ROWCOUNT;
 
	SELECT	BetId, BetIdSource, TransactionTypeID, TransactionStatusID, TransactionMethodID, TransactionDirection, BalanceTypeId, FromDate, MasterTransactionTimestamp, LegNumber,
			CASE 
				-- If the first subsequent timestamp retrieved from staging falls between the current pool timestamp and the next pool timestamp then this pool transaction is unfinalised on the timestamp from staging
				WHEN NextStaging_MasterTransactionTimestamp BETWEEN MasterTransactionTimestamp AND NextPool_MasterTransactionTimestamp THEN NextStaging_MasterTransactionTimestamp
				-- If the first subsequent FromDate retrieved from staging falls between the current pool timestamp and the next pool timestamp then this pool transaction is unfinalised on the FromDate from staging
				-- (this is for situations where a deduped accept transaction as retrieved by the inner query has the original BetDate as its timestamp, so the unfinalise is inferred to have occurred on the FromDate)
				WHEN NextStaging_FromDate BETWEEN MasterTransactionTimestamp AND NextPool_MasterTransactionTimestamp THEN NextStaging_FromDate
				-- Otherwise, if the fist subsequent staging transaction does not fall between current and subsequent pool transactions, then either the current pool transaction is not the last pool transaction 
				-- for the current bet retrieved from [$(Dimensional)] (in which case should have previously been unfinalised) or the current pool transaction is the last in the pool and therefore the most recent from 
				-- staging - either way, no unfinalisation required for this transaction so flag with null Unfinalised Timestamp
				ELSE NULL 
			END UnfinalisedMasterTransactionTimestamp,
			NextStaging_FromDate UnfinalisedFromDate
	INTO	#UnfinaliseDates
	FROM	(	SELECT	A.BetId, A.BetIdSource, A.TransactionTypeID, A.TransactionStatusID, A.TransactionMethodID, A.TransactionDirection, A.BalanceTypeId, A.FromDate, A.MasterTransactionTimestamp, A.LegNumber,
						MIN(B.FromDate) NextStaging_FromDate,
						MIN(B.MasterTransactionTimestamp) NextStaging_MasterTransactionTimestamp,
						MIN(C.MasterTransactionTimestamp) NextPool_MasterTransactionTimestamp
				FROM	#UnfinaliseCandidates A
						LEFT OUTER JOIN	(	-- Reselect staged transactions without excluding those already flagged as ExistsInDim
											-- This is to include the situation where a bet accept (BAI) is reasserted after an initial settlement due to a subsequent unfinalise which has not yet been refinalised
											-- In these circumstances, we would want the FromDate/MasterTransactionTimestamp of this reasserted accept to feature in the logic when selecting a date/time for the unfinalise transaction 
											SELECT	BetId, BetIdSource,TransactionTypeID, TransactionStatusID, TransactionMethodID, TransactionDirection, BalanceTypeId, FromDate, MasterTransactionTimestamp 
											FROM	Stage.[Transaction] 
											WHERE	BatchKey = @BatchKey
										) B	ON A.BetId=B.BetId AND A.BetIdSource=B.BetIdSource AND (B.FromDate > A.FromDate OR B.MasterTransactionTimestamp > A.MasterTransactionTimestamp)
						LEFT OUTER JOIN #UnfinaliseCandidates C ON B.BetId=C.BetId AND B.BetIdSource=C.BetIdSource AND C.DATE_RANK < A.DATE_RANK
						LEFT OUTER JOIN #PartialCancellationCandidates P ON A.BetId=P.BETID AND A.BetIdSource=P.BETIDSOURCE AND A.TransactionStatusID = P.TransactionStatusID -- Dont Unfinalize Partially cancelled Bets
				WHERE	A.TransactionStatusID <> 'AC'
						AND P.BETID IS NULL -- Dont Unfinalize Partially cancelled Bets
				GROUP BY A.BetId,A.BetIdSource,A.TransactionTypeID, A.TransactionStatusID, A.TransactionMethodID, A.TransactionDirection,A.BalanceTypeId,A.FromDate,A.MasterTransactionTimestamp,A.LegNumber
			) x
	OPTION (RECOMPILE);

	------------------------------- Insert unfinalised transactions into staging  -----------------------------------------

	EXEC [Control].Sp_Log @BatchKey, @ProcName, 'InsertStagingUnfinalise', 'Start', @RowsProcessed = @@ROWCOUNT;

	DROP TABLE #UnfinaliseCandidates
	DROP TABLE #PartialCancellationCandidates

	INSERT	[Stage].[Transaction]
			(	BatchKey, TransactionId, TransactionIdSource, BalanceTypeId, TransactionTypeID, TransactionStatusID, TransactionMethodID, TransactionDirection,
				BetGroupID, BetGroupIDSource, BetId, BetIdSource, LegId, LegIdSource, FreeBetID, ExternalID, External1, External2,
				UserID, UserIDSource, InstrumentID, InstrumentIDSource, LedgerID, LedgerSource, WalletId, PromotionId, PromotionIDSource,
				MasterTransactionTimestamp, MasterDayText, MasterTimeText, Channel, ActionChannel, CampaignId, TransactedAmount, 
				BetGroupType, BetTypeName, BetSubTypeName, LegBetTypeName, GroupingType, PriceTypeID, PriceTypeSource, ClassId, ClassIdSource, EventID, EventIDSource, MarketId, MarketIdSource, LegNumber, NumberOfLegs,
				ExistsInDim, Conditional, MappingType, MappingId, FromDate, DateTimeUpdated, RequestID, WithdrawID, InPlay, ClickToCall, CashoutType, IsDoubleDown, IsDoubledDown)
	SELECT  @BatchKey BatchKey, p.TransactionId, p.TransactionIdSource, p.BalanceTypeId,
			p.TransactionTypeID,
			CASE 
				WHEN p.TransactionStatusId = 'CN' THEN p.TransactionStatusId
				WHEN p.TransactionStatusId = 'CO' THEN p.TransactionStatusId
			ELSE 'UN'
			END TransactionStatusId,
			CASE
				WHEN p.TransactionStatusID = 'CN' THEN p.TransactionMethodID
				WHEN p.TransactionStatusID = 'CO' THEN p.TransactionMethodID
				ELSE 'AD'
			END TransactionMethodID,
			CASE 
				WHEN p.TransactionDirection = 'CR' THEN 'DR'
				ELSE 'CR'
			END TransactionDirection,

			p.BetGroupID, p.BetGroupIDSource, p.BetId, p.BetIdSource, p.LegId, p.LegIdSource, p.FreeBetID, p.ExternalID, p.External1, p.External2,

			p.UserID, p.UserIDSource,p.InstrumentID, p.InstrumentIDSource, p.LedgerID, p.LedgerSource, p.WalletId, p.PromotionId, p.PromotionIDSource,
			m.UnfinalisedMasterTransactionTimestamp MasterTransactionTimestamp,
			CONVERT(VARCHAR(11), CONVERT(DATE, m.UnfinalisedMasterTransactionTimestamp)) MasterDayText,
			CONVERT(VARCHAR(5), CONVERT(TIME, m.UnfinalisedMasterTransactionTimestamp)) MasterTimeText,
			p.Channel, p.ActionChannel, p.CampaignId, p.TransactedAmount * (-1) TransactedAmount, 
			p.BetGroupType, p.BetTypeName, p.BetSubTypeName, p.LegBetTypeName, p.GroupingType, p.PriceTypeID, p.PriceTypeSource, p.ClassId, p.ClassIdSource, p.EventID, p.EventIDSource, p.MarketId, p.MarketIdSource,
			p.LegNumber, p.NumberOfLegs, 0 ExistsInDim, 0 Conditional, 99 MappingType, -1 MappingId, m.UnfinalisedFromDate FromDate, 0 DateTimeUpdated, NULL AS RequestID, NULL AS WithdrawID, p.InPlay, p.ClickToCall, p.CashoutType, p.IsDoubleDown, p.IsDoubledDown
	FROM	#Pool p
			INNER JOIN #UnfinaliseDates m ON (m.BetId=p.BetId AND m.BetIdSource=p.BetIdSource AND m.TransactionTypeID = p.TransactionTypeID AND m.TransactionStatusID = p.TransactionStatusID AND m.TransactionMethodID = p.TransactionMethodID AND m.TransactionDirection = p.TransactionDirection AND m.BalanceTypeId=p.BalanceTypeId AND m.FromDate=p.FromDate 
											AND m.MasterTransactionTimestamp=p.MasterTransactionTimestamp AND m.LegNumber=p.LegNumber AND m.UnfinalisedMasterTransactionTimestamp IS NOT NULL)
	OPTION (RECOMPILE);

	SET @RowsProcessed = @@ROWCOUNT;
	SET @RowsAffected = @RowsProcessed;

	------------------------------- Identify settlement transactions which must be converted to adjustment types following unfinalise  -----------------------------------------

	EXEC [Control].Sp_Log @BatchKey, @ProcName, 'Adjustments', 'Start', @RowsProcessed = @RowsProcessed;

	DROP TABLE #UnfinaliseDates

	SELECT	BetID, BetIDSource, TransactionTypeID, TransactionStatusID, TransactionMethodID, TransactionDirection, BalanceTypeId, FromDate, MasterTransactionTimestamp, LegNumber
	INTO	#Adjustments
	FROM	(	SELECT	BetID, BetIDSource, TransactionTypeID, TransactionStatusID, TransactionMethodID, TransactionDirection, BalanceTypeId, FromDate, MasterTransactionTimestamp, LegNumber, 
						DENSE_RANK() OVER(PARTITION BY BetID, BetIDSource ORDER BY MasterTransactionTimestamp, FromDate) DateRank, Src
				FROM	(	SELECT	BetID, BetIDSource, TransactionTypeID, TransactionStatusID, TransactionMethodID, TransactionDirection, BalanceTypeId, FromDate, MasterTransactionTimestamp, LegNumber, Src 
							FROM	#Pool
							WHERE	(TransactionTypeID = 'BT' and TransactionStatusID IN ('LO','WN','CO') and TransactionMethodID = 'SE')
						) x
			) y
	WHERE	y.DateRank > 1 AND Src = 'S'
	OPTION (RECOMPILE)

	------------------------------- Amend the identified Adjustmenttransactions  -----------------------------------------

	EXEC [Control].Sp_Log @BatchKey, @ProcName, 'UpdateStagingAdjustment', 'Start', @RowsProcessed = @@ROWCOUNT;

	UPDATE	s
	SET		s.ExistsInDim = 10
	FROM	[Stage].[Transaction] s
			INNER HASH JOIN #Adjustments a ON (a.BetID = s.BetID AND a.BetIDSource = s.BetIDSource 
												AND a.TransactionTypeID = s.TransactionTypeID AND a.TransactionStatusID = s.TransactionStatusID 
												AND a.TransactionMethodID = s.TransactionMethodID AND a.TransactionDirection = s.TransactionDirection 
												AND a.BalanceTypeId = s.BalanceTypeId AND a.LegNumber = s.LegNumber
												AND a.MasterTransactionTimestamp = s.MasterTransactionTimestamp AND a.FromDate = s.FromDate) 
	WHERE	BatchKey = @BatchKey AND ExistsInDim = 0
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log @BatchKey, @ProcName, 'InsertStagingAdjustment', 'Start', @RowsProcessed = @@ROWCOUNT;

	DROP TABLE #Adjustments

	INSERT	[Stage].[Transaction]	
			(	BatchKey, TransactionId, TransactionIdSource, BalanceTypeId, TransactionTypeID, TransactionStatusID, TransactionMethodID, TransactionDirection,
				BetGroupID, BetGroupIDSource, BetId, BetIdSource, LegId, LegIdSource, FreeBetID, ExternalID, External1, External2,
				UserID, UserIDSource, InstrumentID, InstrumentIDSource, LedgerID, LedgerSource, WalletId, PromotionId, PromotionIDSource,
				MasterTransactionTimestamp, MasterDayText, MasterTimeText, 
				Channel, ActionChannel, CampaignId, TransactedAmount,
				BetGroupType, BetTypeName, BetSubTypeName, LegBetTypeName, GroupingType, PriceTypeID, PriceTypeSource, ClassId, ClassIdSource, EventID, EventIDSource, MarketId, MarketIdSource, LegNumber, NumberOfLegs,
				ExistsInDim, Conditional, MappingType, MappingId, FromDate, DateTimeUpdated,RequestID,WithdrawID,InPlay,ClickToCall,CashoutType, IsDoubleDown, IsDoubledDown
			)
	SELECT	BatchKey, TransactionId, TransactionIdSource, BalanceTypeId, TransactionTypeID, TransactionStatusID, 'AD' TransactionMethodID, TransactionDirection,
			BetGroupID, BetGroupIDSource, BetId, BetIdSource, LegId, LegIdSource, FreeBetID, ExternalID, External1, External2,
			UserID, UserIDSource, InstrumentID, InstrumentIDSource, LedgerID, LedgerSource, WalletId, PromotionId, PromotionIDSource,
			MasterTransactionTimestamp, MasterDayText, MasterTimeText,
			Channel,ActionChannel,CampaignId,TransactedAmount,
			BetGroupType, BetTypeName, BetSubTypeName, LegBetTypeName, GroupingType, PriceTypeID, PriceTypeSource, ClassId, ClassIdSource, EventID, EventIDSource, MarketId, MarketIdSource, LegNumber, NumberOfLegs,
			0 ExistsInDim, Conditional, 88 MappingType, -8 MappingId, FromDate, DateTimeUpdated,RequestID,WithdrawID,InPlay,ClickToCall,CashoutType, IsDoubleDown, IsDoubledDown
	FROM	[Stage].[Transaction]
	WHERE	BatchKey = @BatchKey 
			AND ExistsInDim = 10
	OPTION (RECOMPILE)

	SET @RowsProcessed = @@ROWCOUNT;
	SET @RowsAffected = @RowsAffected + @RowsProcessed;

	EXEC [Control].Sp_Log @BatchKey, @ProcName, NULL, 'Success', @RowsProcessed = @RowsProcessed;

	DROP TABLE #Pool

END TRY
BEGIN CATCH
			
	DECLARE @ErrorMessage VARCHAR(255) = (SELECT ERROR_MESSAGE());

	EXEC [Control].Sp_Log @BatchKey, @ProcName, NULL, 'Failed', @ErrorMessage; 
			
	THROW;

END CATCH

﻿CREATE PROC [Recursive].[Sp_Transaction_Adjustments]
(
	@BatchKey	INT
)
WITH RECOMPILE
AS 
BEGIN

		DECLARE @ProcName VARCHAR(255) = (SELECT OBJECT_NAME(@@PROCID));

		SET ANSI_NULLS ON;
		SET QUOTED_IDENTIFIER ON;

		EXEC [Control].Sp_Log	@BatchKey, @ProcName, 'Moved to sp_Transaction_Unfinalise','Start'

		EXEC [Control].Sp_Log	@BatchKey, @ProcName, NULL, 'Success';
		--EXEC [PreProd].[Sp_LogProcess] @BatchKey = @BatchKey, @LogID = @LogID, @ObjectID = @@PROCID, @Status = 'Success', @RowsAffected = @RowsAffected;

		--END TRY
		--BEGIN CATCH
			
		--	DECLARE @Error VARCHAR(1000) = (SELECT ERROR_MESSAGE()); 
			
		--	EXEC [PreProd].[Sp_LogProcess] @BatchKey = @BatchKey, @LogID = @LogID, @ObjectID = @@PROCID, @Status = 'Failed', @RowsAffected = @RowsAffected, @Error = @Error;

		--	THROW;

		--END CATCH

END
GO

﻿/*
	Developer: Aaron Jackson
	Date: 24/08/2015
	Desc: Search for dimensions that are missing from dimension table, but exist in fact table. Insert stub records into dimension table.
		
	Test: EXEC [Recursive].[Sp_Dimension_User] @BatchKey = 305;
*/

CREATE PROC [Recursive].[Sp_Dimension_User]
(
	@BatchKey	INT,
	@RowsProcessed	INT = 0 OUTPUT
)
AS 
BEGIN TRY

	DECLARE @ProcName VARCHAR(255) = (SELECT OBJECT_NAME(@@PROCID));

	DECLARE @RowCount int

	--SET STATISTICS IO ON;
	--SET STATISTICS TIME ON;

	SET ANSI_NULLS ON;
	SET QUOTED_IDENTIFIER ON;
	SET NOCOUNT ON;

	EXEC [Control].Sp_Log @BatchKey, @ProcName, 'Get Missing Keys','Start';
	
	-- Create new records based on -1 record
	SELECT
		 @BatchKey as [BatchKey] 
		,[UserKey]
		,[UserId]
		,[Source]
		,[SystemName]
		,[EmployeeKey]
		,[Forename]
		,[Surname]
		,[PayrollNumber]
		,[FromDate]
    INTO #DefaultUnknown
    FROM [$(Dimensional)].[Dimension].[User] 
    WHERE UserKey = -1;

	-- Get list of natural keys from stage.transaction
	-- check natural keys exist in dimension

	SELECT
		S.UserID,
		S.UserIDSource,
		MAX(S.FromDate) FromDate
	INTO #Dim
	FROM [Stage].[Transaction] S
	LEFT OUTER JOIN [$(Dimensional)].[Dimension].[User] DUK ON S.UserID=DUK.UserID AND S.UserIDSource=DUK.Source
	WHERE S.ExistsInDim = 0
	AND S.UserID IS NOT NULL
	AND DUK.UserKey IS NULL
	AND S.BatchKey = @BatchKey
	GROUP BY S.UserID,
		S.UserIDSource
	OPTION(RECOMPILE);

	EXEC [Control].Sp_Log @BatchKey, @ProcName, 'Insert into Staging','Start', @RowsProcessed = @@ROWCOUNT;

	-- insert missing keys into staging.user table
	INSERT INTO [Stage].[User] ([BatchKey]
      ,[UserId]
      ,[Source]
      ,[SystemName]
      ,[PayrollNumber]
      ,[Forename]
      ,[Surname]
      ,[FromDate]) 
	SELECT DISTINCT
		 U.[BatchKey]
		,D.[UserId]
		,D.[UserIDSource]
		,U.[SystemName]
		,U.[PayrollNumber]
		,U.[Forename]
		,U.[Surname]
		,U.[FromDate]
	FROM #Dim as D
	LEFT JOIN [Stage].[User] as S on S.UserId  = D.UserId AND S.Source = D.UserIDSource AND S.BatchKey = @BatchKey
	CROSS APPLY (SELECT * FROM #DefaultUnknown) as U
	WHERE S.UserId IS NULL
	OPTION(RECOMPILE);

	SET @RowCount  = @@ROWCOUNT;
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey, @ProcName, NULL,'Success', @RowsProcessed = @RowCount;
	
END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, @ProcName, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
﻿CREATE PROC [Recursive].[Sp_Transaction_UnbalancedWithdrawals]
(
	@BatchKey	INT
)
AS 

/*
	Developer: Joseph George
	Date: Unknown
	Desc: Unbalanced Withdrawals

	Last Changed By: Edward Chen
	Last Changed Date: 6/JUL/2015
	Last Change Desc: Remove TransactionDetailID Dependency
*/

BEGIN
	DECLARE @ProcName VARCHAR(255) = (SELECT OBJECT_NAME(@@PROCID));

	SET ANSI_NULLS ON;
	SET QUOTED_IDENTIFIER ON;
	SET NOCOUNT ON;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
		
	IF OBJECT_ID('tempdb..#Dim') IS NOT NULL BEGIN DROP TABLE #Dim END;
	IF OBJECT_ID('tempdb..#Additional') IS NOT NULL BEGIN DROP TABLE #Additional END;

	BEGIN TRY

		DECLARE @RowsAffected INT

		------------------------------- Identify those transactions which are additional steps to original transactions  -----------------------------------------

		EXEC [Control].Sp_Log @BatchKey, @ProcName, 'CacheDim', 'Start';

		SELECT	t.TransactionID, t.TransactionIdSource, d.TransactionTypeID, d.TransactionStatusID, d.TransactionMethodID, d.TransactionDirection, b.BalanceTypeID, TransactedAmount, t.MasterTransactionTimestamp
		INTO	#Dim
		FROM	(	SELECT	DISTINCT TransactionId, TransactionIdSource 
					FROM	[Stage].[Transaction] 
					WHERE	BatchKey = @BatchKey AND ExistsInDim = 0 AND TransactionTypeID = 'WI'
				) s
				INNER LOOP JOIN [$(Dimensional)].[Fact].[Transaction] t ON t.TransactionId = s.TransactionId AND t.TransactionIdSource = s.TransactionIdSource	AND t.CreatedBatchKey < @BatchKey
				INNER JOIN [$(Dimensional)].[Dimension].TransactionDetail d ON d.TransactionDetailKey=t.TransactionDetailKey
				INNER JOIN [$(Dimensional)].[Dimension].BalanceType b ON b.BalanceTypeKey=t.BalanceTypeKey
		WHERE	d.TransactionDetailId LIKE 'WR%'
		OPTION (RECOMPILE);

		EXEC [Control].Sp_Log @BatchKey, @ProcName, 'IsolateAdditionalTrans', 'Start', @RowsProcessed = @@ROWCOUNT;

		SELECT	TransactionIDSource, TransactionID, TransactionTypeID, TransactionStatusID, TransactionMethodID, TransactionDirection, BalanceTypeID, TransactedAmount, MasterTransactionTimeStamp
		INTO	#Additional
		FROM 
			 ( SELECT	TransactionID, TransactionIDSource, TransactionTypeID, TransactionStatusID, TransactionMethodID, TransactionDirection, BalanceTypeID, TransactedAmount, MasterTransactionTimeStamp,
						ROW_NUMBER() OVER(PARTITION BY TransactionID, TransactionIDSource, TransactionTypeID, TransactionStatusID, TransactionMethodID, TransactionDirection, BalanceTypeID, TransactedAmount ORDER BY MasterTransactionTimestamp) RowNumber, Source
			   FROM 
				   ( 	SELECT	'S' AS Source, TransactionID, TransactionIDSource, TransactionTypeID, TransactionStatusID, TransactionMethodID, TransactionDirection, BalanceTypeID, TransactedAmount, MasterTransactionTimeStamp
						FROM	[Stage].[Transaction]
						WHERE	BatchKey = @BatchKey AND ExistsInDim = 0 AND TransactionTypeID = 'WI'
						UNION ALL
						SELECT	'D' Source, TransactionID, TransactionIDSource, TransactionTypeID, TransactionStatusID, TransactionMethodID, TransactionDirection, BalanceTypeID, TransactedAmount, MasterTransactionTimeStamp
						FROM	#Dim
				  ) x
			 ) r
		WHERE RowNumber > 1
		OPTION (RECOMPILE);

		------------------------------- Flag the Additional Withdrawal records identified in staging  -----------------------------------------

		EXEC [Control].Sp_Log @BatchKey, @ProcName, 'UpdateStaging', 'Start', @RowsProcessed = @@ROWCOUNT;

		DROP TABLE #Dim

		UPDATE	t
		SET		ExistsInDim = 5
		FROM	[Stage].[Transaction] t  WITH(NOLOCK)
				INNER HASH JOIN #Additional a ON (a.TransactionID = t.TransactionID AND a.TransactionIDSource = t.TransactionIDSource 
													AND  a.TransactionTypeID = t.TransactionTypeID AND a.TransactionStatusID = t.TransactionStatusID AND a.TransactionMethodID = t.TransactionMethodID AND a.TransactionDirection = t.TransactionDirection AND a.BalanceTypeID = t.BalanceTypeID AND a.TransactedAmount = t.TransactedAmount
													AND a.MasterTransactionTimeStamp = t.MasterTransactionTimeStamp)
		WHERE	t.BatchKey = @BatchKey AND t.ExistsInDim = 0
		OPTION (RECOMPILE)

		SET @RowsAffected = @@ROWCOUNT;
 
		EXEC [Control].Sp_Log @BatchKey,@ProcName, NULL, 'Success', @RowsProcessed = @RowsAffected;

		DROP TABLE #Additional

	END TRY
	BEGIN CATCH

		DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

		EXEC [Control].Sp_Log @BatchKey, @ProcName, NULL, 'Failed', @ErrorMessage;

		THROW;

	END CATCH
END

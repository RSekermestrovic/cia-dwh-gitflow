﻿CREATE PROC [Recursive].[Sp_Position_FactCheck]
(
	@BatchKey	int,
	@RowsProcessed	int = 0 OUTPUT
)
AS

	DECLARE @RowCount INT
	DECLARE @ReloadFromDimensional bit = 0

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

BEGIN TRY

	SET @ReloadFromDimensional = CASE WHEN ISNULL(CONVERT(int,Control.ParameterValue('ETL','StagingLowerBound')),0) <= @BatchKey THEN 0 ELSE 1 END

	EXEC [Control].Sp_Log	@BatchKey,'Recursive.Sp_PositionFactCheck','Collect Transactions','Start'

	SELECT	@BatchKey BatchKey, LegId, BetId, TransactionId, LedgerID, LedgerSource, MasterDayText, AnalysisDayText, MasterTransactionTimestamp,
			BalanceTypeID, TransactionTypeID, TransactionStatusID, TransactionMethodID, WalletId, 
			BetGroupType, BetTypeName, BetSubTypeName, LegBetTypeName, GroupingType, ClassId, Channel,
			TransactedAmount, 0 ExistsInDim
	INTO	#Transaction
	FROM	(	SELECT	c.LegId, ISNULL(c.BetId,c.TransactionID) BetId, t.TransactionId, a.LedgerID, a.LedgerSource, z.MasterDayText, z.AnalysisDayText, t.MasterTransactionTimestamp,
						b.BalanceTypeID, d.TransactionTypeID, d.TransactionStatusID, d.TransactionMethodID, W.WalletId, 
						bt.BetGroupType, bt.BetType BetTypeName, bt.BetSubType BetSubTypeName, bt.LegType LegBetTypeName, bt.GroupingType, cl.ClassId, ch.ChannelId Channel,
						t.TransactedAmount
				FROM	[$(Dimensional)].Fact.[Transaction] t						
						INNER JOIN [$(Dimensional)].Dimension.TransactionDetail d ON (d.TransactionDetailKey = t.TransactionDetailKey) 
						INNER JOIN [$(Dimensional)].Dimension.BalanceType b ON (b.BalanceTypeKey = t.BalanceTypeKey) 
						INNER JOIN [$(Dimensional)].Dimension.BetType bt ON (bt.BetTypeKey = t.BetTypeKey) 
						INNER JOIN [$(Dimensional)].Dimension.Class cl ON (cl.ClassKey = t.ClassKey) 
						INNER JOIN [$(Dimensional)].Dimension.Channel ch ON (ch.ChannelKey = t.ChannelKey) 
						INNER JOIN [$(Dimensional)].Dimension.Wallet w ON (w.WalletKey = t.WalletKey) 
						INNER JOIN [$(Dimensional)].Dimension.Contract c ON (c.ContractKey = t.ContractKey) 
						INNER JOIN [$(Dimensional)].Dimension.Account a ON (a.AccountKey = t.AccountKey) 
						INNER JOIN [$(Dimensional)].Dimension.DayZone z ON (z.DayKey = t.DayKey) 
				WHERE	t.CreatedBatchKey = @BatchKey
						AND	@ReloadFromDimensional = 1
				UNION ALL
				SELECT	t.LegId, t.BetId, t.TransactionId, t.LedgerID, t.LedgerSource, t.MasterDayText, z.AnalysisDayText, t.MasterTransactionTimestamp,
						t.BalanceTypeID, t.TransactionTypeID, t.TransactionStatusID, t.TransactionMethodID, t.WalletId,
						t.BetGroupType, t.BetTypeName, t.BetSubTypeName, t.LegBetTypeName, t.GroupingType, t.ClassId, t.Channel,
						t.TransactedAmount
				FROM	Stage.[Transaction] t						
						INNER JOIN [$(Dimensional)].Dimension.DayZone z ON (z.MasterDayText = t.MasterDayText AND z.FromDate <= t.MasterTransactionTimestamp AND z.ToDate > t.MasterTransactionTimestamp) 
				WHERE	t.BatchKey = @BatchKey
						AND ExistsInDim = 0
						AND	@ReloadFromDimensional = 0
			) x
	WHERE	(	(BalanceTypeID ='CLI')																							
				OR (BalanceTypeID = 'UNS' and TransactionTypeID = 'BT'  and TransactionStatusID IN ('LO','WN','UN'))		
				OR (BalanceTypeID = 'P&L' and TransactionTypeID = 'BT')
				OR (BalanceTypeID = 'PWD' and TransactionTypeID = 'WI' and TransactionStatusID IN ('CO','RV'))
			)				
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,'Recursive.Sp_PositionFactCheck','Collect Balances','Start', @RowsProcessed = @@ROWCOUNT

	SELECT	DISTINCT MasterDayText, AnalysisDayText INTO #ImpactedDay FROM #Transaction

	SELECT	DISTINCT z.DayKey
	INTO	#ImpactedDayKey
	FROM	#ImpactedDay t
			INNER JOIN [$(Dimensional)].Dimension.DayZone z ON (z.MasterDayText = t.MasterDayText OR z.AnalysisDayText = t.AnalysisDayText)
	OPTION (RECOMPILE)

	SELECT	@BatchKey BatchKey,
			LedgerID,
			LedgerSource,
			BalanceTypeID,
			WalletId,
			MasterDayText,
			AnalysisDayText,
			MasterTimestamp,
			FromDate,
			OpeningBalance,
			TransactedAmount,
			ClosingBalance
	INTO	#Balance 
	FROM	(	SELECT	a.LedgerID,
						a.LedgerSource,
						t.BalanceTypeID,
						w.WalletId,
						z.MasterDayText,
						z.AnalysisDayText,
						convert(datetime2(3), CASE MasterDayText WHEN 'N/A' THEN AnalysisDayText ELSE MasterDayText END) MasterTimestamp,
						convert(datetime2(3), CASE MasterDayText WHEN 'N/A' THEN AnalysisDayText ELSE MasterDayText END) FromDate,
						b.OpeningBalance,
						b.TotalTransactedAmount TransactedAmount,
						b.ClosingBalance
				FROM	[$(Dimensional)].Fact.Balance b
						INNER JOIN [$(Dimensional)].Dimension.BalanceType t ON (t.BalanceTypeKey = b.BalanceTypeKey) 
						INNER JOIN [$(Dimensional)].Dimension.Wallet w ON (w.WalletKey = b.WalletKey) 
						INNER JOIN [$(Dimensional)].Dimension.Account a ON (a.AccountKey = b.AccountKey) 
						INNER JOIN [$(Dimensional)].Dimension.DayZone z ON (z.DayKey = b.DayKey) 
				WHERE	(b.CreatedBatchKey = @BatchKey or b.ModifiedBatchKey = @BatchKey)
						AND @ReloadFromDimensional = 1
						AND b.DayKey IN (SELECT DayKey FROM #ImpactedDayKey)
				UNION ALL
				SELECT	LedgerID,
						LedgerSource,
						BalanceTypeID,
						WalletId,
						MasterDayText,
						AnalysisDayText,
						MasterTimeStamp,
						FromDate,
						OpeningBalance,
						TransactedAmount,
						ClosingBalance
				From	[Stage].[Balance]
				WHERE	BatchKey = @BatchKey
						AND @ReloadFromDimensional = 0
			) x
	WHERE	BalanceTypeID = 'CLI'
			AND WalletId = 'C'				
	OPTION (RECOMPILE)


	EXEC [Control].Sp_Log	@BatchKey,'Recursive.Sp_PositionFactCheck','Updating Balances from Balance Fact','Start', @RowsProcessed = @@ROWCOUNT

	UPDATE b
	SET b.[OpeningBalance] = a.OpeningBalance,
		b.[TransactedAmount] = a.TransactedAmount,
		b.[ClosingBalance] = a.ClosingBalance
	FROM #Balance a
	INNER JOIN Stage.Position b on a.BatchKey = b.BatchKey and a.BalanceTypeID = b.BalanceTypeID 
								and a.LedgerID = b.LedgerID and a.LedgerSource = b.LedgerSource
								and a.MasterDayText = b.MasterDayText and a.AnalysisDayText = b.AnalysisDayText
	WHERE a.BalanceTypeID = 'CLI'
	and a.BatchKey = @BatchKey
	and b.BatchKey = @BatchKey
	and a.WalletId = 'C'
	OPTION (RECOMPILE);

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,'Recursive.Sp_PositionFactCheck','Add Accounts for New Transactions','Start', @RowsProcessed = @RowCount

	Select	a.BatchKey,
			a.LedgerID,
			a.LedgerSource,
			a.BalanceTypeID,
			a.MasterDayText,
			a.AnalysisDayText,
			a.OpeningBalance,
			a.TransactedAmount,
			a.ClosingBalance,
			a.MasterTimeStamp,
			a.FromDate
	INTO #NewTxns
	From #Balance a
	LEFT OUTER JOIN Stage.Position b on a.BatchKey = b.BatchKey and a.BalanceTypeID = b.BalanceTypeID 
								and a.LedgerID = b.LedgerID and a.LedgerSource = b.LedgerSource
								and a.MasterDayText = b.MasterDayText and a.AnalysisDayText = b.AnalysisDayText
								and b.BatchKey = @BatchKey
	Where a.BalanceTypeID = 'CLI'
	and a.BatchKey = @BatchKey
	and a.WalletId = 'C'
	and b.LedgerID is null
	OPTION (RECOMPILE)

	Select a.*, b.DayKey, DAK.AccountKey
	INTO #NewTxnDay
	From #NewTxns a WITH(NOLOCK)
	INNER JOIN [$(Dimensional)].Dimension.DayZone b on a.MasterDayText = b.MasterDayText and a.AnalysisDayText = b.AnalysisDayText
	INNER JOIN [$(Dimensional)].Dimension.ACCOUNT DAK ON a.LedgerID=DAK.LedgerID and a.LedgerSource= DAK.LedgerSource 				
	OPTION (RECOMPILE)

	DROP TABLE #NewTxns

	INSERT INTO [Stage].[Position]
	Select	a.BatchKey,
			a.LedgerID,
			'IAA' as LedgerSource,
			a.BalanceTypeID,
			a.MasterDayText,
			a.AnalysisDayText,
			a.OpeningBalance,
			a.TransactedAmount,
			a.ClosingBalance,
			CONVERT(Datetime2(3),CASE WHEN FDDZ.MasterDayText = 'N/A' THEN null else FDDZ.MasterDayText END) as FirstCreditDate,
			CONVERT(Datetime2(3),CASE WHEN LDDZ.MasterDayText = 'N/A' THEN null else LDDZ.MasterDayText END) as LastDepositDate,
			null as FirstDebitDate,
			null as LastDebitDate,
			null as FirstBetID,
			CONVERT(Datetime2(3),CASE WHEN FBD.MasterDayText = 'N/A' THEN null else FBD.MasterDayText END) as FirstBetDate,
			FBT.BetType as FirstBetType,
			FBT.BetSubType as FirstBetSubType,
			FBT.GroupingType as FirstGroupingType,
			FBT.BetGroupType as FirstBetGroupType,
			FBT.LegType as FirstLegType,
			FCH.ChannelId as FirstChannel,
			null as FirstEventId,
			FCL.ClassId as FirstClassId,
			null as LastBetId,
			CONVERT(Datetime2(3),CASE WHEN LBD.MasterDayText = 'N/A' THEN null else LBD.MasterDayText END) as LastBetDate,
			LBT.BetType as LastBetType,
			LBT.BetSubType as LastBetSubType,
			LBT.GroupingType as LastGroupingType,
			LBT.BetGroupType as LastBetGroupType,
			LBT.LegType as LastLegType,
			LCH.ChannelId as LastChannel,
			null as LastEventId,
			LCL.ClassId as LastClassId,
			IsNull(Act.HasDeposited,'No') as HasDeposited,
			IsNull(Act.HasWithdrawn,'No') as HasWithdrawn,
			IsNull(Act.HasBet,'No') as HasBet,
			IsNull(Act.HasWon,'No') as HasWon,
			IsNull(Act.HasLost,'No') as HasLost,
			c.LifetimeTurnover,
			c.LifetimeGrossWin,
			c.LifetimeBetCount,
			c.LifetimeBonus,
			c.LifetimeDeposits,
			c.LifetimeWithdrawals,
			a.MasterTimeStamp,
			a.FromDate,
			null as FirstCreditId,
			null as LastCreditId,
			null as TierNumber,
			null as TierNumberFiscal,
			null as Turnover,
			null as TurnoverCalenderMonthToDate,
			null as TurnoverFiscalMonthToDate,
			NULL as DaysSinceAccountOpened,
			NULL as DaysSinceFirstDeposit,
			NULL as DaysSinceFirstBet,
			NULL as DaysSinceLastDeposit,
			NULL as DaysSinceLastBet,
			'N/A' as RevenueLifeCycle,
			'Unknown' as LifeStage,
			'Unknown' as PreviousLifeStage
	From #NewTxnDay a
	INNER JOIN [$(Dimensional)].Fact.Position c on c.AccountKey = a.AccountKey and c.DayKey = a.DayKey
	LEFT OUTER JOIN [$(Dimensional)].Dimension.DayZone FDDZ on c.FirstDepositDayKey = FDDZ.DayKey
	LEFT OUTER JOIN [$(Dimensional)].Dimension.DayZone LDDZ on c.LastDepositDayKey = LDDZ.DayKey
	LEFT OUTER JOIN [$(Dimensional)].Dimension.DayZone FBD on c.FirstBetDayKey = FBD.DayKey
	LEFT OUTER JOIN [$(Dimensional)].Dimension.BetType FBT on c.FirstBetTypeKey = FBT.BetTypeKey
	LEFT OUTER JOIN [$(Dimensional)].Dimension.Channel FCH on c.FirstChannelKey = FCH.ChannelKey
	LEFT OUTER JOIN [$(Dimensional)].Dimension.Class FCL on c.FirstClassKey = FCL.ClassKey
	LEFT OUTER JOIN [$(Dimensional)].Dimension.DayZone LBD on c.LastBetDayKey = LBD.DayKey
	LEFT OUTER JOIN [$(Dimensional)].Dimension.BetType LBT on c.LastBetTypeKey = LBT.BetTypeKey
	LEFT OUTER JOIN [$(Dimensional)].Dimension.Channel LCH on c.LastChannelKey = LCH.ChannelKey
	LEFT OUTER JOIN [$(Dimensional)].Dimension.Class LCL on c.LastClassKey = LCL.ClassKey
	LEFT OUTER JOIN [$(Dimensional)].Dimension.Activity Act on c.ActivityKey = Act.ActivityKey
	Where c.DayKey IN (Select distinct DayKey From #NewTxnDay)
	OPTION (RECOMPILE)

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	-------------------------------------------------------------------------------------------------------------------
	--------------------------------------------Get Activity Key Metrices----------------------------------------------

	EXEC [Control].Sp_Log	@BatchKey,'Recursive.Sp_PositionFactCheck','Get all metrices','Start', @RowsProcessed = @RowCount

	DROP TABLE #NewTxnDay

	SELECT 
		T.BatchKey,
		T.BalanceTypeID,
		T.LedgerID,
		T.LedgerSource,
		T.MasterDayText,
		MasterTransactionTimestamp,
		t.TransactionTypeID,
		t.TransactionStatusID,
		MIN(t.BetId) as FirstBetId,
		MAX(t.BetId) as LastBetId,
		MAX(T.BetGroupType) as ContractType,
		MAX(T.BetTypeName) as BetTypeName,
		MAX(T.BetSubTypeName) as BetSubTypeName,
		MAX(T.LegBetTypeName) as LegBetTypeName,
		MAX(T.GroupingType) as GroupingType,
		MAX(T.CLassID) as CLassID,
		MAX(T.Channel) as Channel,
		CONVERT(VARCHAR(11),CONVERT(DATE,DATEADD(MI,Z.OffsetMinutes,T.MasterTransactionTimestamp))) as AnalysisDayText,
		MAX(CASE WHEN ISNULL(W.WalletName,'CASH')='CASH' and t.TransactionTypeID = 'DP' and t.TransactionStatusID = 'CO' THEN 'Yes' ELSE 'No' END) as HasDeposited,
		MAX(CASE WHEN ISNULL(W.WalletName,'CASH')='CASH' and t.BalanceTypeID = 'CLI' and t.TransactionTypeID = 'WI' and t.TransactionStatusID = 'RE' THEN 'Yes' ELSE 'No' END) as HasWithdrawn,
		MAX(CASE WHEN t.TransactionTypeID = 'BT' and t.TransactionStatusID ='RE' THEN 'Yes' ELSE 'No' END) as HasBet,
		MAX(CASE WHEN t.TransactionTypeID = 'BT' and t.TransactionStatusID ='WN' THEN 'Yes' ELSE 'No' END) as HasWon,				
		MAX(CASE WHEN t.TransactionTypeID = 'BT' and t.TransactionStatusID ='LO' THEN 'Yes' ELSE 'No' END) as HasLost,
		SUM(CASE WHEN ISNULL(W.WalletName,'CASH')='CASH' and t.BalanceTypeID = 'UNS' and t.TransactionTypeID = 'BT'  and t.TransactionStatusID IN ('LO','WN','UN') AND t.TransactionMethodID='SE' THEN -t.TransactedAmount ELSE 0 END) as Turnover,
		SUM(CASE WHEN ISNULL(W.WalletName,'CASH')='CASH' and t.BalanceTypeID = 'P&L' and t.TransactionTypeID = 'BT' THEN t.TransactedAmount ELSE 0 END) as GrossWin,
		SUM(CASE WHEN t.TransactionTypeID = 'BT' and t.TransactionStatusID ='RE' THEN 1 ELSE 0 END) as BetCount,
		SUM(CASE WHEN ISNULL(W.WalletName,'CASH')='CASH' and t.TransactionTypeID = 'DP' and t.TransactionStatusID IN('CO','RV') THEN t.TransactedAmount ELSE 0	END) as	DepositsCompleted,
		SUM(CASE WHEN ISNULL(W.WalletName,'CASH')='CASH' and t.BalanceTypeID = 'PWD' and t.TransactionTypeID = 'WI' and t.TransactionStatusID IN ('CO','RV') THEN t.TransactedAmount ELSE 0 END) WithdrawalsCompleted,
		SUM(CASE WHEN t.TransactionTypeID = 'BO' and t.BalanceTypeID = 'CLI' THEN TransactedAmount ELSE 0 END) as Bonus,
		MIN(CASE WHEN t.TransactionTypeID = 'BT' and t.TransactionStatusID ='RE' THEN t.MasterTransactionTimestamp ELSE NULL END) as FirstBetDate,
		MAX(CASE WHEN t.TransactionTypeID = 'BT' and t.TransactionStatusID ='RE' THEN t.MasterTransactionTimestamp ELSE NULL END) as LastBetDate,
		MIN(CASE WHEN ISNULL(W.WalletName,'CASH')='CASH' and t.TransactionTypeID = 'DP' THEN t.MasterTransactionTimestamp ELSE NULL END) as FirstDepositDate,
		MAX(CASE WHEN ISNULL(W.WalletName,'CASH')='CASH' and t.TransactionTypeID = 'DP' THEN t.MasterTransactionTimestamp ELSE NULL END) as LastDepositDate,
		MIN(CASE WHEN ISNULL(W.WalletName,'CASH')='CASH' and t.TransactionTypeID = 'DP' THEN t.TransactionId ELSE NULL END) as FirstDepositId
	INTO #PositionStaging
	FROM #Transaction t						
		LEFT OUTER JOIN [$(Dimensional)].Dimension.Wallet W ON T.WalletID=W.WalletId
		INNER JOIN Reference.TimeZone Z ON T.MasterTransactionTimestamp>=Z.FromDate and T.MasterTransactionTimestamp<Z.ToDate and Z.TimeZone='Analysis'
	WHERE 
		1=1																														
		and (	   (t.BalanceTypeID ='CLI')																							
				OR (t.BalanceTypeID = 'UNS' and t.TransactionTypeID = 'BT'  and t.TransactionStatusID IN ('LO','WN','UN'))		
				OR (t.BalanceTypeID = 'P&L' and t.TransactionTypeID = 'BT')
				OR (t.BalanceTypeID = 'PWD' and t.TransactionTypeID = 'WI' and t.TransactionStatusID IN ('CO','RV'))
			)				
		and T.ExistsInDim=0
		and BatchKey  = @BatchKey
	GROUP BY 		
		T.BatchKey,	
		t.BalanceTypeID,																	
		T.LedgerID,																			
		T.LedgerSource,																			
		T.MasterDayText,
		t.TransactionTypeID,
		t.TransactionStatusID,																	
		T.MasterTransactionTimestamp,																		
		CONVERT(VARCHAR(11),CONVERT(DATE,DATEADD(MI,Z.OffsetMinutes,T.MasterTransactionTimestamp)))
		OPTION (RECOMPILE)

	-------------------------SUM MASTER-----------------------
	EXEC [Control].Sp_Log	@BatchKey,'Recursive.Sp_PositionFactCheck','Sum Master','Start', @RowsProcessed = @@ROWCOUNT

	SELECT 
		t.BatchKey,
		t.LedgerID,	
		t.LedgerSource,
		t.MasterDayText,
		MAX(HasDeposited) as HasDeposited,
		MAX(HasWithdrawn) as HasWithdrawn,
		MAX(HasBet) as HasBet,
		MAX(HasWon)	as HasWon,
		MAX(HasLost) as HasLost,
		MIN(ISNULL(tbp.FirstBetTypeName,'N/A')) as FirstBetTypeName,
		MIN(ISNULL(tbp.FirstBetSubTypeName,'N/A')) as FirstBetSubTypeName,
		MIN(ISNULL(tbp.FirstContractType,'N/A')) as FirstContractType,
		MIN(ISNULL(tbp.FirstGroupingType,'N/A')) as FirstGroupingType,
		MIN(ISNULL(tbp.FirstLegBetTypeName,'N/A')) as FirstLegBetTypeName,
		MIN(ISNULL(tbp.FirstClassID,0)) as FirstClassID,
		MIN(ISNULL(td.FirstChannel,0)) as FirstChannel,
		MIN(ISNULL(tbp.LastBetTypeName,'N/A')) as LastBetTypeName,
		MIN(ISNULL(tbp.LastBetSubTypeName,'N/A')) as LastBetSubTypeName,
		MIN(ISNULL(tbp.LastContractType,'N/A')) as LastContractType,
		MIN(ISNULL(tbp.LastGroupingType,'N/A')) as LastGroupingType,
		MIN(ISNULL(tbp.LastLegBetTypeName,'N/A')) as LastLegBetTypeName,
		MIN(ISNULL(tbp.LastClassID,0)) as LastClassID,
		MIN(ISNULL(td.LastChannel,0)) as LastChannel,
		SUM(Turnover) as Turnover,
		SUM(GrossWin) as GrossWin,
		SUM(BetCount) as BetCount,
		SUM(DepositsCompleted) as DepositsCompleted,
		SUM(WithdrawalsCompleted) as WithdrawalsCompleted,
		SUM(Bonus) as Bonus,
		MIN(FirstBetDate) as FirstBetDate,
		MAX(LastBetDate) as LastBetDate,
		MIN(FirstDepositDate) as FirstDepositDate,
		MAX(LastDepositDate) as LastDepositDate,
		MIN(FirstDepositId) as FirstDepositId,
		MIN(tbp.FirstBetId) as FirstBetId,
		MAX(tbp.LastBetID) as LastBetID
	INTO #PositionUpdate
	FROM #PositionStaging t
	LEFT OUTER HASH JOIN																																								
				(	SELECT DISTINCT
					LedgerID,
					LedgerSource,
					FIRST_VALUE(BetTypeName)		OVER (PARTITION BY tb.LedgerID,tb.LedgerSource ORDER BY tb.MasterTransactionTimestamp) as FirstBetTypeName,
					FIRST_VALUE(ContractType)		OVER (PARTITION BY tb.LedgerID,tb.LedgerSource ORDER BY tb.MasterTransactionTimestamp) as FirstContractType,
					FIRST_VALUE(BetSubTypeName)		OVER (PARTITION BY tb.LedgerID,tb.LedgerSource ORDER BY tb.MasterTransactionTimestamp) as FirstBetSubTypeName,
					FIRST_VALUE(LegBetTypeName)		OVER (PARTITION BY tb.LedgerID,tb.LedgerSource ORDER BY tb.MasterTransactionTimestamp) as FirstLegBetTypeName,
					FIRST_VALUE(GroupingType)		OVER (PARTITION BY tb.LedgerID,tb.LedgerSource ORDER BY tb.MasterTransactionTimestamp) as FirstGroupingType,
					FIRST_VALUE(FirstBetId)			OVER (PARTITION BY tb.LedgerID,tb.LedgerSource ORDER BY tb.MasterTransactionTimestamp) as FirstBetId,
					FIRST_VALUE(tb.CLassID)			OVER (PARTITION BY tb.LedgerID,tb.LedgerSource ORDER BY tb.MasterTransactionTimestamp) as FirstClassID,
					LAST_VALUE(BetTypeName)			OVER (PARTITION BY tb.LedgerID,tb.LedgerSource ORDER BY tb.MasterTransactionTimestamp ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as LastBetTypeName,
					LAST_VALUE(ContractType)		OVER (PARTITION BY tb.LedgerID,tb.LedgerSource ORDER BY tb.MasterTransactionTimestamp ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as LastContractType,
					LAST_VALUE(BetSubTypeName)		OVER (PARTITION BY tb.LedgerID,tb.LedgerSource ORDER BY tb.MasterTransactionTimestamp ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as LastBetSubTypeName,
					LAST_VALUE(LegBetTypeName)		OVER (PARTITION BY tb.LedgerID,tb.LedgerSource ORDER BY tb.MasterTransactionTimestamp ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as LastLegBetTypeName,
					LAST_VALUE(GroupingType)		OVER (PARTITION BY tb.LedgerID,tb.LedgerSource ORDER BY tb.MasterTransactionTimestamp ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as LastGroupingType,
					LAST_VALUE(tb.CLassID)			OVER (PARTITION BY tb.LedgerID,tb.LedgerSource ORDER BY tb.MasterTransactionTimestamp ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as LastClassID,
					LAST_VALUE(LastBetId)			OVER (PARTITION BY tb.LedgerID,tb.LedgerSource ORDER BY tb.MasterTransactionTimestamp ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as LastBetID
					FROM #PositionStaging tb
					WHERE TransactionTypeID IN ('BT') and TransactionStatusID IN ('RE') AND tb.BalanceTypeID='CLI'
				) tbp ON t.LedgerID=tbp.LedgerID AND t.LedgerSource=tbp.LedgerSource
	LEFT OUTER HASH JOIN 																																								
				(	SELECT DISTINCT 
					LedgerID,
					LedgerSource,
					FIRST_VALUE(tb.Channel)		OVER (PARTITION BY tb.LedgerID,tb.LedgerSource ORDER BY tb.MasterTransactionTimestamp) as FirstChannel,
					LAST_VALUE(tb.Channel)		OVER (PARTITION BY tb.LedgerID,tb.LedgerSource ORDER BY tb.MasterTransactionTimestamp ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as LastChannel
					FROM #PositionStaging tb
					WHERE TransactionTypeID IN ('DP') and TransactionStatusID IN ('CO')  AND tb.BalanceTypeID='CLI'
				) td ON t.LedgerID=td.LedgerID AND t.LedgerSource=td.LedgerSource
	GROUP BY 
		t.BatchKey,
		t.LedgerID,	
		t.LedgerSource,
		t.MasterDayText
		OPTION (RECOMPILE)

	-------------------------SUM ANALYSIS-----------------------
	EXEC [Control].Sp_Log	@BatchKey,'Recursive.Sp_PositionFactCheck','Sum Analysis','Start', @RowsProcessed = @@ROWCOUNT

	SELECT 
		t.BatchKey,
		t.LedgerID,	
		t.LedgerSource,
		t.AnalysisDayText,
		MAX(HasDeposited) as HasDeposited,
		MAX(HasWithdrawn) as HasWithdrawn,
		MAX(HasBet) as HasBet,
		MAX(HasWon)	as HasWon,
		MAX(HasLost) as HasLost,
		MIN(ISNULL(tbp.FirstBetTypeName,'N/A')) as FirstBetTypeName,
		MIN(ISNULL(tbp.FirstBetSubTypeName,'N/A')) as FirstBetSubTypeName,
		MIN(ISNULL(tbp.FirstContractType,'N/A')) as FirstContractType,
		MIN(ISNULL(tbp.FirstGroupingType,'N/A')) as FirstGroupingType,
		MIN(ISNULL(tbp.FirstLegBetTypeName,'N/A')) as FirstLegBetTypeName,
		MIN(ISNULL(tbp.FirstClassID,0)) as FirstClassID,
		MIN(ISNULL(td.FirstChannel,0)) as FirstChannel,
		MIN(ISNULL(tbp.LastBetTypeName,'N/A')) as LastBetTypeName,
		MIN(ISNULL(tbp.LastBetSubTypeName,'N/A')) as LastBetSubTypeName,
		MIN(ISNULL(tbp.LastContractType,'N/A')) as LastContractType,
		MIN(ISNULL(tbp.LastGroupingType,'N/A')) as LastGroupingType,
		MIN(ISNULL(tbp.LastLegBetTypeName,'N/A')) as LastLegBetTypeName,
		MIN(ISNULL(tbp.LastClassID,0)) as LastClassID,
		MIN(ISNULL(td.LastChannel,0)) as LastChannel,
		SUM(Turnover) as Turnover,
		SUM(GrossWin) as GrossWin,
		SUM(BetCount) as BetCount,
		SUM(DepositsCompleted) as DepositsCompleted,
		SUM(WithdrawalsCompleted) as WithdrawalsCompleted,
		SUM(Bonus) as Bonus,
		MIN(FirstBetDate) as FirstBetDate,
		MAX(LastBetDate) as LastBetDate,
		MIN(FirstDepositDate) as FirstDepositDate,
		MAX(LastDepositDate) as LastDepositDate,
		MIN(FirstDepositId) as FirstDepositId,
		MIN(tbp.FirstBetId) as FirstBetId,
		MAX(tbp.LastBetID) as LastBetID
	INTO #PositionUpdateAnalysis
	FROM #PositionStaging t
	LEFT OUTER HASH JOIN																																								
				(	SELECT DISTINCT
					LedgerID,
					LedgerSource,
					FIRST_VALUE(BetTypeName)		OVER (PARTITION BY tb.LedgerID,tb.LedgerSource ORDER BY tb.MasterTransactionTimestamp) as FirstBetTypeName,
					FIRST_VALUE(ContractType)		OVER (PARTITION BY tb.LedgerID,tb.LedgerSource ORDER BY tb.MasterTransactionTimestamp) as FirstContractType,
					FIRST_VALUE(BetSubTypeName)		OVER (PARTITION BY tb.LedgerID,tb.LedgerSource ORDER BY tb.MasterTransactionTimestamp) as FirstBetSubTypeName,
					FIRST_VALUE(LegBetTypeName)		OVER (PARTITION BY tb.LedgerID,tb.LedgerSource ORDER BY tb.MasterTransactionTimestamp) as FirstLegBetTypeName,
					FIRST_VALUE(GroupingType)		OVER (PARTITION BY tb.LedgerID,tb.LedgerSource ORDER BY tb.MasterTransactionTimestamp) as FirstGroupingType,
					FIRST_VALUE(FirstBetId)			OVER (PARTITION BY tb.LedgerID,tb.LedgerSource ORDER BY tb.MasterTransactionTimestamp) as FirstBetId,
					FIRST_VALUE(tb.CLassID)			OVER (PARTITION BY tb.LedgerID,tb.LedgerSource ORDER BY tb.MasterTransactionTimestamp) as FirstClassID,
					LAST_VALUE(BetTypeName)			OVER (PARTITION BY tb.LedgerID,tb.LedgerSource ORDER BY tb.MasterTransactionTimestamp ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as LastBetTypeName,
					LAST_VALUE(ContractType)		OVER (PARTITION BY tb.LedgerID,tb.LedgerSource ORDER BY tb.MasterTransactionTimestamp ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as LastContractType,
					LAST_VALUE(BetSubTypeName)		OVER (PARTITION BY tb.LedgerID,tb.LedgerSource ORDER BY tb.MasterTransactionTimestamp ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as LastBetSubTypeName,
					LAST_VALUE(LegBetTypeName)		OVER (PARTITION BY tb.LedgerID,tb.LedgerSource ORDER BY tb.MasterTransactionTimestamp ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as LastLegBetTypeName,
					LAST_VALUE(GroupingType)		OVER (PARTITION BY tb.LedgerID,tb.LedgerSource ORDER BY tb.MasterTransactionTimestamp ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as LastGroupingType,
					LAST_VALUE(tb.CLassID)			OVER (PARTITION BY tb.LedgerID,tb.LedgerSource ORDER BY tb.MasterTransactionTimestamp ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as LastClassID,
					LAST_VALUE(LastBetId)			OVER (PARTITION BY tb.LedgerID,tb.LedgerSource ORDER BY tb.MasterTransactionTimestamp ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as LastBetID
					FROM #PositionStaging tb
					WHERE TransactionTypeID IN ('BT') and TransactionStatusID IN ('RE') AND tb.BalanceTypeID='CLI'
				) tbp ON t.LedgerID=tbp.LedgerID AND t.LedgerSource=tbp.LedgerSource
	LEFT OUTER HASH JOIN 																																								
				(	SELECT DISTINCT 
					LedgerID,
					LedgerSource,
					FIRST_VALUE(tb.Channel)		OVER (PARTITION BY tb.LedgerID,tb.LedgerSource ORDER BY tb.MasterTransactionTimestamp) as FirstChannel,
					LAST_VALUE(tb.Channel)		OVER (PARTITION BY tb.LedgerID,tb.LedgerSource ORDER BY tb.MasterTransactionTimestamp ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as LastChannel
					FROM #PositionStaging tb
					WHERE TransactionTypeID IN ('DP') and TransactionStatusID IN ('CO')  AND tb.BalanceTypeID='CLI'
				) td ON t.LedgerID=td.LedgerID AND t.LedgerSource=td.LedgerSource
	GROUP BY 
		t.BatchKey,
		t.LedgerID,	
		t.LedgerSource,
		t.AnalysisDayText
		OPTION (RECOMPILE)

	-------------------------------------------------------------------------------------------
	EXEC [Control].Sp_Log	@BatchKey,'Recursive.Sp_PositionFactCheck','Insert Missing Master Values','Start', @RowsProcessed = @@ROWCOUNT

	DROP TABLE #PositionStaging

	Select	a.*
	INTO #NoRecordMaster
	From #PositionUpdate a
	LEFT OUTER JOIN Stage.Position b on b.BatchKey = @BatchKey and a.LedgerID = b.LedgerID and a.LedgerSource = b.LedgerSource and a.MasterDayText = b.MasterDayText
	Where b.LedgerID is null
	OPTION (RECOMPILE)

	Select	a.*,
			dz.DayKey,
			acc.AccountKey
	INTO #LookupMaster
	From #NoRecordMaster a
	INNER JOIN [$(Dimensional)].Dimension.DayZone dz on a.MasterDayText = dz.MasterDayText and dz.AnalysisDayText = 'N/A'
	INNER JOIN [$(Dimensional)].Dimension.Account acc on a.LedgerId = acc.LedgerId and acc.LedgerSource = a.LedgerSource
	OPTION (RECOMPILE)

	INSERT INTO Stage.Position
	Select	a.BatchKey,
			a.LedgerID,
			'IAA' as LedgerSource,
			'CLI' as BalanceTypeID,
			a.MasterDayText,
			'N/A' AnalysisDayText,
			c.OpeningBalance,
			c.TransactedAmount,
			c.ClosingBalance,
			CONVERT(Datetime2(3),CASE WHEN FDDZ.MasterDayText = 'N/A' THEN null else FDDZ.MasterDayText END) as FirstCreditDate,
			CONVERT(Datetime2(3),CASE WHEN LDDZ.MasterDayText = 'N/A' THEN null else LDDZ.MasterDayText END) as LastDepositDate,
			null as FirstDebitDate,
			null as LastDebitDate,
			null as FirstBetID,
			CONVERT(Datetime2(3),CASE WHEN FBD.MasterDayText = 'N/A' THEN null else FBD.MasterDayText END) as FirstBetDate,
			FBT.BetType as FirstBetType,
			FBT.BetSubType as FirstBetSubType,
			FBT.GroupingType as FirstGroupingType,
			FBT.BetGroupType as FirstBetGroupType,
			FBT.LegType as FirstLegType,
			FCH.ChannelId as FirstChannel,
			null as FirstEventId,
			FCL.ClassId as FirstClassId,
			null as LastBetId,
			CONVERT(Datetime2(3),CASE WHEN LBD.MasterDayText = 'N/A' THEN null else LBD.MasterDayText END) as LastBetDate,
			LBT.BetType as LastBetType,
			LBT.BetSubType as LastBetSubType,
			LBT.GroupingType as LastGroupingType,
			LBT.BetGroupType as LastBetGroupType,
			LBT.LegType as LastLegType,
			LCH.ChannelId as LastChannel,
			null as LastEventId,
			LCL.ClassId as LastClassId,
			IsNull(Act.HasDeposited,'No') as HasDeposited,
			IsNull(Act.HasWithdrawn,'No') as HasWithdrawn,
			IsNull(Act.HasBet,'No') as HasBet,
			IsNull(Act.HasWon,'No') as HasWon,
			IsNull(Act.HasLost,'No') as HasLost,
			c.LifetimeTurnover,
			c.LifetimeGrossWin,
			c.LifetimeBetCount,
			c.LifetimeBonus,
			c.LifetimeDeposits,
			c.LifetimeWithdrawals,
			ACS.FromDate MasterTimeStamp,
			ACS.FromDate FromDate,
			null as FirstCreditId,
			null as LastCreditId,
			null as TierNumber,
			null as TierNumberFiscal,
			null as Turnover,
			null as TurnoverCalenderMonthToDate,
			null as TurnoverFiscalMonthToDate,
			NULL as DaysSinceAccountOpened,
			NULL as DaysSinceFirstDeposit,
			NULL as DaysSinceFirstBet,
			NULL as DaysSinceLastDeposit,
			NULL as DaysSinceLastBet,
			'N/A' as RevenueLifeCycle,
			'Unknown' as LifeStage,
			'Unknown' as PreviousLifeStage
	From #LookupMaster a
	INNER JOIN [$(Dimensional)].Fact.Position c on c.AccountKey = a.AccountKey and c.DayKey = a.DayKey
	LEFT OUTER JOIN [$(Dimensional)].Dimension.DayZone FDDZ on c.FirstDepositDayKey = FDDZ.DayKey
	LEFT OUTER JOIN [$(Dimensional)].Dimension.DayZone LDDZ on c.LastDepositDayKey = LDDZ.DayKey
	LEFT OUTER JOIN [$(Dimensional)].Dimension.DayZone FBD on c.FirstBetDayKey = FBD.DayKey
	LEFT OUTER JOIN [$(Dimensional)].Dimension.BetType FBT on c.FirstBetTypeKey = FBT.BetTypeKey
	LEFT OUTER JOIN [$(Dimensional)].Dimension.Channel FCH on c.FirstChannelKey = FCH.ChannelKey
	LEFT OUTER JOIN [$(Dimensional)].Dimension.Class FCL on c.FirstClassKey = FCL.ClassKey
	LEFT OUTER JOIN [$(Dimensional)].Dimension.DayZone LBD on c.LastBetDayKey = LBD.DayKey
	LEFT OUTER JOIN [$(Dimensional)].Dimension.BetType LBT on c.LastBetTypeKey = LBT.BetTypeKey
	LEFT OUTER JOIN [$(Dimensional)].Dimension.Channel LCH on c.LastChannelKey = LCH.ChannelKey
	LEFT OUTER JOIN [$(Dimensional)].Dimension.Class LCL on c.LastClassKey = LCL.ClassKey
	LEFT OUTER JOIN [$(Dimensional)].Dimension.Activity Act on c.ActivityKey = Act.ActivityKey
	LEFT OUTER JOIN [$(Dimensional)].Dimension.AccountStatus Acs on c.AccountStatusKey = Acs.AccountStatusKey
	OPTION (RECOMPILE)

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	---Analysis---
	EXEC [Control].Sp_Log	@BatchKey,'Recursive.Sp_PositionFactCheck','Insert Missing Analysis Values','Start', @RowsProcessed = @RowCount

	Select	a.*
	INTO #NoRecordAnalysis
	From #PositionUpdateAnalysis a
	LEFT OUTER JOIN Stage.Position b on b.BatchKey = @BatchKey and a.LedgerID = b.LedgerID and a.LedgerSource = b.LedgerSource and a.AnalysisDayText = b.AnalysisDayText
	Where b.LedgerID is null
	OPTION (RECOMPILE)

	Select	a.*,
			dz.DayKey,
			acc.AccountKey
	INTO #LookupAnalysis
	From #NoRecordAnalysis a
	INNER JOIN [$(Dimensional)].Dimension.DayZone dz on a.AnalysisDayText = dz.AnalysisDayText and dz.MasterDayText = 'N/A'
	INNER JOIN [$(Dimensional)].Dimension.Account acc on a.LedgerId = acc.LedgerId and acc.LedgerSource = a.LedgerSource
	OPTION (RECOMPILE)

	INSERT INTO Stage.Position
	Select	a.BatchKey,
			a.LedgerID,
			'IAA' as LedgerSource,
			'CLI' as BalanceTypeID,
			'N/A' MasterDayText,
			a.AnalysisDayText,
			c.OpeningBalance,
			c.TransactedAmount,
			c.ClosingBalance,
			CONVERT(Datetime2(3),CASE WHEN FDDZ.MasterDayText = 'N/A' THEN null else FDDZ.MasterDayText END) as FirstCreditDate,
			CONVERT(Datetime2(3),CASE WHEN LDDZ.MasterDayText = 'N/A' THEN null else LDDZ.MasterDayText END) as LastDepositDate,
			null as FirstDebitDate,
			null as LastDebitDate,
			null as FirstBetID,
			CONVERT(Datetime2(3),CASE WHEN FBD.MasterDayText = 'N/A' THEN null else FBD.MasterDayText END) as FirstBetDate,
			FBT.BetType as FirstBetType,
			FBT.BetSubType as FirstBetSubType,
			FBT.GroupingType as FirstGroupingType,
			FBT.BetGroupType as FirstBetGroupType,
			FBT.LegType as FirstLegType,
			FCH.ChannelId as FirstChannel,
			null as FirstEventId,
			FCL.ClassId as FirstClassId,
			null as LastBetId,
			CONVERT(Datetime2(3),CASE WHEN LBD.MasterDayText = 'N/A' THEN null else LBD.MasterDayText END) as LastBetDate,
			LBT.BetType as LastBetType,
			LBT.BetSubType as LastBetSubType,
			LBT.GroupingType as LastGroupingType,
			LBT.BetGroupType as LastBetGroupType,
			LBT.LegType as LastLegType,
			LCH.ChannelId as LastChannel,
			null as LastEventId,
			LCL.ClassId as LastClassId,
			IsNull(Act.HasDeposited,'No') as HasDeposited,
			IsNull(Act.HasWithdrawn,'No') as HasWithdrawn,
			IsNull(Act.HasBet,'No') as HasBet,
			IsNull(Act.HasWon,'No') as HasWon,
			IsNull(Act.HasLost,'No') as HasLost,
			c.LifetimeTurnover,
			c.LifetimeGrossWin,
			c.LifetimeBetCount,
			c.LifetimeBonus,
			c.LifetimeDeposits,
			c.LifetimeWithdrawals,
			ACS.FromDate MasterTimeStamp,
			ACS.FromDate FromDate,
			null as FirstCreditId,
			null as LastCreditId,
			null as TierNumber,
			null as TierNumberFiscal,
			null as Turnover,
			null as TurnoverCalenderMonthToDate,
			null as TurnoverFiscalMonthToDate,
			NULL as DaysSinceAccountOpened,
			NULL as DaysSinceFirstDeposit,
			NULL as DaysSinceFirstBet,
			NULL as DaysSinceLastDeposit,
			NULL as DaysSinceLastBet,
			'N/A' as RevenueLifeCycle,
			'Unknown' as LifeStage,
			'Unknown' as PreviousLifeStage
	From #LookupAnalysis a
	INNER JOIN [$(Dimensional)].Fact.Position c on c.AccountKey = a.AccountKey and c.DayKey = a.DayKey
	LEFT OUTER JOIN [$(Dimensional)].Dimension.DayZone FDDZ on c.FirstDepositDayKey = FDDZ.DayKey
	LEFT OUTER JOIN [$(Dimensional)].Dimension.DayZone LDDZ on c.LastDepositDayKey = LDDZ.DayKey
	LEFT OUTER JOIN [$(Dimensional)].Dimension.DayZone FBD on c.FirstBetDayKey = FBD.DayKey
	LEFT OUTER JOIN [$(Dimensional)].Dimension.BetType FBT on c.FirstBetTypeKey = FBT.BetTypeKey
	LEFT OUTER JOIN [$(Dimensional)].Dimension.Channel FCH on c.FirstChannelKey = FCH.ChannelKey
	LEFT OUTER JOIN [$(Dimensional)].Dimension.Class FCL on c.FirstClassKey = FCL.ClassKey
	LEFT OUTER JOIN [$(Dimensional)].Dimension.DayZone LBD on c.LastBetDayKey = LBD.DayKey
	LEFT OUTER JOIN [$(Dimensional)].Dimension.BetType LBT on c.LastBetTypeKey = LBT.BetTypeKey
	LEFT OUTER JOIN [$(Dimensional)].Dimension.Channel LCH on c.LastChannelKey = LCH.ChannelKey
	LEFT OUTER JOIN [$(Dimensional)].Dimension.Class LCL on c.LastClassKey = LCL.ClassKey
	LEFT OUTER JOIN [$(Dimensional)].Dimension.Activity Act on c.ActivityKey = Act.ActivityKey
	LEFT OUTER JOIN [$(Dimensional)].Dimension.AccountStatus Acs on c.AccountStatusKey = Acs.AccountStatusKey
	OPTION (RECOMPILE)

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	-------------------------------------------------------------------------------------------
	-------------------------------------------------------------------------------------------
	EXEC [Control].Sp_Log	@BatchKey,'Recursive.Sp_PositionFactCheck','Get Prior Values If Any','Start', @RowsProcessed = @RowCount

	Select Distinct MasterDayText, AnalysisDayText
	INTO #Days 
	From Stage.Position
	Where BatchKey = @BatchKey
	OPTION (RECOMPILE)

	Select Distinct DayKey
	INTO #DayKeys
	From #Days a
	INNER JOIN [$(Dimensional)].Dimension.DayZone b on (a.AnalysisDayText = b.AnalysisDayText and a.AnalysisDayText <> 'N/A') OR (a.MasterDayText = b.MasterDayText and a.MasterDayText <> 'N/A')
	Where MasterDayKey <> -1 and AnalysisDayKey <> -1
	OPTION (RECOMPILE)

	Select	t.DayKey,
			a.LedgerID,
			a.LedgerSource,
			dz.AnalysisDayText,
			dz.MasterDayText,
			SUM(CASE WHEN ISNULL(W.WalletName,'CASH')='CASH' and b.BalanceTypeID = 'UNS' and td.TransactionTypeID = 'BT'  and td.TransactionStatusID IN ('LO','WN','UN') AND td.TransactionMethodID='SE' THEN -t.TransactedAmount ELSE 0 END) as Turnover,
			SUM(CASE WHEN ISNULL(W.WalletName,'CASH')='CASH' and b.BalanceTypeID = 'P&L' and td.TransactionTypeID = 'BT' THEN t.TransactedAmount ELSE 0 END) as GrossWin,
			SUM(CASE WHEN td.TransactionTypeID = 'BT' and td.TransactionStatusID ='RE' THEN 1 ELSE 0 END) as BetCount,
			SUM(CASE WHEN ISNULL(W.WalletName,'CASH')='CASH' and td.TransactionTypeID = 'DP' and td.TransactionStatusID IN('CO','RV') THEN t.TransactedAmount ELSE 0	END) as	DepositsCompleted,
			SUM(CASE WHEN ISNULL(W.WalletName,'CASH')='CASH' and b.BalanceTypeID = 'PWD' and td.TransactionTypeID = 'WI' and td.TransactionStatusID IN ('CO','RV') THEN t.TransactedAmount ELSE 0 END) WithdrawalsCompleted,
			SUM(CASE WHEN td.TransactionTypeID = 'BO' and b.BalanceTypeID = 'CLI' THEN TransactedAmount ELSE 0 END) as Bonus
	INTO #PreviousValues
	From [$(Dimensional)].Fact.[transaction] t
	INNER JOIN [$(Dimensional)].Dimension.Wallet w on t.WalletKey = w.WalletKey
	INNER JOIN [$(Dimensional)].Dimension.BalanceType b on t.BalanceTypeKey = b.BalanceTypeKey
	INNER JOIN [$(Dimensional)].Dimension.TransactionDetail td on t.TransactionDetailKey = td.TransactionDetailKey
	INNER JOIN [$(Dimensional)].Dimension.DayZone dz on t.DayKey = dz.DayKey
	INNER JOIN [$(Dimensional)].Dimension.Account a on t.AccountKey = a.AccountKey
	Where t.DayKey IN (Select DayKey From #DayKeys)
	and (	   (b.BalanceTypeID ='CLI')																							
				OR (b.BalanceTypeID = 'UNS' and td.TransactionTypeID = 'BT'  and td.TransactionStatusID IN ('LO','WN','UN'))		
				OR (b.BalanceTypeID = 'P&L' and td.TransactionTypeID = 'BT')
				OR (B.BalanceTypeID = 'PWD' and td.TransactionTypeID = 'WI' and td.TransactionStatusID IN ('CO','RV'))
		)
	and t.CreatedBatchKey < @BatchKey
	GROUP BY
		t.DayKey,
		a.LedgerID,
		a.LedgerSource,
		dz.AnalysisDayText,
		dz.MasterDayText
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,'Recursive.Sp_PositionFactCheck','Previous Master','Start', @RowsProcessed = @@ROWCOUNT

	Select	LedgerID,
			LedgerSource,
			'N/A' AnalysisDayText,
			MasterDayText,
			SUM(Turnover) Turnover,
			SUM(GrossWin) GrossWin,
			SUM(BetCount) BetCount,
			SUM(DepositsCompleted) DepositsCompleted,
			SUM(WithdrawalsCompleted) WithdrawalsCompleted,
			SUM(Bonus) Bonus
	INTO #PreviousMaster
	from #PreviousValues
	Group By 
		LedgerID,
		LedgerSource,
		MasterDayText
		OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,'Recursive.Sp_PositionFactCheck','Previous Analysis','Start', @RowsProcessed = @@ROWCOUNT

	Select	LedgerID,
			LedgerSource,
			AnalysisDayText,
			'N/A' MasterDayText,
			SUM(Turnover) Turnover,
			SUM(GrossWin) GrossWin,
			SUM(BetCount) BetCount,
			SUM(DepositsCompleted) DepositsCompleted,
			SUM(WithdrawalsCompleted) WithdrawalsCompleted,
			SUM(Bonus) Bonus
	INTO #PreviousAnalysis
	from #PreviousValues
	Group By 
		LedgerID,
		LedgerSource,
		AnalysisDayText
		OPTION (RECOMPILE)

	-------------------------------------------------------------------------------------------
	EXEC [Control].Sp_Log	@BatchKey,'Recursive.Sp_PositionFactCheck','Update Staging Master','Start', @RowsProcessed = @@ROWCOUNT

	DROP TABLE #PreviousValues
	DROP TABLE #Days
	DROP TABLE #DayKeys

	Update b
	Set b.HasBet = CASE WHEN a.HasBet='Yes' THEN a.HasBet ELSE b.HasBet END,
		b.HasDeposited = CASE WHEN a.HasDeposited='Yes' THEN a.HasDeposited ELSE b.HasDeposited END,
		b.HasLost = CASE WHEN a.HasLost='Yes' THEN a.HasLost ELSE b.HasLost END,
		b.HasWithdrawn = CASE WHEN a.HasWithdrawn='Yes' THEN a.HasWithdrawn ELSE b.HasWithdrawn END,
		b.HasWon = CASE WHEN a.HasWon='Yes' THEN a.HasWon ELSE b.HasWon END,
		b.LifetimeGrossWin = IsNull(b.LifetimeGrossWin,0) + IsNull(a.GrossWin,0) + IsNull(c.GrossWin,0),
		b.LifetimeTurnover = IsNull(b.LifetimeTurnover,0) + IsNull(a.Turnover,0) + IsNull(c.Turnover,0),
		b.LifetimeBetCount = IsNull(b.LifetimeBetCount,0) + IsNull(a.BetCount,0) + IsNull(c.BetCount,0),
		b.LifetimeDeposits = IsNull(b.LifetimeDeposits,0) + IsNull(a.DepositsCompleted,0) + IsNull(c.DepositsCompleted,0),
		b.LifetimeWithdrawals = IsNull(b.LifetimeWithdrawals,0) + IsNull(a.WithdrawalsCompleted,0) + IsNull(c.WithdrawalsCompleted,0),
		b.LifetimeBonus = IsNull(b.LifetimeBonus,0) + IsNull(a.Bonus,0) + IsNull(c.Bonus,0),
		b.FirstCreditDate = CASE WHEN b.FirstCreditDate IS NULL THEN a.FirstDepositDate ELSE b.FirstCreditDate END,
		b.FirstBetId = CASE WHEN b.FirstBetDate IS NULL THEN a.FirstBetId ELSE b.FirstBetId END,
		b.FirstBetDate = CASE WHEN b.FirstBetDate IS NULL THEN a.FirstBetDate ELSE b.FirstBetDate END,
		b.FirstBetType = CASE WHEN b.FirstBetType IS NULL THEN a.FirstBetTypeName ELSE b.FirstBetType END,
		b.FirstBetSubType = CASE WHEN b.FirstBetSubType IS NULL THEN a.FirstBetSubTypeName ELSE b.FirstBetSubType END,
		b.firstGroupingType = CASE WHEN b.firstGroupingType IS NULL THEN a.FirstGroupingType ELSE b.firstGroupingType END,
		b.firstBetGroupType = CASE WHEN b.firstBetGroupType IS NULL THEN a.FirstContractType ELSE b.firstBetGroupType END,
		b.FirstLegType = CASE WHEN b.FirstLegType IS NULL THEN a.FirstLegBetTypeName ELSE b.FirstLegType END,
		b.FirstChannel = CASE WHEN b.FirstChannel IS NULL THEN a.FirstChannel ELSE b.FirstChannel END,
		b.FirstClassId = CASE WHEN b.FirstClassId IS NULL THEN a.FirstClassID ELSE b.FirstClassId END,
		b.LastCreditDate = IsNull(a.LastDepositDate,b.LastCreditDate),
		b.LastBetId = IsNull(a.LastBetId,b.LastBetId),
		b.LastBetDate = IsNull(a.LastBetDate,b.LastBetDate),
		b.LastBetType = IsNull(a.LastBetTypeName,b.LastBetType),
		b.LastBetSubType = IsNull(a.LastBetSubTypeName,b.LastBetSubType),
		b.LastGroupingType = IsNull(a.LastGroupingType,b.LastGroupingType),
		b.LastBetGroupType = IsNull(a.LastContractType,b.LastBetGroupType),
		b.LastLegType = IsNull(a.LastLegBetTypeName,b.LastLegType),
		b.LastChannel = IsNull(a.LastChannel,b.LastChannel),
		b.LastClassId = IsNull(a.LastClassID,b.LastClassId),
		b.FirstCreditId = CASE WHEN b.FirstCreditDate IS NULL THEN a.FirstDepositId ELSE b.FirstCreditId END,
		b.Turnover = IsNull(a.Turnover,0) + IsNull(c.Turnover,0)
	From #PositionUpdate a
	INNER JOIN Stage.Position b on b.BatchKey = a.BatchKey and a.LedgerID = b.LedgerID and a.LedgerSource = b.LedgerSource and a.MasterDayText = b.MasterDayText
	LEFT OUTER JOIN #PreviousMaster c on b.LedgerID = c.LedgerID and b.LedgerSource = c.LedgerSource and b.MasterDayText = c.MasterDayText
	Where b.BatchKey = @BatchKey
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,'Recursive.Sp_PositionFactCheck','Update Staging Analysis','Start', @RowsProcessed = @@ROWCOUNT

	Update b
	Set b.HasBet = CASE WHEN a.HasBet='Yes' THEN a.HasBet ELSE b.HasBet END,
		b.HasDeposited = CASE WHEN a.HasDeposited='Yes' THEN a.HasDeposited ELSE b.HasDeposited END,
		b.HasLost = CASE WHEN a.HasLost='Yes' THEN a.HasLost ELSE b.HasLost END,
		b.HasWithdrawn = CASE WHEN a.HasWithdrawn='Yes' THEN a.HasWithdrawn ELSE b.HasWithdrawn END,
		b.HasWon = CASE WHEN a.HasWon='Yes' THEN a.HasWon ELSE b.HasWon END,
		b.LifetimeGrossWin = IsNull(b.LifetimeGrossWin,0) + IsNull(a.GrossWin,0) + IsNull(c.GrossWin,0),
		b.LifetimeTurnover = IsNull(b.LifetimeTurnover,0) + IsNull(a.Turnover,0) + IsNull(c.Turnover,0),
		b.LifetimeBetCount = IsNull(b.LifetimeBetCount,0) + IsNull(a.BetCount,0) + IsNull(c.BetCount,0),
		b.LifetimeDeposits = IsNull(b.LifetimeDeposits,0) + IsNull(a.DepositsCompleted,0) + IsNull(c.DepositsCompleted,0),
		b.LifetimeWithdrawals = IsNull(b.LifetimeWithdrawals,0) + IsNull(a.WithdrawalsCompleted,0) + IsNull(c.WithdrawalsCompleted,0),
		b.LifetimeBonus = IsNull(b.LifetimeBonus,0) + IsNull(a.Bonus,0) + IsNull(c.Bonus,0),
		b.FirstCreditDate = CASE WHEN b.FirstCreditDate IS NULL THEN a.FirstDepositDate ELSE b.FirstCreditDate END,
		b.FirstBetId = CASE WHEN b.FirstBetDate IS NULL THEN a.FirstBetId ELSE b.FirstBetId END,
		b.FirstBetDate = CASE WHEN b.FirstBetDate IS NULL THEN a.FirstBetDate ELSE b.FirstBetDate END,
		b.FirstBetType = CASE WHEN b.FirstBetType IS NULL THEN a.FirstBetTypeName ELSE b.FirstBetType END,
		b.FirstBetSubType = CASE WHEN b.FirstBetSubType IS NULL THEN a.FirstBetSubTypeName ELSE b.FirstBetSubType END,
		b.firstGroupingType = CASE WHEN b.firstGroupingType IS NULL THEN a.FirstGroupingType ELSE b.firstGroupingType END,
		b.firstBetGroupType = CASE WHEN b.firstBetGroupType IS NULL THEN a.FirstContractType ELSE b.firstBetGroupType END,
		b.FirstLegType = CASE WHEN b.FirstLegType IS NULL THEN a.FirstLegBetTypeName ELSE b.FirstLegType END,
		b.FirstChannel = CASE WHEN b.FirstChannel IS NULL THEN a.FirstChannel ELSE b.FirstChannel END,
		b.FirstClassId = CASE WHEN b.FirstClassId IS NULL THEN a.FirstClassID ELSE b.FirstClassId END,
		b.LastCreditDate = IsNull(a.LastDepositDate,b.LastCreditDate),
		b.LastBetId = IsNull(a.LastBetId,b.LastBetId),
		b.LastBetDate = IsNull(a.LastBetDate,b.LastBetDate),
		b.LastBetType = IsNull(a.LastBetTypeName,b.LastBetType),
		b.LastBetSubType = IsNull(a.LastBetSubTypeName,b.LastBetSubType),
		b.LastGroupingType = IsNull(a.LastGroupingType,b.LastGroupingType),
		b.LastBetGroupType = IsNull(a.LastContractType,b.LastBetGroupType),
		b.LastLegType = IsNull(a.LastLegBetTypeName,b.LastLegType),
		b.LastChannel = IsNull(a.LastChannel,b.LastChannel),
		b.LastClassId = IsNull(a.LastClassID,b.LastClassId),
		b.FirstCreditId = CASE WHEN b.FirstCreditDate IS NULL THEN a.FirstDepositId ELSE b.FirstCreditId END,
		b.Turnover = IsNull(a.Turnover,0) + IsNull(c.Turnover,0)
	From #PositionUpdateAnalysis a
	INNER JOIN Stage.Position b on b.BatchKey = a.BatchKey and a.LedgerID = b.LedgerID and a.LedgerSource = b.LedgerSource and a.AnalysisDayText = b.AnalysisDayText
	LEFT OUTER JOIN #PreviousAnalysis c on b.LedgerID = c.LedgerID and b.LedgerSource = c.LedgerSource and b.AnalysisDayText = c.AnalysisDayText
	Where b.BatchKey = @BatchKey
	OPTION (RECOMPILE)

	-------------------------------------------------------------------------------------------
	EXEC [Control].Sp_Log	@BatchKey,'Recursive.Sp_PositionFactCheck','Update Days Since','Start', @RowsProcessed = @@ROWCOUNT

	DROP TABLE #PreviousMaster
	DROP TABLE #PreviousAnalysis

-- RH - need to improve this logic - as it is, mandates that Dimensions must be loaded before this process is run so that any new accounts in this batch are already in Dimensional rather than just in staging
	Update a
			Set DaysSinceAccountOpened = CASE WHEN AccountOpenedDate is null THEN -1 ELSE DATEDIFF(dd,AccountOpenedDate,CONVERT(date, CASE WHEN MasterDayText = 'N/A' THEN AnalysisDayText ELSE MasterDayText END)) END,
				DaysSinceFirstDeposit = CASE WHEN FirstCreditDate is null THEN -1 ELSE DATEDIFF(dd,FirstCreditDate,CONVERT(date, CASE WHEN MasterDayText = 'N/A' THEN AnalysisDayText ELSE MasterDayText END)) END,
				DaysSinceLastDeposit = CASE WHEN LastCreditDate is null THEN -1 ELSE DATEDIFF(dd,LastCreditDate,CONVERT(date, CASE WHEN MasterDayText = 'N/A' THEN AnalysisDayText ELSE MasterDayText END)) END,
				DaysSinceFirstBet = CASE WHEN FirstBetDate is null THEN -1 ELSE DATEDIFF(dd,FirstBetDate,CONVERT(date, CASE WHEN MasterDayText = 'N/A' THEN AnalysisDayText ELSE MasterDayText END)) END,
				DaysSinceLastBet = CASE WHEN LastBetDate is null THEN -1 ELSE DATEDIFF(dd,LastBetDate,CONVERT(date, CASE WHEN MasterDayText = 'N/A' THEN AnalysisDayText ELSE MasterDayText END)) END
	From Stage.Position a
	LEFT OUTER JOIN [$(Dimensional)].Dimension.Account b on a.LedgerID = b.LedgerId and a.LedgerSource = b.LedgerSource
	Where a.BatchKey = @BatchKey
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,'Recursive.Sp_PositionFactCheck','Update LifeStage','Start', @RowsProcessed = @@ROWCOUNT

	Update a
	Set a.LifeStage = CASE WHEN DAKS.LedgerStatus <> 'A' THEN 'Out Of Play' ELSE IsNull(r.LifeStage,'Unknown') END
	From Stage.Position a WITH(NOLOCK)
	LEFT OUTER JOIN [$(Dimensional)].Dimension.AccountStatus DAKS  WITH(NOLOCK) ON a.LedgerId=DAKS.LedgerId and a.LedgerSource= DAKS.LedgerSource and CONVERT(DATETIME2(3),a.FromDate) >=DAKS.FROMDATE and CONVERT(DATETIME2(3),a.FromDate) <DAKS.TODATE
	LEFT OUTER JOIN [Reference].[LifeStages] r WITH(NOLOCK) on	
												a.DaysSinceAccountOpened >= IsNull(r.DaysSinceAccountOpenedLower,a.DaysSinceAccountOpened)
											AND	a.DaysSinceAccountOpened <= IsNull(r.DaysSinceAccountOpenedUpper,a.DaysSinceAccountOpened)
											AND	a.DaysSinceLastBet >= ISNULL(r.DaysSinceLastBetLower, a.DaysSinceLastBet) 
											AND a.DaysSinceLastBet <= ISNULL(r.DaysSinceLastBetUpper, a.DaysSinceLastBet)
	Where a.BatchKey = @BatchKey
	OPTION (RECOMPILE)

	---------------------------------------------------------------------------------------------
	EXEC [Control].Sp_Log	@BatchKey,'Recursive.Sp_PositionFactCheck','New Month To Date Turnovers','Start', @RowsProcessed = @@ROWCOUNT

	Select	LedgerID,
			LedgerSource,
			BalanceTypeID,
			MasterDayText,
			AnalysisDayText,
			IsNull(Turnover,0) Turnover,
			CASE WHEN MasterDayText = 'N/A' THEN 0 ELSE 1 END AS DayTextPartition
	INTO #PostitionStage
	From Stage.Position a
	Where BatchKey = @BatchKey
	OPTION (RECOMPILE)

	Select	a.*,
			b.DayKey,
			CASE WHEN md.PreviousDayKey = -1 THEN CONVERT(INT,CONVERT(VARCHAR,ad.PreviousDayKey) + '03') ELSE NULL END  PreviousAnalysisDayKey,
			CASE WHEN ad.PreviousDayKey = -1 THEN CONVERT(INT,CONVERT(VARCHAR,md.PreviousDayKey) + '02') ELSE NULL END  PreviousMasterDayKey,
			(CASE WHEN md.MonthKey = -1 THEN ad.DayDate ELSE md.DayDate END) DayDate,
			(CASE WHEN md.MonthKey = -1 THEN ad.MonthKey ELSE md.MonthKey END) MonthKey,
			(CASE WHEN md.MonthKey = -1 THEN ad.FiscalMonthKey ELSE md.FiscalMonthKey END) FiscalMonthKey,
			SUM(IsNull(Turnover,0)) OVER (PARTITION BY LedgerId, LedgerSource, DayTextPartition, (CASE WHEN md.MonthKey = -1 THEN ad.MonthKey ELSE md.MonthKey END) ORDER BY a.MasterDayText, a.AnalysisDayText RANGE BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) as TurnoverCalenderMonthToDate,
			SUM(IsNull(Turnover,0)) OVER (PARTITION BY LedgerId, LedgerSource, DayTextPartition, (CASE WHEN md.FiscalMonthKey = -1 THEN ad.FiscalMonthKey ELSE md.FiscalMonthKey END) ORDER BY a.MasterDayText, a.AnalysisDayText RANGE BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) as TurnoverFiscalMonthToDate
	INTO #WithMTD
	From #PostitionStage a
	INNER JOIN [$(Dimensional)].Dimension.DayZone b on a.MasterDayText = b.MasterDayText and a.AnalysisDayText = b.AnalysisDayText
	INNER JOIN [$(Dimensional)].Dimension.[Day] md on b.MasterDayKey = md.DayKey
	INNER JOIN [$(Dimensional)].Dimension.[Day] ad on b.AnalysisDayKey = ad.DayKey
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,'Recursive.Sp_PositionFactCheck','Existing Month To Date Turnovers','Start', @RowsProcessed = @@ROWCOUNT

	Select	Distinct 
			MasterDayText,
			AnalysisDayText,
			PreviousDayKey
	INTO #MinDates
	From
	(
		Select	LedgerId,
				LedgerSource,
				MIN(MasterDayText) MasterDayText,
				'N/A' AnalysisDayText,
				MIN(PreviousMasterDayKey) PreviousDayKey
		From #WithMTD
		Where AnalysisDayText = 'N/A'
		Group By	LedgerId,
					LedgerSource
		UNION
		Select	LedgerId,
				LedgerSource,
				'N/A' MasterDayText,
				MIN(AnalysisDayText) AnalysisDayText,
				MIN(PreviousAnalysisDayKey) PreviousDayKey
		From #WithMTD
		Where MasterDayText = 'N/A'
		Group By	LedgerId,
					LedgerSource
	) a
	OPTION (RECOMPILE)

	Select	LedgerID,
			LedgerSource,
			a.DayKey,
			b.MasterDayText,
			b.AnalysisDayText,
			TurnoverMTDCalender,
			TurnoverMTDFiscal,
			(CASE WHEN md.MonthKey = -1 THEN ad.MonthKey ELSE md.MonthKey END) MonthKey,
			(CASE WHEN md.MonthKey = -1 THEN ad.FiscalMonthKey ELSE md.FiscalMonthKey END) FiscalMonthKey,
			LifeStage
	INTO #ExistingMTD
	From [$(Dimensional)].Fact.Position a
	INNER JOIN #MinDates b on a.DayKey = b.PreviousDayKey
	INNER JOIN [$(Dimensional)].Dimension.DayZone dz on a.DayKey = dz.DayKey
	INNER JOIN [$(Dimensional)].Dimension.[Day] md on dz.MasterDayKey = md.DayKey
	INNER JOIN [$(Dimensional)].Dimension.[Day] ad on dz.AnalysisDayKey = ad.DayKey
	LEFT OUTER JOIN [$(Dimensional)].Dimension.LifeStage ls on a.LifeStageKey = ls.LifeStageKey
	Where a.DayKey IN
	(
		Select Distinct PreviousDayKey 
		From #MinDates
	)
	OPTION (RECOMPILE)

	-------------Sneaky, Out of place Update for Previous Life Stage Orz...-------------

	EXEC [Control].Sp_Log	@BatchKey,'Recursive.Sp_PositionFactCheck','Previous LifeStage','Start', @RowsProcessed = @@ROWCOUNT

	Update a
	Set a.PreviousLifeStage = b.LifeStage
	From Stage.Position a WITH(NOLOCK)
	INNER JOIN #ExistingMTD b on a.LedgerID = b.LedgerID and a.LedgerSource = b.LedgerSource and a.MasterDayText = b.MasterDayText and a.AnalysisDayText = b.AnalysisDayText
	Where a.BatchKey = @BatchKey
	OPTION (RECOMPILE)

	----And if previous is updated in the current batch

	EXEC [Control].Sp_Log	@BatchKey,'Recursive.Sp_PositionFactCheck','Updated Previous LifeStage','Start', @RowsProcessed = @@ROWCOUNT

	SELECT		*,
				CASE WHEN MasterDayText <> 'N/A' THEN 1 ELSE 0 END AS Part,
				LAG(LifeStage, 1, 'Unknown') OVER (PARTITION BY  LedgerId, CASE WHEN MasterDayText <> 'N/A' THEN 1 ELSE 0 END ORDER BY   MasterDayText, AnalysisDayText ASC)  AS ActualLastStage 
	INTO #UpdatedInCurrent
	from Stage.Position
	Where
	BatchKey = @BatchKey
	OPTION (RECOMPILE)

	Update b
	Set b.PreviousLifeStage = a.ActualLastStage
	From #UpdatedInCurrent a
	INNER JOIN Stage.Position b on a.LedgerID = b.LedgerId and a.LedgerSource = b.LedgerSource and a.MasterDayText = b.MasterDayText and a.AnalysisDayText = b.AnalysisDayText
	Where a.ActualLastStage <> 'Unknown' and a.PreviousLifeStage <> a.ActualLastStage
	and b.BatchKey = @BatchKey
	OPTION (RECOMPILE)

	-----------------------------------------------------------------------------------

	EXEC [Control].Sp_Log	@BatchKey,'Recursive.Sp_PositionFactCheck','Apply Month To Date Turnovers','Start', @RowsProcessed = @@ROWCOUNT

	Select	a.*,
			DENSE_RANK () over(partition by a.LedgerId, a.DayTextPartition order by a.DayDate ASC) as Ranking,
			b.TurnoverMTDCalender as PreviousTurnoverMTDCalender,
			b.TurnoverMTDFiscal as PreviousTurnoverMTDFiscal,
			b.MonthKey as ExistingMonthKey,
			b.FiscalMonthKey as ExistingfiscalMonthKey
	INTO #RankedChanges
	From #WithMTD a
	LEFT OUTER JOIN #ExistingMTD b on a.LedgerID = b.LedgerId and a.LedgerSource = b.LedgerSource and a.MasterDayText = b.MasterDayText and a.AnalysisDayText = b.AnalysisDayText
	OPTION (RECOMPILE)

	Select	LedgerId, 
			LedgerSource,
			BalanceTypeId,
			MasterDayText,
			AnalysisDayText,
			Turnover,
			DayTextPartition,
			MonthKey,
			FiscalMonthKey,
			TurnoverCalenderMonthToDate,
			TurnoverFiscalMonthToDate,
			CASE WHEN Ranking = 1 and MonthKey = ExistingMonthKey THEN IsNull(PreviousTurnoverMTDCalender,0) ELSE 0 END AS AdditionalCalenderMTD,
			CASE WHEN Ranking = 1 and FiscalMonthKey = ExistingfiscalMonthKey THEN IsNull(PreviousTurnoverMTDFiscal,0) ELSE 0 END AS AdditionalFiscalMTD
	INTO #AdditionalTurnover
	From #RankedChanges a
	OPTION (RECOMPILE)

	DROP TABLE #RankedChanges
	DROP TABLE #WithMTD
	DROP TABLE #MinDates
	DROP TABLE #ExistingMTD
	DROP TABLE #PostitionStage

	Select	*,
			SUM(IsNull(AdditionalCalenderMTD,0)) OVER (PARTITION BY LedgerId, LedgerSource, DayTextPartition, MonthKey ORDER BY MasterDayText, AnalysisDayText RANGE BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) as CalenderNew,
			SUM(IsNull(AdditionalFiscalMTD,0)) OVER (PARTITION BY LedgerId, LedgerSource, DayTextPartition, FiscalMonthKey ORDER BY MasterDayText, AnalysisDayText RANGE BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) as FiscalNew 
	INTO #AdditionalRollover
	From #AdditionalTurnover
	OPTION (RECOMPILE)

	DROP TABLE #AdditionalTurnover

	Update a
	Set a.TurnoverCalenderMonthToDate = b.TurnoverCalenderMonthToDate + CalenderNew,
		a.TurnoverFiscalMonthToDate = b.TurnoverFiscalMonthToDate + FiscalNew,
		a.TierNumber = vtc.TierNumber,
		a.TierNumberFiscal = vtf.TierNumber
	From Stage.Position a
	INNER JOIN #AdditionalRollover b on a.LedgerID = b.LedgerID and a.LedgerSource = b.LedgerSource and a.MasterDayText = b.MasterDayText and a.AnalysisDayText = b.AnalysisDayText
	LEFT OUTER JOIN [Reference].[ValueTiers] vtc ON b.TurnoverCalenderMonthToDate + CalenderNew >= vtc.ValueFloor AND b.TurnoverCalenderMonthToDate + CalenderNew < vtc.ValueCeiling
	LEFT OUTER JOIN [Reference].[ValueTiers] vtf ON b.TurnoverFiscalMonthToDate + FiscalNew >= vtf.ValueFloor AND b.TurnoverFiscalMonthToDate + FiscalNew < vtf.ValueCeiling
	Where BatchKey = @BatchKey
	OPTION (RECOMPILE)

	---------------------------------------------------------------------------------------------
	EXEC [Control].Sp_Log	@BatchKey,'Recursive.Sp_PositionFactCheck','Revenue Life Cycle','Start', @RowsProcessed = @@ROWCOUNT

	Select	BatchKey,
			LedgerID,
			LedgerSource,
			BalanceTypeID,
			MasterDayText,
			AnalysisDayText,
			FirstBetDate,
			LastBetDate,
			TurnoverCalenderMonthToDate,
			LifetimeTurnover
	INTO #BaseSetRequired
	From Stage.Position
	Where BatchKey = @BatchKey
	and TurnoverCalenderMonthToDate > 0
	OPTION (RECOMPILE)

	Select	*,
			CASE WHEN MasterDayText = 'N/A' THEN 'N/A' ELSE CONVERT(Varchar(10),DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,CONVERT(DATE,MasterDayText)),0)),120) END AS OldMasterDayText,
			CASE WHEN AnalysisDayText = 'N/A' THEN 'N/A' ELSE CONVERT(Varchar(10),DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,CONVERT(DATE,AnalysisDayText)),0)),120) END AS OldAnalysisDayText
	INTO #GetLastMonthBase
	From #BaseSetRequired
	OPTION (RECOMPILE)

	Select	a.*,
			p.TurnoverMTDCalender as LastTurnoverMTDCalender,
			p.LifetimeTurnover as LastLifetimeTurnover
	INTO #LastMonthSet
	From #GetLastMonthBase  a WITH(NOLOCK)
	INNER JOIN [$(Dimensional)].Dimension.DayZone DZ WITH(NOLOCK) ON a.OldMasterDayText = DZ.MasterDayText and a.OldAnalysisDayText = DZ.AnalysisDayText
	INNER JOIN [$(Dimensional)].Dimension.ACCOUNT DAK  WITH(NOLOCK) ON a.LedgerId=DAK.LedgerId and a.LedgerSource= DAK.LedgerSource 
	INNER JOIN [$(Dimensional)].Fact.Position p WITH(NOLOCK, FORCESEEK) ON dz.DayKey = p.DayKey and DAK.AccountKey = p.AccountKey
	--Where p.TurnoverMTDCalender > 0
	OPTION (RECOMPILE, FORCE ORDER)

	DROP TABLE #GetLastMonthBase

	Select	a.*,
			CASE 
			WHEN 
				b.LastLifetimeTurnover = 0 and a.LifetimeTurnover > 0 THEN 'FTB'
			WHEN 
				b.LastLifetimeTurnover is null and a.LifetimeTurnover > 0 THEN 'FTB'
			WHEN b.LastTurnoverMTDCalender > 0 THEN 'Retained'
			ELSE 'Reactivated'
			END AS RevenueModelLifeCycle
	INTO #UpdateSet
	From #BaseSetRequired a
	LEFT OUTER JOIN #LastMonthSet b on a.LedgerID = b.LedgerID and a.LedgerSource = b.LedgerSource and a.MasterDayText = b.MasterDayText and a.AnalysisDayText = b.AnalysisDayText
	OPTION (RECOMPILE)

	DROP TABLE #BaseSetRequired
	DROP TABLE #LastMonthSet

	Update b
	Set b.RevenueLifeCycle = a.RevenueModelLifeCycle
	From #UpdateSet a
	INNER JOIN Stage.Position b on a.LedgerID = b.LedgerID and a.LedgerSource = b.LedgerSource and a.MasterDayText = b.MasterDayText and a.AnalysisDayText = b.AnalysisDayText
	where b.BatchKey = @BatchKey
	OPTION (RECOMPILE)


	EXEC [Control].Sp_Log	@BatchKey,'Recursive.Sp_PositionFactCheck',NULL,'Success', @RowsProcessed = @@ROWCOUNT

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Recursive.Sp_PositionFactCheck', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

GO



CREATE PROC [Recursive].[Sp_Transaction_CleanDates]
(
	@BatchKey	INT
)
AS 
BEGIN TRY

/*
	Developer: Joseph George
	Date: Unknown
	Desc: Clean Dates

	Last Changed By: Edward Chen
	Last Changed Date: 6/JUL/2015
	Last Change Desc: Remove TransactionDetailID Dependency
*/

	DECLARE @ProcName VARCHAR(255) = (SELECT OBJECT_NAME(@@PROCID));

	/** OVERRIDE REMOVE ME **/
	--SET STATISTICS IO ON;
	--SET STATISTICS TIME ON;
	--DECLARE @BatchKey INT = 277;
	/**					   **/

	SET ANSI_NULLS ON;
	SET QUOTED_IDENTIFIER ON;
	SET NOCOUNT ON;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
		
	IF OBJECT_ID('tempdb..#Dim') IS NOT NULL BEGIN DROP TABLE #Dim END;
	IF OBJECT_ID('tempdb..#Additional') IS NOT NULL BEGIN DROP TABLE #Additional END;

	DECLARE @RowsProcessed INT, @RowsAffected INT

	------------------------------- Identify those transactions which are additional steps to original transactions  -----------------------------------------

	EXEC [Control].Sp_Log @BatchKey, @ProcName, 'CacheDim', 'Start';
		
	SELECT	t.TransactionID, t.TransactionIdSource, b.BalanceTypeID, d.TransactionTypeID, d.TransactionStatusID, d.TransactionMethodID, d.TransactionDirection, l.LegNumber, l.NumberOfLegs, t.MasterTransactionTimestamp, t.FromDate
	INTO	#Dim
	FROM	(	SELECT	DISTINCT TransactionId, TransactionIdSource 
				FROM	[Stage].[Transaction] 
				WHERE	BatchKey = @BatchKey AND ExistsInDim = 0
			) s
			INNER LOOP JOIN [$(Dimensional)].[Fact].[Transaction] t ON t.TransactionId = s.TransactionId AND t.TransactionIdSource = s.TransactionIdSource	AND t.CreatedBatchKey < @BatchKey
			INNER JOIN [$(Dimensional)].[Dimension].TransactionDetail d ON d.TransactionDetailKey=t.TransactionDetailKey
			INNER JOIN [$(Dimensional)].[Dimension].LegType l ON l.LegTypeKey=t.LegTypeKey
			INNER JOIN [$(Dimensional)].[Dimension].BalanceType b ON b.BalanceTypeKey=t.BalanceTypeKey
	OPTION (RECOMPILE);

	EXEC [Control].Sp_Log @BatchKey, @ProcName, 'IsolateAdditionalTrans', 'Start', @RowsProcessed = @@ROWCOUNT;
		
	SELECT	TransactionIDSource, TransactionID,  TransactionTypeID, TransactionStatusID, TransactionMethodID, TransactionDirection, BalanceTypeID, LegNumber, NumberOfLegs, MasterTransactionTimeStamp
	INTO	#Additional
	FROM 
			( SELECT	TransactionIDSource, TransactionID,  TransactionTypeID, TransactionStatusID, TransactionMethodID, TransactionDirection, BalanceTypeID, LegNumber, NumberOfLegs, MasterTransactionTimeStamp,
					DENSE_RANK() OVER(PARTITION BY TransactionId, TransactionIDSource ORDER BY FromDate) AS DenseRank, Source
			FROM 
				( 	SELECT	'S' AS Source, TransactionIDSource, TransactionID,  TransactionTypeID, TransactionStatusID, TransactionMethodID, TransactionDirection, BalanceTypeID, MasterTransactionTimeStamp, LegNumber, NumberOfLegs, FromDate
					FROM	[Stage].[Transaction]
					WHERE	BatchKey = @BatchKey AND ExistsInDim = 0 
					UNION ALL
					SELECT	'D' Source, TransactionIDSource, TransactionID, TransactionTypeID, TransactionStatusID, TransactionMethodID, TransactionDirection, BalanceTypeID, MasterTransactionTimeStamp, LegNumber, NumberOfLegs, FromDate
					FROM	#Dim
				) x
			) r
	WHERE DenseRank > 1 AND Source = 'S'
	OPTION (RECOMPILE);

	------------------------------- Assert FromDate as MasterTransactionTimestamp on Additional Transaction Steps  -----------------------------------------

	EXEC [Control].Sp_Log @BatchKey, @ProcName, 'UpdateStaging', 'Start', @RowsProcessed = @@ROWCOUNT;

	DROP TABLE #Dim

	UPDATE	t
	SET		ExistsInDim = 8
	FROM	[Stage].[Transaction] t
			INNER HASH JOIN #Additional a ON (a.TransactionID = t.TransactionID AND a.TransactionIDSource = t.TransactionIDSource 
												AND a.TransactionTypeID = t.TransactionTypeID AND a.TransactionStatusID = t.TransactionStatusID
												AND a.TransactionMethodID = t.TransactionMethodID AND a.TransactionDirection = t.TransactionDirection AND a.BalanceTypeID = t.BalanceTypeID AND a.LegNumber = t.LegNumber AND a.NumberOfLegs = t.NumberOfLegs
												AND a.MasterTransactionTimeStamp = t.MasterTransactionTimeStamp)
	WHERE	t.BatchKey = @BatchKey AND t.ExistsInDim = 0 AND t.DateTimeUpdated <> 1 AND t.MappingId <> -1
	OPTION (RECOMPILE)

	SET @RowsProcessed = @@ROWCOUNT;
	SET @RowsAffected = @RowsProcessed;
 
	EXEC [Control].Sp_Log @BatchKey, @ProcName, 'InsertStaging', 'Start', @RowsProcessed = @RowsProcessed;
		
	DROP TABLE #Additional

	INSERT	[Stage].[Transaction]	
			(	BatchKey, TransactionId, TransactionIdSource, BalanceTypeID, TransactionTypeID, TransactionStatusID, TransactionMethodID, TransactionDirection,
				BetGroupID, BetGroupIDSource, BetId, BetIdSource, LegId, LegIdSource, FreeBetID, ExternalID, External1, External2,
				UserID, UserIDSource, InstrumentID, InstrumentIDSource, LedgerID, LedgerSource, WalletId, PromotionId, PromotionIDSource,
				MasterTransactionTimestamp, MasterDayText, MasterTimeText, 
				Channel, ActionChannel, CampaignId, TransactedAmount,
				BetGroupType, BetTypeName, BetSubTypeName, LegBetTypeName, GroupingType, PriceTypeID, PriceTypeSource, ClassId, ClassIdSource, EventID, EventIDSource, MarketId, MarketIdSource, LegNumber, NumberOfLegs,
				ExistsInDim, Conditional, MappingType, MappingId, FromDate, DateTimeUpdated,RequestID,WithdrawID,InPlay,ClickToCall,CashoutType,IsDoubleDown,IsDoubledDown
			)
	SELECT	BatchKey, TransactionId, TransactionIdSource, BalanceTypeID, TransactionTypeID, TransactionStatusID, TransactionMethodID, TransactionDirection,
			BetGroupID, BetGroupIDSource, BetId, BetIdSource, LegId, LegIdSource, FreeBetID, ExternalID, External1, External2,
			UserID, UserIDSource, InstrumentID, InstrumentIDSource, LedgerID, LedgerSource, WalletId, PromotionId, PromotionIDSource,
			FromDate MasterTransactionTimestamp, CONVERT(VARCHAR(11),CONVERT(DATE,FromDate)) MasterDayText, CONVERT(VARCHAR(5),CONVERT(TIME,FromDate)) MasterTimeText,
			Channel,ActionChannel,CampaignId,TransactedAmount,
			BetGroupType, BetTypeName, BetSubTypeName, LegBetTypeName, GroupingType, PriceTypeID, PriceTypeSource, ClassId, ClassIdSource, EventID, EventIDSource, MarketId, MarketIdSource, LegNumber, NumberOfLegs,
			0 ExistsInDim, Conditional, MappingType, -5 MappingId, FromDate, DateTimeUpdated,RequestID,WithdrawID,InPlay,ClickToCall,CashoutType,IsDoubleDown,IsDoubledDown
	FROM	[Stage].[Transaction]
	WHERE	BatchKey = @BatchKey 
			AND ExistsInDim = 8
	OPTION (RECOMPILE)

	SET @RowsProcessed = @@ROWCOUNT;
	SET @RowsAffected = @RowsAffected + @RowsProcessed;

	EXEC [Control].Sp_Log @BatchKey,@ProcName, NULL, 'Success', @RowsProcessed = @RowsProcessed;

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, @ProcName, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

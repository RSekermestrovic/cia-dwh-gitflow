﻿CREATE PROC [Recursive].[Sp_Transaction_OtherAdj]
(
	@BatchKey	INT
)
AS 

/*
	Developer: Joseph George
	Date: Unknown
	Desc: OtherAdj

	Last Changed By: Edward Chen
	Last Changed Date: 6/JUL/2015
	Last Change Desc: Remove TransactionDetailID Dependency
*/

BEGIN
		DECLARE @ProcName VARCHAR(255) = (SELECT OBJECT_NAME(@@PROCID));

		SET ANSI_NULLS ON;
		SET QUOTED_IDENTIFIER ON;
		SET NOCOUNT ON;

		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
		
		BEGIN TRY
		
		DECLARE @RowsProcessed INT, @RowsAffected INT

		------------------------------- Force N/A values (should really be done in the Transaformations into Staging ------------------------------------------

		EXEC [Control].Sp_Log @BatchKey, @ProcName, 'N/A', 'Start';

		UPDATE A
		SET 	A.BetGroupType		= 'N/A',
				A.BetTypeName		= 'N/A',
				A.BetSubTypeName	= 'N/A',
				A.LegBetTypeName	= 'N/A',
				A.GroupingType		= 'N/A',
				A.ClassId			= 0
		FROM	[Stage].[Transaction] A
				--INNER HASH JOIN [ED_Dimensional].Dimension.TransactionDetail B	ON A.TransactionTypeID = B.TransactionTypeID AND A.TransactionStatusID = B.TransactionStatusID
				--						AND A.TransactionMethodID = B.TransactionMethodID AND A.TransactionDirection = B.TransactionDirection
		WHERE	A.TransactionTypeID NOT IN ('BT','BB','FE') AND A.TransactionTypeID NOT IN ('Unk','N/A','UK') AND (A.BetTypeName<>'N/A' OR A.LegBetTypeName<>'N/A')
		AND A.BatchKey = @BatchKey AND A.ExistsInDim = 0
		OPTION (RECOMPILE)
		
		SET @RowsProcessed = @@ROWCOUNT;
		SET @RowsAffected = @RowsProcessed;

		------------------------------- Resolve inconsistent Campaigns (should really be done in the Transaformations into Staging ------------------------------------------

		EXEC [Control].Sp_Log @BatchKey, @ProcName, 'Campaign', 'Start', @RowsProcessed = @RowsProcessed;

		UPDATE	y 
		SET		CampaignId = FirstCampaignId
		FROM	(	SELECT	x.BetID,x.BetIDSource, t.CampaignId, x.FirstCampaignId
					FROM	(	SELECT	BetID,BetIDSource, MIN(CampaignId) FirstCampaignId
								FROM	Stage.[Transaction] A
								WHERE	BatchKey = @BatchKey AND ExistsInDim = 0
								GROUP BY BetID,BetIDSource
								HAVING COUNT(DISTINCT BetTypeName)>1
							) x 
							INNER HASH JOIN [Stage].[Transaction] t ON t.BetId = x.BetId AND t.BetIdSource = x.BetIdSource
					WHERE	t.BatchKey = @BatchKey AND t.ExistsInDim = 0
				) y
		WHERE	CampaignId <> FirstCampaignId
		OPTION (RECOMPILE)

		SET @RowsProcessed = @@ROWCOUNT;
		SET @RowsAffected = @RowsAffected + @RowsProcessed;

		------------------------------- Resolve inconsistent BetTypeName (should really be done in the Transaformations into Staging ------------------------------------------

		EXEC [Control].Sp_Log @BatchKey, @ProcName, 'BetTypeName', 'Start', @RowsProcessed = @RowsProcessed;

		UPDATE	y 
		SET		BetTypeName = FirstBetTypeName
		FROM	(	SELECT	x.BetID,x.BetIDSource, t.BetTypeName, FIRST_VALUE(BetTypeName) OVER(PARTITION BY x.BetID, x.BetIDSource ORDER BY t.MasterTransactionTimestamp) FirstBetTypeName
					FROM	(	SELECT	BetID,BetIDSource
								FROM	Stage.[Transaction] A
								WHERE	BatchKey = @BatchKey AND ExistsInDim = 0
								GROUP BY BetID, BetIDSource
								HAVING COUNT(DISTINCT BetTypeName) > 1
							) x 
							INNER HASH JOIN [Stage].[Transaction] t ON t.BetId = x.BetId AND t.BetIdSource = x.BetIdSource
					WHERE	t.BatchKey = @BatchKey AND t.ExistsInDim = 0
				) y
		WHERE	BetTypeName <> FirstBetTypeName
		OPTION (RECOMPILE)

		SET @RowsProcessed = @@ROWCOUNT;
		SET @RowsAffected = @RowsAffected + @RowsProcessed;

		------------------------------- Resolve inconsistent ChannelName (should really be done in the Transaformations into Staging ------------------------------------------

		EXEC [Control].Sp_Log @BatchKey, @ProcName, 'ChannelName', 'Start', @RowsProcessed = @RowsProcessed;

		UPDATE	y 
		SET		Channel = FirstChannel
		FROM	(	SELECT	x.BetID,x.BetIDSource, t.Channel, FIRST_VALUE(Channel) OVER(PARTITION BY x.BetID, x.BetIDSource ORDER BY t.MasterTransactionTimestamp) FirstChannel
					FROM	(	SELECT	BetID,BetIDSource
								FROM	Stage.[Transaction] A
								WHERE	BatchKey = @BatchKey AND ExistsInDim = 0
								GROUP BY BetID, BetIDSource
								HAVING COUNT(DISTINCT Channel) > 1
							) x 
							INNER HASH JOIN [Stage].[Transaction] t ON t.BetId = x.BetId AND t.BetIdSource = x.BetIdSource
					WHERE	t.BatchKey = @BatchKey AND t.ExistsInDim = 0
				) y
		WHERE	Channel <> FirstChannel
		OPTION (RECOMPILE)

		SET @RowsProcessed = @@ROWCOUNT;
		SET @RowsAffected = @RowsAffected + @RowsProcessed;

		------------------------------------------------------------------------- LIVE EVENTS ----------------------------------------------------------------------------------

		--EXEC [Control].Sp_Log @BatchKey, @ProcName, 'Live Events', 'Start', @RowsProcessed = @RowsProcessed;

		--Select Distinct EventID
		--INTO #CurrentEvents 
		--From [Stage].[Transaction]
		--Where BatchKey = @BatchKey

		--Select 
		--	Distinct SourceEventId EventId
		--INTO #LiveEvents
		--From [$(Dimensional)].[Dimension].[Event]
		--where 
		--EventID IN
		--(
		--	Select EventID From #CurrentEvents
		--)
		--AND 
		--(
		--	(EventName like '%live bett%' or SourceParentEventName like '%live bett%' or SourceGrandParentEventName like '%live bett%' or SourceGreatGrandParentEventName like '%live bett%')
		--	or 
		--	(EventName like '%in-play%' or SourceParentEventName like '%in-play%' or SourceGrandParentEventName like '%in-play%' or SourceGreatGrandParentEventName like '%in-play%')
		--	or 
		--	(EventName like '%in play%' or SourceParentEventName like '%in play%' or SourceGrandParentEventName like '%in play%' or SourceGreatGrandParentEventName like '%in play%')
		--)
		--UNION
		--Select Distinct EventId 
		--From stage.[Event] 
		--Where BatchKey = @BatchKey
		--AND
		--EventID IN
		--(
		--	Select EventID From #CurrentEvents
		--)
		--AND 
		--(
		--	(SourceEventName like '%live bett%' or SourceParentEventName like '%live bett%' or SourceGrandParentEventName like '%live bett%' or SourceGreatGrandParentEventName like '%live bett%')
		--	or 
		--	(SourceEventName like '%in-play%' or SourceParentEventName like '%in-play%' or SourceGrandParentEventName like '%in-play%' or SourceGreatGrandParentEventName like '%in-play%')
		--	or 
		--	(SourceEventName like '%in play%' or SourceParentEventName like '%in play%' or SourceGrandParentEventName like '%in play%' or SourceGreatGrandParentEventName like '%in play%')
		--)

		--Update a
		--Set a.InPlay = 'Y'
		--From [Stage].[Transaction] a
		--INNER JOIN #LiveEvents b on a.EventID = b.EventId
		--Where BatchKey = @BatchKey

		----Update a
		----Set a.InPlay = 'N'
		----From [Stage].[Transaction] a
		----Where BatchKey = @BatchKey and InPlay IS NULL

		----Update a
		----Set a.ClickToCall = 'N'
		----From [Stage].[Transaction] a
		----Where BatchKey = @BatchKey and ClickToCall IS NULL

		------------------------------- Finish ------------------------------------------

		EXEC [Control].Sp_Log @BatchKey, @ProcName, NULL, 'Success', @RowsProcessed = @RowsProcessed;

	END TRY
	BEGIN CATCH

		DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

		EXEC [Control].Sp_Log @BatchKey, @ProcName, NULL, 'Failed', @ErrorMessage;
			
		THROW;

	END CATCH
END




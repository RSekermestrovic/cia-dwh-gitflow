﻿/*
	Developer: Aaron Jackson
	Date: 24/08/2015
	Desc: Search for dimensions that are missing from dimension table, but exist in fact table. Insert stub records into dimension table.
		
	Test: EXEC [Recursive].[Sp_Dimension_Account] @BatchKey = 305;
*/

CREATE PROC [Recursive].[Sp_Dimension_Account]
(
	@BatchKey	INT,
	@RowsProcessed	INT = 0 OUTPUT
)
AS 
BEGIN TRY

	DECLARE @ProcName VARCHAR(255) = (SELECT OBJECT_NAME(@@PROCID));

	DECLARE @RowCount int

	--SET STATISTICS IO ON;
	--SET STATISTICS TIME ON;

	SET ANSI_NULLS ON;
	SET QUOTED_IDENTIFIER ON;
	SET NOCOUNT ON;

	EXEC [Control].Sp_Log @BatchKey, @ProcName, 'Get Missing Keys','Start';
	
	-- Get list of natural keys from stage.transaction
	-- check natural keys exist in dimension

	-- Create new records based on -1 record
	SELECT	@BatchKey BatchKey, 
			LedgerId,
			LedgerSource,
			LedgerSequenceNumber,
			LegacyLedgerID,
			LedgerTypeID,
			LedgerType,
			LedgerOpenedDate,
			CONVERT(char(10),CONVERT(date,LedgerOpenedDate))LedgerOpenedDayText,
			LedgerOpenedDateOrigin,
			AccountID,
			AccountSource,
			PIN,
			Username,
			ExactTargetID,
			AccountFlags,
			c.ChannelId AccountOpenedChannelId,
			SourceBTag2,
			SourceTrafficSource,
			SourceRefURL,
			SourceCampaignID,
			SourceKeywords,
			StatementMethod,
			StatementFrequency,
			AccountTitle,
			AccountSurname,
			AccountFirstName,
			AccountMiddleName,
			AccountGender,
			AccountDOB,
			HasHomeAddress,
			HomeAddress1,
			HomeAddress2,
			HomeSuburb,
			HomeStateCode,
			HomeState,
			HomePostCode,
			HomeCountryCode,
			HomeCountry,
			HomePhone,
			HomeMobile,
			HomeFax,
			HomeEmail,
			HomeAlternateEmail,
			HasPostalAddress,
			PostalAddress1,
			PostalAddress2,
			PostalSuburb,
			PostalStateCode,
			PostalState,
			PostalPostCode,
			PostalCountryCode,
			PostalCountry,
			PostalPhone,
			PostalMobile,
			PostalFax,
			PostalEmail,
			PostalAlternateEmail,
			HasBusinessAddress,
			BusinessAddress1,
			BusinessAddress2,
			BusinessSuburb,
			BusinessStateCode,
			BusinessState,
			BusinessPostCode,
			BusinessCountryCode,
			BusinessCountry,
			BusinessPhone,
			BusinessMobile,
			BusinessFax,
			BusinessEmail,
			BusinessAlternateEmail,
			HasOtherAddress,
			OtherAddress1,
			OtherAddress2,
			OtherSuburb,
			OtherStateCode,
			OtherState,
			OtherPostCode,
			OtherCountryCode,
			OtherCountry,
			OtherPhone,
			OtherMobile,
			OtherFax,
			OtherEmail,
			OtherAlternateEmail,
			FirstDate,
			FromDate
    INTO	#DefaultUnknown
    FROM	[$(Dimensional)].[Dimension].[Account] a
			INNER JOIN [$(Dimensional)].[Dimension].[Channel] c ON c.ChannelKey = a.AccountOpenedChannelKey 
    WHERE AccountKey = -1;

	SELECT
		S.LedgerID,
		S.LedgerSource,
		MIN(S.FromDate) FromDate
	INTO #Dim
	FROM Stage.[Transaction] S
	LEFT OUTER JOIN [$(Dimensional)].Dimension.Account DAC ON S.LedgerID = DAC.LedgerID AND S.LedgerSource = DAC.LedgerSource
	WHERE S.ExistsInDim = 0
	AND S.LedgerId <> -1
	AND DAC.AccountKey IS NULL
	AND S.BatchKey = @BatchKey
	GROUP BY S.LedgerID, S.LedgerSource		
	OPTION(RECOMPILE);

	EXEC [Control].Sp_Log @BatchKey, @ProcName, 'Insert into Staging','Start', @RowsProcessed = @@ROWCOUNT;

	-- insert missing keys into staging.account table
	INSERT	Stage.Account
		(	BatchKey, 
			LedgerID, LedgerSource, LedgerSequenceNumber, LegacyLedgerID, LedgerTypeID, LedgerType, LedgerOpenedDate, LedgerOpenedDayText, LedgerOpenedDateOrigin,
			AccountID, AccountSource, PIN, UserName, ExactTargetID, AccountFlags,
			AccountOpenedChannelId, SourceBTag2, SourceTrafficSource, SourceRefURL, SourceCampaignId, SourceKeywords,
			StatementMethod, StatementFrequency,
			AccountTitle, AccountSurname, AccountFirstName, AccountMiddleName, AccountGender, AccountDOB,
			HasHomeAddress, HomeAddress1, HomeAddress2, HomeSuburb, HomeStateCode, HomeState, HomePostCode, HomeCountryCode, HomeCountry,
			HomePhone, HomeMobile, HomeFax, HomeEmail, HomeAlternateEmail, 
			HasPostalAddress, PostalAddress1, PostalAddress2, PostalSuburb, PostalStateCode, PostalState, PostalPostCode, PostalCountryCode, PostalCountry,
			PostalPhone, PostalMobile, PostalFax, PostalEmail, PostalAlternateEmail,
			HasBusinessAddress, BusinessAddress1, BusinessAddress2, BusinessSuburb, BusinessStateCode, BusinessState, BusinessPostCode, BusinessCountryCode, BusinessCountry,
			BusinessPhone, BusinessMobile, BusinessFax, BusinessEmail, BusinessAlternateEmail,
			HasOtherAddress, OtherAddress1, OtherAddress2, OtherSuburb, OtherStateCode, OtherState, OtherPostCode, OtherCountryCode, OtherCountry,
			OtherPhone, OtherMobile, OtherFax, OtherEmail, OtherAlternateEmail,
			FirstDate, FromDate
		)
	SELECT DISTINCT
		@BatchKey BatchKey, 
		U.LedgerId,
		U.LedgerSource,
		U.LedgerSequenceNumber,
		U.LegacyLedgerID,
		U.LedgerTypeID,
		U.LedgerType,
		U.LedgerOpenedDate,
		U.LedgerOpenedDayText,
		U.LedgerOpenedDateOrigin,
		U.AccountID,
		U.AccountSource,
		U.PIN,
		U.Username,
		U.ExactTargetID,
		U.AccountFlags,
		U.AccountOpenedChannelId,
		U.SourceBTag2,
		U.SourceTrafficSource,
		U.SourceRefURL,
		U.SourceCampaignID,
		U.SourceKeywords,
		U.StatementMethod,
		U.StatementFrequency,
		U.AccountTitle,
		U.AccountSurname,
		U.AccountFirstName,
		U.AccountMiddleName,
		U.AccountGender,
		U.AccountDOB,
		U.HasHomeAddress,
		U.HomeAddress1,
		U.HomeAddress2,
		U.HomeSuburb,
		U.HomeStateCode,
		U.HomeState,
		U.HomePostCode,
		U.HomeCountryCode,
		U.HomeCountry,
		U.HomePhone,
		U.HomeMobile,
		U.HomeFax,
		U.HomeEmail,
		U.HomeAlternateEmail,
		U.HasPostalAddress,
		U.PostalAddress1,
		U.PostalAddress2,
		U.PostalSuburb,
		U.PostalStateCode,
		U.PostalState,
		U.PostalPostCode,
		U.PostalCountryCode,
		U.PostalCountry,
		U.PostalPhone,
		U.PostalMobile,
		U.PostalFax,
		U.PostalEmail,
		U.PostalAlternateEmail,
		U.HasBusinessAddress,
		U.BusinessAddress1,
		U.BusinessAddress2,
		U.BusinessSuburb,
		U.BusinessStateCode,
		U.BusinessState,
		U.BusinessPostCode,
		U.BusinessCountryCode,
		U.BusinessCountry,
		U.BusinessPhone,
		U.BusinessMobile,
		U.BusinessFax,
		U.BusinessEmail,
		U.BusinessAlternateEmail,
		U.HasOtherAddress,
		U.OtherAddress1,
		U.OtherAddress2,
		U.OtherSuburb,
		U.OtherStateCode,
		U.OtherState,
		U.OtherPostCode,
		U.OtherCountryCode,
		U.OtherCountry,
		U.OtherPhone,
		U.OtherMobile,
		U.OtherFax,
		U.OtherEmail,
		U.OtherAlternateEmail,
		D.FromDate FirstDate,
		D.FromDate
	FROM #Dim as D
	LEFT JOIN Stage.Account as A on A.LedgerID = D.LedgerID AND A.LedgerSource = D.LedgerSource AND A.BatchKey = @BatchKey
	CROSS APPLY (SELECT * FROM #DefaultUnknown) as U
	WHERE A.LedgerID IS NULL
	OPTION(RECOMPILE);

	SET @RowCount  = @@ROWCOUNT;
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey, @ProcName, NULL,'Success', @RowsProcessed = @RowCount;
	
END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, @ProcName, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
﻿/*
	Test: EXEC [Recursive].[Sp_Dimension_Event] @BatchKey = 305;
*/

CREATE PROC [Recursive].[Sp_Dimension_Event]
(
	@BatchKey	INT,
	@RowsProcessed	INT = 0 OUTPUT
)
AS 
BEGIN TRY

	DECLARE @ProcName VARCHAR(255) = (SELECT OBJECT_NAME(@@PROCID));

	DECLARE @RowCount int

	--SET STATISTICS IO ON;
	--SET STATISTICS TIME ON;

	SET ANSI_NULLS ON;
	SET QUOTED_IDENTIFIER ON;
	SET NOCOUNT ON;

	EXEC [Control].Sp_Log @BatchKey, @ProcName, 'Get Missing Keys','Start';
	
	-- Create new records based on -1 record
	SELECT
		 @BatchKey as [BatchKey] 
		,[Source]
		,[SourceEventId]
		,[SourceEvent]
		,[SourceEventType]
		,[ParentEventId]
		,[ParentEvent]
		,[GrandparentEventId]
		,[GrandparentEvent]
		,[GreatGrandparentEventId]
		,[GreatGrandparentEvent]
		,[SourceEventDate]
		,[ResultedDateTime]
		,[EventAbandonedDateTime]
		,[SourceRaceNumber]
		,[SourceVenueId]
		,[SourceVenue]
		,[SourceVenueType]
		,[SourceVenueEventType]
		,[SourceVenueEventTypeName]		
		,[SourceVenueStateCode]
		,[SourceVenueState]
		,[SourceVenueCountryCode]
		,[SourceVenueCountry]
		,[ClassID]
		,'IBT' [ClassSource]
		,[SourceTrackCondition]
		,[SourceEventWeather]
		,[SourceSubSportType]
		,[SourceRaceGroup]
	    ,[SourceRaceClass]
	    ,[SourceHybridPricingTemplate]
	    ,[SourcePrizePool]
	    ,[SourceWeightType]
	    ,[SourceDistance]
	    ,[SourceEventAbandoned]
		,[SourceLiveStreamPerformId]
		,[FromDate]
    INTO #DefaultUnknown
    FROM [$(Dimensional)].[Dimension].[Event]
    WHERE EventKey = -1;

	-- Get list of natural keys from stage.transaction
	-- check natural keys exist in dimension

	SELECT DISTINCT
		S.EventID,
		S.[EventIDSource] Source,
		S.FromDate
	INTO #Dim
	FROM Stage.[Transaction] S
	LEFT OUTER JOIN [$(Dimensional)].Dimension.[Event] DE ON S.EventID = DE.EventID AND S.EventIDSource = DE.Source
	WHERE S.ExistsInDim = 0
	AND DE.EventKey IS NULL
	AND S.BatchKey = @BatchKey
	OPTION(RECOMPILE);

	EXEC [Control].Sp_Log @BatchKey, @ProcName, 'Insert into Staging','Start', @RowsProcessed = @@ROWCOUNT;

	-- Insert missing keys into Staging.Event table
	INSERT INTO Stage.Event ([BatchKey] 
		,[Source]
		,[SourceEventId]
		,[SourceEvent]
		,[SourceEventType]
		,[ParentEventId]
		,[ParentEvent]
		,[GrandparentEventId]
		,[GrandparentEvent]
		,[GreatGrandparentEventId]
		,[GreatGrandparentEvent]
		,[SourceEventDate]
		,[ResultedDateTime]
		,[EventAbandonedDateTime]
		,[SourceRaceNumber]
		,[SourceVenueId]
		,[SourceVenue]
		,[SourceVenueType]
		,[SourceVenueEventType]
		,[SourceVenueEventTypeName]		
		,[SourceVenueStateCode]
		,[SourceVenueState]
		,[SourceVenueCountryCode]
		,[SourceVenueCountry]
		,[ClassID]
		,[ClassSource]
		,[SourceTrackCondition]
		,[SourceEventWeather]
		,[SourceSubSportType]
		,[SourceRaceGroup]
	    ,[SourceRaceClass]
	    ,[SourceHybridPricingTemplate]
	    ,[SourcePrizePool]
	    ,[SourceWeightType]
	    ,[SourceDistance]
	    ,[SourceEventAbandoned]
		,[SourceLiveStreamPerformId]
		,[FromDate]) 
	SELECT DISTINCT
		@BatchKey as [BatchKey]
		,D.[Source]
		,D.[EventId]
		,U.[SourceEvent]
		,U.[SourceEventType]
		,U.[ParentEventId]
		,U.[ParentEvent]
		,U.[GrandparentEventId]
		,U.[GrandparentEvent]
		,U.[GreatGrandparentEventId]
		,U.[GreatGrandparentEvent]
		,U.[SourceEventDate]
		,U.[ResultedDateTime]
		,U.[EventAbandonedDateTime]
		,U.[SourceRaceNumber]
		,U.[SourceVenueId]
		,U.[SourceVenue]
		,U.[SourceVenueType]
		,U.[SourceVenueEventType]
		,U.[SourceVenueEventTypeName]		
		,U.[SourceVenueStateCode]
		,U.[SourceVenueState]
		,U.[SourceVenueCountryCode]
		,U.[SourceVenueCountry]
		,U.[ClassID]
		,U.[ClassSource]
		,U.[SourceTrackCondition]
		,U.[SourceEventWeather]
		,U.[SourceSubSportType]
		,U.[SourceRaceGroup]
	    ,U.[SourceRaceClass]
	    ,U.[SourceHybridPricingTemplate]
	    ,U.[SourcePrizePool]
	    ,U.[SourceWeightType]
	    ,U.[SourceDistance]
	    ,U.[SourceEventAbandoned]
		,U.[SourceLiveStreamPerformId]
		,U.[FromDate]
	FROM #Dim as D
	LEFT JOIN Stage.Event as S on S.SourceEventID = D.EventID AND S.Source = D.Source AND S.BatchKey = @BatchKey
	CROSS APPLY (SELECT * FROM #DefaultUnknown) as U
	WHERE S.SourceEventID IS NULL
	OPTION(RECOMPILE);

	SET @RowCount  = @@ROWCOUNT;
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey, @ProcName, NULL,'Success', @RowsProcessed = @RowCount;
	
END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, @ProcName, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
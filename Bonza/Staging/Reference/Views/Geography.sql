﻿CREATE VIEW [Reference].[Geography]
AS
SELECT C.CountryName, C.Country2Code,C.Country3Code,UPPER(S.StateCode) AS StateCode,UPPER(S.STATENAME) AS STATENAME,S.PhoneCode,P.GeographicLocation,UPPER(P.PostCode) AS PostCode,UPPER(SU.SUBURB) AS SUBURB
FROM Reference.Country C
INNER JOIN Reference.[State] S ON C.CountryNumericCode=S.CountryNumericCode
INNER JOIN Reference.PostCode P ON S.CountryNumericCode=P.CountryNumericCode AND S.StateCode=P.StateCode
INNER JOIN Reference.Suburb SU ON SU.CountryNumericCode=P.CountryNumericCode AND SU.StateCode=P.StateCode AND SU.PostCode=P.PostCode



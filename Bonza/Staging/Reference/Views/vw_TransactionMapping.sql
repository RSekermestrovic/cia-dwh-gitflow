﻿

CREATE VIEW [Reference].[vw_TransactionMapping]
AS
SELECT DISTINCT
		M.AccountTypeName,
		ISNULL(D_ATN.[Like],'**Other**')	AS D_ATN_Like,
		ISNULL(D_ATN.[Priority],1)			AS D_ATN_Priority,
		M.BetTypeID,
		ISNULL(D_BTI.[Like],'**Other**')	AS D_BTI_Like,
		ISNULL(D_BTI.[Priority],1)			AS D_BTI_Priority,
		M.BetStatusID,
		M.FreeBetID,
		M.BalanceEffect,
		M.BankAccountID,
		ISNULL(D_BAI.[Like],'**Other**')	AS D_BAI_Like,
		ISNULL(D_BAI.[Priority],1)			AS D_BAI_Priority,
		M.EventDescription,
		ISNULL(D_ED.[Like],'**Other**')		AS D_ED_Like,
		ISNULL(D_ED.[Priority],1)			AS D_ED_Priority,
		M.Comments,
		ISNULL(D_C.[Like],'**Other**')	AS D_C_Like,
		ISNULL(D_C.[Priority],1)			AS D_C_Priority,
		M.TransactionDetailId
FROM Reference.TransactionMapping M
	LEFT OUTER JOIN Reference.TransactionMappingDecoder D_ATN
			ON D_ATN.[Column]='AccountTypeName' AND ISNULL(M.AccountTypeName,'X')=ISNULL(D_ATN.[Return],'X')
	LEFT OUTER JOIN Reference.TransactionMappingDecoder D_BTI
			ON D_BTI.[Column]='BetTypeID' AND ISNULL(M.BetTypeID,'X')=ISNULL(D_BTI.[Return],'X')
	LEFT OUTER JOIN Reference.TransactionMappingDecoder D_BAI
			ON D_BAI.[Column]='BankAccountID' AND ISNULL(M.BankAccountID,'X')=ISNULL(D_BAI.[Return],'X')
	LEFT OUTER JOIN Reference.TransactionMappingDecoder D_ED
			ON D_ED.[Column]='EventDescription' AND ISNULL(M.EventDescription,'X')=ISNULL(D_ED.[Return],'X')
	LEFT OUTER JOIN Reference.TransactionMappingDecoder D_C
			ON D_C.[Column]='Comments' AND ISNULL(M.Comments,'X')=ISNULL(D_C.[Return],'X')



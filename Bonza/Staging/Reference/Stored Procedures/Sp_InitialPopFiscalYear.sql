﻿Create Procedure Reference.Sp_InitialPopFiscalYears

as

IF not exists (Select [Year] From [Reference].[FiscalYears])
Begin
begin transaction
INSERT [Reference].[FiscalYears] ([Year], [FiscalStart], [FiscalEnd]) VALUES (N'1989', CAST(N'1988-12-28' AS Date), CAST(N'1989-12-26' AS Date))
INSERT [Reference].[FiscalYears] ([Year], [FiscalStart], [FiscalEnd]) VALUES (N'1990', CAST(N'1989-12-27' AS Date), CAST(N'1991-01-01' AS Date))
INSERT [Reference].[FiscalYears] ([Year], [FiscalStart], [FiscalEnd]) VALUES (N'1991', CAST(N'1991-01-02' AS Date), CAST(N'1991-12-31' AS Date))
INSERT [Reference].[FiscalYears] ([Year], [FiscalStart], [FiscalEnd]) VALUES (N'1992', CAST(N'1992-01-01' AS Date), CAST(N'1992-12-29' AS Date))
INSERT [Reference].[FiscalYears] ([Year], [FiscalStart], [FiscalEnd]) VALUES (N'1993', CAST(N'1992-12-30' AS Date), CAST(N'1993-12-28' AS Date))
INSERT [Reference].[FiscalYears] ([Year], [FiscalStart], [FiscalEnd]) VALUES (N'1994', CAST(N'1993-12-29' AS Date), CAST(N'1994-12-27' AS Date))
INSERT [Reference].[FiscalYears] ([Year], [FiscalStart], [FiscalEnd]) VALUES (N'1995', CAST(N'1994-12-28' AS Date), CAST(N'1995-12-26' AS Date))
INSERT [Reference].[FiscalYears] ([Year], [FiscalStart], [FiscalEnd]) VALUES (N'1996', CAST(N'1995-12-27' AS Date), CAST(N'1996-12-31' AS Date))
INSERT [Reference].[FiscalYears] ([Year], [FiscalStart], [FiscalEnd]) VALUES (N'1997', CAST(N'1997-01-01' AS Date), CAST(N'1997-12-30' AS Date))
INSERT [Reference].[FiscalYears] ([Year], [FiscalStart], [FiscalEnd]) VALUES (N'1998', CAST(N'1997-12-31' AS Date), CAST(N'1998-12-29' AS Date))
INSERT [Reference].[FiscalYears] ([Year], [FiscalStart], [FiscalEnd]) VALUES (N'1999', CAST(N'1998-12-30' AS Date), CAST(N'1999-12-28' AS Date))
INSERT [Reference].[FiscalYears] ([Year], [FiscalStart], [FiscalEnd]) VALUES (N'2000', CAST(N'1999-12-29' AS Date), CAST(N'2000-12-26' AS Date))
INSERT [Reference].[FiscalYears] ([Year], [FiscalStart], [FiscalEnd]) VALUES (N'2001', CAST(N'2000-12-27' AS Date), CAST(N'2002-01-01' AS Date))
INSERT [Reference].[FiscalYears] ([Year], [FiscalStart], [FiscalEnd]) VALUES (N'2002', CAST(N'2002-01-02' AS Date), CAST(N'2002-12-31' AS Date))
INSERT [Reference].[FiscalYears] ([Year], [FiscalStart], [FiscalEnd]) VALUES (N'2003', CAST(N'2003-01-01' AS Date), CAST(N'2003-12-30' AS Date))
INSERT [Reference].[FiscalYears] ([Year], [FiscalStart], [FiscalEnd]) VALUES (N'2004', CAST(N'2003-12-31' AS Date), CAST(N'2004-12-28' AS Date))
INSERT [Reference].[FiscalYears] ([Year], [FiscalStart], [FiscalEnd]) VALUES (N'2005', CAST(N'2004-12-29' AS Date), CAST(N'2005-12-27' AS Date))
INSERT [Reference].[FiscalYears] ([Year], [FiscalStart], [FiscalEnd]) VALUES (N'2006', CAST(N'2005-12-28' AS Date), CAST(N'2006-12-26' AS Date))
INSERT [Reference].[FiscalYears] ([Year], [FiscalStart], [FiscalEnd]) VALUES (N'2007', CAST(N'2006-12-27' AS Date), CAST(N'2008-01-01' AS Date))
INSERT [Reference].[FiscalYears] ([Year], [FiscalStart], [FiscalEnd]) VALUES (N'2008', CAST(N'2008-01-02' AS Date), CAST(N'2008-12-30' AS Date))
INSERT [Reference].[FiscalYears] ([Year], [FiscalStart], [FiscalEnd]) VALUES (N'2009', CAST(N'2008-12-31' AS Date), CAST(N'2009-12-29' AS Date))
INSERT [Reference].[FiscalYears] ([Year], [FiscalStart], [FiscalEnd]) VALUES (N'2010', CAST(N'2009-12-30' AS Date), CAST(N'2010-12-28' AS Date))
INSERT [Reference].[FiscalYears] ([Year], [FiscalStart], [FiscalEnd]) VALUES (N'2011', CAST(N'2010-12-29' AS Date), CAST(N'2011-12-27' AS Date))
INSERT [Reference].[FiscalYears] ([Year], [FiscalStart], [FiscalEnd]) VALUES (N'2012', CAST(N'2011-12-28' AS Date), CAST(N'2013-01-01' AS Date))
INSERT [Reference].[FiscalYears] ([Year], [FiscalStart], [FiscalEnd]) VALUES (N'2013', CAST(N'2013-01-02' AS Date), CAST(N'2013-12-31' AS Date))
INSERT [Reference].[FiscalYears] ([Year], [FiscalStart], [FiscalEnd]) VALUES (N'2014', CAST(N'2014-01-01' AS Date), CAST(N'2014-12-30' AS Date))
INSERT [Reference].[FiscalYears] ([Year], [FiscalStart], [FiscalEnd]) VALUES (N'2015', CAST(N'2014-12-31' AS Date), CAST(N'2015-12-29' AS Date))
INSERT [Reference].[FiscalYears] ([Year], [FiscalStart], [FiscalEnd]) VALUES (N'2016', CAST(N'2015-12-30' AS Date), CAST(N'2016-12-27' AS Date))
INSERT [Reference].[FiscalYears] ([Year], [FiscalStart], [FiscalEnd]) VALUES (N'2017', CAST(N'2016-12-28' AS Date), CAST(N'2017-12-26' AS Date))
INSERT [Reference].[FiscalYears] ([Year], [FiscalStart], [FiscalEnd]) VALUES (N'2018', CAST(N'2017-12-27' AS Date), CAST(N'2019-01-01' AS Date))
INSERT [Reference].[FiscalYears] ([Year], [FiscalStart], [FiscalEnd]) VALUES (N'2019', CAST(N'2019-01-02' AS Date), CAST(N'2019-12-31' AS Date))
INSERT [Reference].[FiscalYears] ([Year], [FiscalStart], [FiscalEnd]) VALUES (N'2020', CAST(N'2020-01-01' AS Date), CAST(N'2020-12-29' AS Date))
INSERT [Reference].[FiscalYears] ([Year], [FiscalStart], [FiscalEnd]) VALUES (N'2021', CAST(N'2020-12-30' AS Date), CAST(N'2021-12-28' AS Date))
INSERT [Reference].[FiscalYears] ([Year], [FiscalStart], [FiscalEnd]) VALUES (N'2022', CAST(N'2021-12-29' AS Date), CAST(N'2022-12-27' AS Date))
INSERT [Reference].[FiscalYears] ([Year], [FiscalStart], [FiscalEnd]) VALUES (N'2023', CAST(N'2022-12-28' AS Date), CAST(N'2023-12-26' AS Date))
INSERT [Reference].[FiscalYears] ([Year], [FiscalStart], [FiscalEnd]) VALUES (N'2024', CAST(N'2023-12-27' AS Date), CAST(N'2024-12-31' AS Date))
INSERT [Reference].[FiscalYears] ([Year], [FiscalStart], [FiscalEnd]) VALUES (N'2025', CAST(N'2025-01-01' AS Date), CAST(N'2025-12-30' AS Date))
INSERT [Reference].[FiscalYears] ([Year], [FiscalStart], [FiscalEnd]) VALUES (N'2026', CAST(N'2025-12-31' AS Date), CAST(N'2026-12-29' AS Date))
INSERT [Reference].[FiscalYears] ([Year], [FiscalStart], [FiscalEnd]) VALUES (N'2027', CAST(N'2026-12-30' AS Date), CAST(N'2027-12-28' AS Date))
INSERT [Reference].[FiscalYears] ([Year], [FiscalStart], [FiscalEnd]) VALUES (N'2028', CAST(N'2027-12-29' AS Date), CAST(N'2028-12-26' AS Date))
INSERT [Reference].[FiscalYears] ([Year], [FiscalStart], [FiscalEnd]) VALUES (N'2029', CAST(N'2028-12-27' AS Date), CAST(N'2030-01-01' AS Date))
INSERT [Reference].[FiscalYears] ([Year], [FiscalStart], [FiscalEnd]) VALUES (N'2030', CAST(N'2030-01-02' AS Date), CAST(N'2030-12-31' AS Date))
INSERT [Reference].[FiscalYears] ([Year], [FiscalStart], [FiscalEnd]) VALUES (N'2031', CAST(N'2031-01-01' AS Date), CAST(N'2031-12-30' AS Date))
If @@ERROR = 0
Begin
	Commit transaction
End
Else
Begin
	Rollback transaction
End
End
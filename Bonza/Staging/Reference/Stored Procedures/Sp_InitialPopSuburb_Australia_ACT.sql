Create Procedure Reference.Sp_InitialPopSuburb_Australia_ACT

as

IF not exists (Select [CountryNumericCode] From [Reference].[Suburb] where [CountryNumericCode] = 36 and [StateCode] = 'ACT')
Begin
begin transaction


INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'200', N'AUSTRALIAN NATIONAL UNIVERSITY')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'221', N'BARTON')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2540', N'HMAS CRESWELL')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2540', N'JERVIS BAY')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2600', N'BARTON')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2600', N'CANBERRA')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2600', N'CAPITAL HILL')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2600', N'DEAKIN')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2600', N'DEAKIN WEST')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2600', N'DUNTROON')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2600', N'HARMAN')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2600', N'HMAS HARMAN')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2600', N'PARKES')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2600', N'PARLIAMENT HOUSE')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2600', N'RUSSELL')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2600', N'RUSSELL HILL')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2600', N'YARRALUMLA')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2601', N'ACTON')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2601', N'BLACK MOUNTAIN')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2601', N'CANBERRA')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2601', N'CITY')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2602', N'AINSLIE')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2602', N'DICKSON')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2602', N'DOWNER')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2602', N'HACKETT')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2602', N'LYNEHAM')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2602', N'O''CONNOR')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2602', N'WATSON')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2603', N'FORREST')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2603', N'GRIFFITH')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2603', N'MANUKA')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2603', N'RED HILL')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2604', N'CAUSEWAY')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2604', N'KINGSTON')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2604', N'NARRABUNDAH')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2605', N'CURTIN')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2605', N'GARRAN')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2605', N'HUGHES')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2606', N'CHIFLEY')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2606', N'LYONS')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2606', N'O''MALLEY')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2606', N'PHILLIP')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2606', N'PHILLIP DC')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2606', N'SWINGER HILL')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2606', N'WODEN')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2607', N'FARRER')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2607', N'ISAACS')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2607', N'MAWSON')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2607', N'PEARCE')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2607', N'TORRENS')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2608', N'CIVIC SQUARE')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2609', N'CANBERRA INTERNATIONAL AIRPORT')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2609', N'FYSHWICK')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2609', N'MAJURA')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2609', N'PIALLIGO')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2609', N'SYMONSTON')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2610', N'CANBERRA BC')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2610', N'CANBERRA MC')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2611', N'CHAPMAN')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2611', N'DUFFY')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2611', N'FISHER')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2611', N'HOLDER')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2611', N'MOUNT STROMLO')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2611', N'PIERCES CREEK')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2611', N'RIVETT')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2611', N'STIRLING')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2611', N'URIARRA')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2611', N'URIARRA FOREST')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2611', N'WARAMANGA')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2611', N'WESTON')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2611', N'WESTON CREEK')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2612', N'BRADDON')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2612', N'CAMPBELL')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2612', N'REID')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2612', N'TURNER')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2614', N'ARANDA')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2614', N'COOK')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2614', N'HAWKER')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2614', N'JAMISON CENTRE')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2614', N'MACQUARIE')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2614', N'PAGE')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2614', N'SCULLIN')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2614', N'WEETANGERA')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2615', N'CHARNWOOD')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2615', N'DUNLOP')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2615', N'FLOREY')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2615', N'FLYNN')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2615', N'FRASER')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2615', N'HIGGINS')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2615', N'HOLT')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2615', N'KIPPAX')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2615', N'LATHAM')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2615', N'MACGREGOR')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2615', N'MELBA')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2615', N'SPENCE')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2616', N'BELCONNEN')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2617', N'BELCONNEN')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2617', N'BELCONNEN DC')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2617', N'BRUCE')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2617', N'EVATT')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2617', N'GIRALANG')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2617', N'KALEEN')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2617', N'LAWSON')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2617', N'MCKELLAR')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2617', N'UNIVERSITY OF CANBERRA')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2618', N'HALL')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2620', N'BEARD')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2620', N'HUME')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2620', N'KOWEN FOREST')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2620', N'OAKS ESTATE')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2620', N'THARWA')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2620', N'TOP NAAS')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2620', N'WILLIAMSDALE')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2900', N'GREENWAY')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2900', N'TUGGERANONG')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2901', N'TUGGERANONG DC')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2902', N'KAMBAH')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2902', N'KAMBAH VILLAGE')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2903', N'ERINDALE CENTRE')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2903', N'OXLEY')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2903', N'WANNIASSA')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2904', N'FADDEN')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2904', N'GOWRIE')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2904', N'MACARTHUR')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2904', N'MONASH')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2905', N'BONYTHON')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2905', N'CALWELL')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2905', N'CHISHOLM')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2905', N'GILMORE')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2905', N'ISABELLA PLAINS')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2905', N'RICHARDSON')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2905', N'THEODORE')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2906', N'BANKS')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2906', N'CONDER')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2906', N'GORDON')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2911', N'CRACE')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2911', N'MITCHELL')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2912', N'GUNGAHLIN')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2913', N'CASEY')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2913', N'FRANKLIN')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2913', N'GINNINDERRA VILLAGE')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2913', N'KINLYSIDE')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2913', N'NGUNNAWAL')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2913', N'NICHOLLS')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2913', N'PALMERSTON')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2913', N'TAYLOR')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2914', N'AMAROO')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2914', N'BONNER')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2914', N'FORDE')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2914', N'HARRISON')
INSERT [Reference].[Suburb] ([CountryNumericCode], [StateCode], [PostCode], [Suburb]) VALUES (36, N'ACT', N'2914', N'MONCRIEFF')
If @@ERROR = 0	
Begin	
	Commit transaction
End	
Else	
Begin	
	Rollback transaction
End	
End	

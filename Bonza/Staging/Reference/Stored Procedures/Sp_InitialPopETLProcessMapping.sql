Create Procedure Reference.Sp_InitialPopETLProcessMapping

as

begin transaction

Truncate Table [Control].[ETLProcessMapping]


INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (101, N'ETLAtomic', N'Intrabet', N'sp_AtomicBets')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (102, N'ETLAtomic', N'Intrabet', N'sp_AtomicBalances')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (103, N'ETLAtomic', N'Intrabet', N'Sp_AtomicOpeningBalances')

INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (201, N'ETLStaging', N'Intrabet', N'sp_TransactionBets')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (202, N'ETLStaging', N'Intrabet', N'sp_FreeBetCredits')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (203, N'ETLStaging', N'Intrabet', N'sp_FreeBetExpiry')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (204, N'ETLStaging', N'Intrabet', N'sp_Deposits')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (205, N'ETLStaging', N'Intrabet', N'sp_Withdrawals')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (206, N'ETLStaging', N'Intrabet', N'sp_OtherTrans')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (207, N'ETLStaging', N'Intrabet', N'sp_ClientTransfers')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (208, N'ETLStaging', N'Intrabet', N'sp_InterOrgAccTransfer')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (209, N'ETLStaging', N'Intrabet', N'sp_Account')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (210, N'ETLStaging', N'Intrabet', N'sp_AccountStatus')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (211, N'ETLStaging', N'Intrabet', N'sp_Event')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (212, N'ETLStaging', N'Intrabet', N'sp_Promotion')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (213, N'ETLStaging', N'Intrabet', N'sp_Instrument')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (214, N'ETLStaging', N'Intrabet', N'sp_Note')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (215, N'ETLStaging', N'Intrabet', N'sp_User')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (216, N'ETLStaging', N'Intrabet', N'sp_Channel')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (217, N'ETLStaging', N'Intrabet', N'sp_Market')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (218, N'ETLStaging', N'Intrabet', N'Sp_BalanceStaging')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (219, N'ETLStaging', N'Intrabet', N'Sp_PriceType')

INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (301, N'ETLRecursive', N'Recursive', N'sp_Transaction_DeDup')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (302, N'ETLRecursive', N'Recursive', N'sp_Transaction_CleanDates')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (303, N'ETLRecursive', N'Recursive', N'sp_Transaction_Unfinalised')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (304, N'ETLRecursive', N'Recursive', N'sp_Transaction_UnbalancedWithdrawals')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (305, N'ETLRecursive', N'Recursive', N'sp_Transaction_Adjustments')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (306, N'ETLRecursive', N'Recursive', N'sp_Transaction_AddDedup')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (307, N'ETLRecursive', N'Recursive', N'Sp_Transaction_OtherAdj')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (308, N'ETLRecursive', N'Recursive', N'Recursive.Sp_BalanceFactCheck')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (309, N'ETLRecursive', N'Recursive', N'Sp_Rec_KPI')

INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (401, N'ETLDimensionRecursive', N'Recursive', N'Sp_Dimension_Account')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (402, N'ETLDimensionRecursive', N'Recursive', N'Sp_Dimension_Event')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (403, N'ETLDimensionRecursive', N'Recursive', N'Sp_Dimension_Instrument')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (404, N'ETLDimensionRecursive', N'Recursive', N'Sp_Dimension_Market')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (405, N'ETLDimensionRecursive', N'Recursive', N'Sp_Dimension_Note')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (406, N'ETLDimensionRecursive', N'Recursive', N'Sp_Dimension_PriceType')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (407, N'ETLDimensionRecursive', N'Recursive', N'Sp_Dimension_Promotion')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (408, N'ETLDimensionRecursive', N'Recursive', N'Sp_Dimension_User')

INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (501, N'ETLDimension', N'Dimensional', N'sp_DimDay')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (502, N'ETLDimension', N'Dimensional', N'sp_DimDayZone')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (503, N'ETLDimension', N'Dimensional', N'sp_DimPromotion')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (504, N'ETLDimension', N'Dimensional', N'sp_DimBetType')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (505, N'ETLDimension', N'Dimensional', N'sp_DimContract')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (506, N'ETLDimension', N'Dimensional', N'sp_DimAccount')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (507, N'ETLDimension', N'Dimensional', N'sp_DimAccountType2Adj')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (508, N'ETLDimension', N'Dimensional', N'sp_DimAccountStatus')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (509, N'ETLDimension', N'Dimensional', N'sp_DimEvent')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (510, N'ETLDimension', N'Dimensional', N'Sp_DimMarket')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (511, N'ETLDimension', N'Dimensional', N'Sp_DimInstrument')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (512, N'ETLDimension', N'Dimensional', N'Sp_DimNote')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (513, N'ETLDimension', N'Dimensional', N'Sp_DimUser')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (514, N'ETLDimension', N'Dimensional', N'Sp_DimPriceType')

INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (550, N'ETLDimension', N'Cleanse', N'Sp_AccountClean')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (551, N'ETLDimension', N'Cleanse', N'Sp_TestAccounts')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (552, N'ETLDimension', N'Cleanse', N'SP_Market_AmericanFootball')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (553, N'ETLDimension', N'Cleanse', N'SP_Market_AustralianRules')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (554, N'ETLDimension', N'Cleanse', N'SP_Market_Baseball')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (555, N'ETLDimension', N'Cleanse', N'SP_Market_BasketBall')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (556, N'ETLDimension', N'Cleanse', N'SP_Market_Cricket')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (557, N'ETLDimension', N'Cleanse', N'SP_Market_Golf')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (558, N'ETLDimension', N'Cleanse', N'SP_Market_RacingGhTrots')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (559, N'ETLDimension', N'Cleanse', N'SP_Market_RugbyLeague')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (560, N'ETLDimension', N'Cleanse', N'SP_Market_RugbyUnion')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (561, N'ETLDimension', N'Cleanse', N'SP_Market_Soccer')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (562, N'ETLDimension', N'Cleanse', N'SP_Market_Tennis')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (563, N'ETLDimension', N'Cleanse', N'SP_Market_CsSdEdGrDis')

INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (601, N'ETLTransaction', N'Dimensional', N'sp_TransactionFactLoad')

INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (701, N'ETLBalanceFact', N'Dimension', N'Sp_BalanceFactLoad')

INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (801, N'ETLPositionStaging', N'Intrabet', N'sp_PositionStaging')
INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (802, N'ETLPositionStaging', N'Recursive', N'Sp_Rec_PositionFactCheck')

INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (901, N'ETLPositionFact', N'Dimension', N'Sp_PositionFactLoad')

INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (1001, N'ETLKPIRecursive', N'Recursive', N'Sp_KPI')

INSERT [Control].[ETLProcessMapping] (SequenceID, [ProcessName], [SchemaName], [ProcedureName]) VALUES (1101, N'ETLKPI', N'Dimension', N'sp_KPIFact')

If @@ERROR = 0
Begin
	Commit transaction
End
Else
Begin
	Rollback transaction
End
GO


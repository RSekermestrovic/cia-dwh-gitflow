﻿Create Procedure Reference.[Sp_InitialPopSourceMapping]

as

Begin
begin transaction
INSERT [Reference].[SourceMapping] ([SystemName], [TableName], [ColumnName], [Source]) VALUES (N'Intrabet', N'tblAccounts', N'AccountID', N'IAA')
INSERT [Reference].[SourceMapping] ([SystemName], [TableName], [ColumnName], [Source]) VALUES (N'Intrabet', N'tblAccounts', N'LedgerID', N'IAL')
INSERT [Reference].[SourceMapping] ([SystemName], [TableName], [ColumnName], [Source]) VALUES (N'Intrabet', N'tblAccounts', N'ClientID', N'IAC')
INSERT [Reference].[SourceMapping] ([SystemName], [TableName], [ColumnName], [Source]) VALUES (N'Intrabet', N'tblUsers', N'UserID', N'IUU')
INSERT [Reference].[SourceMapping] ([SystemName], [TableName], [ColumnName], [Source]) VALUES (N'Intrabet', N'tblCompetitors', N'CompetitorID', N'ICC')
If @@ERROR = 0
Begin
	Commit transaction
End
Else
Begin
	Rollback transaction
End
End
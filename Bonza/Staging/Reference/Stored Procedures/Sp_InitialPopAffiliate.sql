Create Procedure Reference.Sp_InitialPopAffiliate

as

IF not exists (Select [AffiliateId] From [Reference].[Affiliate])
Begin
begin transaction
INSERT [Reference].[Affiliate] ([AffiliateId], [AffiliateMedia], [AffiliateName], [AffiliatePortfolio], [AffId], [Referrer]) VALUES (0, N'Organic', N'N/A', N'N/A', NULL, NULL)
INSERT [Reference].[Affiliate] ([AffiliateId], [AffiliateMedia], [AffiliateName], [AffiliatePortfolio], [AffId], [Referrer]) VALUES (0, N'Procured', N'Bet247', N'N/A', NULL, N'Bet247')
INSERT [Reference].[Affiliate] ([AffiliateId], [AffiliateMedia], [AffiliateName], [AffiliatePortfolio], [AffId], [Referrer]) VALUES (0, N'Procured', N'Frozen', N'N/A', NULL, N'TW')
INSERT [Reference].[Affiliate] ([AffiliateId], [AffiliateMedia], [AffiliateName], [AffiliatePortfolio], [AffId], [Referrer]) VALUES (0, N'Procured', N'Sports Alive', N'N/A', NULL, N'SportsAlive')
INSERT [Reference].[Affiliate] ([AffiliateId], [AffiliateMedia], [AffiliateName], [AffiliatePortfolio], [AffId], [Referrer]) VALUES (140010010, N'SEM', N'Google', N'Brand', NULL, N'140010010')
INSERT [Reference].[Affiliate] ([AffiliateId], [AffiliateMedia], [AffiliateName], [AffiliatePortfolio], [AffId], [Referrer]) VALUES (140010020, N'SEM', N'Google', N'Generic', NULL, N'140010020')
INSERT [Reference].[Affiliate] ([AffiliateId], [AffiliateMedia], [AffiliateName], [AffiliatePortfolio], [AffId], [Referrer]) VALUES (140010030, N'SEM', N'Google', N'Competitors ', NULL, N'140010030')
INSERT [Reference].[Affiliate] ([AffiliateId], [AffiliateMedia], [AffiliateName], [AffiliatePortfolio], [AffId], [Referrer]) VALUES (140010040, N'SEM', N'Google', N'Sports', NULL, N'140010040')
INSERT [Reference].[Affiliate] ([AffiliateId], [AffiliateMedia], [AffiliateName], [AffiliatePortfolio], [AffId], [Referrer]) VALUES (140010050, N'SEM', N'Google', N'AFL', NULL, N'140010050')
INSERT [Reference].[Affiliate] ([AffiliateId], [AffiliateMedia], [AffiliateName], [AffiliatePortfolio], [AffId], [Referrer]) VALUES (140010060, N'SEM', N'Google', N'NRL', NULL, N'140010060')
INSERT [Reference].[Affiliate] ([AffiliateId], [AffiliateMedia], [AffiliateName], [AffiliatePortfolio], [AffId], [Referrer]) VALUES (140010070, N'SEM', N'Google', N'Horse Racing', NULL, N'140010070')
INSERT [Reference].[Affiliate] ([AffiliateId], [AffiliateMedia], [AffiliateName], [AffiliatePortfolio], [AffId], [Referrer]) VALUES (140010080, N'SEM', N'Google', N'Harness/Greyhounds', NULL, N'140010080')
INSERT [Reference].[Affiliate] ([AffiliateId], [AffiliateMedia], [AffiliateName], [AffiliatePortfolio], [AffId], [Referrer]) VALUES (140010090, N'SEM', N'Google', N'Harness/Greyhounds', NULL, N'140010090')
INSERT [Reference].[Affiliate] ([AffiliateId], [AffiliateMedia], [AffiliateName], [AffiliatePortfolio], [AffId], [Referrer]) VALUES (340010010, N'Display', N'Xaxis', N'N/A', NULL, N'340010010')
INSERT [Reference].[Affiliate] ([AffiliateId], [AffiliateMedia], [AffiliateName], [AffiliatePortfolio], [AffId], [Referrer]) VALUES (340020010, N'Display', N'Microsoft', N'N/A', NULL, N'340020010')
INSERT [Reference].[Affiliate] ([AffiliateId], [AffiliateMedia], [AffiliateName], [AffiliatePortfolio], [AffId], [Referrer]) VALUES (540301000, N'Social', N'Facebook', N'N/A', NULL, N'540301000')
INSERT [Reference].[Affiliate] ([AffiliateId], [AffiliateMedia], [AffiliateName], [AffiliatePortfolio], [AffId], [Referrer]) VALUES (740010010, N'Direct Marketing', N'EDM', N'Empowered', NULL, N'740010010')
INSERT [Reference].[Affiliate] ([AffiliateId], [AffiliateMedia], [AffiliateName], [AffiliatePortfolio], [AffId], [Referrer]) VALUES (740020010, N'Direct Marketing', N'EDM', N'Adconion', NULL, N'740020010')
If @@ERROR = 0
Begin
	Commit transaction
End
Else
Begin
	Rollback transaction
End
End

﻿CREATE TABLE [Reference].[Country] (
    [CountryNumericCode] INT           NOT NULL,
    [Country2Code]       CHAR (2)      NOT NULL,
    [Country3Code]       CHAR (3)      NOT NULL,
    [CountryName]        VARCHAR (100) NOT NULL,
    [PhoneCode]          VARCHAR (3)   NOT NULL,
    CONSTRAINT [Country_PK] PRIMARY KEY CLUSTERED ([CountryNumericCode] ASC)
);
GO

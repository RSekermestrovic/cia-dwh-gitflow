﻿CREATE TABLE [Reference].[Mapping] (
    [MappingKey]   INT           IDENTITY (1, 1) NOT NULL,
    [FromDatabase] VARCHAR (MAX) NOT NULL,
    [FromSchema]   VARCHAR (MAX) NOT NULL,
    [FromTable]    VARCHAR (MAX) NOT NULL,
    [FromColumn]   VARCHAR (MAX) NOT NULL,
    [ToDatabase]   VARCHAR (MAX) NOT NULL,
    [ToSchema]     VARCHAR (MAX) NOT NULL,
    [ToTable]      VARCHAR (MAX) NOT NULL,
    [ToColumn]     VARCHAR (MAX) NOT NULL,
    [DefaultKey]   INT           NOT NULL,
    CONSTRAINT [Mapping_PK] PRIMARY KEY CLUSTERED ([MappingKey] ASC),
    CONSTRAINT [Default_FK_Mapping] FOREIGN KEY ([DefaultKey]) REFERENCES [Reference].[Default] ([DefaultKey])
);


GO
CREATE NONCLUSTERED INDEX [Mapping_FK_Default]
    ON [Reference].[Mapping]([DefaultKey] ASC);


GO

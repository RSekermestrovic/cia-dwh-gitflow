﻿CREATE TABLE [Reference].[Suburb] (
    [CountryNumericCode] INT           NOT NULL,
    [StateCode]          VARCHAR (3)   NOT NULL,
    [PostCode]           VARCHAR (10)  NOT NULL,
    [Suburb]             VARCHAR (100) NOT NULL,
    CONSTRAINT [Suburb_PK] PRIMARY KEY CLUSTERED ([CountryNumericCode] ASC, [StateCode] ASC, [PostCode] ASC, [Suburb] ASC),
    CONSTRAINT [Suburb_FK_PostCode] FOREIGN KEY ([CountryNumericCode], [StateCode], [PostCode]) REFERENCES [Reference].[PostCode] ([CountryNumericCode], [StateCode], [PostCode])
);
GO

﻿CREATE TABLE [Reference].[Default] (
    [DefaultKey] INT           IDENTITY (1, 1) NOT NULL,
    [ToValue]    VARCHAR (MAX) NOT NULL,
    [Comment]    VARCHAR (MAX) NULL,
    CONSTRAINT [Default_PK] PRIMARY KEY CLUSTERED ([DefaultKey] ASC)
);


GO

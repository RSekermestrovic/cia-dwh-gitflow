﻿CREATE TABLE [Reference].[Transformation] (
    [TransformationKey] INT           IDENTITY (1, 1) NOT NULL,
    [FromValue]         VARCHAR (MAX) NOT NULL,
    [ToValue]           VARCHAR (MAX) NOT NULL,
    [Comment]           VARCHAR (MAX) NULL,
    CONSTRAINT [Transformation_PK] PRIMARY KEY CLUSTERED ([TransformationKey] ASC)
);


GO

﻿CREATE TABLE [Reference].[LifeStages](
	[LifeStage] [varchar](50) NULL,
	[DaysSinceAccountOpenedLower] [int] NULL,
	[DaysSinceAccountOpenedUpper] [int] NULL,
	[DaysSinceLastBetLower] [int] NULL,
	[DaysSinceLastBetUpper] [int] NULL
) ON [PRIMARY]
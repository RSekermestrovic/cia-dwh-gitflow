﻿CREATE TABLE [Reference].[SourceMapping] (
    [SystemName] VARCHAR (30) NOT NULL,
    [TableName]  VARCHAR (30) NOT NULL,
    [ColumnName] VARCHAR (30) NULL,
    [Source]     CHAR (3)     NOT NULL
);


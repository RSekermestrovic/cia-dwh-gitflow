﻿CREATE TABLE [Reference].[Providernames_BSB](
	[BSB] [int] NULL,
	[BankCode1] [varchar](3) NULL,
	[BankCode2] [varchar](3) NULL,
	[BankName] [varchar](50) NULL,
	[ParentProvider] [varchar](50) NULL
) ON [PRIMARY]
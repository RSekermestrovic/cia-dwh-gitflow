﻿CREATE TABLE [Reference].[MappingTransformation] (
    [MappingKey]        INT NOT NULL,
    [TransformationKey] INT NOT NULL,
    CONSTRAINT [MappingTransformation_PK] PRIMARY KEY CLUSTERED ([MappingKey] ASC, [TransformationKey] ASC),
    CONSTRAINT [MappingTransformation_FK_Mapping] FOREIGN KEY ([MappingKey]) REFERENCES [Reference].[Mapping] ([MappingKey]),
    CONSTRAINT [MappingTransformation_FK_Transformation] FOREIGN KEY ([TransformationKey]) REFERENCES [Reference].[Transformation] ([TransformationKey])
);


GO
CREATE NONCLUSTERED INDEX [MappingTransformation_FK_Transformation]
    ON [Reference].[MappingTransformation]([TransformationKey] ASC);


GO

﻿CREATE TABLE [Reference].[Affiliate] (
    [AffiliateId]        INT          NOT NULL,
    [AffiliateMedia]     VARCHAR (32) NOT NULL,
    [AffiliateName]      VARCHAR (32) NOT NULL,
    [AffiliatePortfolio] VARCHAR (32) NOT NULL,
    [AffId]              INT          NULL,
    [Referrer]           VARCHAR (32) NULL,
    CONSTRAINT [Mapping_PK_1] PRIMARY KEY CLUSTERED ([AffiliateId] ASC, [AffiliateMedia] ASC, [AffiliateName] ASC, [AffiliatePortfolio] ASC)
);


GO

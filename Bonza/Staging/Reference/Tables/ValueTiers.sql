﻿CREATE TABLE [Reference].[ValueTiers] (
    [TierNumber]   INT          NULL,
    [TierName]     VARCHAR (30) NULL,
    [ValueFloor]   MONEY        NULL,
    [ValueCeiling] MONEY        NULL
) ON [PRIMARY];


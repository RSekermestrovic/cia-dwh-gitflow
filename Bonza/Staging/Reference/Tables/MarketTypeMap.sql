﻿CREATE TABLE [Reference].[MarketTypeMap] (
    [Sport]       VARCHAR (50)  NULL,
    [Keyword]     VARCHAR (200) NULL,
    [MarketType]  VARCHAR (200) NULL,
    [MarketGroup] VARCHAR (200) NULL,
    [LevyRate]    VARCHAR (32)  NULL
) ON [PRIMARY];


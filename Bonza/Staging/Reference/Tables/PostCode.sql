﻿CREATE TABLE [Reference].[PostCode] (
    [CountryNumericCode] INT               NOT NULL,
    [StateCode]          VARCHAR (3)       NOT NULL,
    [PostCode]           VARCHAR (10)      NOT NULL,
    [GeographicLocation] [sys].[geography] NULL,
    CONSTRAINT [PostCode_PK] PRIMARY KEY CLUSTERED ([CountryNumericCode] ASC, [StateCode] ASC, [PostCode] ASC),
    CONSTRAINT [PostCode_FK_State] FOREIGN KEY ([CountryNumericCode], [StateCode]) REFERENCES [Reference].[State] ([CountryNumericCode], [StateCode])
);

GO

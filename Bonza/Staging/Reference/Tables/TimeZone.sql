﻿CREATE TABLE [Reference].[TimeZone] (
    [FromDate]      DATETIME2 (3) NOT NULL,
    [ToDate]        DATETIME2 (3) NOT NULL,
    [TimeZone]      VARCHAR (32)  NOT NULL,
    [TimeZoneDesc]  VARCHAR (100) NULL,
    [OffsetMinutes] SMALLINT      NOT NULL
);


GO
CREATE UNIQUE CLUSTERED INDEX [CI-CompositeKey]
    ON [Reference].[TimeZone]([FromDate] ASC, [ToDate] ASC, [TimeZone] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [NCI-NaturalKeys]
    ON [Reference].[TimeZone]([FromDate] ASC, [ToDate] ASC, [TimeZone] ASC)
    INCLUDE([OffsetMinutes]);


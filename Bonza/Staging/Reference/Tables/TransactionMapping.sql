﻿CREATE TABLE [Reference].[TransactionMapping] (
    [MappingID]           INT          NULL,
    [AccountTypeName]     VARCHAR (20) NULL,
    [BetTypeID]           VARCHAR (20) NULL,
    [BetStatusID]         INT          NULL,
    [FreeBetID]           VARCHAR (20) NULL,
    [BalanceEffect]       CHAR (2)     NULL,
    [BankAccountID]       VARCHAR (20) NULL,
    [EventDescription]    VARCHAR (50) NULL,
    [Comments]            VARCHAR (50) NULL,
    [Outcome]             VARCHAR (50) NULL,
    [TransactionDetailId] VARCHAR (20) NULL,
    [-Balance]            VARCHAR (50) NULL,
    [+Balance]            VARCHAR (50) NULL,
    [Conditional]         VARCHAR (50) NULL,
    [MappingType]         INT          NULL,
    [ISACTIVE]            SMALLINT     NULL,
    [BetAmount]           SMALLINT     NULL,
    [Return_FreeBetID]    VARCHAR (10) NULL
);
GO
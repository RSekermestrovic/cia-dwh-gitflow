CREATE PROC [Dimension].[Sp_KPIFact]
(
	@BatchKey INT,
	@RowsProcessed	int = 0 OUTPUT
)
AS

	DECLARE @RowCount int

BEGIN TRY

	EXEC [Control].Sp_Log	@BatchKey,'Sp_KPIFact','Prepare','Start'

	Select 
		ContractKey,
		DayKey,
		CompanyKey,
		AccountKey,
		AccountStatusKey,
		BetTypeKey,
		LegTypeKey,
		ChannelKey,
		ActionChannelKey,
		CampaignKey,
		MarketKey,
		EventKey,
		ClassKey,
		UserKey,
		InstrumentKey,
		InPlayKey,
		MAX(AccountOpened)				AccountOpened,
		MAX(FirstDeposit)				FirstDeposit,
		MAX(FirstBet)					FirstBet,
		SUM(DepositsRequested)			DepositsRequested,
		SUM(DepositsCompleted)			DepositsCompleted,
		MAX(DepositsRequestedCount)		DepositsRequestedCount,
		MAX(DepositsCompletedCount)		DepositsCompletedCount,
		SUM(Promotions)					Promotions,
		MAX(BettorsPlaced)				BettorsPlaced,
		MAX(BonusBettorsPlaced)			BonusBettorsPlaced,
		MAX(PlayDaysPlaced)				PlayDaysPlaced,
		MAX(BonusPlayDaysPlaced)		BonusPlayDaysPlaced,
		MAX(PlayFiscalWeeksPlaced)		PlayFiscalWeeksPlaced,
		MAX(BonusPlayFiscalWeeksPlaced)		BonusPlayFiscalWeeksPlaced,
		MAX(PlayCalenderWeeksPlaced)	PlayCalenderWeeksPlaced,
		MAX(BonusPlayCalenderWeeksPlaced)	BonusPlayCalenderWeeksPlaced,
		MAX(PlayFiscalMonthsPlaced)		PlayFiscalMonthsPlaced,
		MAX(BonusPlayFiscalMonthsPlaced)	BonusPlayFiscalMonthsPlaced,
		MAX(PlayCalenderMonthsPlaced)	PlayCalenderMonthsPlaced,
		MAX(BonusPlayCalenderMonthsPlaced)	BonusPlayCalenderMonthsPlaced,
		MAX(BetsPlaced)					BetsPlaced,
		MAX(BonusBetsPlaced)			BonusBetsPlaced,
		MAX(ContractsPlaced)			ContractsPlaced,
		MAX(BonusContractsPlaced)		BonusContractsPlaced,
		SUM(Stakes)						Stakes,
		SUM(BonusStakes)				BonusStakes,
		SUM(Winnings)					Winnings,
		MAX(BettorsCashedOut)			BettorsCashedOut,
		MAX(BetsCashedOut)				BetsCashedOut,
		SUM(Cashout)					Cashout,
		SUM(CashoutDifferential)		CashoutDifferential,
		SUM(Fees)						Fees,
		SUM(WithdrawalsRequested)		WithdrawalsRequested,
		SUM(WithdrawalsCompleted)		WithdrawalsCompleted,
		MAX(WithdrawalsRequestedCount)	WithdrawalsRequestedCount,
		MAX(WithdrawalsCompletedCount)	WithdrawalsCompletedCount,
		SUM(Adjustments)				Adjustments,
		SUM(Transfers)					Transfers,
		MAX(BettorsResulted)			BettorsResulted,
		MAX(BonusBettorsResulted)		BonusBettorsResulted,
		MAX(PlayDaysResulted)			PlayDaysResulted,
		MAX(BonusPlayDaysResulted)		BonusPlayDaysResulted,
		MAX(PlayFiscalWeeksResulted)	PlayFiscalWeeksResulted,
		MAX(BonusPlayFiscalWeeksResulted)	BonusPlayFiscalWeeksResulted,
		MAX(PlayCalenderWeeksResulted)	PlayCalenderWeeksResulted,
		MAX(BonusPlayCalenderWeeksResulted)	BonusPlayCalenderWeeksResulted,
		MAX(PlayFiscalMonthsResulted)	PlayFiscalMonthsResulted,
		MAX(BonusPlayFiscalMonthsResulted)	BonusPlayFiscalMonthsResulted,
		MAX(PlayCalenderMonthsResulted) PlayCalenderMonthsResulted,
		MAX(BonusPlayCalenderMonthsResulted) BonusPlayCalenderMonthsResulted,
		MAX(ContractsResulted)			ContractsResulted,
		MAX(BonusContractsResulted)		BonusContractsResulted,
		MAX(BetsResulted)				BetsResulted,
		MAX(BonusBetsResulted)			BonusBetsResulted,
		SUM(Turnover)					Turnover,
		SUM(BonusTurnover)				BonusTurnover,
		SUM(GrossWin)					GrossWin,
		SUM(NetRevenue)					NetRevenue,
		SUM(NetRevenueAdjustment)		NetRevenueAdjustment,
		SUM(BonusWinnings)				BonusWinnings,
		SUM(Betback)					Betback,
		SUM(GrossWinAdjustment)			GrossWinAdjustment
	INTO #ALLKPI
	From
	(
		Select 
			IsNull(ContractKey,-1)			ContractKey,
			IsNull(DayKey,-1)				DayKey,
			IsNull(sa.CompanyKey,-1)			CompanyKey,
			IsNull(a.AccountKey,-1)			AccountKey,
			IsNull(Sa.AccountStatusKey,-1)	AccountStatusKey,
			IsNull(BetTypeKey,-1)			BetTypeKey,
			IsNull(LegTypeKey,-1)			LegTypeKey,
			IsNull(c.ChannelKey,-1)			ChannelKey,
			IsNull(ac.ChannelKey,-1)		ActionChannelKey,
			IsNull(CampaignKey,-1)			CampaignKey,
			IsNull(MarketKey,-1)			MarketKey,
			IsNull(EventKey,-1)				EventKey,
			IsNull(CL.ClassKey,-1)			ClassKey,
			IsNull(UserKey,-1)				UserKey,
			IsNull(InstrumentKey,-1)		InstrumentKey,
			ISNull(InPlayKey,-1)			InPlayKey,
			MAX(AccountOpened)				AccountOpened,
			MAX(FirstDeposit)				FirstDeposit,
			MAX(FirstBet)					FirstBet,
			SUM(DepositsRequested)			DepositsRequested,
			SUM(DepositsCompleted)			DepositsCompleted,
			MAX(DepositsRequestedCount)		DepositsRequestedCount,
			MAX(DepositsCompletedCount)		DepositsCompletedCount,
			SUM(Promotions)					Promotions,
			MAX(BettorsPlaced)				BettorsPlaced,
			MAX(BonusBettorsPlaced)			BonusBettorsPlaced,
			MAX(PlayDaysPlaced)				PlayDaysPlaced,
			MAX(BonusPlayDaysPlaced)		BonusPlayDaysPlaced,
			MAX(PlayFiscalWeeksPlaced)		PlayFiscalWeeksPlaced,
			MAX(BonusPlayFiscalWeeksPlaced)		BonusPlayFiscalWeeksPlaced,
			MAX(PlayCalenderWeeksPlaced)	PlayCalenderWeeksPlaced,
			MAX(BonusPlayCalenderWeeksPlaced)	BonusPlayCalenderWeeksPlaced,
			MAX(PlayFiscalMonthsPlaced)		PlayFiscalMonthsPlaced,
			MAX(BonusPlayFiscalMonthsPlaced)	BonusPlayFiscalMonthsPlaced,
			MAX(PlayCalenderMonthsPlaced)	PlayCalenderMonthsPlaced,
			MAX(BonusPlayCalenderMonthsPlaced)	BonusPlayCalenderMonthsPlaced,
			MAX(BetsPlaced)					BetsPlaced,
			MAX(BonusBetsPlaced)			BonusBetsPlaced,
			MAX(ContractsPlaced)			ContractsPlaced,
			MAX(BonusContractsPlaced)		BonusContractsPlaced,
			SUM(Stakes)						Stakes,
			SUM(BonusStakes)				BonusStakes,
			SUM(Winnings)					Winnings,
			MAX(BettorsCashedOut)			BettorsCashedOut,
			MAX(BetsCashedOut)				BetsCashedOut,
			SUM(Cashout)					Cashout,
			SUM(CashoutDifferential)		CashoutDifferential,
			SUM(Fees)						Fees,
			SUM(WithdrawalsRequested)		WithdrawalsRequested,
			SUM(WithdrawalsCompleted)		WithdrawalsCompleted,
			MAX(WithdrawalsRequestedCount)	WithdrawalsRequestedCount,
			MAX(WithdrawalsCompletedCount)	WithdrawalsCompletedCount,
			SUM(Adjustments)				Adjustments,
			SUM(Transfers)					Transfers,
			MAX(BettorsResulted)			BettorsResulted,
			MAX(BonusBettorsResulted)		BonusBettorsResulted,
			MAX(PlayDaysResulted)			PlayDaysResulted,
			MAX(BonusPlayDaysResulted)		BonusPlayDaysResulted,
			MAX(PlayFiscalWeeksResulted)	PlayFiscalWeeksResulted,
			MAX(BonusPlayFiscalWeeksResulted)	BonusPlayFiscalWeeksResulted,
			MAX(PlayCalenderWeeksResulted)	PlayCalenderWeeksResulted,
			MAX(BonusPlayCalenderWeeksResulted)	BonusPlayCalenderWeeksResulted,
			MAX(PlayFiscalMonthsResulted)	PlayFiscalMonthsResulted,
			MAX(BonusPlayFiscalMonthsResulted)	BonusPlayFiscalMonthsResulted,
			MAX(PlayCalenderMonthsResulted) PlayCalenderMonthsResulted,
			MAX(BonusPlayCalenderMonthsResulted) BonusPlayCalenderMonthsResulted,
			MAX(ContractsResulted)			ContractsResulted,
			MAX(BonusContractsResulted)		BonusContractsResulted,
			MAX(BetsResulted)				BetsResulted,
			MAX(BonusBetsResulted)			BonusBetsResulted,
			SUM(Turnover)					Turnover,
			SUM(BonusTurnover)				BonusTurnover,
			SUM(GrossWin)					GrossWin,
			SUM(NetRevenue)					NetRevenue,
			SUM(NetRevenueAdjustment)		NetRevenueAdjustment,
			SUM(BonusWinnings)				BonusWinnings,
			SUM(Betback)					Betback,
			SUM(GrossWinAdjustment)			GrossWinAdjustment
		From Stage.KPI t
		LEFT OUTER JOIN [$(Dimensional)].Dimension.[Contract] b	WITH (NOLOCK)	ON (b.LegID = t.LegID AND b.LegIdSource=t.LegIdSource)
		LEFT OUTER JOIN [$(Dimensional)].Dimension.Account a		WITH (NOLOCK)	ON (a.LedgerId = t.LedgerId AND a.LedgerSource=t.LedgerSource)
		LEFT OUTER JOIN [$(Dimensional)].Dimension.AccountStatus sa	WITH (NOLOCK)	ON (sa.LedgerId = t.LedgerId AND sa.LedgerSource=t.LedgerSource AND sa.FromDate <= t.MasterTransactionTimestamp AND sa.ToDate > t.MasterTransactionTimestamp)
		LEFT OUTER JOIN [$(Dimensional)].Dimension.DayZone y		WITH (NOLOCK)	ON t.MasterDayText = y.MasterDayText and t.AnalysisDayText = y.AnalysisDayText
		LEFT OUTER JOIN [$(Dimensional)].Dimension.BetType bt		WITH (NOLOCK)	ON (bt.BetType = t.BetTypeName AND bt.LegType=t.LegBetTypeName AND bt.BetSubType=t.BetSubTypeName AND bt.BetGroupType=t.BetGroupType AND bt.GroupingType=t.GroupingType)
		LEFT OUTER JOIN [$(Dimensional)].Dimension.Channel c		WITH (NOLOCK)	ON (c.ChannelId = t.Channel)
		LEFT OUTER JOIN [$(Dimensional)].Dimension.Channel ac		WITH (NOLOCK)	ON (ac.ChannelId = t.ActionChannel)
		LEFT OUTER JOIN [$(Dimensional)].Dimension.Campaign cp	WITH (NOLOCK)	ON (cp.CampaignId = t.CampaignId)
		LEFT OUTER JOIN [$(Dimensional)].Dimension.LegType l		WITH (NOLOCK)	ON (l.LegNumber=t.LegNumber AND l.NumberOfLegs=t.NumberOfLegs)
		LEFT OUTER JOIN [$(Dimensional)].Dimension.[Class] cl		WITH (NOLOCK)	ON (cl.ClassID=t.ClassID AND cl.Source=t.ClassIdSource AND t.MasterTransactionTimestamp >=cl.FROMDATE AND t.MasterTransactionTimestamp <cl.TODATE)
		LEFT OUTER JOIN [$(Dimensional)].Dimension.[User] u		WITH (NOLOCK)	ON (u.UserID=t.UserID and t.UserIDSource=u.source)
		LEFT OUTER JOIN [$(Dimensional)].Dimension.Market mk		WITH (NOLOCK)	ON (mk.MarketID=t.EventID AND mk.Source=t.EventIDSource)
		LEFT OUTER JOIN [$(Dimensional)].Dimension.[Event] e WITH (NOLOCK)	ON (t.EventID = e.EventId AND t.EventIDSource = e.Source)
		LEFT OUTER JOIN [$(Dimensional)].Dimension.Instrument i	WITH (NOLOCK)	ON (i.InstrumentID=t.InstrumentID AND i.Source=t.InstrumentIDSource)
		LEFT OUTER JOIN [$(Dimensional)].Dimension.InPlay	ip WITH (NOLOCK) ON (t.ClickToCall = ip.ClickToCall AND t.InPlay = ip.InPlay AND t.CashoutType = ip.CashoutType AND t.IsDoubleDown = ip.IsDoubleDown AND t.IsDoubledDown = ip.IsDoubledDown)
		WHERE t.BatchKey = @BatchKey
		GROUP BY 
			BatchKey,
			ContractKey,
			DayKey,
			sa.CompanyKey,
			a.AccountKey,
			AccountStatusKey,
			BetTypeKey,
			LegTypeKey,
			c.ChannelKey,
			ac.ChannelKey,
			CampaignKey,
			MarketKey,
			EventKey,
			CL.ClassKey,
			UserKey,
			InstrumentKey,
			InPlayKey
	) a
	Group By
		ContractKey,
		DayKey,
		CompanyKey,
		AccountKey,
		AccountStatuskey,
		BetTypeKey,
		LegTypeKey,
		ChannelKey,
		ActionChannelKey,
		CampaignKey,
		MarketKey,
		EventKey,
		ClassKey,
		UserKey,
		InstrumentKey,
		InPlayKey
	OPTION (RECOMPILE)

	SELECT T.ContractKey,T.DayKey,T.AccountKey,T.AccountStatusKey,T.BetTypeKey,T.LegTypeKey,T.ChannelKey,T.ActionChannelKey,T.CampaignKey,T.MarketKey,T.ClassKey,T.UserKey,T.InstrumentKey,T.AccountOpened,T.FirstDeposit,T.FirstBet,
				T.DepositsRequested,T.DepositsCompleted,T.DepositsRequestedCount,T.DepositsCompletedCount,T.Promotions,T.BetsPlaced,T.ContractsPlaced,T.BettorsPlaced,T.Stakes,T.Winnings,T.BettorsCashedOut,T.BetsCashedOut,T.Cashout,T.CashoutDifferential,T.Fees,T.WithdrawalsRequested,T.WithdrawalsCompleted,T.WithdrawalsRequestedCount,T.WithdrawalsCompletedCount,T.Adjustments,T.Transfers,T.BetsResulted,
				T.ContractsResulted,T.BettorsResulted,T.Turnover,T.GrossWin,T.NetRevenue,T.NetRevenueAdjustment,T.BonusWinnings,T.Betback,T.GrossWinAdjustment,T.PlayDaysPlaced,T.PlayDaysResulted,
				T.PlayFiscalWeeksPlaced,T.PlayCalenderWeeksPlaced,T.PlayFiscalMonthsPlaced,T.PlayCalenderMonthsPlaced,T.PlayFiscalWeeksResulted,T.PlayCalenderWeeksResulted,T.PlayFiscalMonthsResulted,T.PlayCalenderMonthsResulted,T.InPlayKey,T.EventKey,
				T.BonusBetsPlaced, T.BonusContractsPlaced, T.BonusBettorsPlaced, T.BonusPlayDaysPlaced, T.BonusPlayFiscalWeeksPlaced,
				T.BonusPlayCalenderWeeksPlaced,T.BonusPlayFiscalMonthsPlaced, T.BonusPlayCalenderMonthsPlaced, T.BonusStakes,T.BonusBetsResulted,
				T.BonusContractsResulted, T.BonusBettorsResulted,T.BonusPlayDaysResulted,T.BonusPlayFiscalWeeksResulted,T.BonusPlayCalenderWeeksResulted,
				T.BonusPlayFiscalMonthsResulted,T.BonusPlayCalenderMonthsResulted,T.BonusTurnover,
				CASE WHEN S.AccountKey IS NULL THEN 'I' ELSE 'U' END Upsert
	INTO #KPI
	FROM #ALLKPI T
	LEFT OUTER JOIN [$(Dimensional)].Fact.[KPI] S
		ON	S.ContractKey=T.ContractKey AND S.DayKey=T.DayKey AND S.AccountKey=T.AccountKey AND S.BetTypeKey=T.BetTypeKey  and S.AccountStatusKey=T.AccountStatusKey
		AND S.LegTypeKey=T.LegTypeKey AND S.MarketKey=T.MarketKey AND S.ClassKey=T.ClassKey AND S.ChannelKey = T.ChannelKey AND S.ActionChannelKey = T.ActionChannelKey
		AND S.CampaignKey = T.CampaignKey AND S.UserKey = T.UserKey AND S.InstrumentKey = T.InstrumentKey AND S.EventKey = T.EventKey
	OPTION (RECOMPILE)


	EXEC [Control].Sp_Log	@BatchKey,'Sp_KPIFact','Update','Start',@RowsProcessed = @@ROWCOUNT

	UPDATE S
		SET		S.AccountOpened			=	T.AccountOpened,
				S.FirstDeposit			=	T.FirstDeposit,
				S.FirstBet				=	T.FirstBet,
				S.DepositsRequested		=	CONVERT(MONEY,T.DepositsRequested) + S.DepositsRequested,
				S.DepositsCompleted		=	CONVERT(MONEY,T.DepositsCompleted) + S.DepositsCompleted,
				S.DepositsRequestedCount	=	T.DepositsRequestedCount,
				S.DepositsCompletedCount	=	T.DepositsCompletedCount,
				S.Promotions			=	CONVERT(MONEY,T.Promotions) + S.Promotions,
				S.BetsPlaced			=	T.BetsPlaced,
				S.ContractsPlaced		=	T.ContractsPlaced,
				S.BettorsPlaced			=	T.BettorsPlaced,
				S.PlayDaysPlaced		=	T.PlayDaysPlaced,
				S.PlayFiscalWeeksPlaced		=	T.PlayFiscalWeeksPlaced,
				S.PlayCalenderWeeksPlaced	=	T.PlayCalenderWeeksPlaced,
				S.PlayFiscalMonthsPlaced	=	T.PlayFiscalMonthsPlaced,
				S.PlayCalenderMonthsPlaced	=	T.PlayCalenderMonthsPlaced,
				S.Stakes				=	CONVERT(MONEY,T.Stakes) + S.Stakes,
				S.Winnings				=	CONVERT(MONEY,T.Winnings) + S.Winnings,
				S.BettorsCashedOut		=	T.BettorsCashedOut,
				S.BetsCashedOut			=	T.BetsCashedOut,
				S.Cashout				=	CONVERT(MONEY,T.Cashout) + S.Cashout,
				S.CashoutDifferential	=	CONVERT(MONEY,T.CashoutDifferential) + S.CashoutDifferential,
				S.Fees					=	CONVERT(MONEY,T.Fees) + S.Fees,
				S.WithdrawalsRequested	=	CONVERT(MONEY,T.WithdrawalsRequested) + S.WithdrawalsRequested,
				S.WithdrawalsCompleted	=	CONVERT(MONEY,T.WithdrawalsCompleted) + S.WithdrawalsCompleted,
				S.WithdrawalsRequestedCount	=	T.WithdrawalsRequestedCount,
				S.WithdrawalsCompletedCount =	T.WithdrawalsCompletedCount,
				S.Adjustments			=	CONVERT(MONEY,T.Adjustments) + S.Adjustments,
				S.Transfers				=	CONVERT(MONEY,T.Transfers) + S.Transfers,
				S.BetsResulted			=	T.BetsResulted,
				S.ContractsResulted		=	T.ContractsResulted,
				S.BettorsResulted		=	T.BettorsResulted,
				S.PlayDaysResulted		=	T.PlayDaysResulted,
				S.PlayFiscalWeeksResulted	=	T.PlayFiscalWeeksResulted,
				S.PlayCalenderWeeksResulted	=	T.PlayCalenderWeeksResulted,
				S.PlayFiscalMonthsResulted	=	T.PlayFiscalMonthsResulted,
				S.PlayCalenderMonthsResulted	=	T.PlayCalenderMonthsResulted,
				S.Turnover				=	CONVERT(MONEY,T.Turnover) + S.Turnover,
				S.GrossWin				=	CONVERT(MONEY,T.GrossWin) + S.GrossWin,
				S.NetRevenue			=	CONVERT(MONEY,T.NetRevenue) + S.NetRevenue,
				S.NetRevenueAdjustment	=	CONVERT(MONEY,T.NetRevenueAdjustment) + S.NetRevenueAdjustment,
				S.BonusWinnings			=	CONVERT(MONEY,T.BonusWinnings) + S.BonusWinnings,
				S.Betback				=	CONVERT(MONEY,T.Betback) + S.Betback,
				S.GrossWinAdjustment	=	CONVERT(MONEY,T.GrossWinAdjustment) + S.GrossWinAdjustment,
				S.ModifiedDate			=	GETDATE(),
				S.ModifiedBatchKey		=	@BatchKey,
				S.ModifiedBy			=	'Sp_KPIFact',
				S.BonusBetsPlaced		= T.BonusBetsPlaced,
				S.BonusContractsPlaced	= T.BonusContractsPlaced,
				S.BonusBettorsPlaced	= T.BonusBettorsPlaced,
				S.BonusPlayDaysPlaced	= T.BonusPlayDaysPlaced,
				S.BonusPlayFiscalWeeksPlaced	= T.BonusPlayFiscalWeeksPlaced,
				S.BonusPlayCalenderWeeksPlaced	= T.BonusPlayCalenderWeeksPlaced,
				S.BonusPlayFiscalMonthsPlaced	= T.BonusPlayFiscalMonthsPlaced,
				S.BonusPlayCalenderMonthsPlaced = T.BonusPlayCalenderMonthsPlaced,
				S.BonusStakes					= CONVERT(MONEY,T.BonusStakes) + S.BonusStakes,
				S.BonusBetsResulted				= T.BonusBetsResulted,
				S.BonusContractsResulted		= T.BonusContractsResulted,
				S.BonusBettorsResulted			= T.BonusBettorsResulted,
				S.BonusPlayDaysResulted			= T.BonusPlayDaysResulted,
				S.BonusPlayFiscalWeeksResulted	= T.BonusPlayFiscalWeeksResulted,
				S.BonusPlayCalenderWeeksResulted	= T.BonusPlayCalenderWeeksResulted,
				S.BonusPlayFiscalMonthsResulted		= T.BonusPlayFiscalMonthsResulted,
				S.BonusPlayCalenderMonthsResulted	= T.BonusPlayCalenderMonthsResulted,
				S.BonusTurnover						= CONVERT(MONEY,T.BonusTurnover) + S.BonusTurnover

	FROM [$(Dimensional)].Fact.[KPI] S
	INNER JOIN #KPI T
		ON	S.ContractKey=T.ContractKey AND S.DayKey=T.DayKey AND S.AccountKey=T.AccountKey AND S.BetTypeKey=T.BetTypeKey and S.AccountStatusKey=T.AccountStatusKey
			AND S.LegTypeKey=T.LegTypeKey AND S.MarketKey=T.MarketKey AND S.ClassKey=T.ClassKey AND S.ChannelKey = T.ChannelKey AND S.ActionChannelKey = T.ActionChannelKey
			AND S.CampaignKey = T.CampaignKey AND S.UserKey = T.UserKey AND S.InstrumentKey = T.InstrumentKey AND S.EventKey = T.EventKey AND S.InPlayKey = T.InPlayKey
	WHERE T.Upsert = 'U'
	OPTION (RECOMPILE)

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,'Sp_KPIFact','Insert','Start',@RowsProcessed = @RowCount

	INSERT INTO [$(Dimensional)].Fact.[KPI](ContractKey,DayKey,AccountKey,AccountStatusKey,BetTypeKey,LegTypeKey,ChannelKey,ActionChannelKey,CampaignKey,MarketKey,ClassKey,UserKey,InstrumentKey,AccountOpened,
											FirstDeposit,FirstBet,DepositsRequested,DepositsCompleted,Promotions,BetsPlaced,ContractsPlaced,BettorsPlaced,Stakes,Winnings,BettorsCashedOut,BetsCashedOut,Cashout,CashoutDifferential,Fees,WithdrawalsRequested,WithdrawalsCompleted,
											Adjustments,Transfers,BetsResulted,ContractsResulted,BettorsResulted,Turnover,GrossWin,NetRevenue,NetRevenueAdjustment,BonusWinnings,Betback,GrossWinAdjustment,CreatedDate,
											CreatedBatchKey,CreatedBy,ModifiedDate,ModifiedBatchKey,ModifiedBy,PlayDaysPlaced,PlayDaysResulted,
											PlayFiscalWeeksPlaced,PlayCalenderWeeksPlaced,PlayFiscalMonthsPlaced,PlayCalenderMonthsPlaced,PlayFiscalWeeksResulted,PlayCalenderWeeksResulted,PlayFiscalMonthsResulted,PlayCalenderMonthsResulted,InPlayKey,EventKey,DepositsRequestedCount,DepositsCompletedCount,WithdrawalsRequestedCount,WithdrawalsCompletedCount,
											BonusBetsPlaced, BonusContractsPlaced, BonusBettorsPlaced, BonusPlayDaysPlaced, BonusPlayFiscalWeeksPlaced,
											BonusPlayCalenderWeeksPlaced,BonusPlayFiscalMonthsPlaced, BonusPlayCalenderMonthsPlaced, BonusStakes,BonusBetsResulted,
											BonusContractsResulted, BonusBettorsResulted,BonusPlayDaysResulted,BonusPlayFiscalWeeksResulted,BonusPlayCalenderWeeksResulted,
											BonusPlayFiscalMonthsResulted,BonusPlayCalenderMonthsResulted,BonusTurnover)
	SELECT T.ContractKey,T.DayKey,T.AccountKey,T.AccountStatusKey,T.BetTypeKey,T.LegTypeKey,T.ChannelKey,T.ActionChannelKey,T.CampaignKey,T.MarketKey,T.ClassKey,T.UserKey,T.InstrumentKey,T.AccountOpened,T.FirstDeposit,T.FirstBet,
				T.DepositsRequested,T.DepositsCompleted,T.Promotions,T.BetsPlaced,T.ContractsPlaced,T.BettorsPlaced,T.Stakes,T.Winnings,T.BettorsCashedOut,T.BetsCashedOut,T.Cashout,T.CashoutDifferential, T.Fees,T.WithdrawalsRequested,T.WithdrawalsCompleted,T.Adjustments,T.Transfers,T.BetsResulted,
				T.ContractsResulted,T.BettorsResulted,T.Turnover,T.GrossWin,T.NetRevenue,T.NetRevenueAdjustment,T.BonusWinnings,T.Betback,T.GrossWinAdjustment,GETDATE(),@BatchKey,'Sp_KPIFact',GETDATE(),@BatchKey,'Sp_KPIFact',T.PlayDaysPlaced,T.PlayDaysResulted,
				T.PlayFiscalWeeksPlaced,T.PlayCalenderWeeksPlaced,T.PlayFiscalMonthsPlaced,T.PlayCalenderMonthsPlaced,T.PlayFiscalWeeksResulted,T.PlayCalenderWeeksResulted,T.PlayFiscalMonthsResulted,T.PlayCalenderMonthsResulted,T.InplayKey,T.EventKey,T.DepositsRequestedCount,T.DepositsCompletedCount,T.WithdrawalsRequestedCount,T.WithdrawalsCompletedCount,
				T.BonusBetsPlaced, T.BonusContractsPlaced, T.BonusBettorsPlaced, T.BonusPlayDaysPlaced, T.BonusPlayFiscalWeeksPlaced,
				T.BonusPlayCalenderWeeksPlaced,T.BonusPlayFiscalMonthsPlaced, T.BonusPlayCalenderMonthsPlaced, T.BonusStakes,T.BonusBetsResulted,
				T.BonusContractsResulted, T.BonusBettorsResulted,T.BonusPlayDaysResulted,T.BonusPlayFiscalWeeksResulted,T.BonusPlayCalenderWeeksResulted,
				T.BonusPlayFiscalMonthsResulted,T.BonusPlayCalenderMonthsResulted,T.BonusTurnover
	FROM #KPI T
	WHERE T.Upsert = 'I'

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,'Sp_KPIFact',NULL,'Success',@RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log	@BatchKey, 'Sp_KPIFact', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

GO



CREATE PROCEDURE [Dimension].[Sp_DimAccountStatus]
	@BatchKey int
WITH RECOMPILE
AS 
BEGIN

BEGIN TRY

SET NOCOUNT ON;

BEGIN TRANSACTION Prod_AccountStatus ;

		IF OBJECT_ID('TempDB..#temp','U') IS NOT NULL BEGIN DROP TABLE #temp END;
		IF OBJECT_ID('TempDB..#temp2','U') IS NOT NULL BEGIN DROP TABLE #temp2 END;


EXEC [Control].Sp_Log	@BatchKey,'Sp_DimAccountStatus','InsertIntoAccountStatus','Start'
INSERT INTO [$(Dimensional)].[Dimension].[AccountStatus] (AccountStatusKey, AccountKey, LedgerID,LedgerSource, LedgerEnabled, LedgerStatus, AccountType,InternetProfile,VIP,CompanyKey,CreditLimit,MinBetAmount,MaxBetAmount,
									Introducer,Handled,PhoneColourDesc,DisableDeposit,DisableWithdrawal,AccountClosedByUserID,AccountClosedByUserSource,AccountClosedByUserName,AccountClosedDate,ReIntroducedBy,
									ReIntroducedDate,AccountClosureReason,DepositLimit,LossLimit,LimitPeriod,LimitTimeStamp,ReceiveMarketingEmail,ReceiveCompetitionEmail,
									ReceiveSMS,ReceivePostalMail,ReceiveFax,CurrentVersion,FromDate,ToDate,FirstDate,CreatedDate,CreatedBatchKey,CreatedBy,ModifiedDate,ModifiedBatchKey,ModifiedBy)
SELECT
	CASE 
		WHEN Dim_AccountStatusKey.AccountStatusKey IS NULL 
		 THEN	(SELECT ISNULL(MAX(AccountStatusKey),0) FROM [$(Dimensional)].Dimension.[AccountStatus])+
					DENSE_RANK() OVER (PARTITION BY Dim_AccountStatusKey.AccountStatusKey ORDER BY A.LedgerID,A.LedgerSource,A.FromDate)
		ELSE Dim_AccountStatusKey.AccountStatusKey 
	END 																																AS AccountStatusKey,
	isnull(A1.AccountKey,-1)																											AS AccountKey,
	A.LedgerID																															AS LedgerID,
	A.LedgerSource																														AS LedgerSource,
	isnull(A.LedgerEnabled,'No')																										AS LedgerEnabled,
	isnull(A.LedgerStatus,'U')																											AS LedgerStatus,
	isnull(A.AccountType,'Client')																										AS AccountType,
	isnull(A.InternetProfile,'N/A')																										AS InternetProfile,
	isnull(A.VIP,'No')																													AS VIP,
	isnull(cm.CompanyKey,-1)																											AS CompanyKey,
	isnull(A.CreditLimit,0)																												AS CreditLimit,
	isnull(A.MinBetAmount,0)																											AS MinBetAmount,
	isnull(A.MaxBetAmount,0)																											AS MaxBetAmount,
	isnull(A.Introducer,'N/A')																											AS Introducer,
	isnull(A.Handled,'No')																												AS Handled,
	isnull(A.PhoneColourDesc,'N/A')																										AS PhoneColourDesc,
	isnull(A.DisableDeposit,'No')																										AS DisableDeposit,
	isnull(A.DisableWithdrawal,'No')																									AS DisableWithdrawal,
	isnull(A.AccountClosedByUserID,0)																									AS AccountClosedByUserID,
	isnull(A.AccountClosedByUserSource,'N/A')																							AS AccountClosedByUserSource,
	Case When isnull(A.AccountClosedByUserID,0)>0
		 Then isnull(Concat(U.Forename,' ',U.Surname),'N/A')	
		 Else Coalesce (U.Forename, 'N/A')
	End																																	AS AccountClosedByUserName,
	A.AccountClosedDate																													AS AccountClosedDate,
	isnull(A.ReIntroducedBy,0)																											AS ReIntroducedBy,
	A.ReIntroducedDate																													AS ReIntroducedDate,
	isnull(A.AccountClosureReason,'N/A')																								AS AccountClosureReason,
	isnull(A.DepositLimit,0)																											AS DepositLimit,
	isnull(A.LossLimit,0)																												AS LossLimit,
	isnull(A.LimitPeriod,0)																												AS LimitPeriod,
	A.LimitTimeStamp																													AS LimitTimeStamp,
	isnull(A.ReceiveMarketingEmail,'No')																								AS ReceiveMarketingEmail,
	isnull(A.ReceiveCompetitionEmail,'No')																								AS ReceiveCompetitionEmail,
	isnull(A.ReceiveSMS,'No')																											AS ReceiveSMS,
	isnull(A.ReceivePostalMail,'No')																									AS ReceivePostalMail,
	isnull(A.ReceiveFax,'No')																											AS ReceiveFax,
	''																																	AS CurrentVersion,
	A.FromDate																															AS FromDate,
	'9999-12-31 00:00:00.000' 																											AS ToDate,
	CONVERT(DATE,GETDATE())																												AS FirstDate,
	GETDATE()																															AS CreatedDate,
	@BatchKey																															AS CreatedBatchKey,
	'Sp_DimAccountStatus'																												AS CreatedBy,
	GETDATE()																															AS ModifiedDate,
	@BatchKey																															AS ModifiedBatchKey,
	'Sp_DimAccountStatus'																												AS ModifiedBy

FROM Stage.[AccountStatus] A with (nolock)
LEFT OUTER JOIN [$(Dimensional)].[Dimension].[AccountStatus] A2 with (nolock) on (A.LedgerID=A2.LedgerID and A.LedgerSource=A2.LedgerSource and A.FromDate=A2.FromDate)
LEFT OUTER JOIN [$(Dimensional)].[Dimension].[Account] A1 with (nolock) on (A.LedgerID=A1.LedgerID and A.LedgerSource=A1.LedgerSource)
LEFT OUTER JOIN (select distinct AccountStatusKey,LedgerID,LedgerSource,FromDate from [$(Dimensional)].[Dimension].[AccountStatus]) Dim_AccountStatusKey 
			on (A.LedgerID=Dim_AccountStatusKey.LedgerID and A.LedgerSource=Dim_AccountStatusKey.LedgerSource and A.FromDate=Dim_AccountStatusKey.FromDate)
LEFT OUTER JOIN [$(Dimensional)].[Dimension].[User] U with (nolock) on (A.AccountClosedByUserID=U.UserId and A.AccountClosedByUserSource=U.Source)
LEFT OUTER JOIN [$(Dimensional)].Dimension.Company cm on (A.BrandCode=cm.BrandCode and A.DivisionName=cm.DivisionName)
where A2.LedgerID is null and  A.BatchKey=@BatchKey;

--Load the temp table with records that need to be updated to indicate the current version of a Account
select DA.LedgerID,DA.LedgerSource,DA.FromDate,ROW_NUMBER() OVER(PARTITION BY DA.LedgerID, DA.LedgerSource ORDER BY DA.FromDate DESC) AS I_RNK ,
	   Case when DA.CurrentVersion='yes'  and (select distinct s.LedgerID from Stage.[AccountStatus] s where s.LedgerID=DA.LedgerID and s.LedgerSource=DA.LedgerSource and s.BatchKey=@batchkey) is not null
	   Then 'Yes' Else 'No' End Modified
into #temp
from [$(Dimensional)].[Dimension].[AccountStatus] DA with (nolock)
inner join  Stage.[AccountStatus] A with (nolock) on A.LedgerID=DA.LedgerID and A.LedgerSource=DA.LedgerSource and (A.BatchKey=@BatchKey or DA.CurrentVersion='yes')

select * ,ROW_NUMBER() OVER(PARTITION BY LedgerID, LedgerSource,FromDate ORDER BY I_RNK ) AS I_RNK2
into #temp2
from #temp

EXEC [Control].Sp_Log	@BatchKey,'Sp_DimAccountStatus','UpdateAccountStatus','Start'
--Update the records to indicate the Current version status and the related metadata
Update DA
SET DA.CurrentVersion=Case when A.I_RNK=1 THEN 'Yes'
						   Else 'No'
					   End,
	DA.ToDate=Case when A.I_RNK=1 THEN '9999-12-31 00:00:00.000'
						   Else (select t.fromDate from #temp t where t.LedgerID=A.ledgerID and A.LedgerSource=t.LedgerSource and t.I_RNK=A.I_RNK-1)
					   End,
	DA.ModifiedDate=Case when Modified='Yes'  Then GETDATE() Else DA.ModifiedDate End,
	DA.ModifiedBatchKey=Case when Modified='Yes'  Then @BatchKey Else DA.ModifiedBatchKey End
from [$(Dimensional)].[Dimension].[AccountStatus] DA with (nolock)
inner join  #temp2 A on A.LedgerID=DA.LedgerID and A.LedgerSource=DA.LedgerSource and DA.FromDate=A.FromDate  and I_RNK2=1

EXEC [Control].Sp_Log	@BatchKey,'Sp_DimAccountStatus','UpdateAccountStatus','Success'
COMMIT TRANSACTION Prod_AccountStatus;

END TRY
BEGIN CATCH

	Rollback TRANSACTION Prod_AccountStatus;
	declare @Error int = ERROR_NUMBER();
	declare @ErrorMessage varchar(max) = ERROR_MESSAGE();

    -- Log the failure at the object level
	EXEC [Control].Sp_Log @BatchKey, 'Sp_DimAccountStatus', NULL, 'Failed', @ErrorMessage;
	THROW;
END CATCH

END

﻿CREATE PROC [Dimension].[Sp_DimCommunication_Push]
(
	@BatchKey	int
)
WITH RECOMPILE
AS 

BEGIN TRY

EXEC [Control].Sp_Log @BatchKey,'Sp_DimCommunication_Push','InsertIntoDimension','Start'


Update b
		Set b.Title = a.Title,
			b.Details = a.Details,
			b.SubDetails = a.SubDetails,
			b.ModifiedDate = GetDate(),
			b.ModifiedBatchKey = @BatchKey
from [Stage].[Communication] a
INNER JOIN [$(Dimensional)].[Dimension].[Communication] b on a.CommunicationId = b.CommunicationId
Where BatchKey = @BatchKey and a.CommunicationSource='ETP'

INSERT INTO [$(Dimensional)].[Dimension].[Communication]
Select
		a.CommunicationId,
		a.CommunicationSource,
		a.Title,
		a.Details,
		a.SubDetails,
		GetDate() CreatedDate,
		GetDate() ModifiedDate,
		@BatchKey createdBatchKey,
		'Sp_DimCommunication' CreatedBy,
		@BatchKey ModifiedBatchKey,
		'Sp_DimCommunication' ModifiedBy 
from [Stage].[Communication] a
LEFT OUTER JOIN [$(Dimensional)].[Dimension].[Communication] b on a.CommunicationId = b.CommunicationId
Where BatchKey = @BatchKey and a.CommunicationSource='ETP' and b.CommunicationId is null

EXEC [Control].Sp_Log @BatchKey, 'Sp_DimCommunication_Push', NULL, 'Success',@RowsProcessed=@@RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_DimCommunication_Push', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

GO



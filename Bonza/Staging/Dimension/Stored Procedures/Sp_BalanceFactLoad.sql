﻿Create PROC [Dimension].[Sp_BalanceFactLoad]
(
	@BatchKey	int,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @RowCount int

BEGIN TRY

	EXEC [Control].Sp_Log	@BatchKey,'Sp_BalanceFactLoad','PreJoin Dimensions','Start'

	Select		   a.[BalanceTypeID]
				  ,a.[LedgerID]
				  ,a.[LedgerSource]
				  ,a.[WalletId]
				  ,a.[MasterDayText]
				  ,a.[AnalysisDayText]
				  ,a.[OpeningBalance]
				  ,a.[TransactedAmount]
				  ,a.[ClosingBalance]
				  ,a.[MasterTimeStamp]
				  ,ISNULL(DAK.AccountKey,-1) as AccountKey
				  ,ISNULL(DAKS.AccountStatusKey,-1) as AccountStatusKey
				  ,ISNULL(DZ.DayKey,-1) as DayKey
				  ,ISNULL(BT.BalanceTypeKey,-1) as BalanceTypeKey
				  ,ISNULL(W.WalletKey, -1) as WalletKey
	INTO #PreJoin
	From	Stage.balance a WITH(NOLOCK)
			LEFT OUTER JOIN [$(Dimensional)].Dimension.DayZone DZ WITH(NOLOCK) ON a.MasterDayText = DZ.MasterDayText and a.AnalysisDayText = DZ.AnalysisDayText
			LEFT OUTER JOIN [$(Dimensional)].Dimension.ACCOUNT DAK  WITH(NOLOCK) ON a.LedgerID=DAK.LedgerID and a.LedgerSource= DAK.LedgerSource 
			LEFT OUTER JOIN [$(Dimensional)].Dimension.ACCOUNTSTATUS DAKS  WITH(NOLOCK) ON a.LedgerID=DAKS.LedgerID and a.LedgerSource= DAKS.LedgerSource
							and a.FromDate >=DAKS.FROMDATE and a.FromDate <DAKS.TODATE
			LEFT OUTER JOIN [$(Dimensional)].Dimension.BalanceType BT WITH(NOLOCK) ON a.BalanceTypeID = BT.BalanceTypeID
			LEFT OUTER JOIN [$(Dimensional)].Dimension.Wallet W WITH(NOLOCK) ON a.WalletId = W.WalletId
	Where a.BatchKey = @BatchKey
	option (recompile)

	EXEC [Control].Sp_Log	@BatchKey,'Sp_BalanceFactLoad','Prejoin Fact','Start', @RowsProcessed = @@ROWCOUNT

	select distinct daykey 
	Into #DayKey
	from #PreJoin

	Select 	a.AccountKey,
			a.AccountStatusKey,
			a.LedgerID,
			a.LedgerSource,
			a.DayKey,
			a.BalanceTypeKey,
			a.WalletKey,
			a.OpeningBalance,
			a.TransactedAmount as TotalTransactedAmount,
			a.ClosingBalance,
			CASE 
				WHEN b.AccountKey IS NULL THEN 'I' 
				WHEN b.OpeningBalance <> a.OpeningBalance THEN 'U' 
				WHEN b.TotalTransactedAmount <> a.TransactedAmount THEN 'U' 
				WHEN b.ClosingBalance <> a.ClosingBalance THEN 'U' 
				ELSE 'X' 
			END Upsert  
	INTO	#Balance	
	From	#PreJoin a
			LEFT OUTER JOIN [$(Dimensional)].[Fact].[Balance] b on b.AccountKey = a.AccountKey 
																and b.BalanceTypeKey = a.BalanceTypeKey 
																and	b.DayKey = a.DayKey 
																and b.WalletKey = a.WalletKey 
																and b.DayKey IN (select daykey from #DayKey)
	OPTION (RECOMPILE)


	EXEC [Control].Sp_Log	@BatchKey,'Sp_BalanceFactLoad','Update','Start', @RowsProcessed = @@ROWCOUNT

	Update  b
	Set	b.ClosingBalance = a.ClosingBalance,
		b.OpeningBalance = a.OpeningBalance,
		b.TotalTransactedAmount = a.TotalTransactedAmount,
		b.ModifiedDate = GetDate(),
		b.ModifiedBatchKey = @BatchKey
	From #Balance a
	INNER JOIN [$(Dimensional)].[Fact].[Balance] b on b.AccountKey = a.AccountKey and b.BalanceTypeKey = a.BalanceTypeKey 
											and	 b.DayKey = a.DayKey and b.WalletKey = a.WalletKey
	Where b.DayKey IN (select daykey from #DayKey)
		AND a.Upsert = 'U'
	option (recompile)

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,'Sp_BalanceFactLoad','Insert','Start', @RowsProcessed = @RowCount

	INSERT INTO [$(Dimensional)].[Fact].[Balance] WITH(TABLOCK)
	Select 	a.AccountKey,
			a.AccountStatusKey,
			a.LedgerID,
			a.LedgerSource,
			a.DayKey,
			a.BalanceTypeKey,
			a.WalletKey,
			a.OpeningBalance,
			a.TotalTransactedAmount,
			a.ClosingBalance,
			GetDate() as CreatedDate,
			@BatchKey as CreatedBatchKey,
			GetDate() as ModifiedDate,
			@BatchKey as ModifiedBatchKey 
	From	#Balance a
	Where	a.Upsert = 'I'
	option (recompile)

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,'Sp_BalanceFactLoad',NULL,'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH
	declare @ErrorMessage nvarchar(max), @ErrorSeverity int, @ErrorState int;
	select @ErrorMessage = ERROR_MESSAGE() + ' Line ' + cast(ERROR_LINE() as nvarchar(5)), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();
	EXEC [Control].Sp_Log	@BatchKey,'Sp_BalanceFactLoad',NULL,'Failed',@ErrorMessage
	raiserror (@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH

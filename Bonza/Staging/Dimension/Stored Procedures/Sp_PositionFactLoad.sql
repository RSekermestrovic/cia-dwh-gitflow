﻿CREATE PROC [Dimension].[Sp_PositionFactLoad]
(
	@BatchKey	int,
	@RowsProcessed	int = 0 OUTPUT
)
AS

	DECLARE @RowCount INT;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

BEGIN TRY

	EXEC [Control].Sp_Log	@BatchKey,'Sp_PositionFactLoad','Main Join','Start', @RowsProcessed = @@ROWCOUNT

	SELECT	a.LedgerID,
			a.LedgerSource,
			a.MasterDayText,
			a.AnalysisDayText,
			ISNULL(DAK.AccountKey,-1) AccountKey,
			ISNULL(DZ.DayKey,-1) DayKey,
			ISNULL(DAKS.AccountStatusKey,-1) AccountStatusKey,
			ISNULL(BT.BalanceTypeKey,-1) BalanceTypeKey,
			ISNULL(AC.ActivityKey,0) ActivityKey,
			IsNull(VTC.TierKey,-1) TierKey,
			IsNull(VTF.TierKey,-1) TierKeyFiscal,
			IsNull(RLC.RevenueLifeCycleKey,-1) RevenueLifeCycleKey,
			IsNull(LS.LifeStageKey,-1) LifeStageKey,
			IsNull(PLS.LifeStageKey,-1) PreviousLifeStageKey
	INTO	#Join
	FROM	Stage.Position a
			LEFT OUTER JOIN [$(Dimensional)].Dimension.DayZone DZ ON a.MasterDayText = DZ.MasterDayText AND a.AnalysisDayText = DZ.AnalysisDayText
			LEFT OUTER JOIN [$(Dimensional)].Dimension.ACCOUNT DAK ON a.LedgerId=DAK.LedgerId AND a.LedgerSource= DAK.LedgerSource 
			LEFT OUTER JOIN [$(Dimensional)].Dimension.AccountStatus DAKS ON a.LedgerId=DAKS.LedgerId AND a.LedgerSource= DAKS.LedgerSource AND a.FromDate >=DAKS.FROMDATE AND a.FromDate <DAKS.TODATE
			LEFT OUTER JOIN [$(Dimensional)].Dimension.BalanceType BT ON a.BalanceTypeID = BT.BalanceTypeID
			LEFT OUTER JOIN [$(Dimensional)].Dimension.Activity AC	ON (a.HasDeposited=AC.HasDeposited AND a.HasWithdrawn=AC.HasWithdrawn AND a.HasBet=AC.HasBet AND a.HasWon=AC.HasWon AND a.HasLost=AC.HasLost)
			LEFT OUTER JOIN [$(Dimensional)].Dimension.ValueTiers VTC ON a.TierNumber = VTC.TierNumber
			LEFT OUTER JOIN [$(Dimensional)].Dimension.ValueTiers VTF ON a.TierNumberFiscal = VTF.TierNumber
			LEFT OUTER JOIN [$(Dimensional)].Dimension.RevenueLifeCycle RLC ON a.RevenueLifeCycle = RLC.RevenueLifeCycle
			LEFT OUTER JOIN [$(Dimensional)].dimension.LifeStage LS ON a.LifeStage = LS.LifeStage
			LEFT OUTER JOIN [$(Dimensional)].dimension.LifeStage PLS ON a.PreviousLifeStage = PLS.LifeStage
	WHERE	BatchKey = @BatchKey
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,'Sp_PositionFactLoad','Firsts Join','Start', @RowsProcessed = @@ROWCOUNT

	SELECT	a.LedgerID,
			a.LedgerSource,
			a.MasterDayText,
			a.AnalysisDayText,
			ISNULL(FDDK.DayKey,0) FirstDepositDayKey,
			ISNULL(FBDK.DayKey,0) FirstBetDayKey,
			ISNULL(FBT.BetTypeKey,0) FirstBetTypeKey,
			ISNULL(FC.ClassKey,0) FirstClassKey,
			ISNULL(FCH.ChannelKey,0) FirstChannelKey
	INTO	#First
	FROM	Stage.Position a
			LEFT OUTER JOIN [$(Dimensional)].Dimension.BetType FBT	ON (a.FirstBetGroupType=FBT.BetGroupType AND a.FirstBetType=FBT.BetType AND a.FirstBetSubType=FBT.BetSubType AND a.FirstGroupingType=FBT.GroupingType AND a.FirstLegType=FBT.LegType)
			LEFT OUTER JOIN [$(Dimensional)].Dimension.Class FC ON (a.FirstClassID=FC.ClassID AND FC.Source='IBT' AND a.FirstBetDate >=FC.FROMDATE AND a.FirstBetDate <FC.TODATE)
			LEFT OUTER JOIN [$(Dimensional)].Dimension.Channel FCH	ON (a.FirstChannel=FCH.ChannelId)
			LEFT JOIN [$(Dimensional)].Dimension.DayZone FDDK	ON CONVERT(char(10),CONVERT(date,a.FirstCreditDate)) = FDDK.MasterDayText AND a.FirstCreditDate >= FDDK.FromDate AND a.FirstCreditDate < FDDK.ToDate
			LEFT JOIN [$(Dimensional)].Dimension.DayZone FBDK	ON CONVERT(char(10),CONVERT(date,a.FirstBetDate)) = FBDK.MasterDayText  AND a.FirstBetDate >= FBDK.FromDate AND a.FirstBetDate < FBDK.ToDate
	WHERE	BatchKey = @BatchKey
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,'Sp_PositionFactLoad','Lasts Join','Start', @RowsProcessed = @@ROWCOUNT

	SELECT	a.LedgerID,
			a.LedgerSource,
			a.MasterDayText,
			a.AnalysisDayText,
			ISNULL(LDDK.DayKey,0) LastDepositDayKey,
			ISNULL(LBDK.DayKey,0) LastBetDayKey,
			ISNULL(LBT.BetTypeKey,0) LastBetTypeKey,
			ISNULL(LC.ClassKey,0) LastClasskey,
			ISNULL(LCH.ChannelKey,0) LastChannelKey
	INTO	#Last
	FROM	Stage.Position a
			LEFT OUTER JOIN [$(Dimensional)].Dimension.BetType LBT	ON (a.LastBetGroupType=LBT.BetGroupType AND a.LastBetType=LBT.BetType AND a.LastBetSubType=LBT.BetSubType AND a.LastGroupingType=LBT.GroupingType AND a.LastLegType=LBT.LegType)
			LEFT OUTER JOIN [$(Dimensional)].Dimension.Class LC ON (a.LastClassID=LC.ClassID AND LC.Source='IBT' AND a.LastBetDate >=LC.FROMDATE AND a.LastBetDate <LC.TODATE)
			LEFT OUTER JOIN [$(Dimensional)].Dimension.Channel LCH	ON (a.LastChannel=LCH.ChannelId)
			LEFT JOIN [$(Dimensional)].Dimension.DayZone LDDK	ON CONVERT(char(10),CONVERT(date,a.LastCreditDate)) = LDDK.MasterDayText AND a.LastCreditDate >= LDDK.FromDate AND a.LastCreditDate < LDDK.ToDate
			LEFT JOIN [$(Dimensional)].Dimension.DayZone LBDK	ON CONVERT(char(10),CONVERT(date,a.LastBetDate)) = LBDK.MasterDayText AND a.LastBetDate >= LBDK.FromDate AND a.LastBetDate < LBDK.ToDate
	WHERE	BatchKey = @BatchKey
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,'Sp_PositionFactLoad','Dimensional Lookup','Start', @RowsProcessed = @@ROWCOUNT

	SELECT DISTINCT daykey INTO #Days FROM #Join

	SELECT AccountKey, DayKey INTO #Dim FROM [$(Dimensional)].Fact.Position WHERE DayKey in (SELECT DayKey FROM #Days)

	EXEC [Control].Sp_Log	@BatchKey,'Sp_PositionFactLoad','Combine','Start', @RowsProcessed = @@ROWCOUNT

	SELECT	a.LedgerID,
			a.LedgerSource,
			a.MasterDayText,
			a.AnalysisDayText,
			a.OpeningBalance,
			a.TransactedAmount,
			a.ClosingBalance,
			CASE WHEN a.DaysSinceAccountOpened = -1 THEN null ELSE a.DaysSinceAccountOpened END DaysSinceAccountOpened,
			CASE WHEN a.DaysSinceFirstDeposit = -1 THEN null ELSE a.DaysSinceFirstDeposit END DaysSinceFirstDeposit,
			CASE WHEN a.DaysSinceFirstBet = -1 THEN null ELSE a.DaysSinceFirstBet END DaysSinceFirstBet,
			CASE WHEN a.DaysSinceLastDeposit = -1 THEN null ELSE a.DaysSinceLastDeposit END DaysSinceLastDeposit,
			CASE WHEN a.DaysSinceLastBet = -1 THEN null ELSE a.DaysSinceLastBet END DaysSinceLastBet,
			a.LifetimeTurnover,
			a.LifetimeGrossWin,
			a.LifetimeBetCount,
			a.LifetimeBonus,
			a.LifetimeDeposits,
			a.LifetimeWithdrawals,
			a.TurnoverCalenderMonthToDate TurnoverMTDCalender,
			a.TurnoverFiscalMonthToDate TurnoverMTDFiscal,
			b.BalanceTypeKey,
			b.AccountKey,
			b.AccountStatusKey,
			b.DayKey,
			b.ActivityKey,
			c.FirstDepositDayKey,
			c.FirstBetDayKey,
			c.FirstBetTypeKey,
			c.FirstClassKey,
			c.FirstChannelKey,
			d.LastDepositDayKey,
			d.LastBetDayKey,
			d.LastBetTypeKey,
			d.LastClasskey,
			d.LastChannelKey,
			b.TierKey,
			b.TierKeyFiscal,
			b.RevenueLifeCycleKey,
			b.LifeStageKey,
			b.PreviousLifeStageKey,
			CASE WHEN x.AccountKey IS NULL THEN 'I' ELSE 'U' END Upsert
	INTO	#Position
	FROM	Stage.Position a
			INNER JOIN #Join b ON (b.LedgerID = a.LedgerID AND b.LedgerSource = a.LedgerSource AND b.MasterDayText = a.MasterDayText AND b.AnalysisDayText = a.AnalysisDayText)
			INNER JOIN #First c ON (c.LedgerID = a.LedgerID AND c.LedgerSource = a.LedgerSource AND c.MasterDayText = a.MasterDayText AND c.AnalysisDayText = a.AnalysisDayText)
			INNER JOIN #Last d ON (d.LedgerID = a.LedgerID AND d.LedgerSource = a.LedgerSource AND d.MasterDayText = a.MasterDayText AND d.AnalysisDayText = a.AnalysisDayText)
			LEFT JOIN #Dim x ON (x.AccountKey = b.AccountKey AND x.DayKey = b.DayKey)
	WHERE	BatchKey = @BatchKey 
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,'Sp_PositionFactLoad','Update','Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	b
	SET		b.ActivityKey = a.ActivityKey,
			b.AccountStatusKey=a.AccountStatusKey,
			b.FirstDepositDayKey = a.FirstDepositDayKey,
			b.FirstBetDayKey = a.FirstBetDayKey,
			b.FirstBetTypeKey = a.FirstBetTypeKey,
			b.FirstClassKey = a.FirstClassKey,
			b.FirstChannelKey = a.FirstChannelKey,
			b.LastDepositDayKey = a.LastDepositDayKey,
			b.LastBetDayKey = a.LastBetDayKey,
			b.LastBetTypeKey = a.LastBetTypeKey,
			b.LastClassKey = a.LastClasskey,
			b.LastChannelKey = a.LastChannelKey,
			b.OpeningBalance = a.OpeningBalance,
			b.TransactedAmount = a.TransactedAmount,
			b.ClosingBalance = a.ClosingBalance,
			b.DaysSinceAccountOpened = a.DaysSinceAccountOpened,
			b.DaysSinceFirstDeposit = a.DaysSinceFirstDeposit,
			b.DaysSinceFirstBet = a.DaysSinceFirstBet,
			b.DaysSinceLastDeposit = a.DaysSinceLastDeposit,
			b.DaysSinceLastBet = a.DaysSinceLastBet,
			b.LifetimeTurnover = a.LifetimeTurnover,
			b.LifetimeGrossWin = a.LifetimeGrossWin,
			b.LifetimeBetCount = a.LifetimeBetCount,
			b.LifetimeBonus = a.LifetimeBonus,
			b.LifetimeDeposits = a.LifetimeDeposits,
			b.LifetimeWithdrawals = a.LifetimeWithdrawals,
			b.TierKey = a.TierKey,
			b.TierKeyFiscal = a.TierKeyFiscal,
			b.TurnoverMTDCalender = a.TurnoverMTDCalender,
			b.TurnoverMTDFiscal = a.TurnoverMTDFiscal,
			b.RevenueLifeCycleKey = a.RevenueLifeCycleKey,
			b.LifeStageKey = a.LifeStageKey,
			b.PreviousLifeStageKey = a.PreviousLifeStageKey,
			b.ModifiedDate = GetDate(),
			b.ModifiedBatchKey = @BatchKey,
			b.ModifiedBy = 'Sp_PositionFactLoad'
	FROM	#Position a
			INNER JOIN [$(Dimensional)].Fact.Position b on a.AccountKey = b.AccountKey and a.DayKey = b.DayKey
	WHERE	a.Upsert = 'U'
			AND b.DayKey in (SELECT DayKey FROM #Days)
	OPTION (RECOMPILE)

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,'Sp_PositionFactLoad','Insert','Start', @RowsProcessed = @RowCount

	INSERT	[$(Dimensional)].[Fact].[Position]
		(	LedgerID,
			LedgerSource,
			OpeningBalance,
			TransactedAmount,
			ClosingBalance,
			DaysSinceAccountOpened,
			DaysSinceFirstDeposit,
			DaysSinceFirstBet,
			DaysSinceLastDeposit,
			DaysSinceLastBet,
			LifetimeTurnover,
			LifetimeGrossWin,
			LifetimeBetCount,
			LifetimeBonus,
			LifetimeDeposits,
			LifetimeWithdrawals,
			TurnoverMTDCalender,
			TurnoverMTDFiscal,
			BalanceTypeKey,
			AccountKey,
			AccountStatusKey,
			DayKey,
			ActivityKey,
			FirstDepositDayKey,
			FirstBetDayKey,
			FirstBetTypeKey,
			FirstClassKey,
			FirstChannelKey,
			LastDepositDayKey,
			LastBetDayKey,
			LastBetTypeKey,
			LastClasskey,
			LastChannelKey,
			TierKey,
			TierKeyFiscal,
			RevenueLifeCycleKey,
			LifeStageKey,
			PreviousLifeStageKey,
			CreatedDate,
			CreatedBatchKey,
			CreatedBy,
			ModifiedDate,
			ModifiedBatchKey,
			ModifiedBy 
		)
	SELECT	LedgerID,
			LedgerSource,
			OpeningBalance,
			TransactedAmount,
			ClosingBalance,
			DaysSinceAccountOpened,
			DaysSinceFirstDeposit,
			DaysSinceFirstBet,
			DaysSinceLastDeposit,
			DaysSinceLastBet,
			LifetimeTurnover,
			LifetimeGrossWin,
			LifetimeBetCount,
			LifetimeBonus,
			LifetimeDeposits,
			LifetimeWithdrawals,
			TurnoverMTDCalender,
			TurnoverMTDFiscal,
			BalanceTypeKey,
			AccountKey,
			AccountStatusKey,
			DayKey,
			ActivityKey,
			FirstDepositDayKey,
			FirstBetDayKey,
			FirstBetTypeKey,
			FirstClassKey,
			FirstChannelKey,
			LastDepositDayKey,
			LastBetDayKey,
			LastBetTypeKey,
			LastClasskey,
			LastChannelKey,
			TierKey,
			TierKeyFiscal,
			RevenueLifeCycleKey,
			LifeStageKey,
			PreviousLifeStageKey,
			GETDATE() CreatedDate,
			@BatchKey CreatedBatchKey,
			'Sp_PositionFactLoad' CreatedBy,
			GETDATE() ModifiedDate,
			@BatchKey ModifiedBatchKey,
			'Sp_PositionFactLoad' as ModifiedBy 
	FROM	#Position
	WHERE	Upsert = 'I'
	OPTION (RECOMPILE)

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,'Sp_PositionFactLoad',NULL,'Success', @RowsProcessed = @RowCount

END TRY

BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log	@BatchKey, 'Sp_PositionFactLoad', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

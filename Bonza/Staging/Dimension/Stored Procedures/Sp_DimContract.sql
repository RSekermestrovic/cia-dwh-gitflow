﻿CREATE PROCEDURE [Dimension].[Sp_DimContract]
(
	@BatchKey	INT
)
WITH RECOMPILE
AS 

BEGIN
	
	EXEC [Control].[Sp_Log]	@BatchKey,'Sp_DimContract','Candidates','Start'

	BEGIN TRY DROP TABLE #ContractBuffer END TRY BEGIN CATCH END CATCH
	BEGIN TRY DROP TABLE #Contract END TRY BEGIN CATCH END CATCH

	SELECT 	MAX(CASE WHEN TransactionTypeId IN ('BT','BB','FE') THEN x.BetGroupID ELSE NULL END) BetGroupID,
			MAX(CASE WHEN TransactionTypeId IN ('BT','BB','FE') THEN x.BetGroupIDSource ELSE NULL END) BetGroupIDSource,
			MAX(CASE WHEN TransactionTypeId IN ('BT','BB','FE') THEN x.BetId ELSE NULL END) BetId,
			MAX(CASE WHEN TransactionTypeId IN ('BT','BB','FE') THEN x.BetIdSource ELSE NULL END) BetIdSource,
			LegId,
			LegIdSource,
			MAX(TransactionId) TransactionId,
			MAX(TransactionIdSource) TransactionIdSource,
			MAX(FreeBetID) FreeBetID,
			MAX(CASE ExternalID WHEN '' THEN 'N/A' WHEN 'NULL' THEN 'N/A' ELSE ISNULL(ExternalID,'N/A') END) ExternalID,
			MAX(CASE External1 WHEN '' THEN 'N/A' WHEN 'NULL' THEN 'N/A' ELSE ISNULL(External1,'N/A') END) External1,
			MAX(CASE External2 WHEN '' THEN 'N/A' WHEN 'NULL' THEN 'N/A' ELSE ISNULL(External2,'N/A') END) External2,
			MAX(RequestID) RequestID,
			MAX(WithdrawID) WithdrawID,
			MIN(x.FromDate) FromDate
	INTO	#ContractBuffer
	FROM	Stage.[Transaction] x
	WHERE	BatchKey = @BatchKey
			AND ExistsInDim = 0
	GROUP BY LegId,
			LegIdSource

	EXEC [Control].[Sp_Log] @BatchKey,'Sp_DimContract','Prejoin','Start', @RowsProcessed = @@ROWCOUNT

	SELECT	B.*,
			CASE WHEN A.LegId IS NULL THEN 'I'
			ELSE 'U'
			END Upsert
	INTO	#Contract
	FROM	#ContractBuffer B
			LEFT JOIN [$(Dimensional)].Dimension.Contract A ON A.LegId=B.LegId AND A.LegIdSource=B.LegIdSource
	WHERE	A.LegId IS NULL
			OR (A.ExternalID = 'N/A' AND B.ExternalID <> 'N/A')
			OR (A.External1 = 'N/A' AND B.External1 <> 'N/A')
			OR (A.External2 = 'N/A' AND B.External2 <> 'N/A')
			OR (A.RequestID IS NULL AND B.RequestID IS NOT NULL)
			OR (A.WithdrawID IS NULL AND B.WithdrawID IS NOT NULL)


	EXEC [Control].[Sp_Log] @BatchKey,'Sp_DimContract','Update','Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	A
	SET		A.ExternalID	=	CASE WHEN A.ExternalID = 'N/A'	THEN B.ExternalID	ELSE A.ExternalID	END,
			A.External1		=	CASE WHEN A.External1 = 'N/A'	THEN B.External1	ELSE A.External1	END,
			A.External2		=	CASE WHEN A.External2 = 'N/A'	THEN B.External2	ELSE A.External2	END,
			A.RequestID		=	CASE WHEN A.RequestID  IS NULL	THEN B.RequestID	ELSE A.RequestID	END,
			A.WithdrawID	=	CASE WHEN A.WithdrawID IS NULL	THEN B.WithdrawID	ELSE A.WithdrawID	END,
			A.ModifiedDate	= GETDATE(),
			A.ModifiedBatchKey = @BatchKey,
			A.ModifiedBy	= 'Sp_DimContract'
	FROM	[$(Dimensional)].Dimension.Contract A
			INNER JOIN #Contract B ON A.LegId=B.LegId AND A.LegIdSource=B.LegIdSource
	WHERE	B.Upsert = 'U'

	EXEC [Control].[Sp_Log] @BatchKey,'Sp_DimContract','Insert','Start', @RowsProcessed = @@ROWCOUNT

	INSERT INTO [$(Dimensional)].Dimension.Contract 
		(	BetGroupID,BetGroupIDSource,BetId,BetIdSource,LegId,LegIdSource,
			TransactionID,TransactionIDSource,FreeBetID,
			ExternalID,External1,External2,RequestID,WithdrawID,
			FromDate,ToDate,FirstDate,CreatedDate,CreatedBatchKey,CreatedBy,ModifiedDate,ModifiedBatchKey,ModifiedBy)
	SELECT	A.BetGroupID,A.BetGroupIDSource,A.BetId,A.BetIdSource,A.LegId,A.LegIdSource,
			A.TransactionID,A.TransactionIDSource,A.FreeBetID,
			A.ExternalID,A.External1,A.External2,A.RequestID,A.WithdrawID,
			A.FromDate,'9999-12-31',A.FromDate,GETDATE(),@BatchKey,'Sp_DimContract',GETDATE(),@BatchKey,'Sp_DimContract'
	FROM	#Contract A
	WHERE	A.Upsert = 'I'

	EXEC [Control].[Sp_Log] @BatchKey,'Sp_DimContract',NULL,'Success', @RowsProcessed = @@ROWCOUNT

END

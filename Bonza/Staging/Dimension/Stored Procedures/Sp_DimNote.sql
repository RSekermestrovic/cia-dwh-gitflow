﻿CREATE PROCEDURE [Dimension].[Sp_DimNote]
	@BatchKey int	
AS
BEGIN TRY

	SET NOCOUNT ON;
	
	EXEC [Control].Sp_Log	@BatchKey,'Sp_DimNote','Type1-UpdateStaging','Start'

	UPDATE d
	set d.Notes=s.Notes,
	D.FromDate=S.fromDate,
	D.ModifiedDate=CURRENT_TIMESTAMP,
	D.ModifiedBatchKey=@BatchKey,
	D.ModifiedBy='Sp_DimNote'
	from [Stage].[Note] s
	inner join [$(Dimensional)].Dimension.Note d on (d.NoteId=s.NoteId and d.NoteSource=s.NoteSource)
	where s.BatchKey = @BatchKey 
		and d.Notes<>s.Notes;

	INSERT INTO[$(Dimensional)]. Dimension.Note(NoteId,NoteSource,Notes,FromDate,ToDate,FirstDate,CreatedDate,CreatedBatchKey,CreatedBy,ModifiedDate,ModifiedBatchKey,ModifiedBy)
	SELECT
	s.NoteId,
	s.NoteSource,
	s.Notes,
	S.FromDate	,
	'9999-12-31 00:00:00.000',
	CURRENT_TIMESTAMP,
	CURRENT_TIMESTAMP,
	@BatchKey,
	'Sp_DimNote',
	CURRENT_TIMESTAMP	,
	@BatchKey,
	'Sp_DimNote'
	from [Stage].[Note] s
	left outer join [$(Dimensional)].Dimension.Note d on (d.NoteId=s.NoteId and d.NoteSource=s.NoteSource)
	where s.BatchKey = @BatchKey 
		and d.NoteId IS NULL;

	EXEC [Control].Sp_Log @BatchKey, 'Sp_DimNote', NULL, 'Success'
	
END TRY
BEGIN CATCH

	declare @Error int = ERROR_NUMBER();
	declare @ErrorMessage varchar(max) =  ERROR_MESSAGE();
	
	EXEC [Control].Sp_Log @BatchKey, 'Sp_DimNote', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

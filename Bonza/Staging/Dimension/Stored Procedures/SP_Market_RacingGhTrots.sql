-- =============================================
-- Author:		Lekha Narra
-- Create date: 12/11/2014
-- Description:	Insert Event names, Round names, Competition names, Grade, Distance  for 'Racing','Greyhounds','Trots' event types
-- =============================================
CREATE PROCEDURE Dimension.[SP_Market_RacingGhTrots] 
	-- Add the parameters for the stored procedure here
	@BatchKey int 
AS
BEGIN
BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

BEGIN TRANSACTION Market_RacingGhTrots;

-- Speculative attempt to drop pre-existing Temp table, don't care if it fails due to non-existence
	BEGIN TRY
		DROP TABLE #TempRound;
	END TRY BEGIN CATCH	END CATCH 
		
  -- ================================================================================================================================================================
  --                        PART 1: Update Event names in Market Dimension for 'Racing','Greyhounds','Trots' event types
  -- ================================================================================================================================================================
	
		--Remove time part from event name
		update [$(Dimensional)].Dimension.market
		set EventName=CASE 
							when SourceEventName like '%[[]%'
							then RTRIM(LTRIM(SUBString(SourceEventName, CHARINDEX(']',SourceEventName)+1,LEN(SourceEventName))))
							ELSE SourceEventName
							END
		FROM [$(Dimensional)].Dimension.MARKET
		where sourceeventtype in ('Greyhounds','Racing','Trots' )
		and ModifiedBatchKey=@BatchKey and (ColumnLock&Power(2,(2-1))<>Power(2,(2-1)))

		--Remove time part from event name
		update [$(Dimensional)].Dimension.market
		set EventName=CASE 
							when EventName like '%[0-9][0-9]:[0-9][0-9] %'
							then RTRIM(LTRIM(SUBString(EventName, PATINDEX('%[0-9][0-9]:[0-9][0-9] %',EventName)+5,LEN(EventName))))
							ELSE EventName
							END
		FROM [$(Dimensional)].Dimension.MARKET
		where sourceeventtype in ('Greyhounds','Racing','Trots')
		and ModifiedBatchKey=@BatchKey  and (ColumnLock&Power(2,(2-1))<>Power(2,(2-1)))

		--Remove Distance part from event name  
		update [$(Dimensional)].Dimension.market
		set EventName=CASE 
							when EventName like '%[1-9][0-9][0-9][0-9]m%'
							then RTRIM(LTRIM(SUBString(EventName, 1,PATINDEX('%[1-9][0-9][0-9][0-9]m%',EventName)-1)))
							ELSE EventName
							END
		FROM [$(Dimensional)].Dimension.MARKET
		where sourceeventtype in ('Greyhounds','Racing','Trots' )
		and ModifiedBatchKey=@BatchKey and (ColumnLock&Power(2,(2-1))<>Power(2,(2-1)))

		--Remove Distance part from event name
		update [$(Dimensional)].Dimension.market
		set EventName=CASE 
							when EventName like '%[1-9][0-9][0-9]m%'
							then RTRIM(LTRIM(SUBString(EventName, 1,PATINDEX('%[1-9][0-9][0-9]m%',EventName)-1)))
							when EventName like '%[1-9][0-9][0-9] mtrs%'
							then RTRIM(LTRIM(SUBString(EventName, 1,PATINDEX('%[1-9][0-9][0-9] mtrs%',EventName)-1)))
							ELSE EventName
							END
		FROM [$(Dimensional)].Dimension.MARKET
		where sourceeventtype in ('Greyhounds','Racing','Trots')
		and ModifiedBatchKey=@BatchKey  and (ColumnLock&Power(2,(2-1))<>Power(2,(2-1)))
		
		--Remove Race Number from event name
		update [$(Dimensional)].Dimension.market
		set EventName=CASE 
							when EventName like '%Race [1-9]%'
							then RTRIM(LTRIM(SUBString(EventName, PATINDEX('%Race [1-9]%',EventName)+6,Len(EventName)-6)))
							ELSE EventName
							END
		FROM [$(Dimensional)].Dimension.MARKET
		where sourceeventtype in ('Greyhounds','Racing','Trots' )
		and ModifiedBatchKey=@BatchKey  and (ColumnLock&Power(2,(2-1))<>Power(2,(2-1)))
		/**/
		--Remove Race Number from event name
		update [$(Dimensional)].Dimension.market
		set EventName=CASE 
							When EventName like 'Race [1-9]'
							then Eventname
							When EventName like 'Race [1-9][0-9]'
							then Eventname
							when EventName like '%Race [1-9] - %'
							then RTRIM(LTRIM(SUBString(EventName, PATINDEX('%Race [1-9]%',EventName)+9,Len(EventName)-9)))
							when EventName like '%Race [1-9][0-9] - %'
							then RTRIM(LTRIM(SUBString(EventName, PATINDEX('%Race [1-9][0-9]%',EventName)+10,Len(EventName)-10)))
							when EventName like '%Race [1-9]%'
							then Ltrim(RTRIM(Substring(Eventname,1,PATINDEX('%Race [1-9]%',EventName)-1)+Substring(Eventname,PATINDEX('%Race [1-9]%',EventName)+7,(Len(EVentname)))))
							
							ELSE EventName
							END
		FROM [$(Dimensional)].Dimension.MARKET
		where sourceeventtype in ('Greyhounds','Racing','Trots' )
		and ModifiedBatchKey=@BatchKey  and (ColumnLock&Power(2,(2-1))<>Power(2,(2-1))) 
		
		--Format the eventname 
		update [$(Dimensional)].Dimension.market
		set EventName=Rtrim(Ltrim(Replace(Rtrim(Ltrim(EVentname)),'-','')))
		where Eventname like '%-'  or Eventname like '-%' or Eventname like '- %' 
		and ModifiedBatchKey=@BatchKey and (ColumnLock&Power(2,(2-1))<>Power(2,(2-1)))
		
		update [$(Dimensional)].Dimension.market
		set EventName=Rtrim(Ltrim(Replace(Rtrim(Ltrim(EVentname)),':','')))
		where Eventname like '%:'  or Eventname like ':%' or Eventname like ': %' 
		and ModifiedBatchKey=@BatchKey and (ColumnLock&Power(2,(2-1))<>Power(2,(2-1)))
		
		Update [$(Dimensional)].Dimension.Market
		set EventName='Unknown'
		where  SourceEventType in ('Trots','Greyhounds','Racing') and ModifiedBatchKey=@BatchKey and EventName='' 
		 and (ColumnLock&Power(2,(2-1))<>Power(2,(2-1)))

		
-- ================================================================================================================================================================
  --                        PART 2: Update Round names in Market Dimension for 'Racing','Greyhounds','Trots' event types
-- ================================================================================================================================================================

	create table #tempRound
	(

	 EventID int,
	 RaceNumber int
	 )
	 Insert into #tempRound(EventID,RaceNumber)
	 select m.sourceEventID, a.RaceNumber
	 from [$(Dimensional)].Dimension.market m
	 left join [$(Acquisition)].IntraBet.tblEvents a on a.EventID=m.SourceEventID and (a.ToDate='9999-12-30 00:00:00.000' or a.ToDate='9999-12-31 00:00:00.000')
	 and m.SourceEventType in ('Greyhounds' ,'Racing','Trots' )
 
	 Update [$(Dimensional)].Dimension.market
	 set RoundName=Rtrim(case when (a.RaceNumber=0 or a.RaceNumber is null)
							  then 'Race Unknown'+(case when b.SourceEventName like'%[[]%' 
														then ltrim(Rtrim(Substring(b.SourceEventName,2,5))) 
														else '' 
												   end)
							   else	'Race '+Cast(a.RaceNumber as varchar(10))+' '+(case when b.SourceEventName like'%[[]%' 
																						then ltrim(Rtrim(Substring(b.SourceEventName,2,5)))
																						else ''
																				   end)
						 End)
	from [$(Dimensional)].Dimension.market b left join  #tempRound a 
	on a.EventID=b.SourceEventId
	where b.ModifiedBatchKey=@BatchKey and (b.ColumnLock&Power(2,(10-1))<>Power(2,(10-1)))
	and b.SourceEventType in ('Greyhounds' ,'Racing','Trots' )
	
	Update [$(Dimensional)].Dimension.Market
	set RoundName='Unknown'
	where  SourceEventType in ('Trots','Greyhounds','Racing') and ModifiedBatchKey=@BatchKey and RoundName=''
	  and (ColumnLock&Power(2,(10-1))<>Power(2,(10-1)))

-- ================================================================================================================================================================
  --                        PART 3: Update Competition names in Market Dimension for 'Racing','Greyhounds','Trots' event types
-- ================================================================================================================================================================

	Update [$(Dimensional)].Dimension.market
	set CompetitionName=VenueName+' '+cast(Convert(Date,SourceEventDate) as varchar(32))
	from [$(Dimensional)].Dimension.market 
	where SourceEventType in ('Greyhounds','Racing','Trots')
	and ModifiedBatchKey=@BatchKey and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1)))
	
	Update [$(Dimensional)].Dimension.Market
	set CompetitionName='Unknown'
	where  SourceEventType in ('Trots','Greyhounds','Racing') and ModifiedBatchKey=@BatchKey and CompetitionName=''
	 and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1)))

-- ================================================================================================================================================================
  --                        PART 4: Update Distance in Market Dimension for 'Racing','Greyhounds','Trots' event types
-- ================================================================================================================================================================

	Update [$(Dimensional)].Dimension.market
	set distance=(Case when(SourceEventName like '%[1-9][0-9][0-9]%m')
					  Then Ltrim(Substring(SourceEventname,Len(SourceEventname)-4,4)+' m')
					  when(SourceEventName like '%[1-9][0-9][0-9]%Mtrs')
					  Then RTRIM(Ltrim(Substring(SourceEventname,Len(SourceEventname)-8,5)))+' m'
					  Else 'Unknown'
				 End)
	from [$(Dimensional)].Dimension.market where SourceEventType in ('Trots','Greyhounds','Racing')
	and ModifiedBatchKey=@BatchKey and (ColumnLock&Power(2,(6-1))<>Power(2,(6-1)))
	
	Update [$(Dimensional)].Dimension.Market
	set Distance='Unknown'
	where  SourceEventType in ('Trots','Greyhounds','Racing') and ModifiedBatchKey=@BatchKey  and Distance=''
	 and (ColumnLock&Power(2,(6-1))<>Power(2,(6-1)))

-- ================================================================================================================================================================
  --                        PART 5: Update Grade in Market Dimension for 'Racing' event type
-- ================================================================================================================================================================

	Update [$(Dimensional)].Dimension.Market
	set Grade=''
	where SourceEventType='Racing'
	and ModifiedBatchKey=@BatchKey and (ColumnLock&Power(2,(5-1))<>Power(2,(5-1)))

		Update [$(Dimensional)].Dimension.Market
		set Grade=Grade+Case when (eventname like '%FM%' or eventname like '%F & M%')
							 then '-'+'Fillies & Mares'
							 else ''
						 end

						+Case when (eventname like '%Benchmark [1-9][0-9]%')
							  then '-'+Rtrim(substring(eventname,Charindex('Benchmark',eventname),13))
							  Else ''
						 End
						 +Case when (eventname like '%Bm[1-9][0-9]%'and eventname not like '%Bm[1-9][0-9]+%')
							  then '-'+Rtrim(substring(eventname,PATINDEX('%Bm[1-9][0-9]%',eventname),4))
							  Else ''
						 End
						 +Case when (eventname like '%Bm[1-9][0-9]+%')
							  then '-'+Rtrim(substring(eventname,PATINDEX('%Bm[1-9][0-9]%',eventname),5))
							  Else ''
						 End
						 +Case when (eventname like '%Maiden%' or eventname like '%Mdn%')
							  then '-'+'Maiden'
							  Else ''
						 End
						 +Case when (eventname like '%Auction%')
							  then '-'+'Auction'
							  Else ''
						 End
						 +Case when (eventname like '%Plate%' or eventname like '%Plte%')
							  then '-'+'Plate'
							  Else ''
						 End
						 +Case when (eventname like '%C[1-9]%')
							  then '-'+Rtrim(substring(eventname,PATINDEX('%C[1-9]%',eventname),2))
							  Else ''
						 End
						 +Case when (eventname like '%Fillies%')
							  then '-'+'Fillies'
							  Else ''
						 End
						 +Case when (eventname  like '%Derby%')
							  then '-'+'Derby'
							  Else ''
						 End
						 +Case when (eventname like '%Divided%')
							  then '-'+'Divided'
							  Else ''
						 End
						 +Case when (eventname like '%Handicap%' or eventname like '%Hcp%' or eventname like '%Hcap%')
							  then '-'+'Handicap'
							  Else ''
						 End
						 +Case when (eventname like '%Hcp-[1-9][0-9]%')
							  then '-'+Rtrim(substring(eventname,PATINDEX('%Hcp-[1-9][0-9]%',eventname)+4,3))
							  Else ''
						 End
						 +Case when (eventname like '%Hcp-([1-9][0-9])%')
							  then '-'+Rtrim(substring(eventname,PATINDEX('%Hcp-([1-9][0-9])%',eventname)+5,2))
							  Else ''
						 End
						 +Case when (eventname like '%Handicap [1-9][0-9]%')
							  then '-'+Rtrim(substring(eventname,PATINDEX('%Handicap [1-9][0-9]%',eventname)+9,2))
							  Else ''
						 End
						 +Case when (eventname like '%[1-9][0-9] Handicap%' and eventname not like '%MR [1-9][0-9] Handicap%')
							  then '-'+Rtrim(substring(eventname,PATINDEX('%[1-9][0-9] Handicap%',eventname),2))
							  Else ''
						 End
						 +Case when (eventname like '%Hcp([1-9][0-9])%' or eventname like '%Hcp([1-9][0-9])%')
							  then '-'+Ltrim(Rtrim(substring(eventname,PATINDEX('%Hcp%([1-9][0-9])%',eventname)+3,3)))
							  Else ''
						 End
						 +Case when (eventname like '%Handicap Chase%')
							  then '-'+'Chase'
							  Else ''
						 End
						 +Case when (eventname like '%Hcp ([1-9][0-9]) %' or eventname like '%Hcp-[1-9][0-9] %')
							  then '-'+Rtrim(substring(eventname,PATINDEX('%Hcp ([1-9][0-9]) %',eventname)+5,3))
							  Else ''
						 End
						 +Case when (eventname  like '%Yearling%')
							  then '-'+'Yearling'
							  Else ''
						 End
						 +Case when (eventname  like '%Straight%' or eventname  like '%Str %')
							  then '-'+'Straight'
							  Else ''
						 End
						 +Case when (eventname  like '%Stake%' or eventname  like '% Stks%')
							  then '-'+'Stakes'
							  Else ''
						 End
						 +Case when (eventname  like '%Group_[1-9]%')
							  then '-'+'Group'+Rtrim(substring(eventname,PATINDEX('%Group%',eventname)+5,2))
							  Else ''
						 End
						 +Case when (eventname  like '%Grp_[1-9]%')
							  then '-'+'Group'+Rtrim(substring(eventname,PATINDEX('%Grp%',eventname)+3,2))
							  Else ''
						 End
						 +Case when (eventname  like '%Hurdle%' or eventname  like '%Hrdl%')
							  then '-'+'Hurdle'
							  Else ''
						 End
						 +Case when (eventname like '%Hrdl-[1-9][0-9]%' or eventname like '%Hrdl ([1-9][0-9]) %')
							  then '-'+Rtrim(substring(eventname,PATINDEX('%Hrdl-[1-9][0-9]%',eventname)+5,3))
							  Else ''
						 End
						 +Case when (eventname  like '%Hurdle-[1-9][0-9]%')
							  then '-'+Rtrim(substring(eventname,PATINDEX('%Hurdle-[1-9][0-9] %',eventname)+8,3))
							  Else ''
						 End
						 +Case when (eventname like '%Hwt-[1-9][0-9]%')
							  then '-'+'Hwt '+Rtrim(substring(eventname,PATINDEX('%Hwt-[1-9][0-9]%',eventname)+4,3))
							  Else ''
						 End
						 +Case when (eventname  like '%Flat%')
							  then '-'+'Flat Race'
							  Else ''
						 End
						 +Case when (eventname  like '%Cl _%')
							  then '-'+Rtrim(substring(eventname,PATINDEX('%Cl _%',eventname),4))
							  Else ''
						 End
						  +Case when (eventname  like '%Cl_[1-9]%')
							  then '-'+Rtrim(substring(eventname,PATINDEX('%Cl_[1-9]%',eventname)+2,2))
							  Else ''
						 End
						  +Case when (eventname  like '% F %')
							  then '-'+'F'
							  Else ''
						 End
						  +Case when (eventname  like '% [1-9]m%')
							  then '-'+Rtrim(substring(eventname,PATINDEX('%[1-9]m%',eventname),2))
							  Else ''
						 End
						  +Case when (eventname  like '% [1-9]f%' and eventname not like '%[1-9]m%[1-9]f%')
							  then '-'+Rtrim(substring(eventname,PATINDEX('%[1-9]f%',eventname),2))
							  Else ''
						 End
						  +Case when (eventname like '%[1-9]m%[1-9]f%')
							  then '-'+Rtrim(substring(eventname,PATINDEX('%[1-9]m%',eventname)+3,PATINDEX('%[1-9]f%',eventname)+1))
							  Else ''
						 End
						  +Case when (eventname  like '%LEG[1-9]%' or eventname  like '%LEG [1-9]%')
							  then '-'+'Leg'+Rtrim(substring(eventname,PATINDEX('%LEG_[1-9]%',eventname)+3,2))
							  Else ''
						 End
						  +Case when (eventname  like '%PICK[1-9]%' or eventname  like '%PICK [1-9]%' )
							  then '-'+'Pick'+Ltrim(Rtrim(substring(eventname,PATINDEX('%PICK_[1-9]%',eventname)+4,2)))
							  Else ''
						 End
						  +Case when (eventname  like '%DIV[1-9]%' or eventname  like '%DIV [1-9]%')
							  then '-'+'Div'+Ltrim(Rtrim(substring(eventname,PATINDEX('%DIV_[1-9]%',eventname)+3,2)))
							  Else ''
						 End
						  +Case when (eventname  like '%Listed%')
							  then '-'+'Listed'
							  Else ''
						 End
						  +Case when (eventname  like '%Nursery%')
							  then '-'+'Nursery'
							  Else ''
						 End
						 +Case when (eventname  like '%Grade_[1-9]%')
							  then '-'+Rtrim(substring(eventname,PATINDEX('%Grade_[1-9]%',eventname),7))
							  Else ''
						 End
						 +Case when (eventname  like '%[1-9][0-9]-[1-9][0-9]%' and eventname not like '%[1-9][0-9]-[1-9][0-9][0-9]%')
							  then '-'+Rtrim(substring(eventname,PATINDEX('%[1-9][0-9]-[1-9][0-9]%',eventname),5))
							  Else ''
						 End
						 +Case when (eventname  like '%[1-9]_-[1-9][0-9][0-9]%')
							  then '-'+Rtrim(substring(eventname,PATINDEX('%[1-9]_-[1-9][0-9][0-9]%',eventname),5))
							  Else ''
						 End
						 +Case when (eventname  like '%French Tote Only%')
							  then '-'+Rtrim(substring(eventname,PATINDEX('%French Tote Only%',eventname),16))
							  Else ''
						 End
						 +Case when (eventname  like '%[1-9] YO Only%' or eventname  like '%[1-9]YO Only%')
							  then '-'+Rtrim(substring(eventname,PATINDEX('%[1-9]%Only%',eventname),1))+'YO Only'
							  Else ''
						 End
						 +Case when (eventname  like '%[1-9] YO Plus%' or eventname  like '%[1-9]YO Plus%')
							  then '-'+Rtrim(substring(eventname,PATINDEX('%[1-9]YO Plus%',eventname),8))
							  Else ''
						 End
						 +Case when (EventName  like '%[1-9] YO+%' or EventName  like '%[1-9]YO+%' )
							  then '-'+Rtrim(substring(eventname,PATINDEX('%[1-9]_YO +%',eventname),5))
							  Else ''
						 End
						 +Case when (EventName  like '%[1-9]YO/[1-9]YO%' )
							  then '-'+Rtrim(substring(eventname,PATINDEX('%[1-9]YO/[1-9]YO%',eventname),7))
							  Else ''
						 End
						 +Case when ((EventName  like '%[1-9] YO%' or EventName  like '%[1-9]YO%') and EventName not like '%[1-9]YO plus%' 
						 and EventName not like '%[1-9]YO only%' and EventName not like '%[1-9]YO+%' and EventName not like '%[1-9]YO/[1-9]YO%')
							  then '-'+Rtrim(substring(eventname,PATINDEX('%[1-9]%YO%',eventname),4))
							  Else ''
						 End
						 +Case when (EventName  like '%Staple%' or eventname like '%stpl%')
							  then '-'+'Staple'
							  Else ''
						 End
						 +Case when (EventName  like '%Class %')
							  then '-'+Rtrim(substring(eventname,PATINDEX('%Class%',eventname),8))
							  Else ''
						 End
						 +Case when (EventName  like '%Stud-[1-9][0-9]%')
							  then '-'+Rtrim(substring(eventname,PATINDEX('%Stud%',eventname),7))
							  Else ''
						 End
						 +Case when (EventName  like '% Mare %' or eventname like '% MR %')
							  then '-'+'Mare'
							  Else ''
						 End
						 +Case when (eventname like '%MR[1-9][0-9]%')
							  then '-'+'Mare'+Rtrim(substring(eventname,PATINDEX('%Mr%',eventname)+2,3))
							  Else ''
						 End
						 +Case when (EventName  like '%Mr [1-9][0-9]%')
							  then '-'+Rtrim(substring(eventname,PATINDEX('%Mr%',eventname)+3,3))
							  Else ''
						 End
						 +Case when (EventName  like '%[1-9][0-9] Mr%')
							  then '-'+Rtrim(substring(eventname,PATINDEX('%Mr%',eventname),3))
							  Else ''
						 End
						 +Case when (eventname like '%Rnd%')
							  then '-'+'RND'
							  Else ''
						 End
						 +Case when (eventname like '%Ntnl%')
							  then '-'+'Ntnl'
							  Else ''
						 End
						 +Case when (substring(eventname,Len(eventname)-2,3) like '%-[1-9][0-9]%' and right(eventname,7) not like '%Hwt-[1-9][0-9]%' 
									and eventname not like '%Hcp-[1-9][0-9]%' and eventname not like '%Bm-[1-9][0-9]%' and eventname not like '%Mr-[1-9][0-9]%')
							  then '-'+substring(eventname,Len(eventname)-1,2)
							  Else ''
						 End
						 +Case when (substring(eventname,Len(eventname)-3,4) like '%([1-9][0-9])%' 
						             and Grade not like substring(eventname,Len(eventname)-2,2) and SourceEventType='Racing')
							  then '-'+substring(eventname,Len(eventname)-2,2)
							  Else ''
						 End
						
		where  ModifiedBatchKey=@BatchKey  and SourceEventType='Racing' and (ColumnLock&Power(2,(5-1))<>Power(2,(5-1)))
			
		Update [$(Dimensional)].Dimension.Market
		set Grade=Replace(Grade,'+',' plus')
		where  SourceEventType='Racing' and ModifiedBatchKey=@BatchKey and (ColumnLock&Power(2,(5-1))<>Power(2,(5-1)))
				
		Update [$(Dimensional)].Dimension.Market
		set Grade=Substring(Grade,2,Len(Grade)-1)
		where Grade<>'' and  SourceEventType='Racing'
		and ModifiedBatchKey=@BatchKey and (ColumnLock&Power(2,(5-1))<>Power(2,(5-1)))

		Update [$(Dimensional)].Dimension.Market
		set Grade=Ltrim(Replace(Replace(Grade,'(',''),')',''))
		where  SourceEventType='Racing' and ModifiedBatchKey=@BatchKey and (ColumnLock&Power(2,(5-1))<>Power(2,(5-1)))

		Update [$(Dimensional)].Dimension.Market
		set Grade='Unknown'
		where  SourceEventType='Racing' and ModifiedBatchKey=@BatchKey and Grade='' and (ColumnLock&Power(2,(5-1))<>Power(2,(5-1)))

-- ================================================================================================================================================================
  --                        PART 6: Update Grade in Market Dimension for 'Greyhounds' event type
-- ================================================================================================================================================================

Update [$(Dimensional)].Dimension.Market
set Grade=''
where SourceEventType='Greyhounds'  and ModifiedBatchKey=@BatchKey and (ColumnLock&Power(2,(5-1))<>Power(2,(5-1)))
		
		Update [$(Dimensional)].Dimension.Market
		set Grade=Grade+Case when (eventname like '%C[1-9]%')
								 then '-'+Rtrim(substring(eventname,PATINDEX('%C[1-9]%',eventname),2))
								 Else ''
							End
						   +Case when (eventname like '%C [1-9]/[1-9]%')
								  then '-'+Rtrim(substring(eventname,PATINDEX('%C [1-9]/[1-9]%',eventname),5))
								  Else ''
							 End
						   +Case when (eventname like '%Maiden%'  or eventname like '%Mdn%')
								  then '-Maiden'
								  Else ''
							 End
						    +Case when (eventname like '%Mixed%')
								  then '-Mixed'
								  Else ''
							 End
						    +Case when (eventname like '%Novice%')
								  then '-Novice'
								  Else ''
							 End
							+Case when (eventname like '%Juvenile%')
								  then '-Juvenile'
								  Else ''
							 End
							+Case when (eventname like '%Group Listed%')
								  then '-Group Listed'
								  Else ''
							 End
							+Case when (eventname like '%Stake%' or eventname like '%Stk%')
								  then '-Stake'
								  Else ''
							 End
							+Case when (eventname like '%[0-9]-[1-9] wins%')
								  then '-'+Rtrim(substring(eventname,PATINDEX('%[0-9]-[1-9] wins%',eventname),8))
								  Else ''
							 End
							+Case when (eventname like '%Sprint%')
								  then '-Sprint'
								  Else ''
							 End
							 +Case when (eventname like '%A[1-9]%')
								  then '-'+Rtrim(substring(eventname,PATINDEX('%A[1-9]%',eventname),3))
								  Else ''
							 End
							 +Case when (eventname like '%S[1-9]%')
								  then '-'+Rtrim(substring(eventname,PATINDEX('%S[1-9]%',eventname),3))
								  Else ''
							 End
							+Case when (eventname like '%D[1-9]%')
								  then '-'+Rtrim(substring(eventname,PATINDEX('%D[1-9]%',eventname),3))
								  Else ''
							 End
							+Case when (eventname like '%H[1-9]%')
								  then '-'+Rtrim(substring(eventname,PATINDEX('%H[1-9]%',eventname),3))
								  Else ''
							 End
							+Case when (eventname ='Or')
								  then '-'+'Or'
								  Else ''
							 End
							 +Case when (eventname like '%Tier [1-9]%')
								  then '-'+Rtrim(substring(eventname,PATINDEX('%Tier [1-9]%',eventname),6))
								  Else ''
							 End
							 +Case when (eventname like '%Derby%')
								  then '-'+'Derby'
								  Else ''
							 End
							 +Case when (eventname like '%Stud%')
								  then '-'+'Stud'
								  Else ''
							 End
							 +Case when (eventname like '%Dash%')
								  then '-'+'Dash'
								  Else ''
							 End
							 +Case when (eventname like '%Handicap%' or eventname like '%Hcp%' or eventname like '%Hcap%')
								  then '-'+'Handicap'
								  Else ''
							 End
							 +Case when (eventname  like '%Race winners only%')
								  then '-'+'Race winners only'
								  Else ''
							 End
							+Case when (eventname  like '%[1-9][0-9][0-9]+ rank%')
								  then '-'+Rtrim(substring(eventname,PATINDEX('%[1-9][0-9][0-9]+ rank%',eventname),9))
								  Else ''
							 End
							 +Case when (eventname like '%[1-9]__ Grade%' and eventname not like '%[1-9]__ and [1-9]__ Grade%')
								  then '-'+Rtrim(substring(eventname,PATINDEX('%[1-9]__ Grade%',eventname),9))
								  Else ''
							 End
							 +Case when (eventname like '%Grade [1-9]%')
								  then '-'+Rtrim(substring(eventname,PATINDEX('%Grade [1-9]%',eventname),7))
								  Else ''
							 End
							 +Case when (eventname like '%GR_[1-9]%' and eventname not like '%[1-9]__ and [1-9]__ Grade%')
								  then '-'+Rtrim(substring(eventname,PATINDEX('%GR_[1-9]%',eventname),4))
								  Else ''
							 End
							 +Case when (eventname like '%[1-9]__ and [1-9]__ Grade%')
								  then '-'+Rtrim(substring(eventname,PATINDEX('%[1-9]__ and [1-9]__ Grade%',eventname),17))
								  Else ''
							 End
							 +Case when (eventname like '%Ht [1-9]%')
								  then '-'+Rtrim(substring(eventname,PATINDEX('%Ht [1-9]%',eventname),4))
								  Else ''
							 End
							 +Case when (eventname  like '%Group_[1-9]%')
								  then '-'+Rtrim(substring(eventname,PATINDEX('%Group%',eventname)+5,2))
								  Else ''
							 End
							 +Case when (eventname  like '%Restricted Age%')
								  then '-'+'Restricted Age'
								  Else ''
							 End
							  +Case when (eventname  like '%Grade Restricted%' or eventname  like '% Gdr Restricted%')
								  then '-'+'Grade Restricted'
								  Else ''
							 End
							 +Case when (eventname  like '%Non Graded%')
								  then '-'+'Non Graded'
								  Else ''
							 End
							  +Case when (eventname  like '%No Penalty%')
								  then '-'+'No Penalty'
								  Else ''
							 End
							  +Case when (eventname  like '%ffa%'or eventname like '%free for all%')
								  then '-'+'Free for all'
								  Else ''
							 End
							 
		where  SourceEventType='Greyhounds'	and ModifiedBatchKey=@BatchKey and (ColumnLock&Power(2,(5-1))<>Power(2,(5-1)))
		
		Update [$(Dimensional)].Dimension.Market
		set Grade=Replace(Grade,'+',' plus')
		where  SourceEventType='Greyhounds'
		 and ModifiedBatchKey=@BatchKey and (ColumnLock&Power(2,(5-1))<>Power(2,(5-1)))
				
		Update [$(Dimensional)].Dimension.Market
		set Grade=Substring(Grade,2,Len(Grade)-1)
		where Grade<>'' and  SourceEventType='Greyhounds'
		 and ModifiedBatchKey=@BatchKey and (ColumnLock&Power(2,(5-1))<>Power(2,(5-1)))

		Update [$(Dimensional)].Dimension.Market
		set Grade=Ltrim(Replace(Replace(Grade,'(',''),')',''))
		where  SourceEventType='Greyhounds'
		 and ModifiedBatchKey=@BatchKey and (ColumnLock&Power(2,(5-1))<>Power(2,(5-1)))
		 
		Update [$(Dimensional)].Dimension.Market
		set Grade='Unknown'
		where  SourceEventType='Greyhounds' and ModifiedBatchKey=@BatchKey  and Grade='' and (ColumnLock&Power(2,(5-1))<>Power(2,(5-1)))

-- ================================================================================================================================================================
  --                        PART 7: Update Grade in Market Dimension for 'Trots' event type
-- ================================================================================================================================================================

Update [$(Dimensional)].Dimension.Market
set Grade=''
where SourceEventType='Trots' and ModifiedBatchKey=@BatchKey and (ColumnLock&Power(2,(5-1))<>Power(2,(5-1)))

		Update [$(Dimensional)].Dimension.Market
		set Grade=Grade+Case when (eventname like '%[^a-z,^0-9]FM[^a-z,^0-9]%' or eventname like '%F & M%' or eventname like '%F&M%' or eventname like '%[^a-z,^0-9]F M[^a-z,^0-9]%')
							 then '-'+'Fillies Mares'
							 else ''
						End
						+Case when (eventname like '%Colts%' or eventname like '%[,",",&, ]C[ ,",",&,]%' 
						or eventname like 'C[ ,",",&,]%' or eventname like '%[ ,",",&,]C[^a-z,^0-9,^'']%')
							 then '-'+'Colts'
							 else ''
						End
						+Case when (eventname like '%Geldings%' or eventname like '%[",",&, ]G[ ,",",&,]%' 
						or eventname like 'G[ ,",",&]%' or eventname like '%[ ,",",&]G[^a-z,^0-9,^'']%')
							 then '-'+'Geldings'
							 else ''
						End
						+Case when (eventname like '%Entires%' or eventname like '%[",",&, ]E[ ,",",&,]%' 
						or eventname like 'E[ ,",",&,]%' or eventname like '%[ ,",",&,]E[^a-z,^0-9,^'']%')
							 then '-'+'Entires'
							 else ''
						End
						+Case when (eventname like '%Fillies%')
							 then '-'+'Fillies'
							 else ''
						End
						+Case when (eventname like '%Sire%')
							 then '-'+'Sires'
							 else ''
						End
						+Case when (eventname like '%Stud%')
							 then '-'+'Stud'
							 else ''
						End
						+Case when ( eventname like '%Mares%')
							 then '-'+'Mares'
							 else ''
						End
						+Case when (eventname like '%Stakes%')
							 then '-'+'Stakes'
							 else ''
						End
						+Case when (eventname like '%Swedish Tote Only%')
							 then '-'+'Swedish Tote Only'
							 else ''
						End
						+Case when (eventname like '%French Tote Only%')
							 then '-'+'French Tote Only'
							 else ''
						End
						+Case when (eventname like '%[^a-z,^0-9][m,c,r,t,g,q][0-9] %'  and eventname not like '%[m,c,r,t,g,q][0-9] to [m,c,r,t,g,q][1-9]%'  
									and eventname not like '%[m,c,r,t,g,q][0-9]/[m,c,r,t,g,q][0-9]%'  and eventname not like '%tq[0-9]%'	and eventname not like '%[2,3]_[0-9]%'
									and eventname not like '%[m,c,r,t,g,q][0-9]/[0-9]%'  and eventname not like '%[m,c,r,t,g,q][0-9]-[m,c,r,t,g,q][0-9]%'
									and eventname not like '%[m,c,r,t,g,q][0-9] or better%' and Rtrim(substring(eventname,PATindex('%r[0-9]%',eventname)-1,3)) not like '%Gr[0-9]%')
							 then '-'+Rtrim(substring(eventname,PATindex('%[m,c,r,t,g,q][0-9]%',eventname),2))
							 else ''
						End
						+Case when (eventname like '%[^a-z,^0-9][m,c,r,t,g,q][0-9]+%')
							 then '-'+Rtrim(substring(eventname,PATINDEX('%[m,c,r,t,g,q][0-9]+%',eventname),3))
							 else ''
						End
						+Case when (eventname like '%[^a-z,^0-9][m,c,r,t,g,q][0-9]-[m,c,r,t,g,q][0-9]%')
							 then '-'+Rtrim(substring(eventname,PATINDEX('%[m,c,r,t,g,q][0-9]-[m,c,r,t,g,q][0-9]%',eventname),5))
							 else ''
						End
						+Case when (eventname like '%[^a-z,^0-9][m,c,r,t,g,q][0-9][0-9]-[m,c,r,t,g,q][0-9][0-9]%')
							 then '-'+Rtrim(substring(eventname,PATINDEX('%[m,c,r,t,g,q][0-9][0-9]-[m,c,r,t,g,q][0-9][0-9]%',eventname),7))
							 else ''
						End
						+Case when ( eventname like '%[^a-z,^0-9][m,c,r,t,g,q][0-9][0-9] to [m,c,r,t,g,q][0-9][0-9]%')
							 then '-'+Rtrim(substring(eventname,PATINDEX('%[m,c,r,t,g,q][0-9][0-9] to [m,c,r,t,g,q][0-9][0-9]%',eventname),10))
							 else ''
						End
						+Case when (eventname like '%[m,c,r,t,g,q][0-9] to [c,m,r,t,g,q][1-9]%')
							 then '-'+Rtrim(substring(eventname,PATINDEX('%[m,c,r,t,g,q][0-9] to [m,c,r,t,g,q][1-9]%',eventname),8))
							 else ''
						End
						+Case when (eventname like '%[^a-z,^0-9][m,c,r,g,q,t][0-9] or better%')
							 then '-'+Rtrim(substring(eventname,PATINDEX('%[m,c,r,g,q,t][0-9] or better%',eventname),2))+' or better'
							 else ''
						End
						+Case when (eventname like '%[^a-z,^0-9][m,c,r,t,g,q][0-9]/[0-9]%' and eventname not like '%3[c,r,t,g,q][0-9]/[0-9]%')
							 then '-'+Rtrim(substring(eventname,PATINDEX('%[m,c,r,t,g,q][0-9]/[0-9]%',eventname),4))
							 else ''
						End
						+Case when (eventname  like '%[^a-z,^0-9][m,c,r,t,g,q][0-9]/[m,c,r,t,g,q][0-9]%' and eventname not like '%3[m,c,r,t,g,q][0-9]/3[m,c,r,t,g,q][0-9]%')
							 then '-'+Rtrim(substring(eventname,PATINDEX('%[m,c,r,t,g,q][0-9]/[m,c,r,t,g,q][0-9]%',eventname),5))
							 else ''

						End
						+Case when (eventname  like '%tQ[0-9]%' and eventname  not like '%tQ[0-9]/tQ[0-9]%'  and eventname  not like '%tQ[0-9] or better%')
							 then '-'+Ltrim(Rtrim(substring(eventname,PATINDEX('%tQ[0-9]%',eventname),3)))
							 else ''
						End
						+Case when (eventname  like '%tQ[0-9] or better%')
							 then '-'+Ltrim(Rtrim(substring(eventname,PATINDEX('%tQ[0-9]%',eventname),3)))+' or better'
							 else ''
						End
						+Case when (eventname like '% [2,3]_[0-9] %' and eventname not like '%[2,3]_[0-9]_to_[2,3]_[0-9]%' and eventname not like '%[2,3]_[0-9] or better%')
							 then '-'+Rtrim(substring(eventname,PATINDEX('%[2,3]_[0-9]%',eventname),3))
							 else ''
						End
						+Case when (eventname like '%[2,3]_[0-9]_to_[2,3]_[0-9]%')
							 then '-'+Rtrim(substring(eventname,PATINDEX('%[2,3]_[0-9]_to_[2,3]_[0-9]%',eventname),10))
							 else ''
						End
						+Case when (eventname like '%[2,3]_[0-9]/[2,3]_[0-9]%')
							 then '-'+Rtrim(substring(eventname,PATINDEX('%[2,3]_[0-9]/[2,3]_[0-9]%',eventname),7))
							 else ''
						End
						+Case when (eventname like '%[2,3]_[0-9]/[0-9]%')
							 then '-'+Rtrim(substring(eventname,PATINDEX('%[2,3]_[0-9]/[0-9]%',eventname),5))
							 else ''
						End
						+Case when (eventname like '%[2,3]_[0-9] or better%')
							 then '-'+Rtrim(substring(eventname,PATINDEX('%[2,3]_[0-9] or better%',eventname),13))
							 else ''
						End
						+Case when (eventname  like '%[^a-z,^0-9]Mobile%'  or eventname  like '%Mobile[^a-z,^0-9]%' or eventname  like '%[^a-z,^0-9]Ms%')
							 then '-'+'Mobile Start'
							 else ''
						End
						+Case when (eventname  like '%[^a-z,^0-9]Ss%' or eventname  like '%[^a-z,^0-9]Stand%' or eventname  like '%Stand[^a-z,^0-9]%')
							 then '-'+'Stand Start'
							 else ''
						End
						+Case when (eventname like '%Ffa%' or  eventname  like '%Free for all%')
							 then '-'+'Free for all'
							 else ''
						End
						+Case when (eventname like '%Handicap%' or eventname like '%Hcp%' or eventname like '%Hcap%')
							 then '-'+'Handicap'
							 else ''
						End
						+Case when (eventname  like '%Heat_[1-9]%')
							 then '-'+'Heat'+Rtrim(substring(eventname,PATINDEX('%Heat%',eventname)+4,2))
							 else ''
						End
						+Case when (eventname like '%Heat' and  eventname not like '%[1-9][s,t,r,n][t,h,d] Heat')
							 then '-'+'Heat'
							 else ''
						End
						+Case when (eventname like '%Heat%'and eventname not like '%Heat' 
									and eventname not like '%Heat_[1-9]%' and eventname not like '%[1-9][s,t,r,n][t,h,d] Heat%')
							 then '-'+'Heat '+Substring(substring(eventname,PATINDEX('%Heat %',eventname)+5,Len(eventname)),1,PATINDEX('% %',substring(eventname,PATINDEX('%Heat %',eventname)+5,Len(eventname))+' '))
							 else ''
						End
						+Case when (eventname like '%[1-9][s,t,r,n][t,h,d] Heat%'  and  eventname not like '%Heat [0-9]%')
							 then '-'+'Heat '+Rtrim(substring(eventname,PATINDEX('%Heat%',eventname)-4,1))
							 else ''
						End
						+Case when (eventname  like '%Division_[1-9][^a-z,^0-9]%')
							 then '-'+'Division'+Rtrim(substring(eventname,PATINDEX('%Division%',eventname)+9,2))
							 else ''
						End
						+Case when (eventname like '%[^a-z]Division %'  and eventname not like '%Division' 
									and eventname not like '%Division_[1-9][^a-z,^0-9]%' and eventname not like '%[1-9][s,t,r,n][t,h,d] Division%')
							 then '-'+'Division '+
							 Substring(substring(eventname,PATINDEX('%Division %',eventname)+9,Len(eventname)),1,PATINDEX('% %',substring(eventname,PATINDEX('%Division %',eventname)+9,Len(eventname))+' '))
							 else ''
						End
						+Case when (eventname like '%[1-9][s,t,r,n][t,h,d] Division%'  and  eventname not like '%Division [0-9]%')
							 then '-'+'Division '+Rtrim(substring(eventname,PATINDEX('%Division%',eventname)-4,1))
							 else ''
						End
						+Case when (eventname like '%[^a-z]Div %' and eventname not like '%Div [0-9]%' and eventname not like '%Div'  
									and eventname not like '%Div_[1-9][^a-z,^0-9]%' and eventname not like '%[1-9][s,t,r,n][t,h,d] Div%')
							 then '-'+'Div '+
							 Substring(substring(eventname,PATINDEX('%Div %',eventname)+4,Len(eventname)),1,PATINDEX('% %',substring(eventname,PATINDEX('%Div %',eventname)+4,Len(eventname))+' '))
							 else ''
						End
						+Case when (eventname like '%[1-9][s,t,r,n][t,h,d] Div %'  and  eventname not like '%Div [0-9]%')
							 then '-'+'Div '+Rtrim(substring(eventname,PATINDEX('%Div%',eventname)-4,1))
							 else ''
						End
						+Case when (eventname  like '%Group_[1-9][^a-z]%')
							 then '-'+'Group'+Ltrim(Rtrim(substring(eventname,PATINDEX('%Group%',eventname)+5,2)))
							 else ''
						End
						+Case when (eventname  like '%Grp_[1-9][^a-z]%')
							 then '-'+'Group'+Ltrim(Rtrim(substring(eventname,PATINDEX('%Gr%',eventname)+3,2)))
							 else ''
						End
						+Case when (eventname  like '%Gr[1-9][^a-z]%')
							 then '-'+'Gr'+Ltrim(Rtrim(substring(eventname,PATINDEX('%Gr%',eventname)+2,1)))
							 else ''
						End
						+Case when (eventname  like '% Ht%' and eventname not like '%Ht [1-9]%')
							 then '-'+'Ht'
							 else ''
						End
						+Case when (eventname like '%[^a-z]Ht [1-9]%')
							 then '-'+'Ht'+Rtrim(substring(eventname,PATINDEX('%Ht [1-9]%',eventname)+3,1))
							 else ''
						End
						+Case when (eventname  like '%[1-9]YO & older%' or eventname  like '%[1-9]YO/older%')
							 then '-'+Rtrim(substring(eventname,PATINDEX('%[1-9]YO%',eventname),1))+'YO & Older'
							 else ''
						End
						+Case when (EventName  like '%[1-9]_YO+%' or EventName  like '%[1-9]YO+%' or EventName  like '%[1-9]YO plus%')
							 then '-'+Rtrim(substring(eventname,PATINDEX('%[1-9][^a-z,^0-9]YO%',eventname),1))+'YO Plus'
							 else ''
						End
						+Case when (EventName  like '%[1-9]YO%' and  EventName not like '%[1-9]-[1-9]YO%' and  EventName not like '%[1-9]-[1-9][0-9]YO%' 
									and  EventName not like '%[1-9]&[1-9]YO%' and  EventName not like '%[1-9] YO+%' and  EventName not like '%[1-9]YO%older%' 
									and  EventName not like '% [1-9] [1-9]YO%'  and EventName not like '%[1-9]YO-[1-9]YO%' 
									and  EventName not like '%[1-9][^a-z,^0-9]YO+%' and  EventName not like '%[1-9]YO plus%' )
							 then '-'+Rtrim(substring(eventname,PATINDEX('%[1-9]YO%',eventname),1)+'YO')
							 else ''
						End
						+Case when (EventName  like '%[1-9] [1-9]_YO%')
							 then '-'+Rtrim(substring(eventname,PATINDEX('%[1-9] [1-9]_YO%',eventname),3))+'YO'
							 else ''
						End
						+Case when (EventName  like '%[1-9],[1-9]&[1-9]_YO%')
							 then '-'+Rtrim(substring(eventname,PATINDEX('%[1-9],[1-9]&[1-9]_YO%',eventname),5))+'YO'
							 else ''
						End
						+Case when (EventName  like '% Year Old%')
							 then '-'+Rtrim(LTRIM(REVERSE(LEFT(REVERSE(substring(eventname,1,PATINDEX('% Year Old%',eventname)-1)), CHARINDEX(' ', REVERSE(substring(eventname,1,PATINDEX('% Year Old%',eventname)-1))+' ') - 1))))+'YO'
							 else ''
						End
						+Case when (EventName  like '%[1-9]-[1-9]YO%')
							 then '-'+Rtrim(substring(eventname,PATINDEX('%[1-9]-[1-9]YO%',eventname),3))+'YO'
							 else ''
						End
						+Case when (EventName  like '%[1-9]-[1-9][0-9]YO%')
							 then '-'+Rtrim(substring(eventname,PATINDEX('%[1-9]-[1-9][0-9]YO%',eventname),4))+'YO'
							 else ''
						End
						+Case when (EventName  like '%[1-9]YO-[1-9]YO%')
							 then '-'+Replace(Rtrim(substring(eventname,PATINDEX('%[1-9]YO-[1-9]YO%',eventname),5)),'YO','')+'YO'
							 else ''
						End
						+Case when (EventName  like '%[1-9] & [1-9]YO%')
							 then '-'+Rtrim(substring(eventname,PATINDEX('%[1-9] & [1-9]YO%',eventname),5)+'YO')
							 else ''
						End
						+Case when (EventName like '%[1-9]&[1-9]YO%' and EventName not like '%[1-9],[1-9]&[1-9]YO%')
							 then '-'+Rtrim(substring(eventname,PATINDEX('%[1-9]&[1-9]YO%',eventname),3)+'YO')
							 else ''
						End
						+Case when (EventName like '%[1-9],[1-9]&[1-9]YO%')
							 then '-'+substring(eventname,PATINDEX('%[1-9],[1-9]&[1-9]YO%',eventname),(PATINDEX('%[1-9]YO%',eventname)+1-PATINDEX('%[1-9],[1-9]&[1-9]YO%',eventname)))+'YO'
							 else ''
						End
						+Case when (eventname  like '%DIV[1-9]%' or eventname  like '%DIV [1-9]%')
							 then '-'+'Div'+Ltrim(Rtrim(substring(eventname,PATINDEX('%DIV_[1-9]%',eventname)+3,2)))
							 else ''
						End
						+Case when (eventname  like '%Listed%')
							 then '-'+'Listed'
							 else ''
						End
						+Case when ((eventname  like '%Pace%' or eventname  like '%Pce%') and eventname not like '%[1-9][0-9]__Pace%')
							 then '-'+'Pace'
							 else ''
						End
						+Case when (eventname  like '%[1-9][0-9][0-9,][ ,]Pace%')
							 then '-'+Rtrim(substring(eventname,PATINDEX('%[1-9][0-9][0-9,][ ,]Pace%',eventname),8))
							 else ''
						End
								
		where  SourceEventType='Trots' and ModifiedBatchKey=@BatchKey and (ColumnLock&Power(2,(5-1))<>Power(2,(5-1)))

		Update [$(Dimensional)].Dimension.Market
		set Grade=Replace(Grade,'One',1)
		where Grade like '%[^a-z,^0-9]One%' and SourceEventType='Trots'
		and ModifiedBatchKey=@BatchKey and (ColumnLock&Power(2,(5-1))<>Power(2,(5-1)))

		Update [$(Dimensional)].Dimension.Market
		set Grade=Replace(Grade,'Two',2)
		where Grade like '%[^a-z,^0-9]Two%' and SourceEventType='Trots'
		and ModifiedBatchKey=@BatchKey and (ColumnLock&Power(2,(5-1))<>Power(2,(5-1)))

		Update [$(Dimensional)].Dimension.Market
		set Grade=Replace(Grade,'Three',3)
		where Grade like '%[^a-z,^0-9]Three%' and SourceEventType='Trots'
		and ModifiedBatchKey=@BatchKey  and (ColumnLock&Power(2,(5-1))<>Power(2,(5-1)))

		Update [$(Dimensional)].Dimension.Market
		set Grade=Replace(Grade,'Four',4)
		where Grade like '%[^a-z,^0-9]Four%' and SourceEventType='Trots'
		and ModifiedBatchKey=@BatchKey and (ColumnLock&Power(2,(5-1))<>Power(2,(5-1)))

		Update [$(Dimensional)].Dimension.Market
		set Grade=Substring(Grade,2,Len(Grade)-1)
		where Grade<>'' and  SourceEventType='Trots'
		and ModifiedBatchKey=@BatchKey and (ColumnLock&Power(2,(5-1))<>Power(2,(5-1)))

		Update [$(Dimensional)].Dimension.Market
		set Grade=Ltrim(Replace(Replace(Grade,'(',''),')',''))
		where SourceEventType='Trots'
		and ModifiedBatchKey=@BatchKey and (ColumnLock&Power(2,(5-1))<>Power(2,(5-1)))
		 
		Update [$(Dimensional)].Dimension.Market
		set Grade='Unknown'
		where  SourceEventType='Trots' and ModifiedBatchKey=@BatchKey and Grade='' and (ColumnLock&Power(2,(5-1))<>Power(2,(5-1)))

COMMIT TRANSACTION Market_RacingGhTrots;
END TRY
BEGIN CATCH
	Rollback TRANSACTION Market_RacingGhTrots;
	declare @Error int;
	declare @ErrorMessage varchar(max);
	                           -- Log the failure at the object level
                           SET    @Error = ERROR_NUMBER()
                           SET    @ErrorMessage = ERROR_MESSAGE()
						   Raiserror(@ErrorMessage,16,1)

END CATCH

-- ================================================================================================================================================================
  --                        PART 8: Load ModifiedBy and ModifiedDate in to Market Dimension for 'Racing','Greyhounds','Trots' event types
-- ================================================================================================================================================================

Update m
Set m.RoundName = CASE WHEN RaceNumber > 0 THEN 'Race ' + CONVERT(VARCHAR,RaceNumber) ELSE 'N/A' END,
	m.RoundSequence = RaceNumber
From [$(Dimensional)].dimension.market m
left join [$(Acquisition)].IntraBet.tblEvents e on (e.EventID = m.SourceEventId and e.ToDate >= '9999-12-30') 
Where sourceeventtype IN
(
	'Jockey Challenge',
	'Racing',
	'Greys Box Challenge',
	'Trots',
	'Greyhounds',
	'Inside Or Outside',
	'Feature Races',
	'Odd Or Even'
)
and ModifiedBatchKey = @BatchKey

Update 	[$(Dimensional)].Dimension.market
set	ModifiedBy=SYSTEM_USER,
	ModifiedDate=CURRENT_TIMESTAMP
where ModifiedBatchKey=@BatchKey and SourceEventType in ('Racing','Greyhounds','Trots')

END


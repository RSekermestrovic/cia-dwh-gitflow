CREATE PROC Dimension.[Sp_PurgeData]
	@AreYouSure CHAR(1) = 'N', -- Leave the safety catch on, this must be Y for anything to be purged
	@Schema VARCHAR(255) = 'Fact' -- Comma separated list of schemas to truncate
AS
BEGIN
/*
	Developer: Aaron Jackson
	Date: 13/05/2015
	Desc: Remove all non-default data. Good for cleansing test environments.
	Exec: EXEC [Dimension].[Sp_PurgeData] @AreYouSure = 'Y'
*/

	SET NOCOUNT ON;

	IF @AreYouSure = 'Y'
	BEGIN

		DECLARE @SQL NVARCHAR(255), @TableName VARCHAR(255);

		/* Control Tables*/
		PRINT 'TRUNCATE TABLE [Control].[CompleteDate]';
		Truncate Table [$(Dimensional)].[Control].[CompleteDate]
		
		/* TRUNCATE slowly changing dimensions */
		
		PRINT 'Delete contents of TABLE [Dimension].[Account]'; --cannot Truncate due to Schemabinding on Views
		DELETE FROM [$(Dimensional)].[Dimension].[Account];
						
		PRINT 'TRUNCATE TABLE [Dimension].[Market]';
		TRUNCATE TABLE [$(Dimensional)].[Dimension].[Market];
		
		PRINT 'TRUNCATE TABLE [Dimension].[Wallet]';
		TRUNCATE TABLE [$(Dimensional)].[Dimension].[Wallet];

		/* Type 1 Dimensions */

		PRINT 'TRUNCATE TABLE [Dimension].[Contract]';
		TRUNCATE TABLE [$(Dimensional)].[Dimension].[Contract];

		/* Selected Schemas */

		DECLARE tableCursor CURSOR
		FOR 
			SELECT '[' + TABLE_SCHEMA + '].[' + TABLE_NAME + ']'
			FROM [$(Dimensional)].information_schema.tables
				 left join (
							select	Distinct O.Name
							from	[$(Dimensional)].sys.sql_dependencies as D
									inner join [$(Dimensional)].sys.all_objects as O on D.referenced_major_id = O.object_id
							where	D.class = 1
									and D.referenced_major_id != D.OBJECT_ID
							) as SchemaBound on TABLE_NAME = SchemaBound.Name
			WHERE TABLE_SCHEMA IN (SELECT Value FROM [Utility].[SplitString](@Schema))
				 and TABLE_TYPE = 'BASE TABLE'
				 and SchemaBound.Name is null
		FOR READ ONLY;

		--Open the cursor
		OPEN tableCursor

		--Get the first table name from the cursor
		FETCH NEXT FROM tableCursor INTO @TableName

		--Loop until the cursor was not able to fetch
		WHILE (@@Fetch_Status >= 0)
		BEGIN

			SET @SQL = 'TRUNCATE TABLE ' + '[$(Dimensional)].' + @TableName;
			PRINT @SQL;
			EXEC sp_executesql @SQL;

			FETCH NEXT FROM tableCursor INTO @TableName
		END

		--Get rid of the cursor
		CLOSE tableCursor
		DEALLOCATE tableCursor;

				--Second pass to deal with schemabound tables that can't be truncated
		DECLARE tableCursor2 CURSOR
		FOR 
			SELECT '[' + TABLE_SCHEMA + '].[' + TABLE_NAME + ']'
			FROM [$(Dimensional)].information_schema.tables
				 left join (
							select	Distinct O.Name
							from	[$(Dimensional)].sys.sql_dependencies as D
									inner join [$(Dimensional)].sys.all_objects as O on D.referenced_major_id = O.object_id
							where	D.class = 1
									and D.referenced_major_id != D.OBJECT_ID
							) as SchemaBound on TABLE_NAME = SchemaBound.Name
			WHERE TABLE_SCHEMA IN (SELECT Value FROM [Utility].[SplitString]('Fact,Dimension'))
				 and TABLE_TYPE = 'BASE TABLE'
				 and SchemaBound.Name is not null
		FOR READ ONLY;

		--Open the cursor
		OPEN tableCursor2

		--Get the first table name from the cursor
		FETCH NEXT FROM tableCursor2 INTO @TableName

		--Loop until the cursor was not able to fetch
		WHILE (@@Fetch_Status >= 0)
		BEGIN
			SET @SQL = 'Delete ' + '[$(Dimensional)].' + @TableName;
			PRINT @SQL;
			EXEC sp_executesql @SQL;

			SET @SQL = 'DBCC CHECKIDENT(' + '[$(Dimensional)].' + @TableName + ', RESEED, 0)'
			PRINT @SQL;

			FETCH NEXT FROM tableCursor2 INTO @TableName
		END

		--Get rid of the cursor
		CLOSE tableCursor2
		DEALLOCATE tableCursor2;

		Exec [$(Dimensional)].Dimension.sp_InitialPopALL
	END
	ELSE
		PRINT '@AreYouSure was set to ''N''. No data was purged';
END
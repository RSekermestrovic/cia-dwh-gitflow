﻿CREATE PROC [Dimension].[Sp_DimChannel]
(
	@BatchKey	int
)

AS 

BEGIN

EXEC [Control].Sp_Log	@BatchKey,'Sp_DimChannel','InsertIntoDim','Start'

INSERT INTO [$(Dimensional)].Dimension.Channel
Select	a.ChannelId,
		a.ChannelDescription,
		a.ChannelName,
		a.ChannelTypeName,
		GetDate() CreatedDate,@BatchKey CreatedBatchKey,'Sp_DimChannel' CreatedBy,GetDate() ModifiedDate,@BatchKey ModifiedBatchKey,'Sp_DimChannel' ModifiedBy
From [Stage].[Channel] a
LEFT OUTER JOIN [$(Dimensional)].Dimension.Channel b on a.ChannelId = b.ChannelId
Where BatchKey = @BatchKey and b.ChannelId is null

EXEC [Control].Sp_Log	@BatchKey,'Sp_DimChannel',NULL,'Success'

END
GO



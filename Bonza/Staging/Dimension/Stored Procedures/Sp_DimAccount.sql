CREATE PROCEDURE [Dimension].[Sp_DimAccount]
(
	@BatchKey	int,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @RowCount int

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

BEGIN TRY

	EXEC [Control].Sp_Log	@BatchKey,'Sp_DimAccount','Prepare','Start'

	SELECT	a.AccountKey, ISNULL(c.ChannelKey,-1) AccountOpenedChannelKey, ISNULL(d.DayKey,-1) LedgerOpenedDayKey, s.*
	INTO	#Account
	FROM	Stage.[Account] s
			LEFT JOIN [$(Dimensional)].Dimension.[Account] a ON a.LedgerID = s.LedgerId AND a.LedgerSource = s.LedgerSource
			LEFT JOIN [$(Dimensional)].Dimension.[DayZone] d ON d.MasterDayText = s.LedgerOpenedDayText AND d.FromDate <= s.LedgerOpenedDate AND d.ToDate > s.LedgerOpenedDate
			LEFT JOIN [$(Dimensional)].Dimension.[Channel] c ON c.ChannelId = s.AccountOpenedChannelId
	WHERE	s.BatchKey = @BatchKey
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,'Sp_DimAccount','Update','Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	d
	SET		LedgerOpenedDayKey = a.LedgerOpenedDayKey,
			AccountOpenedChannelKey = a.AccountOpenedChannelKey,
			LegacyLedgerID = a.LegacyLedgerID, 
			LedgerSequenceNumber = a.LedgerSequenceNumber, 
			LedgerTypeID = a.LedgerTypeID,
			LedgerType = a.LedgerType, 
			LedgerOpenedDate = a.LedgerOpenedDate, 
			LedgerOpenedDateOrigin = a.LedgerOpenedDateOrigin,
			AccountID = a.AccountID,
			AccountSource = a.AccountSource,
			PIN = a.PIN,
			UserName = a.UserName,
			ExactTargetID = a.ExactTargetID,
			AccountFlags = a.AccountFlags,
			StatementMethod = a.StatementMethod,
			StatementFrequency = a.StatementFrequency,
			AccountTitle = a.AccountTitle,
			AccountSurname = a.AccountSurname,
			AccountFirstName = a.AccountFirstName,
			AccountMiddleName = a.AccountMiddleName,
			AccountGender = a.AccountGender,
			AccountDOB = a.AccountDOB,
			HasHomeAddress = a.HasHomeAddress,
			HomeAddress1 = a.HomeAddress1,
			HomeAddress2 = a.HomeAddress2,
			HomeSuburb = a.HomeSuburb,
			HomeStateCode = a.HomeStateCode,
			HomeState = a.HomeState,
			HomePostCode = a.HomePostCode,
			HomeCountryCode = a.HomeCountryCode,
			HomeCountry = a.HomeCountry,
			HomePhone = a.HomePhone,
			HomeMobile = a.HomeMobile,
			HomeFax = a.HomeFax,
			HomeEmail = a.HomeEmail,
			HomeAlternateEmail = a.HomeAlternateEmail,
			HasPostalAddress = a.HasPostalAddress,
			PostalAddress1 = a.PostalAddress1,
			PostalAddress2 = a.PostalAddress2,
			PostalSuburb = a.PostalSuburb,
			PostalStateCode = a.PostalStateCode,
			PostalState = a.PostalState,
			PostalPostCode = a.PostalPostCode,
			PostalCountryCode= a.PostalCountryCode,
			PostalCountry = a.PostalCountry,
			PostalPhone = a.PostalPhone,
			PostalMobile = a.PostalMobile,
			PostalFax = a.PostalFax,
			PostalEmail = a.PostalEmail,
			PostalAlternateEmail = a.PostalAlternateEmail,
			HasBusinessAddress = a.HasBusinessAddress,
			BusinessAddress1 = a.BusinessAddress1,
			BusinessAddress2 = a.BusinessAddress2,
			BusinessSuburb = a.BusinessSuburb,
			BusinessStateCode = a.BusinessStateCode,
			BusinessState = a.BusinessState,
			BusinessPostCode = a.BusinessPostCode,
			BusinessCountryCode = a.BusinessCountryCode,
			BusinessCountry = a.BusinessCountry,
			BusinessPhone = a.BusinessPhone,
			BusinessMobile = a.BusinessMobile,
			BusinessFax = a.BusinessFax,
			BusinessEmail = a.BusinessEmail,
			BusinessAlternateEmail = a.BusinessAlternateEmail,
			HasOtherAddress = a.HasOtherAddress,
			OtherAddress1 = a.OtherAddress1,
			OtherAddress2 = a.OtherAddress2,
			OtherSuburb = a.OtherSuburb,
			OtherStateCode = a.OtherStateCode,
			OtherState = a.OtherState,
			OtherPostCode = a.OtherPostCode,
			OtherCountryCode = a.OtherCountryCode,
			OtherCountry = a.OtherCountry,
			OtherPhone = a.OtherPhone,
			OtherMobile = a.OtherMobile,
			OtherFax = a.OtherFax,
			OtherEmail = a.OtherEmail,
			OtherAlternateEmail = a.OtherAlternateEmail,
			SourceBTag2 = a.SourceBTag2,
			SourceTrafficSource = a.SourceTrafficSource,
			SourceRefURL = a.SourceRefURL,
			SourceCampaignID = a.SourceCampaignID,
			SourceKeywords = a.SourceKeywords,
			FromDate = a.FromDate,
			ModifiedDate = CURRENT_TIMESTAMP,
			ModifiedBatchKey = @BatchKey,
			ModifiedBy = 'Sp_DimAccount'
	FROM	[$(Dimensional)].Dimension.[Account] d
			INNER JOIN #Account a on a.AccountKey = d.AccountKey
	WHERE	a.AccountKey IS NOT NULL
	OPTION (RECOMPILE)

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,'Sp_DimAccount','Insert','Start', @RowsProcessed = @RowCount

	INSERT [$(Dimensional)].Dimension.Account 
		(	CompanyKey, LedgerOpenedDayKey, AccountOpenedDayKey, AccountOpenedChannelKey,
			LedgerID, LedgerSource, LegacyLedgerID, LedgerSequenceNumber, LedgerTypeID, LedgerType, LedgerGrouping, LedgerOpenedDate, LedgerOpenedDateOrigin,
			AccountID, AccountSource, PIN, UserName, ExactTargetID, AccountFlags, AccountTypeCode, AccountType,
			AccountOpenedDate, BDM, VIPCode, VIP, BTag2, AffiliateID, AffiliateName, SiteID, TrafficSource, RefURL, CampaignID, Keywords,
			StatementMethod, StatementFrequency,
			CustomerID, CustomerSource,
			Title, Surname, FirstName, MiddleName, Gender, DOB, 
			Address1, Address2, Suburb, StateCode, State, PostCode, CountryCode, Country, GeographicalLocation, AustralianClient,
			Phone, Mobile, Fax, Email,
			AccountTitle, AccountSurname, AccountFirstName, AccountMiddleName, AccountGender, AccountDOB,
			HasHomeAddress, HomeAddress1, HomeAddress2, HomeSuburb, HomeStateCode, HomeState, HomePostCode, HomeCountryCode, HomeCountry,
			HomePhone, HomeMobile, HomeFax, HomeEmail, HomeAlternateEmail, 
			HasPostalAddress, PostalAddress1, PostalAddress2, PostalSuburb,PostalStateCode, PostalState, PostalPostCode, PostalCountryCode, PostalCountry,
			PostalPhone, PostalMobile, PostalFax, PostalEmail, PostalAlternateEmail, 
			HasBusinessAddress,	BusinessAddress1, BusinessAddress2, BusinessSuburb, BusinessStateCode, BusinessState, BusinessPostCode, BusinessCountryCode, BusinessCountry,
			BusinessPhone, BusinessMobile, BusinessFax, BusinessEmail, BusinessAlternateEmail, 
			HasOtherAddress, OtherAddress1, OtherAddress2, OtherSuburb, OtherStateCode, OtherState, OtherPostCode, OtherCountryCode, OtherCountry,
			OtherPhone, OtherMobile, OtherFax, OtherEmail, OtherAlternateEmail, 
			SourceBTag2, SourceTrafficSource, SourceRefURL, SourceCampaignID, SourceKeywords, 
			FirstDate, FromDate, ToDate, CreatedDate, CreatedBatchKey, CreatedBy, ModifiedDate, ModifiedBatchKey, ModifiedBy
		)
	SELECT	-1 CompanyKey, LedgerOpenedDayKey, -1 AccountOpenedDayKey, AccountOpenedChannelKey,
			LedgerID, LedgerSource, LegacyLedgerID, LedgerSequenceNumber, LedgerTypeID, LedgerType, 'Pending' LedgerGrouping, LedgerOpenedDate, LedgerOpenedDateOrigin,
			AccountID, AccountSource, PIN, UserName, ExactTargetID, AccountFlags, 'P' AccountTypeCode, 'Pending' AccountType,
			NULL AccountOpenedDate, 'Pending' BDM, NULL VIPCode, 'Pending' VIP, 'Pending' BTag2, 'Pending' AffiliateID, 'Pending' AffiliateName, 'Pending' SiteID, 'Pending' TrafficSource, 'Pending' RefURL, 'Pending' CampaignID, 'Pending' Keywords,
			StatementMethod, StatementFrequency,
			-1 CustomerID, 'PDG' CustomerSource,
			'Pending' Title, 'Pending' Surname, 'Pending' FirstName, 'Pending' MiddleName, 'P' Gender, NULL DOB, 
			'Pending' Address1, 'Pending' Address2, 'Pending' Suburb, 'PDG' StateCode, 'Pending' State, 'Pending' PostCode, 'PDG' CountryCode, 'Pending' Country, NULL GeographicalLocation, 'PDG' AustralianClient,
			'Pending' Phone, 'Pending' Mobile, 'Pending' Fax, 'Pending' Email,
			AccountTitle, AccountSurname, AccountFirstName, AccountMiddleName, AccountGender, AccountDOB,
			HasHomeAddress, HomeAddress1, HomeAddress2, HomeSuburb, HomeStateCode, HomeState, HomePostCode, HomeCountryCode, HomeCountry,
			HomePhone, HomeMobile, HomeFax, HomeEmail, HomeAlternateEmail, 
			HasPostalAddress, PostalAddress1, PostalAddress2, PostalSuburb,PostalStateCode, PostalState, PostalPostCode, PostalCountryCode, PostalCountry,
			PostalPhone, PostalMobile, PostalFax, PostalEmail, PostalAlternateEmail, 
			HasBusinessAddress,	BusinessAddress1, BusinessAddress2, BusinessSuburb, BusinessStateCode, BusinessState, BusinessPostCode, BusinessCountryCode, BusinessCountry,
			BusinessPhone, BusinessMobile, BusinessFax, BusinessEmail, BusinessAlternateEmail, 
			HasOtherAddress, OtherAddress1, OtherAddress2, OtherSuburb, OtherStateCode, OtherState, OtherPostCode, OtherCountryCode, OtherCountry,
			OtherPhone, OtherMobile, OtherFax, OtherEmail, OtherAlternateEmail, 
			SourceBTag2, SourceTrafficSource, SourceRefURL, SourceCampaignID, SourceKeywords, 
			FirstDate, FromDate, '9999-12-31' ToDate, CURRENT_TIMESTAMP CreatedDate, @BatchKey CreatedBatchKey, 'Sp_DimAccount' CreatedBy, CURRENT_TIMESTAMP ModifiedDate, @BatchKey ModifiedBatchKey, 'Sp_DimAccount' ModifiedBy
	FROM	#Account 
	WHERE	AccountKey IS NULL
	OPTION (RECOMPILE)
	
	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,'Sp_DimAccount',NULL,'Success', @RowsProcessed = @RowCount	

END TRY
BEGIN CATCH

	declare @Error int = ERROR_NUMBER();
	declare @ErrorMessage varchar(max)  = ERROR_MESSAGE();
	
	 -- Log the failure at the object level
	EXEC [Control].Sp_Log @BatchKey, 'Sp_DimAccount', NULL, 'Failed', @ErrorMessage;
	THROW;
END CATCH

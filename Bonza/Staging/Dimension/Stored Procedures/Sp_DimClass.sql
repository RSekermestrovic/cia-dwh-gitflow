﻿CREATE PROC [Dimension].[Sp_DimClass]
(
	@BatchKey	int
)
WITH RECOMPILE
AS 
BEGIN TRY


INSERT INTO [$(Dimensional)].Dimension.Class
           ([ClassId]
           ,[Source]
           ,[SourceClassName]
           ,[ClassCode]
           ,[ClassName]
           ,[ClassIcon]
           ,[SuperclassKey]
           ,[SuperclassName]
           ,[FromDate]
           ,[ToDate]
		   ,[CreatedDate]
		   ,[CreatedBatchKey]
		   ,[CreatedBy]
		   ,[ModifiedDate]
		   ,[ModifiedBatchKey]
		   ,[ModifiedBy]
		   )
Select a.[ClassId]
      ,a.[Source]
      ,a.[SourceclassName]
      ,a.[ClassCode]
      ,a.[ClassName]
      ,a.[classIcon]
      ,a.[SuperClassKey]
      ,a.[SuperClassName]
      ,a.[FromDate]
      ,a.[ToDate] 
	  , getdate()
	  , @BatchKey
	  , 'Sp_DimClass'
	  , getdate()
	  , @BatchKey
	  , 'Sp_DimClass'
From [Stage].[Class] a
LEFT OUTER JOIN [$(Dimensional)].Dimension.Class b on a.ClassId = b.ClassId
Where a.BatchKey = @BatchKey and b.ClassId is null

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_DimClass', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
GO
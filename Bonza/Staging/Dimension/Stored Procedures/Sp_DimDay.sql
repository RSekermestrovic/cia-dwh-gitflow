﻿Create Procedure [Dimension].[Sp_DimDay]
(
	@BatchKey INT
)
WITH RECOMPILE
AS 
BEGIN TRY

	DECLARE @ProcName VARCHAR(255) = (SELECT OBJECT_NAME(@@PROCID));

	EXEC [Control].Sp_Log @BatchKey, @ProcName, 'Start','Start';

--Test Code
--exec Dimension.Sp_DimDay

Truncate Table Stage.Day

Set DateFirst 1

Declare @LastDayKey int
Set @LastDayKey = (Select Max(DayKey) from [$(Dimensional)].[Dimension].[Day] where Daykey not in (0 ,-1))

Declare @LastDay date
Set @LastDay = (Select DayDate from [$(Dimensional)].[Dimension].[Day] where Daykey = @LastDayKey)

--Test Section
	--If you wish to test from a certain date (rather than the last), you can manually set the parameters here
	--Set @LastDayKey = 19910101
	--set @LastDay = '1991-01-01'

While @LastDay < DateAdd (Year, 2, CONVERT(DATE,GetDate()))
Begin

	Declare @Day date
	Set @Day = DateAdd (Day,1,@LastDay)
	Print @Day

	Declare @FirstMonday date
	Set @FirstMonday = DateAdd(DAY, (@@DATEFIRST - DATEPART(WEEKDAY, DATEADD(YEAR, DATEPART(Year,@Day) - 1900, 0)) + (8 - @@DATEFIRST) * 2) % 7, DATEADD(YEAR, DATEPART(Year,@Day) - 1900, 0))

	Declare @FiscalStart date
	Set @FiscalStart = ( Select	FiscalStart From Reference.FiscalYears Where @Day between FiscalStart and FiscalEnd)

	Declare @FiscalEnd date
	Set @FiscalEnd = ( Select	FiscalEnd From Reference.FiscalYears Where @Day between FiscalStart and FiscalEnd)

	Declare @FiscalYear int
	Set @FiscalYear = ( Select	[Year] From Reference.FiscalYears Where @Day between FiscalStart and FiscalEnd)

	Print 'Insert Record'
	Insert into Stage.[Day] ([DayKey]) Values (Cast (Convert (varchar(12),@Day,112) as int))

	Print 'Update Cycle 1'
	Update	Stage.[Day] 
	Set		[DayName] = Replace (Convert (varchar(11),@Day,13), ' ', '-')
		  , [DayText] = Replace (Convert (varchar(11),@Day,102), '.', '-')
		  , [DayDate] = @Day
		  , [DayOfWeekText] = CASE
				When DatePart(WEEKDAY,@Day) = 1 then 'Mon'
				When DatePart(WEEKDAY,@Day) = 2 then 'Tue'
				When DatePart(WEEKDAY,@Day) = 3 then 'Wed'
				When DatePart(WEEKDAY,@Day) = 4 then 'Thu'
				When DatePart(WEEKDAY,@Day) = 5 then 'Fri'
				When DatePart(WEEKDAY,@Day) = 6 then 'Sat'
				When DatePart(WEEKDAY,@Day) = 7 then 'Sun'
				Else 'ERR'
				End
		  , [DayOfWeekNumber] = DatePart(WEEKDAY,@Day) 
		  , [DayOfMonthNumber] = DatePart(Day,@Day) 
		  , [DayOfQuarterNumber] = DATEDIFF(Day, DATEADD(Quarter, DATEDIFF(Quarter, 0, @Day),0), @Day) +1
		  , [DayOfYearNumber] = DatePart(DayofYear, @Day)
		  , [DayOfTaxQuarterNumber] = DATEDIFF(Day, DATEADD(Quarter, DATEDIFF(Quarter, 0, @Day),0), @Day) +1
		  , [DayOfTaxYearNumber] =  DateDiff (Day, DATEADD(year,DATEDIFF(month,'19010701',@Day)/12,'19010701'), @Day) + 1 
		  , [DayOfFiscalWeekNumber] = CASE
				When DatePart(WEEKDAY,@Day) = 1 then 6
				When DatePart(WEEKDAY,@Day) = 2 then 7
				When DatePart(WEEKDAY,@Day) = 3 then 1
				When DatePart(WEEKDAY,@Day) = 4 then 2
				When DatePart(WEEKDAY,@Day) = 5 then 3
				When DatePart(WEEKDAY,@Day) = 6 then 4
				When DatePart(WEEKDAY,@Day) = 7 then 5
				Else 0
				End
		  , [DayOfFiscalYearNumber] = DateDiff (Day, @FiscalStart, @Day) +1
		  , [WeekKey] = Cast(DatePart(Year,@Day) as char(4)) + Right ('0' + Cast (DATEPART(Week,@Day) as varchar(2)),2)
		  , [WeekName] = 'W' + Right (Cast(DatePart(Year,@Day) as char(4)) + Right ('0' + Cast (DATEPART(Week,@Day) as varchar(2)),2),2) + '-' + Left (Cast(DatePart(Year,@Day) as char(4)) + Right ('0' + Cast (DATEPART(Week,@Day) as varchar(2)),2),4)
		  , [WeekText] = 'W' + Right (Cast(DatePart(Year,@Day) as char(4)) + Right ('0' + Cast (DATEPART(Week,@Day) as varchar(2)),2),2) + '-' + Left (Cast(DatePart(Year,@Day) as char(4)) + Right ('0' + Cast (DATEPART(Week,@Day) as varchar(2)),2),4)
		  , [FiscalYearStartDate] = @FiscalStart
		  , [FiscalYearEndDate] = @FiscalEnd
		  , [FiscalYearDays] = DateDiff (Day,@FiscalStart,@FiscalEnd) + 1 
		  , [WeekStartDate] = CASE
								When @Day < @FirstMonday then Cast (DATEPART(Year,@Day) as char(4)) + '-01-01'
								when @Day = @FirstMonday then @FirstMonday
								When @FirstMonday = Cast (DATEPART(Year,@Day) as char(4)) + '-01-01' then DateAdd(day,(DATEPART(Week,@Day)-1)*7,@FirstMonday)
								else DateAdd(day,(DATEPART(Week,@Day)-2)*7,@FirstMonday)
								end
		  , [WeekOfYearText] = 'W' + Right ('0' + cast (DATEPART(Week,@Day) as varchar(2)),2)
		  , [WeekOfYearNumber] = DatePart(WEEK,@Day)
		  , [WeekYearNumber] = DATEPART(Year,@Day)
		  , [MonthKey] = Replace (Left (@Day, 7), '-', '')
		  , [MonthName] = Right (Replace (Convert(varchar(30),@Day, 106), ' ', '-'), 8)
		  , [MonthText] = Right (Replace (Convert(varchar(30),@Day, 106), ' ', '-'), 8)
		  , MonthStartDate = Cast (DatePart(Year, @Day) as char(4)) + '-' + Right (('0' + Cast (DatePart(Month, @Day) as varchar(2))), 2) + '-01'
		  , [MonthOfYearNumber] = Month(@Day)
		  , [QuarterKey] = Case
							When Month(@Day) in (1,2,3) then cast (Year(@Day) as char(4)) + '1' 
							When Month(@Day) in (4,5,6) then cast (Year(@Day) as char(4)) + '2' 
							When Month(@Day) in (7,8,9) then cast (Year(@Day) as char(4)) + '3' 
							When Month(@Day) in (10,11,12) then cast (Year(@Day) as char(4)) + '4' 
							end
		  , [QuarterName] = Case
							When Month(@Day) in (1,2,3) then 'Q1-' + cast (Year(@Day) as char(4))
							When Month(@Day) in (4,5,6) then 'Q2-' + cast (Year(@Day) as char(4))
							When Month(@Day) in (7,8,9) then 'Q3-' + cast (Year(@Day) as char(4))
							When Month(@Day) in (10,11,12) then 'Q4-' + cast (Year(@Day) as char(4)) 
							end
		  , [QuarterText] = Case
							 When Month(@Day) in (1,2,3) then 'Q1-' + cast (Year(@Day) as char(4))
							 When Month(@Day) in (4,5,6) then 'Q2-' + cast (Year(@Day) as char(4))
							 When Month(@Day) in (7,8,9) then 'Q3-' + cast (Year(@Day) as char(4))
							 When Month(@Day) in (10,11,12) then 'Q4-' + cast (Year(@Day) as char(4)) 
							 end
		  , [QuarterStartDate] = Case
									When Month(@Day) in (1,2,3) then cast (Year(@Day) as char(4)) + '-01-01'
									When Month(@Day) in (4,5,6) then cast (Year(@Day) as char(4)) + '-04-01'
									When Month(@Day) in (7,8,9) then cast (Year(@Day) as char(4)) + '-07-01'
									When Month(@Day) in (10,11,12) then cast (Year(@Day) as char(4)) + '-10-01'
									end
		  , [QuarterEndDate] = Case
								When Month(@Day) in (1,2,3) then cast (Year(@Day) as char(4)) + '-03-31'
								When Month(@Day) in (4,5,6) then cast (Year(@Day) as char(4)) + '-06-30'
								When Month(@Day) in (7,8,9) then cast (Year(@Day) as char(4)) + '-09-30'
								When Month(@Day) in (10,11,12) then cast (Year(@Day) as char(4)) + '-12-31'
								end
      
		  , [QuarterOfYearText] = Case
									When Month(@Day) in (1,2,3) then 'Q1'
									When Month(@Day) in (4,5,6) then 'Q2'
									When Month(@Day) in (7,8,9) then 'Q3'
									When Month(@Day) in (10,11,12) then 'Q4'
									end
		  , [QuarterOfYearNumber] = Case
									 When Month(@Day) in (1,2,3) then 1
									 When Month(@Day) in (4,5,6) then 2
									 When Month(@Day) in (7,8,9) then 3
									 When Month(@Day) in (10,11,12) then 4
									 end
		  , [YearKey] = DatePart (Year, @Day)
		  , [YearName] = DatePart (Year, @Day)
		  , [YearText] = DatePart (Year, @Day)
		  , [YearNumber] = Cast (DatePart (Year, @Day) as int) 
		  , [YearStartDate] = Cast (DatePart(Year,@Day) as Char(4)) + '-01-01'
		  , [YearEndDate] = Cast (DatePart(Year,@Day) as Char(4)) + '-12-31'
		  , [YearDays] = DateDiff(Day, Cast (DatePart(Year,@Day) as Char(4)) + '-01-01', Cast (DatePart(Year,@Day) as Char(4)) + '-12-31') +1
		  , [TaxQuarterKey] = Case
								When Month(@Day) in (1,2,3) then cast (Year(@Day) -1 as char(4)) + '3'
								When Month(@Day) in (4,5,6) then cast (Year(@Day) -1 as char(4)) + '4'
								When Month(@Day) in (7,8,9) then cast (Year(@Day) as char(4)) + '1'
								When Month(@Day) in (10,11,12) then cast (Year(@Day) as char(4)) + '2'
								end
		  , [TaxQuarterName] = Case
								When Month(@Day) in (1,2,3) then 'Q3-' + Right (cast (Year(@Day) -1 as char(4)),2) + '/' + Right(cast (Year(@Day) as char(4)),2) 
								When Month(@Day) in (4,5,6) then 'Q4-' + Right (cast (Year(@Day) -1 as char(4)),2) + '/' + Right(cast (Year(@Day) as char(4)),2)
								When Month(@Day) in (7,8,9) then 'Q1-' + Right (cast (Year(@Day) as char(4)),2) + '/' + Right(cast (Year(@Day) +1 as char(4)),2)
								When Month(@Day) in (10,11,12) then 'Q2-' + Right (cast (Year(@Day) as char(4)),2) + '/' + Right(cast (Year(@Day) +1 as char(4)),2)
								end
		  , [TaxQuarterText] = Case
								When Month(@Day) in (1,2,3) then 'Q3-' + Right (cast (Year(@Day) -1 as char(4)),2) + '/' + Right(cast (Year(@Day) as char(4)),2) 
								When Month(@Day) in (4,5,6) then 'Q4-' + Right (cast (Year(@Day) -1 as char(4)),2) + '/' + Right(cast (Year(@Day) as char(4)),2)
								When Month(@Day) in (7,8,9) then 'Q1-' + Right (cast (Year(@Day) as char(4)),2) + '/' + Right(cast (Year(@Day) +1 as char(4)),2)
								When Month(@Day) in (10,11,12) then 'Q2-' + Right (cast (Year(@Day) as char(4)),2) + '/' + Right(cast (Year(@Day) +1 as char(4)),2)
								end
		  , [TaxYearKey] = Case
							When Month(@Day) in (1,2,3) then cast (Year(@Day) -1 as char(4))
							When Month(@Day) in (4,5,6) then cast (Year(@Day) -1 as char(4))
							When Month(@Day) in (7,8,9) then cast (Year(@Day) as char(4))
							When Month(@Day) in (10,11,12) then cast (Year(@Day) as char(4))
							end
		  , [TaxYearName] = Case
							 When Month(@Day) in (1,2,3) then Right (cast (Year(@Day) -1 as char(4)),2) + '/' + Right(cast (Year(@Day) as char(4)),2) 
							 When Month(@Day) in (4,5,6) then Right (cast (Year(@Day) -1 as char(4)),2) + '/' + Right(cast (Year(@Day) as char(4)),2)
							 When Month(@Day) in (7,8,9) then Right (cast (Year(@Day) as char(4)),2) + '/' + Right(cast (Year(@Day) +1 as char(4)),2)
							 When Month(@Day) in (10,11,12) then Right (cast (Year(@Day) as char(4)),2) + '/' + Right(cast (Year(@Day) +1 as char(4)),2)
							 end
		  , [TaxYearText] = Case
							 When Month(@Day) in (1,2,3) then Right (cast (Year(@Day) -1 as char(4)),2) + '/' + Right(cast (Year(@Day) as char(4)),2) 
							 When Month(@Day) in (4,5,6) then Right (cast (Year(@Day) -1 as char(4)),2) + '/' + Right(cast (Year(@Day) as char(4)),2)
							 When Month(@Day) in (7,8,9) then Right (cast (Year(@Day) as char(4)),2) + '/' + Right(cast (Year(@Day) +1 as char(4)),2)
							 When Month(@Day) in (10,11,12) then Right (cast (Year(@Day) as char(4)),2) + '/' + Right(cast (Year(@Day) +1 as char(4)),2)
							 end
		  , [TaxYearNumber] = Case
							When Month(@Day) in (1,2,3) then cast (Year(@Day) -1 as char(4))
							When Month(@Day) in (4,5,6) then cast (Year(@Day) -1 as char(4))
							When Month(@Day) in (7,8,9) then cast (Year(@Day) as char(4))
							When Month(@Day) in (10,11,12) then cast (Year(@Day) as char(4))
							end
		  , [FiscalWeekKey] = CASE
								When DateDiff(Day, @FiscalStart, @Day) +1 <= 7 then Cast (@FiscalYear as char(4)) + '01'
								When DateDiff(Day, @FiscalStart, @Day) +1 between 8 and 14 then Cast (@FiscalYear as char(4)) + '02'
								When DateDiff(Day, @FiscalStart, @Day) +1 between 15 and 21 then Cast (@FiscalYear as char(4)) + '03'
								When DateDiff(Day, @FiscalStart, @Day) +1 between 22 and 28 then Cast (@FiscalYear as char(4)) + '04'
								When DateDiff(Day, @FiscalStart, @Day) +1 between 29 and 35 then Cast (@FiscalYear as char(4)) + '05'
								When DateDiff(Day, @FiscalStart, @Day) +1 between 36 and 42 then Cast (@FiscalYear as char(4)) + '06'
								When DateDiff(Day, @FiscalStart, @Day) +1 between 43 and 49 then Cast (@FiscalYear as char(4)) + '07'
								When DateDiff(Day, @FiscalStart, @Day) +1 between 50 and 56 then Cast (@FiscalYear as char(4)) + '08'
								When DateDiff(Day, @FiscalStart, @Day) +1 between 57 and 63 then Cast (@FiscalYear as char(4)) + '09'
								When DateDiff(Day, @FiscalStart, @Day) +1 between 64 and 70 then Cast (@FiscalYear as char(4)) + '10'
								When DateDiff(Day, @FiscalStart, @Day) +1 between 71 and 77 then Cast (@FiscalYear as char(4)) + '11'
								When DateDiff(Day, @FiscalStart, @Day) +1 between 78 and 84 then Cast (@FiscalYear as char(4)) + '12'
								When DateDiff(Day, @FiscalStart, @Day) +1 between 85 and 91 then Cast (@FiscalYear as char(4)) + '13'
								When DateDiff(Day, @FiscalStart, @Day) +1 between 92 and 98 then Cast (@FiscalYear as char(4)) + '14'
								When DateDiff(Day, @FiscalStart, @Day) +1 between 99 and 105 then Cast (@FiscalYear as char(4)) + '15'
								When DateDiff(Day, @FiscalStart, @Day) +1 between 106 and 112 then Cast (@FiscalYear as char(4)) + '16'
								When DateDiff(Day, @FiscalStart, @Day) +1 between 113 and 119 then Cast (@FiscalYear as char(4)) + '17'
								When DateDiff(Day, @FiscalStart, @Day) +1 between 120 and 126 then Cast (@FiscalYear as char(4)) + '18'
								When DateDiff(Day, @FiscalStart, @Day) +1 between 127 and 133 then Cast (@FiscalYear as char(4)) + '19'
								When DateDiff(Day, @FiscalStart, @Day) +1 between 134 and 140 then Cast (@FiscalYear as char(4)) + '20'
								When DateDiff(Day, @FiscalStart, @Day) +1 between 141 and 147 then Cast (@FiscalYear as char(4)) + '21'
								When DateDiff(Day, @FiscalStart, @Day) +1 between 148 and 154 then Cast (@FiscalYear as char(4)) + '22'
								When DateDiff(Day, @FiscalStart, @Day) +1 between 155 and 161 then Cast (@FiscalYear as char(4)) + '23'
								When DateDiff(Day, @FiscalStart, @Day) +1 between 162 and 168 then Cast (@FiscalYear as char(4)) + '24'
								When DateDiff(Day, @FiscalStart, @Day) +1 between 169 and 175 then Cast (@FiscalYear as char(4)) + '25'
								When DateDiff(Day, @FiscalStart, @Day) +1 between 176 and 182 then Cast (@FiscalYear as char(4)) + '26'
								When DateDiff(Day, @FiscalStart, @Day) +1 between 183 and 189 then Cast (@FiscalYear as char(4)) + '27'
								When DateDiff(Day, @FiscalStart, @Day) +1 between 190 and 196 then Cast (@FiscalYear as char(4)) + '28'
								When DateDiff(Day, @FiscalStart, @Day) +1 between 197 and 203 then Cast (@FiscalYear as char(4)) + '29'
								When DateDiff(Day, @FiscalStart, @Day) +1 between 204 and 210 then Cast (@FiscalYear as char(4)) + '30'
								When DateDiff(Day, @FiscalStart, @Day) +1 between 211 and 217 then Cast (@FiscalYear as char(4)) + '31'
								When DateDiff(Day, @FiscalStart, @Day) +1 between 218 and 224 then Cast (@FiscalYear as char(4)) + '32'
								When DateDiff(Day, @FiscalStart, @Day) +1 between 225 and 231 then Cast (@FiscalYear as char(4)) + '33'
								When DateDiff(Day, @FiscalStart, @Day) +1 between 232 and 238 then Cast (@FiscalYear as char(4)) + '34'
								When DateDiff(Day, @FiscalStart, @Day) +1 between 239 and 245 then Cast (@FiscalYear as char(4)) + '35'
								When DateDiff(Day, @FiscalStart, @Day) +1 between 246 and 252 then Cast (@FiscalYear as char(4)) + '36'
								When DateDiff(Day, @FiscalStart, @Day) +1 between 253 and 259 then Cast (@FiscalYear as char(4)) + '37'
								When DateDiff(Day, @FiscalStart, @Day) +1 between 260 and 266 then Cast (@FiscalYear as char(4)) + '38'
								When DateDiff(Day, @FiscalStart, @Day) +1 between 267 and 273 then Cast (@FiscalYear as char(4)) + '39'
								When DateDiff(Day, @FiscalStart, @Day) +1 between 274 and 280 then Cast (@FiscalYear as char(4)) + '40'
								When DateDiff(Day, @FiscalStart, @Day) +1 between 281 and 287 then Cast (@FiscalYear as char(4)) + '41'
								When DateDiff(Day, @FiscalStart, @Day) +1 between 288 and 294 then Cast (@FiscalYear as char(4)) + '42'
								When DateDiff(Day, @FiscalStart, @Day) +1 between 295 and 301 then Cast (@FiscalYear as char(4)) + '43'
								When DateDiff(Day, @FiscalStart, @Day) +1 between 302 and 308 then Cast (@FiscalYear as char(4)) + '44'
								When DateDiff(Day, @FiscalStart, @Day) +1 between 309 and 315 then Cast (@FiscalYear as char(4)) + '45'
								When DateDiff(Day, @FiscalStart, @Day) +1 between 316 and 322 then Cast (@FiscalYear as char(4)) + '46'
								When DateDiff(Day, @FiscalStart, @Day) +1 between 323 and 329 then Cast (@FiscalYear as char(4)) + '47'
								When DateDiff(Day, @FiscalStart, @Day) +1 between 330 and 336 then Cast (@FiscalYear as char(4)) + '48'
								When DateDiff(Day, @FiscalStart, @Day) +1 between 337 and 343 then Cast (@FiscalYear as char(4)) + '49'
								When DateDiff(Day, @FiscalStart, @Day) +1 between 344 and 350 then Cast (@FiscalYear as char(4)) + '50'
								When DateDiff(Day, @FiscalStart, @Day) +1 between 351 and 357 then Cast (@FiscalYear as char(4)) + '51'
								When DateDiff(Day, @FiscalStart, @Day) +1 between 358 and 364 then Cast (@FiscalYear as char(4)) + '52'
								else Cast (@FiscalYear as char(4)) + '53'
								end
		  , [FiscalWeekStartDate] = CASE
									 When DatePart(WEEKDAY,@Day) = 1 then DateAdd(Day,-5,@Day)
									 When DatePart(WEEKDAY,@Day) = 2 then DateAdd(Day,-6,@Day)
									 When DatePart(WEEKDAY,@Day) = 3 then @Day
									 When DatePart(WEEKDAY,@Day) = 4 then DateAdd(Day,-1,@Day)
									 When DatePart(WEEKDAY,@Day) = 5 then DateAdd(Day,-2,@Day)
									 When DatePart(WEEKDAY,@Day) = 6 then DateAdd(Day,-3,@Day)
									 When DatePart(WEEKDAY,@Day) = 7 then DateAdd(Day,-4,@Day)
									 else 'ERROR'
									 End
		  , [FiscalWeekEndDate] = CASE
										When DatePart(WEEKDAY,@Day) = 1 then DateAdd(Day,1,@Day)
										When DatePart(WEEKDAY,@Day) = 2 then @Day
										When DatePart(WEEKDAY,@Day) = 3 then DateAdd(Day,6,@Day)
										When DatePart(WEEKDAY,@Day) = 4 then DateAdd(Day,5,@Day)
										When DatePart(WEEKDAY,@Day) = 5 then DateAdd(Day,4,@Day)
										When DatePart(WEEKDAY,@Day) = 6 then DateAdd(Day,3,@Day)
										When DatePart(WEEKDAY,@Day) = 7 then DateAdd(Day,2,@Day)
										else 'ERROR'
										End
		  , [FiscalMonthKey] = CASE
										When DateDiff(Day, @FiscalStart, @Day) +1 <= 28 then Cast (@FiscalYear as char(4)) + '01'
										When DateDiff(Day, @FiscalStart, @Day) +1 between 29 and 56 then Cast (@FiscalYear as char(4)) + '02'
										When DateDiff(Day, @FiscalStart, @Day) +1 between 57 and 91 then Cast (@FiscalYear as char(4)) + '03'
										When DateDiff(Day, @FiscalStart, @Day) +1 between 92 and 119 then Cast (@FiscalYear as char(4)) + '04'
										When DateDiff(Day, @FiscalStart, @Day) +1 between 120 and 147 then Cast (@FiscalYear as char(4)) + '05'
										When DateDiff(Day, @FiscalStart, @Day) +1 between 148 and 182 then Cast (@FiscalYear as char(4)) + '06'
										When DateDiff(Day, @FiscalStart, @Day) +1 between 183 and 210 then Cast (@FiscalYear as char(4)) + '07'
										When DateDiff(Day, @FiscalStart, @Day) +1 between 211 and 238 then Cast (@FiscalYear as char(4)) + '08'
										When DateDiff(Day, @FiscalStart, @Day) +1 between 239 and 273 then Cast (@FiscalYear as char(4)) + '09'
										When DateDiff(Day, @FiscalStart, @Day) +1 between 274 and 301 then Cast (@FiscalYear as char(4)) + '10'
										When DateDiff(Day, @FiscalStart, @Day) +1 between 302 and 329 then Cast (@FiscalYear as char(4)) + '11'
										else Cast (@FiscalYear as char(4)) + '12'
										end
		  , [FiscalQuarterKey] = CASE
									When @Day < @FiscalStart then cast (datepart(year,@Day)-1 as char(4)) + '4'
									When @Day > @FiscalEnd then cast (datepart(year,@Day)+1 as char(4)) + '1'
									When @Day >= dateadd(day,273,@FiscalStart) then Cast (@FiscalYear as char(4)) + '4'
									When @Day >= dateadd(day,182,@FiscalStart) then Cast (@FiscalYear as char(4)) + '3'
									When @Day >= dateadd(day,91,@FiscalStart) then Cast (@FiscalYear as char(4)) + '2'
									else Cast (@FiscalYear as char(4)) + '1'
									end
		  , [FiscalYearKey] = @FiscalYear
		  , [FiscalYearName] = @FiscalYear
		  , [FiscalYearText] = @FiscalYear
		  , [FiscalYearNumber] = @FiscalYear

	Print 'Update Cycle 2'  
	Update		Stage.[Day] 
	set			[WeekEndDate] =  CASE
									When DatePart(WeekDay,@Day) = 7 then @Day
									When DatePart(Year,DateAdd(Day,6,WeekStartDate)) != DatePart(Year,@Day) then YearEndDate
									When DatePart(Week,@Day) >= 2 then DateAdd(day,6,WeekStartDate)
									When DatePart(WEEKDAY,YearStartDate) = 1 then DateAdd(day,6,WeekStartDate)
									When DatePart(WEEKDAY,YearStartDate) = 2 then DateAdd(day,5,WeekStartDate)
									When DatePart(WEEKDAY,YearStartDate) = 3 then DateAdd(day,4,WeekStartDate)
									When DatePart(WEEKDAY,YearStartDate) = 4 then DateAdd(day,3,WeekStartDate)
									When DatePart(WEEKDAY,YearStartDate) = 5 then DateAdd(day,2,WeekStartDate)
									When DatePart(WEEKDAY,YearStartDate) = 6 then DateAdd(day,1,WeekStartDate)
									else DateAdd(day,6,WeekStartDate)
									end
				, [MonthEndDate] = DateAdd(Day,-1,DateAdd(Month,1,MonthStartDate))
				, MonthOfYearText = Left ([MonthName],3)
				, [QuarterDays] = DateDiff(Day,QuarterStartDate, QuarterEndDate) +1
				, [TaxQuarterStartDate] = QuarterStartDate
				, [TaxQuarterEndDate] = QuarterEndDate
				, [TaxQuarterOfYearText] = Left (TaxQuarterText,2)
				, [TaxQuarterOfYearNumber] = Right (Left (TaxQuarterText,2),1)
				, [TaxYearStartDate] = cast (TaxYearKey as char(4)) + '-07-01'
				, [TaxYearEndDate] = cast (TaxYearKey + 1 as char(4)) + '-06-30'
				, [FiscalWeekName] = 'W' + Cast (Right (FiscalWeekKey,2) as char(2)) + '-' + Cast (Left (FiscalWeekKey,4) as char(4))
				, [FiscalWeekText] = 'W' + Cast (Right (FiscalWeekKey,2) as char(2)) + '-' + Cast (Left (FiscalWeekKey,4) as char(4))
				, [FiscalWeekOfMonthText] = case
												When right (FiscalWeekKey,2) = '01' then 'W1'
												When right (FiscalWeekKey,2) = '02' then 'W2'
												When right (FiscalWeekKey,2) = '03' then 'W3'
												When right (FiscalWeekKey,2) = '04' then 'W1'
												When right (FiscalWeekKey,2) = '05' then 'W2'
												When right (FiscalWeekKey,2) = '06' then 'W3'
												When right (FiscalWeekKey,2) = '07' then 'W1'
												When right (FiscalWeekKey,2) = '08' then 'W2'
												When right (FiscalWeekKey,2) = '09' then 'W3'
												When right (FiscalWeekKey,2) = '10' then 'W1'
												When right (FiscalWeekKey,2) = '11' then 'W2'
												When right (FiscalWeekKey,2) = '12' then 'W3'
												else 'W4'
												End
				, [FiscalWeekOfMonthNumber] = case
												When right (FiscalWeekKey,2) = '01' then 1
												When right (FiscalWeekKey,2) = '02' then 2
												When right (FiscalWeekKey,2) = '03' then 3
												When right (FiscalWeekKey,2) = '04' then 1
												When right (FiscalWeekKey,2) = '05' then 2
												When right (FiscalWeekKey,2) = '06' then 3
												When right (FiscalWeekKey,2) = '07' then 1
												When right (FiscalWeekKey,2) = '08' then 2
												When right (FiscalWeekKey,2) = '09' then 3
												When right (FiscalWeekKey,2) = '10' then 1
												When right (FiscalWeekKey,2) = '11' then 2
												When right (FiscalWeekKey,2) = '12' then 3
												else 4
												End
				, [FiscalMonthName] = CASE
					When Right (FiscalMonthKey, 2) = 01 then 'Jan-' + FiscalYearText
					When Right (FiscalMonthKey, 2) = 02 then 'Feb-' + FiscalYearText
					When Right (FiscalMonthKey, 2) = 03 then 'Mar-' + FiscalYearText
					When Right (FiscalMonthKey, 2) = 04 then 'Apr-' + FiscalYearText
					When Right (FiscalMonthKey, 2) = 05 then 'May-' + FiscalYearText
					When Right (FiscalMonthKey, 2) = 06 then 'Jun-' + FiscalYearText
					When Right (FiscalMonthKey, 2) = 07 then 'Jul-' + FiscalYearText
					When Right (FiscalMonthKey, 2) = 08 then 'Aug-' + FiscalYearText
					When Right (FiscalMonthKey, 2) = 09 then 'Sep-' + FiscalYearText
					When Right (FiscalMonthKey, 2) = 10 then 'Oct-' + FiscalYearText
					When Right (FiscalMonthKey, 2) = 11 then 'Nov-' + FiscalYearText
					When Right (FiscalMonthKey, 2) = 12 then 'Dec-' + FiscalYearText
					else 'ERROR'
					End
				, [FiscalMonthText] = CASE
					When Right (FiscalMonthKey, 2) = 01 then 'Jan-' + FiscalYearText
					When Right (FiscalMonthKey, 2) = 02 then 'Feb-' + FiscalYearText
					When Right (FiscalMonthKey, 2) = 03 then 'Mar-' + FiscalYearText
					When Right (FiscalMonthKey, 2) = 04 then 'Apr-' + FiscalYearText
					When Right (FiscalMonthKey, 2) = 05 then 'May-' + FiscalYearText
					When Right (FiscalMonthKey, 2) = 06 then 'Jun-' + FiscalYearText
					When Right (FiscalMonthKey, 2) = 07 then 'Jul-' + FiscalYearText
					When Right (FiscalMonthKey, 2) = 08 then 'Aug-' + FiscalYearText
					When Right (FiscalMonthKey, 2) = 09 then 'Sep-' + FiscalYearText
					When Right (FiscalMonthKey, 2) = 10 then 'Oct-' + FiscalYearText
					When Right (FiscalMonthKey, 2) = 11 then 'Nov-' + FiscalYearText
					When Right (FiscalMonthKey, 2) = 12 then 'Dec-' + FiscalYearText
					else 'ERROR'
					End
				, [FiscalQuarterName] = 'Q' + cast (right (FiscalQuarterKey,1) as varchar(1)) + '-' + Left (FiscalQuarterKey,4)
				, [FiscalQuarterText] = 'Q' + cast (right (FiscalQuarterKey,1) as varchar(1)) + '-' + Left (FiscalQuarterKey,4)
				, [FiscalQuarterStartDate] = CASE
												When right (FiscalQuarterKey,1) = 4 then dateadd(day,273,[FiscalYearStartDate])
												When right (FiscalQuarterKey,1) = 3 then dateadd(day,182,[FiscalYearStartDate])
												When right (FiscalQuarterKey,1) = 2 then dateadd(day,91,[FiscalYearStartDate])
												else [FiscalYearStartDate]
												end
				, [FiscalQuarterEndDate] = CASE
											When right (FiscalQuarterKey,1) = 3 then dateadd(day,272,[FiscalYearStartDate])
											When right (FiscalQuarterKey,1) = 2 then dateadd(day,181,[FiscalYearStartDate])
											When right (FiscalQuarterKey,1) = 1 then dateadd(day,90,[FiscalYearStartDate])
											else [FiscalYearEndDate]
											end
				, [FiscalMonthOfYearNumber] = cast (Right (FiscalMonthKey,2) as int)
				, [WeekOfMonthText] = 'N/A'
				, [WeekOfMonthNumber] = 0



	Print 'Update Cycle 3'
	Update		Stage.[Day] 
	set			[MonthDays] = DateDiff(Day, MonthStartDate, MonthEndDate) +1
				, [TaxYearDays] = DateDiff(Day, [TaxYearStartDate], [TaxYearEndDate]) +1
				, [FiscalWeekOfYearText] = Replace (Left (FiscalWeekText,3),'W0', 'W')
				, FiscalWeekOfYearNumber = Replace (Replace (Left (FiscalWeekText,3),'W0', ''), 'W', '')
				, [FiscalQuarterDays] = DateDiff(Day, [FiscalQuarterStartDate], [FiscalQuarterEndDate]) +1
				, [FiscalQuarterWeeks] = (DateDiff(Day, [FiscalQuarterStartDate], [FiscalQuarterEndDate]) +1) / 7
				, [FiscalMonthStartDate] = CASE
											When DateDiff(Day,FiscalQuarterStartDate,Daydate)+1 <= 28 then FiscalQuarterStartDate
											When DateDiff(Day,FiscalQuarterStartDate,Daydate)+1 between 29 and 56 then DateAdd(Day,28,FiscalQuarterStartDate)									
											else DateAdd(Day,56,FiscalQuarterStartDate)
											end
				, [FiscalMonthOfYearText] = Case
											 When cast (Right (FiscalMonthKey,2) as int) = 1 then 'Jan'
											 When cast (Right (FiscalMonthKey,2) as int) = 2 then 'Feb'
											 When cast (Right (FiscalMonthKey,2) as int) = 3 then 'Mar'
											 When cast (Right (FiscalMonthKey,2) as int) = 4 then 'Apr'
											 When cast (Right (FiscalMonthKey,2) as int) = 5 then 'May'
											 When cast (Right (FiscalMonthKey,2) as int) = 6 then 'Jun'
											 When cast (Right (FiscalMonthKey,2) as int) = 7 then 'Jul'
											 When cast (Right (FiscalMonthKey,2) as int) = 8 then 'Aug'
											 When cast (Right (FiscalMonthKey,2) as int) = 9 then 'Sep'
											 When cast (Right (FiscalMonthKey,2) as int) = 10 then 'Oct'
											 When cast (Right (FiscalMonthKey,2) as int) = 11 then 'Nov'
											 When cast (Right (FiscalMonthKey,2) as int) = 12 then 'Dec'
											 else 'ERR'
											 End
			

				, [FiscalQuarterOfYearText] = CASE	
												When Cast (Right (FiscalWeekKey, 2) as int) <= 13 then 'Q1'
												When Cast (Right (FiscalWeekKey, 2) as int) >= 14 and Cast (Right (FiscalWeekKey, 2) as int) <= 26 then 'Q2'
												When Cast (Right (FiscalWeekKey, 2) as int) >= 27 and Cast (Right (FiscalWeekKey, 2) as int) <= 39 then 'Q3'
												Else 'Q4'
												End
				, [FiscalQuarterOfYearNumber] = CASE	
												When Cast (Right (FiscalWeekKey, 2) as int) <= 13 then 1
												When Cast (Right (FiscalWeekKey, 2) as int) >= 14 and Cast (Right (FiscalWeekKey, 2) as int) <= 26 then 2
												When Cast (Right (FiscalWeekKey, 2) as int) >= 27 and Cast (Right (FiscalWeekKey, 2) as int) <= 39 then 3
												Else 4
												End
				, [DayOfFiscalQuarterNumber] = DateDiff(Day,FiscalQuarterStartDate, DayDate) +1

	Print 'Update Cycle 4'
	Update		Stage.[Day] 
	set			[FiscalMonthEndDate] = CASE
										When FiscalMonthOfYearNumber = 12 then FiscalYearEndDate
										When FiscalMonthOfYearNumber in (3,6,9) then DateAdd(Day, 34, FiscalMonthStartDate)
										else DateAdd(Day, 27, FiscalMonthStartDate)
										End
				, [FiscalYearWeeks] = FiscalYearDays / 7
				, [DayOfFiscalMonthNumber] = Coalesce (DateDiff(Day,FiscalMonthStartDate, DayDate) +1, 0)
				, [TaxQuarterDays] = QuarterDays

	Print 'Update Cycle 5'
	Update		Stage.[Day] 
	set			[FiscalMonthDays] = DateDiff(Day, FiscalMonthStartDate, FiscalMonthEndDate) +1
				, [FiscalMonthWeeks]  = (DateDiff(Day, FiscalMonthStartDate, FiscalMonthEndDate) +1) / 7

	Print 'Update Cycle - Previous Period'

	Begin Try 
	Drop Table #PreviousDays 
	End Try
	Begin Catch
		Print '#PreviousDays does not yet exist'
	End Catch

	Select	*
	into	#PreviousDays
	from	[$(Dimensional)].Dimension.[Day]
	where	DayKey >= Replace (DateAdd(Year, -1, cast (@LastDay as date)), '-', '')


	Update		Stage.[Day]
	set			[PreviousDayKey] = @LastDayKey
				,[PreviousWeekKey] =		(Select		Coalesce (PreviousRecord.WeekKey, 0) as WeekKey
											 From		Stage.[Day] as CurrentRecord
														left join #PreviousDays as PreviousRecord on DateAdd(Day, -1, CurrentRecord.WeekStartDate) = PreviousRecord.DayDate 		
											)
				,[PreviousWeekDayKey] =		(Select		Coalesce (PreviousRecord.DayKey, 0) as DayKey
											 From		Stage.[Day] as CurrentRecord
														left join #PreviousDays as PreviousRecord on DateAdd(Day, -7, CurrentRecord.DayDate) = PreviousRecord.DayDate 		
											)
				,[PreviousMonthKey] =		(Select		Coalesce (PreviousRecord.MonthKey, 0) as MonthKey
											From		Stage.[Day] as CurrentRecord
														left join #PreviousDays as PreviousRecord on DateAdd(Day, -1, CurrentRecord.MonthStartDate) = PreviousRecord.DayDate 		
											)
				,[PreviousMonthDayKey] =	(Select		Coalesce (PreviousRecord.DayKey, 0) as DayKey
											 From		Stage.[Day] as CurrentRecord
														left join #PreviousDays as PreviousRecord on DateAdd(Month, -1, CurrentRecord.DayDate) = PreviousRecord.DayDate 		
											)
				,[PreviousQuarterKey] =		(Select		Coalesce (PreviousRecord.QuarterKey, 0) as QuarterKey
											 From		Stage.[Day] as CurrentRecord
														left join #PreviousDays as PreviousRecord on DateAdd(Day, -1, CurrentRecord.QuarterStartDate) = PreviousRecord.DayDate 		
											)
				,PreviousQuarterMonthKey =  (Select		Coalesce (PreviousRecord.MonthKey, 0) as MonthKey
											 From		Stage.[Day] as CurrentRecord
														left join #PreviousDays as PreviousRecord on DateAdd(Month, -3, CurrentRecord.DayDate) = PreviousRecord.DayDate 		
											)
				,[PreviousQuarterDayKey] =	(Select		Coalesce (PreviousRecord.DayKey, 0) as DayKey
											 From		Stage.[Day] as CurrentRecord
														left join #PreviousDays as PreviousRecord on DateAdd(Month, -3, CurrentRecord.DayDate) = PreviousRecord.DayDate 		
											)
				,[PreviousYearKey]=			(Select		Coalesce (PreviousRecord.YearKey, 0) as YearKey
											 From		Stage.[Day] as CurrentRecord
														left join #PreviousDays as PreviousRecord on DateAdd(Day, -1, CurrentRecord.YearStartDate) = PreviousRecord.DayDate 		
											)
				,[PreviousYearQuarterKey] = (Select		Coalesce (PreviousRecord.QuarterKey, 0) as QuarterKey
											 From		Stage.[Day] as CurrentRecord
														left join #PreviousDays as PreviousRecord on DateAdd(Year, -1, CurrentRecord.DayDate) = PreviousRecord.DayDate 		
											)
				,[PreviousYearMonthKey] =	(Select		Coalesce (PreviousRecord.MonthKey, 0) as MonthKey
											 From		Stage.[Day] as CurrentRecord
														left join #PreviousDays as PreviousRecord on DateAdd(Year, -1, CurrentRecord.DayDate) = PreviousRecord.DayDate 		
											)
				,[PreviousYearDayKey] =		(Select		Coalesce (PreviousRecord.DayKey, 0) as DayKey
											 From		Stage.[Day] as CurrentRecord
														left join #PreviousDays as PreviousRecord on DateAdd(Year, -1, CurrentRecord.DayDate) = PreviousRecord.DayDate 		
											)
				,[PreviousTaxQuarterKey] = (Select		Coalesce (PreviousRecord.TaxQuarterKey, 0) as TaxQuarterKey
											From		Stage.[Day] as CurrentRecord
														left join #PreviousDays as PreviousRecord on DateAdd(Day, -1, CurrentRecord.TaxQuarterStartDate) = PreviousRecord.DayDate 		
											)
				,PreviousTaxQuarterMonthKey = (Select	Coalesce (PreviousRecord.MonthKey, 0) as MonthKey
											   From		Stage.[Day] as CurrentRecord
														left join #PreviousDays as PreviousRecord on DateAdd(Month, -3, CurrentRecord.DayDate) = PreviousRecord.DayDate 		
											  )
				,[PreviousTaxQuarterDayKey] = (Select	Coalesce (PreviousRecord.DayKey, 0) as DayKey
											   From		Stage.[Day] as CurrentRecord
														left join #PreviousDays as PreviousRecord on DateAdd(Month, -3, CurrentRecord.DayDate) = PreviousRecord.DayDate 		
											  )
				,[PreviousTaxYearKey] =		(Select		Coalesce (PreviousRecord.TaxYearKey, 0) as DayKey
											 From		Stage.[Day] as CurrentRecord
														left join #PreviousDays as PreviousRecord on DateAdd(Day, -1, CurrentRecord.TaxYearStartDate) = PreviousRecord.DayDate 		
											)
				,[PreviousTaxYearQuarterKey] = (Select	Coalesce (PreviousRecord.TaxQuarterKey, 0) as DayKey
											   From		Stage.[Day] as CurrentRecord
														left join #PreviousDays as PreviousRecord on DateAdd(Year, -1, CurrentRecord.DayDate) = PreviousRecord.DayDate 		
											  )
				,[PreviousTaxYearMonthKey] = (Select	Coalesce (PreviousRecord.MonthKey, 0) as DayKey
											   From		Stage.[Day] as CurrentRecord
														left join #PreviousDays as PreviousRecord on DateAdd(Year, -1, CurrentRecord.DayDate) = PreviousRecord.DayDate 		
											  )
				,[PreviousTaxYearDayKey] =	(Select	Coalesce (PreviousRecord.DayKey, 0) as DayKey
											   From		Stage.[Day] as CurrentRecord
														left join #PreviousDays as PreviousRecord on DateAdd(Year, -1, CurrentRecord.DayDate) = PreviousRecord.DayDate 		
											  )
				,[PreviousFiscalWeekKey] =	(Select	Coalesce (PreviousRecord.WeekKey,0) as WeekKey
											 From		Stage.[Day] as CurrentRecord
														left join #PreviousDays as PreviousRecord on DateAdd(Day, -1, CurrentRecord.FiscalWeekStartDate) = PreviousRecord.DayDate 		
											)
				,[PreviousFiscalWeekDayKey] =	(Select	Coalesce (PreviousRecord.DayKey, 0) as DayKey
											   From		Stage.[Day] as CurrentRecord
														left join #PreviousDays as PreviousRecord on DateAdd(day, -7, CurrentRecord.DayDate) = PreviousRecord.DayDate 		
											  )
				,[PreviousFiscalMonthKey] =	(Select	Coalesce (PreviousRecord.FiscalMonthKey, 0) as DayKey
											   From		Stage.[Day] as CurrentRecord
														left join #PreviousDays as PreviousRecord on DateAdd(day, -7, CurrentRecord.DayDate) = PreviousRecord.DayDate 		
											  )
				,[PreviousFiscalMonthDayKey] =	(Select	Coalesce (PreviousRecord.DayKey, 0) as DayKey
											   From		Stage.[Day] as CurrentRecord
														left join #PreviousDays as PreviousRecord on DateAdd(day, -7, CurrentRecord.DayDate) = PreviousRecord.DayDate 		
											  )
				,[PreviousFiscalQuarterKey] =	(Select		Coalesce (PreviousRecord.FiscalQuarterKey, 0) as FiscalQuarterKey
												From		Stage.[Day] as CurrentRecord
															left join #PreviousDays as PreviousRecord on DateAdd(day, -1, CurrentRecord.FiscalQuarterStartDate) = PreviousRecord.DayDate 		
												)
				,[PreviousFiscalQuarterMonthKey] =	(Select	Coalesce (PreviousRecord.FiscalMonthKey, 0) as DayKey
													From		Stage.[Day] as CurrentRecord
														left join #PreviousDays as PreviousRecord on DateAdd(month, -3, CurrentRecord.DayDate) = PreviousRecord.DayDate 		
													)		
				,[PreviousFiscalQuarterDayKey] =	(Select	Coalesce (PreviousRecord.DayKey, 0) as DayKey
													From		Stage.[Day] as CurrentRecord
														left join #PreviousDays as PreviousRecord on DateAdd(month, -3, CurrentRecord.DayDate) = PreviousRecord.DayDate 		
													)
				,[PreviousFiscalYearKey] =		(Select		Coalesce (PreviousRecord.FiscalYearKey, 0) as FiscalYearKey
												From		Stage.[Day] as CurrentRecord
															left join #PreviousDays as PreviousRecord on DateAdd(day, -1, CurrentRecord.FiscalYearStartDate) = PreviousRecord.DayDate 		
												)
				,[PreviousFiscalYearQuarterKey] = (Select		Coalesce (PreviousRecord.FiscalQuarterKey, 0) as DayKey
													From		Stage.[Day] as CurrentRecord
																left join #PreviousDays as PreviousRecord on DateAdd(year, -1, CurrentRecord.DayDate) = PreviousRecord.DayDate 		
													)
				,[PreviousFiscalYearMonthKey] = (Select		Coalesce (PreviousRecord.FiscalMonthKey, 0) as DayKey
													From		Stage.[Day] as CurrentRecord
																left join #PreviousDays as PreviousRecord on DateAdd(year, -1, CurrentRecord.DayDate) = PreviousRecord.DayDate 		
													)
				,[PreviousFiscalYearDayKey] = (Select		Coalesce (PreviousRecord.DayKey, 0) as DayKey
													From		Stage.[Day] as CurrentRecord
																left join #PreviousDays as PreviousRecord on DateAdd(year, -1, CurrentRecord.DayDate) = PreviousRecord.DayDate 		
													)

	Print 'Update Cycle - Next Period'
	Update		Stage.[Day]
		set		  [NextDayKey] = Replace (DateAdd(Day, 1, @Day), '-', '')
				  ,[NextWeekKey] = CASE
									When Right (WeekKey, 2) = 53 then Cast (YearKey + 1 as char(4)) + '01'
									Else Cast (YearKey as char(4)) + Right ('0' + Cast (Right (WeekKey, 2) + 1 as varchar(2)),2)
									End
				  ,[NextWeekDayKey] = Replace (DateAdd(Day, 7, @Day), '-', '')
				  ,[NextMonthKey] = CASE
									When Right (MonthKey, 2) = 12 then Cast (YearKey + 1 as char(4)) + '01'
									Else Cast (YearKey as char(4)) + Right ('0' + Cast (Right (MonthKey, 2) + 1 as varchar(2)),2)
									End
				  ,[NextMonthDayKey] = Replace (DateAdd(Month, 1, @Day), '-', '')
				  ,[NextQuarterKey] = CASE
									When Right (QuarterKey, 1) = 4 then Cast (YearKey + 1 as char(4)) + '1'
									Else Cast (YearKey as char(4)) + Cast (Right (QuarterKey, 1) + 1 as char(1))
									End
				  ,[NextQuarterMonthKey] = CASE
									When Right (MonthKey, 2) = 10 then Cast (YearKey + 1 as char(4)) + '01'
									When Right (MonthKey, 2) = 11 then Cast (YearKey + 1 as char(4)) + '02'
									When Right (MonthKey, 2) = 12 then Cast (YearKey + 1 as char(4)) + '03'
									Else (Cast (YearKey as char(4)) + Right ('0' + Cast (Right (MonthKey, 2) + 3 as varchar(2)),2))
									End
				  ,[NextQuarterDayKey] = Replace (DateAdd(Month, 3, @Day), '-', '')
				  ,[NextYearKey] = YearKey + 1
				  ,[NextYearQuarterKey] = Cast(YearKey + 1 as char(4)) + Cast(Right(QuarterKey,1) as char(1))
				  ,[NextYearMonthKey] = Cast(YearKey + 1 as char(4)) + Right ('0' + Cast(Right(MonthKey,2) as varchar(2)),2)
				  ,[NextYearDayKey] = Replace (DateAdd(Year, 1, @Day), '-', '')
				  ,[NextTaxQuarterKey] = CASE
									When Right (TaxQuarterKey, 1) = 4 then Cast (TaxYearKey + 1 as char(4)) + '1'
									Else Cast (TaxYearKey as char(4)) + Cast (Right (TaxQuarterKey, 1) + 1 as char(1))
									End
				  ,[NextTaxQuarterMonthKey] = CASE
									When Right (MonthKey, 2) = 1 then Cast (TaxYearKey as char(4)) + '10'
									When Right (MonthKey, 2) = 2 then Cast (TaxYearKey as char(4)) + '11'
									When Right (MonthKey, 2) = 3 then Cast (TaxYearKey as char(4)) + '12'
									When Right (MonthKey, 2) = 4 then Cast (TaxYearKey + 1 as char(4)) + '01'
									When Right (MonthKey, 2) = 5 then Cast (TaxYearKey + 1 as char(4)) + '02'
									When Right (MonthKey, 2) = 6 then Cast (TaxYearKey + 1 as char(4)) + '03'
									When Right (MonthKey, 2) = 7 then Cast (TaxYearKey + 1 as char(4)) + '04'
									When Right (MonthKey, 2) = 8 then Cast (TaxYearKey + 1 as char(4)) + '05'
									When Right (MonthKey, 2) = 9 then Cast (TaxYearKey + 1 as char(4)) + '06'
									When Right (MonthKey, 2) = 10 then Cast (TaxYearKey + 1 as char(4)) + '07'
									When Right (MonthKey, 2) = 11 then Cast (TaxYearKey + 1 as char(4)) + '08'
									When Right (MonthKey, 2) = 12 then Cast (TaxYearKey + 1 as char(4)) + '09'
									Else '0'
									End
				  ,[NextTaxQuarterDayKey] = Replace (DateAdd(Month, 3, @Day), '-', '')
				  ,[NextTaxYearKey] = TaxYearKey + 1
				  ,[NextTaxYearQuarterKey] = Cast(TaxYearKey + 1 as char(4)) + Cast(Right(QuarterKey,1) as char(1))
				  ,[NextTaxYearMonthKey] = Cast(TaxYearKey + 1 as char(4)) + Right ('0' + Cast(Right(MonthKey,2) as varchar(2)),2)
				  ,[NextTaxYearDayKey] = Replace (DateAdd(Year, 1, @Day), '-', '')
				  ,[NextFiscalWeekKey] = CASE
									When FiscalYearWeeks = 53 and Right (FiscalWeekKey, 2) = 53 then Cast (FiscalYearKey + 1 as char(4)) + '01'
									When FiscalYearWeeks = 52 and Right (FiscalWeekKey, 2) = 52 then Cast (FiscalYearKey + 1 as char(4)) + '01'
									Else Cast (FiscalYearKey as char(4)) + Right ('0' + Cast (Right (FiscalWeekKey, 2) + 1 as varchar(2)),2)
									End
				  ,[NextFiscalWeekDayKey] = Replace (DateAdd(Day, 7, @Day), '-', '')
				  ,[NextFiscalMonthKey] = CASE
									When Right (FiscalMonthKey, 2) = 12 then Cast (FiscalYearKey + 1 as char(4)) + '01'
									Else Cast (FiscalYearKey as char(4)) + Right ('0' + Cast (Right (FiscalMonthKey, 2) + 1 as varchar(2)),2)
									End
				  ,[NextFiscalMonthDayKey] = Replace (DateAdd(Day, FiscalMonthDays, @Day), '-', '')
				  ,[NextFiscalQuarterKey] = CASE
									When Right (FiscalQuarterKey, 1) = 4 then Cast (FiscalYearKey + 1 as char(4)) + '1'
									Else Cast (FiscalYearKey as char(4)) + Cast (Right (FiscalQuarterKey, 1) + 1 as char(1))
									End
				  ,[NextFiscalQuarterMonthKey] = CASE
									When Right (FiscalMonthKey, 2) = 1 then Cast (FiscalYearKey as char(4)) + '04'
									When Right (FiscalMonthKey, 2) = 2 then Cast (FiscalYearKey as char(4)) + '05'
									When Right (FiscalMonthKey, 2) = 3 then Cast (FiscalYearKey as char(4)) + '06'
									When Right (FiscalMonthKey, 2) = 4 then Cast (FiscalYearKey as char(4)) + '07'
									When Right (FiscalMonthKey, 2) = 5 then Cast (FiscalYearKey as char(4)) + '08'
									When Right (FiscalMonthKey, 2) = 6 then Cast (FiscalYearKey as char(4)) + '09'
									When Right (FiscalMonthKey, 2) = 7 then Cast (FiscalYearKey as char(4)) + '10'
									When Right (FiscalMonthKey, 2) = 8 then Cast (FiscalYearKey as char(4)) + '11'
									When Right (FiscalMonthKey, 2) = 9 then Cast (FiscalYearKey as char(4)) + '12'
									When Right (FiscalMonthKey, 2) = 10 then Cast (FiscalYearKey + 1 as char(4)) + '01'
									When Right (FiscalMonthKey, 2) = 11 then Cast (FiscalYearKey + 1 as char(4)) + '02'
									When Right (FiscalMonthKey, 2) = 12 then Cast (FiscalYearKey + 1 as char(4)) + '03'
									Else '0'
									End
				  ,[NextFiscalQuarterDayKey] = Replace (DateAdd(Day, FiscalQuarterDays, @Day), '-', '')
				  ,[NextFiscalYearKey] = FiscalYearKey + 1
				  ,[NextFiscalYearQuarterKey] = Cast(FiscalYearKey + 1 as char(4)) + Cast(Right(FiscalQuarterKey,1) as char(1))
				  ,[NextFiscalYearMonthKey] = Cast(FiscalYearKey + 1 as char(4)) + Cast(Right(FiscalMonthKey,1) as char(1))
				  ,[NextFiscalYearDayKey] = Replace (DateAdd(Day, FiscalYearDays, @Day), '-', '')

	Update	Stage.[Day]
		SET	[CreatedDate] = GetDate(),
			[CreatedBatchKey] = @BatchKey,
			[CreatedBy] = 'Sp_DimDay',
			[ModifiedDate] = GetDate(),
			[ModifiedBatchKey] = @BatchKey,
			[ModifiedBy] = 'Sp_DimDay'

	Insert into [$(Dimensional)].Dimension.[Day]
		(	[DayKey] ,[DayName], [DayText], [DayDate], [DayOfWeekText], [DayOfWeekNumber], [DayOfMonthNumber], [DayOfQuarterNumber], [DayOfYearNumber], [DayOfTaxQuarterNumber], [DayOfTaxYearNumber], [DayOfFiscalWeekNumber], [DayOfFiscalMonthNumber], [DayOfFiscalQuarterNumber], [DayOfFiscalYearNumber],
			[WeekKey], [WeekName], [WeekText], [WeekStartDate], [WeekEndDate], [WeekOfMonthText], [WeekOfMonthNumber], [WeekOfYearText], [WeekOfYearNumber], [WeekYearNumber],
			[MonthKey], [MonthName], [MonthText], [MonthStartDate], [MonthEndDate], [MonthDays], [MonthOfYearText], [MonthOfYearNumber],
			[QuarterKey], [QuarterName], [QuarterText], [QuarterStartDate], [QuarterEndDate], [QuarterDays], [QuarterOfYearText], [QuarterOfYearNumber],
			[YearKey], [YearName], [YearText], [YearNumber], [YearStartDate], [YearEndDate], [YearDays],
			[TaxQuarterKey], [TaxQuarterName], [TaxQuarterText], [TaxQuarterStartDate], [TaxQuarterEndDate], [TaxQuarterDays], [TaxQuarterOfYearText], [TaxQuarterOfYearNumber],
			[TaxYearKey], [TaxYearName], [TaxYearText], [TaxYearNumber], [TaxYearStartDate], [TaxYearEndDate], [TaxYearDays],
			[FiscalWeekKey], [FiscalWeekName], [FiscalWeekText], [FiscalWeekStartDate], [FiscalWeekEndDate], [FiscalWeekOfMonthText], [FiscalWeekOfMonthNumber], [FiscalWeekOfYearText], [FiscalWeekOfYearNumber],
			[FiscalMonthKey], [FiscalMonthName], [FiscalMonthText], [FiscalMonthStartDate], [FiscalMonthEndDate], [FiscalMonthDays], [FiscalMonthWeeks], [FiscalMonthOfYearText], [FiscalMonthOfYearNumber],
			[FiscalQuarterKey], [FiscalQuarterName], [FiscalQuarterText], [FiscalQuarterStartDate], [FiscalQuarterEndDate], [FiscalQuarterDays], [FiscalQuarterWeeks], [FiscalQuarterOfYearText], [FiscalQuarterOfYearNumber],
			[FiscalYearKey], [FiscalYearName], [FiscalYearText], [FiscalYearNumber], [FiscalYearStartDate], [FiscalYearEndDate], [FiscalYearDays], [FiscalYearWeeks],
			[PreviousDayKey], [PreviousWeekKey], [PreviousWeekDayKey], [PreviousMonthKey], [PreviousMonthDayKey], [PreviousQuarterKey], [PreviousQuarterMonthKey], [PreviousQuarterDayKey], [PreviousYearKey], [PreviousYearQuarterKey], [PreviousYearMonthKey], [PreviousYearDayKey],
			[PreviousTaxQuarterKey], [PreviousTaxQuarterMonthKey], [PreviousTaxQuarterDayKey], [PreviousTaxYearKey], [PreviousTaxYearQuarterKey], [PreviousTaxYearMonthKey], [PreviousTaxYearDayKey],
			[PreviousFiscalWeekKey], [PreviousFiscalWeekDayKey], [PreviousFiscalMonthKey], [PreviousFiscalMonthDayKey], [PreviousFiscalQuarterKey], [PreviousFiscalQuarterMonthKey], [PreviousFiscalQuarterDayKey], [PreviousFiscalYearKey], [PreviousFiscalYearQuarterKey], [PreviousFiscalYearMonthKey], [PreviousFiscalYearDayKey],
			[NextDayKey], [NextWeekKey], [NextWeekDayKey], [NextMonthKey], [NextMonthDayKey], [NextQuarterKey], [NextQuarterMonthKey], [NextQuarterDayKey], [NextYearKey], [NextYearQuarterKey], [NextYearMonthKey], [NextYearDayKey],
			[NextTaxQuarterKey], [NextTaxQuarterMonthKey], [NextTaxQuarterDayKey], [NextTaxYearKey], [NextTaxYearQuarterKey], [NextTaxYearMonthKey], [NextTaxYearDayKey],
			[NextFiscalWeekKey], [NextFiscalWeekDayKey], [NextFiscalMonthKey], [NextFiscalMonthDayKey], [NextFiscalQuarterKey], [NextFiscalQuarterMonthKey], [NextFiscalQuarterDayKey], [NextFiscalYearKey], [NextFiscalYearQuarterKey], [NextFiscalYearMonthKey], [NextFiscalYearDayKey],
			[CreatedDate], [CreatedBatchKey], [CreatedBy], [ModifiedDate], [ModifiedBatchKey], [ModifiedBy]
		)
	Select	[DayKey] ,[DayName], [DayText], [DayDate], [DayOfWeekText], [DayOfWeekNumber], [DayOfMonthNumber], [DayOfQuarterNumber], [DayOfYearNumber], [DayOfTaxQuarterNumber], [DayOfTaxYearNumber], [DayOfFiscalWeekNumber], [DayOfFiscalMonthNumber], [DayOfFiscalQuarterNumber], [DayOfFiscalYearNumber],
			[WeekKey], [WeekName], [WeekText], [WeekStartDate], [WeekEndDate], [WeekOfMonthText], [WeekOfMonthNumber], [WeekOfYearText], [WeekOfYearNumber], [WeekYearNumber],
			[MonthKey], [MonthName], [MonthText], [MonthStartDate], [MonthEndDate], [MonthDays], [MonthOfYearText], [MonthOfYearNumber],
			[QuarterKey], [QuarterName], [QuarterText], [QuarterStartDate], [QuarterEndDate], [QuarterDays], [QuarterOfYearText], [QuarterOfYearNumber],
			[YearKey], [YearName], [YearText], [YearNumber], [YearStartDate], [YearEndDate], [YearDays],
			[TaxQuarterKey], [TaxQuarterName], [TaxQuarterText], [TaxQuarterStartDate], [TaxQuarterEndDate], [TaxQuarterDays], [TaxQuarterOfYearText], [TaxQuarterOfYearNumber],
			[TaxYearKey], [TaxYearName], [TaxYearText], [TaxYearNumber], [TaxYearStartDate], [TaxYearEndDate], [TaxYearDays],
			[FiscalWeekKey], [FiscalWeekName], [FiscalWeekText], [FiscalWeekStartDate], [FiscalWeekEndDate], [FiscalWeekOfMonthText], [FiscalWeekOfMonthNumber], [FiscalWeekOfYearText], [FiscalWeekOfYearNumber],
			[FiscalMonthKey], [FiscalMonthName], [FiscalMonthText], [FiscalMonthStartDate], [FiscalMonthEndDate], [FiscalMonthDays], [FiscalMonthWeeks], [FiscalMonthOfYearText], [FiscalMonthOfYearNumber],
			[FiscalQuarterKey], [FiscalQuarterName], [FiscalQuarterText], [FiscalQuarterStartDate], [FiscalQuarterEndDate], [FiscalQuarterDays], [FiscalQuarterWeeks], [FiscalQuarterOfYearText], [FiscalQuarterOfYearNumber],
			[FiscalYearKey], [FiscalYearName], [FiscalYearText], [FiscalYearNumber], [FiscalYearStartDate], [FiscalYearEndDate], [FiscalYearDays], [FiscalYearWeeks],
			[PreviousDayKey], [PreviousWeekKey], [PreviousWeekDayKey], [PreviousMonthKey], [PreviousMonthDayKey], [PreviousQuarterKey], [PreviousQuarterMonthKey], [PreviousQuarterDayKey], [PreviousYearKey], [PreviousYearQuarterKey], [PreviousYearMonthKey], [PreviousYearDayKey],
			[PreviousTaxQuarterKey], [PreviousTaxQuarterMonthKey], [PreviousTaxQuarterDayKey], [PreviousTaxYearKey], [PreviousTaxYearQuarterKey], [PreviousTaxYearMonthKey], [PreviousTaxYearDayKey],
			[PreviousFiscalWeekKey], [PreviousFiscalWeekDayKey], [PreviousFiscalMonthKey], [PreviousFiscalMonthDayKey], [PreviousFiscalQuarterKey], [PreviousFiscalQuarterMonthKey], [PreviousFiscalQuarterDayKey], [PreviousFiscalYearKey], [PreviousFiscalYearQuarterKey], [PreviousFiscalYearMonthKey], [PreviousFiscalYearDayKey],
			[NextDayKey], [NextWeekKey], [NextWeekDayKey], [NextMonthKey], [NextMonthDayKey], [NextQuarterKey], [NextQuarterMonthKey], [NextQuarterDayKey], [NextYearKey], [NextYearQuarterKey], [NextYearMonthKey], [NextYearDayKey],
			[NextTaxQuarterKey], [NextTaxQuarterMonthKey], [NextTaxQuarterDayKey], [NextTaxYearKey], [NextTaxYearQuarterKey], [NextTaxYearMonthKey], [NextTaxYearDayKey],
			[NextFiscalWeekKey], [NextFiscalWeekDayKey], [NextFiscalMonthKey], [NextFiscalMonthDayKey], [NextFiscalQuarterKey], [NextFiscalQuarterMonthKey], [NextFiscalQuarterDayKey], [NextFiscalYearKey], [NextFiscalYearQuarterKey], [NextFiscalYearMonthKey], [NextFiscalYearDayKey],
			[CreatedDate], [CreatedBatchKey], [CreatedBy], [ModifiedDate], [ModifiedBatchKey], [ModifiedBy]
	from	Stage.[Day]

	Truncate table Stage.[Day]
	
	Set @LastDayKey = (Select Max(DayKey) from [$(Dimensional)].[Dimension].[Day] where Daykey not in (0 ,-1))
	Set @LastDay = (Select DayDate from [$(Dimensional)].[Dimension].[Day] where Daykey = @LastDayKey)

	END

	EXEC [Control].Sp_Log @BatchKey, @ProcName, NULL,'Success';
	
END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, @ProcName, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

﻿CREATE PROCEDURE [Dimension].[Sp_DimBetType]
	@BatchKey	INT
WITH RECOMPILE
AS 

BEGIN

		EXEC [Control].[Sp_Log] @BatchKey,'Sp_DimBetType','InsertIntoDim','Start'

		INSERT INTO [$(Dimensional)].Dimension.BetType (BetGroupType,BetType,BetSubType,GroupingType,LegType,FromDate,ToDate,FirstDate,CreatedDate,CreatedBatchKey,CreatedBy,ModifiedDate,ModifiedBatchKey,ModifiedBy)
		SELECT DISTINCT A.BetGroupType,A.BetTypeName,A.BetSubTypeName,A.GroupingType,A.LegBetTypeName,GETDATE(),'9999-12-31',GETDATE(),GETDATE(),@BATCHKEY,'Sp_DimBetType',GETDATE(),@BATCHKEY,'Sp_DimBetType'
		FROM [Stage].[Transaction] A
		LEFT OUTER JOIN [$(Dimensional)].Dimension.BetType B	ON A.BetGroupType=B.BetGroupType AND A.BetTypeName=B.BetType AND A.BetSubTypeName=B.BetSubType AND A.GroupingType=B.GroupingType AND A.LegBetTypeName=B.LegType
		WHERE B.BetType IS NULL
		AND A.BatchKey = @BatchKey
		AND A.ExistsInDim = 0;

		EXEC [Control].[Sp_Log] @BatchKey,'Sp_DimBetType',NULL,'Success'

END

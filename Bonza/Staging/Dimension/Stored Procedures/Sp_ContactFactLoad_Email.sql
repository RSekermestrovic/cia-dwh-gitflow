﻿CREATE PROC [Dimension].[Sp_ContactFactLoad_Email]
(
	@BatchKey	int
)
WITH RECOMPILE
AS 

BEGIN TRY

EXEC [Control].Sp_Log @BatchKey,'Sp_ContactFactLoad_Email','InsertIntoDimension','Start'
select *
into #Contact
from Stage.Contact
Where BatchKey = @BatchKey and JobIDSource='ETE'

Select 
	IsNull(c.AccountKey,-1) AccountKey,
	IsNull(e.AccountStatusKey,-1) AccountStatusKey,
	a.JobID CommunicationId,
	IsNull(d.CommunicationKey,-1) CommunicationKey,
	IsNull(s.StoryKey,-1) StoryKey,
	IsNull(ch.ChannelKey,-1) ChannelKey,
	IsNull(p.PromotionKey,-1) PromotionKey,
	IsNull(DBT.BettypeKey,-1) BettypeKey,
	IsNull(cl.classKey,-1) classKey,
	IsNull(co.CompanyKey,-1) CommunicationCompanyKey,
	a.EventId,
	a.EventSource,
	IsNull(it.InteractionTypeKey,-1) InteractionTypeKey,
	IsNull(DZ.DayKey,-1) InteractionDayKey,
	ISNULL(TMD.TimeKey,-1) AS InteractionTimeKeyMaster,
	--ISNULL(TAD.TimeKey,-1) AS InteractionTimeKeyAnalysis,
	a.Details,
	a.EventDate,
	tz.OffsetMinutes
INTO #PreLoad
From #Contact a
LEFT OUTER JOIN [$(Dimensional)].Dimension.Account c on a.AccountID = c.AccountID and c.AccountSource=a.AccountSource
LEFT OUTER JOIN [$(Dimensional)].[Dimension].[Communication] d on a.JobId = d.CommunicationId and a.JobIDSource = d.CommunicationSource
LEFT OUTER JOIN [$(Dimensional)].Dimension.AccountStatus e on c.AccountKey = e.AccountKey and a.EventDate >=e.FromDate and a.EventDate <e.ToDate
LEFT OUTER JOIN Reference.TimeZone	TZ   WITH(NOLOCK) ON a.EventDate>=TZ.FromDate AND a.EventDate<TZ.ToDate AND TZ.TimeZone='Analysis'
LEFT OUTER JOIN [$(Dimensional)].Dimension.[DayZone] DZ WITH(NOLOCK) ON CONVERT(VARCHAR(11),CONVERT(DATE,a.EventDate))=DZ.MasterDayText AND a.EventDate>=DZ.FromDate AND a.EventDate<DZ.ToDate
LEFT OUTER JOIN [$(Dimensional)].Dimension.[TIME] TMD  WITH(NOLOCK) ON CONVERT(VARCHAR(5),CONVERT(TIME,a.EventDate))=TMD.TimeText
--LEFT OUTER JOIN [$(Dimensional)].Dimension.[TIME] TAD  WITH(NOLOCK) ON CONVERT(VARCHAR(5),CONVERT(TIME,DATEADD(MI,TZ.OffsetMinutes,a.EventDate)))=TAD.TimeText
LEFT OUTER JOIN [$(Dimensional)].Dimension.InteractionType it on a.InteractionTypeId = it.InteractionTypeId and a.InteractionTypeSource = it.InteractionTypeSource
LEFT OUTER JOIN [$(Dimensional)].Dimension.Story s on a.Story = s.Story
LEFT OUTER JOIN [$(Dimensional)].Dimension.Channel ch on a.ChannelId = ch.ChannelId
LEFT OUTER JOIN [$(Dimensional)].Dimension.BetType DBT on a.BetType=DBT.BetType AND a.LegType=DBT.LegType AND a.BetGroupType=DBT.BetGroupType AND a.BetSubType=DBT.BetSubType AND a.GroupingType=DBT.GroupingType
LEFT OUTER JOIN [$(Dimensional)].Dimension.Class cl on a.ClassId = cl.ClassId and cl.Source=a.ClassIDSource
LEFT OUTER JOIN [$(Dimensional)].Dimension.Company co on a.CommunicationCompanyId = co.CompanyID
LEFT OUTER JOIN [$(Dimensional)].Dimension.Promotion p on a.PromotionId = p.PromotionId and a.PromotionSource=p.Source
--INNER JOIN [$(Dimensional)].Dimension.[Event] ev on a.EventId = ev.EventId
--Where a.BatchKey = @BatchKey and a.JobIDSource='ETE'
--and e.ToDate = '9999-12-31'
OPTION (RECOMPILE)

EXEC [Control].Sp_Log @BatchKey, 'Sp_ContactFactLoad_Email', 'Insert into Fact Contact', 'Success',@RowsProcessed=@@RowCount
INSERT INTO [$(Dimensional)].[Fact].[Contact]
           ([AccountKey]
           ,[AccountStatusKey]
		   ,[CommunicationId]
           ,[CommunicationKey]
           ,[StoryKey]
		   ,[ContactDate]
           ,[ChannelKey]
           ,[PromotionKey]
           ,[BetTypeKey]
           ,[ClassKey]
           ,[CommunicationCompanyKey]
           ,[InteractionTypeKey]
           ,[InteractionDayKey]
           ,[InteractionTimeKeyMaster]
		   ,[InteractionTimeKeyAnalysis]
           ,[Details]
		   ,[EventKey]
		   ,[CreatedDate]
		   ,[CreatedBatchKey]
		   ,[ModifiedDate]
		   ,[ModifiedBatchKey]
		   )
Select
	Distinct	
	a.AccountKey,
	a.AccountStatusKey,
	a.CommunicationId,
	a.CommunicationKey,
	a.StoryKey,
	a.EventDate,
	a.ChannelKey,
	a.PromotionKey,
	a.BetTypeKey,
	a.ClassKey,
	a.CommunicationCompanyKey,
	a.InteractionTypeKey,
	a.InteractionDayKey,
	a.InteractionTimeKeyMaster,
	ISNULL(TAD.TimeKey,-1) AS InteractionTimeKeyAnalysis,
	a.Details,
	IsNull(ev.EventKey, -1) EventKey,
	GetDate(),
	@BatchKey,
	GetDate(),
	@BatchKey
From #PreLoad a
LEFT OUTER JOIN [$(Dimensional)].Dimension.[Event] ev on a.EventId = ev.EventId and a.EventSource=ev.Source
LEFT OUTER JOIN [$(Dimensional)].Dimension.[TIME] TAD  WITH(NOLOCK) ON CONVERT(VARCHAR(5),CONVERT(TIME,DATEADD(MI,a.OffsetMinutes,a.EventDate)))=TAD.TimeText
LEFT OUTER JOIN [$(Dimensional)].fact.contact c on a.AccountKey = c.AccountKey and a.CommunicationKey = c.CommunicationKey and a.ChannelKey = c.ChannelKey and a.BettypeKey = c.BetTypeKey
										and   a.classKey = c.ClassKey and a.InteractionTypeKey = c.InteractionTypeKey and a.InteractionDayKey = c.InteractionDayKey
										and	  ISNULL(TAD.TimeKey,-1) = c.InteractionTimeKeyAnalysis and a.InteractionTimeKeyMaster = c.InteractionTimeKeyMaster	
										and   IsNull(ev.EventKey, -1) = c.EventKey
Where c.AccountKey is null
OPTION (RECOMPILE)

Drop table #Contact
Drop table #PreLoad

EXEC [Control].Sp_Log @BatchKey, 'Sp_ContactFactLoad_Email', NULL, 'Success',@RowsProcessed=@@RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_ContactFactLoad_Email', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

GO



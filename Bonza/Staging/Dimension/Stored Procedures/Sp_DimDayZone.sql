﻿CREATE PROCEDURE [Dimension].[Sp_DimDayZone]
(
	@BatchKey INT
)
WITH RECOMPILE
AS 
BEGIN TRY

	DECLARE @ProcName VARCHAR(255) = (SELECT OBJECT_NAME(@@PROCID));

	EXEC [Control].Sp_Log @BatchKey, @ProcName, 'Start','Start';

Declare @StartDate datetime2
Set @StartDate = (Select max(MasterDayText) from [$(Dimensional)].[Dimension].[DayZone] where Right (DayKey, 2) = '00')

While @StartDate < DateAdd(Year, 2, CONVERT(DATE,GetDate()))
BEGIN
	Set @StartDate = DateAdd(Day,1,@StartDate)

	INSERT INTO [$(Dimensional)].[Dimension].[DayZone]
	Select	a.*
	From
	(
	Select	CONVERT(INT,CONVERT(VARCHAR,DayKey) + '00') as DayKey, 
			DayKey as MasterDayKey,
			DayKey as AnalysisDayKey,
			DayText	as MasterDayText,
			DayText as AnalysisDayText,
			CONVERT(DateTime,DayText) as FromDate,
			CASE 
			WHEN b.OffSetMinutes = 30 THEN CONVERT(DateTime,(DayText +' 23:30:00.000'))
			ELSE CONVERT(DateTime,(DayText +' 22:30:00.000'))
			END AS ToDate,
			GetDate() as CreatedDate,
			@BatchKey as CreatedBatchKey,
			'Sp_DimDayZone' as CreatedBy,
			GetDate() as ModifiedDate,
			@BatchKey as ModifiedBatchKey,
			'Sp_DimDayZone' as ModifiedBy
	From [$(Dimensional)].[Dimension].[Day] a
	LEFT OUTER JOIN [Reference].[TimeZone] b on a.DayDate >= b.FromDate and a.DayDate < b.ToDate
	Where b.FromDate > '1980-01-01'
	and DayDate = CONVERT(DATE,@StartDate)
	UNION
	Select	CONVERT(INT,CONVERT(VARCHAR,DayKey) + '01') as DayKey, 
			DayKey as MasterDayKey,
			Convert(int,Replace (CONVERT(Varchar,DATEADD(DD,1,DayDate)),'-','')) as AnalysisDayKey,
			DayText	as MasterDayText,
			CONVERT(Varchar,DATEADD(DD,1,DayDate)) as AnalysisDayText,
			CASE 
			WHEN b.OffSetMinutes = 30 THEN CONVERT(DateTime,(DayText +' 23:30:00.000'))
			ELSE CONVERT(DateTime,(DayText +' 22:30:00.000'))
			END AS FromDate,
			CONVERT(DateTime,DATEADD(DD,1,DayDate)) as ToDate,
			GetDate() as CreatedDate,
			@BatchKey as CreatedBatchKey,
			'Sp_DimDayZone' as CreatedBy,
			GetDate() as ModifiedDate,
			@BatchKey as ModifiedBatchKey,
			'Sp_DimDayZone' as ModifiedBy
	From [$(Dimensional)].[Dimension].[Day]  a
	LEFT OUTER JOIN [Reference].[TimeZone] b on a.DayDate >= b.FromDate and a.DayDate < b.ToDate
	Where b.FromDate > '1980-01-01'
	and DayDate = CONVERT(DATE,@StartDate)
	UNION
	Select	CONVERT(INT,CONVERT(VARCHAR,DayKey) + '02') as DayKey, 
			DayKey as MasterDayKey,
			-1 as AnalysisDayKey,
			DayText	as MasterDayText,
			'N/A' as AnalysisDayText,
			null as FromDate,
			null as ToDate,
			GetDate() as CreatedDate,
			@BatchKey as CreatedBatchKey,
			'Sp_DimDayZone' as CreatedBy,
			GetDate() as ModifiedDate,
			@BatchKey as ModifiedBatchKey,
			'Sp_DimDayZone' as ModifiedBy
	From [$(Dimensional)].[Dimension].[Day] Where DayDate = CONVERT(DATE,@StartDate)
	UNION
	Select	CONVERT(INT,CONVERT(VARCHAR,DayKey) + '03') as DayKey, 
			-1 as MasterDayKey,
			DayKey as AnalysisDayKey,
			'N/A'	as MasterDayText,
			DayText as AnalysisDayText,
			null as FromDate,
			null as ToDate,
			GetDate() as CreatedDate,
			@BatchKey as CreatedBatchKey,
			'Sp_DimDayZone' as CreatedBy,
			GetDate() as ModifiedDate,
			@BatchKey as ModifiedBatchKey,
			'Sp_DimDayZone' as ModifiedBy
	From [$(Dimensional)].[Dimension].[Day] Where DayDate = CONVERT(DATE,@StartDate)
	) a

--	Set @StartDate = (Select max(MasterDayText) from [$(Dimensional)].[Dimension].[DayZone] where Right (DayKey, 2) = '00')
END

	EXEC [Control].Sp_Log @BatchKey, @ProcName, NULL,'Success';
	
END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, @ProcName, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

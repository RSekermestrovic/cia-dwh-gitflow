CREATE PROCEDURE [Dimension].[Sp_DimUser]
	@BatchKey int
WITH RECOMPILE
AS 
BEGIN TRY

SET NOCOUNT ON;

BEGIN TRANSACTION PreProd_User;

EXEC [Control].Sp_Log	@BatchKey,'Sp_DimUser','Insert','Start'



INSERT INTO [$(Dimensional)].[Dimension].[User] (UserId,Source,SystemName,EmployeeKey,Forename,Surname,PayrollNumber,FromDate,ToDate,FirstDate,CreatedDate,CreatedBatchKey,CreatedBy,ModifiedDate,ModifiedBatchKey,ModifiedBy)
SELECT
	isnull(s.UserID,-1)																													AS UserID,
	isnull(s.Source,'Unk')																												AS Source,
	isnull(s.SystemName,'Unknown')																										AS SystemName,
	CASE WHEN Dim_EmployeeKey.EmployeeKey IS NULL 
		 THEN	(SELECT ISNULL(MAX(A.EmployeeKey),0) FROM [$(Dimensional)].Dimension.[User] AS A)+
					DENSE_RANK() OVER (PARTITION BY Dim_EmployeeKey.EmployeeKey ORDER BY s.Forename,s.Surname)
		 ELSE Dim_EmployeeKey.EmployeeKey 
	END 																																AS EmployeeKey,
	isnull(s.Forename,'')																												AS Forename,
	isnull(s.Surname,'')																												AS Surname,
	s.PayrollNumber																														AS PayrollNumber,
	s.FromDate																															AS FromDate,
	'9999-12-31'																														AS ToDate,
	CURRENT_TIMESTAMP																													AS FirstDate,
	CURRENT_TIMESTAMP																													AS CreatedDate,
	@BatchKey																															AS CreatedBatchKey,
	'Sp_DimUser'																													AS CreatedBy,
	CURRENT_TIMESTAMP																													AS ModifiedDate,
	@BatchKey																															AS ModifiedBatchKey,
	'Sp_DimUser'																													AS ModifiedBy

FROM Stage.[User] s with (nolock)
LEFT OUTER JOIN [$(Dimensional)].[Dimension].[User] D with (nolock) on (s.UserID=D.UserID and s.Source=D.Source)
LEFT OUTER JOIN (select distinct UserKey,UserID,Source from [$(Dimensional)].[Dimension].[User]) Dim_UserKey 
			on (s.UserID=Dim_UserKey.UserID and s.Source=Dim_UserKey.Source)
LEFT OUTER JOIN (select distinct EmployeeKey ,Forename,Surname from [$(Dimensional)].[Dimension].[User]) Dim_EmployeeKey 
			on (s.Forename=Dim_EmployeeKey.Forename and s.Surname=Dim_EmployeeKey.Surname)
WHERE D.UserID is null and s.BatchKey=@BatchKey;


UPDATE D
SET
	d.employeeKey=CASE WHEN Dim_EmployeeKey.EmployeeKey IS NULL 
						THEN (SELECT ISNULL(MAX(A.EmployeeKey),0) FROM [$(Dimensional)].Dimension.[User] AS A) + (select DENSE_RANK() OVER (PARTITION BY Dim_EmployeeKey.EmployeeKey ORDER BY s.Forename,s.Surname))
						ELSE Dim_EmployeeKey.EmployeeKey 
					End,
	d.Forename=isnull(s.Forename,''),
	d.Surname=isnull(s.Surname,''),
	d.PayrollNumber=s.PayrollNumber,
	d.FromDate=s.fromDate,
	D.ModifiedDate=CURRENT_TIMESTAMP,
	D.ModifiedBatchKey=@BatchKey,
	D.ModifiedBy='Sp_DimUser'
from [$(Dimensional)].[Dimension].[User] D with (nolock)
INNER JOIN Stage.[User] s with (nolock) on (s.UserID=D.UserID and s.Source=D.Source)
LEFT OUTER JOIN (select distinct UserKey,UserID,Source from [$(Dimensional)].[Dimension].[User]) Dim_UserKey 
			on (s.UserID=Dim_UserKey.UserID and s.Source=Dim_UserKey.Source)
LEFT OUTER JOIN (select distinct EmployeeKey ,Forename,Surname from [$(Dimensional)].[Dimension].[User]) Dim_EmployeeKey 
			on (s.Forename=Dim_EmployeeKey.Forename and s.Surname=Dim_EmployeeKey.Surname)
where s.BatchKey=@BatchKey

EXEC [Control].Sp_Log	@BatchKey,'Sp_DimUser',NULL,'Success'	
COMMIT TRANSACTION PreProd_User;

END TRY
BEGIN CATCH

	Rollback TRANSACTION PreProd_User;
	declare @Error int;
	declare @ErrorMessage varchar(max);
	                           -- Log the failure at the object level
                           SET    @Error = ERROR_NUMBER()
                           SET    @ErrorMessage = ERROR_MESSAGE()
						   EXEC [Control].Sp_Log	@BatchKey, 'Sp_DimUser', NULL, 'Failed', @ErrorMessage;
						   Raiserror(@ErrorMessage,16,1)

END CATCH
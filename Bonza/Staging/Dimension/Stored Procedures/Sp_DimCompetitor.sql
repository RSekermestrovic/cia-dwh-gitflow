﻿CREATE PROCEDURE [Dimension].[Sp_DimCompetitor]
	@BatchKey int
WITH RECOMPILE
AS 
BEGIN

BEGIN TRY

SET NOCOUNT ON;

BEGIN TRANSACTION Prod_Competitor ;

EXEC [Control].Sp_Log	@BatchKey,'Sp_DimCompetitor','UpdateCompetitorDimension','Start'		
UPDATE DC
SET
	DC.SaddleNumber=C.SaddleNumber,
	DC.CompetitorName=C.CompetitorName,
	DC.Scratched=C.Scratched,
	DC.Jockey=C.Jockey,
	DC.Draw=C.Draw,
	DC.Form=C.Form,
	DC.History=C.History,
	DC.[Weight]=C.[Weight],
	DC.Trainer=C.Trainer,
	DC.ManualIntPrice=C.ManualIntPrice,
	DC.BestTime=C.BestTime,
	DC.ScratchedLate=C.ScratchedLate,
	DC.rulesOverrideState=C.rulesOverrideState,
	DC.[ManualIntPriceCB]=C.[ManualIntPriceCB],
	DC.[TabRollCount]=C.[TabRollCount] ,
	DC.[IsKnockedOut]=C.[IsKnockedOut] ,
	DC.[EVENT_SK]=C.[EVENT_SK] ,
	DC.[ManualIntPriceTW]=C.[ManualIntPriceTW] ,
	DC.FromDate=C.fromDate,
	DC.ModifiedDate=CURRENT_TIMESTAMP,
	DC.ModifiedBatchKey=@BatchKey,
	DC.ModifiedBy='Sp_DimCompetitor'
from [$(Dimensional)].Dimension.[Competitor] DC with (nolock)
inner join  Stage.[Competitor] C with (nolock) on C.CompetitorID=DC.CompetitorID and C.CompetitorSource=DC.CompetitorSource
where C.BatchKey=@BatchKey

EXEC [Control].Sp_Log	@BatchKey,'Sp_DimCompetitor','InsertIntoCompetitorDimension','Start'

INSERT INTO [$(Dimensional)].[Dimension].[Competitor] ([CompetitorID],[CompetitorSource],[SaddleNumber],[CompetitorName],[Scratched],[Jockey],[Draw],	
														[Form],	[History],[Weight],	[Trainer] ,[ManualIntPrice],[bestTime] ,[scratchedLate],[rulesOverrideState],	[ManualIntPriceCB],	[TabRollCount],	
														[IsKnockedOut],	[EVENT_SK],	[ManualIntPriceTW],	[Fromdate],[ToDate],[FirstDate],[CreatedDate],[CreatedBatchKey],[CreatedBy],[ModifiedDate],
														[ModifiedBatchKey],[ModifiedBy])
SELECT
	C.CompetitorID,
	C.CompetitorSource,
	C.SaddleNumber,
	C.CompetitorName,
	C.Scratched,
	C.Jockey,
	C.Draw,
	C.Form,
	C.History,
	C.[Weight],
	C.Trainer,
	C.ManualIntPrice,
	C.BestTime,
	C.ScratchedLate,
	C.rulesOverrideState,
	C.[ManualIntPriceCB],
	C.[TabRollCount] ,
	C.[IsKnockedOut] ,
	C.[EVENT_SK] ,
	C.[ManualIntPriceTW] ,
	C.FromDate																												AS FromDate,
	'9999-12-31'																											AS ToDate,
	CURRENT_TIMESTAMP																										AS FirstDate,
	CURRENT_TIMESTAMP																										AS CreatedDate,
	@BatchKey																												AS CreatedBatchKey,
	'Sp_DimCompetitor'																											AS CreatedBy,
	CURRENT_TIMESTAMP																										AS ModifiedDate,
	@BatchKey																												AS ModifiedBatchKey,
	'Sp_DimCompetitor'																											AS ModifiedBy

FROM Stage.[Competitor] C with (nolock)
LEFT OUTER JOIN [$(Dimensional)].Dimension.[Competitor] C1 with (nolock) on (C.CompetitorID=C1.CompetitorID and C.CompetitorSource=C1.CompetitorSource)
where C.BatchKey=@BatchKey and C1.CompetitorID is null	;
	

EXEC [Control].Sp_Log	@BatchKey,'Sp_DimCompetitor','InsertIntoCompetitorDimension','Success'	
COMMIT TRANSACTION Prod_Competitor;

END TRY
BEGIN CATCH

	Rollback TRANSACTION Prod_Competitor;
	declare @Error int;
	declare @ErrorMessage varchar(max);
	                           -- Log the failure at the object level
                           SET    @Error = ERROR_NUMBER()
                           SET    @ErrorMessage = ERROR_MESSAGE()

	EXEC [Control].Sp_Log @BatchKey, 'Sp_DimCompetitor', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

END
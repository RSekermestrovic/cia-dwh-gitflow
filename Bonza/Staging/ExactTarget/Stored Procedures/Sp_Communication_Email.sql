﻿CREATE PROC [ExactTarget].[Sp_Communication_Email]
(
	@FromDate	datetime2(3),
	@ToDate		datetime2(3),
	@BatchKey	int
)
WITH RECOMPILE
AS 

BEGIN TRY

EXEC [Control].Sp_Log @BatchKey,'Sp_Communication_Email','InsertIntoStagingET','Start'

INSERT INTO [Stage].[Communication]
Select	Distinct
		@BatchKey BatchKey,
		JobID,
		'ETE' as Source,
		EmailName as Title,
		EmailSubject as Details,
		null as SubDetails
From [$(Acquisition)].ExactTarget.Job
Where FromDate >= @FromDate and FromDate < @ToDate

EXEC [Control].Sp_Log @BatchKey, 'Sp_Communication_Email', NULL, 'Success',@RowsProcessed=@@RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Communication_Email', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

GO



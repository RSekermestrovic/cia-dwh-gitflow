﻿CREATE PROC [ExactTarget].[Sp_Contact_Push]
(
	@FromDate	datetime2(3),
	@ToDate		datetime2(3),
	@BatchKey	int
)
WITH RECOMPILE
AS 

BEGIN TRY

EXEC [Control].Sp_Log @BatchKey,'Sp_Contact_Push','InsertIntoStaging','Start'

INSERT INTO Stage.Contact
Select	
	Distinct 
	@BatchKey BatchKey,
	b.clientID AccountId,
	'IAC' AccountSource,
	MessageID as JobID,
	'ETP' as JobIDSource,
	'N/A' as Story,
	CASE 
		WHEN [Platform] like '%iphone%' THEN 8888
		WHEN [Platform] like '%Android%' THEN 8889
		ELSE 8887
	END AS 	ChannelID,
	0 PromotionId,
	'N/A' PromotionSource,
	'N/A' BetType,
	'N/A' BetGroupType,
	'N/A' BetSubType,
	'N/A' GroupingType,
	'N/A' Legtype,
	0 classId,
	'IBT' ClassIDSource,
	0 CommunicationCompanyId,
	0 EventId,
	'N/A' EventSource,
	'SNT' InteractionTypeId,
	'ETP' InteractionTypeSource,
	DATEADD(minute,930,a.SendDateTime) as EventDate, --15.5 hour difference between Indianopolis and Darwin
	CONVERT(time,DATEADD(minute,930,a.SendDateTime)) EventTime,
	'' Details,
	a.FromDate
From [$(Acquisition)].[ExactTargetPush].[PushReport] a
INNER JOIN [$(Acquisition)].IntraBet.tblSubscriptionCentre b on a.SubscriberKey = Cast(b.exactTargetID as varchar(36))
Where a.FromDate >= @FromDate and a.FromDate < @ToDate

UNION ALL

Select	
	Distinct 
	@BatchKey BatchKey,
	b.clientID AccountId,
	'IAC' AccountSource,
	MessageID as JobID,
	'ETP' as JobIDSource,
	'N/A' as Story,
	CASE 
		WHEN [Platform] like '%iphone%' THEN 8888
		WHEN [Platform] like '%Android%' THEN 8889
		ELSE 8887
	END AS 	ChannelID,
	0 PromotionId,
	'N/A' PromotionSource,
	'N/A' BetType,
	'N/A' BetGroupType,
	'N/A' BetSubType,
	'N/A' GroupingType,
	'N/A' Legtype,
	0 classId,
	'IBT' ClassIDSource,
	0 CommunicationCompanyId,
	0 EventId,
	'N/A' EventSource,
	'OPN' InteractionTypeId,
	'ETP' InteractionTypeSource,
	DATEADD(minute,930,a.OpenDate) as EventDate, --15.5 hour difference between Indianopolis and Darwin
	CONVERT(time,DATEADD(minute,930,a.OpenDate)) EventTime,
	'' Details,
	a.FromDate
From [$(Acquisition)].[ExactTargetPush].[PushReport] a
INNER JOIN [$(Acquisition)].IntraBet.tblSubscriptionCentre b on a.SubscriberKey = Cast(b.exactTargetID as varchar(36))
Where a.MessageOpened='yes' and a.FromDate >= @FromDate and a.FromDate < @ToDate

UNION ALL

Select	
	Distinct 
	@BatchKey BatchKey,
	b.clientID AccountId,
	'IAC' AccountSource,
	MessageID as JobID,
	'ETP' as JobIDSource,
	'N/A' as Story,
	CASE 
		WHEN [Platform] like '%iphone%' THEN 8888
		WHEN [Platform] like '%Android%' THEN 8889
		ELSE 8887
	END AS 	ChannelID,
	0 PromotionId,
	'N/A' PromotionSource,
	'N/A' BetType,
	'N/A' BetGroupType,
	'N/A' BetSubType,
	'N/A' GroupingType,
	'N/A' Legtype,
	0 classId,
	'IBT' ClassIDSource,
	0 CommunicationCompanyId,
	0 EventId,
	'N/A' EventSource,
	'FDL' InteractionTypeId,
	'ETP' InteractionTypeSource,
	DATEADD(minute,930,a.SendDateTime) as EventDate, --15.5 hour difference between Indianopolis and Darwin
	CONVERT(time,DATEADD(minute,930,a.SendDateTime)) EventTime,
	'' Details,
	a.FromDate
From [$(Acquisition)].[ExactTargetPush].[PushReport] a
INNER JOIN [$(Acquisition)].IntraBet.tblSubscriptionCentre b on a.SubscriberKey = Cast(b.exactTargetID as varchar(36))
Where a.[Status]='Failed' and a.FromDate >= @FromDate and a.FromDate < @ToDate

EXEC [Control].Sp_Log @BatchKey, 'Sp_Contact_Push', NULL, 'Success',@RowsProcessed=@@RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Contact_Push', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

GO



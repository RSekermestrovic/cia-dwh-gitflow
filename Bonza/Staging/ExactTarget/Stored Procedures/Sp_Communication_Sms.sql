﻿CREATE PROC [ExactTarget].[Sp_Communication_Sms]
(
	@FromDate	datetime2(3),
	@ToDate		datetime2(3),
	@BatchKey	int
)
WITH RECOMPILE
AS 

BEGIN TRY

EXEC [Control].Sp_Log @BatchKey,'Sp_Communication_Sms','InsertIntoStagingETSMS','Start'

INSERT INTO [Stage].[Communication]
SELECT	Distinct
		@BatchKey BatchKey,
		MessageName as JobId,
		'ETS' as Source,
		MessageName as Title,
		IsNull(MAX(b.MessageText),'') as MessageText,
		null as SubDetails
  FROM [$(Acquisition)].[ExactTargetSms].[SmsReport] a
  LEFT OUTER JOIN [$(Acquisition)].[ExactTargetSms].[LifecycleSmsPush] b on a.MessageName = b.AssetID and b.Channel = 'SMS'
  Where a.FromDate >= @FromDate and a.FromDate < @ToDate
  Group By MessageName

EXEC [Control].Sp_Log @BatchKey, 'Sp_Communication_Sms', NULL, 'Success',@RowsProcessed=@@RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Communication_Sms', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

GO



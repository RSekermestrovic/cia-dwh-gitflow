﻿CREATE TABLE [GenWeb].[Transaction] (
    [BatchKey]                   INT           NOT NULL,
    [TransactionId]              BIGINT        NOT NULL,
    [TransactionIdSource]        CHAR (3)      NOT NULL,
    [BalanceName]                VARCHAR (32)  NOT NULL,
    [TransactionDetailId]        CHAR (4)      NOT NULL,
    [ContractID]                 BIGINT        NOT NULL,
    [ContractIDSource]           CHAR (3)      NOT NULL,
    [BetId]                      BIGINT        NOT NULL,
    [BetIdSource]                VARCHAR (32)  NULL,
    [LegId]                      BIGINT        NULL,
    [LegIdSource]                VARCHAR (32)  NULL,
    [FreeBetID]                  BIGINT        NULL,
    [ExternalID]                 VARCHAR (32)  NULL,
    [External1]                  VARCHAR (32)  NULL,
    [External2]                  VARCHAR (32)  NULL,
    [UserID]                     VARCHAR (32)  NOT NULL,
    [InstrumentID]               INT           NOT NULL,
    [AccountId]                  BIGINT        NOT NULL,
    [AccountIDSource]            CHAR (3)      NOT NULL,
    [WalletId]                   INT           NOT NULL,
    [WalletIDSource]             CHAR (3)      NOT NULL,
    [MasterTransactionTimestamp] DATETIME2 (7) NOT NULL,
    [MasterDayText]              VARCHAR (11)  NOT NULL,
    [MasterTimeText]             VARCHAR (5)   NOT NULL,
    [ChannelName]                VARCHAR (32)  NOT NULL,
    [CampaignId]                 INT           NOT NULL,
    [TransactedAmount]           MONEY         NOT NULL,
    [ContractType]               VARCHAR (32)  NOT NULL,
    [BetTypeName]                VARCHAR (32)  NOT NULL,
    [BetSubTypeName]             VARCHAR (32)  NOT NULL,
    [LegBetTypeName]             VARCHAR (32)  NOT NULL,
    [GroupingType]               VARCHAR (32)  NOT NULL,
    [ClassId]                    INT           NOT NULL,
    [LegNumber]                  TINYINT       NOT NULL,
    [NumberOfLegs]               TINYINT       NOT NULL,
    [ExistsInDim]                TINYINT       NOT NULL,
    [Conditional]                SMALLINT      NOT NULL,
    [MappingType]                TINYINT       NOT NULL,
    [MappingId]                  SMALLINT      NOT NULL,
    [FromDate]                   DATETIME2 (3) NOT NULL,
    [DateTimeUpdated]            INT           NULL
) ON [par_sch_int_Stage] ([BatchKey])
WITH (DATA_COMPRESSION = ROW );













﻿
CREATE FUNCTION dbo.fn_ActionBitmap
(	
	@ActionBitmap INT
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT
		[ActionBitmap],
		[Action]
	FROM (	
			SELECT 1 ActionBitmap, 'Request' Action UNION ALL
			SELECT 2 ActionBitmap, 'Accept' Action  UNION ALL
			SELECT 4 ActionBitmap, 'Decline' Action UNION ALL
			SELECT 8 ActionBitmap, 'Cancel' Action
						) as qry
   WHERE qry.ActionBitmap = qry.ActionBitmap & @ActionBitmap
)
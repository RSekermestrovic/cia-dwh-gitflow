CREATE proc [Cleanse].[Sp_Instrument_BPay]
(
	@BatchKey int = 0,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @RowCount INT;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

BEGIN TRY

	EXEC [Control].Sp_Log @BatchKey, 'Cleanse.Sp_Instrument_BPay', 'Cache', 'Start'

	SELECT	InstrumentKey,
			AccountNumber,
			SafeAccountNumber,
			BSB
			
	INTO	#Instrument
	FROM	[$(Dimensional)].Dimension.Instrument 
	WHERE ModifiedBatchKey = CASE @BatchKey WHEN 0 THEN ModifiedBatchKey ELSE @BatchKey END




	EXEC [Control].Sp_Log @BatchKey, 'Cleanse.Sp_Instrument_BPay', 'Account Number(Ref Number)', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	u
	SET		AccountNumber = UpdatedAccountNumber
	FROM	(	SELECT	AccountNumber,
						CASE ISNULL(AccountNumber,'')
							WHEN '' THEN ''
							WHEN ' ' THEN ''
							WHEN 'N/A' THEN ''
							WHEN 'Unknown' THEN ''
							ELSE Cleanse.ExtractNumbersOnly(Replace(AccountNumber,'''',''''))
						END UpdatedAccountNumber
				FROM	#Instrument
			) u
	WHERE	ISNULL(AccountNumber,'!') <> UpdatedAccountNumber




	EXEC [Control].Sp_Log @BatchKey, 'Cleanse.Sp_Instrument_BPay', 'Safe Account Number(Safe Ref Number)', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	u
	SET		SafeAccountNumber = isnull((Replicate('*',Len(UpdatedSafeAccountNumber)-4)+Right(UpdatedSafeAccountNumber,4)),'')
	FROM	(	SELECT	SafeAccountNumber,
						CASE ISNULL(SafeAccountNumber,'')
							WHEN '' THEN ''
							WHEN ' ' THEN ''
							WHEN 'N/A' THEN ''
							WHEN 'Unknown' THEN ''
							ELSE Cleanse.ExtractNumbersOnly(Replace(AccountNumber,'''',''''))
						END UpdatedSafeAccountNumber
				FROM	#Instrument
			) u
	WHERE	ISNULL(SafeAccountNumber,'!') <> UpdatedSafeAccountNumber




	EXEC [Control].Sp_Log @BatchKey, 'Cleanse.Sp_Instrument_BPay', 'BSB(Biller Code)', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	u
	SET		BSB= UpdatedBSB
	FROM	(	SELECT	BSB,
						CASE ISNULL(BSB,'')
							WHEN '' THEN ''
							WHEN ' ' THEN ''
							WHEN 'N/A' THEN ''
							WHEN 'Unknown' THEN ''
							ELSE Cleanse.ExtractNumbersOnly(Replace(BSB,'''',''''))
						END UpdatedBSB
				FROM	#Instrument
			) u
	WHERE	ISNULL(BSB,'!') <> UpdatedBSB


    EXEC [Control].Sp_Log @BatchKey, 'Cleanse.Sp_Instrument_BPay', 'Update', 'Start', @RowsProcessed = @@ROWCOUNT


	UPDATE	i
	SET		AccountNumber = u.AccountNumber,
			SafeAccountNumber = u.SafeAccountNumber,
			BSB = u.BSB

	FROM	[$(Dimensional)].Dimension.Instrument i
			INNER JOIN #Instrument u ON u.InstrumentKey = i.InstrumentKey
	WHERE	ISNULL(i.AccountNumber,'!') <> ISNULL(u.AccountNumber,'!')
			OR ISNULL(i.SafeAccountNumber,'!') <> ISNULL(u.SafeAccountNumber,'!')
			OR ISNULL(i.BSB,'!') <> ISNULL(u.BSB,'!')

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey, 'Cleanse.Sp_Instrument_BPay', NULL, 'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Cleanse.Sp_Instrument_BPay', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

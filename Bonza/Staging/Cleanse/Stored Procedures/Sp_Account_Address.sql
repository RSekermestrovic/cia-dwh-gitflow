﻿CREATE proc [Cleanse].[Sp_Account_Address]
(
	@BatchKey int = 0,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @RowCount INT;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

BEGIN TRY

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Account_Address', 'Cache Accounts', 'Start'

	SELECT	AccountKey,
			Address1,
			Address2,
			Suburb,
			StateCode,
			State,
			PostCode,
			CountryCode,
			Country,
			AustralianClient,
			GeographicalLocation,
			ISNULL(LTRIM(RTRIM(CASE 1 WHEN HasHomeAddress THEN HomeAddress1 WHEN HasPostalAddress THEN PostalAddress1 WHEN HasBusinessAddress THEN BusinessAddress1 WHEN HasOtherAddress THEN OtherAddress1 ELSE NULL END)), 'Unknown') RawAddress1,
			ISNULL(LTRIM(RTRIM(CASE 1 WHEN HasHomeAddress THEN HomeAddress2 WHEN HasPostalAddress THEN PostalAddress2 WHEN HasBusinessAddress THEN BusinessAddress2 WHEN HasOtherAddress THEN OtherAddress2 ELSE NULL END)), 'Unknown') RawAddress2,
			ISNULL(LTRIM(RTRIM(Replace(Replace(Replace(Replace(CASE 1 WHEN HasHomeAddress THEN HomeSuburb WHEN HasPostalAddress THEN PostalSuburb WHEN HasBusinessAddress THEN BusinessSuburb WHEN HasOtherAddress THEN OtherSuburb ELSE NULL END + ' ','Mt.','Mount'),'Mt ','Mount '),'Nth ','North '),'Sth ','South '))), 'Unknown') RawSuburb,
			ISNULL(LTRIM(RTRIM(CASE 1 WHEN HasHomeAddress THEN HomeStateCode WHEN HasPostalAddress THEN PostalStateCode WHEN HasBusinessAddress THEN BusinessStateCode WHEN HasOtherAddress THEN OtherStateCode ELSE NULL END)), 'UNK') RawStateCode,
			ISNULL(LTRIM(RTRIM(CASE 1 WHEN HasHomeAddress THEN HomeState WHEN HasPostalAddress THEN PostalState WHEN HasBusinessAddress THEN BusinessState WHEN HasOtherAddress THEN OtherState ELSE NULL END)), 'Unknown') RawState,
			ISNULL(LTRIM(RTRIM(CASE 1 WHEN HasHomeAddress THEN HomePostCode WHEN HasPostalAddress THEN PostalPostCode WHEN HasBusinessAddress THEN BusinessPostCode WHEN HasOtherAddress THEN OtherPostCode ELSE NULL END)), 'Unknown') RawPostCode,
			ISNULL(LTRIM(RTRIM(CASE 1 WHEN HasHomeAddress THEN HomeCountryCode WHEN HasPostalAddress THEN PostalCountryCode WHEN HasBusinessAddress THEN BusinessCountryCode WHEN HasOtherAddress THEN OtherCountryCode ELSE NULL END)), 'UNK') RawCountryCode,
			ISNULL(LTRIM(RTRIM(CASE 1 WHEN HasHomeAddress THEN HomeCountry WHEN HasPostalAddress THEN PostalCountry WHEN HasBusinessAddress THEN BusinessCountry WHEN HasOtherAddress THEN OtherCountry ELSE NULL END)), 'Unknown') RawCountry,
			CONVERT(varchar(60), NULL) UpdatedAddress1,
			CONVERT(varchar(60), NULL) UpdatedAddress2,
			CONVERT(varchar(50), NULL) UpdatedSuburb,
			CONVERT(varchar(3), NULL) UpdatedStateCode,
			CONVERT(varchar(30), NULL) UpdatedState,
			CONVERT(varchar(7), NULL) UpdatedPostCode,
			CONVERT(varchar(3), NULL) UpdatedCountryCode,
			CONVERT(varchar(40), NULL) UpdatedCountry,
			CONVERT(varchar(3), NULL) UpdatedAustralianClient,
			CONVERT(geography, NULL) UpdatedGeographicalLocation
	INTO	#Accounts 
	FROM	[$(Dimensional)].Dimension.Account 
	WHERE	ModifiedBatchKey = CASE @BatchKey WHEN 0 THEN ModifiedBatchKey ELSE @BatchKey END

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Account_Address', 'Postcode+Suburb(Perfect match)', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	u
	SET		UpdatedSuburb = Cleanse.[CamelCase](CASE Outcome WHEN 0 THEN Suburb0 WHEN 1 THEN Suburb1 WHEN 2 THEN Suburb2 ELSE 'Unknown' END),
			UpdatedPostCode = RIGHT('0' + CASE Outcome WHEN 0 THEN PostCode0 WHEN 1 THEN PostCode1 WHEN 2 THEN PostCode2 ELSE 'Unknown' END,4),
			UpdatedStateCode = CASE Outcome WHEN 0 THEN StateCode0 WHEN 1 THEN StateCode1 WHEN 2 THEN StateCode2 ELSE 'Unknown' END,
			UpdatedCountryCode = 'AU'
	FROM	(	SELECT	u.AccountKey,
						r.Suburb Suburb0,
						r.StateCode StateCode0,
						r.PostCode PostCode0,
						r1.Suburb Suburb1,
						r1.StateCode StateCode1,
						r1.PostCode PostCode1,
						r2.Suburb Suburb2,
						r2.StateCode StateCode2,
						r2.PostCode PostCode2,
						CASE	-- The gereral rule is take any suburb which is a perfect match with postcode from the earliest point in the address where it is found (i.e. end of address1, then end of address2, then suburb)
								-- However there are some exceptions we need to pick up first where a matched suburb in address line 1 might actually be the street name (e.g. 35 Kingsway), particularly where a number of address1's
								-- have had their street type truncated (e.g. 37 Sydney St has been truncated to 37 Sydney)
							WHEN REPLACE(u.Address1,' ','') = REPLACE(r1.Suburb,' ','') THEN 1 -- If the entirety of Address1 is a matching suburb then the Address is badly corrupted and keep the matched suburb as the best we can do
							WHEN REPLACE(REPLACE(u.Address1,' ',''), REPLACE(r1.Suburb,' ',''),'') NOT LIKE '%[A-Z]%' AND r2.Suburb IS NOT NULL THEN 2 -- If the suburb matched to Address 1 leaves only numbers and slashes etc, then it was more likely a street name if a good match for a suburb has been found in address 2
							WHEN REPLACE(REPLACE(u.Address1,' ',''), REPLACE(r1.Suburb,' ',''),'') NOT LIKE '%[A-Z]%' AND r.Suburb IS NOT NULL THEN 0 -- As above if a good match found to the actual suburb
							WHEN u.Address2 LIKE 'PO%Box%' AND r1.Suburb IS NOT NULL THEN 1 -- If Address 2 contains a PO Box number, then any suburb it contains could relate to the PO Box address rather than Address1 primary Address, so only take Address1 suburb match or actual suburb match in that order
							WHEN u.Address2 LIKE 'PO%Box%' AND r.Suburb IS NOT NULL THEN 0 -- As above taking suburb match if no Address1 suburb match 
							WHEN r2.PostCode <> r1.Postcode THEN 1 -- If postcode of any suburb matched to Address 2 differs from the postcode of suburb matched to Address1 or the actual Suburb column, then Address2 could contain a secondary address so only condsider suburbs matched to Address1 and Suburb columns 
							WHEN r2.PostCode <> r.Postcode THEN 0 -- As above taking suburb match if no Address1 suburb match  
							WHEN r1.Suburb IS NOT NULL THEN 1 -- Otherwise, as described above resort to using the matching suburb found at the earliest point in the address hierarchy
							WHEN r2.Suburb IS NOT NULL THEN 2
							WHEN r.Suburb IS NOT NULL THEN 0
							ELSE NULL
						END Outcome,
						u.UpdatedSuburb,
						u.UpdatedStateCode,
						u.UpdatedPostCode,
						u.UpdatedCountryCode,
						ROW_NUMBER() OVER (PARTITION BY u.AccountKey ORDER BY CASE WHEN r1.StateCode = u.RawStateCode THEN 1 ELSE 2 END, LEN(r1.Suburb) DESC, CASE WHEN r2.StateCode = u.RawStateCode THEN 1 ELSE 2 END, LEN(r2.Suburb) DESC, CASE WHEN r.StateCode = u.RawStateCode THEN 1 ELSE 2 END, LEN(r.Suburb) DESC) Rnk -- Where there are 2 suburbs in the one postcode and the name of one is a superset of the other (e.g. EAST BRIGHTON and BRIGHTON) we can end up with a double match - in these circumstances, take the one that matched the most characters
				FROM	(	SELECT	AccountKey,
									CASE
										WHEN Address1 LIKE '%' + RawStateCode THEN RTRIM(REPLACE(Address1,RawStateCode,''))
										ELSE Address1
									END Address1,
									Address1PostCode,
									CASE
										WHEN Address2 LIKE '%' + RawStateCode THEN RTRIM(REPLACE(Address2,RawStateCode,''))
										ELSE Address2
									END Address2,
									Address2PostCode,
									RawSuburb,
									RawPostCode,
									RawStateCode,
									UpdatedSuburb,
									UpdatedStateCode,
									UpdatedPostCode,
									UpdatedCountryCode
							FROM	(	SELECT	AccountKey,
												CASE
													WHEN RTRIM(RawAddress1) LIKE '%_[0-9][0-9][0-9][0-9]' THEN RTRIM(REPLACE(REPLACE(REPLACE(RawAddress1,',',''),'.',''),right(RTRIM(RawAddress1),4),''))
													ELSE RTRIM(RawAddress1)
												END Address1,
												CASE
													WHEN RTRIM(RawAddress1) LIKE '%_[0-9][0-9][0-9][0-9]' THEN RIGHT(RTRIM(RawAddress1),4)
													ELSE RawPostCode
												END Address1PostCode, 
												CASE
													WHEN RTRIM(RawAddress2) LIKE '%_[0-9][0-9][0-9][0-9]' THEN RTRIM(REPLACE(REPLACE(REPLACE(RawAddress2,',',''),'.',''),right(RTRIM(RawAddress2),4),''))
													ELSE RTRIM(RawAddress2)
												END Address2, 
												CASE
													WHEN RTRIM(RawAddress2) LIKE '%_[0-9][0-9][0-9][0-9]' THEN RIGHT(RTRIM(RawAddress2),4)
													ELSE RawPostCode
												END Address2PostCode,
												RawSuburb,
												RawPostCode,
												RawStateCode,
												UpdatedSuburb,
												UpdatedStateCode,
												UpdatedPostCode,
												UpdatedCountryCode
										FROM	#Accounts 
									) x
						) u
						LEFT JOIN [Reference].[Suburb] r ON TRY_CONVERT(int,r.PostCode) = TRY_CONVERT(int,u.RawPostCode) 
															AND REPLACE(u.RawSuburb,' ','') = REPLACE(r.Suburb,' ','')
						LEFT JOIN [Reference].[Suburb] r1 ON TRY_CONVERT(int,r1.PostCode) = TRY_CONVERT(int,u.Address1PostCode) 
															AND REPLACE(u.Address1,' ','') LIKE '%' + REPLACE(r1.Suburb,' ','')
						LEFT JOIN [Reference].[Suburb] r2 ON TRY_CONVERT(int,r2.PostCode) = TRY_CONVERT(int,u.Address2PostCode) 
															AND REPLACE(u.Address2,' ','') LIKE '%' + REPLACE(r2.Suburb,' ','')
				WHERE	r.Suburb IS NOT NULL
						OR r1.Suburb IS NOT NULL
						OR r2.Suburb IS NOT NULL
			) u
	WHERE	Rnk = 1

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Account_Address', 'Postcode+Suburb(Word Resequence)', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	u
	SET		UpdatedSuburb = Cleanse.[CamelCase](r.Suburb),
			UpdatedPostcode = RIGHT('0' + r.Postcode, 4),
			UpdatedStateCode = r.StateCode,
			UpdatedCountryCode = 'AU'
	FROM	#Accounts u
			INNER JOIN [Reference].[Suburb] r ON TRY_CONVERT(int,r.PostCode) = TRY_CONVERT(int,u.RawPostCode) 
												AND REPLACE(r.Suburb,' ','') = REPLACE(SUBSTRING(RawSuburb, CHARINDEX(' ',RawSuburb) + 1, LEN(RawSuburb) - CHARINDEX(' ',RawSuburb)) + SUBSTRING(RawSuburb, 1, CHARINDEX(' ',RawSuburb) - 1),' ','')
	WHERE UpdatedSuburb IS NULL AND RawSuburb LIKE '% %'

	UPDATE	u
	SET		UpdatedSuburb = Cleanse.[CamelCase](r.Suburb),
			UpdatedPostcode = RIGHT('0' + r.Postcode, 4),
			UpdatedStateCode = r.StateCode,
			UpdatedCountryCode = 'AU'
	FROM	#Accounts u
			INNER JOIN [Reference].[Suburb] r ON TRY_CONVERT(int,r.PostCode) = TRY_CONVERT(int,u.RawPostCode) 
												AND REPLACE(u.RawSuburb,' ','') = REPLACE(SUBSTRING(r.Suburb, CHARINDEX(' ',r.Suburb) + 1, LEN(r.Suburb) - CHARINDEX(' ',r.Suburb)) + SUBSTRING(r.Suburb, 1, CHARINDEX(' ',r.Suburb) - 1),' ','')
	WHERE UpdatedSuburb IS NULL AND r.Suburb LIKE '% %'

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Account_Address', 'Postcode+Suburb(Embedded Suburb)', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	u
	SET		UpdatedSuburb = Cleanse.[CamelCase](NewUpdatedSuburb),
			UpdatedPostCode = RIGHT('0' + NewUpdatedPostCode, 4),
			UpdatedStateCode = NewUpdatedStateCode,
			UpdatedCountryCode = 'AU'
	FROM	(	SELECT	u.UpdatedSuburb, r.Suburb NewUpdatedSuburb, u.UpdatedPostcode, r.Postcode NewUpdatedPostcode, u.UpdatedStateCode, r.StateCode NewUpdatedStateCode, u.UpdatedCountryCode,
						ROW_NUMBER() OVER (PARTITION BY u.AccountKey ORDER BY LEN(r.Suburb) DESC, r.Suburb) Rnk
				FROM	#Accounts u
						INNER JOIN [Reference].[Suburb] r ON TRY_CONVERT(int,r.PostCode) = TRY_CONVERT(int,u.RawPostCode) 
															AND REPLACE(u.RawSuburb,' ','') LIKE '%' + REPLACE(r.Suburb,' ','') + '%'
				WHERE UpdatedSuburb IS NULL
			) u
	WHERE	Rnk = 1

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Account_Address', 'Postcode+Suburb(Postcode Fuzzy Match)', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	u
	SET		UpdatedSuburb = Cleanse.[CamelCase](x.UpdatedSuburb),
			UpdatedPostCode = RIGHT('0' + x.UpdatedPostCode, 4),
			UpdatedStateCode = x.UpdatedStateCode,
			UpdatedCountryCode = 'AU'
	FROM	(	SELECT	u.AccountKey, r.Suburb UpdatedSuburb, r.Postcode UpdatedPostcode, r.StateCode UpdatedStateCode,
						Cleanse.[StringPercentageMatch](u.RawPostcode, r.Postcode) UpdatedMatch,
						ROW_NUMBER() OVER (	PARTITION BY u.AccountKey 
											ORDER BY	CASE 
															WHEN u.SuburbX LIKE '%' + r.PostCode + '%' THEN 0 
															WHEN r.StateCode = u.RawStateCode THEN 1 
															WHEN LEN(u.RawPostCode) = 4 AND TRY_CONVERT(int,r.PostCode)/1000 = TRY_CONVERT(int,u.RawPostCode)/1000 THEN 2 
															ELSE 3
														END, 
														Cleanse.[StringPercentageMatch](u.RawPostcode, r.Postcode) DESC, 
														r.PostCode
											) Rnk
				FROM	(	SELECT AccountKey, REPLACE(RawAddress2,' ','') SuburbX, RawStateCode, RawPostCode, RawCountryCode, UpdatedSuburb, UpdatedStateCode, UpdatedPostCode, UpdatedCountryCode FROM #Accounts WHERE ISNULL(RawAddress2,'') NOT IN ('','Unknown') 
							UNION ALL
							SELECT AccountKey, REPLACE(RawSuburb,' ','') SuburbX, RawStateCode, RawPostCode, RawCountryCode, UpdatedSuburb, UpdatedStateCode, UpdatedPostCode, UpdatedCountryCode FROM #Accounts
						) u
						INNER JOIN	(	SELECT	*, REPLACE(Suburb,' ','') SuburbX
										FROM	[Reference].[Suburb]
										WHERE	NOT (	PostCode BETWEEN 0200 AND 0299 -- Exclude all the LVRs and PO Boxes when trying to come up with a postcode that isn't a perfect match
														OR PostCode BETWEEN 0900 AND 1999
														OR PostCode BETWEEN 5800 AND 5999
														OR PostCode BETWEEN 6800 AND 6999
														OR PostCode BETWEEN 7800 AND 7999
														OR PostCode BETWEEN 8000 AND 9999
													) 
									) r ON  r.SuburbX = u.SuburbX
						LEFT JOIN [Reference].[Suburb] r1 ON TRY_CONVERT(int,r1.PostCode) = TRY_CONVERT(int,u.RawPostCode)
				WHERE	UpdatedSuburb IS NULL
						AND u.RawCountryCode = 'AU'
						AND (r1.PostCode IS NULL OR u.SuburbX NOT IN ('Darwin','Melbourne','Sydney','Hobart','Brisbane','Adelaide','Newcastle','Perth','Townsville','Geelong')) -- Rule out large generic conurbations which may be less accurate than the current postcode
			) x
			INNER JOIN #Accounts u ON u.AccountKey = x.AccountKey
	WHERE	Rnk = 1
 
	EXEC [Control].Sp_Log @BatchKey, 'Sp_Account_Address', 'Postcode+Suburb(Suburb Fuzzy Match)', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	u
	SET		UpdatedSuburb = Cleanse.[CamelCase](x.UpdatedSuburb),
			UpdatedPostCode = RIGHT('0' + x.UpdatedPostCode, 4),
			UpdatedStateCode = x.UpdatedStateCode,
			UpdatedCountryCode = 'AU'
	FROM	(	SELECT	accountkey, r.Suburb UpdatedSuburb, r.PostCode UpdatedPostCode, r.StateCode UpdatedStateCode,
						Cleanse.[StringPercentageMatch](s.Suburbx, r.Suburbx) UpdatedMatch,
						ROW_NUMBER() OVER (PARTITION BY s.AccountKey ORDER BY (Cleanse.[StringPercentageMatch](s.Suburbx  ,r.Suburbx)) DESC, LEN(r.Suburb) DESC) Rnk
				FROM	(	SELECT AccountKey, RawSuburb, RawSuburb Suburbx, RawPostCode, RawCountryCode, UpdatedSuburb FROM #Accounts
							UNION ALL
							SELECT AccountKey, RawSuburb, SUBSTRING(RawSuburb, CHARINDEX(' ',RawSuburb) + 1, LEN(RawSuburb) - CHARINDEX(' ',RawSuburb)) + ' ' + SUBSTRING(RawSuburb, 1, CHARINDEX(' ',RawSuburb) - 1) Suburbx, RawPostCode, RawCountryCode, UpdatedSuburb FROM #Accounts  WHERE RawSuburb LIKE '% %'
							UNION ALL
							SELECT AccountKey, RawSuburb, REVERSE(RawSuburb) Suburbx, RawPostCode, RawCountryCode, UpdatedSuburb FROM #Accounts
						) s
						LEFT JOIN (	SELECT Suburb, Suburb Suburbx, Postcode, StateCode FROM [Reference].[Suburb]
									UNION ALL
									SELECT Suburb, SUBSTRING(Suburb, CHARINDEX(' ',Suburb) + 1, LEN(Suburb) - CHARINDEX(' ',Suburb)) + ' ' + SUBSTRING(Suburb, 1, CHARINDEX(' ',Suburb) - 1) Suburbx, Postcode, StateCode FROM [Reference].[Suburb] where Suburb LIKE '% %'
									UNION ALL
									SELECT Suburb, REVERSE(Suburb) Suburbx, Postcode, StateCode FROM [Reference].[Suburb]
									) r on TRY_CONVERT(int,r.PostCode) = TRY_CONVERT(int,s.RawPostCode)
				WHERE	s.UpdatedSuburb IS NULL
						AND s.RawCountryCode = 'AU'
						AND s.RawSuburb NOT IN ('Darwin','Melbourne','Sydney','Hobart','Brisbane','Adelaide','Newcastle','Perth','Townsville','Geelong') -- Rule out large generic conurbations which may be less accurate than the current postcode
						AND Cleanse.[StringPercentageMatch](s.Suburbx, r.Suburbx)>=50
			) x
			INNER JOIN #Accounts u ON (u.AccountKey = x.AccountKey)
	WHERE	Rnk = 1

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Account_Address', 'PostCode Overwrite Suburb', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	u
	SET		UpdatedSuburb = Cleanse.[CamelCase](x.UpdatedSuburb),
			UpdatedPostCode = x.UpdatedPostCode,
			UpdatedStateCode = x.UpdatedStateCode,
			UpdatedCountryCode = 'AU'
	FROM	(	SELECT 	u.AccountKey, 
						CASE WHEN MAX(r.suburb) = MIN(r.suburb) THEN Cleanse.[CamelCase](MAX(r.Suburb)) ELSE 'Unknown' END UpdatedSuburb, 
						CASE 
							WHEN MAX(r.suburb) = MIN(r.suburb) THEN MAX(r.StateCode)
							WHEN TRY_CONVERT(int,r.PostCode)/1000 = 0 AND u.RawStateCode IN ('NT') THEN u.RawStateCode
							WHEN TRY_CONVERT(int,r.PostCode)/1000 = 2 AND u.RawStateCode IN ('NSW','ACT') THEN u.RawStateCode
							WHEN TRY_CONVERT(int,r.PostCode)/1000 = 3 AND u.RawStateCode IN ('VIC') THEN u.RawStateCode
							WHEN TRY_CONVERT(int,r.PostCode)/1000 = 4 AND u.RawStateCode IN ('QLD') THEN u.RawStateCode
							WHEN TRY_CONVERT(int,r.PostCode)/1000 = 5 AND u.RawStateCode IN ('SA') THEN u.RawStateCode
							WHEN TRY_CONVERT(int,r.PostCode)/1000 = 6 AND u.RawStateCode IN ('WA') THEN u.RawStateCode
							WHEN TRY_CONVERT(int,r.PostCode)/1000 = 7 AND u.RawStateCode IN ('TAS') THEN u.RawStateCode
							ELSE 'UNK' 
						END UpdatedStateCode,
						ISNULL(r.Postcode,'Unknown') UpdatedPostCode -- Will only ever be one per AccountKey, given the query below
				FROM	#Accounts u
							LEFT JOIN (	SELECT	*
										FROM	[Reference].[Suburb]
										WHERE	NOT (	PostCode BETWEEN 0200 AND 0299 -- Exclude all the LVRs and PO Boxes when trying to come up with a postcode that isn't a perfect match
														OR PostCode BETWEEN 0900 AND 1999
														OR PostCode BETWEEN 5800 AND 5999
														OR PostCode BETWEEN 6800 AND 6999
														OR PostCode BETWEEN 7800 AND 7999
														OR PostCode BETWEEN 8000 AND 9999
													) 
									) r ON TRY_CONVERT(int,r.PostCode) = TRY_CONVERT(int,u.RawPostCode) 
				WHERE	u.UpdatedSuburb IS NULL
						AND u.RawCountryCode = 'AU'
				GROUP BY u.AccountKey, u.RawStateCode, r.PostCode
			) x
			INNER JOIN #Accounts u ON (u.AccountKey = x.AccountKey)

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Account_Address', 'PostCode+Suburb(Overseas)', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	#Accounts
	SET		UpdatedSuburb = Cleanse.[CamelCase](RawSuburb),
			UpdatedStateCode = CASE WHEN RawCountryCode IS NULL THEN 'UNK' ELSE 'N/A' END,
			UpdatedPostCode = RawPostCode,
			UpdatedCountryCode = ISNULL(RawCountryCode,'UNK')
	WHERE	UpdatedSuburb IS NULL
			AND ISNULL(RawCountryCode,'') <> 'AU'

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Account_Address', 'Address', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	u
	-- The address lines are not viewed as critical at the moment for any interface of analyses, so only the minimum of effort has been applied to claening these columns. 
	-- Much could be done to remove repetition of data across the Address columns and to redistribute the data across the columns such as when the building number ends up in Address 1 and the street name in address 2 (e.g. the commented case line below - not worth progressing in isolation)
	SET		UpdatedAddress1 = Cleanse.[CamelCase](CASE NewUpdatedAddress1 WHEN '' THEN NewUpdatedAddress2 ELSE NewUpdatedAddress1 END),
			UpdatedAddress2 = Cleanse.[CamelCase](CASE NewUpdatedAddress1 WHEN '' THEN '' ELSE NewUpdatedAddress2 END)
	FROM	(	SELECT	UpdatedAddress1,
						UpdatedAddress2,
						CASE
							WHEN ISNULL(REPLACE(RawAddress1,' ',''),'') IN ('','N/A','<missed><missed>---','<missed><missed><missed>','.','..','...','*','**','***','-','MobiPartialSignup','Unknown','AsAbove') THEN '' -- Sundry non-values
							WHEN RawAddress1 NOT LIKE '%[a-z,-,/]%' AND RawAddress2 NOT LIKE '%[a-z,-,/]%' THEN '' -- No alpha characters in either Address1 or 2
							WHEN RawAddress1 LIKE '%www.%' THEN '' -- Anything that is left looking like an email address musn't have anything useful following, so ditch the lot
							WHEN RawAddress1 LIKE '%don''t%' THEN '' -- Anything that is left looking like an email address musn't have anything useful following, so ditch the lot
							WHEN RawAddress1 LIKE '%@%.%' AND CHARINDEX(' ',RawAddress1) > 0 THEN SUBSTRING(RawAddress1, CHARINDEX(' ',RawAddress1) + 1, LEN(RawAddress1) - CHARINDEX(' ',RawAddress1)) -- Discard anything like an email address and keep anything that follows
							WHEN RawAddress1 LIKE '%@%.%' THEN '' -- Anything that is left looking like an email address musn't have anything useful following, so ditch the lot
							ELSE RawAddress1
						END NewUpdatedAddress1,
						CASE
							WHEN ISNULL(REPLACE(RawAddress2,' ',''),'') IN ('','Unknown','AsAbove','AddressLine2(optional)','.','\','?','-','`','''','MobiPartialSignup','Unknown','AsAbove') THEN '' -- Sundry non-values
							WHEN RawAddress1 NOT LIKE '%[a-z,-,/]%' and RawAddress2 NOT LIKE '%[a-z,-,/]%' THEN '' -- No alpha characters in either Address1 or 2
							WHEN REPLACE(RawAddress2,' ','') = REPLACE(UpdatedSuburb,' ','') THEN '' -- Replicates what is already in the UpdatedSuburb
							WHEN REPLACE(RawAddress1,' ','') LIKE '%' + REPLACE(RawAddress2,' ','') + '%' THEN '' -- Is a subset of or replicates Address 1
							WHEN REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(RawAddress2,' ',''),REPLACE(RawSuburb,' ',''),''),REPLACE(UpdatedSuburb,' ',''),''),RawPostcode,''),UpdatedPostCode,''),RawStatecode,''),UpdatedStateCode,''),RawState,'') = '' THEN '' -- Completely replicated across other address columns
--							WHEN TRY_CONVERT(int,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(RawAddress1,' ',''),'/',''),',',''),'-',''),'Lot',''),'Unit','')) IS NOT NULL THEN '' -- Address1 is just a building or unit number, so apped Address2 to Address 1
							WHEN RawAddress2 LIKE '%www.%' THEN '' -- Anything that is left looking like an email address musn't have anything useful following, so ditch the lot
							WHEN RawAddress2 LIKE '%don''t%' THEN '' -- Anything that is left looking like an email address musn't have anything useful following, so ditch the lot
							WHEN RawAddress2 LIKE '%@%.%' AND CHARINDEX(' ',RawAddress2) > 0 THEN SUBSTRING(RawAddress2, CHARINDEX(' ',RawAddress2) + 1, LEN(RawAddress2) - CHARINDEX(' ',RawAddress2)) -- Discard anything like an email address and keep anything that follows
							WHEN RawAddress2 LIKE '%@%.%' THEN '' -- Anything that is left looking like an email address musn't have anything useful following, so ditch the lot
							ELSE RawAddress2
						END NewUpdatedAddress2
				FROM	#Accounts
			) u

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Account_Address', 'State', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	#Accounts
	SET		UpdatedState =	CASE UpdatedStateCode
								WHEN 'NT' THEN 'Northern Territories'
								WHEN 'NSW' THEN 'New South Wales'
								WHEN 'ACT' THEN 'Australian Capital Territory'
								WHEN 'VIC' THEN 'Victoria'
								WHEN 'QLD' THEN 'Queensland'
								WHEN 'SA' THEN 'South Australia'
								WHEN 'WA' THEN 'Western Australia'
								WHEN 'TAS' THEN 'Tasmania'
								WHEN 'N/A' THEN 'N/A'
								ELSE 'Unknown'
							END
	WHERE	UpdatedState IS NULL

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Account_Address', 'Country', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	#Accounts
	SET		UpdatedCountry = CASE UpdatedCountryCode WHEN 'AU' THEN 'Australia' ELSE Cleanse.[CamelCase](RawCountry) END,
			UpdatedAustralianClient = CASE UpdatedCountryCode WHEN 'AU' THEN 'Yes' ELSE 'No' END
	WHERE	UpdatedCountry IS NULL

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Account_Address', 'Geography', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	u
	SET		UpdatedGeographicalLocation = CONVERT(geography, p.GeographicLocation) 
	FROM	#Accounts u
			INNER JOIN [Reference].[PostCode] p on p.PostCode = u.UpdatedPostCode and p.StateCode=u.UpdatedStateCode
	WHERE	u.UpdatedCountryCode = 'AU'

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Account_Address', 'Update', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	u
	SET		Address1 = a.UpdatedAddress1,
			Address2 = a.UpdatedAddress2,
			Suburb = a.UpdatedSuburb,
			StateCode = a.UpdatedStateCode,
			State = a.UpdatedState,
			PostCode = a.UpdatedPostCode,
			CountryCode = a.UpdatedCountryCode,
			Country = a.UpdatedCountry,
			AustralianClient = a.UpdatedAustralianClient,
			GeographicalLocation = a.UpdatedGeographicalLocation
	FROM	[$(Dimensional)].Dimension.Account u
			INNER JOIN #Accounts a ON a.AccountKey = u.AccountKey
	WHERE	ISNULL(u.Address1,'!') <> ISNULL(a.UpdatedAddress1,'!')
			OR ISNULL(u.Address2,'!') <> ISNULL(a.UpdatedAddress2,'!')
			OR ISNULL(u.Suburb,'!') <> ISNULL(a.UpdatedSuburb,'!')
			OR ISNULL(u.StateCode,'!') <> ISNULL(a.UpdatedStateCode,'!')
			OR ISNULL(u.State,'!') <> ISNULL(a.UpdatedState,'!')
			OR ISNULL(u.PostCode,'!') <> ISNULL(a.UpdatedPostCode,'!')
			OR ISNULL(u.CountryCode,'!') <> ISNULL(a.UpdatedCountryCode,'!')
			OR ISNULL(u.Country,'!') <> ISNULL(a.UpdatedCountry,'!')
			OR ISNULL(u.AustralianClient,'!') <> ISNULL(a.UpdatedAustralianClient,'!')


	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowCount

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Account_Address', NULL, 'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Account_Address', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

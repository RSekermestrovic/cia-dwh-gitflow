﻿-- =============================================
-- Author:		Lekha Narra
-- Create date: 20/11/2014
-- Description:	Insert events,rounds,Competitions for American Football event type
-- =============================================
CREATE PROCEDURE [Cleanse].[SP_Event_AmericanFootball] 
	-- Add the parameters for the stored procedure here
	@BatchKey int 
AS
BEGIN
BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  BEGIN TRANSACTION Event_AmericanFootball ;
	
--======================================================================================================================================================================   
--                                                PART 1: Insert Competition Names in to Event Dimension for 'AmericanFootball' event type
--====================================================================================================================================================================== 
EXEC [Control].Sp_Log	@BatchKey,'SP_Event_AmericanFootball','Part 1','Start'
Update [$(Dimensional)].Dimension.Event
set Competition=(Case 
						When (SourceEvent like '%Team[1-9] %' or ParentEvent like '%Team[1-9] %'or GrandparentEvent like '%Team[1-9] %'
							or SourceEvent like '%Template%' or ParentEvent like '%Template%'or GrandparentEvent like '%Template%'
							or SourceEvent like '%test[a-z] ' or ParentEvent like 'test[a-z] 'or GrandparentEvent like 'test[a-z] ')
						Then 'NA'
						When (GrandparentEvent is not null and GrandparentEvent<>'Live Betting'
						and GrandparentEvent not like '% @ %' and GrandparentEvent not like '% v %' and GrandparentEvent not like '% vs %')
						Then (Case 
								When (GrandparentEvent like '%/%/%'  or GrandparentEvent like '%Round [1-9]%' or GrandparentEvent like '% Final%'
								or GrandparentEvent like '% [1-9][a-z][a-z] %' or GrandparentEvent like '% second%' or GrandparentEvent like '%week%'
								 or GrandparentEvent like '% - %' or GrandparentEvent like '%|%')
								Then (Case 
										When (GrandparentEvent like '%week%')
										Then Substring(GrandparentEvent,1,Patindex('%week%',GrandparentEvent)-1)
										When (GrandparentEvent like '% [1-9][a-z][a-z] %')
										Then Substring(GrandparentEvent,1,Patindex('% [1-9][a-z][a-z] %',GrandparentEvent)-1)
										When (GrandparentEvent like '%Round [1-9]%')
										Then Substring(GrandparentEvent,1,Patindex('%Round [1-9]%',GrandparentEvent)-2)
										When (GrandparentEvent like '% Semi%Final%')
										Then Substring(GrandparentEvent,1,Patindex('% Semi%Final%',GrandparentEvent)-1)
										When (GrandparentEvent like '% Quarter%Final%')
										Then Substring(GrandparentEvent,1,Patindex('% Quarter%Final%',GrandparentEvent)-1)
										When (GrandparentEvent like '% Final%')
										Then Substring(GrandparentEvent,1,Patindex('% Final%',GrandparentEvent)-1)
										When (GrandparentEvent like '% second%')
										Then Substring(GrandparentEvent,1,Patindex('% second%',GrandparentEvent)-1)
										When (GrandparentEvent like '%|%')
										Then Substring(GrandparentEvent,1,Patindex('%|%',GrandparentEvent)-1)
										When (GrandparentEvent like '% - %')
										Then (Case 
												when GrandparentEvent like '%Live Betting - %'
												Then Substring(GrandparentEvent,Patindex('% - %',GrandparentEvent)+3,Len(GrandparentEvent))
												Else Substring(GrandparentEvent,1,Patindex('% - %',GrandparentEvent)-1)
											 End)
										When (GrandparentEvent like '%/%/%')
										Then Substring(GrandparentEvent,1,Patindex('%/%/%',GrandparentEvent)-3)
										Else 'Check'
									 End)
								Else GrandparentEvent
						     End)
						When (ParentEvent is not null and ParentEvent not like '% @ %' and ParentEvent not like '% v %'
						 and ParentEvent not like '% vs %'  and ParentEvent<>'Live Betting')
						Then (Case 
								When (ParentEvent like '%/%/%'  or ParentEvent like '%Round [1-9]%' or ParentEvent like '% Final%'
								or ParentEvent like '% [1-9][a-z][a-z] %' or ParentEvent like '% second%' or ParentEvent like '%week%'
								 or ParentEvent like '% - %' or ParentEvent like '%|%')
								Then (Case
										When (ParentEvent like '%week%')
										Then Substring(ParentEvent,1,Patindex('%week%',ParentEvent)-1) 
										When (ParentEvent like '% [1-9][a-z][a-z] %')
										Then Substring(ParentEvent,1,Patindex('% [1-9][a-z][a-z] %',ParentEvent)-1)
										When (ParentEvent like '%Round [1-9]%')
										Then Substring(ParentEvent,1,Patindex('%Round [1-9]%',ParentEvent)-2)
										When (ParentEvent like '% Semi%Final%')
										Then Substring(ParentEvent,1,Patindex('% Semi%Final%',ParentEvent)-1)
										When (ParentEvent like '% Quarter%Final%')
										Then Substring(ParentEvent,1,Patindex('% Quarter%Final%',ParentEvent)-1)
										When (ParentEvent like '% Final%')
										Then Substring(ParentEvent,1,Patindex('% Final%',ParentEvent)-1)
										When (ParentEvent like '% second%')
										Then Substring(ParentEvent,1,Patindex('% second%',ParentEvent)-1)
										When (ParentEvent like '%|%')
										Then Substring(ParentEvent,1,Patindex('%|%',ParentEvent)-1)
										When (ParentEvent like '% - %')
										Then (Case 
												when ParentEvent like '%Live Betting - %'
												Then Substring(ParentEvent,Patindex('% - %',ParentEvent)+3,Len(ParentEvent))
												Else Substring(ParentEvent,1,Patindex('% - %',ParentEvent)-1)
											 End)
										When (ParentEvent like '%/%/%')
										Then Substring(ParentEvent,1,Patindex('%/%/%',ParentEvent)-3)
										Else 'Check'
									 End)
								Else ParentEvent
						     End)
						When (SourceEvent is not null and SourceEvent not like '% @ %' and SourceEvent not like '% v %'
						 and SourceEvent not like '% vs %'  and SourceEvent<>'Live Betting' and SourceEvent not like '%Total%'
						  and SourceEvent not like '%First%'  and SourceEvent not like '%to %')
						Then (Case 
								When (SourceEvent like '%/%/%'  or SourceEvent like '%Round [1-9]%' or SourceEvent like '% Final%'
								or SourceEvent like '% [1-9][a-z][a-z] %' or SourceEvent like '% second%' or SourceEvent like '%week%'
								 or SourceEvent like '% - %' or SourceEvent like '%|%')
								Then (Case 
										When (SourceEvent like '%week%')
										Then Substring(SourceEvent,1,Patindex('%week%',SourceEvent)-1)
										When (SourceEvent like '%Round [1-9]%')
										Then Substring(SourceEvent,1,Patindex('%Round [1-9]%',SourceEvent)-2)
										When (SourceEvent like '% Semi%Final%')
										Then Substring(SourceEvent,1,Patindex('% Semi%Final%',SourceEvent)-1)
										When (SourceEvent like '% Quarter%Final%')
										Then Substring(SourceEvent,1,Patindex('% Quarter%Final%',SourceEvent)-1)
										When (SourceEvent like '% Final%')
										Then Substring(SourceEvent,1,Patindex('% Final%',SourceEvent)-1)
										When (SourceEvent like '% [1-9][a-z][a-z] %')
										Then Substring(SourceEvent,1,Patindex('% [1-9][a-z][a-z] %',SourceEvent)-1)
										When (SourceEvent like '% second%')
										Then Substring(SourceEvent,1,Patindex('% second%',SourceEvent)-1)
										When (SourceEvent like '%|%')
										Then Substring(SourceEvent,1,Patindex('%|%',SourceEvent)-1)
										When (SourceEvent like '% - %' and (SourceEvent not like '%Total%' and SourceEvent not like '%First%'))
										Then (Case 
												when SourceEvent like '%Live Betting - %'
												Then Substring(SourceEvent,Patindex('% - %',SourceEvent)+3,Len(SourceEvent))
												Else Substring(SourceEvent,1,Patindex('% - %',SourceEvent)-1)
											 End)
										When (SourceEvent like '%/%/%')
										Then Substring(SourceEvent,1,Patindex('%/%/%',SourceEvent)-3)
										Else 'Check'
									 End)
								Else SourceEvent
						     End)
						Else 'Unknown'
					End)
where SourceEventType='American Football' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1)))

Update [$(Dimensional)].Dimension.Event
set Competition=(Case 
						when Competition like '%?%'
						Then 'NA'
						when Competition like '%Regular season%'
						Then Substring(Competition,1,PATINDEX('%Regular %',Competition)-1)
						when Competition like '%wagering/%'
						Then Substring(Competition,1,PATINDEX('%Will /%',Competition)-1)
						Else Competition
					 End)
where SourceEventType='American Football' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1)))

Update [$(Dimensional)].Dimension.Event
set Competition=Ltrim(Rtrim(Replace(Replace(Replace(Replace(Replace(Replace(Competition,'test dump',''),'team total',''),'Winner',''),'wins',''),'Wagering',''),'-','')))
where SourceEventType='American Football' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey)
 and (Competition like '%-' or Competition like '%Winner%' 
or Competition like '%wins%' or Competition like '%Wagering%'  or Competition like '%team total%'  or Competition like '%test dump%')
 and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1)))

Update [$(Dimensional)].Dimension.Event
set Competition='Unknown'
where SourceEventType='American Football' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and Competition='' and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1)))

--======================================================================================================================================================================   
--                                                PART 2: Insert Round Names in to Event Dimension for 'AmericanFootball' event type
--====================================================================================================================================================================== 
EXEC [Control].Sp_Log	@BatchKey,'SP_Event_AmericanFootball','Part 2','Start',@RowsProcessed = @@RowCount
Update [$(Dimensional)].Dimension.Event
set [Round]=(Case 
				When (SourceEvent like '%Team[1-9] %' or ParentEvent like '%Team[1-9] %'or GrandparentEvent like '%Team[1-9] %'
							or SourceEvent like '%Template%' or ParentEvent like '%Template%'or GrandparentEvent like '%Template%'
							or SourceEvent like '%test[a-z] ' or ParentEvent like 'test[a-z] 'or GrandparentEvent like 'test[a-z] ')
				Then 'NA'
				When (isnull(GrandparentEvent,'') like '%Final%'  or isnull(GrandparentEvent,'') like '%Round [1-9]%' 
				or isnull(GrandparentEvent,'') like '%Week #[1-9]%')
				Then (Case 
						When (isnull(GrandparentEvent,'') like '%Semi_Final%')
						Then 'Semi Final'
						When (isnull(GrandparentEvent,'') like '%Quarter_Final%' )
						Then 'Quarter Final'
						When (isnull(GrandparentEvent,'') like '%Final%')
						Then 'Final'
						When (isnull(GrandparentEvent,'') like '%Round [1-9]%')
						Then Substring(isnull(GrandparentEvent,''),Patindex('%Round [1-9]%',isnull(GrandparentEvent,'')),8)
						When (isnull(GrandparentEvent,'') like '%Week #[1-9]%')
						Then 'Week '+Substring(isnull(GrandparentEvent,''),Patindex('%Week #[1-9]%',isnull(GrandparentEvent,''))+6,2)
						Else 'Check'
						End)
				When (isnull(ParentEvent,'') like '%Final%'  or isnull(ParentEvent,'') like '%Round [1-9]%' 
				or isnull(ParentEvent,'') like '%Week #[1-9]%')
				Then (Case 
						When (isnull(ParentEvent,'') like '%Semi_Final%')
						Then 'Semi Final'
						When (isnull(ParentEvent,'') like '%Quarter_Final%' )
						Then 'Quarter Final'
						When (isnull(ParentEvent,'') like '%Final%')
						Then 'Final'
						When (isnull(ParentEvent,'') like '%Round [1-9]%')
						Then Substring(isnull(ParentEvent,''),Patindex('%Round [1-9]%',isnull(ParentEvent,'')),8)
						When (isnull(ParentEvent,'') like '%Week #[1-9]%')
						Then 'Week '+Substring(isnull(ParentEvent,''),Patindex('%Week #[1-9]%',isnull(ParentEvent,''))+6,2)
						Else 'Check'
						End)
				When (isnull(SourceEvent,'') like '%Final%'  or isnull(SourceEvent,'') like '%Round [1-9]%' 
				or isnull(SourceEvent,'') like '%Week #[1-9]%')
				Then (Case 
						When (isnull(SourceEvent,'') like '%Semi_Final%')
						Then 'Semi Final'
						When (isnull(SourceEvent,'') like '%Quarter_Final%' )
						Then 'Quarter Final'
						When (isnull(SourceEvent,'') like '%Final%')
						Then 'Final'
						When (isnull(SourceEvent,'') like '%Round [1-9]%')
						Then Substring(isnull(SourceEvent,''),Patindex('%Round [1-9]%',isnull(SourceEvent,'')),8)
						When (isnull(SourceEvent,'') like '%Week #[1-9]%')
						Then 'Week '+Substring(isnull(SourceEvent,''),Patindex('%Week #[1-9]%',isnull(SourceEvent,''))+6,2)
						Else 'Check'
						End)
				Else 'Unknown'
				End)
where SourceEventType='American Football' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(10-1))<>Power(2,(10-1)))

--======================================================================================================================================================================   
--                                                PART 3: Insert Event Names in to Event Dimension for 'American Football' event type
--====================================================================================================================================================================== 
EXEC [Control].Sp_Log	@BatchKey,'SP_Event_AmericanFootball','Part 3','Start',@RowsProcessed = @@RowCount
Update [$(Dimensional)].Dimension.Event
set [Event]=(Case	
				When (SourceEvent like '%Team[1-9] %' or ParentEvent like '%Team[1-9] %'or GrandparentEvent like '%Team[1-9] %'
							or SourceEvent like '%Template%' or ParentEvent like '%Template%'or GrandparentEvent like '%Template%'
							or SourceEvent like '%test[a-z] ' or ParentEvent like 'test[a-z] 'or GrandparentEvent like 'test[a-z] ')
				Then 'NA'	
				WHEN (isnull(ParentEvent,'') like '% vs %' or isnull(ParentEvent,'') like '% v %' or isnull(ParentEvent,'') like '%@%' )
				THEN 
					(CASE 
						WHEN Replace((isnull(ParentEvent,'')),'UL - ','UL-') like'% - %'			
						THEN 
							(CASE
								WHEN ((SUBString(Replace((isnull(ParentEvent,'')),'UL - ','UL-'),1, CHARINDEX(' - ',Replace((isnull(ParentEvent,'')),'UL - ','UL-'))) like '% vs %') or
									(SUBString(Replace((isnull(ParentEvent,'')),'UL - ','UL-'),1, CHARINDEX(' - ',Replace((isnull(ParentEvent,'')),'UL - ','UL-'))) like '% v %') or
									(SUBString(Replace((isnull(ParentEvent,'')),'UL - ','UL-'),1, CHARINDEX(' - ',Replace((isnull(ParentEvent,'')),'UL - ','UL-'))) like '%@%') )
								THEN SUBString(Replace((isnull(ParentEvent,'')),'UL - ','UL-'),1, CHARINDEX(' - ',Replace((isnull(ParentEvent,'')),'UL - ','UL-'))-1)
								ELSE SUBString(Replace((isnull(ParentEvent,'')),'UL - ','UL-'), CHARINDEX(' - ',Replace((isnull(ParentEvent,'')),'UL - ','UL-'))+3,LEN(Replace((isnull(ParentEvent,'')),'UL - ','UL-')))
							END)
						ELSE isnull(ParentEvent,'')
					END)
					ELSE
					(CASE
						WHEN (SourceEvent like '% vs %' or SourceEvent like '% v %' or SourceEvent like '%@%' )
						THEN 
							(CASE 
								WHEN Replace(SourceEvent,'UL - ','UL-') like'% - %'			
								THEN 
									(CASE
										WHEN ((SUBString(Replace(SourceEvent,'UL - ','UL-'),1, CHARINDEX(' - ',Replace(SourceEvent,'UL - ','UL-'))) like '% vs %') or
											(SUBString(Replace(SourceEvent,'UL - ','UL-'),1, CHARINDEX(' - ',Replace(SourceEvent,'UL - ','UL-'))) like '% v %') or
											(SUBString(Replace(SourceEvent,'UL - ','UL-'),1, CHARINDEX(' - ',Replace(SourceEvent,'UL - ','UL-'))) like '%@%') )
										THEN SUBString(Replace(SourceEvent,'UL - ','UL-'),1, CHARINDEX(' - ',Replace(SourceEvent,'UL - ','UL-'))-1)
										ELSE SUBString(Replace(SourceEvent,'UL - ','UL-'), CHARINDEX(' - ',Replace(SourceEvent,'UL - ','UL-'))+3,LEN(Replace(SourceEvent,'UL - ','UL-')))
									END)
								ELSE SourceEvent
								END)
							ELSE 'NA'
						End)
					ENd)
where SourceEventType='American Football' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(2-1))<>Power(2,(2-1)))
EXEC [Control].Sp_Log	@BatchKey,'SP_Event_AmericanFootball',NULL,'Success'
COMMIT TRANSACTION Event_AmericanFootball;

--======================================================================================================================================================================   
--                                                PART 4: Insert Modification details in to Event Dimension for 'American Football' event type
--====================================================================================================================================================================== 
EXEC [Control].Sp_Log	@BatchKey,'SP_Event_AmericanFootball','Part 4','Start',@RowsProcessed = @@RowCount
Update 	[$(Dimensional)].Dimension.Event
set	ModifiedBy='SP_Event_AmericanFootball',
	ModifiedDate=CURRENT_TIMESTAMP
where ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and SourceEventType='American Football'

EXEC [Control].Sp_Log	@BatchKey,'SP_Event_AmericanFootball',NULL,'Success',@RowsProcessed = @@RowCount

END TRY
BEGIN CATCH
	Rollback TRANSACTION Event_AmericanFootball;
	declare @Error int;
	declare @ErrorMessage varchar(max);
	                           -- Log the failure at the object level
                           SET    @Error = ERROR_NUMBER()
                           SET    @ErrorMessage = ERROR_MESSAGE()
						   EXEC [Control].Sp_Log @BatchKey, 'SP_Event_AmericanFootball', NULL, 'Failed', @ErrorMessage;
						   Raiserror(@ErrorMessage,16,1)

END CATCH

END

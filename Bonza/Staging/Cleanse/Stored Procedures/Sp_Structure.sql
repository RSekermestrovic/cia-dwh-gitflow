﻿CREATE proc [Cleanse].[SP_Structure]
(
	@BatchKey int = 0,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @RowCount int

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

BEGIN TRY

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Structure', 'VIP', 'Start';

	UPDATE	[$(Dimensional)].Dimension.Structure
	SET		FY2016VIPCode = CASE
								WHEN ISNULL(FY2016CanManageVIP,0)=0	THEN 0
								WHEN FY2016ManagerId in (-5,1427999,1506167) or FY2016Manager like '%Kevin Murphy%'  or FY2016Manager='Kelly, Ben' THEN 1
								WHEN FY2016ManagerId in (663028) or FY2016Manager like '%North, Adam (CBT)%' THEN 2
								ELSE 0
							END,
			FY2015VIPCode = CASE 
								WHEN FY2015IntroducerId = 1150832 THEN 1	-- Damien Cooper
								WHEN FY2015IntroducerId = 663028 THEN 1		-- Adam North
								WHEN FY2015IntroducerId = 791020 THEN 1		-- Jess Caine
								ELSE 0 
							END,
			FY2014VIPCode = CASE 
								WHEN FY2014IntroducerId = 1150832 THEN 1	-- Damien Cooper
								WHEN FY2014IntroducerId = 663028 THEN 1		-- Adam North
								WHEN FY2014IntroducerId = 791020 THEN 1		-- Jess Caine
								ELSE 0 
							END
	WHERE ModifiedBatchKey = CASE @BatchKey WHEN 0 THEN ModifiedBatchKey ELSE @BatchKey END
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log @BatchKey,'Sp_Structure','BDM','Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	[$(Dimensional)].Dimension.Structure
	SET		FY2016BDM =	CASE 
							WHEN ISNULL(FY2016ManagerId,0) IN (0,1494692,-1) OR FY2016VIPCode > 0 THEN 'N/A' -- If VIP or Introducer in Darwin Office, Internet client or Ben Kelly Digital, then not BDM
							WHEN FY2016Manager like '%Unassigned BDM%' THEN 'Legacy'
							Else FY2016Manager
						END,
			FY2015BDM =	CASE 
							WHEN FY2015IntroducerId NOT IN (-1, 0, 10241, 14616, 663028, 681860, 681861, 791017, 791020, 1150832, 1263564) THEN FY2015Introducer
							ELSE 'N/A' 
						END,
			FY2014BDM =	CASE 
							WHEN FY2014IntroducerId NOT IN (-1, 0, 10241, 14616, 663028, 681860, 681861, 791017, 791020, 1150832, 1263564) THEN FY2014Introducer
							ELSE 'N/A' 
						END
	WHERE ModifiedBatchKey = CASE @BatchKey WHEN 0 THEN ModifiedBatchKey ELSE @BatchKey END
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log @BatchKey,'Sp_Structure','Level0','Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	[$(Dimensional)].Dimension.Structure
	SET		FY2016Level0 =	CASE 
								WHEN FY2016OrgId IS NULL THEN 'N/A' 
								WHEN FY2016BDM NOT IN ('N/A', 'Unknown') THEN 'BDM'
								WHEN FY2016OrgId = 1 THEN 'WHA'
								WHEN FY2016OrgId = 2 THEN 'CBT'
								WHEN FY2016OrgId = 3 THEN 'RWWA'
								WHEN FY2016OrgId = 8 THEN 'TWH' 
								ELSE 'UNK' 
							END,
			FY2015Level0 =	CASE 
								WHEN FY2015OrgId IS NULL THEN 'N/A' 
								WHEN FY2015BDM NOT IN ('N/A', 'Unknown') THEN 'BDM'
								WHEN FY2015OrgId = 1 THEN 'WHA'
								WHEN FY2015OrgId = 2 THEN 'CBT'
								WHEN FY2015OrgId = 3 THEN 'RWWA'
								WHEN FY2015OrgId = 8 THEN 'TWH' 
								ELSE 'UNK' 
							END,
			FY2014Level0 =	CASE 
								WHEN FY2014OrgId IS NULL THEN 'N/A' 
								WHEN FY2014BDM NOT IN ('N/A', 'Unknown') THEN 'BDM'
								WHEN FY2014OrgId = 1 THEN 'WHA'
								WHEN FY2014OrgId = 2 THEN 'CBT'
								WHEN FY2014OrgId = 3 THEN 'RWWA'
								WHEN FY2014OrgId = 8 THEN 'TWH' 
								ELSE 'UNK' 
							END
	WHERE ModifiedBatchKey = CASE @BatchKey WHEN 0 THEN ModifiedBatchKey ELSE @BatchKey END
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log @BatchKey,'Sp_Structure','Level1','Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	[$(Dimensional)].Dimension.Structure
	SET		FY2016Level1 = CASE AccountID WHEN 1070305 THEN 'JW' WHEN 777860 THEN 'JW' ELSE FY2016Level0 END,
			FY2015Level1 = FY2015Level0,
			FY2014Level1 = FY2014Level0
	WHERE ModifiedBatchKey = CASE @BatchKey WHEN 0 THEN ModifiedBatchKey ELSE @BatchKey END
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log @BatchKey,'Sp_Structure','Level2','Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	[$(Dimensional)].Dimension.Structure
	SET		FY2016Level2 = CASE
								WHEN FY2016Level0 = 'N/A' THEN 'N/A'
								WHEN InPlay = 'Y' AND FY2016Level1 = 'WHA' THEN 'INP'
								WHEN FY2016VIPCode = 1 THEN 'VIP1'
								WHEN FY2016VIPCode = 2 THEN 'VIP2'
								WHEN FY2015OrgId = 8 THEN 'TWH'
								ELSE FY2016Level1
							END,
			FY2015Level2 =	CASE 
								WHEN FY2015Level0 = 'N/A' THEN 'N/A'
								WHEN FY2015VIPCode > 0 THEN 'VIP'
								ELSE FY2015Level1 
							END,
			FY2014Level2 =	CASE 
								WHEN FY2014Level0 = 'N/A' THEN 'N/A'
								WHEN FY2014VIPCode > 0 THEN 'VIP'
								ELSE FY2014Level1 
							END
	WHERE ModifiedBatchKey = CASE @BatchKey WHEN 0 THEN ModifiedBatchKey ELSE @BatchKey END
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log @BatchKey,'Sp_Structure','Cross Apply','Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	s
	SET		Level0 = FY2016Level0,
			Level1 = FY2016Level1,
			Level2 = FY2016Level2,
			BDM = FY2016BDM,
			VIPCode = FY2016VIPCode,
			VIP = CASE WHEN [FY2016VIPCode] = 0 THEN 'N/A' WHEN [FY2016VIPCode] > 0 THEN 'VIP ' + Cast ([FY2016VIPCode] as char(1)) ELSE 'Unknown' END,
			PreviousLevel0 = FY2015Level0,
			PreviousLevel1 = FY2015Level1,
			PreviousLevel2 = FY2015Level2,
			PreviousBDM = FY2015BDM,
			PreviousVIPCode = FY2015VIPCode,
			PreviousVIP = CASE WHEN [FY2015VIPCode] = 0 THEN 'N/A' WHEN [FY2015VIPCode] > 0 THEN 'VIP ' + Cast ([FY2015VIPCode] as char(1)) ELSE 'Unknown' END,
			CompanyKey = ISNULL(c2016.CompanyKey,-1),
			PreviousCompanyKey = ISNULL(c2015.CompanyKey,-1),
			FY2016CompanyKey = ISNULL(c2016.CompanyKey,-1),
			FY2015CompanyKey = ISNULL(c2015.CompanyKey,-1),
			FY2014CompanyKey = ISNULL(c2014.CompanyKey,-1),
			FY2016VIP = CASE WHEN [FY2016VIPCode] = 0 THEN 'N/A' WHEN [FY2016VIPCode] > 0 THEN 'VIP ' + Cast ([FY2016VIPCode] as char(1)) ELSE 'Unknown' END,
			FY2015VIP = CASE WHEN [FY2015VIPCode] = 0 THEN 'N/A' WHEN [FY2015VIPCode] > 0 THEN 'VIP ' + Cast ([FY2015VIPCode] as char(1)) ELSE 'Unknown' END,
			FY2014VIP = CASE WHEN [FY2014VIPCode] = 0 THEN 'N/A' WHEN [FY2014VIPCode] > 0 THEN 'VIP ' + Cast ([FY2014VIPCode] as char(1)) ELSE 'Unknown' END
	FROM	(	SELECT	*, 
						CASE FY2014Level0 WHEN 'N/A' THEN 0 WHEN 'BDM' THEN 2 WHEN 'RWWA' THEN 100 ELSE 1 END FY2014DivisionId, 
						CASE FY2015Level0 WHEN 'N/A' THEN 0 WHEN 'BDM' THEN 2 WHEN 'RWWA' THEN 100 ELSE 1 END FY2015DivisionId, 
						CASE FY2016Level0 WHEN 'N/A' THEN 0 WHEN 'BDM' THEN 2 WHEN 'RWWA' THEN 100 ELSE 1 END FY2016DivisionId 
				FROM	[$(Dimensional)].Dimension.Structure 
				WHERE ModifiedBatchKey = CASE @BatchKey WHEN 0 THEN ModifiedBatchKey ELSE @BatchKey END
			) s
			LEFT JOIN [$(Dimensional)].Dimension.Company c2014 ON (c2014.OrgID = ISNULL(s.FY2014OrgId,0) AND c2014.DivisionId = s.FY2014DivisionId)
			LEFT JOIN [$(Dimensional)].Dimension.Company c2015 ON (c2015.OrgID = ISNULL(s.FY2015OrgId,0) AND c2015.DivisionId = s.FY2015DivisionId)
			LEFT JOIN [$(Dimensional)].Dimension.Company c2016 ON (c2016.OrgID = ISNULL(s.FY2016OrgId,0) AND c2016.DivisionId = s.FY2016DivisionId)
	OPTION (RECOMPILE)

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,'Sp_Structure',NULL,'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Structure', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

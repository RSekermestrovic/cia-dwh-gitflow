﻿CREATE proc [Cleanse].[Sp_Account_Opened]
(
	@BatchKey int = 0,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @RowCount INT;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

BEGIN TRY


	EXEC [Control].Sp_Log @BatchKey, 'Sp_Account_Opened', 'Update', 'Start';

	SELECT	a.AccountKey,
			a.AccountOpenedDate, 
			a.AccountOpenedDayKey, 
			ISNULL(a1.LedgerOpeneddate, a.LedgerOpenedDate) UpdatedAccountOpenedDate, 
			ISNULL(a1.LedgerOpenedDayKey,a.LedgerOpenedDayKey) UpdatedAccountOpenedDayKey
	INTO	#Accounts
	FROM	[$(Dimensional)].Dimension.Account a
			LEFT LOOP JOIN [$(Dimensional)].Dimension.Account a1 ON a.LedgerSequenceNumber > 1 AND a1.AccountID = a.AccountID AND a1.LedgerSource = a.LedgerSource AND a1.LedgerSequenceNumber = 1
	WHERE	a.ModifiedBatchKey = CASE @BatchKey WHEN 0 THEN a.ModifiedBatchKey ELSE @BatchKey END

	UPDATE	a
	SET		AccountOpenedDate = u.UpdatedAccountOpenedDate,
			AccountOpenedDayKey = u.UpdatedAccountOpenedDayKey
	FROM	[$(Dimensional)].Dimension.Account a
			INNER JOIN #Accounts u ON u.AccountKey = a.AccountKey
	WHERE	ISNULL(a.AccountOpenedDate,CONVERT(datetime2(3),'1900-01-01')) <> ISNULL(u.UpdatedAccountOpenedDate,CONVERT(datetime2(3),'1900-01-01'))
			OR ISNULL(a.AccountOpenedDayKey,-99) <> ISNULL(u.UpdatedAccountOpenedDayKey,-99)

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Account_Opened', NULL, 'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Account_Opened', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
﻿-- =============================================
-- Author:		Lekha Narra
-- Create date: 20/11/2014
-- Description:	Insert eventNames,roundNames,CompetitionNames for Golf event type
-- =============================================
CREATE PROCEDURE [Cleanse].[SP_Event_Golf] 
	-- Add the parameters for the stored procedure here
	@BatchKey int 
AS
BEGIN
BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  BEGIN TRANSACTION Event_Golf ;
	
--======================================================================================================================================================================   
--                                                PART 1: Insert Competition Names in to Event Dimension for 'Golf' event type
--====================================================================================================================================================================== 
EXEC [Control].Sp_Log	@BatchKey,'SP_Event_Golf','Part 1','Start'
Update [$(Dimensional)].Dimension.Event
set Competition=(Case 
						When (SourceEvent like '%Team[1-9] %' or ParentEvent like '%Team[1-9] %'or GrandparentEvent like '%Team[1-9] %'
							or SourceEvent like '%Template%' or ParentEvent like '%Template%'or GrandparentEvent like '%Template%'
							or SourceEvent like '%Dump%' or ParentEvent like '%Dump%'or GrandparentEvent like '%Dump%'
							or SourceEvent like '%test[a-z] ' or ParentEvent like 'test[a-z] 'or GrandparentEvent like 'test[a-z] ')
						Then 'NA'
						When (GrandparentEvent is not null and GrandparentEvent<>'Live Betting'
						and GrandparentEvent not like '% @ %' and GrandparentEvent not like '% v %' and GrandparentEvent not like '% vs %')
						Then (Case 
								When (GrandparentEvent like '% - %'  )
								Then (Case
								        When(SUBString(GrandparentEvent,1, CHARINDEX(' - ',GrandparentEvent)) like '%Top %')
										Then SUBString(GrandparentEvent, CHARINDEX(' - ',GrandparentEvent),Len(GrandparentEvent))
										Else SUBString(GrandparentEvent,1, CHARINDEX(' - ',GrandparentEvent)-1)
									  End)
								Else GrandparentEvent
						     End)
						When (ParentEvent is not null and ParentEvent not like '% @ %' and ParentEvent not like '% v %'
						 and ParentEvent not like '% vs %'  and ParentEvent<>'Live Betting')
						Then (Case 
								When (ParentEvent like '% - %'  )
								Then (Case
								        When(SUBString(ParentEvent,1, CHARINDEX(' - ',ParentEvent)) like '%Top %')
										Then SUBString(ParentEvent, CHARINDEX(' - ',ParentEvent),Len(ParentEvent))
										Else SUBString(ParentEvent,1, CHARINDEX(' - ',ParentEvent)-1)
									  End)
								Else ParentEvent
						     End)
						When (SourceEvent is not null and SourceEvent not like '% @ %' and SourceEvent not like '% v %'
						 and SourceEvent not like '% vs %'  and SourceEvent<>'Live Betting' and SourceEvent not like '%Total%'
						  and SourceEvent not like '%First%'  and SourceEvent not like '%to %')
						Then (Case 
								When (SourceEvent like '% - %'  )
								Then (Case
								        When(SUBString(SourceEvent,1, CHARINDEX(' - ',SourceEvent)) like '%Top %')
										Then SUBString(SourceEvent, CHARINDEX(' - ',SourceEvent),Len(SourceEvent))
										Else SUBString(SourceEvent,1, CHARINDEX(' - ',SourceEvent)-1)
									  End)
								Else SourceEvent
						     End)
						Else 'Unknown'
					End)
where SourceEventType='Golf' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1)))

--Remove erroneous data from competition 
Update [$(Dimensional)].Dimension.Event
set Competition=(Case 
						When(Competition like '%Round%' or Competition like '%36%' or Competition like '%72%' or
						 Competition like '%Betting%' or Competition like '%Top%')
						 Then 'Unknown'
						When(Competition like '%|%')
						Then Substring(Competition,1,Patindex('%|%',Competition)-1)
						When(Competition like '%*%')
						Then Substring(Competition,1,Patindex('%*%',Competition)-1)
						Else Competition
					 End)
where SourceEventType='Golf' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1)))

--Remove trailing spaces
Update [$(Dimensional)].Dimension.Event
set Competition=Ltrim(Rtrim(Competition))
where SourceEventType='Golf' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1)))

--======================================================================================================================================================================   
--                                                PART 2: Insert Round Names in to Event Dimension for 'Golf' event type
--====================================================================================================================================================================== 
EXEC [Control].Sp_Log	@BatchKey,'SP_Event_Golf','Part 2','Start',@RowsProcessed = @@RowCount
Update [$(Dimensional)].Dimension.Event
set round=(Case 
				When (SourceEvent like '%Team[1-9] %' or ParentEvent like '%Team[1-9] %'or GrandparentEvent like '%Team[1-9] %'
							or SourceEvent like '%Template%' or ParentEvent like '%Template%'or GrandparentEvent like '%Template%'
							or SourceEvent like '%Dump%' or ParentEvent like '%Dump%'or GrandparentEvent like '%Dump%'
							or SourceEvent like '%test[a-z] ' or ParentEvent like 'test[a-z] 'or GrandparentEvent like 'test[a-z] ')
				Then 'NA'
				When (isnull(GrandparentEvent,'') like '%Final%'  or isnull(GrandparentEvent,'') like '%Round [1-9]%' 
				or isnull(GrandparentEvent,'') like '%Day [1-9]%')
				Then (Case 
						When (isnull(GrandparentEvent,'') like '%Semi_Final%')
						Then 'Semi Final'
						When (isnull(GrandparentEvent,'') like '%Quarter_Final%' )
						Then 'Quarter Final'
						When (isnull(GrandparentEvent,'') like '%Final%')
						Then 'Final'
						When (isnull(GrandparentEvent,'') like '%Round [1-9]%')
						Then Substring(isnull(GrandparentEvent,''),Patindex('%Round [1-9]%',isnull(GrandparentEvent,'')),8)
						When (isnull(GrandparentEvent,'') like '%Day [1-9]%')
						Then Substring(isnull(GrandparentEvent,''),Patindex('%Day [1-9]%',isnull(GrandparentEvent,'')),5)
						Else 'Check'
						End)
				When (isnull(ParentEvent,'') like '%Final%'  or isnull(ParentEvent,'') like '%Round [1-9]%' 
				or isnull(ParentEvent,'') like '%Day [1-9]%')
				Then (Case 
						When (isnull(ParentEvent,'') like '%Semi_Final%')
						Then 'Semi Final'
						When (isnull(ParentEvent,'') like '%Quarter_Final%' )
						Then 'Quarter Final'
						When (isnull(ParentEvent,'') like '%Final%')
						Then 'Final'
						When (isnull(ParentEvent,'') like '%Round [1-9]%')
						Then Substring(isnull(ParentEvent,''),Patindex('%Round [1-9]%',isnull(ParentEvent,'')),8)
						When (isnull(ParentEvent,'') like '%Day [1-9]%')
						Then Substring(isnull(ParentEvent,''),Patindex('%Day [1-9]%',isnull(ParentEvent,'')),5)
						Else 'Check'
						End)
				When (isnull(SourceEvent,'') like '%Final%'  or isnull(SourceEvent,'') like '%Round [1-9]%' 
				or isnull(SourceEvent,'') like '%Week #[1-9]%')
				Then (Case 
						When (isnull(SourceEvent,'') like '%Semi_Final%')
						Then 'Semi Final'
						When (isnull(SourceEvent,'') like '%Quarter_Final%' )
						Then 'Quarter Final'
						When (isnull(SourceEvent,'') like '%Final%')
						Then 'Final'
						When (isnull(SourceEvent,'') like '%Round [1-9]%')
						Then Substring(isnull(SourceEvent,''),Patindex('%Round [1-9]%',isnull(SourceEvent,'')),8)
						When (isnull(SourceEvent,'') like '%Day [1-9]%')
						Then Substring(isnull(SourceEvent,''),Patindex('%Day [1-9]%',isnull(SourceEvent,'')),5)
						Else 'Check'
						End)
				Else 'Unknown'
				End)
where SourceEventType='Golf' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(10-1))<>Power(2,(10-1)))

--======================================================================================================================================================================   
--                                                PART 3: Insert Event Names in to Event Dimension for 'Golf' event type
--====================================================================================================================================================================== 
EXEC [Control].Sp_Log	@BatchKey,'SP_Event_Golf','Part 3','Start',@RowsProcessed = @@RowCount
Update [$(Dimensional)].Dimension.Event
set Event=(Case		
					When (SourceEvent like '%Team[1-9] %' or isnull(ParentEvent,'') like '%Team[1-9] %'or isnull(GrandparentEvent,'') like '%Team[1-9] %'
							or SourceEvent like '%Template%' or isnull(ParentEvent,'') like '%Template%'or isnull(GrandparentEvent,'') like '%Template%'
							or SourceEvent like '%Dump%' or ParentEvent like '%Dump%'or GrandparentEvent like '%Dump%'
							or SourceEvent like 'test' or isnull(ParentEvent,'') like 'test'or isnull(GrandparentEvent,'') like 'test')
					Then 'NA'
					WHEN (isnull(ParentEvent,'') like '% vs %' or isnull(ParentEvent,'') like '% v %' or isnull(ParentEvent,'') like '%@%' )
					THEN 
						(CASE 
							WHEN isnull(ParentEvent,'') like'% - %'			
							THEN 
								(CASE
									WHEN ((SUBString(isnull(ParentEvent,''),1, CHARINDEX(' - ',isnull(ParentEvent,''))) like '% vs %') or
										(SUBString(isnull(ParentEvent,''),1, CHARINDEX(' - ',isnull(ParentEvent,''))) like '% v %') or
										(SUBString(isnull(ParentEvent,''),1, CHARINDEX(' - ',isnull(ParentEvent,''))) like '%@%') )
									THEN SUBString(isnull(ParentEvent,''),1, CHARINDEX(' - ',isnull(ParentEvent,''))-1)
									ELSE SUBString(isnull(ParentEvent,''), CHARINDEX(' - ',isnull(ParentEvent,''))+3,LEN(isnull(ParentEvent,'')))
								END)
							ELSE isnull(ParentEvent,'')
						END)
						ELSE
						(CASE
							WHEN (SourceEvent like '% vs %' or SourceEvent like '% v %' or SourceEvent like '%@%' )
							THEN 
								(CASE 
									WHEN SourceEvent like'% - %'			
									THEN 
										(CASE
											WHEN ((SUBString(SourceEvent,1, CHARINDEX(' - ',SourceEvent)) like '% vs %') or
												(SUBString(SourceEvent,1, CHARINDEX(' - ',SourceEvent)) like '% v %') or
												(SUBString(SourceEvent,1, CHARINDEX(' - ',SourceEvent)) like '%@%') )
											THEN SUBString(SourceEvent,1, CHARINDEX(' - ',SourceEvent)-1)
											ELSE SUBString(SourceEvent, CHARINDEX(' - ',SourceEvent)+3,LEN(SourceEvent))
										END)
									ELSE SourceEvent
									END)
								ELSE 'NA'
							End)
						ENd)
where SourceEventType='Golf' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(2-1))<>Power(2,(2-1)))

COMMIT TRANSACTION Event_Golf;

--======================================================================================================================================================================   
--                                                PART 4: Insert Modification details in to Event Dimension for 'Golf' event type
--====================================================================================================================================================================== 
EXEC [Control].Sp_Log	@BatchKey,'SP_Event_Golf','Part 4','Start',@RowsProcessed = @@RowCount
Update 	[$(Dimensional)].Dimension.Event
set	ModifiedBy='SP_Event_Golf',
	ModifiedDate=CURRENT_TIMESTAMP
where ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and SourceEventType='Golf'
EXEC [Control].Sp_Log	@BatchKey,'SP_Event_Golf',NULL,'Success',@RowsProcessed = @@RowCount

END TRY
BEGIN CATCH
	Rollback TRANSACTION Event_Golf;
	declare @Error int;
	declare @ErrorMessage varchar(max);
	                           -- Log the failure at the object level
                           SET    @Error = ERROR_NUMBER()
                           SET    @ErrorMessage = ERROR_MESSAGE()
						   EXEC [Control].Sp_Log @BatchKey, 'SP_Event_Golf', NULL, 'Failed', @ErrorMessage;
						   Raiserror(@ErrorMessage,16,1)

END CATCH

END

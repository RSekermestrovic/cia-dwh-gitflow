﻿CREATE proc [Cleanse].[Sp_Account_Customer]
(
	@BatchKey int = 0,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @RowCount INT;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

BEGIN TRY

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Account_Customer', 'Update', 'Start';

	-- Ultimately, the CustomerId will allow for the grouping of multiple accounts belonging to the same customer and the cleansed Name/Address/Phone/Email colums will be consolidated at the
	-- customer level rather than the current account level.
	-- However, there is no immediate priority to implement this functionality, so in the meantime, just set the CustomerId to the AccountId with no grouping

	UPDATE	[$(Dimensional)].Dimension.Account
	SET		CustomerId = AccountID,
			CustomerSource = 'DW'
	WHERE	CustomerID <> AccountId
			OR CustomerSource <> 'DW'

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Account_Customer', NULL, 'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Account_Customer', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

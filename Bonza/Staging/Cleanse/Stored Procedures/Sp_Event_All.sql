﻿-- =============================================
-- Author:		Lekha Narra
-- Create date: 02/09/2016
-- Description:	Load presentation fields which have a source counterpart for all events
-- =============================================
CREATE PROCEDURE [Cleanse].[Sp_Event_All]
	-- Add the parameters for the stored procedure here
	@BatchKey int
AS
BEGIN
BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

BEGIN TRANSACTION Event_All;

-- ================================================================================================================================================================
  --                        PART 1: Update common fields for all event types in Event Dimension for all event types
-- ================================================================================================================================================================
EXEC [Control].Sp_Log	@BatchKey,'SP_Event_All','Part 1 - Round sequence','Start'

Update m
Set RoundSequence = isnull(SourceRaceNumber,0),
	EventAbandoned = (CASE ISNULL(SourceEventAbandoned,0) WHEN 1 THEN 'Yes' WHEN 0 then 'No' ELSE 'Unknown' END),
	Distance = (CASE WHEN SourceDistance is not null then CONVERT(Varchar,SourceDistance) + ' m' ELSE Distance END),
	--EventID = m.SourceEventID, --To be cleaned up when Event level data can be seggregated from current Market level data
	Event = CASE When ISNULL(SourceEvent,'') = '' then 'Unknown' else SourceEvent End,
	ScheduledDateTime = SourceEventDate,
	ScheduledDayKey = IsNULL(SED.DayKey,-1),
	VenueID = isnull(m.SourceVenueID,0),
	Venue = CASE When ISNULL(SourceVenue,'') = '' then 'Unknown' else SourceVenue End,
	VenueType = (CASE SourceVenueType WHEN 'M' THEN 'Metro' WHEN 'c' THEN 'Country' WHEN 'p' THEN 'Provincial' ELSE 'Unknown' END),
	VenueStateCode = ISNULL(Cast(SourceVenueStateCode as varchar),'UNK'),
	VenueCountryCode = ISNULL(Cast(SourceVenueCountryCode as varchar),'UNK'),
	VenueState = ISNULL(SourceVenueState,'Unknown'),
	TrackCondition = isnull(SourceTrackCondition,'Unknown'),
	EventWeather = isnull(SourceEventWeather,'Unknown'),
	VenueCountry = ISNULL(b.Country,SourceVenueCountry),
	LiveVideoStreamed = (Case When (SourceLiveStreamPerformID like '%[0-9]%' and SourceLiveStreamPerformID not like '%[a-z]%') or SourceLiveStreamPerformID like '%id="[0-9]%'
							  Then 'Yes'
							  Else 'No'
						 End),
    LiveScoreBoard = (Case When SourceLiveStreamPerformID like '%[a-z]%' and SourceLiveStreamPerformID not like '%id="[0-9]%' Then 'Yes'
						   Else 'No'
					  End),
	PrizePool = m.SourcePrizePool,
	SubSportType = m.SourceSubSportType,
	RaceGroup = isnull(m.SourceRaceGroup,''),
	WeightType = isnull(m.SourceWeightType,''),
	HybridPricingTemplate = isnull(m.SourceHybridPricingTemplate,''),
	RaceClass = isnull(m.SourceRaceClass,''),
	ModifiedBy='SP_Event_All',
	ModifiedDate=CURRENT_TIMESTAMP
From [$(Dimensional)].Dimension.[Event] m
LEFT JOIN [$(Dimensional)].Dimension.DayZone SED ON SED.MasterDayText = ISNULL(CONVERT(VARCHAR,CONVERT(DATE,m.SourceEventDate)),'N/A') and SED.AnalysisDayText = ISNULL(CONVERT(VARCHAR,CONVERT(DATE,m.SourceEventDate)),'N/A')
LEFT JOIN [Reference].[SportsKeywordCountryMap] b on m.SourceEventType = b.Sport and
(
  m.SourceEvent like b.Keyword or m.GrandparentEvent like b.Keyword or m.ParentEvent like b.Keyword or m.GreatGrandparentEvent like b.Keyword or m.SourceSubSportType like b.Keyword
)
Where m.ModifiedBatchKey=IIF(@BatchKey=0,m.ModifiedBatchKey,@BatchKey)	and m.EventKey not in (-1,0)
OPTION (RECOMPILE);

COMMIT TRANSACTION Event_All;

EXEC [Control].Sp_Log	@BatchKey,'SP_Event_All',NULL,'Success',@RowsProcessed = @@RowCount

END TRY
BEGIN CATCH
	Rollback TRANSACTION Event_All;
	declare @Error int;
	declare @ErrorMessage varchar(max);
	                           -- Log the failure at the object level
                           SET    @Error = ERROR_NUMBER()
                           SET    @ErrorMessage = ERROR_MESSAGE()
						   EXEC [Control].Sp_Log @BatchKey, 'SP_Event_All', NULL, 'Failed', @ErrorMessage;
						   Raiserror(@ErrorMessage,16,1)

END CATCH
			
END
	
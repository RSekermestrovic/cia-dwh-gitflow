﻿-- =============================================
-- Author:		Lekha Narra
-- Create date: 12/11/2014
-- Description:	Insert Event names, Round names, Competition names, Grade, Distance  for 'Racing','Greyhounds','Trots' Event types
-- =============================================
CREATE PROCEDURE [Cleanse].[SP_Event_RacingGhTrots] 
	-- Add the parameters for the stored procedure here
	@BatchKey int 
AS
BEGIN
BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

BEGIN TRANSACTION Event_RacingGhTrots;
		
  -- ================================================================================================================================================================
  --                        PART 1: Update Event names in Event Dimension for 'Racing','Greyhounds','Trots' Event types
  -- ================================================================================================================================================================
EXEC [Control].Sp_Log	@BatchKey,'SP_Event_RacingGhTrots','Part 1','Start'	
		--Remove time part from [Event] 
		update e
		set [Event] =CASE 
							when SourceEvent like '%[[]%'
							then RTRIM(LTRIM(SUBString(SourceEvent, CHARINDEX(']',SourceEvent)+1,LEN(SourceEvent))))
							ELSE SourceEvent
							END
		from [$(Dimensional)].Dimension.Event e
		left join (select classKey, SourceClassName from [$(Dimensional)].Dimension.Class) c on e.ClassKey=c.ClassKey
		where SourceClassName in ('Greyhounds','Racing','Trots' )
		and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(2-1))<>Power(2,(2-1)))

		--Remove time part from [Event] 
		update e
		set [Event] =CASE 
							when [Event] like '%[0-9][0-9]:[0-9][0-9] %'
							then RTRIM(LTRIM(SUBString([Event], PATINDEX('%[0-9][0-9]:[0-9][0-9] %',[Event])+5,LEN([Event]))))
							ELSE [Event]
							END
		from [$(Dimensional)].Dimension.Event e
		left join (select classKey, SourceClassName from [$(Dimensional)].Dimension.Class) c on e.ClassKey=c.ClassKey
		where SourceClassName in ('Greyhounds','Racing','Trots')
		and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey)  and (ColumnLock&Power(2,(2-1))<>Power(2,(2-1)))

		--Remove Distance part from [Event]   
		update e
		set [Event] =CASE 
							when [Event] like '%[1-9][0-9][0-9][0-9]m%'
							then RTRIM(LTRIM(SUBString([Event], 1,PATINDEX('%[1-9][0-9][0-9][0-9]m%',[Event])-1)))
							ELSE [Event]
							END
		from [$(Dimensional)].Dimension.Event e
		left join (select classKey, SourceClassName from [$(Dimensional)].Dimension.Class) c on e.ClassKey=c.ClassKey
		where SourceClassName in ('Greyhounds','Racing','Trots' )
		and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(2-1))<>Power(2,(2-1)))

		--Remove Distance part from [Event] 
		update e
		set [Event] =CASE 
							when [Event] like '%[1-9][0-9][0-9]m%'
							then RTRIM(LTRIM(SUBString([Event], 1,PATINDEX('%[1-9][0-9][0-9]m%',[Event])-1)))
							when [Event] like '%[1-9][0-9][0-9] mtrs%'
							then RTRIM(LTRIM(SUBString([Event], 1,PATINDEX('%[1-9][0-9][0-9] mtrs%',[Event])-1)))
							ELSE [Event]
							END
		from [$(Dimensional)].Dimension.Event e
		left join (select classKey, SourceClassName from [$(Dimensional)].Dimension.Class) c on e.ClassKey=c.ClassKey
		where SourceClassName in ('Greyhounds','Racing','Trots')
		and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey)  and (ColumnLock&Power(2,(2-1))<>Power(2,(2-1)))
		
		--Remove Race Number from [Event] 
		update e
		set [Event] =CASE 
							when [Event] like '%Race [1-9]%'
							then RTRIM(LTRIM(SUBString([Event], PATINDEX('%Race [1-9]%',[Event])+6,Len([Event])-6)))
							ELSE [Event]
							END
		from [$(Dimensional)].Dimension.Event e
		left join (select classKey, SourceClassName from [$(Dimensional)].Dimension.Class) c on e.ClassKey=c.ClassKey
		where SourceClassName in ('Greyhounds','Racing','Trots' )
		and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey)  and (ColumnLock&Power(2,(2-1))<>Power(2,(2-1)))
		/**/
		--Remove Race Number from [Event] 
		update e
		set [Event] =CASE 
							When [Event] like 'Race [1-9]'
							then [Event] 
							When [Event] like 'Race [1-9][0-9]'
							then [Event] 
							when [Event] like '%Race [1-9] - %'
							then RTRIM(LTRIM(SUBString([Event], PATINDEX('%Race [1-9]%',[Event])+9,Len([Event])-9)))
							when [Event] like '%Race [1-9][0-9] - %'
							then RTRIM(LTRIM(SUBString([Event], PATINDEX('%Race [1-9][0-9]%',[Event])+10,Len([Event])-10)))
							when [Event] like '%Race [1-9]%'
							then Ltrim(RTRIM(Substring([Event],1,PATINDEX('%Race [1-9]%',[Event])-1)+Substring([Event],PATINDEX('%Race [1-9]%',[Event])+7,(Len([Event])))))
							ELSE [Event]
							END
		from [$(Dimensional)].Dimension.Event e
		left join (select classKey, SourceClassName from [$(Dimensional)].Dimension.Class) c on e.ClassKey=c.ClassKey
		where SourceClassName in ('Greyhounds','Racing','Trots' )
		and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey)  and (ColumnLock&Power(2,(2-1))<>Power(2,(2-1))) 
		
		--Format the [Event] 
		update e
		set [Event] =Rtrim(Ltrim(Replace(Rtrim(Ltrim([Event])),'-','')))
		from [$(Dimensional)].Dimension.Event e
		left join (select classKey, SourceClassName from [$(Dimensional)].Dimension.Class) c on e.ClassKey=c.ClassKey
		where [Event] like '%-'  or [Event] like '-%' or [Event] like '- %' 
		and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(2-1))<>Power(2,(2-1)))
		
		update e
		set [Event] =Rtrim(Ltrim(Replace(Rtrim(Ltrim([EVent])),':','')))
		from [$(Dimensional)].Dimension.Event e
		left join (select classKey, SourceClassName from [$(Dimensional)].Dimension.Class) c on e.ClassKey=c.ClassKey
		where [Event] like '%:'  or [Event] like ':%' or [Event] like ': %' 
		and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(2-1))<>Power(2,(2-1)))
		
		Update e
		set [Event]='Unknown'
		from [$(Dimensional)].Dimension.Event e
		left join (select classKey, SourceClassName from [$(Dimensional)].Dimension.Class) c on e.ClassKey=c.ClassKey
		where  SourceClassName in ('Trots','Greyhounds','Racing') and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and [Event] ='' 
		 and (ColumnLock&Power(2,(2-1))<>Power(2,(2-1)))

		
-- ================================================================================================================================================================
  --                        PART 2: Update Round names in Event Dimension for 'Racing','Greyhounds','Trots' Event types
-- ================================================================================================================================================================
EXEC [Control].Sp_Log	@BatchKey,'SP_Event_RacingGhTrots','Part 2','Start',@RowsProcessed = @@RowCount	

Update b
	 set Round=Rtrim(case when RoundSequence=0
						  then 'Race Unknown'+(case when b.SourceEvent like'%[[]%' 
													then ltrim(Rtrim(Substring(b.SourceEvent,PATINDEX('%[[]%',b.SourceEvent)+1,5))) 
													else '' 
											   end)
						  else  'Race '+Cast(RoundSequence as varchar(10))+' '+(case when b.SourceEvent like'%[[]%' 
																					 then ltrim(Rtrim(Substring(b.SourceEvent,PATINDEX('%[[]%',b.SourceEvent)+1,5)))
																					 else ''
																			    end)
					End)
	from [$(Dimensional)].Dimension.Event b
	left join (select classKey, SourceClassName from [$(Dimensional)].Dimension.Class) c on b.ClassKey=c.ClassKey 
	where b.ModifiedBatchKey=IIF(@BatchKey=0,b.ModifiedBatchKey,@BatchKey) and (b.ColumnLock&Power(2,(10-1))<>Power(2,(10-1)))
	and SourceClassName in ('Greyhounds' ,'Racing','Trots' )
	
	Update e
	set Round='Unknown'
	from [$(Dimensional)].Dimension.Event e
	left join (select classKey, SourceClassName from [$(Dimensional)].Dimension.Class) c on e.ClassKey=c.ClassKey
	where  SourceClassName in ('Trots','Greyhounds','Racing') and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and Round=''
	  and (ColumnLock&Power(2,(10-1))<>Power(2,(10-1)))

-- ================================================================================================================================================================
  --                        PART 3: Update Competition names in Event Dimension for 'Racing','Greyhounds','Trots' Event types
-- ================================================================================================================================================================
EXEC [Control].Sp_Log	@BatchKey,'SP_Event_RacingGhTrots','Part 3','Start'	
	Update e
	set Competition=Venue+' '+cast(Convert(Date,SourceEventDate) as varchar(32))
	from [$(Dimensional)].Dimension.Event e
	left join (select classKey, SourceClassName from [$(Dimensional)].Dimension.Class) c on e.ClassKey=c.ClassKey
	where SourceClassName in ('Greyhounds','Racing','Trots')
	and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1)))
	
	Update e
	set Competition='Unknown'
	from [$(Dimensional)].Dimension.Event e
	left join (select classKey, SourceClassName from [$(Dimensional)].Dimension.Class) c on e.ClassKey=c.ClassKey
	where  SourceClassName in ('Trots','Greyhounds','Racing') and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and Competition=''
	 and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1)))

-- ================================================================================================================================================================
  --                        PART 4: Update Distance in Event Dimension for 'Racing','Greyhounds','Trots' Event types
-- ================================================================================================================================================================
EXEC [Control].Sp_Log	@BatchKey,'SP_Event_RacingGhTrots','Part 4','Start',@RowsProcessed = @@RowCount	
	Update e
	set distance=(Case when(SourceEvent like '%[1-9][0-9][0-9]%m')
					  Then Ltrim(Substring(SourceEvent,Len(SourceEvent)-4,4)+' m')
					  when(SourceEvent like '%[1-9][0-9][0-9]%Mtrs')
					  Then RTRIM(Ltrim(Substring(SourceEvent,Len(SourceEvent)-8,5)))+' m'
					  Else 'Unknown'
				 End)
	from [$(Dimensional)].Dimension.Event e
	left join (select classKey, SourceClassName from [$(Dimensional)].Dimension.Class) c on e.ClassKey=c.ClassKey
	where SourceClassName in ('Trots','Greyhounds','Racing')
	and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(6-1))<>Power(2,(6-1)))
	
	Update e
	set Distance='Unknown'
	from [$(Dimensional)].Dimension.Event e
	left join (select classKey, SourceClassName from [$(Dimensional)].Dimension.Class) c on e.ClassKey=c.ClassKey
	where  SourceClassName in ('Trots','Greyhounds','Racing') and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey)  and Distance=''
	 and (ColumnLock&Power(2,(6-1))<>Power(2,(6-1)))

-- ================================================================================================================================================================
  --                        PART 5: Update Grade in Event Dimension for 'Racing' Event type
-- ================================================================================================================================================================
EXEC [Control].Sp_Log	@BatchKey,'SP_Event_RacingGhTrots','Part 5','Start',@RowsProcessed = @@RowCount	
	Update e
	set Grade=''
	from [$(Dimensional)].Dimension.Event e
	left join (select classKey, SourceClassName from [$(Dimensional)].Dimension.Class) c on e.ClassKey=c.ClassKey
	where SourceClassName='Racing'
	and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(5-1))<>Power(2,(5-1)))

		Update e
		set Grade=Grade+Case when ([Event] like '%FM%' or [Event] like '%F & M%')
							 then '-'+'Fillies & Mares'
							 else ''
						 end

						+Case when ([Event] like '%Benchmark [1-9][0-9]%')
							  then '-'+Rtrim(substring([Event],Charindex('Benchmark',[Event]),13))
							  Else ''
						 End
						 +Case when ([Event] like '%Bm[1-9][0-9]%'and [Event] not like '%Bm[1-9][0-9]+%')
							  then '-'+Rtrim(substring([Event],PATINDEX('%Bm[1-9][0-9]%',[Event]),4))
							  Else ''
						 End
						 +Case when ([Event] like '%Bm[1-9][0-9]+%')
							  then '-'+Rtrim(substring([Event],PATINDEX('%Bm[1-9][0-9]%',[Event]),5))
							  Else ''
						 End
						 +Case when ([Event] like '%Maiden%' or [Event] like '%Mdn%')
							  then '-'+'Maiden'
							  Else ''
						 End
						 +Case when ([Event] like '%Auction%')
							  then '-'+'Auction'
							  Else ''
						 End
						 +Case when ([Event] like '%Plate%' or [Event] like '%Plte%')
							  then '-'+'Plate'
							  Else ''
						 End
						 +Case when ([Event] like '%C[1-9]%')
							  then '-'+Rtrim(substring([Event],PATINDEX('%C[1-9]%',[Event]),2))
							  Else ''
						 End
						 +Case when ([Event] like '%Fillies%')
							  then '-'+'Fillies'
							  Else ''
						 End
						 +Case when ([Event]  like '%Derby%')
							  then '-'+'Derby'
							  Else ''
						 End
						 +Case when ([Event] like '%Divided%')
							  then '-'+'Divided'
							  Else ''
						 End
						 +Case when ([Event] like '%Handicap%' or [Event] like '%Hcp%' or [Event] like '%Hcap%')
							  then '-'+'Handicap'
							  Else ''
						 End
						 +Case when ([Event] like '%Hcp-[1-9][0-9]%')
							  then '-'+Rtrim(substring([Event],PATINDEX('%Hcp-[1-9][0-9]%',[Event])+4,3))
							  Else ''
						 End
						 +Case when ([Event] like '%Hcp-([1-9][0-9])%')
							  then '-'+Rtrim(substring([Event],PATINDEX('%Hcp-([1-9][0-9])%',[Event])+5,2))
							  Else ''
						 End
						 +Case when ([Event] like '%Handicap [1-9][0-9]%')
							  then '-'+Rtrim(substring([Event],PATINDEX('%Handicap [1-9][0-9]%',[Event])+9,2))
							  Else ''
						 End
						 +Case when ([Event] like '%[1-9][0-9] Handicap%' and [Event] not like '%MR [1-9][0-9] Handicap%')
							  then '-'+Rtrim(substring([Event],PATINDEX('%[1-9][0-9] Handicap%',[Event]),2))
							  Else ''
						 End
						 +Case when ([Event] like '%Hcp([1-9][0-9])%' or [Event] like '%Hcp([1-9][0-9])%')
							  then '-'+Ltrim(Rtrim(substring([Event],PATINDEX('%Hcp%([1-9][0-9])%',[Event])+3,3)))
							  Else ''
						 End
						 +Case when ([Event] like '%Handicap Chase%')
							  then '-'+'Chase'
							  Else ''
						 End
						 +Case when ([Event] like '%Hcp ([1-9][0-9]) %' or [Event] like '%Hcp-[1-9][0-9] %')
							  then '-'+Rtrim(substring([Event],PATINDEX('%Hcp ([1-9][0-9]) %',[Event])+5,3))
							  Else ''
						 End
						 +Case when ([Event]  like '%Yearling%')
							  then '-'+'Yearling'
							  Else ''
						 End
						 +Case when ([Event]  like '%Straight%' or [Event]  like '%Str %')
							  then '-'+'Straight'
							  Else ''
						 End
						 +Case when ([Event]  like '%Stake%' or [Event]  like '% Stks%')
							  then '-'+'Stakes'
							  Else ''
						 End
						 +Case when ([Event]  like '%Group_[1-9]%')
							  then '-'+'Group'+Rtrim(substring([Event],PATINDEX('%Group%',[Event])+5,2))
							  Else ''
						 End
						 +Case when ([Event]  like '%Grp_[1-9]%')
							  then '-'+'Group'+Rtrim(substring([Event],PATINDEX('%Grp%',[Event])+3,2))
							  Else ''
						 End
						 +Case when ([Event]  like '%Hurdle%' or [Event]  like '%Hrdl%')
							  then '-'+'Hurdle'
							  Else ''
						 End
						 +Case when ([Event] like '%Hrdl-[1-9][0-9]%' or [Event] like '%Hrdl ([1-9][0-9]) %')
							  then '-'+Rtrim(substring([Event],PATINDEX('%Hrdl-[1-9][0-9]%',[Event])+5,3))
							  Else ''
						 End
						 +Case when ([Event]  like '%Hurdle-[1-9][0-9]%')
							  then '-'+Rtrim(substring([Event],PATINDEX('%Hurdle-[1-9][0-9] %',[Event])+8,3))
							  Else ''
						 End
						 +Case when ([Event] like '%Hwt-[1-9][0-9]%')
							  then '-'+'Hwt '+Rtrim(substring([Event],PATINDEX('%Hwt-[1-9][0-9]%',[Event])+4,3))
							  Else ''
						 End
						 +Case when ([Event]  like '%Flat%')
							  then '-'+'Flat Race'
							  Else ''
						 End
						 +Case when ([Event]  like '%Cl _%')
							  then '-'+Rtrim(substring([Event],PATINDEX('%Cl _%',[Event]),4))
							  Else ''
						 End
						  +Case when ([Event]  like '%Cl_[1-9]%')
							  then '-'+Rtrim(substring([Event],PATINDEX('%Cl_[1-9]%',[Event])+2,2))
							  Else ''
						 End
						  +Case when ([Event]  like '% F %')
							  then '-'+'F'
							  Else ''
						 End
						  +Case when ([Event]  like '% [1-9]m%')
							  then '-'+Rtrim(substring([Event],PATINDEX('%[1-9]m%',[Event]),2))
							  Else ''
						 End
						  +Case when ([Event]  like '% [1-9]f%' and [Event] not like '%[1-9]m%[1-9]f%')
							  then '-'+Rtrim(substring([Event],PATINDEX('%[1-9]f%',[Event]),2))
							  Else ''
						 End
						  +Case when ([Event] like '%[1-9]m%[1-9]f%')
							  then '-'+Rtrim(substring([Event],PATINDEX('%[1-9]m%',[Event])+3,PATINDEX('%[1-9]f%',[Event])+1))
							  Else ''
						 End
						  +Case when ([Event]  like '%LEG[1-9]%' or [Event]  like '%LEG [1-9]%')
							  then '-'+'Leg'+Rtrim(substring([Event],PATINDEX('%LEG_[1-9]%',[Event])+3,2))
							  Else ''
						 End
						  +Case when ([Event]  like '%PICK[1-9]%' or [Event]  like '%PICK [1-9]%' )
							  then '-'+'Pick'+Ltrim(Rtrim(substring([Event],PATINDEX('%PICK_[1-9]%',[Event])+4,2)))
							  Else ''
						 End
						  +Case when ([Event]  like '%DIV[1-9]%' or [Event]  like '%DIV [1-9]%')
							  then '-'+'Div'+Ltrim(Rtrim(substring([Event],PATINDEX('%DIV_[1-9]%',[Event])+3,2)))
							  Else ''
						 End
						  +Case when ([Event]  like '%Listed%')
							  then '-'+'Listed'
							  Else ''
						 End
						  +Case when ([Event]  like '%Nursery%')
							  then '-'+'Nursery'
							  Else ''
						 End
						 +Case when ([Event]  like '%Grade_[1-9]%')
							  then '-'+Rtrim(substring([Event],PATINDEX('%Grade_[1-9]%',[Event]),7))
							  Else ''
						 End
						 +Case when ([Event]  like '%[1-9][0-9]-[1-9][0-9]%' and [Event] not like '%[1-9][0-9]-[1-9][0-9][0-9]%')
							  then '-'+Rtrim(substring([Event],PATINDEX('%[1-9][0-9]-[1-9][0-9]%',[Event]),5))
							  Else ''
						 End
						 +Case when ([Event]  like '%[1-9]_-[1-9][0-9][0-9]%')
							  then '-'+Rtrim(substring([Event],PATINDEX('%[1-9]_-[1-9][0-9][0-9]%',[Event]),5))
							  Else ''
						 End
						 +Case when ([Event]  like '%French Tote Only%')
							  then '-'+Rtrim(substring([Event],PATINDEX('%French Tote Only%',[Event]),16))
							  Else ''
						 End
						 +Case when ([Event]  like '%[1-9] YO Only%' or [Event]  like '%[1-9]YO Only%')
							  then '-'+Rtrim(substring([Event],PATINDEX('%[1-9]%Only%',[Event]),1))+'YO Only'
							  Else ''
						 End
						 +Case when ([Event]  like '%[1-9] YO Plus%' or [Event]  like '%[1-9]YO Plus%')
							  then '-'+Rtrim(substring([Event],PATINDEX('%[1-9]YO Plus%',[Event]),8))
							  Else ''
						 End
						 +Case when ([Event]  like '%[1-9] YO+%' or [Event]  like '%[1-9]YO+%' )
							  then '-'+Rtrim(substring([Event],PATINDEX('%[1-9]_YO +%',[Event]),5))
							  Else ''
						 End
						 +Case when ([Event]  like '%[1-9]YO/[1-9]YO%' )
							  then '-'+Rtrim(substring([Event],PATINDEX('%[1-9]YO/[1-9]YO%',[Event]),7))
							  Else ''
						 End
						 +Case when (([Event]  like '%[1-9] YO%' or [Event]  like '%[1-9]YO%') and [Event] not like '%[1-9]YO plus%' 
						 and [Event] not like '%[1-9]YO only%' and [Event] not like '%[1-9]YO+%' and [Event] not like '%[1-9]YO/[1-9]YO%')
							  then '-'+Rtrim(substring([Event],PATINDEX('%[1-9]%YO%',[Event]),4))
							  Else ''
						 End
						 +Case when ([Event]  like '%Staple%' or [Event] like '%stpl%')
							  then '-'+'Staple'
							  Else ''
						 End
						 +Case when ([Event]  like '%Class %')
							  then '-'+Rtrim(substring([Event],PATINDEX('%Class%',[Event]),8))
							  Else ''
						 End
						 +Case when ([Event]  like '%Stud-[1-9][0-9]%')
							  then '-'+Rtrim(substring([Event],PATINDEX('%Stud%',[Event]),7))
							  Else ''
						 End
						 +Case when ([Event]  like '% Mare %' or [Event] like '% MR %')
							  then '-'+'Mare'
							  Else ''
						 End
						 +Case when ([Event] like '%MR[1-9][0-9]%')
							  then '-'+'Mare'+Rtrim(substring([Event],PATINDEX('%Mr%',[Event])+2,3))
							  Else ''
						 End
						 +Case when ([Event]  like '%Mr [1-9][0-9]%')
							  then '-'+Rtrim(substring([Event],PATINDEX('%Mr%',[Event])+3,3))
							  Else ''
						 End
						 +Case when ([Event]  like '%[1-9][0-9] Mr%')
							  then '-'+Rtrim(substring([Event],PATINDEX('%Mr%',[Event]),3))
							  Else ''
						 End
						 +Case when ([Event] like '%Rnd%')
							  then '-'+'RND'
							  Else ''
						 End
						 +Case when ([Event] like '%Ntnl%')
							  then '-'+'Ntnl'
							  Else ''
						 End
						 +Case when (substring([Event],Len([Event])-2,3) like '%-[1-9][0-9]%' and right([Event],7) not like '%Hwt-[1-9][0-9]%' 
									and [Event] not like '%Hcp-[1-9][0-9]%' and [Event] not like '%Bm-[1-9][0-9]%' and [Event] not like '%Mr-[1-9][0-9]%')
							  then '-'+substring([Event],Len([Event])-1,2)
							  Else ''
						 End
						 +Case when (substring([Event],Len([Event])-3,4) like '%([1-9][0-9])%' 
						             and Grade not like substring([Event],Len([Event])-2,2) and SourceClassName='Racing')
							  then '-'+substring([Event],Len([Event])-2,2)
							  Else ''
						 End
		from [$(Dimensional)].Dimension.Event e
		left join (select classKey, SourceClassName from [$(Dimensional)].Dimension.Class) c on e.ClassKey=c.ClassKey				
		where  ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey)  and SourceClassName='Racing' and (ColumnLock&Power(2,(5-1))<>Power(2,(5-1)))
			
		Update e
		set Grade=Replace(Grade,'+',' plus')
		from [$(Dimensional)].Dimension.Event e
		left join (select classKey, SourceClassName from [$(Dimensional)].Dimension.Class) c on e.ClassKey=c.ClassKey
		where  SourceClassName='Racing' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(5-1))<>Power(2,(5-1)))
				
		Update e
		set Grade=Substring(Grade,2,Len(Grade)-1)
		from [$(Dimensional)].Dimension.Event e
		left join (select classKey, SourceClassName from [$(Dimensional)].Dimension.Class) c on e.ClassKey=c.ClassKey
		where Grade<>'' and  SourceClassName='Racing'
		and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(5-1))<>Power(2,(5-1)))

		Update e
		set Grade=Ltrim(Replace(Replace(Grade,'(',''),')',''))
		from [$(Dimensional)].Dimension.Event e
		left join (select classKey, SourceClassName from [$(Dimensional)].Dimension.Class) c on e.ClassKey=c.ClassKey
		where  SourceClassName='Racing' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(5-1))<>Power(2,(5-1)))

		Update e
		set Grade='Unknown'
		from [$(Dimensional)].Dimension.Event e
		left join (select classKey, SourceClassName from [$(Dimensional)].Dimension.Class) c on e.ClassKey=c.ClassKey
		where  SourceClassName='Racing' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and Grade='' and (ColumnLock&Power(2,(5-1))<>Power(2,(5-1)))

-- ================================================================================================================================================================
  --                        PART 6: Update Grade in Event Dimension for 'Greyhounds' Event type
-- ================================================================================================================================================================
		EXEC [Control].Sp_Log	@BatchKey,'SP_Event_RacingGhTrots','Part 6','Start',@RowsProcessed = @@RowCount	
		Update e
		set Grade=''
		from [$(Dimensional)].Dimension.Event e
		left join (select classKey, SourceClassName from [$(Dimensional)].Dimension.Class) c on e.ClassKey=c.ClassKey
		where SourceClassName='Greyhounds'  and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(5-1))<>Power(2,(5-1)))
		
		Update e
		set Grade=Grade+Case when ([Event] like '%C[1-9]%')
								 then '-'+Rtrim(substring([Event],PATINDEX('%C[1-9]%',[Event]),2))
								 Else ''
							End
						   +Case when ([Event] like '%C [1-9]/[1-9]%')
								  then '-'+Rtrim(substring([Event],PATINDEX('%C [1-9]/[1-9]%',[Event]),5))
								  Else ''
							 End
						   +Case when ([Event] like '%Maiden%'  or [Event] like '%Mdn%')
								  then '-Maiden'
								  Else ''
							 End
						    +Case when ([Event] like '%Mixed%')
								  then '-Mixed'
								  Else ''
							 End
						    +Case when ([Event] like '%Novice%')
								  then '-Novice'
								  Else ''
							 End
							+Case when ([Event] like '%Juvenile%')
								  then '-Juvenile'
								  Else ''
							 End
							+Case when ([Event] like '%Group Listed%')
								  then '-Group Listed'
								  Else ''
							 End
							+Case when ([Event] like '%Stake%' or [Event] like '%Stk%')
								  then '-Stake'
								  Else ''
							 End
							+Case when ([Event] like '%[0-9]-[1-9] wins%')
								  then '-'+Rtrim(substring([Event],PATINDEX('%[0-9]-[1-9] wins%',[Event]),8))
								  Else ''
							 End
							+Case when ([Event] like '%Sprint%')
								  then '-Sprint'
								  Else ''
							 End
							 +Case when ([Event] like '%A[1-9]%')
								  then '-'+Rtrim(substring([Event],PATINDEX('%A[1-9]%',[Event]),3))
								  Else ''
							 End
							 +Case when ([Event] like '%S[1-9]%')
								  then '-'+Rtrim(substring([Event],PATINDEX('%S[1-9]%',[Event]),3))
								  Else ''
							 End
							+Case when ([Event] like '%D[1-9]%')
								  then '-'+Rtrim(substring([Event],PATINDEX('%D[1-9]%',[Event]),3))
								  Else ''
							 End
							+Case when ([Event] like '%H[1-9]%')
								  then '-'+Rtrim(substring([Event],PATINDEX('%H[1-9]%',[Event]),3))
								  Else ''
							 End
							+Case when ([Event] ='Or')
								  then '-'+'Or'
								  Else ''
							 End
							 +Case when ([Event] like '%Tier [1-9]%')
								  then '-'+Rtrim(substring([Event],PATINDEX('%Tier [1-9]%',[Event]),6))
								  Else ''
							 End
							 +Case when ([Event] like '%Derby%')
								  then '-'+'Derby'
								  Else ''
							 End
							 +Case when ([Event] like '%Stud%')
								  then '-'+'Stud'
								  Else ''
							 End
							 +Case when ([Event] like '%Dash%')
								  then '-'+'Dash'
								  Else ''
							 End
							 +Case when ([Event] like '%Handicap%' or [Event] like '%Hcp%' or [Event] like '%Hcap%')
								  then '-'+'Handicap'
								  Else ''
							 End
							 +Case when ([Event]  like '%Race winners only%')
								  then '-'+'Race winners only'
								  Else ''
							 End
							+Case when ([Event]  like '%[1-9][0-9][0-9]+ rank%')
								  then '-'+Rtrim(substring([Event],PATINDEX('%[1-9][0-9][0-9]+ rank%',[Event]),9))
								  Else ''
							 End
							 +Case when ([Event] like '%[1-9]__ Grade%' and [Event] not like '%[1-9]__ and [1-9]__ Grade%')
								  then '-'+Rtrim(substring([Event],PATINDEX('%[1-9]__ Grade%',[Event]),9))
								  Else ''
							 End
							 +Case when ([Event] like '%Grade [1-9]%')
								  then '-'+Rtrim(substring([Event],PATINDEX('%Grade [1-9]%',[Event]),7))
								  Else ''
							 End
							 +Case when ([Event] like '%GR_[1-9]%' and [Event] not like '%[1-9]__ and [1-9]__ Grade%')
								  then '-'+Rtrim(substring([Event],PATINDEX('%GR_[1-9]%',[Event]),4))
								  Else ''
							 End
							 +Case when ([Event] like '%[1-9]__ and [1-9]__ Grade%')
								  then '-'+Rtrim(substring([Event],PATINDEX('%[1-9]__ and [1-9]__ Grade%',[Event]),17))
								  Else ''
							 End
							 +Case when ([Event] like '%Ht [1-9]%')
								  then '-'+Rtrim(substring([Event],PATINDEX('%Ht [1-9]%',[Event]),4))
								  Else ''
							 End
							 +Case when ([Event]  like '%Group_[1-9]%')
								  then '-'+Rtrim(substring([Event],PATINDEX('%Group%',[Event])+5,2))
								  Else ''
							 End
							 +Case when ([Event]  like '%Restricted Age%')
								  then '-'+'Restricted Age'
								  Else ''
							 End
							  +Case when ([Event]  like '%Grade Restricted%' or [Event]  like '% Gdr Restricted%')
								  then '-'+'Grade Restricted'
								  Else ''
							 End
							 +Case when ([Event]  like '%Non Graded%')
								  then '-'+'Non Graded'
								  Else ''
							 End
							  +Case when ([Event]  like '%No Penalty%')
								  then '-'+'No Penalty'
								  Else ''
							 End
							  +Case when ([Event]  like '%ffa%'or [Event] like '%free for all%')
								  then '-'+'Free for all'
								  Else ''
							 End
		from [$(Dimensional)].Dimension.Event e
		left join (select classKey, SourceClassName from [$(Dimensional)].Dimension.Class) c on e.ClassKey=c.ClassKey					 
		where  SourceClassName='Greyhounds'	and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(5-1))<>Power(2,(5-1)))
		
		Update e
		set Grade=Replace(Grade,'+',' plus')
		from [$(Dimensional)].Dimension.Event e
		left join (select classKey, SourceClassName from [$(Dimensional)].Dimension.Class) c on e.ClassKey=c.ClassKey
		where  SourceClassName='Greyhounds'
		 and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(5-1))<>Power(2,(5-1)))
				
		Update e
		set Grade=Substring(Grade,2,Len(Grade)-1)
		from [$(Dimensional)].Dimension.Event e
		left join (select classKey, SourceClassName from [$(Dimensional)].Dimension.Class) c on e.ClassKey=c.ClassKey
		where Grade<>'' and  SourceClassName='Greyhounds'
		 and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(5-1))<>Power(2,(5-1)))

		Update e
		set Grade=Ltrim(Replace(Replace(Grade,'(',''),')',''))
		from [$(Dimensional)].Dimension.Event e
		left join (select classKey, SourceClassName from [$(Dimensional)].Dimension.Class) c on e.ClassKey=c.ClassKey
		where  SourceClassName='Greyhounds'
		 and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(5-1))<>Power(2,(5-1)))
		 
		Update e
		set Grade='Unknown'
		from [$(Dimensional)].Dimension.Event e
		left join (select classKey, SourceClassName from [$(Dimensional)].Dimension.Class) c on e.ClassKey=c.ClassKey
		where  SourceClassName='Greyhounds' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey)  and Grade='' and (ColumnLock&Power(2,(5-1))<>Power(2,(5-1)))

-- ================================================================================================================================================================
  --                        PART 7: Update Grade in Event Dimension for 'Trots' Event type
-- ================================================================================================================================================================
		EXEC [Control].Sp_Log	@BatchKey,'SP_Event_RacingGhTrots','Part 7','Start',@RowsProcessed = @@RowCount	
		Update e
		set Grade=''
		from [$(Dimensional)].Dimension.Event e
		left join (select classKey, SourceClassName from [$(Dimensional)].Dimension.Class) c on e.ClassKey=c.ClassKey
		where SourceClassName='Trots' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(5-1))<>Power(2,(5-1)))

		Update e
		set Grade=Grade+Case when ([Event] like '%[^a-z,^0-9]FM[^a-z,^0-9]%' or [Event] like '%F & M%' or [Event] like '%F&M%' or [Event] like '%[^a-z,^0-9]F M[^a-z,^0-9]%')
							 then '-'+'Fillies Mares'
							 else ''
						End
						+Case when ([Event] like '%Colts%' or [Event] like '%[,",",&, ]C[ ,",",&,]%' 
						or [Event] like 'C[ ,",",&,]%' or [Event] like '%[ ,",",&,]C[^a-z,^0-9,^'']%')
							 then '-'+'Colts'
							 else ''
						End
						+Case when ([Event] like '%Geldings%' or [Event] like '%[",",&, ]G[ ,",",&,]%' 
						or [Event] like 'G[ ,",",&]%' or [Event] like '%[ ,",",&]G[^a-z,^0-9,^'']%')
							 then '-'+'Geldings'
							 else ''
						End
						+Case when ([Event] like '%Entires%' or [Event] like '%[",",&, ]E[ ,",",&,]%' 
						or [Event] like 'E[ ,",",&,]%' or [Event] like '%[ ,",",&,]E[^a-z,^0-9,^'']%')
							 then '-'+'Entires'
							 else ''
						End
						+Case when ([Event] like '%Fillies%')
							 then '-'+'Fillies'
							 else ''
						End
						+Case when ([Event] like '%Sire%')
							 then '-'+'Sires'
							 else ''
						End
						+Case when ([Event] like '%Stud%')
							 then '-'+'Stud'
							 else ''
						End
						+Case when ( [Event] like '%Mares%')
							 then '-'+'Mares'
							 else ''
						End
						+Case when ([Event] like '%Stakes%')
							 then '-'+'Stakes'
							 else ''
						End
						+Case when ([Event] like '%Swedish Tote Only%')
							 then '-'+'Swedish Tote Only'
							 else ''
						End
						+Case when ([Event] like '%French Tote Only%')
							 then '-'+'French Tote Only'
							 else ''
						End
						+Case when ([Event] like '%[^a-z,^0-9][m,c,r,t,g,q][0-9] %'  and [Event] not like '%[m,c,r,t,g,q][0-9] to [m,c,r,t,g,q][1-9]%'  
									and [Event] not like '%[m,c,r,t,g,q][0-9]/[m,c,r,t,g,q][0-9]%'  and [Event] not like '%tq[0-9]%'	and [Event] not like '%[2,3]_[0-9]%'
									and [Event] not like '%[m,c,r,t,g,q][0-9]/[0-9]%'  and [Event] not like '%[m,c,r,t,g,q][0-9]-[m,c,r,t,g,q][0-9]%'
									and [Event] not like '%[m,c,r,t,g,q][0-9] or better%' and Rtrim(substring([Event],PATindex('%r[0-9]%',[Event])-1,3)) not like '%Gr[0-9]%')
							 then '-'+Rtrim(substring([Event],PATindex('%[m,c,r,t,g,q][0-9]%',[Event]),2))
							 else ''
						End
						+Case when ([Event] like '%[^a-z,^0-9][m,c,r,t,g,q][0-9]+%')
							 then '-'+Rtrim(substring([Event],PATINDEX('%[m,c,r,t,g,q][0-9]+%',[Event]),3))
							 else ''
						End
						+Case when ([Event] like '%[^a-z,^0-9][m,c,r,t,g,q][0-9]-[m,c,r,t,g,q][0-9]%')
							 then '-'+Rtrim(substring([Event],PATINDEX('%[m,c,r,t,g,q][0-9]-[m,c,r,t,g,q][0-9]%',[Event]),5))
							 else ''
						End
						+Case when ([Event] like '%[^a-z,^0-9][m,c,r,t,g,q][0-9][0-9]-[m,c,r,t,g,q][0-9][0-9]%')
							 then '-'+Rtrim(substring([Event],PATINDEX('%[m,c,r,t,g,q][0-9][0-9]-[m,c,r,t,g,q][0-9][0-9]%',[Event]),7))
							 else ''
						End
						+Case when ( [Event] like '%[^a-z,^0-9][m,c,r,t,g,q][0-9][0-9] to [m,c,r,t,g,q][0-9][0-9]%')
							 then '-'+Rtrim(substring([Event],PATINDEX('%[m,c,r,t,g,q][0-9][0-9] to [m,c,r,t,g,q][0-9][0-9]%',[Event]),10))
							 else ''
						End
						+Case when ([Event] like '%[m,c,r,t,g,q][0-9] to [c,m,r,t,g,q][1-9]%')
							 then '-'+Rtrim(substring([Event],PATINDEX('%[m,c,r,t,g,q][0-9] to [m,c,r,t,g,q][1-9]%',[Event]),8))
							 else ''
						End
						+Case when ([Event] like '%[^a-z,^0-9][m,c,r,g,q,t][0-9] or better%')
							 then '-'+Rtrim(substring([Event],PATINDEX('%[m,c,r,g,q,t][0-9] or better%',[Event]),2))+' or better'
							 else ''
						End
						+Case when ([Event] like '%[^a-z,^0-9][m,c,r,t,g,q][0-9]/[0-9]%' and [Event] not like '%3[c,r,t,g,q][0-9]/[0-9]%')
							 then '-'+Rtrim(substring([Event],PATINDEX('%[m,c,r,t,g,q][0-9]/[0-9]%',[Event]),4))
							 else ''
						End
						+Case when ([Event]  like '%[^a-z,^0-9][m,c,r,t,g,q][0-9]/[m,c,r,t,g,q][0-9]%' and [Event] not like '%3[m,c,r,t,g,q][0-9]/3[m,c,r,t,g,q][0-9]%')
							 then '-'+Rtrim(substring([Event],PATINDEX('%[m,c,r,t,g,q][0-9]/[m,c,r,t,g,q][0-9]%',[Event]),5))
							 else ''

						End
						+Case when ([Event]  like '%tQ[0-9]%' and [Event]  not like '%tQ[0-9]/tQ[0-9]%'  and [Event]  not like '%tQ[0-9] or better%')
							 then '-'+Ltrim(Rtrim(substring([Event],PATINDEX('%tQ[0-9]%',[Event]),3)))
							 else ''
						End
						+Case when ([Event]  like '%tQ[0-9] or better%')
							 then '-'+Ltrim(Rtrim(substring([Event],PATINDEX('%tQ[0-9]%',[Event]),3)))+' or better'
							 else ''
						End
						+Case when ([Event] like '% [2,3]_[0-9] %' and [Event] not like '%[2,3]_[0-9]_to_[2,3]_[0-9]%' and [Event] not like '%[2,3]_[0-9] or better%')
							 then '-'+Rtrim(substring([Event],PATINDEX('%[2,3]_[0-9]%',[Event]),3))
							 else ''
						End
						+Case when ([Event] like '%[2,3]_[0-9]_to_[2,3]_[0-9]%')
							 then '-'+Rtrim(substring([Event],PATINDEX('%[2,3]_[0-9]_to_[2,3]_[0-9]%',[Event]),10))
							 else ''
						End
						+Case when ([Event] like '%[2,3]_[0-9]/[2,3]_[0-9]%')
							 then '-'+Rtrim(substring([Event],PATINDEX('%[2,3]_[0-9]/[2,3]_[0-9]%',[Event]),7))
							 else ''
						End
						+Case when ([Event] like '%[2,3]_[0-9]/[0-9]%')
							 then '-'+Rtrim(substring([Event],PATINDEX('%[2,3]_[0-9]/[0-9]%',[Event]),5))
							 else ''
						End
						+Case when ([Event] like '%[2,3]_[0-9] or better%')
							 then '-'+Rtrim(substring([Event],PATINDEX('%[2,3]_[0-9] or better%',[Event]),13))
							 else ''
						End
						+Case when ([Event]  like '%[^a-z,^0-9]Mobile%'  or [Event]  like '%Mobile[^a-z,^0-9]%' or [Event]  like '%[^a-z,^0-9]Ms%')
							 then '-'+'Mobile Start'
							 else ''
						End
						+Case when ([Event]  like '%[^a-z,^0-9]Ss%' or [Event]  like '%[^a-z,^0-9]Stand%' or [Event]  like '%Stand[^a-z,^0-9]%')
							 then '-'+'Stand Start'
							 else ''
						End
						+Case when ([Event] like '%Ffa%' or  [Event]  like '%Free for all%')
							 then '-'+'Free for all'
							 else ''
						End
						+Case when ([Event] like '%Handicap%' or [Event] like '%Hcp%' or [Event] like '%Hcap%')
							 then '-'+'Handicap'
							 else ''
						End
						+Case when ([Event]  like '%Heat_[1-9]%')
							 then '-'+'Heat'+Rtrim(substring([Event],PATINDEX('%Heat%',[Event])+4,2))
							 else ''
						End
						+Case when ([Event] like '%Heat' and  [Event] not like '%[1-9][s,t,r,n][t,h,d] Heat')
							 then '-'+'Heat'
							 else ''
						End
						+Case when ([Event] like '%Heat%'and [Event] not like '%Heat' 
									and [Event] not like '%Heat_[1-9]%' and [Event] not like '%[1-9][s,t,r,n][t,h,d] Heat%')
							 then '-'+'Heat '+Substring(substring([Event],PATINDEX('%Heat %',[Event])+5,Len([Event])),1,PATINDEX('% %',substring([Event],PATINDEX('%Heat %',[Event])+5,Len([Event]))+' '))
							 else ''
						End
						+Case when ([Event] like '%[1-9][s,t,r,n][t,h,d] Heat%'  and  [Event] not like '%Heat [0-9]%')
							 then '-'+'Heat '+Rtrim(substring([Event],PATINDEX('%Heat%',[Event])-4,1))
							 else ''
						End
						+Case when ([Event]  like '%Division_[1-9][^a-z,^0-9]%')
							 then '-'+'Division'+Rtrim(substring([Event],PATINDEX('%Division%',[Event])+9,2))
							 else ''
						End
						+Case when ([Event] like '%[^a-z]Division %'  and [Event] not like '%Division' 
									and [Event] not like '%Division_[1-9][^a-z,^0-9]%' and [Event] not like '%[1-9][s,t,r,n][t,h,d] Division%')
							 then '-'+'Division '+
							 Substring(substring([Event],PATINDEX('%Division %',[Event])+9,Len([Event])),1,PATINDEX('% %',substring([Event],PATINDEX('%Division %',[Event])+9,Len([Event]))+' '))
							 else ''
						End
						+Case when ([Event] like '%[1-9][s,t,r,n][t,h,d] Division%'  and  [Event] not like '%Division [0-9]%')
							 then '-'+'Division '+Rtrim(substring([Event],PATINDEX('%Division%',[Event])-4,1))
							 else ''
						End
						+Case when ([Event] like '%[^a-z]Div %' and [Event] not like '%Div [0-9]%' and [Event] not like '%Div'  
									and [Event] not like '%Div_[1-9][^a-z,^0-9]%' and [Event] not like '%[1-9][s,t,r,n][t,h,d] Div%')
							 then '-'+'Div '+
							 Substring(substring([Event],PATINDEX('%Div %',[Event])+4,Len([Event])),1,PATINDEX('% %',substring([Event],PATINDEX('%Div %',[Event])+4,Len([Event]))+' '))
							 else ''
						End
						+Case when ([Event] like '%[1-9][s,t,r,n][t,h,d] Div %'  and  [Event] not like '%Div [0-9]%')
							 then '-'+'Div '+Rtrim(substring([Event],PATINDEX('%Div%',[Event])-4,1))
							 else ''
						End
						+Case when ([Event]  like '%Group_[1-9][^a-z]%')
							 then '-'+'Group'+Ltrim(Rtrim(substring([Event],PATINDEX('%Group%',[Event])+5,2)))
							 else ''
						End
						+Case when ([Event]  like '%Grp_[1-9][^a-z]%')
							 then '-'+'Group'+Ltrim(Rtrim(substring([Event],PATINDEX('%Gr%',[Event])+3,2)))
							 else ''
						End
						+Case when ([Event]  like '%Gr[1-9][^a-z]%')
							 then '-'+'Gr'+Ltrim(Rtrim(substring([Event],PATINDEX('%Gr%',[Event])+2,1)))
							 else ''
						End
						+Case when ([Event]  like '% Ht%' and [Event] not like '%Ht [1-9]%')
							 then '-'+'Ht'
							 else ''
						End
						+Case when ([Event] like '%[^a-z]Ht [1-9]%')
							 then '-'+'Ht'+Rtrim(substring([Event],PATINDEX('%Ht [1-9]%',[Event])+3,1))
							 else ''
						End
						+Case when ([Event]  like '%[1-9]YO & older%' or [Event]  like '%[1-9]YO/older%')
							 then '-'+Rtrim(substring([Event],PATINDEX('%[1-9]YO%',[Event]),1))+'YO & Older'
							 else ''
						End
						+Case when ([Event]  like '%[1-9]_YO+%' or [Event]  like '%[1-9]YO+%' or [Event]  like '%[1-9]YO plus%')
							 then '-'+Rtrim(substring([Event],PATINDEX('%[1-9][^a-z,^0-9]YO%',[Event]),1))+'YO Plus'
							 else ''
						End
						+Case when ([Event]  like '%[1-9]YO%' and  [Event] not like '%[1-9]-[1-9]YO%' and  [Event] not like '%[1-9]-[1-9][0-9]YO%' 
									and  [Event] not like '%[1-9]&[1-9]YO%' and  [Event] not like '%[1-9] YO+%' and  [Event] not like '%[1-9]YO%older%' 
									and  [Event] not like '% [1-9] [1-9]YO%'  and [Event] not like '%[1-9]YO-[1-9]YO%' 
									and  [Event] not like '%[1-9][^a-z,^0-9]YO+%' and  [Event] not like '%[1-9]YO plus%' )
							 then '-'+Rtrim(substring([Event],PATINDEX('%[1-9]YO%',[Event]),1)+'YO')
							 else ''
						End
						+Case when ([Event]  like '%[1-9] [1-9]_YO%')
							 then '-'+Rtrim(substring([Event],PATINDEX('%[1-9] [1-9]_YO%',[Event]),3))+'YO'
							 else ''
						End
						+Case when ([Event]  like '%[1-9],[1-9]&[1-9]_YO%')
							 then '-'+Rtrim(substring([Event],PATINDEX('%[1-9],[1-9]&[1-9]_YO%',[Event]),5))+'YO'
							 else ''
						End
						+Case when ([Event]  like '% Year Old%')
							 then '-'+Rtrim(LTRIM(REVERSE(LEFT(REVERSE(substring([Event],1,PATINDEX('% Year Old%',[Event])-1)), CHARINDEX(' ', REVERSE(substring([Event],1,PATINDEX('% Year Old%',[Event])-1))+' ') - 1))))+'YO'
							 else ''
						End
						+Case when ([Event]  like '%[1-9]-[1-9]YO%')
							 then '-'+Rtrim(substring([Event],PATINDEX('%[1-9]-[1-9]YO%',[Event]),3))+'YO'
							 else ''
						End
						+Case when ([Event]  like '%[1-9]-[1-9][0-9]YO%')
							 then '-'+Rtrim(substring([Event],PATINDEX('%[1-9]-[1-9][0-9]YO%',[Event]),4))+'YO'
							 else ''
						End
						+Case when ([Event]  like '%[1-9]YO-[1-9]YO%')
							 then '-'+Replace(Rtrim(substring([Event],PATINDEX('%[1-9]YO-[1-9]YO%',[Event]),5)),'YO','')+'YO'
							 else ''
						End
						+Case when ([Event]  like '%[1-9] & [1-9]YO%')
							 then '-'+Rtrim(substring([Event],PATINDEX('%[1-9] & [1-9]YO%',[Event]),5)+'YO')
							 else ''
						End
						+Case when ([Event] like '%[1-9]&[1-9]YO%' and [Event] not like '%[1-9],[1-9]&[1-9]YO%')
							 then '-'+Rtrim(substring([Event],PATINDEX('%[1-9]&[1-9]YO%',[Event]),3)+'YO')
							 else ''
						End
						+Case when ([Event] like '%[1-9],[1-9]&[1-9]YO%')
							 then '-'+substring([Event],PATINDEX('%[1-9],[1-9]&[1-9]YO%',[Event]),(PATINDEX('%[1-9]YO%',[Event])+1-PATINDEX('%[1-9],[1-9]&[1-9]YO%',[Event])))+'YO'
							 else ''
						End
						+Case when ([Event]  like '%DIV[1-9]%' or [Event]  like '%DIV [1-9]%')
							 then '-'+'Div'+Ltrim(Rtrim(substring([Event],PATINDEX('%DIV_[1-9]%',[Event])+3,2)))
							 else ''
						End
						+Case when ([Event]  like '%Listed%')
							 then '-'+'Listed'
							 else ''
						End
						+Case when (([Event]  like '%Pace%' or [Event]  like '%Pce%') and [Event] not like '%[1-9][0-9]__Pace%')
							 then '-'+'Pace'
							 else ''
						End
						+Case when ([Event]  like '%[1-9][0-9][0-9,][ ,]Pace%')
							 then '-'+Rtrim(substring([Event],PATINDEX('%[1-9][0-9][0-9,][ ,]Pace%',[Event]),8))
							 else ''
						End
		from [$(Dimensional)].Dimension.Event e
		left join (select classKey, SourceClassName from [$(Dimensional)].Dimension.Class) c on e.ClassKey=c.ClassKey						
		where  SourceClassName='Trots' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(5-1))<>Power(2,(5-1)))

		Update e
		set Grade=Replace(Grade,'One',1)
		from [$(Dimensional)].Dimension.Event e
		left join (select classKey, SourceClassName from [$(Dimensional)].Dimension.Class) c on e.ClassKey=c.ClassKey
		where Grade like '%[^a-z,^0-9]One%' and SourceClassName='Trots'
		and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(5-1))<>Power(2,(5-1)))

		Update e
		set Grade=Replace(Grade,'Two',2)
		from [$(Dimensional)].Dimension.Event e
		left join (select classKey, SourceClassName from [$(Dimensional)].Dimension.Class) c on e.ClassKey=c.ClassKey
		where Grade like '%[^a-z,^0-9]Two%' and SourceClassName='Trots'
		and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(5-1))<>Power(2,(5-1)))

		Update e
		set Grade=Replace(Grade,'Three',3)
		from [$(Dimensional)].Dimension.Event e
		left join (select classKey, SourceClassName from [$(Dimensional)].Dimension.Class) c on e.ClassKey=c.ClassKey
		where Grade like '%[^a-z,^0-9]Three%' and SourceClassName='Trots'
		and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey)  and (ColumnLock&Power(2,(5-1))<>Power(2,(5-1)))

		Update e
		set Grade=Replace(Grade,'Four',4)
		from [$(Dimensional)].Dimension.Event e
		left join (select classKey, SourceClassName from [$(Dimensional)].Dimension.Class) c on e.ClassKey=c.ClassKey
		where Grade like '%[^a-z,^0-9]Four%' and SourceClassName='Trots'
		and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(5-1))<>Power(2,(5-1)))

		Update e
		set Grade=Substring(Grade,2,Len(Grade)-1)
		from [$(Dimensional)].Dimension.Event e
		left join (select classKey, SourceClassName from [$(Dimensional)].Dimension.Class) c on e.ClassKey=c.ClassKey
		where Grade<>'' and  SourceClassName='Trots'
		and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(5-1))<>Power(2,(5-1)))

		Update e
		set Grade=Ltrim(Replace(Replace(Grade,'(',''),')',''))
		from [$(Dimensional)].Dimension.Event e
		left join (select classKey, SourceClassName from [$(Dimensional)].Dimension.Class) c on e.ClassKey=c.ClassKey
		where SourceClassName='Trots'
		and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(5-1))<>Power(2,(5-1)))
		 
		Update e
		set Grade='Unknown'
		from [$(Dimensional)].Dimension.Event e
		left join (select classKey, SourceClassName from [$(Dimensional)].Dimension.Class) c on e.ClassKey=c.ClassKey
		where  SourceClassName='Trots' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and Grade='' and (ColumnLock&Power(2,(5-1))<>Power(2,(5-1)))

-- ========================================================================================================================================================================================
  --                        PART 8: Load Round name for racing type events but not 'Racing','Greyhounds','Trots' and Round Sequence for all Event types in to Event Dimension
-- ========================================================================================================================================================================================
EXEC [Control].Sp_Log	@BatchKey,'SP_Event_RacingGhTrots','Part 8','Start',@RowsProcessed = @@RowCount	

Update e
Set Round = CASE WHEN RoundSequence > 0 
					 THEN 'Race ' + CONVERT(VARCHAR,RoundSequence)
					 ELSE 'N/A' 
				END
from [$(Dimensional)].Dimension.Event e
left join (select classKey, SourceClassName from [$(Dimensional)].Dimension.Class) c on e.ClassKey=c.ClassKey
Where SourceClassName NOT IN ('Racing','Greyhounds','Trots') and  
(RoundSequence>0 or SourceClassName IN ('Jockey Challenge','Greys Box Challenge','Inside Or Outside','Feature Races','ODDS vs EVENS','ODD Or EVEN','Runner vs Field'))
and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey)	

EXEC [Control].Sp_Log	@BatchKey,'SP_Event_RacingGhTrots',NULL,'Success'
COMMIT TRANSACTION Event_RacingGhTrots;

-- ================================================================================================================================================================
  --                        PART 9: Load ModifiedBy and ModifiedDate in to Event Dimension for 'Racing','Greyhounds','Trots' Event types
-- ================================================================================================================================================================
EXEC [Control].Sp_Log	@BatchKey,'SP_Event_RacingGhTrots','Part 9','Start',@RowsProcessed = @@RowCount	
Update 	e
set	ModifiedBy='SP_Event_RacingGhTrots',
	ModifiedDate=CURRENT_TIMESTAMP
from [$(Dimensional)].Dimension.Event e
left join (select classKey, SourceClassName from [$(Dimensional)].Dimension.Class) c on e.ClassKey=c.ClassKey
where ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and SourceClassName in ('Racing','Greyhounds','Trots')
EXEC [Control].Sp_Log	@BatchKey,'SP_Event_RacingGhTrots',NULL,'Success',@RowsProcessed = @@RowCount

END TRY

BEGIN CATCH
	Rollback TRANSACTION Event_RacingGhTrots;
	declare @Error int;
	declare @ErrorMessage varchar(max);
	                           -- Log the failure at the object level
                           SET    @Error = ERROR_NUMBER()
                           SET    @ErrorMessage = ERROR_MESSAGE()
						   EXEC [Control].Sp_Log @BatchKey, 'SP_Event_RacingGhTrots', NULL, 'Failed', @ErrorMessage;
						   Raiserror(@ErrorMessage,16,1)
	
END CATCH
END
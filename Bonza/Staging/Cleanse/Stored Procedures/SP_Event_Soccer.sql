﻿-- =============================================
-- Author:		Lekha Narra
-- Create date: 17/11/2014
-- Description:	Insert eventNames,roundNames,CompetitionNames for Soccer event type
-- =============================================
CREATE PROCEDURE [Cleanse].[SP_Event_Soccer] 
	-- Add the parameters for the stored procedure here
	@BatchKey int 
AS
BEGIN
BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  BEGIN TRANSACTION Event_Soccer ;
	
--======================================================================================================================================================================   
--                                                PART 1: Insert Competition Names in to Event Dimension for 'Soccer' event type
--====================================================================================================================================================================== 
EXEC [Control].Sp_Log	@BatchKey,'SP_Event_Soccer','Part 1','Start'
Update [$(Dimensional)].Dimension.Event
set Competition=''
where SourceEventType='Soccer' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1))) 

Update [$(Dimensional)].Dimension.Event
set Competition=(Case 
						When (SourceEvent like '%?'  or isnull(ParentEvent,'') like '%?'  or isnull(GrandParentEvent,'') like '%?')
						Then 'NA'
						When (GrandParentEvent is not null and GrandParentEvent not like 'Live Betting%'and GrandParentEvent<>'BETRADAR DNO')
						Then (Case 
								When (GrandParentEvent like '%Final%'  or GrandParentEvent like '%Round of 16%'
								or GrandParentEvent like '%|%')
								Then (Case 
										When (GrandParentEvent like '%Semi_Final%')
										Then Substring(GrandParentEvent,1,Patindex('%Semi_Final%',GrandParentEvent)-2)
										When (GrandParentEvent like '%Quarter_Final%')
										Then Substring(GrandParentEvent,1,Patindex('%Quarter_Final%',GrandParentEvent)-2)
										When (GrandParentEvent like '%Final%')
										Then Substring(GrandParentEvent,1,Patindex('%Final%',GrandParentEvent)-2)
										When (GrandParentEvent like '%Round of 16%')
										Then Substring(GrandParentEvent,1,Patindex('%Round of 16%',GrandParentEvent)-2)
										When (GrandParentEvent like '%|%')
										Then Substring(GrandParentEvent,1,Patindex('%|%',GrandParentEvent)-2)
										Else 'Check'
									 End)
								Else GrandParentEvent
						     End)
						When ParentEvent is not null 
						Then (Case 
								When (ParentEvent like '% vs %'  or ParentEvent like '% v %')
								Then (Case 
										When (SourceEvent like '% vs %'  or SourceEvent like '% v %')
										Then 'Unknown'
										Else SourceEvent
									 End)
								Else (Case
										When (ParentEvent like '%Matches%' or ParentEvent like '%Play%off%' or ParentEvent like '% Cup%'
										 or ParentEvent like '% Trophy%' or ParentEvent like '% League%' or ParentEvent like '% Liga%')
										Then (Case 
												When (ParentEvent like '%Semi_Final%')
												Then Substring(ParentEvent,1,Patindex('%Semi_Final%',ParentEvent)-2)
												When (ParentEvent like '%Quarter_Final%')
												Then Substring(ParentEvent,1,Patindex('%Quarter_Final%',ParentEvent)-2)
												When (ParentEvent like '%Final%')
												Then Substring(ParentEvent,1,Patindex('%Final%',ParentEvent)-2)
												When (ParentEvent like '%Round of 16%')
												Then Substring(ParentEvent,1,Patindex('%Round of 16%',ParentEvent)-2)
												When (ParentEvent like '%|%')
												Then Substring(ParentEvent,1,Patindex('%|%',ParentEvent)-2)
												Else ParentEvent
											 End)
										Else (Case
												When (SourceEvent like '% vs %'  or SourceEvent like '% v %')
												Then 'Unknown'
												Else (Case 
														When (SourceEvent like '%Semi_Final%')
														Then Substring(SourceEvent,1,Patindex('%Semi_Final%',SourceEvent)-1)
														When (SourceEvent like '%Quarter_Final%')
														Then Substring(SourceEvent,1,Patindex('%Quarter_Final%',SourceEvent)-1)
														When (SourceEvent like '%Final%')
														Then Substring(SourceEvent,1,Patindex('%Final%',SourceEvent)-1)
														When (SourceEvent like '%Round of 16%')
														Then Substring(SourceEvent,1,Patindex('%Round of 16%',SourceEvent)-1)
														When (SourceEvent like '%|%')
														Then Substring(SourceEvent,1,Patindex('%|%',SourceEvent)-1)
														Else SourceEvent
													 End)
											  End)
									  End)
							  End)/**/
						Else(Case
								When (SourceEvent like '% vs %'  or SourceEvent like '% v %' or SourceEvent like '%Team%' or SourceEvent like '%Player %')
								Then 'Unknown'
								Else SourceEvent
								End)
					End)
where SourceEventType='Soccer' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1)))

Update [$(Dimensional)].Dimension.Event
set Competition=(Case 
						when Competition like '%Bet%Radar%'
						Then Substring(Competition,1,PATINDEX('%Bet%Radar%',Competition)-1)
						when Competition like 'To %'
						Then 'Unknown'
						when (Competition like '% To %' or  Competition like '%-To %')
						Then Substring(Competition,1,patINDEX('%To %',Competition)-1)
						when Competition like '%Will /%'
						Then Substring(Competition,1,PATINDEX('%Will /%',Competition)-1)
						when Competition like '%Will Hill%'
						Then Substring(Competition,1,PATINDEX('%Will Hill%',Competition)-1)
						when Competition like '%WH%'
						Then Substring(Competition,1,PATINDEX('%WH%',Competition)-1) 
						when Competition like '%BR %'
						Then Substring(Competition,1,PATINDEX('%BR %',Competition)-1)
						when Competition like '%Do Not%'
						Then Substring(Competition,1,PATINDEX('%Do Not%',Competition)-1)
						when Competition like '%DNO%'
						Then Substring(Competition,1,PATINDEX('%DNO%',Competition)-1)
						when Competition like '%Live Betting%'
						Then Substring(Competition,1,PATINDEX('%Live Betting%',Competition)-1)
						when Competition like '%Normal %'
						Then Substring(Competition,1,PATINDEX('%Normal %',Competition)-1)
						when Competition like '%|%'
						Then Substring(Competition,1,PATINDEX('%|%',Competition)-1)
						Else Competition
					 End)
where SourceEventType='Soccer' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1)))

Update [$(Dimensional)].Dimension.Event
set Competition=Replace(Replace(Replace(Competition,')',''),'(',''),'Midweek','')
where SourceEventType='Soccer' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (Competition like '%-' or Competition like '%- ' 
or Competition like '%(%' or Competition like '%)%') and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1)))

Update [$(Dimensional)].Dimension.Event
set Competition=Replace(Replace(Replace(Competition,'Relegation',''),'Promotion',''),'/','')
where SourceEventType='Soccer' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (Competition like '%Relegation%' or Competition like '%Promotion%' 
or Competition like '%/%' or Competition like '%Winner%') and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1)))

Update [$(Dimensional)].Dimension.Event
set Competition=(Case 
						when Competition=''
						Then 'Unknown'
						when Competition like '%Qualifying %'
						Then Ltrim(Rtrim(Substring(Competition,1,Patindex('%Qualifying%',Competition)-1)))
						when Competition like '%Matches%'
						Then Ltrim(Rtrim(Substring(Competition,1,Patindex('%Matches%',Competition)+6)))
						When Competition like '%Winner -%'
						Then Ltrim(Rtrim(Substring(Competition,Patindex('%- %',Competition)+2,Len(Competition))))
						When Competition like '%Winner%'
						Then Ltrim(Rtrim(Replace(Competition,'Winner','')))
						When Competition like '%[1-2][a-z][a-z] leg%'
						Then Ltrim(Rtrim(SUbstring(Competition,1,Patindex('%[1-2][a-z][a-z] leg%',Competition)-1)+
								SUbstring(Competition,Patindex('%leg%',Competition)+4,Len(Competition))))
						Else Ltrim(Rtrim(Competition))
					 End)
where SourceEventType='Soccer' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1)))

Update [$(Dimensional)].Dimension.Event
set Competition=(Case
						When (Competition like '[1-9][0-9]%')
						Then SUbstring(Competition,Patindex('% %',Competition)+1,Len(Competition))
						When (Competition like '%[1-9][0-9]')
						Then SUbstring(Competition,1,Patindex('%[1-9][0-9]%',Competition)-2)
						When (Competition like '%[1-9][0-9][0-9][0-9]%')
						Then SUbstring(Competition,1,Patindex('%[1-9][0-9][0-9][0-9]%',Competition)-2)+
							SUbstring(Competition,Patindex('%[1-9][0-9][0-9][0-9]%',Competition)+4,Len(Competition))
						Else Competition
					 End)
where SourceEventType='Soccer' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1)))

Update [$(Dimensional)].Dimension.Event
set Competition=SUbstring(Competition,1,Patindex('%[0-9][0-9]%',Competition)-1)+
					SUbstring(Competition,Patindex('%[0-9][0-9]%',Competition)+2,Len(Competition))
where SourceEventType='Soccer' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey)  and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1))) and
Competition like '%[0-9][0-9]%'

Update [$(Dimensional)].Dimension.Event
set Competition=Replace(Replace(Competition,'-',''),'  ',' ')
where SourceEventType='Soccer' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1))) and
Competition like '%-%'

--======================================================================================================================================================================   
--                                                PART 2: Insert Round Names in to Event Dimension for 'Soccer' event type
--====================================================================================================================================================================== 
EXEC [Control].Sp_Log	@BatchKey,'SP_Event_Soccer','Part 2','Start',@RowsProcessed = @@RowCount
Update [$(Dimensional)].Dimension.Event
set round=(Case 
				When (isnull(GrandParentEvent,'') like '%Final%'  or isnull(GrandParentEvent,'') like '%Round of 16%' or isnull(GrandParentEvent,'') like '%[1-2][a-z][a-z] leg%')
				Then (Case 
						When (isnull(GrandParentEvent,'') like '%Semi_Final%' and isnull(GrandParentEvent,'') like '%[1-2][a-z][a-z] leg%' )
						Then 'Semi Final '+Substring(isnull(GrandParentEvent,''),Patindex('%[1-2][a-z][a-z] leg%',isnull(GrandParentEvent,'')),7)
						When (isnull(GrandParentEvent,'') like '%Semi_Final%' )
						Then 'Semi Final'
						When (isnull(GrandParentEvent,'') like '%Quarter_Final%' and isnull(GrandParentEvent,'') like '%[1-2][a-z][a-z] leg%' )
						Then 'Quarter Final '+Substring(isnull(GrandParentEvent,''),Patindex('%[1-2][a-z][a-z] leg%',isnull(GrandParentEvent,'')),7)
						When (isnull(GrandParentEvent,'') like '%Quarter_Final%' )
						Then 'Quarter Final'
						When (isnull(GrandParentEvent,'') like '%Final%' and isnull(GrandParentEvent,'') like '%[1-2][a-z][a-z] leg%' )
						Then 'Final '+Substring(isnull(GrandParentEvent,''),Patindex('%[1-2][a-z][a-z] leg%',isnull(GrandParentEvent,'')),7)
						When (isnull(GrandParentEvent,'') like '%Final%')
						Then 'Final'
						When (isnull(GrandParentEvent,'') like '%Round of 16%' and isnull(GrandParentEvent,'') like '%[1-2][a-z][a-z] leg%' )
						Then 'Round of 16 '+Substring(isnull(GrandParentEvent,''),Patindex('%[1-2][a-z][a-z] leg%',isnull(GrandParentEvent,'')),7)
						When (isnull(GrandParentEvent,'') like '%Round of 16%')
						Then 'Round of 16'
						When (isnull(GrandParentEvent,'') like '%[1-2][a-z][a-z] leg%')
						Then Substring(isnull(GrandParentEvent,''),Patindex('%[1-2][a-z][a-z] leg%',isnull(GrandParentEvent,'')),7)
						
						Else 'Check'
						End)
				When (isnull(ParentEvent,'') like '%Final%' or isnull(ParentEvent,'') like '%Round of 16%'or isnull(ParentEvent,'') like '%[1-2][a-z][a-z] leg%')
				Then (Case 
						When (isnull(ParentEvent,'') like '%Semi_Final%' and isnull(ParentEvent,'') like '%[1-2][a-z][a-z] leg%' )
						Then 'Semi Final '+Substring(isnull(ParentEvent,''),Patindex('%[1-2][a-z][a-z] leg%',isnull(ParentEvent,'')),7)
						When (isnull(ParentEvent,'') like '%Semi_Final%')
						Then 'Semi Final'
						When (isnull(ParentEvent,'') like '%Quarter_Final%' and isnull(ParentEvent,'') like '%[1-2][a-z][a-z] leg%' )
						Then 'Quarter Final '+Substring(isnull(ParentEvent,''),Patindex('%[1-2][a-z][a-z] leg%',isnull(ParentEvent,'')),7)
						When (isnull(ParentEvent,'') like '%Quarter_Final%')
						Then 'Quarter Final'
						When (isnull(ParentEvent,'') like '%Final%' and isnull(ParentEvent,'') like '%[1-2][a-z][a-z] leg%' )
						Then 'Final '+Substring(isnull(ParentEvent,''),Patindex('%[1-2][a-z][a-z] leg%',isnull(ParentEvent,'')),7)
						When (isnull(ParentEvent,'') like '%Final%')
						Then 'Final'
						When (isnull(ParentEvent,'') like '%Round of 16%' and isnull(ParentEvent,'') like '%[1-2][a-z][a-z] leg%' )
						Then 'Round of 16 '+Substring(isnull(ParentEvent,''),Patindex('%[1-2][a-z][a-z] leg%',isnull(ParentEvent,'')),7)
						When (isnull(ParentEvent,'') like '%Round of 16%')
						Then 'Round of 16'
						When isnull(ParentEvent,'') like '%[1-2][a-z][a-z] leg%'
						Then Substring(isnull(ParentEvent,''),Patindex('%[1-2][a-z][a-z] leg%',isnull(ParentEvent,'')),7)
						
						Else 'Check'
						End)
				When (SourceEvent like '%Final%' or SourceEvent like '%Round of 16%'or SourceEvent like '%[1-2][a-z][a-z] leg%')
				Then (Case 
						When (SourceEvent like '%Semi_Final%' and SourceEvent like '%[1-2][a-z][a-z] leg%' )
						Then 'Semi Final '+Substring(SourceEvent,Patindex('%[1-2][a-z][a-z] leg%',SourceEvent),7)
						When (SourceEvent like '%Semi_Final%')
						Then 'Semi Final'
						When (SourceEvent like '%Quarter_Final%' and SourceEvent like '%[1-2][a-z][a-z] leg%' )
						Then 'Quarter Final '+Substring(SourceEvent,Patindex('%[1-2][a-z][a-z] leg%',SourceEvent),7)
						When (SourceEvent like '%Quarter_Final%')
						Then 'Quarter Final'
						When (SourceEvent like '%Final%' and SourceEvent like '%[1-2][a-z][a-z] leg%' )
						Then 'Final '+Substring(SourceEvent,Patindex('%[1-2][a-z][a-z] leg%',SourceEvent),7)
						When (SourceEvent like '%Final%')
						Then 'Final'
						When (SourceEvent like '%Round of 16%' and SourceEvent like '%[1-2][a-z][a-z] leg%' )
						Then 'Round of 16 '+Substring(SourceEvent,Patindex('%[1-2][a-z][a-z] leg%',SourceEvent),7)
						When (SourceEvent like '%Round of 16%')
						Then 'Round of 16'
						When SourceEvent like '%[1-2][a-z][a-z] leg%'
						Then Substring(SourceEvent,Patindex('%[1-2][a-z][a-z] leg%',SourceEvent),7)
						
						Else 'Check'
						End)
				Else 'NA'
				End)
where SourceEventType='Soccer' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(10-1))<>Power(2,(10-1)))

Update [$(Dimensional)].Dimension.Event
set round=(Case 
					When isnull(GrandParentEvent,'') like '%Qualifying Group [a-z]%' and Round<>'NA'
					Then Round +' '+Substring(isnull(GrandParentEvent,''),Patindex('%Qualifying Group [a-z]%',isnull(GrandParentEvent,'')),18)
					When isnull(GrandParentEvent,'') like '%Qualifying Group [a-z]%' and Round='NA'
					Then Substring(isnull(GrandParentEvent,''),Patindex('%Qualifying Group [a-z]%',isnull(GrandParentEvent,'')),18)
					When isnull(ParentEvent,'') like '%Qualifying Group [a-z]%' and Round='NA'
					Then Substring(isnull(ParentEvent,''),Patindex('%Qualifying Group [a-z]%',isnull(ParentEvent,'')),18)
					When isnull(ParentEvent,'') like '%Qualifying Group [a-z]%' and Round<>'NA'
					Then Round +' '+Substring(isnull(ParentEvent,''),Patindex('%Qualifying Group [a-z]%',isnull(ParentEvent,'')),18)
					When SourceEvent like '%Qualifying Group [a-z]%' and Round<>'NA'
					Then Round +' '+Substring(SourceEvent,Patindex('%Qualifying Group [a-z]%',SourceEvent),18)
					When SourceEvent like '%Qualifying Group [a-z]%' and Round='NA'
					Then Substring(SourceEvent,Patindex('%Qualifying Group [a-z]%',SourceEvent),18)
					Else Round
				End)
where SourceEventType='Soccer' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(10-1))<>Power(2,(10-1)))

Update [$(Dimensional)].Dimension.Event
set round=(Case 
					When (((isnull(GrandParentEvent,'') like '%Promotion%' and isnull(GrandParentEvent,'') like '%Relegation%') or
						(isnull(ParentEvent,'') like '%Promotion%' and isnull(ParentEvent,'') like '%Relegation%') or
						(SourceEvent like '%Promotion%' and SourceEvent like '%Relegation%')) and Round<>'NA')
					Then Round+' Promotion/Relegation'
					When (((isnull(GrandParentEvent,'') like '%Promotion%' and isnull(GrandParentEvent,'') like '%Relegation%') or
						(isnull(ParentEvent,'') like '%Promotion%' and isnull(ParentEvent,'') like '%Relegation%') or
						(SourceEvent like '%Promotion%' and SourceEvent like '%Relegation%')) and Round='NA')
					Then 'Promotion/Relegation'
					When ((isnull(GrandParentEvent,'') like '%Promotion%' or isnull(ParentEvent,'') like '%Promotion%' or SourceEvent like '%Promotion%')
							and Round<>'NA')
					Then Round+' Promotion'
					When ((isnull(GrandParentEvent,'') like '%Promotion%' or isnull(ParentEvent,'') like '%Promotion%' or SourceEvent like '%Promotion%')
							and Round='NA')
					Then 'Promotion'
					When ((isnull(GrandParentEvent,'') like '%Relegation%' or isnull(ParentEvent,'') like '%Relegation%' or SourceEvent like '%Relegation%')
							and Round<>'NA')
					Then Round+' Relegation'
					When ((isnull(GrandParentEvent,'') like '%Relegation%' or isnull(ParentEvent,'') like '%Relegation%' or SourceEvent like '%Relegation%')
							and Round='NA')
					Then 'Relegation'
					Else Round
				End)
where SourceEventType='Soccer' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(10-1))<>Power(2,(10-1)))

--======================================================================================================================================================================   
--                                                PART 3: Insert Event Names in to Event Dimension for 'Soccer' event type
--====================================================================================================================================================================== 
EXEC [Control].Sp_Log	@BatchKey,'SP_Event_Soccer','Part 3','Start',@RowsProcessed = @@RowCount
Update [$(Dimensional)].Dimension.Event
set Event=(Case		
					WHEN (isnull(ParentEvent,'') like '% vs %' or isnull(ParentEvent,'') like '% v %' or isnull(ParentEvent,'') like '%@%' )
					THEN 
						(CASE 
							WHEN isnull(ParentEvent,'') like'% - %'			
							THEN 
								(CASE
									WHEN ((SUBString(isnull(ParentEvent,''),1, CHARINDEX(' - ',isnull(ParentEvent,''))) like '% vs %') or
										(SUBString(isnull(ParentEvent,''),1, CHARINDEX(' - ',isnull(ParentEvent,''))) like '% v %') or
										(SUBString(isnull(ParentEvent,''),1, CHARINDEX(' - ',isnull(ParentEvent,''))) like '%@%') )
									THEN SUBString(isnull(ParentEvent,''),1, CHARINDEX(' - ',isnull(ParentEvent,''))-1)
									ELSE SUBString(isnull(ParentEvent,''), CHARINDEX(' - ',isnull(ParentEvent,''))+3,LEN(isnull(ParentEvent,'')))
								END)
							ELSE isnull(ParentEvent,'')
						END)
						ELSE
						(CASE
							WHEN (SourceEvent like '% vs %' or SourceEvent like '% v %' or SourceEvent like '%@%' )
							THEN 
								(CASE 
									WHEN SourceEvent like'% - %'			
									THEN 
										(CASE
											WHEN ((SUBString(SourceEvent,1, CHARINDEX(' - ',SourceEvent)) like '% vs %') or
												(SUBString(SourceEvent,1, CHARINDEX(' - ',SourceEvent)) like '% v %') or
												(SUBString(SourceEvent,1, CHARINDEX(' - ',SourceEvent)) like '%@%') )
											THEN SUBString(SourceEvent,1, CHARINDEX(' - ',SourceEvent)-1)
											ELSE SUBString(SourceEvent, CHARINDEX(' - ',SourceEvent)+3,LEN(SourceEvent))
										END)
									ELSE SourceEvent
									END)
								ELSE 'NA'
							End)
						ENd)
where SourceEventType='Soccer' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(2-1))<>Power(2,(2-1)))

COMMIT TRANSACTION Event_Soccer;

--======================================================================================================================================================================   
--                                                PART 4: Insert Modification details in to Event Dimension for 'Soccer' event type
--====================================================================================================================================================================== 
EXEC [Control].Sp_Log	@BatchKey,'SP_Event_Soccer','Part 4','Start',@RowsProcessed = @@RowCount
Update 	[$(Dimensional)].Dimension.Event
set	ModifiedBy='SP_Event_Soccer',
	ModifiedDate=CURRENT_TIMESTAMP
where ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and SourceEventType='Soccer'
EXEC [Control].Sp_Log	@BatchKey,'SP_Event_Soccer',NULL,'Success',@RowsProcessed = @@RowCount

END TRY
BEGIN CATCH
	Rollback TRANSACTION Event_Soccer;
	declare @Error int;
	declare @ErrorMessage varchar(max);
	                           -- Log the failure at the object level
                           SET    @Error = ERROR_NUMBER()
                           SET    @ErrorMessage = ERROR_MESSAGE()
						   EXEC [Control].Sp_Log @BatchKey, 'SP_Event_Soccer', NULL, 'Failed', @ErrorMessage;
						   Raiserror(@ErrorMessage,16,1)

END CATCH

END

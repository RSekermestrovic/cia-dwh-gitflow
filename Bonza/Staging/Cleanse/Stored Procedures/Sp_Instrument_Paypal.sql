CREATE proc [Cleanse].[Sp_Instrument_Paypal]
(
	@BatchKey int = 0,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @RowCount INT;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

BEGIN TRY

	EXEC [Control].Sp_Log @BatchKey, 'Cleanse.Sp_Instrument_Paypal', 'Cache', 'Start'

	SELECT	InstrumentKey,
			AccountNumber,
			SafeAccountNumber,
			AccountName,
			BSB,
			ProviderName,
			ProviderKey
			
	INTO	#Instrument
	FROM	[$(Dimensional)].Dimension.Instrument 
	WHERE ModifiedBatchKey = CASE @BatchKey WHEN 0 THEN ModifiedBatchKey ELSE @BatchKey END
	AND Source ='PPL'



	EXEC [Control].Sp_Log @BatchKey, 'Cleanse.Sp_Instrument_Paypal', 'Account Number', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	u
	SET		AccountNumber = UpdatedAccountNumber
	FROM	(	SELECT	AccountNumber,
						CASE ISNULL(AccountNumber,'')
							WHEN '' THEN ''
							WHEN ' ' THEN ''
							WHEN 'N/A' THEN ''
							WHEN 'Unknown' THEN ''
							ELSE Replace(AccountNumber,' ','')
						END UpdatedAccountNumber
				FROM	#Instrument
			) u
	WHERE	ISNULL(AccountNumber,'!') <> UpdatedAccountNumber




	EXEC [Control].Sp_Log @BatchKey, 'Cleanse.Sp_Instrument_Paypal', 'Safe Account Number', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	u
	SET		SafeAccountNumber = UpdatedSafeAccountNumber
	FROM	(	SELECT	SafeAccountNumber,
						CASE ISNULL(SafeAccountNumber,'')
							WHEN '' THEN ''
							WHEN ' ' THEN ''
							WHEN 'N/A' THEN ''
							WHEN 'Unknown' THEN ''
							ELSE Replace(AccountNumber,' ','')
						END UpdatedSafeAccountNumber
				FROM	#Instrument
			) u
	WHERE	ISNULL(SafeAccountNumber,'!') <> UpdatedSafeAccountNumber




	EXEC [Control].Sp_Log @BatchKey, 'Cleanse.Sp_Instrument_Paypal', 'Account Name', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	u
	SET		AccountName= UpdatedAccountName
	FROM	(	SELECT	AccountName,
						CASE  REPLACE(AccountName,' ','')
							 WHEN '%nomail%' THEN 'N/A'
							 WHEN '%_@_%_.__%' THEN  LOWER(REPLACE(AccountName,' ',''))
							 ELSE 'Unknown'
						END UpdatedAccountName
				FROM	#Instrument
			) u
	WHERE	CONVERT(varbinary,ISNULL(AccountName,'!')) <> CONVERT(varbinary,ISNULL(UpdatedAccountName,'!'))

    EXEC [Control].Sp_Log @BatchKey, 'Cleanse.Sp_Instrument_Paypal', 'Update', 'Start', @RowsProcessed = @@ROWCOUNT


	UPDATE	i
	SET		AccountNumber = u.AccountNumber,
			SafeAccountNumber = u.SafeAccountNumber,
			AccountName = u.AccountName

	FROM	[$(Dimensional)].Dimension.Instrument i
			INNER JOIN #Instrument u ON u.InstrumentKey = i.InstrumentKey
	WHERE	ISNULL(i.AccountNumber,'!') <> ISNULL(u.AccountNumber,'!')
			OR ISNULL(i.SafeAccountNumber,'!') <> ISNULL(u.SafeAccountNumber,'!')
			OR ISNULL(i.AccountName,'!') <> ISNULL(u.AccountName,'!')

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey, 'Cleanse.Sp_Instrument_Paypal', NULL, 'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Cleanse.Sp_Instrument_Paypal', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

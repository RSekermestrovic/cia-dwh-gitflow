﻿CREATE proc [Cleanse].[SP_Account_Organisation]
(
	@BatchKey int = 0,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @RowCount INT;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

BEGIN TRY

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Account_Organisation', 'Copy From Structure', 'Start';

	UPDATE	a SET	a.BDM = s.BDM,
					a.VIPCode = s.VIPCode,
					a.VIP = s.VIP,
					a.CompanyKey = ISNULL(c.CompanyKey,-1)
	FROM	[$(Dimensional)].Dimension.Account a
			INNER JOIN [$(Dimensional)].Dimension.Structure s ON (s.AccountId = a.AccountId AND s.InPlay = 'N')
			LEFT JOIN [$(Dimensional)].Dimension.Company c ON (c.OrgId = s.OrgId AND c.DivisionId = CASE WHEN s.OrgId = 3 THEN 100 WHEN s.BDM <> 'N/A' THEN 2 ELSE 1 END)
	WHERE	(	a.ModifiedBatchKey = CASE @BatchKey WHEN 0 THEN a.ModifiedBatchKey ELSE @BatchKey END 
				OR s.ModifiedBatchKey = CASE @BatchKey WHEN 0 THEN s.ModifiedBatchKey ELSE @BatchKey END
			)
			AND (	ISNULL(a.BDM, '!') <> s.BDM 
					OR ISNULL(a.VIPCode, -1) <> s.VIPCode
					OR ISNULL(a.VIP, '!') <> s.VIP
					OR a.CompanyKey <> ISNULL(c.CompanyKey,-1)
				)

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Account_Organisation', NULL, 'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Account_Organisation', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

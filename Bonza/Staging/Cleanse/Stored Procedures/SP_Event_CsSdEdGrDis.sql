-- =============================================
-- Author:		Lekha Narra
-- Create date: 11/11/2014
-- Description:	Insert Competition season, start and end dates for 'Racing','greyhounds','trots','Australian Rules','Rugby league' 
-- and Grade and Distance for all events other than racing type events
-- =============================================
CREATE PROCEDURE [Cleanse].[SP_Event_CsSdEdGrDis] 
	-- Add the parameters for the stored procedure here
	@BatchKey int
AS
BEGIN
BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

BEGIN TRANSACTION Event_CsSdEdGrDis;

-- ================================================================================================================================================================
  --                        PART 1: Update Competition Season in Event Dimension for all event types
-- ================================================================================================================================================================
EXEC [Control].Sp_Log	@BatchKey,'SP_Event_CsSdEdGrDis','Part 1','Start'
		Update [$(Dimensional)].Dimension.Event
		set CompetitionSeason=(case
								 when (Competition='Test' or Competition='NA')
								 Then 'NA'
								 When Competition='Unknown'
								 THen 'Unknown'
								 Else Cast(isnull(DATEPART(yyyy,SourceEventDate),'') as varchar(32))
							   End)
		where Competition is not null and  ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and 
		(Competition not like '%Ashes%' and Competition not like '%English Premier League%'
		and Competition not like '%EPL%' and Competition not like '%SPL%' and Competition not like '%Scottish Premier League%')
		and (ColumnLock&Power(2,(13-1))<>Power(2,(13-1)))
		OPTION (RECOMPILE);

		Update e
		set CompetitionSeason=(case
								 when (Competition like '%Ashes%' and DATEPART(MM,SourceEventDate)=1)
								 Then Cast(isnull((DATEPART(yyyy,SourceEventDate)-1),'') as varchar(32))+'-'+ Cast(isnull(DATEPART(yyyy,SourceEventDate),'') as varchar(32))
								 When (Competition like '%Ashes%' and DATEPART(MM,SourceEventDate)>=11) 
								 Then Cast(isnull((DATEPART(yyyy,SourceEventDate)),'') as varchar(32))+'-'+ Cast(isnull(DATEPART(yyyy,SourceEventDate)+1,'') as varchar(32))
								 when ((Competition like '%English Premier League%' or Competition like '%Scottish Premier League%'
								 or Competition like '%EPL%' or Competition like '%SPL%') and DATEPART(MM,SourceEventDate)<=5)
								 Then Cast(isnull((DATEPART(yyyy,SourceEventDate)-1),'') as varchar(32))+'-'+ Cast(isnull(DATEPART(yyyy,SourceEventDate),'') as varchar(32))
								 When ((Competition like '%English Premier League%' or Competition like '%Scottish Premier League%'
								 or Competition like '%EPL%' or Competition like '%SPL%') and DATEPART(MM,SourceEventDate)>=8) 
								 Then Cast(isnull((DATEPART(yyyy,SourceEventDate)),'') as varchar(32))+'-'+ Cast(isnull(DATEPART(yyyy,SourceEventDate)+1,'') as varchar(32))
								Else 'Check'
							   End)
		from [$(Dimensional)].Dimension.Event e
		left join [$(Dimensional)].Dimension.Class c on e.ClassKey=c.ClassKey
		where SourceClassName in ('Soccer','Cricket') and (Competition like '%Ashes%' or Competition like '%English Premier League%' or Competition like '%EPL%'
															or Competition like '%Scottish Premier League%' or Competition like '%SPL%')
															 and (ColumnLock&Power(2,(13-1))<>Power(2,(13-1)))
		and  e.ModifiedBatchKey=IIF(@BatchKey=0,e.ModifiedBatchKey,@BatchKey)
		OPTION (RECOMPILE);
		
		--Removing combination of years from competition name and inserting them in to competition season
		Update e
		set CompetitionSeason=(Case
									When Competition like '%2[0-9][0-9][0-9]/[0-9][0-9][0-9][0-9]%'
									Then Substring(Competition,patindex('%2[0-9][0-9][0-9]/[0-9][0-9]%',Competition),4)+'-'+
									Substring(Competition,patindex('%2[0-9][0-9][0-9]/[0-9][0-9]%',Competition)+5,4)

									When Competition like '%2[0-9][0-9][0-9]-[0-9][0-9][0-9][0-9]%'
									Then Substring(Competition,patindex('%2[0-9][0-9][0-9]-[0-9][0-9][0-9][0-9]%',Competition),4)+'-'+
									Substring(Competition,patindex('%2[0-9][0-9][0-9]-[0-9][0-9][0-9][0-9]%',Competition)+5,4)

									When Competition like '%2[0-9][0-9][0-9]/[0-9][0-9][^0-9]%'
									Then Substring(Competition,patindex('%2[0-9][0-9][0-9]/[0-9][0-9]%',Competition),4)+'-20'+
									Substring(Competition,patindex('%2[0-9][0-9][0-9]/[0-9][0-9]%',Competition)+5,2)

									When Competition like '%2[0-9][0-9][0-9] /[0-9][0-9][^0-9]%'
									Then Substring(Competition,patindex('%2[0-9][0-9][0-9] /[0-9][0-9]%',Competition),4)+'-20'+
									Substring(Competition,patindex('%2[0-9][0-9][0-9] /[0-9][0-9]%',Competition)+6,2)	

									When Competition like '%2[0-9][0-9][0-9]/[0-9][^0-9]%'
									Then Substring(Competition,patindex('%2[0-9][0-9][0-9]/[0-9]%',Competition),4)+'-200'+
									Substring(Competition,patindex('%2[0-9][0-9][0-9]/[0-9]%',Competition)+5,1)	

									When Competition like '%2[0-9][0-9][0-9]-[0-9][0-9][^0-9]%'
									Then Substring(Competition,patindex('%2[0-9][0-9][0-9]-[0-9][0-9]%',Competition),4)+'-20'+
									Substring(Competition,patindex('%2[0-9][0-9][0-9]-[0-9][0-9]%',Competition)+5,2)	

									Else Cast(isnull(DATEPART(yyyy,SourceEventDate),'') as varchar(32))
								  End)	 
		from [$(Dimensional)].Dimension.Event e
		left join [$(Dimensional)].Dimension.Class c on e.ClassKey=c.ClassKey
		where SourceClassName in ('Cricket','Baseball','Tennis','Rugby Union','Basketball - US','Basketball - World','Basketball - Europe','Basketball - AUS','American Football')
		 and Competition like '%2[0-9][0-9][0-9]%[/,-][0-9]%' and (ColumnLock&Power(2,(13-1))<>Power(2,(13-1)))	 and  e.ModifiedBatchKey=IIF(@BatchKey=0,e.ModifiedBatchKey,@BatchKey)
		 OPTION (RECOMPILE);

		Update e
		set  Competition=(Case
									When (Competition like '%2[0-9][0-9][0-9]/[0-9][0-9][0-9][0-9]%' or Competition like '%2[0-9][0-9][0-9]/[0-9][0-9][0-9][0-9]')
									Then Substring(Competition,1,patindex('%2[0-9][0-9][0-9]/[0-9][0-9]%',Competition)-1)+
										Substring(Competition,patindex('%2[0-9][0-9][0-9]/[0-9][0-9]%',Competition)+9,Len(Competition))

									When (Competition like '%2[0-9][0-9][0-9]/[0-9][0-9][^0-9]%' or Competition like '%2[0-9][0-9][0-9]/[0-9][0-9]')
									Then Substring(Competition,1,patindex('%2[0-9][0-9][0-9]/[0-9][0-9]%',Competition)-1)+
										Substring(Competition,patindex('%2[0-9][0-9][0-9]/[0-9][0-9]%',Competition)+7,Len(Competition))

									When Competition like '%2[0-9][0-9][0-9] /[0-9][0-9][^0-9]%' or Competition like '%2[0-9][0-9][0-9] /[0-9][0-9]'
									Then Substring(Competition,1,patindex('%2[0-9][0-9][0-9] /[0-9][0-9]%',Competition)-1)+
										Substring(Competition,patindex('%2[0-9][0-9][0-9] /[0-9][0-9]%',Competition)+8,Len(Competition))

									When Competition like '%2[0-9][0-9][0-9]-[0-9][0-9][^0-9]%' or Competition like '%2[0-9][0-9][0-9]-[0-9][0-9]'
									Then Substring(Competition,1,patindex('%2[0-9][0-9][0-9]-[0-9][0-9]%',Competition)-1)+
										Substring(Competition,patindex('%2[0-9][0-9][0-9]-[0-9][0-9]%',Competition)+7,Len(Competition))

									When Competition like '%2[0-9][0-9][0-9]/[0-9][^0-9]%' or Competition like '%2[0-9][0-9][0-9]/[0-9]'
									Then Substring(Competition,1,patindex('%2[0-9][0-9][0-9]/[0-9]%',Competition)-1)+
										Substring(Competition,patindex('%2[0-9][0-9][0-9]/[0-9]%',Competition)+6,Len(Competition))
									Else Competition
								End)
		from [$(Dimensional)].Dimension.Event e
		left join [$(Dimensional)].Dimension.Class c on e.ClassKey=c.ClassKey
		where SourceClassName in ('Cricket','Baseball','Tennis','Rugby Union','Basketball - US','Basketball - World','Basketball - Europe','Basketball - AUS','American Football')
		 and Competition like '%2[0-9][0-9][0-9]%[/,-][0-9]%' and e.ModifiedBatchKey=IIF(@BatchKey=0,e.ModifiedBatchKey,@BatchKey)
		 and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1)))
		 OPTION (RECOMPILE);

-- ================================================================================================================================================================
  --                        PART 2: Update Start and End dates in Event Dimension for all event types
-- ================================================================================================================================================================
EXEC [Control].Sp_Log	@BatchKey,'SP_Event_CsSdEdGrDis','Part 2','Start',@RowsProcessed = @@RowCount;

	With startDates(Competition,CompetitionSeason,startDates)
		As(
		select Competition,CompetitionSeason,Convert(date,min(SourceEventDate))
		from [$(Dimensional)].Dimension.Event
		where Competition is not null 
		and Competition not in ('NA','Test','Unknown')
		and  ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey)
		group by Competition,CompetitionSeason)

		Update [$(Dimensional)].Dimension.Event
		set CompetitionStartDate=(case
							when (a.Competition='Test' or a.Competition='NA')
							Then 'NA'
							When a.Competition='Unknown'
							THen 'Unknown'
							Else b.startdates
						End)
		from [$(Dimensional)].Dimension.Event a
		inner hash join startDates b
		on a.Competition=b.Competition and a.CompetitionSeason=b.CompetitionSeason
		where a.Competition is not null 
		and  a.ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey)  and (ColumnLock&Power(2,(14-1))<>Power(2,(14-1)))
		OPTION (RECOMPILE);

		With EndDates(Competition,CompetitionSeason,EndDates)
		As(
		select Competition,CompetitionSeason,Convert(date,max(sourceeventdate))
		from [$(Dimensional)].Dimension.Event
		where Competition is not null 
		and Competition not in ('NA','Test','Unknown')
		and  ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) 
		group by Competition,CompetitionSeason)

		Update [$(Dimensional)].Dimension.Event
		set CompetitionEndDate=(case
							when (a.Competition='Test' or a.Competition='NA')
							Then 'NA'
							When a.Competition='Unknown'
							THen 'Unknown'
							Else b.Enddates
						End)
		from [$(Dimensional)].Dimension.Event a
		inner hash join EndDates b
		on a.Competition=b.Competition and a.CompetitionSeason=b.CompetitionSeason
		where a.Competition is not null 
		and  a.ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(15-1))<>Power(2,(15-1)))
		OPTION (RECOMPILE);

-- ================================================================================================================================================================
  --                        PART 3: Update Grade in Event Dimension for all event types other than 'Racing','Greyhounds','trots'
-- ================================================================================================================================================================
EXEC [Control].Sp_Log	@BatchKey,'SP_Event_CsSdEdGrDis','Part 3','Start',@RowsProcessed = @@RowCount
		update e
		set grade='NA'
		from [$(Dimensional)].Dimension.Event e
		left join [$(Dimensional)].Dimension.Class c on e.ClassKey=c.ClassKey
		where SourceClassName not in ('Racing','Greyhounds','trots')
		and  e.ModifiedBatchKey=IIF(@BatchKey=0,e.ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(5-1))<>Power(2,(5-1)))
		OPTION (RECOMPILE);

-- ================================================================================================================================================================
  --                        PART 4: Update Distance in Event Dimension for all event types other than 'Racing','Greyhounds','trots'
-- ================================================================================================================================================================
EXEC [Control].Sp_Log	@BatchKey,'SP_Event_CsSdEdGrDis','Part 4','Start',@RowsProcessed = @@RowCount
		update e
		set Distance='NA'
		from [$(Dimensional)].Dimension.Event e
		left join [$(Dimensional)].Dimension.Class c on e.ClassKey=c.ClassKey
		where SourceClassName not in ('Racing','Greyhounds','trots')
		and  e.ModifiedBatchKey=IIF(@BatchKey=0,e.ModifiedBatchKey,@BatchKey)  and (ColumnLock&Power(2,(6-1))<>Power(2,(6-1)))
		OPTION (RECOMPILE);

COMMIT TRANSACTION Event_CsSdEdGrDis;

-- ================================================================================================================================================================
  --                        PART 5: Update Modified By and ModifiedDate in Event Dimension for all event types 
-- ================================================================================================================================================================
EXEC [Control].Sp_Log	@BatchKey,'SP_Event_CsSdEdGrDis','Part 5','Start',@RowsProcessed = @@RowCount
Update e
set ModifiedBy='SP_Event_CsSdEdGrDis',
	ModifiedDate=CURRENT_TIMESTAMP
from [$(Dimensional)].Dimension.Event e
left join [$(Dimensional)].Dimension.Class c on e.ClassKey=c.ClassKey
where e.ModifiedBatchKey=IIF(@BatchKey=0,e.ModifiedBatchKey,@BatchKey) and SourceClassName in ('Australian Rules','Rugby League','Racing','Trots','Greyhounds','Tennis',
		'Basketball - US','Basketball - World','Basketball - Aus','Basketball - Europe','Soccer','Baseball','Cricket','American Football','Rugby Union','Golf')
OPTION (RECOMPILE);

EXEC [Control].Sp_Log	@BatchKey,'SP_Event_CsSdEdGrDis',NULL,'Success',@RowsProcessed = @@RowCount

END TRY
BEGIN CATCH
	Rollback TRANSACTION Event_CsSdEdGrDis;
	declare @Error int;
	declare @ErrorMessage varchar(max);
	                           -- Log the failure at the object level
                           SET    @Error = ERROR_NUMBER()
                           SET    @ErrorMessage = ERROR_MESSAGE()
						   EXEC [Control].Sp_Log @BatchKey, 'SP_Event_CsSdEdGrDis', NULL, 'Failed', @ErrorMessage;
						   Raiserror(@ErrorMessage,16,1)

END CATCH
			
END

﻿CREATE proc [Cleanse].[Sp_Account_Ledger]
(
	@BatchKey int = 0,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @RowCount INT;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

BEGIN TRY


	EXEC [Control].Sp_Log @BatchKey, 'Sp_Account_Ledger', 'Update', 'Start';

	UPDATE	u
	SET		LedgerGrouping = u.UpdatedLedgerGrouping
	FROM	(	SELECT	LedgerGrouping, 
						CASE WHEN LedgerTypeID IN (4,5,6,7,8) THEN 'Financial' WHEN LedgerTypeID IS NOT NULL THEN 'Transactional' ELSE 'Unknown' END UpdatedLedgerGrouping
				FROM	[$(Dimensional)].Dimension.Account
				WHERE	ModifiedBatchKey = CASE @BatchKey WHEN 0 THEN ModifiedBatchKey ELSE @BatchKey END
			) u
	WHERE	ISNULL(LedgerGrouping,'!') <> ISNULL(UpdatedLedgerGrouping,'!')

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Account_Ledger', NULL, 'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Account_Ledger', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

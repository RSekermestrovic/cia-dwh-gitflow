﻿-- =============================================
-- Author:		Lekha Narra
-- Create date: 20/11/2014
-- Description:	Insert eventNames,roundNames,CompetitionNames for Rugby Union event type
-- =============================================
CREATE PROCEDURE [Cleanse].[SP_Event_RugbyUnion]
	-- Add the parameters for the stored procedure here
	@BatchKey int 
AS
BEGIN
BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  BEGIN TRANSACTION Event_RugbyUnion ;
	
--======================================================================================================================================================================   
--                                                PART 1: Insert Competition Names in to Event Dimension for 'Rugby Union' event type
--====================================================================================================================================================================== 
EXEC [Control].Sp_Log	@BatchKey,'SP_Event_RugbyUnion','Part 1','Start'
Update [$(Dimensional)].Dimension.Event
set Competition=(Case 
						When (SourceEvent like '%Team[1-9]%' or isnull(ParentEvent,'') like '%Team[1-9]%'or isnull(GrandParentEvent,'') like '%Team[1-9]%'
						or SourceEvent like '%Team [a-z] %' or isnull(ParentEvent,'') like '%Team [a-z] %'or isnull(GrandParentEvent,'') like '%Team [a-z] %'
						or SourceEvent like '%Team [a-z]' or isnull(ParentEvent,'') like '%Team [a-z]'or isnull(GrandParentEvent,'') like '%Team [a-z]'
						or SourceEvent like '%Template%' or isnull(ParentEvent,'') like '%Template%'or isnull(GrandParentEvent,'') like '%Template%'
						or SourceEvent like '%abandoned%' or isnull(ParentEvent,'') like '%abandoned%'or isnull(GrandParentEvent,'') like '%abandoned%'
						or SourceEvent like '%deleted%' or isnull(ParentEvent,'') like '%deleted%'or isnull(GrandParentEvent,'') like '%deleted%'
						or SourceEvent like '%Dump%' or isnull(ParentEvent,'') like '%Dump%'or isnull(GrandParentEvent,'') like '%Dump%'
						or SourceEvent like '%[^a-z,^0-9][a,f] v [b,g][^a-z,^0-9]%' or isnull(ParentEvent,'') like '%[^a-z,^0-9][a,f] v [b,g][^a-z,^0-9]%'
						or isnull(GrandParentEvent,'') like '%[^a-z,^0-9][a,f] v [b,g][^a-z,^0-9]%'
						or SourceEvent like '%[^a-z,^0-9][a,f] vs [b,g][^a-z,^0-9]%' or isnull(ParentEvent,'') like '%[^a-z,^0-9][a,f] vs [b,g][^a-z,^0-9]%'
						or isnull(GrandParentEvent,'') like '%[^a-z,^0-9][a,f] vs [b,g][^a-z,^0-9]%')
						Then 'NA'
						When (GrandParentEvent is not null and GrandParentEvent<>'Live Betting' and GrandParentEvent not like '%Unknown Legacy Event%'
						and GrandParentEvent not like '% @ %' and GrandParentEvent not like '% v %' and GrandParentEvent not like '% vs %')
						Then (Case 
								When (GrandParentEvent like '%/%/%'  or GrandParentEvent like '%Round [1-9]%' or GrandParentEvent like '% Final%'
								or GrandParentEvent like '% [1-9][a-z][a-z] %' or GrandParentEvent like '% second%' or GrandParentEvent like '% week%'
								 or GrandParentEvent like '% - %' or GrandParentEvent like '% Matches%' or GrandParentEvent like '%|%')
								Then (Case 
										When (GrandParentEvent like '%/%/%')
										Then Substring(GrandParentEvent,1,Patindex('%/%/%',GrandParentEvent)-3)
										When (GrandParentEvent like 'Round [1-9]%')
										Then Substring(GrandParentEvent,11,Len(GrandParentEvent)-10)
										When (GrandParentEvent like '%Round [1-9]%')
										Then Substring(GrandParentEvent,1,Patindex('%Round [1-9]%',GrandParentEvent)-2)
										When (GrandParentEvent like '% Semi%Final%')
										Then Substring(GrandParentEvent,1,Patindex('% Semi%Final%',GrandParentEvent)-1)
										When (GrandParentEvent like '% Quarter%Final%')
										Then Substring(GrandParentEvent,1,Patindex('% Quarter%Final%',GrandParentEvent)-1)
										When (GrandParentEvent like '% Final%')
										Then Substring(GrandParentEvent,1,Patindex('% Final%',GrandParentEvent)-1)
										When (GrandParentEvent like '% Matches%')
										Then Substring(GrandParentEvent,1,Patindex('% Matches%',GrandParentEvent)-1)
										When (GrandParentEvent like '% [1-9][a-z][a-z] %')
										Then Substring(GrandParentEvent,1,Patindex('% [1-9][a-z][a-z] %',GrandParentEvent)-1)
										When (GrandParentEvent like '% second%')
										Then Substring(GrandParentEvent,1,Patindex('% second%',GrandParentEvent)-1)
										When (GrandParentEvent like '% week%')
										Then Substring(GrandParentEvent,1,Patindex('% week%',GrandParentEvent)-1)
										When (GrandParentEvent like '%|%')
										Then Substring(GrandParentEvent,1,Patindex('%|%',GrandParentEvent)-1)
										When (GrandParentEvent like '% - %')
										Then (Case 
												when GrandParentEvent like '%Live Betting - %' or GrandParentEvent like '%In-Play - %'
												Then Substring(GrandParentEvent,Patindex('% - %',GrandParentEvent)+3,Len(GrandParentEvent))
												Else Substring(GrandParentEvent,1,Patindex('% - %',GrandParentEvent)-1)
											 End)
										Else 'Check'
									 End)
								Else GrandParentEvent
						     End)
						When (ParentEvent is not null and ParentEvent not like '% @ %' and ParentEvent not like '% v %'
						 and ParentEvent not like '% vs %'  and ParentEvent<>'Live Betting' and ParentEvent not like '%Unknown Legacy Event%')
						Then (Case 
								When (ParentEvent like '%/%/%'  or ParentEvent like '%Round [1-9]%' or ParentEvent like '% Final%'
								or ParentEvent like '% [1-9][a-z][a-z] %' or ParentEvent like '% second%' or ParentEvent like '% week%'
								 or ParentEvent like '% - %' or ParentEvent like '% Matches%' or ParentEvent like '%|%')
								Then (Case 
										When (ParentEvent like '%/%/%')
										Then Substring(ParentEvent,1,Patindex('%/%/%',ParentEvent)-3)
										When (ParentEvent like 'Round [1-9]%')
										Then Substring(ParentEvent,11,Len(ParentEvent)-10)
										When (ParentEvent like '%Round [1-9]%')
										Then Substring(ParentEvent,1,Patindex('%Round [1-9]%',ParentEvent)-2)
										When (ParentEvent like '% Semi%Final%')
										Then Substring(ParentEvent,1,Patindex('% Semi%Final%',ParentEvent)-1)
										When (ParentEvent like '% Quarter%Final%')
										Then Substring(ParentEvent,1,Patindex('% Quarter%Final%',ParentEvent)-1)
										When (ParentEvent like '% Final%')
										Then Substring(ParentEvent,1,Patindex('% Final%',ParentEvent)-1)
										When (ParentEvent like '% Matches%')
										Then Substring(ParentEvent,1,Patindex('% Matches%',ParentEvent)-1)
										When (ParentEvent like '% [1-9][a-z][a-z] %')
										Then Substring(ParentEvent,1,Patindex('% [1-9][a-z][a-z] %',ParentEvent)-1)
										When (ParentEvent like '% second%')
										Then Substring(ParentEvent,1,Patindex('% second%',ParentEvent)-1)
										When (ParentEvent like '% week%')
										Then Substring(ParentEvent,1,Patindex('% week%',ParentEvent)-1)
										When (ParentEvent like '%|%')
										Then Substring(ParentEvent,1,Patindex('%|%',ParentEvent)-1)
										When (ParentEvent like '% - %')
										Then (Case 
												when ParentEvent like '%Live Betting - %' or ParentEvent like '%In-Play - %'
												Then Substring(ParentEvent,Patindex('% - %',ParentEvent)+3,Len(ParentEvent))
												Else Substring(ParentEvent,1,Patindex('% - %',ParentEvent)-1)
											 End)
										Else 'Check'
									 End)
								Else ParentEvent
						     End)
						When (SourceEvent is not null and SourceEvent not like '% @ %' and SourceEvent not like '% v %'
						 and SourceEvent not like '% vs %'  and SourceEvent<>'Live Betting'  and SourceEvent not like '%Unknown Legacy Event%')
						Then (Case 
								When (SourceEvent like '%/%/%'  or SourceEvent like '%Round [1-9]%' or SourceEvent like '% Final%'
								or SourceEvent like '% [1-9][a-z][a-z] %' or SourceEvent like '% second%' or SourceEvent like '% week%'
								 or SourceEvent like '% - %' or SourceEvent like '% Matches%' or SourceEvent like '%|%')
								Then (Case 
										When (SourceEvent like '%/%/%')
										Then Substring(SourceEvent,1,Patindex('%/%/%',SourceEvent)-3)
										When (SourceEvent like 'Round [1-9]%')
										Then Substring(SourceEvent,11,Len(SourceEvent)-10)
										When (SourceEvent like '%Round [1-9]%')
										Then Substring(SourceEvent,1,Patindex('%Round [1-9]%',SourceEvent)-2)
										When (SourceEvent like '% Semi%Final%')
										Then Substring(SourceEvent,1,Patindex('% Semi%Final%',SourceEvent)-1)
										When (SourceEvent like '% Quarter%Final%')
										Then Substring(SourceEvent,1,Patindex('% Quarter%Final%',SourceEvent)-1)
										When (SourceEvent like '% Final%')
										Then Substring(SourceEvent,1,Patindex('% Final%',SourceEvent)-1)
										When (SourceEvent like '% Matches%')
										Then Substring(SourceEvent,1,Patindex('% Matches%',SourceEvent)-1)
										When (SourceEvent like '% [1-9][a-z][a-z] %')
										Then Substring(SourceEvent,1,Patindex('% [1-9][a-z][a-z] %',SourceEvent)-1)
										When (SourceEvent like '% second%')
										Then Substring(SourceEvent,1,Patindex('% second%',SourceEvent)-1)
										When (SourceEvent like '% week%')
										Then Substring(SourceEvent,1,Patindex('% week%',SourceEvent)-1)
										When (SourceEvent like '%|%')
										Then Substring(SourceEvent,1,Patindex('%|%',SourceEvent)-1)
										When (SourceEvent like '% - %' and (SourceEvent not like '%Total%' and SourceEvent not like '%First%'))
										Then (Case 
												when SourceEvent like '%Live Betting - %' or SourceEvent like '%In-Play - %'
												Then Substring(SourceEvent,Patindex('% - %',SourceEvent)+3,Len(SourceEvent))
												Else Substring(SourceEvent,1,Patindex('% - %',SourceEvent)-1)
											 End)
										Else 'Check'
									 End)
								Else SourceEvent
						      End)
						Else 'Unknown'
					End)
where SourceEventType='Rugby Union' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1)))

--Formatting competition  
Update [$(Dimensional)].Dimension.Event
set Competition=Ltrim(Rtrim(Replace(Competition,'Live Betting','')))
where SourceEventType='Rugby Union' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (Competition like '%Live Betting%') 
and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1)))


--Removing Round  from competition 
Update [$(Dimensional)].Dimension.Event
set Competition=(Case
						When Competition like '%game %'
						Then Substring(Competition,1,Patindex('%game %',Competition)-1)
						When Competition like '%week %'
						Then Substring(Competition,1,Patindex('%week%',Competition)-1)
						When Competition like '%round %'
						Then Substring(Competition,1,Patindex('%round %',Competition)-1)
						Else Competition
					  End)
where SourceEventType='Rugby Union' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey)
 and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1))) and (Competition like '%game %' or Competition like '%week %' or Competition like '%round %')

--Removing single year numbers from competition 
Update [$(Dimensional)].Dimension.Event
set Competition= Substring(Competition,1,PATINDEX('%[1-9][0-9][0-9][0-9]%',Competition)-1)+Substring(Competition,PATINDEX('%[1-9][0-9][0-9][0-9]%',Competition)+4,Len(Competition))
where SourceEventType='Rugby Union' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and Competition like '%[1-9][0-9][0-9][0-9]%'and Competition not like '%[1-9][0-9][0-9][0-9]/[0-9]%'
 and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1)))

--formatting competition 
 Update [$(Dimensional)].Dimension.Event
set Competition=Ltrim(Rtrim(Replace(Replace(Replace(Replace(Replace(Replace(Replace(Competition,'winner',''),'finals',''),'result',''),'*',''),'final',''),'-',''),'  ',' ')))
where SourceEventType='Rugby Union' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey)
 and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1)))
 
--formatting competition 
Update [$(Dimensional)].Dimension.Event
set Competition=(Case 
						When(Competition like '%-')
						Then Ltrim(Rtrim(Replace(Substring(Competition,1,Len(Competition)-1),'  ',' ')))
						When(Competition like '%- ')
						Then Ltrim(Rtrim(Replace(Substring(Competition,1,Len(Competition)-2),'  ',' ')))
						When(Competition like '%Do %')
						Then Ltrim(Rtrim(Replace(Substring(Competition,1,Patindex('%Do %',Competition)-1),'  ',' ')))
						When(Competition like '%Try %')
						Then Ltrim(Rtrim(Replace(Substring(Competition,1,Patindex('%Try %',Competition)-1),'  ',' ')))
						When(Competition like '%[% ')
						Then Ltrim(Rtrim(Replace(Substring(Competition,1,Patindex('%[%',Competition)-1),'  ',' ')))
						When ((Competition not like '% %' and Competition not like '%NRC%' and Competition not like '%Pro12%' and Competition not like '%unknown%'
						 and Competition not like '%NA%') or Competition like 'Try %'  or Competition like '%To %')
						Then 'NA'
						Else Ltrim(Rtrim(Replace(Competition,'  ',' ')))
					  End)
where SourceEventType='Rugby Union' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) 
and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1)))
 
 --Setting unknown competition s
 Update [$(Dimensional)].Dimension.Event
set Competition='Unknown'
where SourceEventType='Rugby Union' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) 
 and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1))) and (competition='' or (Competition='NA' 
 and (SourceEvent like '% vs %' or isnull(ParentEvent,'') like '% vs %'or isnull(GrandParentEvent,'') like '% vs %'
 or SourceEvent like '% v %' or isnull(ParentEvent,'') like '% v %'or isnull(GrandParentEvent,'') like '% v %')))

--======================================================================================================================================================================   
--                                                PART 2: Insert Round Names in to Event Dimension for 'Rugby Union' event type
--====================================================================================================================================================================== 
EXEC [Control].Sp_Log	@BatchKey,'SP_Event_RugbyUnion','Part 2','Start',@RowsProcessed = @@RowCount
Update [$(Dimensional)].Dimension.Event
set round=(Case 
				When (SourceEvent like '%Team[1-9]%' or isnull(ParentEvent,'') like '%Team[1-9]%'or isnull(GrandParentEvent,'') like '%Team[1-9]%'
						or SourceEvent like '%Team[a-z]%' or isnull(ParentEvent,'') like '%Team[a-z]%'or isnull(GrandParentEvent,'') like '%Team[a-z]%'
						or SourceEvent like '%Team [a-z] %' or isnull(ParentEvent,'') like '%Team [a-z] %'or isnull(GrandParentEvent,'') like '%Team [a-z] %'
						or SourceEvent like '%Team [a-z]' or isnull(ParentEvent,'') like '%Team [a-z]'or isnull(GrandParentEvent,'') like '%Team [a-z]'
							or SourceEvent like '%Template%' or isnull(ParentEvent,'') like '%Template%'or isnull(GrandParentEvent,'') like '%Template%'
							or SourceEvent like '%abandoned%' or isnull(ParentEvent,'') like '%abandoned%'or isnull(GrandParentEvent,'') like '%abandoned%'
							or SourceEvent like '%deleted%' or isnull(ParentEvent,'') like '%deleted%'or isnull(GrandParentEvent,'') like '%deleted%'
							or SourceEvent like '%Dump%' or isnull(ParentEvent,'') like '%Dump%'or isnull(GrandParentEvent,'') like '%Dump%'
							or SourceEvent like '%[a,f] v [b,g]%' or isnull(ParentEvent,'') like '%[a,f] v [b,g]%'or isnull(GrandParentEvent,'') like '%[a,f] v [b,g]%'
							or SourceEvent like '%[a,f] vs [b,g]%' or isnull(ParentEvent,'') like '%[a,f] vs [b,g]%'or isnull(GrandParentEvent,'') like '%[a,f] vs [b,g]%')
						Then 'NA'
				When (isnull(GrandParentEvent,'') like '%Final%'  or isnull(GrandParentEvent,'') like '%Round [1-9]%'
					  or isnull(GrandParentEvent,'') like '%Week [1-9]%' or isnull(GrandParentEvent,'') like '%[1-9][a-z][a-z] Test%')
				Then (Case 
						When (isnull(GrandParentEvent,'') like '%Semi_Final%' )
						Then 'Semi Final'
						When (isnull(GrandParentEvent,'') like '%Quarter_Final%' )
						Then 'Quarter Final'
						When (isnull(GrandParentEvent,'') like '%Final%')
						Then 'Final'
						When (isnull(GrandParentEvent,'') like '%Round [1-9]%' )
						Then Substring(isnull(GrandParentEvent,''),Patindex('%Round [1-9]%',isnull(GrandParentEvent,'')),8)
						When (isnull(GrandParentEvent,'') like '%Week [1-9]%')
						Then Substring(isnull(GrandParentEvent,''),Patindex('%Week [1-9]%',isnull(GrandParentEvent,'')),7)
						When (isnull(GrandParentEvent,'') like '%[1-9][a-z][a-z] Test%')
						Then Substring(isnull(GrandParentEvent,''),Patindex('%[1-9][a-z][a-z] Test%',isnull(GrandParentEvent,'')),8)
						Else 'Check'
						End)
				When (isnull(ParentEvent,'') like '%Final%' or isnull(ParentEvent,'') like '%Round [1-9]%'
					  or isnull(ParentEvent,'') like '%Week [1-9]%' or isnull(ParentEvent,'') like '%[1-9][a-z][a-z] Test%')
				Then (Case 
						When (isnull(ParentEvent,'') like '%Semi_Final%' )
						Then 'Semi Final'
						When (isnull(ParentEvent,'') like '%Quarter_Final%' )
						Then 'Quarter Final'
						When (isnull(ParentEvent,'') like '%Final%')
						Then 'Final'
						When (isnull(ParentEvent,'') like '%Round [1-9]%' )
						Then Substring(isnull(ParentEvent,''),Patindex('%Round [1-9]%',isnull(ParentEvent,'')),8)
						When (isnull(ParentEvent,'') like '%Week [1-9]%')
						Then Substring(isnull(ParentEvent,''),Patindex('%Week [1-9]%',isnull(ParentEvent,'')),7)
						When (isnull(ParentEvent,'') like '%[1-9][a-z][a-z] Test%')
						Then Substring(isnull(ParentEvent,''),Patindex('%[1-9][a-z][a-z] Test%',isnull(ParentEvent,'')),8)
						Else 'Check'
						End)
				When (SourceEvent like '%Final%' or SourceEvent like '%Round [1-9]%' or SourceEvent like '%Week [1-9]%')
				Then (Case 
						When (SourceEvent like '%Semi_Final%' )
						Then 'Semi Final'
						When (SourceEvent like '%Quarter_Final%' )
						Then 'Quarter Final'
						When (SourceEvent like '%Final%')
						Then 'Final'
						When (SourceEvent like '%Round [1-9]%' )
						Then Substring(SourceEvent,Patindex('%Round [1-9]%',SourceEvent),8)
						When (SourceEvent like '%Week [1-9]%' )
						Then Substring(SourceEvent,Patindex('%Week [1-9]%',SourceEvent),8)
						When (SourceEvent like '%[1-9][a-z][a-z] Test%' )
						Then Substring(SourceEvent,Patindex('%[1-9][a-z][a-z] Test%',SourceEvent),8)
						Else 'Check'
						End)
				Else  'Unknown'
				End)
where SourceEventType='Rugby Union' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(10-1))<>Power(2,(10-1)))

--======================================================================================================================================================================   
--                                                PART 3: Insert Event Names in to Event Dimension for 'Rugby Union' event type
--====================================================================================================================================================================== 
EXEC [Control].Sp_Log	@BatchKey,'SP_Event_RugbyUnion','Part 3','Start',@RowsProcessed = @@RowCount
Update [$(Dimensional)].Dimension.Event
set Event=(Case		
					When (SourceEvent like '%Team[1-9]%' or isnull(ParentEvent,'') like '%Team[1-9]%'or isnull(GrandParentEvent,'') like '%Team[1-9]%'
						or SourceEvent like '%Team[a-z]%' or isnull(ParentEvent,'') like '%Team[a-z]%'or isnull(GrandParentEvent,'') like '%Team[a-z]%'
						or SourceEvent like '%Team [a-z] %' or isnull(ParentEvent,'') like '%Team [a-z] %'or isnull(GrandParentEvent,'') like '%Team [a-z] %'
						or SourceEvent like '%Team [a-z]' or isnull(ParentEvent,'') like '%Team [a-z]'or isnull(GrandParentEvent,'') like '%Team [a-z]'
							or SourceEvent like '%Template%' or isnull(ParentEvent,'') like '%Template%'or isnull(GrandParentEvent,'') like '%Template%'
							or SourceEvent like '%abandoned%' or isnull(ParentEvent,'') like '%abandoned%'or isnull(GrandParentEvent,'') like '%abandoned%'
							or SourceEvent like '%deleted%' or isnull(ParentEvent,'') like '%deleted%'or isnull(GrandParentEvent,'') like '%deleted%'
							or SourceEvent like '%Dump%' or isnull(ParentEvent,'') like '%Dump%'or isnull(GrandParentEvent,'') like '%Dump%'
							or SourceEvent like '%[a,f] v [b,g]%' or isnull(ParentEvent,'') like '%[a,f] v [b,g]%'or isnull(GrandParentEvent,'') like '%[a,f] v [b,g]%'
							or SourceEvent like '%[a,f] vs [b,g]%' or isnull(ParentEvent,'') like '%[a,f] vs [b,g]%'or isnull(GrandParentEvent,'') like '%[a,f] vs [b,g]%')
						Then 'NA'
					WHEN (isnull(ParentEvent,'') like '% vs %' or isnull(ParentEvent,'') like '% v %' or isnull(ParentEvent,'') like '%@%' )
					THEN 
						(CASE 
							WHEN isnull(ParentEvent,'') like'% - %'			
							THEN 
								(CASE
									WHEN ((SUBString(isnull(ParentEvent,''),1, CHARINDEX(' - ',isnull(ParentEvent,''))) like '% vs %') or
										(SUBString(isnull(ParentEvent,''),1, CHARINDEX(' - ',isnull(ParentEvent,''))) like '% v %') or
										(SUBString(isnull(ParentEvent,''),1, CHARINDEX(' - ',isnull(ParentEvent,''))) like '%@%') )
									THEN SUBString(isnull(ParentEvent,''),1, CHARINDEX(' - ',isnull(ParentEvent,''))-1)
									ELSE SUBString(isnull(ParentEvent,''), CHARINDEX(' - ',isnull(ParentEvent,''))+3,LEN(isnull(ParentEvent,'')))
								END)
							ELSE isnull(ParentEvent,'')
						END)
						ELSE
						(CASE
							WHEN (SourceEvent like '% vs %' or SourceEvent like '% v %' or SourceEvent like '%@%' )
							THEN 
								(CASE 
									WHEN SourceEvent like'% - %'			
									THEN 
										(CASE
											WHEN ((SUBString(SourceEvent,1, CHARINDEX(' - ',SourceEvent)) like '% vs %') or
												(SUBString(SourceEvent,1, CHARINDEX(' - ',SourceEvent)) like '% v %') or
												(SUBString(SourceEvent,1, CHARINDEX(' - ',SourceEvent)) like '%@%') )
											THEN SUBString(SourceEvent,1, CHARINDEX(' - ',SourceEvent)-1)
											ELSE SUBString(SourceEvent, CHARINDEX(' - ',SourceEvent)+3,LEN(SourceEvent))
										END)
									ELSE SourceEvent
									END)
								ELSE 'NA'
							End)
						ENd)
where SourceEventType='Rugby Union' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(2-1))<>Power(2,(2-1)))

COMMIT TRANSACTION Event_RugbyUnion;

--======================================================================================================================================================================   
--                                                PART 4: Insert Modification details in to Event Dimension for 'Rugby Union' event type
--====================================================================================================================================================================== 
EXEC [Control].Sp_Log	@BatchKey,'SP_Event_RugbyUnion','Part 4','Start',@RowsProcessed = @@RowCount
Update 	[$(Dimensional)].Dimension.Event
set	ModifiedBy='SP_Event_RugbyUnion',
	ModifiedDate=CURRENT_TIMESTAMP
where ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and SourceEventType='Rugby Union'
EXEC [Control].Sp_Log	@BatchKey,'SP_Event_RugbyUnion',NULL,'Success',@RowsProcessed = @@RowCount

END TRY
BEGIN CATCH
	Rollback TRANSACTION Event_RugbyUnion;
	declare @Error int;
	declare @ErrorMessage varchar(max);
	                           -- Log the failure at the object level
                           SET    @Error = ERROR_NUMBER()
                           SET    @ErrorMessage = ERROR_MESSAGE()
						   EXEC [Control].Sp_Log @BatchKey, 'SP_Event_RugbyUnion', NULL, 'Failed', @ErrorMessage;
						   Raiserror(@ErrorMessage,16,1)

END CATCH

END

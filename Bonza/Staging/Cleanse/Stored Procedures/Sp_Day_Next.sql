﻿CREATE proc [Cleanse].[SP_Day_Next]
(
	@BatchKey int = 0,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @RowCount INT;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

BEGIN TRY

	EXEC [Control].Sp_Log @BatchKey, 'Sp_DayNext', 'Update', 'Start';

	WITH LastPeriods AS
		(	SELECT	d.DayDate, 
					MAX(CASE d1.MonthName WHEN d.MonthName THEN NULL ELSE d1.DayDate END) LastMonthDate,
					MAX(CASE WHEN d1.YearName = d.YearName THEN NULL WHEN d1.MonthOfYearNumber <> d.MonthOfYearNumber THEN NULL ELSE d1.DayDate END) LastYearDate, 
					MAX(CASE d1.FiscalMonthName WHEN d.FiscalMonthName THEN NULL ELSE d1.DayDate END) LastFiscalMonthDate,
					MAX(CASE WHEN d1.FiscalYearName = d.FiscalYearName THEN NULL WHEN d1.FiscalWeekOfYearNumber <> d.FiscalWeekOfYearNumber THEN NULL ELSE d1.DayDate END) LastFiscalYearDate
			FROM	[$(Dimensional)].Dimension.Day d
					LEFT JOIN [$(Dimensional)].Dimension.Day d1 ON (d1.daydate < d.DayDate AND d1.DayDate > CONVERT(datetime2(3),'1900-01-01'))
			WHERE	d.DayDate > CONVERT(datetime2(3),'1900-01-01')
					AND d.DayDate < CONVERT(datetime2(3),'9999-01-01')
			GROUP BY d.DayDate
		)
	UPDATE	d 
	SET		YesterdayName = Yesterday.DayName, 
			YesterdayDate = Yesterday.DayDate, 
			LastWeekName = LastWeek.WeekName, 
			LastWeekSameDayName = LastWeek.DayName, 
			LastWeekSameDayDate = LastWeek.DayDate, 
			LastFiscalWeekName = LastWeek.FiscalWeekName, 
			LastMonthName = LastMonth.MonthName, 
			LastFiscalMonthName = LastFiscalMonth.FiscalMonthName, 
			LastYearName = LastYear.YearName,
			LastYearSameDayName = LastYearDay.DayName,
			LastYearSameDayDate = LastYearDay.DayDate,
			LastYearSameWeekName = LastYearDay.WeekName,
			LastYearSameMonthName = LastYear.MonthName,
			LastFiscalYearName = LastFiscalYear.FiscalYearName,
			LastFiscalYearSameWeekName = LastFiscalYear.FiscalWeekName,
			LastFiscalYearSameMonthName = LastFiscalYear.FiscalMonthName
	FROM	[$(Dimensional)].Dimension.Day d
			LEFT JOIN LastPeriods ON (LastPeriods.DayDate = d.DayDate)
			LEFT JOIN [$(Dimensional)].Dimension.Day Yesterday ON (Yesterday.daydate = DATEADD(day,-1, d.DayDate))
			LEFT JOIN [$(Dimensional)].Dimension.Day LastWeek ON (LastWeek.daydate = DATEADD(day,-7, d.DayDate))
			LEFT JOIN [$(Dimensional)].Dimension.Day LastYearDay ON (LastYearDay.daydate = DATEADD(day,-364, d.DayDate))
			LEFT JOIN [$(Dimensional)].Dimension.Day LastMonth ON (LastMonth.DayDate = LastPeriods.LastMonthDate)
			LEFT JOIN [$(Dimensional)].Dimension.Day LastYear ON (LastYear.DayDate = LastPeriods.LastYearDate)
			LEFT JOIN [$(Dimensional)].Dimension.Day LastFiscalMonth ON (LastFiscalMonth.DayDate = LastPeriods.LastFiscalMonthDate)
			LEFT JOIN [$(Dimensional)].Dimension.Day LastFiscalYear ON (LastFiscalYear.DayDate = LastPeriods.LastFiscalYearDate)
	WHERE	d.DayDate > CONVERT(datetime2(3),'1900-01-01')
			AND d.DayDate < CONVERT(datetime2(3),'9999-01-01')
			AND d.ModifiedBatchKey = CASE @BatchKey WHEN 0 THEN d.ModifiedBatchKey ELSE @BatchKey END 

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey, 'Sp_DayNext', NULL, 'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_DayNext', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

﻿-- =============================================
-- Author:		Lekha Narra
-- Create date: 17/11/2014
-- Description:	Insert eventNames,roundNames,CompetitionNames for Soccer event type
-- =============================================
CREATE PROCEDURE [Cleanse].[SP_Event_Baseball]
	-- Add the parameters for the stored procedure here
	@BatchKey int 
AS
BEGIN
BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  BEGIN TRANSACTION Event_Baseball ;
	
--======================================================================================================================================================================   
--                                                PART 1: Insert Competition Names in to Event Dimension for 'Baseball' event type
--====================================================================================================================================================================== 
EXEC [Control].Sp_Log	@BatchKey,'SP_Event_Baseball','Part 1','Start'
Update [$(Dimensional)].Dimension.Event
set Competition=(Case 
						When (SourceEvent like '%Team1 @ Team2%' or ParentEvent like '%Team1 @ Team2%'or GrandParentEvent like '%Team1 @ Team2%'
							or SourceEvent like '%Template%' or ParentEvent like '%Template%'or GrandParentEvent like '%Template%'
							or SourceEvent like '%Test%' or ParentEvent like '%Test%'or GrandParentEvent like '%Test%'
							or SourceEvent like '%New Master%' or ParentEvent like '%New Master%'or GrandParentEvent like '%New Master%')
						Then 'NA'
						When (GrandParentEvent is not null and GrandParentEvent not like 'Live Betting%' and GrandParentEvent not like '% at %' 
						and GrandParentEvent not like '%@%' and GrandParentEvent not like '% v %' and GrandParentEvent not like '% vs %'
						 and GrandParentEvent<>'Unknown Legacy Event')
						Then (Case 
								When (GrandParentEvent like '%/%/%'  or GrandParentEvent like '%Game #[1-9]%' or GrandParentEvent like '%Final%')
								Then (Case 
										When (GrandParentEvent like '%/%/%')
										Then Substring(GrandParentEvent,1,Patindex('%/%/%',GrandParentEvent)-3)
										When (GrandParentEvent like '%Game #[1-9]%')
										Then Substring(GrandParentEvent,1,Patindex('%Game #[1-9]%',GrandParentEvent)-2)
										When (GrandParentEvent like '%Final%')
										Then Substring(GrandParentEvent,1,Patindex('%Final%',GrandParentEvent)-2)
										Else 'Check'
									 End)
								Else GrandParentEvent
						     End)
						When ParentEvent is not null 
						Then (Case 
								When ((ParentEvent like '%@%'  or ParentEvent like '% v %'  or ParentEvent like '% at %' or ParentEvent like '% vs %')
								       and ParentEvent not like '%Derby%'  and ParentEvent not like '%Series%'  and ParentEvent not like '%Baseball%'
									    and ParentEvent not like '%Championships%' and ParentEvent not like '%MLB %' and ParentEvent not like '%ABL %'
										 and ParentEvent not like '%Unknown Legacy Event%')
								Then (Case 
										When (SourceEvent like '% vs %'  or SourceEvent like '% v %' or SourceEvent like '%@%' or SourceEvent like '% at %' or
												SourceEvent like '%?%'or SourceEvent like '%Team%' or SourceEvent like '%Player %' 
												or SourceEvent like '%Regular Season Wins %')
										Then 'Unknown'
										Else (Case 
												When (SourceEvent like '%/%/%')
												Then Substring(SourceEvent,1,Patindex('%/%/%',SourceEvent)-3)
												When (SourceEvent like '%Game #[1-9]%')
												Then Substring(SourceEvent,1,Patindex('%Game #[1-9]%',SourceEvent)-2)
												When (SourceEvent like '%Final%')
												Then Substring(SourceEvent,1,Patindex('%Final%',SourceEvent)-1)
												Else SourceEvent
											  End)
									   End)
								Else (Case
										When (ParentEvent like '%Games%' or ParentEvent like '%Daily Specials%' or ParentEvent like '%Futures%'
										 or ParentEvent like '%Wins%'  or ParentEvent like '%/%/%'  or ParentEvent like '%live betting%')
										Then (Case 
												When (ParentEvent like '%/%/%')
												Then Substring(ParentEvent,1,Patindex('%/%/%',ParentEvent)-3)
												When (ParentEvent like '%Game #[1-9]%')
												Then Substring(ParentEvent,1,Patindex('%Game #[1-9]%',ParentEvent)-2)
												When (ParentEvent like '%Final%')
												Then Substring(ParentEvent,1,Patindex('%Final%',ParentEvent)-2)
												When (ParentEvent like '%Team%')
												Then Substring(ParentEvent,1,Patindex('%Team%',ParentEvent)-2)
												When (ParentEvent like '%Regular%')
												Then Substring(ParentEvent,1,Patindex('%Regular%',ParentEvent)-2)
												When (ParentEvent like '%live betting%')
												Then Substring(ParentEvent,1,Patindex('%live betting%',ParentEvent)-2)
												Else ParentEvent
											 End)
										Else (Case
												When (SourceEvent like '% vs %'  or SourceEvent like '% v %' or SourceEvent like '%@%' or SourceEvent like '% at %')
												Then 'Unknown'
												When (SourceEvent like '%?%'or SourceEvent like '%Team%' or SourceEvent like '%Player %' 
													 or SourceEvent like '%Regular Season Wins %')
												Then 'NA'
												Else (Case 
														When (SourceEvent like '%/%/%')
														Then Substring(SourceEvent,1,Patindex('%/%/%',SourceEvent)-3)
														When (SourceEvent like '%Game #[1-9]%')
														Then Substring(SourceEvent,1,Patindex('%Game #[1-9]%',SourceEvent)-2)
														When (SourceEvent like '%Final%')
														Then Substring(SourceEvent,1,Patindex('%Final%',SourceEvent)-1)
														Else SourceEvent
													 End)
											  End)
									  End)
							  End)
						Else(Case
								When (SourceEvent like '% vs %'  or SourceEvent like '% v %' or SourceEvent like '%@%' or SourceEvent like '% at %')
								Then 'Unknown'
								When (SourceEvent like '%?%'or SourceEvent like '%Team%' or SourceEvent like '%Player %' 
								 or SourceEvent like '%Regular Season Wins %')
								Then 'NA'
								Else (Case 
										When (SourceEvent like '%/%/%')
										Then Substring(SourceEvent,1,Patindex('%/%/%',SourceEvent)-3)
										When (SourceEvent like '%Game #[1-9]%')
										Then Substring(SourceEvent,1,Patindex('%Game #[1-9]%',SourceEvent)-2)
										When (SourceEvent like '%Final%')
										Then Substring(SourceEvent,1,Patindex('%Final%',SourceEvent)-1)
										Else SourceEvent
										End)
								End)
					End)
where SourceEventType='Baseball' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1)))

--Removing erroneous data from competition 
Update [$(Dimensional)].Dimension.Event
set Competition=(Case 
						when Competition like '%Live Betting%'
						Then Ltrim(Rtrim(Substring(Competition,1,PATINDEX('%Live Betting%',Competition)-1)))
						when Competition like 'To %'
						Then 'Unknown'
						when Competition like '%To %'
						Then Ltrim(Rtrim(Substring(Competition,1,PATINDEX('%To %',Competition)-1)))
						when Competition like '% Winner%'
						Then Ltrim(Rtrim(Replace(Competition,' Winner','')))
						when Competition like '% outcome%'
						Then Ltrim(Rtrim(Replace(Competition,' outcome','')))
						when Competition like '%_[0-9]/[0-9]%' and Competition not like '%[0-9][0-9][0-9][0-9]/[0-9]%'
						Then Ltrim(Rtrim(Substring(Competition,1,PATINDEX('%[0-9]/[0-9]%',Competition)-2)))
						when Competition like '%[1-9][a-z][a-z] %'
						Then Ltrim(Rtrim(Substring(Competition,1,PATINDEX('%[1-9][a-z][a-z] %',Competition)-1)))
						Else Ltrim(Rtrim(Competition))
					 End)
where SourceEventType='Baseball' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey)  and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1)))

--Removing erroneous data from competition 
Update [$(Dimensional)].Dimension.Event
set Competition=(Case 
						when Competition like '%how %' or Competition like '%who %'  or Competition like '%under %'
						Then 'NA'
						when Competition like '% - %' and Competition like '%mlb%'
						Then Ltrim(Rtrim(Substring(Competition,1,PATINDEX('% - %',Competition)-1)))+' MLB'
						when Competition like '% - %' and Competition not like '%minor league%' and Competition not like '%world series%'
								and Competition not like '%pacific league%'
						Then Ltrim(Rtrim(Substring(Competition,1,PATINDEX('% - %',Competition)-1)))
						when Competition like '%[1-9][a-z][a-z] %'
						Then Ltrim(Rtrim(Substring(Competition,1,PATINDEX('%[1-9][a-z][a-z] %',Competition)-1)))
						when Competition like '%Round%'
						Then Ltrim(Rtrim(Substring(Competition,1,PATINDEX('%Round%',Competition)-1)))
						when Competition like '%Semi%Final%' or Competition like '%Semi'
						Then Ltrim(Rtrim(Substring(Competition,1,PATINDEX('%Semi%',Competition)-1)))
						when Competition like '%Quarter%Final%' or Competition like '%Quarter'
						Then Ltrim(Rtrim(Substring(Competition,1,PATINDEX('%Quarter%',Competition)-1)))
						when Competition like '%Final%'
						Then Ltrim(Rtrim(Substring(Competition,1,PATINDEX('%Final%',Competition)-1)))
						when Competition like '%Week %'
						Then Ltrim(Rtrim(Substring(Competition,1,PATINDEX('%Week %',Competition)-1)))
						when Competition like '%Game %' or Competition like '%Games %'
						Then Ltrim(Rtrim(Substring(Competition,1,PATINDEX('%Game%',Competition)-1)))
						when Competition like '%playoff %'
						Then Ltrim(Rtrim(Substring(Competition,1,PATINDEX('%playoff %',Competition)-1)))
						when Competition like '%*%'
						Then Ltrim(Rtrim(Substring(Competition,1,PATINDEX('%*%',Competition)-1)))
						when Competition like '%correct %'
						Then Ltrim(Rtrim(Substring(Competition,1,PATINDEX('%correct %',Competition)-1)))
						when Competition like '%To %'
						Then Ltrim(Rtrim(Substring(Competition,1,PATINDEX('%To %',Competition)-1)))
						when Competition like '%do not %'
						Then Ltrim(Rtrim(Substring(Competition,1,PATINDEX('%do not %',Competition)-1)))
						when Competition like '%Live %' or Competition like '% Live%'
						Then Ltrim(Rtrim(Substring(Competition,1,PATINDEX('%Live%',Competition)-1)))
						Else Ltrim(Rtrim(Competition))
					 End)
where SourceEventType='Baseball' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey)  and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1)))

--Removing erroneous data from competition 
Update [$(Dimensional)].Dimension.Event
set Competition= Ltrim(Rtrim(Substring(Competition,1,PATINDEX('%(%',Competition)-1)))
where SourceEventType='Baseball' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey)  and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1)))
and Competition like '%Salami (%'

--Removing single year numbers from competition 
Update [$(Dimensional)].Dimension.Event
set Competition= Ltrim(Rtrim(Substring(Competition,1,PATINDEX('%[1-9][0-9][0-9][0-9]%',Competition)-1)+
								Substring(Competition,PATINDEX('%[1-9][0-9][0-9][0-9]%',Competition)+4,Len(Competition))))
where SourceEventType='Baseball' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and Competition like '%[1-9][0-9][0-9][0-9]%'and Competition not like '%[1-9][0-9][0-9][0-9]/[0-9]%'
 and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1)))

--Removing trailing '-' from competition 
Update [$(Dimensional)].Dimension.Event
set Competition=Ltrim(Rtrim(Replace(Replace(Replace(Competition,'-',''),'(',''),'wins','')))
where SourceEventType='Baseball' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey)  and (Competition like '%-' or Competition like '%- ' or 
		Competition like '%(' or Competition like '%( ' or Competition like '%wins%')
 and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1)))

--Setting Unknowns for competition 
Update [$(Dimensional)].Dimension.Event
set Competition= 'Unknown'
where SourceEventType='Baseball' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey)	and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1)))	and (Competition='' or
(Competition not like '% %' and Competition not like '%NBA%' and Competition not like '%ABL%' and 
Competition not like '%NA%' and Competition not like '%Unknown%' and Competition not like '%AFL%' and Competition not like '%WBC%'
 and Competition not like '%ALCS%'  and Competition not like '%MLB%'))

--======================================================================================================================================================================   
--                                                PART 2: Insert Round Names in to Event Dimension for 'Baseball' event type
--====================================================================================================================================================================== 
EXEC [Control].Sp_Log	@BatchKey,'SP_Event_Baseball','Part 2','Start', @RowsProcessed = @@ROWCOUNT
Update [$(Dimensional)].Dimension.Event
set round=(Case 
				When (SourceEvent like '%Team1 @ Team2%' or ParentEvent like '%Team1 @ Team2%'or GrandParentEvent like '%Team1 @ Team2%'
							or SourceEvent like '%Template%' or ParentEvent like '%Template%'or GrandParentEvent like '%Template%'
							or SourceEvent like '%Test%' or ParentEvent like '%Test%'or GrandParentEvent like '%Test%'
							or SourceEvent like '%New Master%' or ParentEvent like '%New Master%'or GrandParentEvent like '%New Master%')
						Then 'NA'
				When (isnull(GrandParentEvent,'') like '%Final%'  or isnull(GrandParentEvent,'') like '%Game%#[1-9]%'
				 or isnull(GrandParentEvent,'') like '%Game [1-9]%' or isnull(GrandParentEvent,'') like '%Week [1-9]%'
				 or isnull(GrandParentEvent,'') like '%Playoff%' or isnull(GrandParentEvent,'') like '%/%/%' 
				  or isnull(GrandParentEvent,'') like '%Round%[1-9]%')
				Then (Case 
						When (isnull(GrandParentEvent,'') like '%Semi_Final%' )
						Then 'Semi Final'
						When (isnull(GrandParentEvent,'') like '%Quarter_Final%' )
						Then 'Quarter Final'
						When (isnull(GrandParentEvent,'') like '%Final%')
						Then 'Final'
						When (isnull(GrandParentEvent,'') like '%Game #[1-9]%' ) or (isnull(GrandParentEvent,'') like '%Games #[1-9]%' )
						Then Substring(isnull(GrandParentEvent,''),Patindex('%Game%#[1-9]%',isnull(GrandParentEvent,'')),Len(GrandParentEvent))
						When (isnull(GrandParentEvent,'') like '%Game [1-9]%' )
						Then Substring(isnull(GrandParentEvent,''),Patindex('%Game [1-9]%',isnull(GrandParentEvent,'')),Len(GrandParentEvent))
						When (isnull(GrandParentEvent,'') like '%Round%[1-9]%' )
						Then Substring(isnull(GrandParentEvent,''),Patindex('%Round%[1-9]%',isnull(GrandParentEvent,'')),Len(GrandParentEvent))
						When (isnull(GrandParentEvent,'') like '%Week [1-9]%' )
						Then Substring(isnull(GrandParentEvent,''),Patindex('%Week [1-9]%',isnull(GrandParentEvent,'')),Len(GrandParentEvent))
						When (isnull(GrandParentEvent,'') like '%Playoff%' )
						Then Substring(isnull(GrandParentEvent,''),Patindex('%Playoff%',isnull(GrandParentEvent,'')),Len(GrandParentEvent))
						When (isnull(GrandParentEvent,'') like '%/%/%')
						Then Substring(isnull(GrandParentEvent,''),Patindex('%/%/%',isnull(GrandParentEvent,''))-2,Len(GrandParentEvent))
						Else 'Check'
						End)
				When (isnull(ParentEvent,'') like '%Final%' or isnull(ParentEvent,'') like '%Game%#[1-9]%'or isnull(ParentEvent,'') like '%/%/%'
				 or isnull(ParentEvent,'') like '%Game [1-9]%' or isnull(ParentEvent,'') like '%Week [1-9]%'
				 or isnull(ParentEvent,'') like '%Playoff%'  or isnull(ParentEvent,'') like '%Round%[1-9]%')
				Then (Case 
						When (isnull(ParentEvent,'') like '%Semi_Final%' )
						Then 'Semi Final'
						When (isnull(ParentEvent,'') like '%Quarter_Final%' )
						Then 'Quarter Final'
						When (isnull(ParentEvent,'') like '%Final%')
						Then 'Final'
						When (isnull(ParentEvent,'') like '%Game%#[1-9]%' )
						Then Substring(isnull(ParentEvent,''),Patindex('%Game%#[1-9]%',isnull(ParentEvent,'')),7)
						When (isnull(ParentEvent,'') like '%Round%[1-9]%' )
						Then Substring(isnull(ParentEvent,''),Patindex('%Round%[1-9]%',isnull(ParentEvent,'')),8)
						When (isnull(ParentEvent,'') like '%Game [1-9]%' )
						Then Substring(isnull(ParentEvent,''),Patindex('%Game [1-9]%',isnull(ParentEvent,'')),Len(ParentEvent))
						When (isnull(ParentEvent,'') like '%Week [1-9]%' )
						Then Substring(isnull(ParentEvent,''),Patindex('%Week [1-9]%',isnull(ParentEvent,'')),Len(ParentEvent))
						When (isnull(ParentEvent,'') like '%Playoff%' )
						Then Substring(isnull(ParentEvent,''),Patindex('%Playoff%',isnull(ParentEvent,'')),Len(ParentEvent))
						When (isnull(ParentEvent,'') like '%/%/%')
						Then Substring(isnull(ParentEvent,''),Patindex('%/%/%',isnull(ParentEvent,''))-2,Len(ParentEvent))
						Else 'Check'
						End)
				When (SourceEvent like '%Final%' or SourceEvent like '%Game%#[1-9]%' or SourceEvent like '%/%/%'
				 or SourceEvent like '%Game [1-9]%' or SourceEvent like '%Week [1-9]%' or SourceEvent like '%Playoff%'
				  or SourceEvent like '%Round%[1-9]%')
				Then (Case 
						When (SourceEvent like '%Semi_Final%' )
						Then 'Semi Final'
						When (SourceEvent like '%Quarter_Final%' )
						Then 'Quarter Final'
						When (SourceEvent like '%Final%')
						Then 'Final'
						When (SourceEvent like '%Game%#[1-9]%' )
						Then Substring(SourceEvent,Patindex('%Game%#[1-9]%',SourceEvent),7)
						When (SourceEvent like '%Round%[1-9]%' )
						Then Substring(SourceEvent,Patindex('%Round%[1-9]%',SourceEvent),8)
						When SourceEvent like '%Game [1-9]%' 
						Then Substring(SourceEvent,Patindex('%Game [1-9]%',SourceEvent),Len(SourceEvent))
						When (isnull(SourceEvent,'') like '%Week [1-9]%' )
						Then Substring(SourceEvent,Patindex('%Week [1-9]%',SourceEvent),Len(SourceEvent))
						When (isnull(SourceEvent,'') like '%Playoff%' )
						Then Substring(SourceEvent,Patindex('%Playoff%',SourceEvent),Len(SourceEvent))
						When (SourceEvent like '%/%/%' )
						Then Substring(SourceEvent,Patindex('%/%/%',SourceEvent)-2,Len(SourceEvent))
						Else 'Check'
						End)
				Else  Cast(Format(Convert(date,SourceEventDate),'dd/MM/yyyy') as varchar)
				End)
where SourceEventType='Baseball' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey)  and (ColumnLock&Power(2,(10-1))<>Power(2,(10-1)))

--Formatting round 
Update [$(Dimensional)].Dimension.Event
set round=(Case 
				When round like '%prices %'
				Then Ltrim(Rtrim(Substring(round,1,Patindex('%prices %',round)-1)))
				When round like '%@%'
				Then Ltrim(Rtrim(Substring(round,1,Patindex('%@%',round)-1)))
				When round like '%-% %/%/%'
				Then Ltrim(Rtrim(Substring(round,1,Patindex('%-% %/%/%',round)-1)+
					 Substring(round,Patindex('%/%/%',round)-2,Len(round))))
				When round like '%-%'
				Then Ltrim(Rtrim(Substring(round,1,Patindex('%-%',round)-1)))
				When round like 'Game [0-9]/%/%' or round like 'Game [0-9][0-9]/%/%'
				Then Ltrim(Rtrim(Substring(round,Patindex('Game %',round)+5,Len(round))))
				When round like '%Live Wagering%'
				Then Ltrim(Rtrim(Replace(Replace(round,'Live Wagering',''),'  ',' ')))
				When round like '%Wagering%'
				Then Ltrim(Rtrim(Replace(Replace(round,'Wagering',''),'  ',' ')))
				When round like '%Series%' and round not like '%Series game%'
				Then Ltrim(Rtrim(Substring(round,1,Patindex('%Series%',round)-1)))
				When round like '%Do not%'
				Then Ltrim(Rtrim(Substring(round,1,Patindex('%Do not%',round)-1)))
				When round like '%(ESPN%'
				Then Ltrim(Rtrim(Substring(round,1,Patindex('%(ESPN%',round)-1)))
				When round like '%/%-%/%'
				Then Cast(Format(Convert(date,SourceEventDate),'dd/MM/yyyy') as varchar)
				When round like '%(ACST)%' and round not like '%(ACST)'
				Then Ltrim(Rtrim(Substring(round,1,Patindex('%(ACST)%',round)+5)))
				Else Ltrim(Rtrim(Round))
			  End)
where SourceEventType='Baseball' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey)  and (ColumnLock&Power(2,(10-1))<>Power(2,(10-1)))
--======================================================================================================================================================================   
--                                                PART 3: Insert Event Names in to Event Dimension for 'Baseball' event type
--====================================================================================================================================================================== 
EXEC [Control].Sp_Log	@BatchKey,'SP_Event_Baseball','Part 3','Start', @RowsProcessed = @@ROWCOUNT
Update [$(Dimensional)].Dimension.Event
set Event=(Case		
					When (SourceEvent like '%Team1 @ Team2%' or ParentEvent like '%Team1 @ Team2%'or GrandParentEvent like '%Team1 @ Team2%'
							or SourceEvent like '%Template%' or ParentEvent like '%Template%'or GrandParentEvent like '%Template%'
							or SourceEvent like '%Test%' or ParentEvent like '%Test%'or GrandParentEvent like '%Test%'
							or SourceEvent like '%New Master%' or ParentEvent like '%New Master%'or GrandParentEvent like '%New Master%')
					Then 'NA'
					WHEN (isnull(ParentEvent,'') like '% vs %' or isnull(ParentEvent,'') like '% v %' or isnull(ParentEvent,'') like '%@%' 
					or isnull(ParentEvent,'') like '% at %')
					THEN 
						(CASE 
							WHEN isnull(ParentEvent,'') like'% - %'			
							THEN 
								(CASE
									WHEN ((SUBString(isnull(ParentEvent,''),1, CHARINDEX(' - ',isnull(ParentEvent,''))) like '% vs %') or
										(SUBString(isnull(ParentEvent,''),1, CHARINDEX(' - ',isnull(ParentEvent,''))) like '% v %') or
										(SUBString(isnull(ParentEvent,''),1, CHARINDEX(' - ',isnull(ParentEvent,''))) like '%@%')  or
										(SUBString(isnull(ParentEvent,''),1, CHARINDEX(' - ',isnull(ParentEvent,''))) like '% at %') )
									THEN SUBString(isnull(ParentEvent,''),1, CHARINDEX(' - ',isnull(ParentEvent,''))-1)
									ELSE SUBString(isnull(ParentEvent,''), CHARINDEX(' - ',isnull(ParentEvent,''))+3,LEN(isnull(ParentEvent,'')))
								END)
							ELSE isnull(ParentEvent,'')
						END)
						ELSE
						(CASE
							WHEN (SourceEvent like '% vs %' or SourceEvent like '% v %' or SourceEvent like '%@%'  or SourceEvent like '% at %')
							THEN 
								(CASE 
									WHEN SourceEvent like'% - %'			
									THEN 
										(CASE
											WHEN ((SUBString(SourceEvent,1, CHARINDEX(' - ',SourceEvent)) like '% vs %') or
												(SUBString(SourceEvent,1, CHARINDEX(' - ',SourceEvent)) like '% v %') or
												(SUBString(SourceEvent,1, CHARINDEX(' - ',SourceEvent)) like '%@%')  or
												(SUBString(SourceEvent,1, CHARINDEX(' - ',SourceEvent)) like '% at %') )
											THEN SUBString(SourceEvent,1, CHARINDEX(' - ',SourceEvent)-1)
											ELSE SUBString(SourceEvent, CHARINDEX(' - ',SourceEvent)+3,LEN(SourceEvent))
										END)
									ELSE SourceEvent
									END)
								ELSE 'NA'
							End)
						ENd)
where SourceEventType='Baseball' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey)  and (ColumnLock&Power(2,(2-1))<>Power(2,(2-1)))

COMMIT TRANSACTION Event_Baseball;

--======================================================================================================================================================================   
--                                                PART 4: Insert Modification details in to Event Dimension for 'Baseball' event type
--====================================================================================================================================================================== 
EXEC [Control].Sp_Log	@BatchKey,'SP_Event_Baseball','Part 4','Start', @RowsProcessed = @@ROWCOUNT
Update 	[$(Dimensional)].Dimension.Event
set	ModifiedBy='SP_Event_Baseball',
	ModifiedDate=CURRENT_TIMESTAMP
where ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and SourceEventType='Baseball'
EXEC [Control].Sp_Log	@BatchKey,'SP_Event_Baseball',NULL,'Success'

END TRY
BEGIN CATCH
	Rollback TRANSACTION Event_Baseball;
	declare @Error int;
	declare @ErrorMessage varchar(max);
	                           -- Log the failure at the object level
                           SET    @Error = ERROR_NUMBER()
                           SET    @ErrorMessage = ERROR_MESSAGE()
						   EXEC [Control].Sp_Log @BatchKey, 'SP_Event_Baseball', NULL, 'Failed', @ErrorMessage;
						   Raiserror(@ErrorMessage,16,1)

END CATCH

END

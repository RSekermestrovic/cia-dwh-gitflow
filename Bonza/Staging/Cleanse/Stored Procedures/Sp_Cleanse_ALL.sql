﻿CREATE PROCEDURE [Cleanse].[Sp_Cleanse_ALL] @BatchKey int
AS

-----------------CLEAN NEXT DAYS----------------------------
EXEC [Cleanse].[SP_Day_Next]							@BatchKey

-----------------CLEAN STRUCTURE----------------------------
EXEC Cleanse.[SP_Structure]							@BatchKey

-----------------CLEAN ACCOUNT DIMENSION----------------------------
EXEC [Cleanse].[SP_Account_Organisation]				@BatchKey -- NOTE - This has to follow the clensing of the Structure dimension above as the results of that are propogated into the account dimension
EXEC [Cleanse].[SP_Account_Type]						@BatchKey
EXEC [Cleanse].[SP_Account_Opened]						@BatchKey
EXEC [Cleanse].[SP_Account_Ledger]						@BatchKey
EXEC [Cleanse].[SP_Account_Affiliate]					@BatchKey
EXEC [Cleanse].[SP_Account_Customer]					@BatchKey
EXEC [Cleanse].[SP_Account_Name]						@BatchKey
EXEC [Cleanse].[SP_Account_Address]					@BatchKey
EXEC [Cleanse].[SP_Account_Phone]						@BatchKey
EXEC [Cleanse].[SP_Account_Email]						@BatchKey

-----------------CLEAN DIM EVENT-----------------------------------
EXEC Cleanse.[SP_Event_All]							@BatchKey
EXEC Cleanse.[SP_Event_Venue]							@BatchKey
EXEC Cleanse.[SP_Event_ClassKey]						@BatchKey
EXEC Cleanse.[SP_Event_AmericanFootball]				@BatchKey
EXEC Cleanse.[SP_Event_AustralianRules]					@BatchKey
EXEC Cleanse.[SP_Event_Baseball]						@BatchKey
EXEC Cleanse.[SP_Event_BasketBall]						@BatchKey
EXEC Cleanse.[SP_Event_Cricket]						@BatchKey
EXEC Cleanse.[SP_Event_Golf]							@BatchKey
EXEC Cleanse.[SP_Event_RacingGhTrots]					@BatchKey
EXEC Cleanse.[SP_Event_RugbyLeague]					@BatchKey
EXEC Cleanse.[SP_Event_RugbyUnion]						@BatchKey
EXEC Cleanse.[SP_Event_Soccer]						@BatchKey
EXEC Cleanse.[SP_Event_Tennis]						@BatchKey
EXEC Cleanse.[SP_Event_CsSdEdGrDis]					@BatchKey

-----------------CLEAN INSTRUMENT DIMENSSION -----------------------
EXEC Cleanse.[Sp_Instrument_Type]						@BatchKey
EXEC Cleanse.[Sp_Instrument_EFT]						@BatchKey
EXEC Cleanse.[Sp_Instrument_BPay]						@BatchKey
EXEC Cleanse.[Sp_Instrument_Paypal]					@BatchKey
EXEC Cleanse.[Sp_Instrument_Moneybookers]				@BatchKey
--------------------------------------------------------------------

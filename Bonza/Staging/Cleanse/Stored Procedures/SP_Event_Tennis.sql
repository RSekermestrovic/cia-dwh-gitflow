﻿-- =============================================
-- Author:		Lekha Narra
-- Create date: 12/11/2014
-- Description:	Insert Event Names, Round Names and Competition Names in to Event Dimension for 'Tennis' Event Type
-- =============================================
CREATE PROCEDURE [Cleanse].[SP_Event_Tennis] 
	-- Add the parameters for the stored procedure here
	@BatchKey int 
AS
BEGIN
BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

BEGIN TRANSACTION Event_Tennis ;

-- ================================================================================================================================================================
  --                        PART 1: Update Event Names in Event Dimension for 'Tennis' event type
-- ================================================================================================================================================================
EXEC [Control].Sp_Log	@BatchKey,'SP_Event_Tennis','Part 1','Start'
update [$(Dimensional)].Dimension.Event
set Event=(CASE 
				when (SourceEvent like '%Test%' or isnull(ParentEvent,'')  like '%Test%'or isnull(GrandParentEvent,'')  like '%Test%') 
				then 'Test'
				WHEN (isnull(ParentEvent,'') like '% vs %' or isnull(ParentEvent,'') like '% v %' or isnull(ParentEvent,'') like '% @ %' )
				THEN 
					(CASE 
						WHEN ParentEvent like'% - %'			
						THEN 
							(CASE
							WHEN ((SUBString(ParentEvent,1, CHARINDEX(' - ',ParentEvent)) like '% vs %') or
								   (SUBString(ParentEvent,1, CHARINDEX(' - ',ParentEvent)) like '% v %') or
									(SUBString(ParentEvent,1, CHARINDEX(' - ',ParentEvent)) like '% @ %') )
							THEN SUBString(ParentEvent,1, CHARINDEX(' - ',ParentEvent))
							ELSE SUBString(ParentEvent, CHARINDEX(' - ',ParentEvent)+3,LEN(ParentEvent))
							END)
						ELSE ParentEvent
					END)
				WHEN (SourceEvent like '% vs %' or SourceEvent like '% v %' or SourceEvent like '% @ %' )
				THEN 
					(CASE 
						WHEN SourceEvent like'% - %'			
						THEN 
						 (CASE
							WHEN ((SUBString(SourceEvent,1, CHARINDEX(' - ',SourceEvent)) like '% vs %') or
								(SUBString(SourceEvent,1, CHARINDEX(' - ',SourceEvent)) like '% v %') or
								(SUBString(SourceEvent,1, CHARINDEX(' - ',SourceEvent)) like '% @ %') )
							THEN SUBString(SourceEvent,1, CHARINDEX(' - ',SourceEvent))
							ELSE SUBString(SourceEvent, CHARINDEX(' - ',SourceEvent)+3,LEN(SourceEvent))
						  END)
						ELSE SourceEvent
					END)
				ELSE 'NA'
			  END)
where sourceeventtype='Tennis' 
and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(2-1))<>Power(2,(2-1)))					

update [$(Dimensional)].Dimension.Event
set Event=RTRIM(LTRIM(Event))
where sourceeventtype='Tennis' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey)  and (ColumnLock&Power(2,(2-1))<>Power(2,(2-1)))

update [$(Dimensional)].Dimension.Event
set Event='NA'
from [$(Dimensional)].Dimension.Event
where SourceEventType='Tennis' and event like '%Team1 vs Team2%'
and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(2-1))<>Power(2,(2-1)))
				
-- ================================================================================================================================================================
  --                        PART 2: Update Round Names in Event Dimension for 'Tennis' event type
-- ================================================================================================================================================================
EXEC [Control].Sp_Log	@BatchKey,'SP_Event_Tennis','Part 2','Start',@RowsProcessed = @@RowCount
update [$(Dimensional)].Dimension.Event
set Round=''
where sourceeventtype='Tennis' 
and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(10-1))<>Power(2,(10-1)))			

Update [$(Dimensional)].Dimension.Event
set Round=Round+
					(Case 
						when (isnull(GrandParentEvent,'') like '%Round Robin%' or isnull(ParentEvent,'') like '%Round Robin%' or
									SourceEvent like '%Round Robin%' )
						then ' Round Robin'
						Else ''
					End)
					+(Case 
						when (isnull(GrandParentEvent,'') like '%Qualifying%' or isnull(ParentEvent,'') like '%Qualifying%' or
									SourceEvent like '%Qualifying%' or isnull(GrandParentEvent,'') like '%Qualifier%' or 
									isnull(ParentEvent,'') like '%Qualifier%' or	SourceEvent like '%Qualifier%')
						then ' Qualifying Match'
						Else ''
					End)
					+(Case 
						when (isnull(GrandParentEvent,'') like '%Semi_Final%' or isnull(ParentEvent,'') like '%Semi_Final%' or
									SourceEvent like '%Semi_Final%' )
						then ' Semi Final'
						Else ''
					End)
					+(Case 
						when (isnull(GrandParentEvent,'') like '%Quarter_Final%' or isnull(ParentEvent,'') like '%Quarter_Final%' or
									SourceEvent like '%Quarter_Final%' )
						then ' Quarter Final'
						Else ''
					End)
					+(Case 
						when ((isnull(GrandParentEvent,'') like '%Final[s,^a-r,^t-z]%' or isnull(ParentEvent,'') like '%Final[s,^a-r,^t-z]%' or
								SourceEvent like '%Final[s,^a-r,^t-z]%' or isnull(GrandParentEvent,'') like '%Final' or isnull(ParentEvent,'') like '%Final' or
								SourceEvent like '%Final')and isnull(GrandParentEvent,'') not like '%Quarter_Final%' and 
								isnull(ParentEvent,'') not like '%Quarter_Final%' and	SourceEvent not like '%Quarter_Final%' and 
								isnull(GrandParentEvent,'') not like '%Semi_Final%' and isnull(ParentEvent,'') not like '%Semi_Final%' and
								SourceEvent not like '%Semi_Final%')
						then ' Final'
						Else ''
					End)
					+(Case 
						when (isnull(GrandParentEvent,'') like '%Playoff%' or isnull(ParentEvent,'') like '%Playoff%' or
									SourceEvent like '%Playoff%' or isnull(GrandParentEvent,'') like '%Play off%' or isnull(ParentEvent,'') like '%Play off%' or
									SourceEvent like '%Play off%')
						then ' Playoff'
						Else ''
					End)
								  
				+(Case 
					when (isnull(GrandParentEvent,'') like '%Round [0-9]%' or isnull(ParentEvent,'') like '%Round [0-9]%' or
								SourceEvent like '%Round [0-9]%')
					then ' '+(case
								When (isnull(GrandParentEvent,'') like '%Round%')
								Then 'Round '+SUBString(GrandParentEvent, PATINDEX('%Round%',GrandParentEvent)+6,2)
								When (isnull(ParentEvent,'') like '%Round%')
								Then 'Round '+SUBString(ParentEvent, PATINDEX('%Round%',ParentEvent)+6,2)
								Else 'Round '+SUBString(SourceEvent, PATINDEX('%Round%',SourceEvent)+6,2)
							  End)
					Else ''
					End)
				+(Case 
					when (isnull(GrandParentEvent,'') like '%[0-9][a-z][a-z] Round%' or isnull(ParentEvent,'') like '%[0-9][a-z][a-z] Round%' or
								SourceEvent like '%[0-9][a-z][a-z] Round%')
					then ' '+(case
								When (isnull(GrandParentEvent,'') like '%[0-9][a-z][a-z] Round%')
								Then 'Round '+SUBString(GrandParentEvent, PATINDEX('%[0-9][a-z][a-z] Round%',GrandParentEvent),1)
								When (isnull(ParentEvent,'') like '%[0-9][a-z][a-z] Round%')
								Then 'Round '+SUBString(ParentEvent, PATINDEX('%[0-9][a-z][a-z] Round%',ParentEvent),1)
								Else 'Round '+SUBString(SourceEvent, PATINDEX('%[0-9][a-z][a-z] Round%',SourceEvent),1)
							  End)
					Else ''
					End)
				+(Case 
					when (isnull(GrandParentEvent,'') like '%First Round%' or isnull(ParentEvent,'') like '%First Round%' or
						SourceEvent like '%First Round%' or isnull(GrandParentEvent,'') like '%Second Round%' or isnull(ParentEvent,'') like '%Second Round%' or
						SourceEvent like '%Second Round%' or isnull(GrandParentEvent,'') like '%Third Round%' or isnull(ParentEvent,'') like '%Third Round%' or
						SourceEvent like '%Third Round%' or isnull(GrandParentEvent,'') like '%Fourth Round%' or isnull(ParentEvent,'') like '%Fourth Round%' or
						SourceEvent like '%Fourth Round%' or isnull(GrandParentEvent,'') like '%Fifth Round%' or isnull(ParentEvent,'') like '%Fifth Round%' or
						SourceEvent like '%Fifth Round%' or isnull(GrandParentEvent,'') like '%Sixth Round%' or isnull(ParentEvent,'') like '%Sixth Round%' or
						SourceEvent like '%Sixth Round%')
					then ' '+(case
								When (isnull(GrandParentEvent,'') like '% Round%')
								Then 'Round '+SUBString(GrandParentEvent, PATINDEX('% Round%',GrandParentEvent)-6,6)
								When (isnull(ParentEvent,'') like '% Round%')
								Then 'Round '+SUBString(ParentEvent, PATINDEX('% Round%',ParentEvent)-6,6)
								Else 'Round '+SUBString(SourceEvent, PATINDEX('% Round%',SourceEvent)-6,6)
							  End)
					Else ''
				  End)
				+(Case 
					when (isnull(GrandParentEvent,'') like '%Week [0-9]%' or isnull(ParentEvent,'') like '%Week [0-9]%' or
								SourceEvent like '%Week [0-9]%')
					then ' '+(case
							When (isnull(GrandParentEvent,'') like '%Week [0-9]%')
							Then 'Week '+SUBString(GrandParentEvent, PATINDEX('%Week [0-9]%',GrandParentEvent)+5,2)
							When (isnull(ParentEvent,'') like '%Week [0-9]%')
							Then 'Week '+SUBString(ParentEvent, PATINDEX('%Week [0-9]%',ParentEvent)+5,2)
							Else 'Week '+SUBString(SourceEvent, PATINDEX('%Week [0-9]%',SourceEvent)+5,2)
							End)
					Else ''
					End)
					+(Case 
					when (isnull(GrandParentEvent,'') like '%Day [0-9]%' or isnull(ParentEvent,'') like '%Day [0-9]%' or
								SourceEvent like '%Day [0-9]%')
					then ' '+(case
							When (isnull(GrandParentEvent,'') like '%Day [0-9]%')
							Then 'Day '+SUBString(GrandParentEvent, PATINDEX('%Day [0-9]%',GrandParentEvent)+4,2)
							When (isnull(ParentEvent,'') like '%Day [0-9]%')
							Then 'Day '+SUBString(ParentEvent, PATINDEX('%Day [0-9]%',ParentEvent)+4,2)
							Else 'Day '+SUBString(SourceEvent, PATINDEX('%Day [0-9]%',SourceEvent)+4,2)
							End)
					Else ''
					End)
					
where sourceeventtype='Tennis' 
and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(10-1))<>Power(2,(10-1)))	

update [$(Dimensional)].Dimension.Event
set Round=CASE 
				WHEN Round like '%First%'
				THEN Replace(Round,'first',1)
				WHEN Round like '%Second%'
				THEN Replace(Round,'Second',2)
				WHEN Round like '%Third%'
				THEN Replace(Round,'Third',3)
				WHEN Round like '%Fourth%'
				THEN Replace(Round,'Fourth',4)
				WHEN Round like '%Fifth%'
				THEN Replace(Round,'Fifth',5)
				ELSE Round
			END
from [$(Dimensional)].Dimension.Event
where sourceeventtype='Tennis' 
and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(10-1))<>Power(2,(10-1)))

update [$(Dimensional)].Dimension.Event
set Round=Ltrim(Rtrim(Replace(Replace(Replace(Round,'  ',' '),'-',''),':','')))
where sourceeventtype='Tennis'
and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(10-1))<>Power(2,(10-1)))	

update [$(Dimensional)].Dimension.Event
set Round='Test'
where sourceeventtype='Tennis' and (SourceEvent like '%Test%' or isnull(ParentEvent,'')  like '%Test%'or isnull(GrandParentEvent,'')  like '%Test%') 
and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(10-1))<>Power(2,(10-1)))

update [$(Dimensional)].Dimension.Event
set Round='Unknown'
where sourceeventtype='Tennis' and Round=''
and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(10-1))<>Power(2,(10-1)))

-- ================================================================================================================================================================
  --                        PART 3: Update Competition Names in Event Dimension for 'Tennis' event type
-- ================================================================================================================================================================
EXEC [Control].Sp_Log	@BatchKey,'SP_Event_Tennis','Part 3','Start',@RowsProcessed = @@RowCount
Update [$(Dimensional)].Dimension.Event
set competition=(Case
						when (SourceEvent like '%Test%' or isnull(ParentEvent,'')  like '%Test%'or isnull(GrandParentEvent,'')  like '%Test%') 
						then 'Test'
						when (SourceEvent like '%Team[0-9]%' or isnull(ParentEvent,'')  like '%Team[0-9]%'or isnull(GrandParentEvent,'')  like '%Team[0-9]%') 
						then 'NA'
						--When (isnull(GrandParentEvent,'')='Live Betting' or isnull(ParentEvent,'') like '%Live Betting%' or SourceEvent like '%Live Betting%')
						--Then 'Unknown'
						When(GrandParentEvent is not null)
						Then( Case
								When (GrandParentEvent like'% - % - %' and GrandParentEvent like '%Matches')
								then Substring(GrandParentEvent,1,PatIndex('%Matches%',GrandParentEvent)-4)
								When (GrandParentEvent like'% - % - %' and GrandParentEvent like '%Live Betting')
								then Substring(GrandParentEvent,1,PatIndex('%Live Betting%',GrandParentEvent)-4)
								When GrandParentEvent like'% - % - %'
								then Substring(GrandParentEvent,1,PatIndex('% - %',GrandParentEvent)-1)
								When GrandParentEvent like '% - %' and GrandParentEvent not like '%season specials%'
								then Substring(GrandParentEvent,1,PatIndex('% - %',GrandParentEvent)-1)
								When GrandParentEvent like '%Round [1-9]%'
								Then Substring(GrandParentEvent,1,PATINDEX('%Round [1-9]%',GrandParentEvent)-1)
								When GrandParentEvent like '%[1-9][a-z][a-z] Round%'
								Then Substring(GrandParentEvent,1,PATINDEX('%[1-9][a-z][a-z] Round%',GrandParentEvent)-1)
								When GrandParentEvent like '%Semi Final%'
								Then Substring(GrandParentEvent,1,PATINDEX('%Semi Final%',GrandParentEvent)-1)
								When GrandParentEvent like '%Quarter Final%'
								Then Substring(GrandParentEvent,1,PATINDEX('%Quarter Final%',GrandParentEvent)-1)
								When GrandParentEvent like '%Final%'
								Then Substring(GrandParentEvent,1,PATINDEX('%Final%',GrandParentEvent)-1)
								When GrandParentEvent like '%Qualifying%'
								Then Substring(GrandParentEvent,1,PATINDEX('%Qualifying%',GrandParentEvent)-1)
								Else GrandParentEvent
							  End)
							When(ParentEvent is not null)
							Then	(Case
										When ParentEvent like '% - %'  and ParentEvent not like '% vs %' and ParentEvent not like '% v %'
										 and ParentEvent not like '% To %'  and ParentEvent not like '%season specials%'
										and ParentEvent not like '%win%' and ParentEvent not like '%lose%' and ParentEvent not like '%total%'
										then Substring(ParentEvent,1,PatIndex('% - %',ParentEvent)-1)
										When ParentEvent like '%Round [1-9]%'
										Then Substring(ParentEvent,1,PATINDEX('%Round [1-9]%',ParentEvent)-1)
										When ParentEvent like '%[1-9][a-z][a-z] Round%'
										Then Substring(ParentEvent,1,PATINDEX('%[1-9][a-z][a-z] Round%',ParentEvent)-1)
										When ParentEvent like '%Semi Final%'
										Then Substring(ParentEvent,1,PATINDEX('%Semi Final%',ParentEvent)-1)
										When ParentEvent like '%Quarter Final%'
										Then Substring(ParentEvent,1,PATINDEX('%Quarter Final%',ParentEvent)-1)
										When ParentEvent like '%Final%'
										Then Substring(ParentEvent,1,PATINDEX('%Final%',ParentEvent)-1)
										When ParentEvent like '%Qualifying%'
										Then Substring(ParentEvent,1,PATINDEX('%Qualifying%',ParentEvent)-1)
										When ParentEvent<>''  and ParentEvent not like '% vs %'  and ParentEvent not like '% v %'
										and ParentEvent not like '% To %'
										and ParentEvent not like '%win%' and ParentEvent not like '%lose%' and ParentEvent not like '%total%'
										and ParentEvent not like '% is %'
										then ParentEvent
										Else 'Unknown'
									ENd)
							Else(Case
									When SourceEvent like '% - %' and SourceEvent not like '% vs %'  and SourceEvent not like '% v %'and SourceEvent not like '% To %'
									and SourceEvent not like '%win%' and SourceEvent not like '%lose%' and SourceEvent not like '%total%'
									then Substring(SourceEvent,1,PatIndex('% - %',SourceEvent)-1)
									When SourceEvent like '%Round [1-9]%'
									Then Substring(SourceEvent,1,PATINDEX('%Round [1-9]%',SourceEvent)-1)
									When SourceEvent like '%[1-9][a-z][a-z] Round%'
									Then Substring(SourceEvent,1,PATINDEX('%[1-9][a-z][a-z] Round%',SourceEvent)-1)
									When SourceEvent like '%Semi Final%'
									Then Substring(SourceEvent,1,PATINDEX('%Semi Final%',SourceEvent)-1)
									When SourceEvent like '%Quarter Final%'
									Then Substring(SourceEvent,1,PATINDEX('%Quarter Final%',SourceEvent)-1)
									When SourceEvent like '%Final%'
									Then Substring(SourceEvent,1,PATINDEX('%Final%',SourceEvent)-1)
									When SourceEvent like '%Qualifying%'
									Then Substring(SourceEvent,1,PATINDEX('%Qualifying%',SourceEvent)-1)
									When SourceEvent<>''  and SourceEvent not like '% vs %' and SourceEvent not like '% v %'  and SourceEvent not like '% To %'
									and SourceEvent not like '%win%' and SourceEvent not like '%lose%' and SourceEvent not like '%total%'
									and SourceEvent not like '% is %'
									then SourceEvent
									Else 'Unknown'
									End)
							End)
where SourceEventType='Tennis'
and  ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1)));
		
Update [$(Dimensional)].Dimension.Event
set Competition=(Case
						When Competition like '%|%'
						Then Substring(Competition,1,PATINDEX('%|%',Competition)-1)
						When Competition='Live Tennis' or Competition=''
						Then 'Unknown'
						When Competition like '%*%'
						Then Substring(Competition,1,PATINDEX('%*%',Competition)-1)
						When Competition like '%qualifier%'
						Then Substring(Competition,1,PATINDEX('%qualifier%',Competition)-1)
						When Competition like '%final%'
						Then Substring(Competition,1,PATINDEX('%final%',Competition)-1)
						When Competition like '%playoff%' or Competition like '%play off%'
						Then Substring(Competition,1,PATINDEX('%play%off%',Competition)-1)
						When Competition='Tennis' and 
						(SourceEvent like '%ITF%' or isnull(ParentEvent,'') like '%ITF%' or isnull(GrandParentEvent,'') like '%ITF%')
						Then 'ITF'
						When Competition like '%Live %'
						Then Substring(Competition,1,PATINDEX('%Live %',Competition)-1)
						Else Ltrim(Rtrim(Replace(Competition,'Live','')))
					 End)
where SourceEventType='Tennis'
and  ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1)))

Update [$(Dimensional)].Dimension.Event
set Competition='Unknown'
where SourceEventType='Tennis' and (Competition like'%vs%' or Competition like'%&%' or Competition like'% v %'
 or Competition like'%/%' or Competition like'%,%')
and  ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1))) 

--Removing round information from competition 
Update [$(Dimensional)].Dimension.Event
set Competition=(Case 
						When(Competition like '%Rd [1-9]%')
						Then Substring(Competition,1,PATINDEX('%Rd [1-9]%',Competition)-1)
						When(Competition like '% R[1-9]%')
						Then Substring(Competition,1,PATINDEX('% R[1-9]%',Competition))
						When(Competition like '%[1-9][a-z[a-z] %')
						Then Substring(Competition,1,PATINDEX('%[1-9][a-z[a-z] %',Competition)-1)
						When(Competition like '%betradar%')
						Then Substring(Competition,1,PATINDEX('%betradar%',Competition)-1)
						Else Competition
					  End)
where SourceEventType='Tennis' 
and  ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1))) 

--Removing trailing '-' from competition 
Update [$(Dimensional)].Dimension.Event
set Competition=(Case 
						When(Competition like '%-')
						Then Replace(Substring(Competition,1,Len(Competition)-1),'  ',' ')
						When(Competition like '%- ')
						Then Replace(Substring(Competition,1,Len(Competition)-2),'  ',' ')
						Else Replace(Competition,'  ',' ')
					  End)
where SourceEventType='Tennis' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (Competition like '%-' or Competition like '%- ' or  Competition like '  ')
 and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1)))

 --Removing single year numbers from competition 
Update [$(Dimensional)].Dimension.Event
set Competition= Substring(Competition,1,PATINDEX('%[1-9][0-9][0-9][0-9]%',Competition)-1)+Substring(Competition,PATINDEX('%[1-9][0-9][0-9][0-9]%',Competition)+4,Len(Competition))
where SourceEventType='Tennis' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and Competition like '%[1-9][0-9][0-9][0-9]%'and Competition not like '%[1-9][0-9][0-9][0-9]/[0-9]%'
 and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1)))
 
Update [$(Dimensional)].Dimension.Event
set Competition='NA'
where SourceEventType='Tennis'
and  ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1))) and ((Competition not like '% %' and Competition<>'NA'  and 
Competition<>'Unknown' and Competition<>'Test' and Competition<>'ITF' and Competition<>'ATP' and Competition<>'Wimbledon' and Competition<>'WTA') or
Competition like '%who %' or Competition like '%do %' or Competition like '%xxx%' or Competition like '%top %' or Competition like '%total %' or
Competition like '%best %' or Competition like '%first %' or Competition like '%"%')

Update [$(Dimensional)].Dimension.Event
set Competition='unknown'
where SourceEventType='Tennis' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) 
 and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1))) and competition=''

Update [$(Dimensional)].Dimension.Event
set Competition=Ltrim(Rtrim(Competition))
where SourceEventType='Tennis' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey)  and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1)))

COMMIT TRANSACTION Event_Tennis ;	

-- ================================================================================================================================================================
  --                        PART 3: Update ModifiedBy and ModifiedDate in Event Dimension for 'Tennis' event type
-- ================================================================================================================================================================
EXEC [Control].Sp_Log	@BatchKey,'SP_Event_Tennis','Part 3','Start',@RowsProcessed = @@RowCount
Update [$(Dimensional)].Dimension.Event
set ModifiedBy='SP_Event_Tennis',
	ModifiedDate=CURRENT_TIMESTAMP
where ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and SourceEventType='Tennis'
EXEC [Control].Sp_Log	@BatchKey,'SP_Event_Tennis',NULL,'Success',@RowsProcessed = @@RowCount
			
END TRY
BEGIN CATCH
	Rollback TRANSACTION Event_Tennis;
	declare @Error int;
	declare @ErrorMessage varchar(max);
	                           -- Log the failure at the object level
                           SET    @Error = ERROR_NUMBER()
                           SET    @ErrorMessage = ERROR_MESSAGE()
						   EXEC [Control].Sp_Log @BatchKey, 'SP_Event_Tennis', NULL, 'Failed', @ErrorMessage;
						   Raiserror(@ErrorMessage,16,1)

END CATCH

END

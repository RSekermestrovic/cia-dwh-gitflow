﻿CREATE proc [Cleanse].[Sp_Account_Name]
(
	@BatchKey int = 0,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @RowCount INT;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

BEGIN TRY

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Account_Name', 'Cache', 'Start'

	SELECT	AccountKey,
			AccountSurname,
			AccountFirstName,
			AccountMiddleName,
			AccountTitle,
			AccountGender,
			AccountDOB,
			Surname,
			FirstName,
			MiddleName,
			Title,
			Gender,
			DOB
	INTO	#Accounts
	FROM	[$(Dimensional)].Dimension.Account
	WHERE	ModifiedBatchKey = CASE @BatchKey WHEN 0 THEN ModifiedBatchKey ELSE @BatchKey END

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Account_Name', 'Surname', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	u
	SET		Surname = UpdatedSurname
	FROM	(	SELECT	Surname,
						CASE ISNULL(AccountSurname,'')
							WHEN '' THEN ''
							WHEN ' ' THEN ''
							WHEN 'N/A' THEN ''
							WHEN 'Unknown' THEN ''
							ELSE Cleanse.[CamelCase](Cleanse.RemoveNumbers(Replace(AccountSurname,'''','''')))
						END UpdatedSurname
				FROM	#Accounts
			) u
	WHERE	ISNULL(Surname,'!') <> UpdatedSurname

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Account_Name', 'First Name', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	u
	SET		FirstName = UpdatedFirstname
	FROM	(	SELECT	FirstName,
						CASE ISNULL(AccountFirstName,'')
							WHEN '' THEN ''
							WHEN ' ' THEN ''
							WHEN 'N/A' THEN ''
							WHEN 'Unknown' THEN ''
							ELSE Cleanse.[CamelCase](Cleanse.RemoveNumbers(Replace(AccountFirstName,'''','''')))
						END UpdatedFirstname
				FROM	#Accounts
			) u
	WHERE	ISNULL(FirstName,'!') <> UpdatedFirstName

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Account_Name', 'MiddleName', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	u
	SET		MiddleName= UpdatedMiddleName
	FROM	(	SELECT	MiddleName,
						CASE ISNULL(AccountMiddleName,'')
							WHEN '' THEN ''
							WHEN ' ' THEN ''
							WHEN 'N/A' THEN ''
							WHEN 'Unknown' THEN ''
							ELSE Cleanse.[CamelCase](Cleanse.RemoveNumbers(Replace(AccountMiddleName,'''','''')))
						END UpdatedMiddleName
				FROM	#Accounts
			) u
	WHERE	ISNULL(MiddleName,'!') <> UpdatedMiddleName

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Account_Name', 'Gender', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	x
	SET		x.Gender = y.Gender
	FROM	(	SELECT	a.AccountKey,
						a.AccountFirstName,
						a.Gender,
						CASE a.AccountGender
							WHEN 'M' THEN 'M'
							WHEN 'F' THEN 'F'
							ELSE 'U'
						END AccountGender,
						CASE REPLACE(a.AccountTitle,'.','') 
							WHEN 'Mr' THEN 'M'
							WHEN 'Ms' THEN 'F'
							WHEN 'Mrs' THEN 'F'
							WHEN 'Miss' THEN 'F'
							ELSE 'U'
						END TitleGender,
						ISNULL(r.Gender,'U') NameGender
				FROM	#Accounts a
						LEFT JOIN Reference.GenderLookup r on r.Name = a.AccountFirstName
			) x
			INNER JOIN	(	SELECT	'F' AccountGender, 'F' TitleGender, 'F' NameGender, 'F' Gender UNION ALL
							SELECT	'F' AccountGender, 'F' TitleGender, 'M' NameGender, 'F' Gender UNION ALL
							SELECT	'F' AccountGender, 'F' TitleGender, 'U' NameGender, 'F' Gender UNION ALL
							SELECT	'F' AccountGender, 'M' TitleGender, 'F' NameGender, 'F' Gender UNION ALL
							SELECT	'F' AccountGender, 'M' TitleGender, 'M' NameGender, 'M' Gender UNION ALL
							SELECT	'F' AccountGender, 'M' TitleGender, 'U' NameGender, 'F' Gender UNION ALL
							SELECT	'F' AccountGender, 'U' TitleGender, 'F' NameGender, 'F' Gender UNION ALL
							SELECT	'F' AccountGender, 'U' TitleGender, 'M' NameGender, 'F' Gender UNION ALL
							SELECT	'F' AccountGender, 'U' TitleGender, 'U' NameGender, 'F' Gender UNION ALL
							SELECT	'M' AccountGender, 'F' TitleGender, 'F' NameGender, 'F' Gender UNION ALL
							SELECT	'M' AccountGender, 'F' TitleGender, 'M' NameGender, 'M' Gender UNION ALL
							SELECT	'M' AccountGender, 'F' TitleGender, 'U' NameGender, 'M' Gender UNION ALL
							SELECT	'M' AccountGender, 'M' TitleGender, 'F' NameGender, 'M' Gender UNION ALL
							SELECT	'M' AccountGender, 'M' TitleGender, 'M' NameGender, 'M' Gender UNION ALL
							SELECT	'M' AccountGender, 'M' TitleGender, 'U' NameGender, 'M' Gender UNION ALL
							SELECT	'M' AccountGender, 'U' TitleGender, 'F' NameGender, 'M' Gender UNION ALL
							SELECT	'M' AccountGender, 'U' TitleGender, 'M' NameGender, 'M' Gender UNION ALL
							SELECT	'M' AccountGender, 'U' TitleGender, 'U' NameGender, 'M' Gender UNION ALL
							SELECT	'U' AccountGender, 'F' TitleGender, 'F' NameGender, 'F' Gender UNION ALL
							SELECT	'U' AccountGender, 'F' TitleGender, 'M' NameGender, 'F' Gender UNION ALL
							SELECT	'U' AccountGender, 'F' TitleGender, 'U' NameGender, 'F' Gender UNION ALL
							SELECT	'U' AccountGender, 'M' TitleGender, 'F' NameGender, 'M' Gender UNION ALL
							SELECT	'U' AccountGender, 'M' TitleGender, 'M' NameGender, 'M' Gender UNION ALL
							SELECT	'U' AccountGender, 'M' TitleGender, 'U' NameGender, 'M' Gender UNION ALL
							SELECT	'U' AccountGender, 'U' TitleGender, 'F' NameGender, 'F' Gender UNION ALL
							SELECT	'U' AccountGender, 'U' TitleGender, 'M' NameGender, 'M' Gender UNION ALL
							SELECT	'U' AccountGender, 'U' TitleGender, 'U' NameGender, 'U' Gender
						) y ON x.AccountGender = y.AccountGender AND x.TitleGender = y.TitleGender AND x.NameGender = y.NameGender
	WHERE	ISNULL(x.Gender,'!') <> y.Gender

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Account_Name', 'Title', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	u
	SET		Title = Updatedtitle
	FROM	(	SELECT	Title,
						CASE
							WHEN UpdatedTitle IN ('Mr','Ms','Mrs','Dr','Miss') THEN UpdatedTitle
							WHEN Gender = 'M' THEN 'Mr'
							WHEN Gender = 'F' THEN 'Ms'
							ELSE 'Unknown'
						END UpdatedTitle
				FROM	(	SELECT	Gender,
									Title,
									REPLACE(AccountTitle,'.','') UpdatedTitle
							FROM	#Accounts
						) x
			) u
	WHERE	ISNULL(Title,'!') <> UpdatedTitle
	
	EXEC [Control].Sp_Log @BatchKey, 'Sp_Account_Name', 'DOB', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	#Accounts
	SET		DOB = AccountDOB
	WHERE	ISNULL(DOB,CONVERT(date,'1900-01-01')) <> ISNULL(AccountDOB,CONVERT(date,'1900-01-01'))
	
	EXEC [Control].Sp_Log @BatchKey, 'Sp_Account_Name', 'Update', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	a
	SET		Surname = u.Surname,
			FirstName = u.FirstName,
			MiddleName = u.MiddleName,
			Title = u.Title,
			Gender = u.Gender,
			DOB = u.DOB
	FROM	[$(Dimensional)].Dimension.Account a
			INNER JOIN #Accounts u ON u.AccountKey = a.AccountKey
	WHERE	ISNULL(a.Surname,'!') <> ISNULL(u.Surname,'!')
			OR ISNULL(a.FirstName,'!') <> ISNULL(u.FirstName,'!')
			OR ISNULL(a.MiddleName,'!') <> ISNULL(u.MiddleName,'!')
			OR ISNULL(a.Title,'!') <> ISNULL(u.Title,'!')
			OR ISNULL(a.Gender,'!') <> ISNULL(u.Gender,'!')
			OR ISNULL(a.DOB,CONVERT(date,'1900-01-01')) <> ISNULL(u.DOB,CONVERT(date,'1900-01-01'))

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Account_Name', NULL, 'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Account_Name', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

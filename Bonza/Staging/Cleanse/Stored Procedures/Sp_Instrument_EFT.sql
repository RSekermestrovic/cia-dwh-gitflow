CREATE proc [Cleanse].[Sp_Instrument_EFT]
(
	@BatchKey int = 0,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @RowCount INT; 

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

BEGIN TRY

	EXEC [Control].Sp_Log @BatchKey, 'Cleanse.Sp_Instrument_EFT', 'Cache', 'Start'

	SELECT	InstrumentKey,
			AccountNumber,
			SafeAccountNumber,
			AccountName,
			BSB,
			ProviderName,
			ProviderKey
			
	INTO	#Instrument
	FROM	[$(Dimensional)].Dimension.Instrument 
	WHERE	ModifiedBatchKey = CASE @BatchKey WHEN 0 THEN ModifiedBatchKey ELSE @BatchKey END
	AND Source ='EFT'



	EXEC [Control].Sp_Log @BatchKey, 'Cleanse.Sp_Instrument_EFT', 'Account Number', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	u
	SET		AccountNumber = UpdatedAccountNumber
	FROM	(	SELECT	AccountNumber,
						CASE ISNULL(AccountNumber,'')
							WHEN '' THEN ''
							WHEN ' ' THEN ''
							WHEN 'N/A' THEN ''
							WHEN 'Unknown' THEN ''
							ELSE Cleanse.ExtractNumbersOnly(Replace(AccountNumber,'''',''''))
						END UpdatedAccountNumber
				FROM	#Instrument
			) u
	WHERE	ISNULL(AccountNumber,'!') <> UpdatedAccountNumber




	EXEC [Control].Sp_Log @BatchKey, 'Cleanse.Sp_Instrument_EFT', 'Safe Account Number', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	u
	SET		SafeAccountNumber = isnull((Replicate('*',Len(UpdatedSafeAccountNumber)-4)+Right(UpdatedSafeAccountNumber,4)),'')
	FROM	(	SELECT	SafeAccountNumber,
						CASE ISNULL(SafeAccountNumber,'')
							WHEN '' THEN ''
							WHEN ' ' THEN ''
							WHEN 'N/A' THEN ''
							WHEN 'Unknown' THEN ''
							ELSE Cleanse.ExtractNumbersOnly(Replace(AccountNumber,'''',''''))
						END UpdatedSafeAccountNumber
				FROM	#Instrument
			) u
	WHERE	ISNULL(SafeAccountNumber,'!') <> UpdatedSafeAccountNumber




	EXEC [Control].Sp_Log @BatchKey, 'Cleanse.Sp_Instrument_EFT', 'Account Name', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	u
	SET		AccountName= UpdatedAccountName
	FROM	(	SELECT	AccountName,
						CASE ISNULL(AccountName,'')
							WHEN '' THEN ''
							WHEN ' ' THEN ''
							WHEN 'N/A' THEN ''
							WHEN 'Unknown' THEN ''
							ELSE Cleanse.[CamelCase](Cleanse.RemoveNumbers(Replace(AccountName,'''','''')))
						END UpdatedAccountName
				FROM	#Instrument
			) u
	WHERE	ISNULL(AccountName,'!') <> UpdatedAccountName




	EXEC [Control].Sp_Log @BatchKey, 'Cleanse.Sp_Instrument_EFT', 'BSB', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	u
	SET		BSB= UpdatedBSB
	FROM	(	SELECT	BSB,
						CASE 						
							WHEN ISNULL(BSB,'')	= '' THEN ''
							WHEN ISNULL(BSB,'')	= ' ' THEN ''
							WHEN ISNULL(BSB,'')	= 'N/A' THEN ''
							WHEN ISNULL(BSB,'')	= 'Unknown' THEN ''
							WHEN ISNULL(BSB,'')	not like '%[a-z,.,?]%' AND  Len(Replace(Replace(ISNULL(BSB,''),'-',''),' ','')) <= 6 THEN 
								    (CASE
										WHEN (Replace(Replace(ISNULL(BSB,''),'-',''),' ','') like '[0-9][0-9][0-9][0-9][0-9][0-9]') THEN Replace(Replace(ISNULL(BSB,''),'-',''),' ','')
										WHEN (Replace(Replace(ISNULL(BSB,''),'-',''),' ','') like '[0-9][0-9][0-9][0-9][0-9]') THEN '0' + Replace(Replace(ISNULL(BSB,''),'-',''),' ','')
										ELSE ''
									End)
						END UpdatedBSB
				FROM	#Instrument
			) u
	WHERE	ISNULL(BSB,'!') <> UpdatedBSB



	
	EXEC [Control].Sp_Log @BatchKey, 'Cleanse.Sp_Instrument_EFT', 'Provider Name', 'Start', @RowsProcessed = @@ROWCOUNT
	
	UPDATE	u
	SET		ProviderName= UpdatedProviderName
	FROM	(	SELECT	ProviderName,
						CASE 		
						     WHEN ISNULL(ProviderName,'') = '' THEN ''
							WHEN ISNULL(ProviderName,'') = ' ' THEN ''
							WHEN ISNULL(ProviderName,'') = 'N/A' THEN ''
							WHEN ISNULL(ProviderName,'') = 'Unknown' THEN ''
							WHEN (Replace(Replace(Replace(ProviderName,'*',''),0,''),']','')) in ('',' ') THEN ''
							WHEN pn.BankName IS NOT NULL THEN pn.BankName
							ELSE (Replace(Replace(Replace(ProviderName,'*',''),0,''),']',''))											
						END UpdatedProviderName
				FROM	#Instrument I
				LEFT OUTER JOIN Reference.ProviderNames_BSB pn WITH (NOLOCK)
				on (pn.bsb  = (SELECT CASE WHEN ISNULL(rtrim(Ltrim(I.BSB)),'') NOT LIKE '%[a-z,.,?]%' 
								       AND ISNULL(rtrim(Ltrim(I.BSB)),'') NOT LIKE '%[^a-zA-Z0-9]%'
				                       AND ISNULL(rtrim(Ltrim(I.BSB)),'') <> '' 
				                       AND LEN(ISNULL(rtrim(Ltrim(I.BSB)),'')) > 3   
									   AND ISNULL(rtrim(Ltrim(I.BSB)),'') <> '0' 
									  THEN  
										  CASE WHEN SUBSTRING(I.BSB,1,3) in (select CAST(bsb as varchar(MAX)) from Reference.ProviderNames_BSB) THEN CAST(SUBSTRING(I.BSB,1,3) AS INT)
										  ELSE CAST(SUBSTRING(I.BSB,1,2) AS INT)
										  END
								 ELSE 0
								 END))

			) u
	WHERE	ISNULL(ProviderName,'!') <> UpdatedProviderName


	
	EXEC [Control].Sp_Log @BatchKey, 'Cleanse.Sp_Instrument_EFT', 'Provider Key', 'Start', @RowsProcessed = @@ROWCOUNT


	UPDATE	u
	SET		ProviderKey = UpdatedProviderKey
	FROM	(	SELECT	ProviderKey,
						CASE ProviderName		
							WHEN 'Unknown' THEN -1
							WHEN 'N/A'  THEN 0
							ELSE  (SELECT ISNULL(MAX(A.ProviderKey),0) FROM [$(Dimensional)].Dimension.Instrument AS A)+
						DENSE_RANK() OVER (PARTITION BY ProviderKey ORDER BY ProviderName)						
						END UpdatedProviderKey
			FROM	#Instrument
			) u
	WHERE	ISNULL(ProviderKey,-2) <> UpdatedProviderKey


    EXEC [Control].Sp_Log @BatchKey, 'Cleanse.Sp_Instrument_EFT', 'Update', 'Start', @RowsProcessed = @@ROWCOUNT


	UPDATE	i
	SET		AccountNumber = u.AccountNumber,
			SafeAccountNumber = u.SafeAccountNumber,
			AccountName = u.AccountName,
			BSB = u.BSB,
			ProviderName = u.ProviderName,
			ProviderKey = u.ProviderKey
	FROM	[$(Dimensional)].Dimension.Instrument i
			INNER JOIN #Instrument u ON u.InstrumentKey = i.InstrumentKey
	WHERE	ISNULL(i.AccountNumber,'!') <> ISNULL(u.AccountNumber,'!')
			OR ISNULL(i.SafeAccountNumber,'!') <> ISNULL(u.SafeAccountNumber,'!')
			OR ISNULL(i.AccountName,'!') <> ISNULL(u.AccountName,'!')
			OR ISNULL(i.BSB,'!') <> ISNULL(u.BSB,'!')
			OR ISNULL(i.ProviderName,'!') <> ISNULL(u.ProviderName,'!')
			OR ISNULL(i.ProviderKey,-2) <> ISNULL(u.ProviderKey,-2)

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey, 'Cleanse.Sp_Instrument_EFT', NULL, 'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Cleanse.Sp_Instrument_EFT', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

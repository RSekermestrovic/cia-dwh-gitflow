﻿CREATE proc [Cleanse].[Sp_Account_Email]
(
	@BatchKey int = 0,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @RowCount INT;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

BEGIN TRY

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Account_Email', 'Update', 'Start'

	UPDATE	u
	SET		Email = UpdatedEmail
	FROM	(	SELECT	Email,		
						CASE	
							WHEN REPLACE(HomeEmail,' ','') LIKE '%nomail%' THEN 'N/A'
							WHEN HomeEmail LIKE '%_@_%_.__%' THEN LOWER(HomeEmail)
							WHEN HomeAlternateEmail LIKE '%_@_%_.__%' THEN LOWER(HomeAlternateEmail)
							WHEN PostalEmail LIKE '%_@_%_.__%' THEN LOWER(PostalEmail)
							WHEN PostalAlternateEmail LIKE '%_@_%_.__%' THEN LOWER(PostalAlternateEmail)
							WHEN BusinessEmail LIKE '%_@_%_.__%' THEN LOWER(BusinessEmail)
							WHEN BusinessAlternateEmail LIKE '%_@_%_.__%' THEN LOWER(BusinessAlternateEmail)
							WHEN OtherEmail LIKE '%_@_%_.__%' THEN LOWER(OtherEmail)
							WHEN OtherAlternateEmail LIKE '%_@_%_.__%' THEN LOWER(OtherAlternateEmail)
							ELSE 'Unknown'
						END UpdatedEmail
				FROM	[$(Dimensional)].Dimension.Account
				WHERE	ModifiedBatchKey = CASE @BatchKey WHEN 0 THEN ModifiedBatchKey ELSE @BatchKey END
			) u
	WHERE	CONVERT(varbinary,ISNULL(Email,'!')) <> CONVERT(varbinary,ISNULL(UpdatedEmail,'!')) -- Case sensitive and more generic than nominating collations!
	
	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Account_Email', NULL, 'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Account_Email', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

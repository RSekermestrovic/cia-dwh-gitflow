CREATE proc [Cleanse].[Sp_Instrument_Type]
(
	@BatchKey int = 0,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @RowCount INT;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

BEGIN TRY


	EXEC [Control].Sp_Log @BatchKey, 'Cleanse.Sp_Instrument_Type', 'Update', 'Start';

	UPDATE	u
	SET		InstrumentTypeKey= UpdatedInstrumentTypeKey
	FROM	(	SELECT	InstrumentTypeKey,
						CASE InstrumentType		
							WHEN 'Unknown' THEN -1
							WHEN 'N/A'  THEN 0
							ELSE  (SELECT ISNULL(MAX(A.InstrumentTypeKey),0) FROM [$(Dimensional)].Dimension.Instrument AS A)+
						DENSE_RANK() OVER (PARTITION BY InstrumentTypeKey ORDER BY InstrumentType)						
						END UpdatedInstrumentTypeKey
			FROM	[$(Dimensional)].Dimension.Instrument
			WHERE	ModifiedBatchKey = CASE @BatchKey WHEN 0 THEN ModifiedBatchKey ELSE @BatchKey END
			) u
	WHERE	ISNULL(InstrumentTypeKey,'!') <> UpdatedInstrumentTypeKey


	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey, 'Cleanse.Sp_Instrument_Type', NULL, 'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Cleanse.Sp_Instrument_Type', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

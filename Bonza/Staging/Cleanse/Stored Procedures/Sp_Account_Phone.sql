﻿CREATE proc [Cleanse].[Sp_Account_Phone]
(
	@BatchKey int = 0,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @RowCount INT;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

BEGIN TRY

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Account_Phone', 'Cache', 'Start'

	SELECT	AccountKey,
			Phone,
			Mobile,
			Fax,
			HomePhone,
			HomeMobile,
			HomeFax,
			HomeState,
			HomeCountry,
			PostalPhone,
			PostalMobile,
			PostalFax,
			PostalState,
			PostalCountry,
			BusinessPhone,
			BusinessMobile,
			BusinessFax,
			BusinessState,
			BusinessCountry,
			OtherPhone,
			OtherMobile,
			OtherFax,
			OtherState,
			OtherCountry
	INTO	#Accounts
	FROM	[$(Dimensional)].Dimension.Account
	WHERE	ModifiedBatchKey = CASE @BatchKey WHEN 0 THEN ModifiedBatchKey ELSE @BatchKey END

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Account_Phone', 'Phone', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	u
	SET		Phone = UpdatedPhone
	FROM	(	SELECT	Phone,
						CASE
							WHEN HomePhone NOT IN ('Invalid','Unknown') THEN HomePhone
							WHEN HomeMobile NOT IN ('Invalid','Unknown') THEN HomeMobile
							WHEN PostalPhone NOT IN ('Invalid','Unknown') THEN PostalPhone
							WHEN PostalMobile NOT IN ('Invalid','Unknown') THEN PostalMobile
							WHEN BusinessPhone NOT IN ('Invalid','Unknown') THEN BusinessPhone
							WHEN BusinessMobile NOT IN ('Invalid','Unknown') THEN BusinessMobile
							WHEN OtherPhone NOT IN ('Invalid','Unknown') THEN OtherPhone
							WHEN OtherMobile NOT IN ('Invalid','Unknown') THEN OtherMobile
							WHEN HomePhone  = 'Invalid' THEN 'Invalid'
							WHEN PostalPhone  = 'Invalid' THEN 'Invalid'
							WHEN BusinessPhone  = 'Invalid' THEN 'Invalid'
							WHEN OtherPhone  = 'Invalid' THEN 'Invalid'
							ELSE 'Unknown'
						END UpdatedPhone
				FROM	(	SELECT	Phone,
									[Cleanse].[FormatPhoneNumber](HomeMobile, HomeState, HomeCountry, 0) HomeMobile,
									[Cleanse].[FormatPhoneNumber](HomePhone, HomeState, HomeCountry, 0) HomePhone,
									[Cleanse].[FormatPhoneNumber](PostalMobile, PostalState, PostalCountry, 0) PostalMobile,
									[Cleanse].[FormatPhoneNumber](PostalPhone, PostalState, PostalCountry, 0) PostalPhone,
									[Cleanse].[FormatPhoneNumber](BusinessMobile, BusinessState, BusinessCountry, 0) BusinessMobile,
									[Cleanse].[FormatPhoneNumber](BusinessPhone, BusinessState, BusinessCountry, 0) BusinessPhone,
									[Cleanse].[FormatPhoneNumber](OtherMobile, OtherState, OtherCountry, 0) OtherMobile,
									[Cleanse].[FormatPhoneNumber](OtherPhone, OtherState, OtherCountry, 0) OtherPhone
							FROM	#Accounts
						) x 
			) u
	WHERE	ISNULL(Phone,'!') <> UpdatedPhone

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Account_Phone', 'Mobile', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	u
	SET		Mobile = UpdatedMobile
	FROM	(	SELECT	Mobile,
						CASE
							WHEN HomeMobile NOT IN ('Invalid','Unknown') THEN HomeMobile
							WHEN HomePhone NOT IN ('Invalid','Unknown') THEN HomePhone
							WHEN PostalMobile NOT IN ('Invalid','Unknown') THEN PostalMobile
							WHEN PostalPhone NOT IN ('Invalid','Unknown') THEN PostalPhone
							WHEN BusinessMobile NOT IN ('Invalid','Unknown') THEN BusinessMobile
							WHEN BusinessPhone NOT IN ('Invalid','Unknown') THEN BusinessPhone
							WHEN OtherMobile NOT IN ('Invalid','Unknown') THEN OtherMobile
							WHEN OtherPhone NOT IN ('Invalid','Unknown') THEN OtherPhone
							WHEN HomeMobile  = 'Invalid' THEN 'Invalid'
							WHEN PostalMobile  = 'Invalid' THEN 'Invalid'
							WHEN BusinessMobile  = 'Invalid' THEN 'Invalid'
							WHEN OtherMobile  = 'Invalid' THEN 'Invalid'
							ELSE 'Unknown'
						END UpdatedMobile
				FROM	(	SELECT	Mobile,
									[Cleanse].[FormatPhoneNumber](HomeMobile, HomeState, HomeCountry, 1) HomeMobile,
									[Cleanse].[FormatPhoneNumber](HomePhone, HomeState, HomeCountry, 1) HomePhone,
									[Cleanse].[FormatPhoneNumber](PostalMobile, PostalState, PostalCountry, 1) PostalMobile,
									[Cleanse].[FormatPhoneNumber](PostalPhone, PostalState, PostalCountry, 1) PostalPhone,
									[Cleanse].[FormatPhoneNumber](BusinessMobile, BusinessState, BusinessCountry, 1) BusinessMobile,
									[Cleanse].[FormatPhoneNumber](BusinessPhone, BusinessState, BusinessCountry, 1) BusinessPhone,
									[Cleanse].[FormatPhoneNumber](OtherMobile, OtherState, OtherCountry, 1) OtherMobile,
									[Cleanse].[FormatPhoneNumber](OtherPhone, OtherState, OtherCountry, 1) OtherPhone
							FROM	#Accounts
						) x 
			) u
	WHERE	ISNULL(Mobile,'!') <> UpdatedMobile

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Account_Phone', 'Fax', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	u
	SET		Fax = UpdatedFax
	FROM	(	SELECT	Fax,
						CASE
							WHEN HomeFax NOT IN ('Invalid','Unknown') THEN HomeFax
							WHEN PostalFax NOT IN ('Invalid','Unknown') THEN PostalFax
							WHEN BusinessFax NOT IN ('Invalid','Unknown') THEN BusinessFax
							WHEN OtherFax NOT IN ('Invalid','Unknown') THEN OtherFax
							WHEN HomeFax  = 'Invalid' THEN 'Invalid'
							WHEN PostalFax  = 'Invalid' THEN 'Invalid'
							WHEN BusinessFax  = 'Invalid' THEN 'Invalid'
							WHEN OtherFax  = 'Invalid' THEN 'Invalid'
							ELSE 'Unknown'
						END UpdatedFax
				FROM	(	SELECT	Fax,
									[Cleanse].[FormatPhoneNumber](HomeFax, HomeState, HomeCountry, 0) HomeFax,
									[Cleanse].[FormatPhoneNumber](PostalFax, PostalState, PostalCountry, 0) PostalFax,
									[Cleanse].[FormatPhoneNumber](BusinessFax, BusinessState, BusinessCountry, 0) BusinessFax,
									[Cleanse].[FormatPhoneNumber](OtherFax, OtherState, OtherCountry, 0) OtherFax
							FROM	#Accounts
						) x 
			) u
	WHERE	ISNULL(Fax,'!') <> UpdatedFax

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Account_Phone', 'Update', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	a
	SET		Phone = u.Phone,
			Mobile = u.Mobile,
			Fax = u.Fax
	FROM	[$(Dimensional)].Dimension.Account a
			INNER JOIN #Accounts u ON u.AccountKey = a.AccountKey
	WHERE	ISNULL(a.Phone,'!') <> ISNULL(u.Phone,'!')
			OR ISNULL(a.Mobile,'!') <> ISNULL(u.Mobile,'!')
			OR ISNULL(a.Fax,'!') <> ISNULL(u.Fax,'!')

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Account_Phone', NULL, 'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Account_Phone', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

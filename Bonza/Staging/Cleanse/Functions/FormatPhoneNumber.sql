﻿CREATE FUNCTION [Cleanse].[FormatPhoneNumber]
(
	@Number varchar(30), 
	@State varchar(30), 
	@Country varchar(40), 
	@isMobile bit
)
RETURNS varchar(20)
AS
BEGIN
	
	SET @Number = REPLACE(REPLACE(REPLACE(REPLACE(@Number,' ',''),'(',''),')',''),'-','')

	IF @Number IS NULL
		SET @Number = 'Unknown'
	ELSE IF LEN(@Number) = 0
		SET @Number = 'Unknown'
	ELSE IF @Country = 'Australia' OR @Number LIKE '+61%'
		BEGIN	
			SET @Number = REPLACE(@Number,'+','')

			SET @Number = CASE
							WHEN LEN(@Number) = 8 AND @State = 'Australian Capital Territory'  THEN '612' + @Number
							WHEN LEN(@Number) = 8 AND @State = 'New South Wales'  THEN '612' + @Number
							WHEN LEN(@Number) = 8 AND @State = 'Tasmania'  THEN '613' + @Number
							WHEN LEN(@Number) = 8 AND @State = 'Victoria'  THEN '613' + @Number
							WHEN LEN(@Number) = 8 AND @State = 'Queensland'  THEN '617' + @Number
							WHEN LEN(@Number) = 8 AND @State = 'South Australia'  THEN '618' + @Number
							WHEN LEN(@Number) = 8 AND @State = 'Northern Territories'  THEN '618' + @Number
							WHEN LEN(@Number) = 8 AND @State = 'Western Australia'  THEN '618' + @Number
							WHEN @Number like '[2,3,4,7,8]%' and LEN(@Number) = 9 THEN '61' + @Number
							WHEN @Number like '0[2,3,4,7,8]%' and LEN(@Number) = 10 THEN '61' + SUBSTRING(@Number,2,9)
							WHEN @Number like '610[2,3,4,7,8]%' and LEN(@Number) = 12 THEN '61' + SUBSTRING(@Number,4,9)
							WHEN @Number like '61[2,3,4,7,8]%' and LEN(@Number) = 11 THEN @Number
							WHEN @Number like '6161[2,3,4,7,8]%' and LEN(@Number) = 13 THEN SUBSTRING(@Number,3,11)
							ELSE 'Invalid'
						END
			SET @Number = CASE WHEN @isMobile = 1 AND @Number <> 'Invalid' AND @Number NOT LIKE '614%' THEN 'Unknown' ELSE @Number END
			SET @Number = CASE WHEN ISNULL(@isMobile,0) <> 1 AND @Number <> 'Invalid' AND @Number NOT LIKE '61[2,3,7,8]%' THEN 'Unknown' ELSE @Number END
		END
	ELSE
		SET @Number = @Number
		
	RETURN @Number

END

﻿CREATE FUNCTION [Cleanse].[ExtractNumbersOnly]
(@Str varchar(1000))
RETURNS varchar(1000) AS
BEGIN
  DECLARE @Result varchar(1000)=''
  DECLARE @strcheck varchar(1)=''
  declare @counter int=1
  WHILE @counter<=Len(@str)
  BEGIN
	set @strcheck=isnull(Substring(@str,@counter,1),'')
	IF @strcheck like '[0-9]'
	Begin
			 set @Result=Concat(@Result,@strcheck)
	End
	set @counter=@counter+1
  END
  RETURN @Result
END
﻿CREATE TABLE [Stage].[Competitor] (
    [BatchKey]           INT           NULL,
    [CompetitorID]       VARCHAR (50)  NULL,
    [CompetitorSource]   CHAR (3)      NULL,
    [SaddleNumber]       INT           NULL,
    [CompetitorName]     VARCHAR (100) NULL,
    [Scratched]          BIT           NULL,
    [Jockey]             VARCHAR (50)  NULL,
    [Draw]               VARCHAR (50)  NULL,
    [Form]               VARCHAR (20)  NULL,
    [History]            VARCHAR (10)  NULL,
    [Weight]             FLOAT (53)    NULL,
    [Trainer]            VARCHAR (50)  NULL,
    [ManualIntPrice]     BIT           NULL,
    [bestTime]           VARCHAR (10)  NULL,
    [scratchedLate]      BIT           NULL,
    [rulesOverrideState] TINYINT       NULL,
    [ManualIntPriceCB]   BIT           NULL,
    [TabRollCount]       INT           NULL,
    [IsKnockedOut]       BIT           NULL,
    [EVENT_SK]           INT           NULL,
    [ManualIntPriceTW]   BIT           NULL,
    [Fromdate]           DATETIME2 (3) NULL
) ON [par_sch_int_Stage] ([BatchKey])
WITH (DATA_COMPRESSION = ROW);
GO

ALTER TABLE [Stage].[Competitor] SET (LOCK_ESCALATION = AUTO)
GO


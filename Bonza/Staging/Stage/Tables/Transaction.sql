﻿
CREATE TABLE [Stage].[Transaction] (
    [BatchKey]                   INT           NOT NULL,
    [TransactionId]              BIGINT        NOT NULL,
    [TransactionIdSource]        CHAR (3)      NOT NULL,
    [BalanceTypeID]              CHAR (3)      NOT NULL,
    [TransactionTypeID]          CHAR (2)      NULL,
    [TransactionStatusID]        CHAR (2)      NULL,
    [TransactionMethodID]        CHAR (2)      NULL,
    [TransactionDirection]       VARCHAR (3)   NULL,
    [BetGroupID]                 BIGINT        NOT NULL,
    [BetGroupIDSource]           CHAR (3)      NOT NULL,
    [BetId]                      BIGINT        NOT NULL,
    [BetIdSource]                VARCHAR (32)  NULL,
    [LegId]                      BIGINT        NULL,
    [LegIdSource]                VARCHAR (32)  NULL,
    [FreeBetID]                  BIGINT        NULL,
    [ExternalID]                 VARCHAR (200) NULL,
    [External1]                  VARCHAR (200) NULL,
    [External2]                  VARCHAR (200) NULL,
    [UserID]                     VARCHAR (200) NULL,
    [UserIDSource]               CHAR (3)      NULL,
    [InstrumentID]               VARCHAR (50)  NULL,
    [InstrumentIDSource]         CHAR (3)      NULL,
    [LedgerId]                   INT        NOT NULL,
    [LedgerSource]               CHAR (3)      NOT NULL,
    [WalletId]                   CHAR (1)      NOT NULL,
    [PromotionId]                INT           NOT NULL,
    [PromotionIDSource]          CHAR (3)      NOT NULL,
    [MasterTransactionTimestamp] DATETIME2 (7) NOT NULL,
    [MasterDayText]              CHAR (11)  NOT NULL,
    [MasterTimeText]             VARCHAR (5)   NOT NULL,
    [Channel]					 SMALLINT	   NOT NULL,
    [ActionChannel]				 SMALLINT	   NOT NULL,
    [CampaignId]                 INT           NOT NULL,
    [TransactedAmount]           MONEY         NOT NULL,
    [BetGroupType]               VARCHAR (20)  NOT NULL,
    [BetTypeName]                VARCHAR (20)  NOT NULL,
    [BetSubTypeName]             VARCHAR (20)  NOT NULL,
    [LegBetTypeName]             VARCHAR (20)  NOT NULL,
    [GroupingType]               VARCHAR (20)  NOT NULL,
	[PriceTypeId]                INT           NOT NULL, --NEW
    [PriceTypeSource]            CHAR (3)      NOT NULL, --NEW
    [ClassId]                    SMALLINT      NOT NULL,
    [ClassIdSource]              CHAR (3)      NOT NULL,
    [EventID]                    INT           NOT NULL,
    [EventIDSource]              CHAR (3)      NOT NULL,
	[MarketID]                   INT           NOT NULL, --NEW
    [MarketIDSource]             CHAR (3)      NOT NULL, --NEW
    [LegNumber]                  TINYINT       NOT NULL,
    [NumberOfLegs]               TINYINT       NOT NULL,
    [ExistsInDim]                TINYINT       NOT NULL,
    [Conditional]                SMALLINT      NOT NULL,
    [MappingType]                TINYINT       NOT NULL,
    [MappingId]                  SMALLINT      NOT NULL,
    [FromDate]                   DATETIME2 (3) NOT NULL,
    [DateTimeUpdated]            INT           NULL,
    [RequestID]                  INT           NULL,
    [WithdrawID]                 INT           NULL,
	[ClickToCall]				 VARCHAR(20)   NULL,
	[InPlay]					 VARCHAR(20)   NULL,
	[CashoutType]				[int]			NOT NULL,
	[IsDoubleDown]				 BIT			 NOT NULL,
	[IsDoubledDown]				 BIT			 NOT NULL
) ON [par_sch_int_Stage] ([BatchKey])
WITH (DATA_COMPRESSION = ROW );
GO

ALTER TABLE [Stage].[Transaction] SET (LOCK_ESCALATION = AUTO)
GO




﻿CREATE TABLE [Stage].[LegType] (
    [BatchKey]     INT          NOT NULL,
    [LegNumber]    TINYINT      NOT NULL,
    [NumberOfLegs] TINYINT      NOT NULL,
    [LegTypeName]  VARCHAR (32) NOT NULL
) ON [par_sch_int_Stage] ([BatchKey])
WITH (DATA_COMPRESSION = ROW );
GO

ALTER TABLE [Stage].[LegType] SET (LOCK_ESCALATION = AUTO)
GO


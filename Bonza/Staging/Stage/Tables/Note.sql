﻿CREATE TABLE [Stage].[Note] (
    [NoteId] BIGINT         NOT NULL,
	[NoteSource] [Char](3) NOT NULL,
    [Notes]    VARCHAR (4000)  NULL,
    [BatchKey]  INT            NULL,
	[FromDate]    dateTime2(3) NULL
) ON [par_sch_int_Stage] ([BatchKey])
WITH (DATA_COMPRESSION = ROW );
GO

ALTER TABLE [Stage].[Note] SET (LOCK_ESCALATION = AUTO)
GO




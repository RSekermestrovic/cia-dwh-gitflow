﻿
CREATE TABLE [Stage].[Bet] (
    [BatchKey]                   INT           NOT NULL,
    
	[BetGroupID]                 BIGINT        NOT NULL, /* Contract */
    [BetGroupIDSource]           CHAR (3)      NOT NULL,
    [BetId]                      BIGINT        NOT NULL,
    [BetIdSource]                VARCHAR (32)  NOT NULL,
    [LegId]                      BIGINT        NOT NULL,
    [LegIdSource]                VARCHAR (32)  NOT NULL,
    
	[LedgerId]                   BIGINT        NOT NULL, /* Account */
    [LedgerSource]               CHAR (3)      NOT NULL,
    
	[WalletId]                   CHAR (1)      NOT NULL, /* Wallet */
    
	[PromotionId]                INT           NOT NULL, /* Promotion */
    [PromotionIDSource]          CHAR (3)      NOT NULL,
    
	[MasterTransactionTimestamp] DATETIME2 (7) NOT NULL, /* Day Time */
    [MasterDayText]              VARCHAR (11)  NOT NULL, /* Day */
    [MasterTimeText]             VARCHAR (5)   NOT NULL, /* Time */
    
	[Channel]                INT  NOT NULL, /* Channel */
    
    [BetGroupType]               VARCHAR (32)  NOT NULL, /* Bet Type */
    [BetTypeName]                VARCHAR (32)  NOT NULL,
    [BetSubTypeName]             VARCHAR (32)  NOT NULL,
    [LegBetTypeName]             VARCHAR (32)  NOT NULL,
    [GroupingType]               VARCHAR (32)  NOT NULL,
	
	[PriceTypeId]                INT           NOT NULL, /* Price Type */
    [PriceTypeSource]            CHAR (3)      NOT NULL, 
    
	[ClassId]                    INT           NOT NULL, /* Class */
    [ClassIdSource]              CHAR (3)      NOT NULL,
    
	[MarketID]                   INT           NOT NULL, /* Market */
    [MarketIDSource]             CHAR (3)      NOT NULL,

	[BetStatus]                  VARCHAR (10)  NOT NULL, /* Bet Status */

	[Price]				         MONEY         NULL,     /* Price */

	[SelectionID]                VARCHAR (255) NOT NULL, /* Selection */
	[SelectionIDSource]          CHAR (3)      NOT NULL,
	
	[Stake]				         MONEY         NOT NULL, /* Measures */
	[Payout]				     MONEY         NOT NULL,
	[StakeDelta]				 MONEY         NOT NULL,
	[PayoutDelta]				 MONEY         NOT NULL,
	[Fees]						 MONEY         NOT NULL
    	
) ON [par_sch_int_Stage] ([BatchKey])
WITH (DATA_COMPRESSION = ROW );
GO


ALTER TABLE [Stage].[Bet] SET (LOCK_ESCALATION = AUTO)
GO




﻿CREATE TABLE [Stage].[BetStatus] (
    [BatchKey]    INT          NOT NULL,
    [StatusCode]  CHAR (1)     NOT NULL,
    [OutcomeCode] CHAR (1)     NOT NULL,
    [StatusName]  VARCHAR (32) NOT NULL,
    [OutcomeName] VARCHAR (32) NOT NULL
) ON [par_sch_int_Stage] ([BatchKey])
WITH (DATA_COMPRESSION = ROW );
GO

ALTER TABLE [Stage].[BetStatus] SET (LOCK_ESCALATION = AUTO)
GO


﻿CREATE TABLE [Stage].[Activity] (
    [BatchKey]     INT         NOT NULL,
    [HasDeposited] VARCHAR (3) NOT NULL,
    [HasWithdrawn] VARCHAR (3) NOT NULL,
    [HasWon]       VARCHAR (3) NOT NULL,
    [Has Bet]      VARCHAR (3) NOT NULL
) ON [par_sch_int_Stage] ([BatchKey])
WITH (DATA_COMPRESSION = ROW );
GO

ALTER TABLE [Stage].[Activity] SET (LOCK_ESCALATION = AUTO)
GO


﻿CREATE TABLE [Stage].[User] (
    [BatchKey]      INT           NULL,
    [UserId]        VARCHAR (32)  NULL,
    [Source]        VARCHAR (3)   NULL,
    [SystemName]    VARCHAR (32)  NULL,
    [PayrollNumber] VARCHAR (32)  NULL,
    [Forename]      VARCHAR (100) NULL,
    [Surname]       VARCHAR (100) NULL,
    [FromDate]      DATETIME2 (3) NULL
) ON [par_sch_int_Stage] ([BatchKey])
WITH (DATA_COMPRESSION = ROW );
GO

ALTER TABLE [Stage].[User] SET (LOCK_ESCALATION = AUTO)
GO


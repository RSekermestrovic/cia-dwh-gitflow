﻿CREATE TABLE [Stage].[AccountStatus](
	[BatchKey] [int] NULL,
	[LedgerID] [int] NULL,
	[LedgerSource] [char](3) NULL,
	[LedgerEnabled] [varchar](10) NULL,
	[LedgerStatus] [char](1) NULL,
	[AccountType] [varchar](30) NULL,
	[InternetProfile] [varchar](30) NULL,
	[VIP] [varchar](10) NULL,
	[BrandCode] [varchar](5) NULL,
	[DivisionName] [varchar](30) NULL,
	[CreditLimit] [money] NULL,
	[MinBetAmount] [money] NULL,
	[MaxBetAmount] [money] NULL,
	[Introducer] [varchar](50) NULL,
	[Handled] [varchar](10) NULL,
	[PhoneColourDesc] [varchar](30) NULL,
	[DisableDeposit] [varchar](10) NULL,
	[DisableWithdrawal] [varchar](10) NULL,
	[AccountClosedByUserID] [VARCHAR](32) NULL,
	[AccountClosedByUserSource] [char](3) NULL,
	[AccountClosedDate] [smalldatetime] NULL,
	[ReIntroducedBy] [int] NULL,
	[ReIntroducedDate] [smalldatetime] NULL,
	[AccountClosureReason] [varchar](1000) NULL,
	[DepositLimit] [money] NULL,
	[LossLimit] [money] NULL,
	[LimitPeriod] [int] NULL,
	[LimitTimeStamp] [datetime] NULL,
	[ReceiveMarketingEmail] [varchar](10) NULL,
	[ReceiveCompetitionEmail] [varchar](10) NULL,
	[ReceiveSMS] [varchar](10) NULL,
	[ReceivePostalMail] [varchar](10) NULL,
	[ReceiveFax] [varchar](10) NULL,
	[FromDate] [datetime2](3) NULL
) ON [par_sch_int_Stage] ([BatchKey])
WITH (DATA_COMPRESSION = ROW );
GO

ALTER TABLE [Stage].[AccountStatus] SET (LOCK_ESCALATION = AUTO)
GO





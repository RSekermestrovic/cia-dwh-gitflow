﻿CREATE TABLE [Stage].[Instrument] (
    [BatchKey]          INT           NOT NULL,
    [InstrumentId]      VARCHAR (50)  NOT NULL,
    [Source]            CHAR (3)      NOT NULL,
    [AccountNumber]     VARCHAR (32)  NULL,
    [SafeAccountNumber] VARCHAR (32)  NULL,
    [AccountName]       VARCHAR (100) NULL,
    [BSB]               VARCHAR (32)  NULL,
    [ExpiryDate]        DATETIME2 (0) NULL,
    [Verified]          VARCHAR (3)   NULL,
    [VerifyAmount]      MONEY         NULL,
    [InstrumentType]    VARCHAR (15)  NULL,
    [ProviderName]      VARCHAR (100) NULL,
    [FromDate]          DATETIME2 (0) NULL
) ON [par_sch_int_Stage] ([BatchKey])
WITH (DATA_COMPRESSION = ROW );
GO

ALTER TABLE [Stage].[Instrument] SET (LOCK_ESCALATION = AUTO)
GO




﻿CREATE TABLE [Stage].[Contract] (
    [BatchKey]    INT          NOT NULL,
    [ContractID]  BIGINT       NOT NULL,
    [BetId]       BIGINT       NOT NULL,
    [MultiId]     BIGINT       NULL,
    [FreeBetID]   BIGINT       NULL,
    [CompanyCode] VARCHAR (3)  NOT NULL,
    [ExternalID]  VARCHAR (32) NULL,
    [Source]      CHAR (2)     NOT NULL
) ON [par_sch_int_Stage] ([BatchKey])
WITH (DATA_COMPRESSION = ROW );
GO

ALTER TABLE [Stage].[Contract] SET (LOCK_ESCALATION = AUTO)
GO












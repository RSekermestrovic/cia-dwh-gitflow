﻿CREATE TABLE [Stage].[Limit] (
    [BatchKey]                 INT          NOT NULL,
    [LimitName]                VARCHAR (32) NOT NULL,
    [LimitAmount]              MONEY        NULL,
    [PercentageSelectionLimit] FLOAT (53)   NOT NULL,
    [SelectionLimitName]       VARCHAR (32) NOT NULL,
    [SelectionLimitAmount]     MONEY        NULL
) ON [par_sch_int_Stage] ([BatchKey])
WITH (DATA_COMPRESSION = ROW );
GO

ALTER TABLE [Stage].[Limit] SET (LOCK_ESCALATION = AUTO)
GO


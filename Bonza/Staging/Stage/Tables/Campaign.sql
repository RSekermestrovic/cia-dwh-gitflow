﻿CREATE TABLE [Stage].[Campaign] (
    [BatchKey]           INT           NOT NULL,
    [CampaignId]         INT           NOT NULL,
    [CampaignName]       VARCHAR (32)  NOT NULL,
    [CampaignComment]    VARCHAR (100) NOT NULL,
    [CampaignActive]     VARCHAR (3)   NOT NULL,
    [DayText]            VARCHAR (11)  NOT NULL,
    [CampaignTypeId]     INT           NOT NULL,
    [CampaignTypeName]   VARCHAR (32)  NOT NULL,
    [CampaignTypeActive] VARCHAR (3)   NOT NULL
) ON [par_sch_int_Stage] ([BatchKey])
WITH (DATA_COMPRESSION = ROW );
GO

ALTER TABLE [Stage].[Campaign] SET (LOCK_ESCALATION = AUTO)
GO



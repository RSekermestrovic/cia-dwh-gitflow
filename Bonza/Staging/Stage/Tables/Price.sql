﻿CREATE TABLE [Stage].[Price] (
    [BatchKey]               INT          NOT NULL,
    [PriceText]              VARCHAR (10) NOT NULL,
    [Price]                  FLOAT (53)   NOT NULL,
    [Probability]            FLOAT (53)   NOT NULL,
    [PriceBandName]          VARCHAR (32) NOT NULL,
    [PriceBandLowerBound]    FLOAT (53)   NOT NULL,
    [PriceBandUpperBound]    FLOAT (53)   NOT NULL,
    [BetPriceText]           VARCHAR (10) NOT NULL,
    [BetPrice]               FLOAT (53)   NOT NULL,
    [BetProbability]         FLOAT (53)   NOT NULL,
    [BetPriceBandName]       VARCHAR (32) NOT NULL,
    [BetPriceBandLowerBound] FLOAT (53)   NOT NULL,
    [BetPriceBandUpperBound] FLOAT (53)   NOT NULL
) ON [par_sch_int_Stage] ([BatchKey])
WITH (DATA_COMPRESSION = ROW );
GO

ALTER TABLE [Stage].[Price] SET (lock_escalation = auto)
GO


﻿CREATE TABLE [Stage].[LocalDay] (
    [BatchKey]               INT          NOT NULL,
    [DayText]                VARCHAR (11) NOT NULL,
    [CalendarName]           VARCHAR (32) NOT NULL,
    [LocalDayName]           VARCHAR (32) NOT NULL,
    [TimezoneOffset]         CHAR (4)     NOT NULL,
    [DaylightSaving]         VARCHAR (3)  NOT NULL,
    [PublicHoliday]          VARCHAR (3)  NOT NULL,
    [SchoolHoliday]          VARCHAR (3)  NOT NULL,
    [Timezone]               CHAR (4)     NOT NULL,
    [ObservesDaylightSaving] VARCHAR (3)  NOT NULL
) ON [par_sch_int_Stage] ([BatchKey])
WITH (DATA_COMPRESSION = ROW );
GO

ALTER TABLE [Stage].[LocalDay] SET (LOCK_ESCALATION = AUTO)
GO


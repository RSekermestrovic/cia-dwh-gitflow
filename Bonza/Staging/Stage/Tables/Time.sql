﻿CREATE TABLE [Stage].[Time] (
    [BatchKey]           INT           NOT NULL,
    [TimeID]           VARCHAR (5)   NOT NULL,
    [TimeText]           VARCHAR (7)   NOT NULL,
    [Time]               TIME (0)      NOT NULL,
    [TimeStartTime]      TIME (0)      NOT NULL,
    [TimeEndTime]        TIME (0)      NOT NULL,
    [TimeStartTimestamp] DATETIME2 (7) NOT NULL,
    [TimeEndTimestamp]   DATETIME2 (7) NOT NULL,
    [TimeOfHourNumber]   SMALLINT      NOT NULL,
    [TimeOfDayNumber]    SMALLINT      NOT NULL,
    [HourID]           VARCHAR (3)   NOT NULL,
    [HourText]           VARCHAR (7)   NOT NULL,
    [HourStartTime]      TIME (0)      NOT NULL,
    [HourEndTime]        TIME (0)      NOT NULL,
    [HourOfDayNumber]    TINYINT       NOT NULL,
    [PeriodText]         VARCHAR (32)  NOT NULL
) ON [par_sch_int_Stage] ([BatchKey])
WITH (DATA_COMPRESSION = ROW );
GO

ALTER TABLE [Stage].[Time] SET (LOCK_ESCALATION = AUTO)
GO


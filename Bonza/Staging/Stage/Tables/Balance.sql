﻿CREATE TABLE [Stage].[Balance] (
    [BatchKey]         INT           NOT NULL,
    [BalanceTypeID]    CHAR (3)      NOT NULL,
    [LedgerID]         INT        NOT NULL,
    [LedgerSource]     CHAR (3)      NOT NULL,
    [WalletId]         CHAR (1)      NOT NULL,
    [MasterDayText]    CHAR (11)  NULL,
    [AnalysisDayText]  CHAR (11)  NULL,
    [OpeningBalance]   MONEY         NOT NULL,
    [TransactedAmount] MONEY         NOT NULL,
    [ClosingBalance]   MONEY         NOT NULL,
    [FromDate]         DATETIME2 (3) NOT NULL,
    [MasterTimeStamp]  DATETIME      NULL
) ON [par_sch_int_Stage] ([BatchKey])
WITH (DATA_COMPRESSION = ROW );
GO

ALTER TABLE [Stage].[Balance] SET (LOCK_ESCALATION = AUTO)
GO




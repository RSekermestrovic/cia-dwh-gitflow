CREATE TABLE [Stage].[Account](
	[BatchKey] [int] NULL,

-- Ledger Details --
	[LedgerId] [int] NULL,
	[LedgerSource] [char](3) NULL,
	[LedgerSequenceNumber] [int] NULL,
	[LegacyLedgerId] [int] NULL,
	[LedgerTypeId] [int] NULL,
	[LedgerType] [varchar](50) NULL,
	[LedgerOpenedDate] [datetime] NULL,
	[LedgerOpenedDayText] [char](11) NULL,
	[LedgerOpenedDateOrigin] [varchar](20) NULL,

--Account Details
	[AccountID] [int] NULL,
	[AccountSource] [char](3) NULL,
	[PIN] [int] NULL,
	[UserName] [varchar](64) NULL,
	[ExactTargetID] [varchar](40) NULL,
	[AccountFlags] [int] NULL, -- ClientType from Intrabet

	[AccountOpenedChannelId] [int] NULL,

	[SourceBTag2] [varchar](100) NULL,
	[SourceTrafficSource] [varchar](50) NULL,
	[SourceRefURL] [varchar](500) NULL,
	[SourceCampaignId] [varchar](50) NULL,
	[SourceKeywords] [varchar](500) NULL,

	[StatementMethod] [varchar](20) NULL,
	[StatementFrequency] [varchar](20) NULL,

	[AccountTitle] [varchar](10) NULL,
	[AccountSurname] [varchar](60) NULL,
	[AccountFirstName] [varchar](60) NULL,
	[AccountMiddleName] [varchar](60) NULL,
	[AccountGender] [varchar](10) NULL,
	[AccountDOB] [date] NULL,

	[HasHomeAddress] [tinyint] NULL,
	[HomeAddress1] [varchar](60) NULL,
	[HomeAddress2] [varchar](60) NULL,
	[HomeSuburb] [varchar](50) NULL,
	[HomeStateCode] [varchar](3) NULL,
	[HomeState] [varchar](30) NULL,
	[HomePostCode] [varchar](7) NULL,
	[HomeCountryCode] [varchar](3) NULL,
	[HomeCountry] [varchar](40) NULL,
	[HomePhone] [varchar](20) NULL,
	[HomeMobile] [varchar](20) NULL,
	[HomeFax] [varchar](20) NULL,
	[HomeEmail] [varchar](100) NULL,
	[HomeAlternateEmail] [varchar](100) NULL,
	[HasPostalAddress] [tinyint] NULL,
	[PostalAddress1] [varchar](60) NULL,
	[PostalAddress2] [varchar](60) NULL,
	[PostalSuburb] [varchar](50) NULL,
	[PostalStateCode] [varchar](3) NULL,
	[PostalState] [varchar](30) NULL,
	[PostalPostCode] [varchar](7) NULL,
	[PostalCountryCode] [varchar](3) NULL,
	[PostalCountry] [varchar](40) NULL,
	[PostalPhone] [varchar](20) NULL,
	[PostalMobile] [varchar](20) NULL,
	[PostalFax] [varchar](20) NULL,
	[PostalEmail] [varchar](100) NULL,
	[PostalAlternateEmail] [varchar](100) NULL,
	[HasBusinessAddress] [tinyint] NULL,
	[BusinessAddress1] [varchar](60) NULL,
	[BusinessAddress2] [varchar](60) NULL,
	[BusinessSuburb] [varchar](50) NULL,
	[BusinessStateCode] [varchar](3) NULL,
	[BusinessState] [varchar](30) NULL,
	[BusinessPostCode] [varchar](7) NULL,
	[BusinessCountryCode] [varchar](3) NULL,
	[BusinessCountry] [varchar](40) NULL,
	[BusinessPhone] [varchar](20) NULL,
	[BusinessMobile] [varchar](20) NULL,
	[BusinessFax] [varchar](20) NULL,
	[BusinessEmail] [varchar](100) NULL,
	[BusinessAlternateEmail] [varchar](100) NULL,
	[HasOtherAddress] [tinyint] NULL,
	[OtherAddress1] [varchar](60) NULL,
	[OtherAddress2] [varchar](60) NULL,
	[OtherSuburb] [varchar](50) NULL,
	[OtherStateCode] [varchar](3) NULL,
	[OtherState] [varchar](30) NULL,
	[OtherPostCode] [varchar](7) NULL,
	[OtherCountryCode] [varchar](3) NULL,
	[OtherCountry] [varchar](40) NULL,
	[OtherPhone] [varchar](20) NULL,
	[OtherMobile] [varchar](20) NULL,
	[OtherFax] [varchar](20) NULL,
	[OtherEmail] [varchar](100) NULL,
	[OtherAlternateEmail] [varchar](100) NULL,

--Metadata
	[FirstDate] [datetime2](3) NULL,
	[FromDate] [datetime2](3) NULL
) ON [par_sch_int_Stage] ([BatchKey])
WITH (DATA_COMPRESSION = ROW);
GO

ALTER TABLE [Stage].[Account] SET (LOCK_ESCALATION = AUTO)
GO


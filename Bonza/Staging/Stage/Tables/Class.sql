﻿CREATE TABLE [Stage].[Class](
	[BatchKey] [int] NULL,
	[ClassId] [smallint] NULL,
	[Source] [varchar](3) NOT NULL,
	[SourceclassName] [varchar](50) NULL,
	[ClassCode] [varchar](4) NOT NULL,
	[ClassName] [varchar](7) NOT NULL,
	[classIcon] [varchar](2) NOT NULL,
	[SuperClassKey] [int] NOT NULL,
	[SuperClassName] [varchar](7) NOT NULL,
	[FromDate] [datetime2](3) NULL,
	[ToDate] [datetime2](3) NULL
) ON [par_sch_int_Stage] ([BatchKey])
WITH (DATA_COMPRESSION = ROW );
GO

ALTER TABLE [Stage].[Class] SET (LOCK_ESCALATION = AUTO)
GO

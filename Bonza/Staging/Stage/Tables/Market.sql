CREATE TABLE [Stage].[Market] (
    [BatchKey]					INT           NOT NULL,
    [Source]					CHAR (3)      NOT NULL,
    [MarketId]					INT           NOT NULL,
    [Market]					VARCHAR (255) NOT NULL,
    [MarketType]				VARCHAR (255) NULL,
    [MarketGroup]				VARCHAR (255) NULL,
	[LiveMarket]				VARCHAR (3)   NULL,
	[FutureMarket]				VARCHAR (3)  NULL,
	[MaxWinnings]				INT		    NULL,
	[MaxStake]					INT		    NULL,
    [LevyRate]					VARCHAR (30)  NULL,
    [Sport]						VARCHAR (50)  NULL,
	[MarketOpen]				VARCHAR(10)	  NULL,
	[MarketOpenedDateTime]		DATETIME2(3)  NULL,
	[MarketOpenedVenueDateTime]	DATETIME2(3)  NULL,
	[MarketClosed]				VARCHAR(10)	  NULL,
	[MarketClosedDateTime]		DATETIME2(3)  NULL,
	[MarketClosedVenueDateTime]	DATETIME2(3)  NULL,
	[MarketSettled]				VARCHAR(10)	  NULL,
	[MarketSettledDateTime]		DATETIME2(3)  NULL,
	[MarketSettledVenueDateTime]	DATETIME2(3)  NULL, 
    [FromDate]					DATETIME2 (3) NOT NULL
) ON [par_sch_int_Stage] ([BatchKey])
WITH (DATA_COMPRESSION = ROW ON PARTITIONS (1), DATA_COMPRESSION = ROW ON PARTITIONS (2));
GO

ALTER TABLE [Stage].[Market] SET (LOCK_ESCALATION = AUTO)
GO




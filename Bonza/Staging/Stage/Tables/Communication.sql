﻿CREATE TABLE [Stage].[Communication](
	[BatchKey] [int] NOT NULL,
	[CommunicationId] [varchar](200) NULL,
	[CommunicationSource] [varchar](5) NULL,
	[Title] [varchar](200) NULL,
	[Details] [varchar](max) NULL,
	[SubDetails] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]



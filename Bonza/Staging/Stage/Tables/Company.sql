﻿CREATE TABLE [Stage].[Company] (
    [BatchKey]    INT          NOT NULL,
    [CompanyCode] VARCHAR (3)  NOT NULL,
    [CompanyName] VARCHAR (32) NOT NULL
) ON [par_sch_int_Stage] ([BatchKey])
WITH (DATA_COMPRESSION = ROW);
GO

ALTER TABLE [Stage].[Company] SET (LOCK_ESCALATION = AUTO)
GO


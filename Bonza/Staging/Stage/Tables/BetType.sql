﻿CREATE TABLE [Stage].[BetType] (
    [BatchKey]       INT          NOT NULL,
    [BetTypeName]    VARCHAR (32) NOT NULL,
    [LegBetTypeName] VARCHAR (32) NOT NULL
) ON [par_sch_int_Stage] ([BatchKey])
WITH (DATA_COMPRESSION = ROW );
GO

ALTER TABLE [Stage].[BetType] SET (LOCK_ESCALATION = AUTO)
GO




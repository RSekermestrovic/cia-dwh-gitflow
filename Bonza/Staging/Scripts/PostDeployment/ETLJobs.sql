USE [$(DatabaseName)];

DECLARE @Category sysname = 'ETL'
DECLARE @Schedule sysname = 'ETL'
DECLARE @Prefix sysname = '$(JobPrefix)'
DECLARE @Interval int = CONVERT(int,'$(JobIntervalSeconds)')
DECLARE @Blitz bit = 0

DECLARE @i int, @SQL nvarchar(MAX)

-- Category
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=@Category AND category_class=1)
	EXEC msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=@Category

-- Schedule --
BEGIN TRY DROP TABLE #ScheduleBlueprint END TRY BEGIN CATCH END CATCH
BEGIN TRY DROP TABLE #ScheduleCurrent END TRY BEGIN CATCH END CATCH
BEGIN TRY DROP TABLE #Schedule END TRY BEGIN CATCH END CATCH

SELECT	@Prefix+@Schedule schedule_name, 4 freq_type, 1 freq_interval, 2 freq_subday_type, @Interval freq_subday_interval, 0 freq_relative_interval, 0 freq_recurrence_factor, 000000 active_start_time, 235959 active_end_time, 'sa' owner_login_name
INTO	#ScheduleBlueprint
WHERE	@Blitz <> 1 

SELECT	s.name schedule_, freq_type, freq_interval, freq_subday_type, freq_subday_interval, freq_relative_interval, freq_recurrence_factor, active_start_time, active_end_time, 'sa' owner_login_name
INTO	#ScheduleCurrent 
FROM	msdb.dbo.sysschedules s
		INNER JOIN master.sys.syslogins l ON l.sid = s.owner_sid
WHERE	s.name = @Prefix+@Schedule

SELECT	'msdb.dbo.sp_'+Action+'_schedule '+CASE Action WHEN 'add' THEN '@schedule_name = '''+schedule_name+''''+Params WHEN 'delete' THEN '@schedule_name = '''+schedule_name+'''' ELSE '@name = '''+schedule_name+''''+Params END SQL,
		ROW_NUMBER() OVER(ORDER BY schedule_name) i
INTO	#Schedule
FROM	(	SELECT	MAX(x) x, 
					CASE COUNT(*) OVER (PARTITION BY schedule_name) WHEN 2 THEN 'update' ELSE MAX(x) END Action, 
					schedule_name, 
					', @freq_type = '+CONVERT(varchar,freq_type)
					+', @freq_interval = '+CONVERT(varchar,freq_interval)
					+', @freq_subday_type = '+CONVERT(varchar,freq_subday_type)
					+', @freq_subday_interval = '+CONVERT(varchar,freq_subday_interval)
					+', @freq_relative_interval = '+CONVERT(varchar,freq_relative_interval)
					+', @freq_recurrence_factor = '+CONVERT(varchar,freq_recurrence_factor)
					+', @active_start_time = '+CONVERT(varchar,active_start_time)
					+', @active_end_time = '+CONVERT(varchar,active_end_time)
					+', @owner_login_name = '''+owner_login_name+'''' Params
			FROM	(	SELECT 'add' x, * FROM #ScheduleBlueprint
						UNION ALL
						SELECT 'delete' x, * FROM #ScheduleCurrent
					) x
			GROUP BY schedule_name, freq_type, freq_interval, freq_subday_type, freq_subday_interval, freq_relative_interval, freq_recurrence_factor, active_start_time, active_end_time, owner_login_name
			HAVING COUNT(*) <> 2
		) y
WHERE	Action <> 'Update' OR x <> 'Delete'

SET @i = @@ROWCOUNT
WHILE @i > 0
	BEGIN
		SELECT @SQL = SQL FROM #Schedule WHERE i = @i
		EXEC (@SQL)
   		SET @i = @i - 1
	END

-- Jobs --
BEGIN TRY DROP TABLE #JobBlueprint END TRY BEGIN CATCH END CATCH
BEGIN TRY DROP TABLE #JobCurrent END TRY BEGIN CATCH END CATCH
BEGIN TRY DROP TABLE #Job END TRY BEGIN CATCH END CATCH

SELECT	@Prefix+o.name job_name, 1 start_step_id, @Category category_name, 0 notify_level_eventlog, 0 notify_level_email, 0 notify_level_netsend, 0 notify_level_page, 0 delete_level, 'sa' owner_login_name 
INTO	#JobBlueprint 
FROM	sys.objects o
		INNER JOIN sys.schemas s ON s.schema_id = o.schema_id
WHERE	type = 'P' and o.name like 'ETL%' and s.name = 'Control'
		AND	@Blitz <> 1 

SELECT	j.name job_name, start_step_id, c.name category_name, notify_level_eventlog, notify_level_email, notify_level_netsend, notify_level_page, delete_level, l.name owner_login_name
INTO	#JobCurrent
FROM	msdb.dbo.sysjobs j
		INNER JOIN msdb.dbo.syscategories c ON c.category_id = j.category_id
		INNER JOIN master.sys.syslogins l ON l.sid = j.owner_sid
WHERE	j.name like @Prefix+'ETL%'

SELECT	'msdb.dbo.sp_'+Action+'_job @job_name = '''+job_name+''''+CASE Action WHEN 'delete' THEN '' ELSE Params END SQL,
		ROW_NUMBER() OVER(ORDER BY job_name) i
INTO	#Job
FROM	(	SELECT	MAX(x) x, 
					CASE COUNT(*) OVER (PARTITION BY job_name) WHEN 2 THEN 'update' ELSE MAX(x) END Action, 
					job_name, 
					', @start_step_id = '+CONVERT(varchar,start_step_id)
					+', @category_name = '''+category_name+''''
					+', @notify_level_eventlog = '+CONVERT(varchar,notify_level_eventlog)
					+', @notify_level_email = '+CONVERT(varchar,notify_level_email)
					+', @notify_level_netsend = '+CONVERT(varchar,notify_level_netsend)
					+', @notify_level_page = '+CONVERT(varchar,notify_level_page)
					+', @delete_level = '+CONVERT(varchar,delete_level)
					+', @owner_login_name = '''+CONVERT(varchar,owner_login_name)+'''' Params
			FROM	(	SELECT 'add' x, * FROM #JobBlueprint
						UNION ALL
						SELECT 'delete' x, * FROM #JobCurrent
					) x
			GROUP BY job_name, start_step_id, category_name, notify_level_eventlog, notify_level_email, notify_level_netsend, notify_level_page, delete_level, owner_login_name
			HAVING COUNT(*) <> 2
		) y
WHERE	Action <> 'Update' OR x <> 'Delete'

SET @i = @@ROWCOUNT
WHILE @i > 0
	BEGIN
		SELECT @SQL = SQL FROM #Job WHERE i = @i
		EXEC (@SQL)
   		SET @i = @i - 1
	END

-- Job Steps --
BEGIN TRY DROP TABLE #JobStepBlueprint END TRY BEGIN CATCH END CATCH
BEGIN TRY DROP TABLE #JobStepCurrent END TRY BEGIN CATCH END CATCH
BEGIN TRY DROP TABLE #JobStep END TRY BEGIN CATCH END CATCH

SELECT	@Prefix+o.name job_name, 1 step_id, o.name step_name, 'TSQL' subsystem, 'EXEC ['+s.name+'].['+o.name+']' command, DB_NAME() database_name, 0 flags, 0 cmdexec_success_code, 1 on_success_action, 0 on_success_step_id, 2 on_fail_action, 0 on_fail_step_id, 0 retry_attempts, 0 retry_interval, 0 os_run_priority
INTO	#JobStepBlueprint 
FROM	sys.objects o
		INNER JOIN sys.schemas s ON s.schema_id = o.schema_id
WHERE	type = 'P' and o.name like 'ETL%' and s.name = 'Control'
		AND	@Blitz <> 1 

SELECT	j.name job_name, step_id, step_name, subsystem, command, database_name, flags, cmdexec_success_code, on_success_action, on_success_step_id, on_fail_action, on_fail_step_id, retry_attempts, retry_interval, os_run_priority
INTO	#JobStepCurrent
FROM	msdb.dbo.sysjobsteps s
		INNER JOIN msdb.dbo.sysjobs j ON j.job_id = s.job_id
WHERE	j.name like @Prefix+'ETL%'

SELECT	'msdb.dbo.sp_'+Action+'_jobstep @job_name = '''+job_name+''''+CASE Action WHEN 'delete' THEN '' ELSE Params END SQL,
		ROW_NUMBER() OVER(ORDER BY job_name) i
INTO	#JobStep
FROM	(	SELECT	MAX(x) x, 
					CASE COUNT(*) OVER (PARTITION BY job_name, step_id) WHEN 2 THEN 'update' ELSE MAX(x) END Action, 
					job_name, 
					', @step_id = '+CONVERT(varchar,step_id)
					+', @step_name = '''+step_name+''''
					+', @subsystem = '''+subsystem+''''
					+', @command = '''+command+''''
					+', @database_name = '''+database_name+''''
					+', @flags = '+CONVERT(varchar,flags)
					+', @cmdexec_success_code = '+CONVERT(varchar,cmdexec_success_code)
					+', @on_success_action = '+CONVERT(varchar,on_success_action)
					+', @on_success_step_id = '+CONVERT(varchar,on_success_step_id)
					+', @on_fail_action = '+CONVERT(varchar,on_fail_action)
					+', @on_fail_step_id = '+CONVERT(varchar,on_fail_step_id)
					+', @retry_attempts = '+CONVERT(varchar,retry_attempts)
					+', @retry_interval = '+CONVERT(varchar,retry_interval)
					+', @os_run_priority = '+CONVERT(varchar,os_run_priority) Params
			FROM	(	SELECT 'add' x, * FROM #JobStepBlueprint
						UNION ALL
						SELECT 'delete' x, * FROM #JobStepCurrent
					) x
			GROUP BY job_name, step_id, step_name, subsystem, command, database_name, flags, cmdexec_success_code, on_success_action, on_success_step_id, on_fail_action, on_fail_step_id, retry_attempts, retry_interval, os_run_priority
			HAVING COUNT(*) <> 2
		) y
WHERE	Action <> 'Update' OR x <> 'Delete'

SET @i = @@ROWCOUNT
WHILE @i > 0
	BEGIN
		SELECT @SQL = SQL FROM #JobStep WHERE i = @i
		EXEC (@SQL)
   		SET @i = @i - 1
	END

-- Job Servers --
BEGIN TRY DROP TABLE #JobServerBlueprint END TRY BEGIN CATCH END CATCH
BEGIN TRY DROP TABLE #JobServerCurrent END TRY BEGIN CATCH END CATCH
BEGIN TRY DROP TABLE #JobServer END TRY BEGIN CATCH END CATCH

SELECT	@Prefix+o.name job_name, '(LOCAL)' server_name
INTO	#JobServerBlueprint 
FROM	sys.objects o
		INNER JOIN sys.schemas s ON s.schema_id = o.schema_id
WHERE	type = 'P' and o.name like 'ETL%' and s.name = 'Control'
		AND	@Blitz <> 1 

SELECT	j.name job_name, CASE r.srvname WHEN @@SERVERNAME THEN '(LOCAL)' ELSE r.srvname END server_name
INTO	#JobServerCurrent
FROM	msdb.dbo.sysjobservers s
		INNER JOIN msdb.dbo.sysjobs j ON j.job_id = s.job_id
		INNER JOIN msdb.dbo.sysservers r ON r.srvid = s.server_id
WHERE	j.name like @Prefix+'ETL%'

SELECT	'msdb.dbo.sp_'+Action+'_jobserver @job_name = '''+job_name+''', @server_name = '''+server_name+'''' SQL,
		ROW_NUMBER() OVER(ORDER BY job_name) i
INTO	#JobServer
FROM	(	SELECT	MAX(x) Action, 
					job_name, 
					server_name
			FROM	(	SELECT 'add' x, * FROM #JobServerBlueprint
						UNION ALL
						SELECT 'delete' x, * FROM #JobServerCurrent
					) x
			GROUP BY job_name, server_name
			HAVING COUNT(*) <> 2
		) y
WHERE	Action = 'add'

SET @i = @@ROWCOUNT
WHILE @i > 0
	BEGIN
		SELECT @SQL = SQL FROM #JobServer WHERE i = @i
		EXEC (@SQL)
   		SET @i = @i - 1
	END

-- Job Schedules --
BEGIN TRY DROP TABLE #JobScheduleBlueprint END TRY BEGIN CATCH END CATCH
BEGIN TRY DROP TABLE #JobScheduleCurrent END TRY BEGIN CATCH END CATCH
BEGIN TRY DROP TABLE #JobSchedule END TRY BEGIN CATCH END CATCH

SELECT	name job_name, @Prefix+@Schedule schedule_name 
INTO	#JobScheduleBlueprint
FROM	msdb.dbo.sysjobs
WHERE	name like @Prefix+'ETL%'
		AND @Blitz <> 1 

SELECT	j.name job_name, s.name schedule_name 
INTO	#JobScheduleCurrent
FROM	msdb.dbo.sysjobschedules js
		INNER JOIN msdb.dbo.sysschedules s on s.schedule_id = js.schedule_id
		INNER JOIN msdb.dbo.sysjobs j on j.job_id = js.job_id
WHERE	j.name like @Prefix+'ETL%'

SELECT	'msdb.dbo.sp_'+Action+'_schedule @job_name = '''+job_name+''', @schedule_name = '''+schedule_name+'''' + CASE Action WHEN 'delete' THEN ', @delete_unused_schedule = 1' ELSE '' END SQL,
		ROW_NUMBER() OVER(ORDER BY job_name, schedule_name) i
INTO	#JobSchedule
FROM	(	SELECT	MAX(x) Action, 
					job_name, 
					schedule_name
			FROM	(	SELECT 'attach' x, * FROM #JobScheduleBlueprint
						UNION ALL
						SELECT 'detach' x, * FROM #JobScheduleCurrent
					) x
			GROUP BY job_name, schedule_name
			HAVING COUNT(*) <> 2
		) y

SET @i = @@ROWCOUNT
WHILE @i > 0
	BEGIN
		SELECT @SQL = SQL FROM #JobSchedule WHERE i = @i
		EXEC (@SQL)
   		SET @i = @i - 1
	END


﻿DROP SYNONYM [Control].[LiveETLController]
GO

CREATE SYNONYM [Control].[LiveETLController] FOR [EQ3WNPRDBDW01].[Staging].[Control].[ETLController]
GO
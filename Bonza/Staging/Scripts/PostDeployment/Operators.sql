﻿Begin Try EXEC msdb.dbo.sp_delete_operator @name=N'BI Notify' End Try Begin Catch End Catch
GO
EXEC msdb.dbo.sp_add_operator @name=N'BI Notify', 
		@enabled=1, 
		@weekday_pager_start_time=90000, 
		@weekday_pager_end_time=180000, 
		@saturday_pager_start_time=90000, 
		@saturday_pager_end_time=180000, 
		@sunday_pager_start_time=90000, 
		@sunday_pager_end_time=180000, 
		@pager_days=0, 
		@email_address=N'BI.Service@Williamhill.com.au', 
		@category_name=N'[Uncategorized]'
GO


Begin Try EXEC msdb.dbo.sp_delete_operator @name=N'BI Alert' End Try Begin Catch End Catch
GO
EXEC msdb.dbo.sp_add_operator @name=N'BI Alert', 
		@enabled=1, 
		@weekday_pager_start_time=90000, 
		@weekday_pager_end_time=180000, 
		@saturday_pager_start_time=90000, 
		@saturday_pager_end_time=180000, 
		@sunday_pager_start_time=90000, 
		@sunday_pager_end_time=180000, 
		@pager_days=0, 
		@email_address=N'BI.Alert@Williamhill.com.au', 
		@category_name=N'[Uncategorized]'
GO

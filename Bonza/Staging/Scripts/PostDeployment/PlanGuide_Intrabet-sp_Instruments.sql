﻿EXEC sp_create_plan_guide @name = N'[BULK:Intrabet-sp_Instrument_Paypal:InsertIntoStaging(ThresholdDays=10)]', @stmt = N'
	INSERT	into Stage.Instrument (BatchKey,InstrumentId,Source,AccountNumber,SafeAccountNumber,AccountName,BSB,ExpiryDate,Verified,VerifyAmount,InstrumentType,ProviderName,FromDate)
	SELECT	
			@BatchKey																			AS BatchKey, 
			C.id																				AS instrumentID,
			ISNULL(C.trnType, ''Unknown'')														AS Source, 
			ISNULL(Pg.Token,''Unknown'')															AS AccountNumber,
			ISNULL(Pg.Token,''Unknown'')															AS SafeAccountNumber,
			ISNULL(Pg.Payer,''Unknown'')															AS AccountName,	 		
			''N/A''																				AS BSB,
			NULL																				AS ExpiryDate,
			Case When pg.Payerstatus=''verified'' 
			     Then ''Yes''
				 Else ''No'' End																	AS Verified,
			NULL																				AS VerifiedAmount,
			''PayPal''																			AS InstrumentType,
			''N/A''																				AS ProviderName,
			c.FromDate																			AS FromDate		
	FROM #Candidates C  with (nolock)
	LEFT OUTER JOIN [Acquisition].Intrabet.tblPayPalGEC Pg WITH (NOLOCK) ON 
					C.ID COLLATE SQL_Latin1_General_CP1_CI_AS=Cast(Pg.PayerID  as varchar) 
					and c.TimestampPPL=Pg.[Timestamp]
					AND Pg.FromDate <= c.FromDate AND Pg.ToDate > C.FromDate

	OPTION (RECOMPILE, FORCE ORDER, LOOP JOIN)
', @type = N'OBJECT', @module_or_batch = N'[Intrabet].[Sp_Instrument_Paypal]', @hints = N'OPTION (RECOMPILE)'

EXEC sp_create_plan_guide @name = N'[BULK:Intrabet-sp_Instrument_MoneyBookers:InsertIntoStaging(ThresholdDays=10)]', @stmt = N'
	INSERT	into Stage.Instrument (BatchKey,InstrumentId,Source,AccountNumber,SafeAccountNumber,AccountName,BSB,ExpiryDate,Verified,VerifyAmount,InstrumentType,ProviderName,FromDate)
	SELECT	
			@BatchKey																			AS BatchKey, 
			C.id																				AS instrumentID,
			ISNULL(C.TrnType,''Unknown'') 														AS Source, 
			''N/A''																				AS AccountNumber,
			''N/A''																				AS SafeAccountNumber,
			''N/A''																				AS AccountName,		
			''N/A''																				AS BSB,
			 NULL																				AS ExpiryDate,
			''U/K''																				AS Verified,
			NULL																				AS VerifiedAmount,
			''MoneyBookers''																		AS InstrumentType,
			 ''N/A''																				AS ProviderName,
			c.FromDate																			AS FromDate		

	FROM #Candidates C  with (nolock)
	LEFT OUTER JOIN [Acquisition].Intrabet.tblMoneybookersdepositresponse B WITH (NOLOCK)	ON 
					C.ID =Cast(B.ID as varchar) 
					AND B.FromDate <= c.FromDate AND B.ToDate > C.FromDate

	OPTION (RECOMPILE, FORCE ORDER, LOOP JOIN)
', @type = N'OBJECT', @module_or_batch = N'[Intrabet].[Sp_Instrument_MoneyBookers]', @hints = N'OPTION (RECOMPILE)'


EXEC sp_create_plan_guide @name = N'[BULK:Intrabet-sp_Instrument_EFT:InsertIntoStaging(ThresholdDays=10)]', @stmt = N'
	INSERT	into Stage.Instrument (BatchKey,InstrumentId,Source,AccountNumber,SafeAccountNumber,AccountName,BSB,ExpiryDate,Verified,VerifyAmount,InstrumentType,ProviderName,FromDate)
	SELECT	
			@BatchKey																			AS BatchKey, 
			C.id																				AS instrumentID,
			ISNULL(C.TrnType,''Unknown'')															AS Source, 
			ISNULL(BD.AccountNumber,''Unknown'')													AS AccountNumber,
			ISNULL(BD.AccountNumber,''Unknown'')													AS SafeAccountNumber,
			ISNULL(BD.AccountName,''Unknown'')													AS AccountName,		
			ISNULL(bd.BankBsb,''Unknown'')														AS BSB,
			NULL																				AS ExpiryDate,
			''N/A''																				AS Verified,
			NULL																				AS VerifiedAmount,
			ISNULL(C.TrnType,''Unknown'')															AS InstrumentType,
			ISNULL(bd.BankName,''Unknown'')														AS ProviderName,
			c.FromDate																			AS FromDate	
				
	FROM #Candidates C  with (nolock)
	LEFT OUTER JOIN [Acquisition].Intrabet.tblClientBankDetails BD WITH (NOLOCK) ON 					
					C.ID =Cast(Bd.BankID as varchar)
					AND BD.FromDate <= c.FromDate AND BD.ToDate > C.FromDate
						
	OPTION (RECOMPILE, FORCE ORDER, LOOP JOIN)
', @type = N'OBJECT', @module_or_batch = N'[Intrabet].[Sp_Instrument_EFT]', @hints = N'OPTION (RECOMPILE)'

EXEC sp_create_plan_guide @name = N'[BULK:Intrabet-sp_Instrument_Bpay:InsertIntoStaging(ThresholdDays=10)]', @stmt = N'
	INSERT	into Stage.Instrument (BatchKey,InstrumentId,Source,AccountNumber,SafeAccountNumber,AccountName,BSB,ExpiryDate,Verified,VerifyAmount,InstrumentType,ProviderName,FromDate)
	SELECT	
			@BatchKey						  AS BatchKey, 
			C.id							  AS instrumentID,
			ISNULL(C.trnType, ''Unknown'')		  AS Source, 
			ISNULL(Bp.RefNumber,''Unknown'')	  AS AccountNumber,
			ISNULL(Bp.RefNumber,''Unknown'')	  AS SafeAccountNumber,
			''N/A''						  AS AccountName,		
			ISNULL(Bp.BillerCode,''Unknown'')	  AS BSB,
			NULL							  AS ExpiryDate,
			''U/K''						  AS Verified,
			NULL							  AS VerifiedAmount,
			''BPAY''						  AS InstrumentType,
			''Unknown''						  AS ProviderName,
			c.FromDate					  AS FromDate		

	FROM #Candidates C  with (nolock)
	LEFT OUTER JOIN [Acquisition].Intrabet.tblClientBpayDetails BP WITH (NOLOCK) ON 
					C.ID =Cast(Bp.BPayID as varchar) 
					AND Bp.FromDate <= c.FromDate AND Bp.ToDate > C.FromDate

	OPTION (RECOMPILE, FORCE ORDER, LOOP JOIN)
', @type = N'OBJECT', @module_or_batch = N'[Intrabet].[Sp_Instrument_Bpay]', @hints = N'OPTION (RECOMPILE)'
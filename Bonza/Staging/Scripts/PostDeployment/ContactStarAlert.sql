﻿USE [msdb]
GO

/****** Object:  Job [DW_ContactStarAlert]    Script Date: 7/07/2016 4:39:37 PM ******/
BEGIN TRANSACTION
Declare @JobName varchar(1000)
Set @JobName = '$(JobPrefix)' + 'DW_ContactStarAlert';
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [ETL]    Script Date: 7/07/2016 4:39:37 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'ETL' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'ETL'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=@JobName, 
		@enabled=0, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'ETL', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Check Contact star data load status]    Script Date: 7/07/2016 4:39:37 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Check Contact star data load status', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'SET NOCOUNT ON;

DECLARE @recipient nvarchar(max) = ''BI.Service@williamhill.com.au;Gareth.White@williamhill.com.au;David.Robuck@williamhill.com.au'';
Declare @EmailLoadDate date= (select convert(date,max(BatchFromDate)) from Control.ETLController where ContactEmailFactStatus=1 and ContactEmailFactRowsProcessed>0 );
Declare @SmsLoadDate date= (select convert(date,max(BatchFromDate)) from Control.ETLController where ContactSmsFactStatus=1 and ContactSmsFactRowsProcessed>0 );
Declare @PushLoadDate date= (select convert(date,max(BatchFromDate)) from Control.ETLController where ContactPushFactStatus=1 and ContactPushFactRowsProcessed>0 );
declare @EmailBody varchar(1000)=Concat(
	''Hi
										
The date to which Contact star is loaded

Email : '',@EmailLoadDate,''
Sms : '',@SmsLoadDate,''
Push : '',@PushLoadDate,''
										
Regards
BI Service'');

	EXEC msdb.dbo.sp_send_dbmail																						
		@profile_name	= ''DataWareHouse_Auto_Mails'',
		@recipients		= @recipient,
		@body			= @EmailBody,
		@subject		= ''Contact star status update''', 
		@database_name=N'Staging', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Every morning 9 AM', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20160707, 
		@active_end_date=99991231, 
		@active_start_time=90000, 
		@active_end_time=235959, 
		@schedule_uid=N'3678a171-d1d3-4d03-9e03-b6f8838a64ec'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:

GO



/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

SET NOCOUNT ON

--Plan Guides
PRINT N'Adding Plan Guides...';
:r .\PlanGuide_Intrabet-sp_Account.sql
:r .\PlanGuide_Intrabet-sp_Event.sql
:r .\PlanGuide_Intrabet-sp_Instruments.sql

/* Static Data Inserts*/
PRINT N'Static Data Inserts...';
Exec Reference.Sp_InitialPopAll

--Synonyms
PRINT N'Updating Synonyms...';
:r .\LiveETLController.sql

--Operators
PRINT N'Operators...';
:r .\Operators.sql

/* SQL Server Agent Jobs */
PRINT N'SQL Server Agent Jobs...';
:r .\ETLJobs.sql
:r .\AuditDailyHealthCheckEmail.sql
:r .\AuditQueryMonitor.sql
:r .\AuditTableSizeMonitor.sql
:r .\DWHousekeeping.sql

SET NOCOUNT OFF

/*Version Info */
PRINT N'Setting Version Info...';
Insert into [$(Dimensional)].[Control].[Version] values ('$(VersionNumber)', 'Staging', getdate(), SUser_Name())

use [$(DatabaseName)]

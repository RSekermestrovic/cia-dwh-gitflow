CREATE PROC [Centrebet].[Sp_Balances]
(
	@BatchKey	int
)
AS 

SET TRANSACTION ISOLATION LEVEL SNAPSHOT;

BEGIN TRY

	EXEC [Control].Sp_Log	@BatchKey,'Sp_Balances','AccountBalances','Start'
	BEGIN TRY DROP TABLE #AccountBalances END TRY BEGIN CATCH END CATCH -- Speculative attempt to drop pre-existing Temp table, don't care if it fails due to non-existence
	SELECT  @BatchKey BatchKey, CONVERT(date, a.FromDate) DayDate, a.AccountId, a.FromDate, a.ClientId, a.AccountNumber, a.AccountEnabled, a.Status, a.Balance ClientBalance
	INTO	#AccountBalances
	FROM	[$(Acquisition)].Intrabet.tblAccounts a
			INNER JOIN (SELECT	AccountId, MIN(FromDate) FromDate FROM [$(Acquisition)].Intrabet.tblAccounts GROUP BY AccountId) x ON (x.AccountID = a .AccountID AND x.FromDate = a.FromDate)
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,'Sp_Balances','Transactions','Start', @RowsProcessed = @@ROWCOUNT
	BEGIN TRY DROP TABLE #Transactions END TRY BEGIN CATCH END CATCH -- Speculative attempt to drop pre-existing Temp table, don't care if it fails due to non-existence
	SELECT	ClientId, AccountNumber, FromDay, ToDay, TransactionType, PendingTransactionId,TransactionID, -- Group together all transactions under common day ranges to reduce the number of rows unless they are pending transactions which need to feature in a transaction level extract later
			SUM(PendingAmount) PendingBalance,
			MIN(TransactionDate) FirstDate,
			MAX(TransactionDate) LastDate,
			SUM(Amount) Amount,
			COUNT(*) TransactionCount
	INTO	#Transactions
	FROM	(	SELECT	b.ClientId, b.AccountNumber, convert(date,b.FromDate) FromDay, convert(date,b.ToDate) ToDay, m.TransactionType,
						CASE 
							WHEN m.Pending <> 'Y' THEN NULL 
							WHEN b.ToDate = '9999-12-30' THEN NULL
							ELSE TransactionId 
						END PendingTransactionId, 
						CASE 
							WHEN m.Pending <> 'Y' THEN 0 
							WHEN b.ToDate = '9999-12-30' THEN 0 
							WHEN m.TransactionType = 'Withdrawal' THEN ISNULL(-Amount,0) 
							ELSE ISNULL(Amount,0)
						END PendingAmount, 
						TransactionDate,
						b.TransactionID,
						ISNULL(Amount,0) Amount
				FROM	[$(Acquisition)].Centerbet.tblTransactions b 
--									LEFT JOIN [$(Acquisition)].Centerbet.Latency f ON (f.OriginalDate = b.FromDate)
--									LEFT JOIN [$(Acquisition)].Centerbet.Latency t ON (t.OriginalDate = b.ToDate) -- allow for 31/12/9999
						INNER JOIN (SELECT	TransactionCode, 
											TransactionType, 
											MIN(CASE 
													WHEN TransactionCode = 17 THEN 'Y' -- Temporary measure until understand why Tran Code 17 has a 'Complete' mapping?
													WHEN TransactionCode = 8 THEN 'N' -- Temporary measure until understand why Tran Code 8 does not have a 'Complete' mapping?
													WHEN TransactionCode IN (31,32,33) THEN 'N'
													WHEN TransactionStatus = 'Complete' THEN 'N' 
													ELSE 'Y' 
												END) Pending 
									FROM	Intrabet.TransactionTypeMapping 
									WHERE 	TransactionType IN ('Deposit','Withdrawal')
									GROUP BY TransactionCode, TransactionType
								) m ON (m.TransactionCode = b.TransactionCode)
--							WHERE	b.ToDate > @FromDate AND b.FromDate < @ToDate
			) y
	GROUP BY ClientId, AccountNumber, FromDay, ToDay, TransactionType, PendingTransactionId,TransactionID
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,'Sp_Balances','Transaction Balances','Start', @RowsProcessed = @@ROWCOUNT
	BEGIN TRY DROP TABLE #TransactionBalances END TRY BEGIN CATCH END CATCH -- Speculative attempt to drop pre-existing Temp table, don't care if it fails due to non-existence
	SELECT	x.BatchKey, x.DayDate, x.AccountId,
			SUM(CASE t.TransactionType WHEN 'Withdrawal' THEN t.PendingBalance ELSE 0 END) PendingDebitBalance,
			MIN(CASE t.TransactionType WHEN 'Deposit' THEN t.FirstDate ELSE NULL END) FirstCreditDate,
			MAX(CASE t.TransactionType WHEN 'Deposit' THEN t.LastDate ELSE NULL END) LastCreditDate,
			MIN(CASE t.TransactionType WHEN 'Deposit' THEN t.TransactionID ELSE NULL END) FirstCreditId,
			MAX(CASE t.TransactionType WHEN 'Deposit' THEN t.TransactionID ELSE NULL END) LastCreditId,
			SUM(CASE t.TransactionType WHEN 'Deposit' THEN t.Amount ELSE 0 END) LifetimeCredit,
			SUM(CASE t.TransactionType WHEN 'Deposit' THEN t.TransactionCount ELSE 0 END) CreditCount,
			MIN(CASE t.TransactionType WHEN 'Withdrawal' THEN t.FirstDate ELSE NULL END) FirstDebitDate,
			MAX(CASE t.TransactionType WHEN 'Withdrawal' THEN t.LastDate ELSE NULL END) LastDebitDate,
			SUM(CASE t.TransactionType WHEN 'Withdrawal' THEN t.Amount ELSE 0 END) LifetimeDebit,
			SUM(CASE t.TransactionType WHEN 'Withdrawal' THEN t.TransactionCount ELSE 0 END) DebitCount
	INTO	#TransactionBalances
	FROM	#AccountBalances x
			INNER JOIN #Transactions t ON (t.clientid = x.clientid and t.AccountNumber = x.accountnumber and t.FromDay <= x.DayDate and t.ToDay > x.DayDate)
	GROUP BY BatchKey, DayDate, AccountId
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,'Sp_Balances','BonusBalances','Start', @RowsProcessed = @@ROWCOUNT
	DROP TABLE #Transactions

	BEGIN TRY DROP TABLE #BonusBalances END TRY BEGIN CATCH END CATCH -- Speculative attempt to drop pre-existing Temp table, don't care if it fails due to non-existence
	SELECT	BatchKey, DayDate, AccountId, 
			SUM(Amount) ClientBalance, 
			MIN(CreditDate) FirstCreditDate, 
			MAX(CreditDate) LastCreditDate, 
			SUM(Credit) LifetimeCredit, 
			SUM(CreditCount) CreditCount, 
			MIN(DebitDate) FirstDebitDate, 
			MAX(DebitDate) LastDebitDate, 
			SUM(Debit) LifetimeDebit, 
			SUM(DebitCount) DebitCount
	INTO	#BonusBalances
	FROM	(	SELECT	x.BatchKey, x.DayDate, x.AccountId, 
						CONVERT(money,0) Amount, 
						BetDate CreditDate, 
						t.AmountToWin + t.AmountToPlace Credit, 
						1 CreditCount, 
						CONVERT(datetime2(3),NULL) DebitDate, 
						CONVERT(money,0) Debit, 
						0 DebitCount
				FROM	#AccountBalances x
						INNER JOIN [$(Acquisition)].Centerbet.tblBets t ON (t.ClientID = x.ClientID and t.FromDate <= x.FromDate and t.ToDate > x.FromDate AND ISNULL(t.FreeBetId,0) <> 0)
			) y
	GROUP BY BatchKey, DayDate, AccountId
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,'Sp_Balances','Bets','Start', @RowsProcessed = @@ROWCOUNT
	BEGIN TRY DROP TABLE #Bets END TRY BEGIN CATCH END CATCH -- Speculative attempt to drop pre-existing Temp table, don't care if it fails due to non-existence
	SELECT	ClientId, SIGN(ISNULL(FreeBetId,0)) FreeBet, CONVERT(date,b.fromdate) FromDay, CONVERT(date,b.todate) ToDay, 
			SUM(ISNULL(AmountToWin,0)+ISNULL(AmountToPlace,0)) Stake, SUM(ISNULL(PayoutWin,0)+ISNULL(PayoutPlace,0)) Payout, 
			MIN(BetId) FirstBetId, 
			MAX(BetId) LastBetId,
			COUNT(*) BetsPlaced
	INTO	#Bets
	FROM	[$(Acquisition)].Centerbet.tblBets b
	WHERE	b.Valid = 1
			AND (b.AmountToWin > 0 or b.AmountToPlace > 0) -- Limit to primary leg of multis so that the count returns number of bets, not legs
	GROUP BY clientid, SIGN(ISNULL(FreeBetId,0)), CONVERT(date,b.fromdate), CONVERT(date,b.todate)
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,'Sp_Balances','BetBalances','Start', @RowsProcessed = @@ROWCOUNT
	BEGIN TRY DROP TABLE #BetBalances END TRY BEGIN CATCH END CATCH -- Speculative attempt to drop pre-existing Temp table, don't care if it fails due to non-existence
	SELECT	BatchKey, DayDate, x.FromDate, AccountId, FreeBet,
			SUM(Stake) LifetimeStake,
			SUM(Payout) LifetimePayout, 
			MIN(FirstBetId) FirstBetId, 
			MAX(LastBetId) LastBetId,
			SUM(BetsPlaced) BetsPlaced
	INTO	#BetBalances
	FROM	#AccountBalances x
			INNER JOIN #Bets b ON (b.clientid = x.clientid AND x.AccountNumber = 1 AND b.FromDay <= x.DayDate AND b.ToDay > x.DayDate)
	GROUP BY BatchKey, DayDate, x.FromDate, AccountId, FreeBet
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,'Sp_Balances','FirstLastBets','Start', @RowsProcessed = @@ROWCOUNT
	DROP TABLE #Bets

	BEGIN TRY DROP TABLE #FirstLastBets END TRY BEGIN CATCH END CATCH -- Speculative attempt to drop pre-existing Temp table, don't care if it fails due to non-existence
	SELECT	x.BatchKey, x.DayDate, b.BetID, b.BetDate, b.BetType, b.AmountToPlace, b.AmountToWin,'Straight' BetGroupType, b.Channel, b.EventID, IsNull(e.EventType,-1) ClassID
	INTO	#FirstLastBets
	FROM	(	SELECT DISTINCT BatchKey, DayDate, FromDate, FirstBetId BetId FROM #BetBalances
							UNION
							SELECT DISTINCT BatchKey, DayDate, FromDate, LastBetId BetId FROM #BetBalances
			) x
			INNER JOIN [$(Acquisition)].Centerbet.tblBets b ON (b.BetId = x.BetId AND b.FromDate <= x.FromDate AND b.ToDate > x.FromDate)
			LEFT JOIN	[$(Acquisition)].Centerbet.tblEvents e ON (e.EventID = b.EventID AND e.FromDate <= x.FromDate AND e.ToDate > x.FromDate) -- join to Events to get the Event Type which is used as natural key for ClassId
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,'Sp_Balances','BetBalanceFirstLast','Start', @RowsProcessed = @@ROWCOUNT
	BEGIN TRY DROP TABLE #BetBalancesFirstLast END TRY BEGIN CATCH END CATCH -- Speculative attempt to drop pre-existing Temp table, don't care if it fails due to non-existence
	SELECT	x.*, 
			f.BetDate FirstBetDate, f.BetType FirstBetType, f.AmountToPlace FirstAmountToPlace, f.AmountToWin FirstAmountToWin, f.BetGroupType FirstBetGroupType, f.Channel FirstChannel, f.EventID FirstEventId, f.ClassID FirstClassId,
			l.BetDate LastBetDate, l.BetType LastBetType, l.AmountToPlace LastAmountToPlace, l.AmountToWin LastAmountToWin, l.BetGroupType LastBetGroupType, l.Channel LastChannel, l.EventID LastEventId, l.ClassID LastClassId
	INTO	#BetBalancesFirstLast
	FROM	#BetBalances x
			LEFT JOIN #FirstLastBets f ON (f.BatchKey = x.BatchKey AND f.DayDate = x.DayDate AND f.BetId = x.FirstBetId)
			LEFT JOIN #FirstLastBets l ON (l.BatchKey = x.BatchKey AND l.DayDate = x.DayDate AND l.BetId = x.LastBetId)
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,'Sp_Balances','Atomic','Start', @RowsProcessed = @@ROWCOUNT

	DROP TABLE #BetBalances
	DROP TABLE #FirstLastBets

	TRUNCATE TABLE	Centrebet.Balance
	INSERT	Centrebet.Balance
			(	BatchKey, DayDate, LedgerId, FreeBet, FromDate, AccountEnabled, Status, 
				ClientBalance, UnsettledBalance, PendingDebitbalance, LifetimeStake, LifetimePayout, 
				FirstCreditDate, LastCreditDate, LifetimeCredit, FirstDebitDate, LastDebitDate, LifetimeDebit,
				FirstBetId, FirstBetDate, FirstBetType, FirstChannel, FirstEventId, FirstClassId,
				LastBetId, LastBetDate, LastBetType, LastChannel, LastEventId, LastClassId, LifetimebetsPlaced,FirstWinPlace, LastWinPlace, FirstBetGroupType, LastBetGroupType, FirstCreditId, LastCreditId)
	SELECT  x.BatchKey, x.DayDate, x.AccountId, x.FreeBet, x.FromDate, x.AccountEnabled, x.Status, 
			x.ClientBalance, 0 UnsettledBalance, x.PendingDebitbalance, ISNULL(b.LifetimeStake,0) LifetimeStake, ISNULL(b.LifetimePayout,0) LifetimePayout, 
			x.FirstCreditDate, x.LastCreditDate, x.LifetimeCredit, 
			x.FirstDebitDate, x.LastDebitDate, x.LifetimeDebit,
			b.FirstBetId, b.FirstBetDate, b.FirstBetType, b.FirstChannel, b.FirstEventId, b.FirstClassId,
			b.LastBetId, b.LastBetDate, b.LastBetType, b.LastChannel, b.LastEventId, b.LastClassId, b.BetsPlaced,
			CASE 
				WHEN b.FirstAmountToPlace > 0 and b.FirstAmountToWin = 0 THEN 2 
				WHEN b.FirstAmountToWin > 0 and b.FirstAmountToPlace = 0 THEN 1
				WHEN b.FirstAmountToWin > 0 and b.FirstAmountToPlace > 0 THEN 3
				ELSE NULL END,
			CASE 
				WHEN b.LastAmountToPlace > 0 and b.LastAmountToWin = 0 THEN 2
				WHEN b.LastAmountToWin > 0 and b.LastAmountToPlace = 0 THEN 1
				WHEN b.LastAmountToWin > 0 and b.LastAmountToPlace > 0 THEN 3
				ELSE NULL END,
			b.FirstBetGroupType, b.LastBetGroupType, 
			x.FirstCreditId, x.LastCreditId
	FROM	(	SELECT  a.BatchKey, a.DayDate, a.AccountId, 0 FreeBet, a.FromDate, a.AccountEnabled, a.Status, 
						a.ClientBalance, ISNULL(t.PendingDebitBalance,0) PendingDebitBalance, 
						t.FirstCreditDate, t.LastCreditDate, ISNULL(t.LifetimeCredit,0) LifetimeCredit, 
						t.FirstDebitDate, t.LastDebitDate, ISNULL(t.LifetimeDebit,0) LifetimeDebit, t.FirstCreditId, t.LastCreditId,
						CASE WHEN t.BatchKey IS NULL THEN 0 ELSE 1 END Matched
				FROM	#AccountBalances a
						LEFT JOIN #TransactionBalances t ON (t.BatchKey = a.BatchKey AND t.DayDate = a.DayDate AND t.AccountID = a.AccountID)
				UNION ALL
				SELECT  a.BatchKey, a.DayDate, a.AccountId, 1 FreeBet, a.FromDate, a.AccountEnabled, a.Status, 
						ISNULL(f.ClientBalance,0) ClientBalance, 0 PendingDebitbalance, 
						f.FirstCreditDate, f.LastCreditDate, ISNULL(f.LifetimeCredit,0) LifetimeCredit, 
						f.FirstDebitDate, f.LastDebitDate, ISNULL(f.LifetimeDebit,0) LifetimeDebit, null, null,
						CASE WHEN f.BatchKey IS NULL THEN 0 ELSE 1 END Matched
				FROM	#AccountBalances a
						LEFT JOIN #BonusBalances f ON (f.BatchKey = a.BatchKey AND f.DayDate = a.DayDate AND f.AccountID = a.AccountID)
			) x
			LEFT JOIN #BetBalancesFirstLast b ON (b.BatchKey = x.BatchKey AND b.DayDate = x.DayDate AND b.AccountID = x.AccountID AND b.FreeBet = x.FreeBet)
	WHERE	b.BatchKey is not null or x.Matched = 1
	OPTION (RECOMPILE) -- 215890

	EXEC [Control].Sp_Log	@BatchKey,'Sp_Balances',NULL,'Success', @RowsProcessed = @@ROWCOUNT

	DROP TABLE #AccountBalances
	DROP TABLE #TransactionBalances
	DROP TABLE #BonusBalances
	DROP TABLE #BetBalancesFirstLast

END TRY
BEGIN CATCH
	declare @ErrorMessage nvarchar(max), @ErrorSeverity int, @ErrorState int;
	select @ErrorMessage = ERROR_MESSAGE() + ' Line ' + cast(ERROR_LINE() as nvarchar(5)), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION;
	EXEC [Control].Sp_Log	@BatchKey,'Sp_Balances',NULL,'Failed',@ErrorMessage
	raiserror (@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH

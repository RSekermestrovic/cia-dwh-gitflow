﻿CREATE FUNCTION [Control].[ThresholdDays]
(	
	@PlanGuideName sysname
)
RETURNS int
AS
BEGIN
	DECLARE @Result sysname
	SET @Result = REPLACE(@PlanGuideName,' ','')
	IF @Result LIKE '%(ThresholdDays=%)%'
		BEGIN
			SET @Result = RIGHT(@Result,LEN(@Result) - charindex('(ThresholdDays=',@Result) - 14)
			SET @Result = LEFT(@Result,charindex(')',@Result) - 1)
		END
	ELSE
		SET @Result = '2'
	RETURN ISNULL(TRY_CONVERT(int,@Result),2)
END

﻿CREATE FUNCTION [Control].[ParameterValue]
(	@Class varchar(30),
	@Name varchar(30)
) RETURNS varchar(100) 
AS
BEGIN
	DECLARE @Value varchar(100)
	SELECT @Value = Value FROM Control.Parameter WHERE Class = @Class AND Name = @Name
	RETURN @Value
END


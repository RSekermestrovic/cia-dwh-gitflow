﻿CREATE TABLE [Control].[ETLProcessMapping] (
    [SequenceID]          INT           Unique NOT NULL,
    [ProcessName]    VARCHAR (255) NOT NULL,
    [SchemaName]     [sysname]     NOT NULL,
    [ProcedureName]  [sysname]     NOT NULL,
    PRIMARY KEY CLUSTERED ([ProcessName] ASC, [SchemaName] ASC, [SequenceID] ASC, [ProcedureName] ASC)
);


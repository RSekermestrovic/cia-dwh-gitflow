﻿CREATE TABLE [Control].[Log] (
    [LogID]         INT             IDENTITY (1, 1) NOT NULL,
    [BatchKey]      INT             NOT NULL,
    [Procedure]     NVARCHAR (128)  NOT NULL,
    [Step]          NVARCHAR (128)  NULL,
    [Status]        NVARCHAR (10)   NULL,
    [StartDateTime] DATETIME2 (7)   NULL,
    [EndDateTime]   DATETIME2 (7)   NULL,
    [ErrorMessage]  NVARCHAR (1000) NULL,
	[RowsProcessed]	BIGINT			NULL
) ON [par_sch_int_Stage] ([BatchKey])
WITH (DATA_COMPRESSION = ROW );

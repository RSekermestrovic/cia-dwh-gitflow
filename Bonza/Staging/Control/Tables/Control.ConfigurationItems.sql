﻿CREATE TABLE [Control].[ConfigurationItems] (
    [ConfigurationItem] VARCHAR (255) NOT NULL,
    [Value]             VARCHAR (255) NOT NULL,
    [Description]       VARCHAR (255) NOT NULL,
    PRIMARY KEY CLUSTERED ([ConfigurationItem] ASC)
);


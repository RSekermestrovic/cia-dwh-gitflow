﻿CREATE PROCEDURE [Control].[ETLPositionFact]
	(
		@BatchKey	int	= NULL
	)
AS 

	DECLARE	@RowsProcessed				INT = 0

	SET TRANSACTION ISOLATION LEVEL READ COMMITTED
    BEGIN TRAN ETLPositionFact
	
BEGIN TRY

	IF ISNUMERIC(@BatchKey)<>1																				-- Auto Run - Pick next best batch to run
		SELECT	@BatchKey = MIN(BatchKey) 
		FROM	[Control].[ETLController] c
		WHERE	ISNULL(PositionFactStatus,0)=0
				AND EXISTS (SELECT 1 FROM [Control].[ETLController] c1 WHERE c1.BatchKey = c.BatchKey AND ISNULL(c1.PositionStagingStatus,0) = 1) -- Position Staging completed successfully in current batch
				AND EXISTS (SELECT 1 FROM [Control].[ETLController] c1 WHERE c1.BatchKey = c.BatchKey AND ISNULL(c1.DimensionStatus,0) = 1) -- Dimension completed successfully in current batch
				AND NOT EXISTS (SELECT 1 FROM [Control].[ETLController] c1 WHERE c1.BatchKey < c.BatchKey AND ISNULL(c1.PositionFactStatus,0) <> 1) -- No failed Posiion Fact steps in earlier batches
				AND NOT EXISTS (SELECT 1 FROM [Control].[ETLController] c2 WHERE c2.BatchKey < c.BatchKey AND ISNULL(c2.BatchStatus,0) <> 1 AND Control.ParameterValue ('ETL','SequentialBatches') <> 'No') -- Previous batch complete or Sequential batches overidden

	IF ISNUMERIC(@BatchKey)=1																								-- Only LoadKPI after TransactionDimension is loaded.
	BEGIN
	
		UPDATE [Control].[ETLController] SET PositionFactStartDate=GETDATE() WHERE BatchKey=@BatchKey				-- Set StartTime for Position FACT Load

		COMMIT TRAN ETLPositionFact -- Commit the ETL Controller initiation and switch to READ UNCOMMITTED for rest of processing with no contention in Staging/Dimensional
		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
		BEGIN TRAN ETLPositionFact 

		EXEC Dimension.Sp_PositionFactLoad @BatchKey, @RowsProcessed OUTPUT -- RunPosition Load
							
		UPDATE [Control].[ETLController] SET PositionFactEndDate=GETDATE(), PositionFactStatus=1, PositionFactRowsProcessed=@RowsProcessed WHERE BatchKey=@BatchKey -- Set 'Success' RunStatus for Position FACT Load
				
		COMMIT TRAN ETLPositionFact -- Commit main body of updates to progreess non-esential subsequent updates in their own transaction
		BEGIN TRAN ETLPositionFact
				
		UPDATE STATISTICS [$(Dimensional)].FACT.Position																			-- Update STATS on Fact Table

		UPDATE [$(Dimensional)].Control.CompleteDate SET PositionCompleteDate = ISNULL((SELECT BatchToDate FROM [Control].[ETLController] WHERE BatchKey = @BatchKey),PositionCompleteDate)
				
	END

	COMMIT TRAN ETLPositionFact

END TRY
		
BEGIN CATCH
	DECLARE @ErrorMsg VARCHAR(4000)
	SET @ErrorMsg=ERROR_MESSAGE()

	ROLLBACK TRAN ETLPositionFact

	Declare @EmailSubject varchar(2000)
	Set @EmailSubject = '*** Urgent *** ' + '$(ServerName)' + ' - PositionFact ETL Failed,check ETL log/ETLController for error details'

	EXEC msdb.dbo.sp_send_dbmail																							-- Send Error mail to DW Team
	@profile_name	= 'DataWareHouse_Auto_Mails',
	@recipients		= 'bi.alert@WilliamHill.com.au',
	@body			=  @ErrorMsg,
	@subject		=  @EmailSubject;

	UPDATE [Control].[ETLController] SET PositionFactEndDate=GETDATE(), PositionFactStatus=-1, ErrorMessage = @ErrorMsg WHERE BatchKey=@BatchKey;
	THROW;

END CATCH

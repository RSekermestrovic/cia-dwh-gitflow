CREATE PROCEDURE [Control].[BulkPlanGuide]
(	
	@ObjectName		varchar(128) = NULL,	-- Optional - Nominates a stored procedure or function for which all matching plan guides will be enabled/disabled. A default of NULL indicates all plan guides for any stored procedure
	@Days			int = 0,				-- Optional - Specifies the number of days elapsed for which the stored procedure will be run - for comparison with the threshold parameter embedded in the Plan Guide name to determine whether the plan guide should be enabled or disabled. Default of 0 disables all Plan Guides
	@DryRun			bit = 0					-- Optional - (0=FALSE/1=TRUE) - Allows the procedure to be dry run whereby it will report all of its intended actions but not actually execute them
											-- NOTE: if a Plan Guide name does not contain the (ThresholdDays=x) tag, then the threshold is assumed to be the default of 2 days. i.e. any elpsed period greater than one day requires bulk Plan Guides 
)
AS

	SET NOCOUNT ON 

	DECLARE	@Tag varchar(30) = 'ThresholdDays'

	DECLARE	@i int,
			@SQL varchar(MAX)


	SELECT	ROW_NUMBER() OVER (ORDER BY name DESC) i,
			'sp_control_plan_guide ''' + CASE is_disabled WHEN 1 THEN 'ENABLE' ELSE 'DISABLE' END + ''', ''' + name + '''' sql
	INTO	#PlanGuides
	FROM	sys.plan_guides
	WHERE	scope_type_desc = 'OBJECT'
			AND is_disabled = CASE WHEN @Days >= control.thresholddays(name) THEN 1 ELSE 0 END
			AND name LIKE 'BULK:%'
			AND (scope_object_id = OBJECT_ID(@ObjectName) OR @ObjectName IS NULL)

	SET @i = @@ROWCOUNT

	IF @DryRun = 1 
		BEGIN
			SELECT sql PlanGuide FROM #PlanGuides ORDER BY i DESC
			SET @i = 0
		END
	ELSE IF @i > 0
		BEGIN
			WHILE @i > 0 AND @DryRun = 0
				BEGIN
					SELECT	@SQL = sql FROM	#PlanGuides	WHERE i = @i
					EXEC(@SQL)
					SET @i = @i - 1	
				END
			SET @i = 1
		END
	ELSE
		SET @i = 0

	RETURN @i


Create Procedure [Control].ETLKPI
(
		@BatchKey		int	= NULL
)
AS 

	DECLARE	@RowsProcessed				INT = 0;

	SET TRANSACTION ISOLATION LEVEL READ COMMITTED
    BEGIN TRAN ETLKPIFact 

BEGIN TRY

	IF ISNUMERIC(@BatchKey) <> 1																				-- Auto Run - Pick next best batch to run
		SELECT	@BatchKey = MIN(BatchKey) 
		FROM	[Control].[ETLController] c
		WHERE	ISNULL(KPIFactStatus,0)=0
				AND EXISTS (SELECT 1 FROM [Control].[ETLController] c1 WHERE c1.BatchKey = c.BatchKey AND ISNULL(c1.KPIRecursiveStatus,0) = 1) -- KPI Recursive completed successfully in current batch
				AND EXISTS (SELECT 1 FROM [Control].[ETLController] c1 WHERE c1.BatchKey < c.BatchKey AND ISNULL(c1.DimensionStatus,0) = 1) -- Dimension completed successfully in current batch
				AND NOT EXISTS (SELECT 1 FROM [Control].[ETLController] c1 WHERE c1.BatchKey < c.BatchKey AND ISNULL(c1.KPIFactStatus,0) <> 1) -- No failed KPI steps in earlier batches
				AND NOT EXISTS (SELECT 1 FROM [Control].[ETLController] c1 WHERE c1.BatchKey < c.BatchKey AND ISNULL(c1.BatchStatus,0) <> 1 AND Control.ParameterValue ('ETL','SequentialBatches') <> 'No') -- Previous batch complete or Sequential batches overidden

	IF ISNUMERIC(@BatchKey)=1
	BEGIN
	
		UPDATE [Control].[ETLController] SET KPIFactStartDate=GETDATE() WHERE BatchKey=@BatchKey						-- Set StartTime for KPI Load

		COMMIT TRAN ETLKPIFact -- Commit the ETL Controller initiation and switch to READ UNCOMMITTED for rest of processing due to contention issues and phantom updates from constantly updated Acquisition
		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
		BEGIN TRAN ETLKPIFact

		EXEC [Dimension].Sp_KPIFact @BatchKey, @RowsProcessed OUTPUT														-- Run KPI Load
			
		UPDATE [Control].[ETLController] SET KPIFactEndDate=GETDATE(), KPIFactStatus=1, KPIFactRowsProcessed=@RowsProcessed WHERE BatchKey=@BatchKey	
				
		COMMIT TRAN ETLKPIFact -- Commit main body of updates to progreess non-esential subsequent updates in their own transaction
		BEGIN TRAN ETLKPIFact
				
		UPDATE STATISTICS [$(Dimensional)].FACT.KPI																					-- Update STATS on Fact Table

		UPDATE [$(Dimensional)].Control.CompleteDate SET KPICompleteDate = ISNULL((SELECT BatchToDate FROM [Control].[ETLController] WHERE BatchKey = @BatchKey),KPICompleteDate)

	END

	COMMIT TRAN ETLKPIFact

END TRY
		
BEGIN CATCH
	DECLARE @ErrorMsg VARCHAR(4000)
	SET @ErrorMsg=ERROR_MESSAGE()

	ROLLBACK TRAN ETLKPIFact

	Declare @EmailSubject varchar(2000)
	Set @EmailSubject = '*** Urgent *** ' + '$(ServerName)' + ' - KPI ETL Failed,check ETL log/ETLController for error details'

	EXEC msdb.dbo.sp_send_dbmail																							-- Send Error mail to DW Team
	@profile_name	= 'DataWareHouse_Auto_Mails',
	@recipients		= 'bi.alert@WilliamHill.com.au',
	@body			=  @ErrorMsg,
	@subject		=  @EmailSubject;
			
	UPDATE [Control].[ETLController] SET KPIFactEndDate=GETDATE(), KPIFactStatus=-1, ErrorMessage = @ErrorMsg WHERE BatchKey=@BatchKey;
	THROW;

END CATCH

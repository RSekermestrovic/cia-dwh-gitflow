CREATE PROCEDURE [Control].[ETLAtomic]
(
	@BatchKey int	= NULL
)

AS 

	DECLARE	@FromDate		DATETIME,
	 		@ToDate			DATETIME,
			@RowsProcessed	INT = 0

	SET TRANSACTION ISOLATION LEVEL READ COMMITTED
	BEGIN TRAN ETLAtomic

BEGIN TRY
	
	IF ISNUMERIC(@BatchKey)<>1																				-- Auto Run - Pick next best batch to run
		SELECT	@BatchKey = MIN(BatchKey) 
		FROM	[Control].[ETLController] c
		WHERE	ISNULL(AtomicStatus,0)=0
				AND NOT EXISTS (SELECT 1 FROM [Control].[ETLController] c1 WHERE c1.BatchKey < c.BatchKey AND ISNULL(c1.AtomicStatus,0) <> 1) -- No failed atomic steps in earlier batches
				AND NOT EXISTS (SELECT 1 FROM [Control].[ETLController] c2 WHERE c2.BatchKey < c.BatchKey AND ISNULL(c2.BatchStatus,0) <> 1 AND Control.ParameterValue ('ETL','SequentialBatches') <> 'No') -- Previous batch complete or Sequential batches overidden

	IF ISNUMERIC(@BatchKey)=1	-- Only Run Atomic if there is an atomic step that needs to run and the previous batch has finished
	BEGIN
		SELECT @FromDate= BatchFromDate, @ToDate = BatchToDate FROM [Control].[ETLController] WHERE BatchKey=@BatchKey

		UPDATE [Control].[ETLController] SET AtomicStartDate=GETDATE() WHERE BatchKey=@BatchKey		-- Set StartTime for Atomic

		COMMIT TRAN ETLAtomoic -- Commit the ETL Controller initiation and switch to SNAPSHOT for rest of processing due to contention issues and phantom updates from constantly updated Acquisition
		SET TRANSACTION ISOLATION LEVEL SNAPSHOT
		BEGIN TRAN ETLAtomic
                     
		EXEC Intrabet.sp_AtomicBets				@BatchKey, @FromDate, @ToDate, @RowsProcessed OUTPUT                                                                           
		EXEC Intrabet.Sp_AtomicOpeningBalances	@BatchKey, @FromDate, @ToDate, @RowsProcessed OUTPUT                                                                           

		UPDATE [Control].[ETLController] SET AtomicEndDate=GETDATE(), AtomicStatus=1, AtomicRowsProcessed=@RowsProcessed WHERE BatchKey=@BatchKey         -- Set EndTime for Atomic
	END         
		
	COMMIT TRAN ETLAtomic

END TRY
BEGIN CATCH
	DECLARE @ErrorMsg VARCHAR(4000)
	SET @ErrorMsg=ERROR_MESSAGE()

	ROLLBACK TRAN ETLAtomic

	Declare @EmailSubject varchar(2000)
	Set @EmailSubject = '*** Urgent *** ' + '$(ServerName)' + ' - Atomic ETL Failed,check ETL log/ETLController for error details'

	EXEC msdb.dbo.sp_send_dbmail																							-- Send Error mail to DW Team
	@profile_name	= 'DataWareHouse_Auto_Mails',
	@recipients		= 'bi.alert@WilliamHill.com.au',
	@body			=  @ErrorMsg,
	@subject		=  @EmailSubject;
				
    UPDATE [Control].[ETLController] SET AtomicEndDate=GETDATE(), AtomicStatus=-1, ErrorMessage = @ErrorMsg WHERE BatchKey=@BatchKey;
    THROW
END CATCH

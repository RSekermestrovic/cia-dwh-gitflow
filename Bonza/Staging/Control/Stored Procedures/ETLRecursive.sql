﻿CREATE PROCEDURE [Control].[ETLRecursive]
(
	@BatchKey int	= NULL
)
AS 

	SET TRANSACTION ISOLATION LEVEL READ COMMITTED
	BEGIN TRAN ETLRecursive 

BEGIN TRY

	IF ISNUMERIC(@BatchKey) <> 1																				-- Auto Run - Pick next best batch to run
		SELECT	@BatchKey = MIN(BatchKey) 
		FROM	[Control].[ETLController] c
		WHERE	ISNULL(RecursiveStatus,0)=0
				AND EXISTS (SELECT 1 FROM [Control].[ETLController] c1 WHERE c1.BatchKey = c.BatchKey AND ISNULL(c1.StagingStatus,0) = 1) -- Staging completed successfully in current batch
				AND NOT EXISTS (SELECT 1 FROM [Control].[ETLController] c1 WHERE c1.BatchKey < c.BatchKey AND ISNULL(c1.RecursiveStatus,0) <> 1) -- No failed recursive steps in earlier batches
				AND NOT EXISTS (SELECT 1 FROM [Control].[ETLController] c1 WHERE c1.BatchKey < c.BatchKey AND ISNULL(c1.TransactionStatus,0) <> 1) -- No failed fact transaction steps in earlier batches
				AND NOT EXISTS (SELECT 1 FROM [Control].[ETLController] c1 WHERE c1.BatchKey < c.BatchKey AND ISNULL(c1.BalanceFactStatus,0) <> 1) -- No failed fact balance steps in earlier batches
				AND NOT EXISTS (SELECT 1 FROM [Control].[ETLController] c1 WHERE c1.BatchKey < c.BatchKey AND ISNULL(c1.BatchStatus,0) <> 1 AND Control.ParameterValue ('ETL','SequentialBatches') <> 'No') -- Previous batch complete or Sequential batches overidden

	IF ISNUMERIC(@BatchKey) = 1	-- Only Run if a suitable batch has been nominated or identified above
	BEGIN

		UPDATE [Control].[ETLController] SET RecursiveStartDate=GETDATE() WHERE BatchKey=@BatchKey													-- Set StartTime for Recursive

		COMMIT TRAN ETLRecursive -- Commit the ETL Controller initiation and switch to READ UNCOMMITTED for rest of processing with no contention in Staging/Dimensional
		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
		BEGIN TRAN ETLRecursive 

		EXEC [Recursive].[Sp_Transaction_DeDup]						@BatchKey		-- DeDup
		EXEC [Recursive].[Sp_Transaction_CleanDates]				@BatchKey		-- Clean Dates
		EXEC [Recursive].[Sp_Transaction_Unfinalised]				@BatchKey		-- Unfinalises for Resettlements/Cancellations
		EXEC [Recursive].[Sp_Transaction_UnbalancedWithdrawals]		@BatchKey		-- Unbalanced Withdrawals
		--EXEC [Recursive].[Sp_Transaction_Adjustments]				@BatchKey		-- Rec Adjustments
		EXEC [Recursive].[Sp_Transaction_AddDedup]					@BatchKey		-- Add Dedup after Date Clean
		EXEC [Recursive].[Sp_Transaction_OtherAdj]					@BatchKey		-- Add Adjustments

		EXEC [Recursive].[Sp_Balance_FactCheck]						@BatchKey		-- Balance Recursives

		UPDATE [Control].[ETLController] SET RecursiveEndDate=GETDATE(), RecursiveStatus=1 WHERE BatchKey=@BatchKey								-- Set 'Success' RunStatus for Recursive

		COMMIT TRAN ETLRecursive -- Commit the main body of work and switch to seperate transaction to update rows processed, whihc is informational only
		BEGIN TRAN ETLRecursive 

		UPDATE [Control].[ETLController]																										-- Set RowsProcessed for Recursive
				SET RecursiveRowsProcessed=(SELECT COUNT(*) FROM Stage.[Transaction] WITH(NOLOCK) 
				WHERE BatchKey=@BatchKey AND (ExistsInDim<>0 OR MappingType=99)) 
		WHERE BatchKey=@BatchKey	

	END
				
	COMMIT TRAN ETLRecursive

END TRY

BEGIN CATCH
	DECLARE @ErrorMsg VARCHAR(4000)
	SET @ErrorMsg=ERROR_MESSAGE()

	ROLLBACK TRAN ETLRecursive

	Declare @EmailSubject varchar(2000)
	Set @EmailSubject = '*** Urgent *** ' + '$(ServerName)' + ' - Recursive ETL Failed,check ETL log/ETLController for error details'

	EXEC msdb.dbo.sp_send_dbmail																							-- Send Error mail to DW Team
	@profile_name	= 'DataWareHouse_Auto_Mails',
	@recipients		= 'bi.alert@WilliamHill.com.au',
	@body			=  @ErrorMsg,
	@subject		=  @EmailSubject;

	UPDATE [Control].[ETLController] SET RecursiveEndDate=GETDATE(), RecursiveStatus=-1, ErrorMessage = @ErrorMsg WHERE BatchKey=@BatchKey;
		
	THROW;
	
END CATCH


﻿Create Procedure [Control].WaitForBatchToComplete

as

Declare @BatchKey int
Set @BatchKey = (Select MAX(BatchKey) From Control.ETLController)

Declare @BatchStatus int
Set @BatchStatus = (Select BatchStatus From Control.ETLController Where BatchKey = @BatchKey)

While @BatchStatus != 1
	Begin
			WAITFOR DELAY '00:15'
			Set @BatchStatus = (Select BatchStatus From Control.ETLController Where BatchKey = @BatchKey)
	End
Print 'Batch ' + Cast (@BatchKey as varchar(10)) + ' is finished'
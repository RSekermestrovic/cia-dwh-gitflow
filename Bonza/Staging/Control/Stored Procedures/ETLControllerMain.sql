CREATE PROCEDURE [Control].[ETLControllerMain]
(
	@FromDate DATETIME2(3)	= NULL,
	@ToDate	  DATETIME2(3)	= NULL,
	@BatchKey BIGINT		= NULL,
	@CopyProd BIT			= 0 -- Copy the batch controller data from prod to keep runs aligned
)

AS 
	
	DECLARE @SafeDate datetime2(3),
			@CorrectedDate datetime2(3)

	DECLARE @SQL nvarchar(MAX)

	DECLARE @Increment int

	SET TRANSACTION ISOLATION LEVEL READ COMMITTED
		
BEGIN TRY

	--Prevent the code creating multiple records in ETLController
	IF (SELECT TOP 1 BatchStatus FROM [Control].[ETLController] ORDER BY BatchKey DESC) <> 1	-- Previous batch not complete
		AND Control.ParameterValue ('ETL','SequentialBatches') <> 'No'							-- Sequential batches not overidden
		RAISERROR ('The previous batch has not completed and sequential batches are being enforced, no action taken',16,1)	

	SET @BatchKey = ISNULL(@BatchKey, (SELECT MAX(BatchKey) + 1 FROM [Control].[ETLController]))

	SELECT	@SafeDate = MIN(ToDate), @CorrectedDate = MIN(CorrectedDate) 
	FROM	(	SELECT ToDate, CorrectedDate FROM [$(Acquisition)].IntraBet.CompleteDate
--					UNION ALL
--					SELECT ToDate, CorrectedDate FROM [$(Acquisition)].PricingData.CompleteDate
			) x
	SET @FromDate = ISNULL(@FromDate, (SELECT MAX(BatchToDate) FROM [Control].[ETLController]))

	IF @CopyProd = 1 -- If instructed to copy batches from prod, get the first ToDate higher than the current from date providing it is less than any nominated ToDate parameter
		SELECT @ToDate = MIN(BatchToDate) FROM [Control].[LiveETLController] WHERE BatchToDate > @FromDate AND BatchToDate <= ISNULL(@ToDate, BatchToDate)
	ELSE -- Without any instruction to copy batches from prod, stick with the nominated To Date  parameter of default to the safe date
			SET @ToDate = ISNULL(@ToDate, @SafeDate)

	IF DATEDIFF(day, @FromDate, @ToDate) > 31	-- If a valid ToDate has been obtained that is more than 31 days after the FromDate, then limit the batch to 31 days
		SET @ToDate = DATEADD(day,31,@FromDate)

	SET @SQL = 'SELECT @Increment = DATEDIFF('+ISNULL(Control.ParameterValue ('ETL','Interval'),'DAY')+', @FromDate, @ToDate)'
	EXECUTE dbo.sp_executesql @SQL, N'@FromDate datetime2(3), @ToDate datetime2(3), @Increment int OUTPUT', @FromDate=@FromDate, @ToDate=@ToDate, @Increment=@Increment OUTPUT

	PRINT '@BatchKey: '  + ISNULL(CAST(@BatchKey as VARCHAR(6)),'NULL')
	PRINT '@FromDate: ' + ISNULL(CONVERT(VARCHAR,@FromDate),'NULL')
	PRINT '@ToDate: ' + ISNULL(CONVERT(varchar,@ToDate),'NULL')
	PRINT '@SafeDate: ' + ISNULL(CONVERT(varchar,@SafeDate),'NULL')
	PRINT '@SafeCorrectedDate: ' + ISNULL(CONVERT(varchar,@CorrectedDate),'NULL')
	PRINT '@CopyProd: ' + ISNULL(CONVERT(varchar,@CopyProd),'NULL')
	PRINT '@Increment: ' + ISNULL(CONVERT(varchar,@Increment),'NULL') + ' ' + ISNULL(Control.ParameterValue ('ETL','Interval'),'DAY')


	IF @Increment < ISNULL(CONVERT(int,Control.ParameterValue ('ETL','Increment')),1) 
		RAISERROR ('Insufficient data for minimum run increment',16,1)	
	IF @ToDate > @SafeDate  
		RAISERROR ('Source data Safe Date is before required To Date',16,1)	
	IF @CopyProd = 1 AND @ToDate IS NULL 
		RAISERROR ('There are no suitable live batches to copy',16,1)	

	EXEC [control].Partitioning_Batch @BatchKey
	EXEC [control].Partitioning_Day @FromDate, @ToDate, @GapFill = 1

	SET IDENTITY_INSERT [Control].[ETLController] ON												-- Switch ON Identity_Insert	

	INSERT INTO [Control].[ETLController] (	BatchKey,BatchStatus,BatchFromDate,BatchToDate,BatchStartDate)
	SELECT @BatchKey,0,@FromDate,@ToDate,GETDATE()
		
	SET IDENTITY_INSERT [Control].[ETLController] OFF												-- Switch OFF Identity_Insert

END TRY

BEGIN CATCH
	THROW;
END CATCH

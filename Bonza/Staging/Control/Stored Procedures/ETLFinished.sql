CREATE PROCEDURE [Control].[ETLFinished]
	(
		@BatchKey		int	= NULL
	)
AS 

BEGIN TRY

	IF ISNUMERIC(@BatchKey) <> 1																				-- Auto Run - Pick next best batch to run
		SELECT	@BatchKey = MIN(BatchKey) 
		FROM	[Control].[ETLController]
		WHERE	ISNULL(BatchStatus,0) = 0

	IF ISNUMERIC(@BatchKey)=1																									-- Only ProcessCube if there is an Cube batch that needs to be processed.
		BEGIN
			Declare @TotalStatus int
			Declare @TotalColumns int  
			DECLARE @sql nvarchar(max)
			DECLARE @Columns nvarchar(max)

			Set @TotalColumns = (SELECT Count(*) as NumberOfColumns
									FROM INFORMATION_SCHEMA.COLUMNS
									WHERE TABLE_NAME = 'ETLController' AND COLUMN_NAME LIKE '%Status'and COLUMN_NAME NOT IN ('BatchStatus') and COLUMN_NAME not like '%Contact%'
									)

			SET @sql = 'SELECT @Total = '
						SELECT
							@Columns = COALESCE(@Columns + '+', N'') + '[' + COLUMN_NAME + ']'
							FROM INFORMATION_SCHEMA.COLUMNS
							WHERE TABLE_NAME = 'ETLController' AND COLUMN_NAME LIKE '%Status'and COLUMN_NAME NOT IN ('BatchStatus') and COLUMN_NAME not like '%Contact%'
			SET @sql = @sql + @Columns + ' '
			SET @sql = @sql + ' FROM control.etlcontroller where BatchKey = ' + cast (@BatchKey as varchar(10))

			Execute sp_executesql @sql,N'@Total int OUTPUT',@Total = @TotalStatus OUTPUT

			If @TotalStatus = @TotalColumns
						Begin
							UPDATE [Control].[ETLController] SET BatchEndDate=GETDATE(), BatchStatus=1 WHERE BatchKey=@BatchKey									-- Set 'Success' RunStatus for Batch
							
							EXEC msdb.dbo.sp_send_dbmail
							@profile_name = 'DataWarehouse_Auto_Mails',
							@recipients = 'BI.Service@williamhill.com.au;edward.yh.chen@gmail.com',
							@body = 'Data Warehouse ETL Completed',
							@subject = 'Data Warehouse ETL Completed'						
						End
					
			If @TotalStatus != @TotalColumns and @TotalStatus is not null 
						Begin
							UPDATE [Control].[ETLController] SET BatchEndDate=GETDATE(), BatchStatus=-1 WHERE BatchKey=@BatchKey								-- Set 'Fail' RunStatus for Batch			
						End

			--if the @TotalStatus is null then the batch hasn't finished and no action is taken
			END
END TRY
		
BEGIN CATCH
	DECLARE @ErrorMsg VARCHAR(4000)
		SET @ErrorMsg=ERROR_MESSAGE()

		Declare @EmailSubject varchar(2000)
		Set @EmailSubject = '*** Urgent *** ' + '$(ServerName)' + ' - Finished ETL Failed,check ETL log/ETLController for error details'

		EXEC msdb.dbo.sp_send_dbmail																							-- Send Error mail to DW Team
		@profile_name	= 'DataWareHouse_Auto_Mails',
		@recipients		= 'bi.alert@WilliamHill.com.au',
		@body			=  @ErrorMsg,
		@subject		=  @EmailSubject;

	UPDATE [Control].[ETLController] SET BatchEndDate=GETDATE(), BatchStatus=-1, ErrorMessage = @ErrorMsg WHERE BatchKey=@BatchKey;

	THROW;

END CATCH

﻿CREATE PROCEDURE [Control].[Sp_Log]
(
	@BatchKey	INT,					-- BatchKey of the ETL
	@Procedure	VARCHAR(128),			-- Name of the SP
	@Step		VARCHAR(128) = NULL,	-- Not Mandatory, can be for a Step or the whole Proc
	@Status		VARCHAR(128),			-- Start,Success,Failed
	@Error		VARCHAR(1000) = NULL,		-- Error Message passed for Failure
	@RowsProcessed	BIGINT = NULL
)
AS 
BEGIN

		DECLARE @LogID		INT;
		DECLARE @PreLogID	INT;
		DECLARE @SQL nvarchar(MAX);

		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

		IF @@TRANCOUNT > 0
			BEGIN -- There is a current active transaction, so recall this procedure via the autonomous recursive link so that the log entry can be committed independent of the current active transaction
				SELECT	@SQL = '['+@@SERVERNAME+'_Autonomous].['+db_name()+'].['+OBJECT_SCHEMA_NAME(@@PROCID)+'].['+OBJECT_NAME(@@PROCID)+'] @BatchKey, @Procedure, @Step, @Status, @Error, @RowsProcessed';

				--SELECT @SQL;
				EXEC sp_executesql @SQL, N'@BatchKey INT, @Procedure VARCHAR(128), @Step VARCHAR(128), @Status VARCHAR(128), @Error VARCHAR(1000), @RowsProcessed BIGINT', 
								   @BatchKey = @BatchKey, @Procedure = @Procedure, @Step = @Step, @Status = @Status, @Error = @Error, @RowsProcessed = @RowsProcessed;
			END
		ELSE
		BEGIN

			IF @Status='Start'
			BEGIN
				INSERT INTO [Control].[Log] (BatchKey,[Procedure],Step,[Status],StartDateTime,EndDateTime,ErrorMessage)	
				SELECT @BatchKey, @Procedure, @Step, 'Running', GETDATE(), NULL, @Error -- RH Amended to include any value in @Error provided as additional information during initial call 
				SET @LogID = @@IDENTITY;

				SELECT @PreLogID = MAX(LogID) FROM [Control].[Log] WHERE BatchKey = @BatchKey AND [Procedure] = @Procedure AND LogID < @LogID;

				-- This automatically updates the previous entry as success.. is this correct?
				UPDATE [Control].[Log]
				SET Status		=	'Success',
					EndDateTime	=	GETDATE(),
					RowsProcessed	=	@RowsProcessed
				WHERE LogID = @PreLogID
					AND Status = 'Running';
			END

			IF @Status='Success'
			BEGIN
				SELECT @LogID = MAX(LogID) FROM [Control].[Log] WHERE BatchKey = @BatchKey AND [Procedure] = @Procedure;

				UPDATE [Control].[Log]
				SET Status			=	'Success',
					EndDateTime		=	GETDATE(),
					RowsProcessed	=	@RowsProcessed
				WHERE LogID = @LogID;
			END

			IF @Status='Failed'
			BEGIN
				SELECT @LogID = MAX(LogID) FROM [Control].[Log] WHERE BatchKey = @BatchKey AND [Procedure] = @Procedure;

				UPDATE [Control].[Log]
				SET Status			=	'Failed',
					EndDateTime		=	GETDATE(),
					ErrorMessage	=	@Error,
					RowsProcessed	=	@RowsProcessed
				WHERE LogID=@LogID
			END

		END
END
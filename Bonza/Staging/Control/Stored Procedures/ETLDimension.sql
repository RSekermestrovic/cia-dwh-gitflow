﻿CREATE PROCEDURE [Control].[ETLDimension]
(
	@BatchKey int	= NULL
)
AS 


	SET TRANSACTION ISOLATION LEVEL READ COMMITTED
    BEGIN TRAN ETLDimension 

BEGIN TRY

	IF ISNUMERIC(@BatchKey) <> 1																				-- Auto Run - Pick next best batch to run
		SELECT	@BatchKey = MIN(BatchKey) 
		FROM	[Control].[ETLController] c
		WHERE	ISNULL(DimensionStatus,0)=0
				AND EXISTS (SELECT 1 FROM [Control].[ETLController] c1 WHERE c1.BatchKey = c.BatchKey AND ISNULL(c1.StagingStatus,0) = 1) -- Staging completed successfully in current batch
				AND EXISTS (SELECT 1 FROM [Control].[ETLController] c1 WHERE c1.BatchKey = c.BatchKey AND ISNULL(c1.DimensionRecursiveStatus,0) = 1) -- Dimension Recursive completed successfully in current batch
				AND NOT EXISTS (SELECT 1 FROM [Control].[ETLController] c1 WHERE c1.BatchKey < c.BatchKey AND ISNULL(c1.DimensionStatus,0) <> 1) -- No failed dimension steps in earlier batches
				AND NOT EXISTS (SELECT 1 FROM [Control].[ETLController] c1 WHERE c1.BatchKey < c.BatchKey AND ISNULL(c1.BatchStatus,0) <> 1 AND Control.ParameterValue ('ETL','SequentialBatches') <> 'No') -- Previous batch complete or Sequential batches overidden

	IF ISNUMERIC(@BatchKey) = 1	-- Only Run if a suitable batch has been nominated or identified above
	BEGIN

		UPDATE [Control].[ETLController] SET DimensionStartDate=GETDATE() WHERE BatchKey=@BatchKey													-- Set StartTime for Dimension

		COMMIT TRAN ETLDimension -- Commit the ETL Controller initiation and switch to READ UNCOMMITTED for rest of processing with no contention in Staging/Dimensional
		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
		BEGIN TRAN ETLDimension 

		Exec [Dimension].Sp_DimDay					@BatchKey
		Exec [Dimension].Sp_DimDayZone				@BatchKey

		EXEC [Dimension].[Sp_DimPromotion]			@BatchKey
		EXEC [Dimension].[Sp_DimBetType]			@BatchKey
		EXEC [Dimension].[Sp_DimContract]			@BatchKey
		EXEC [Dimension].[Sp_DimAccount]			@BatchKey
		EXEC [Dimension].[Sp_DimAccountStatus]		@BatchKey

		EXEC [Dimension].[Sp_DimEvent]				@BatchKey
		EXEC [Dimension].[Sp_DimMarket]				@BatchKey
		EXEC [Dimension].[Sp_DimInstrument]			@BatchKey 

		EXEC [Dimension].[Sp_DimNote]				@BatchKey
		EXEC [Dimension].[Sp_DimUser]				@BatchKey
		EXEC [Dimension].[Sp_DimClass]				@BatchKey
		EXEC [Dimension].[Sp_DimChannel]			@BatchKey

		EXEC [Dimension].[Sp_DimStructure]			@BatchKey

		Exec [Cleanse].[Sp_Cleanse_ALL]				@BatchKey 

		UPDATE [Control].[ETLController] SET DimensionEndDate = GETDATE(), DimensionStatus = 1 WHERE BatchKey = @BatchKey																-- Set 'Success' RunStatus for Dimension
			
		COMMIT TRAN ETLDimension -- Commit the main body of work and switch to seperate transaction to update stats and rows processed, which are optional
		BEGIN TRAN ETLDimension 

		EXEC [$(Dimensional)].Control.SP_DWUpdateStats 'Dimension'																								-- Update Stats on All Dimensions in 'Dimension' Schema
			
		UPDATE [Control].[ETLController] 
		SET DimensionRowsProcessed=	(SELECT SUM(CNT)
									FROM 
									(	SELECT COUNT(*) AS CNT FROM [$(Dimensional)].Dimension.Account WHERE ModifiedBatchKey=@BatchKey UNION ALL
										SELECT COUNT(*) FROM [$(Dimensional)].Dimension.BetType WHERE ModifiedBatchKey=@BatchKey UNION ALL
										SELECT COUNT(*) FROM [$(Dimensional)].Dimension.[Contract] WHERE ModifiedBatchKey=@BatchKey UNION ALL
										SELECT COUNT(*) FROM [$(Dimensional)].Dimension.Promotion WHERE ModifiedBatchKey=@BatchKey
									) X)
		WHERE BatchKey=@BatchKey	

	END

	COMMIT TRAN ETLDimension
			
END TRY

BEGIN CATCH
	DECLARE @ErrorMsg VARCHAR(4000)
	SET @ErrorMsg=ERROR_MESSAGE()

	ROLLBACK TRAN ETLDimension

	Declare @EmailSubject varchar(2000)
	Set @EmailSubject = '*** Urgent *** ' + '$(ServerName)' + ' - Dimension ETL Failed,check ETL log/ETLController for error details'

	EXEC msdb.dbo.sp_send_dbmail																							-- Send Error mail to DW Team
	@profile_name	= 'DataWareHouse_Auto_Mails',
	@recipients		= 'bi.alert@WilliamHill.com.au',
	@body			=  @ErrorMsg,
	@subject		=  @EmailSubject;

	UPDATE [Control].[ETLController] SET DimensionEndDate=GETDATE(), DimensionStatus=-1, ErrorMessage = @ErrorMsg WHERE BatchKey=@BatchKey;
	THROW;

END CATCH

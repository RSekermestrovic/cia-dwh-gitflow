﻿CREATE PROCEDURE [Control].[ETLContactDimensionRecursive]
(
	@BatchKey INT	= NULL
)
AS 

	DECLARE	@RowsProcessed				INT = 0;

	SET TRANSACTION ISOLATION LEVEL READ COMMITTED
    BEGIN TRAN ETLContactDimensionRecursive 

BEGIN TRY

	IF ISNUMERIC(@BatchKey) <> 1																				-- Auto Run - Pick next best batch to run
		SELECT	@BatchKey = MIN(BatchKey) 
		FROM	[Control].[ETLController] c
		WHERE	ISNULL(ContactDimensionRecursiveStatus,0)=0
				AND EXISTS (SELECT 1 FROM [Control].[ETLController] c1 WHERE c1.BatchKey = c.BatchKey AND ISNULL(c1.ContactEmailStagingStatus,0) = 1) -- Staging completed successfully in current batch
				--AND EXISTS (SELECT 1 FROM [Control].[ETLController] c1 WHERE c1.BatchKey = c.BatchKey AND ISNULL(c1.RecursiveStatus,0) = 1) -- Recursive completed successfully in current batch/ not needed for Contact star currently
				AND NOT EXISTS (SELECT 1 FROM [Control].[ETLController] c1 WHERE c1.BatchKey < c.BatchKey AND ISNULL(c1.ContactDimensionRecursiveStatus,0) <> 1) -- No failed recursive steps in earlier batches
				AND NOT EXISTS (SELECT 1 FROM [Control].[ETLController] c1 WHERE c1.BatchKey < c.BatchKey AND ISNULL(c1.ContactEmailStagingStatus,0) <> 1) -- No failed Dimension steps in earlier batches
				AND NOT EXISTS (SELECT 1 FROM [Control].[ETLController] c1 WHERE c1.BatchKey < c.BatchKey AND ISNULL(c1.BatchStatus,0) <> 1 AND Control.ParameterValue ('ETL','SequentialBatches') <> 'No') -- Previous batch complete or Sequential batches overidden


	IF ISNUMERIC(@BatchKey) = 1	-- Only Run if a suitable batch has been nominated or identified above
	BEGIN

		UPDATE [Control].[ETLController] SET ContactDimensionRecursiveStartDate=GETDATE() WHERE BatchKey=@BatchKey													-- Set StartTime for Recursive

		COMMIT TRAN ETLContactDimensionRecursive -- Commit the ETL Controller initiation and switch to READ UNCOMMITTED for rest of processing with no contention in Staging/Dimensional
		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
		BEGIN TRAN ETLContactDimensionRecursive 

		EXEC [Recursive].[Sp_Dimension_Communication]	   @BatchKey, @RowsProcessed OUTPUT
			
		UPDATE [Control].[ETLController] SET ContactDimensionRecursiveEndDate=GETDATE(), ContactDimensionRecursiveStatus=1, ContactDimensionRecursiveRowsProcessed=@RowsProcessed WHERE BatchKey=@BatchKey

	END

	COMMIT TRAN ETLContactDimensionRecursive
				
END TRY

BEGIN CATCH
	DECLARE @ErrorMsg VARCHAR(4000)
	SET @ErrorMsg=ERROR_MESSAGE()

	ROLLBACK TRAN ETLContactDimensionRecursive

	Declare @EmailSubject varchar(2000)
	Set @EmailSubject = '*** Urgent *** ' + '$(ServerName)' + ' - ContactDimensionRecursive ETL Failed,check ETL log/ETLController for error details'

	EXEC msdb.dbo.sp_send_dbmail																							-- Send Error mail to DW Team
	@profile_name	= 'DataWareHouse_Auto_Mails',
	@recipients		= 'bi.alert@WilliamHill.com.au',
	@body			=  @ErrorMsg,
	@subject		=  @EmailSubject;
		
	UPDATE [Control].[ETLController] SET ContactDimensionRecursiveEndDate=GETDATE(), ContactDimensionRecursiveStatus=-1, ErrorMessage = @ErrorMsg WHERE BatchKey=@BatchKey;
	THROW;
	
END CATCH


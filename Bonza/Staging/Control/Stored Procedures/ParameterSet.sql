CREATE PROCEDURE [Control].[ParameterSet]
(	@Class varchar(30),
	@Name varchar(30),
	@Value varchar (100)
) 
AS
	UPDATE Control.Parameter SET Value = @Value WHERE Class = @Class AND Name = @Name

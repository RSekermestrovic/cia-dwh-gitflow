﻿CREATE PROCEDURE [Control].[ETLContactFactPush]
	(
		@BatchKey	int	= NULL
	)
AS 

BEGIN
	
		BEGIN TRY

			IF ISNUMERIC(@BatchKey)<>1																								
				SELECT @BatchKey=MIN(BatchKey) FROM [Control].[ETLController] c WHERE ContactPushStagingStatus=1 AND ISNULL(ContactPushFactStatus,0)=0
				AND NOT EXISTS (SELECT 1 FROM [Control].[ETLController] c1 WHERE c1.BatchKey < c.BatchKey AND ISNULL(c1.ContactPushFactStatus,0) <> 1)
			
			IF ISNUMERIC(@BatchKey)=1																								
			BEGIN
	
				UPDATE [Control].[ETLController] SET ContactPushFactStartDate=GETDATE() WHERE BatchKey=@BatchKey	

				EXEC [Dimension].[Sp_DimCommunication_Push] @BatchKey
				EXEC [Dimension].[Sp_ContactFactLoad_Push] @BatchKey
							
				UPDATE [Control].[ETLController] SET ContactPushFactEndDate=GETDATE() WHERE BatchKey=@BatchKey
				UPDATE [Control].[ETLController] SET ContactPushFactStatus=1			 WHERE BatchKey=@BatchKey
				
				UPDATE [Control].[ETLController]
				SET ContactPushFactRowsProcessed=(SELECT COUNT(*) FROM [$(Dimensional)].FACT.Contact c WITH(NOLOCK) 
																	inner join [$(Dimensional)].Dimension.InteractionType I on I.InteractiontypeKey=c.InteractionTypeKey and I.InteractionTypeSource='ETP'
																	 WHERE C.ModifiedBatchKey=@BatchKey) 
				WHERE BatchKey=@BatchKey
				
				UPDATE STATISTICS [$(Dimensional)].FACT.Contact																	

				UPDATE [$(Dimensional)].Control.CompleteDate SET ExactTargetPushCompleteDate = ISNULL((SELECT BatchToDate FROM [Control].[ETLController] WHERE BatchKey = @BatchKey),ExactTargetPushCompleteDate)
				
			END

		END TRY
		
		BEGIN CATCH
			UPDATE [Control].[ETLController] SET ContactPushFactEndDate=GETDATE()	WHERE BatchKey=@BatchKey	
			UPDATE [Control].[ETLController] SET ContactPushFactStatus=-1			WHERE BatchKey=@BatchKey;
			UPDATE [Control].[ETLController] SET ErrorMessage = ERROR_MESSAGE() WHERE BatchKey=@BatchKey;
			THROW;
		END CATCH

		
END

GO



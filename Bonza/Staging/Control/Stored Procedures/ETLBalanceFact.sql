CREATE PROCEDURE [Control].[ETLBalanceFact]
	(
		@BatchKey	int	= NULL
	)
AS 

	DECLARE @RowsProcessed int = 0 

	SET TRANSACTION ISOLATION LEVEL READ COMMITTED
    BEGIN TRAN ETLBalanceFact
	
BEGIN TRY

	IF ISNUMERIC(@BatchKey)<>1																				-- Auto Run - Pick next best batch to run
		SELECT	@BatchKey = MIN(BatchKey) 
		FROM	[Control].[ETLController] c
		WHERE	ISNULL(BalanceFactStatus,0)=0
				AND EXISTS (SELECT 1 FROM [Control].[ETLController] c1 WHERE c1.BatchKey = c.BatchKey AND ISNULL(c1.RecursiveStatus,0) = 1) -- Recursive completed successfully in current batch
				AND EXISTS (SELECT 1 FROM [Control].[ETLController] c1 WHERE c1.BatchKey = c.BatchKey AND ISNULL(c1.DimensionStatus,0) = 1) -- Dimension completed successfully in current batch
				AND NOT EXISTS (SELECT 1 FROM [Control].[ETLController] c1 WHERE c1.BatchKey < c.BatchKey AND ISNULL(c1.BalanceFactStatus,0) <> 1) -- No failed Balance Fact steps in earlier batches
				AND NOT EXISTS (SELECT 1 FROM [Control].[ETLController] c2 WHERE c2.BatchKey < c.BatchKey AND ISNULL(c2.BatchStatus,0) <> 1 AND Control.ParameterValue ('ETL','SequentialBatches') <> 'No') -- Previous batch complete or Sequential batches overidden

	IF ISNUMERIC(@BatchKey)=1	-- Only Run if a suitable batch has been nominated or identified above
	BEGIN
	
		UPDATE [Control].[ETLController] SET BalanceFactStartDate=GETDATE() WHERE BatchKey=@BatchKey				-- Set StartTime for Position FACT Load

		COMMIT TRAN ETLBalanceFact -- Commit the ETL Controller initiation and switch to READ UNCOMMITTED for rest of processing due to contention issues and phantom updates from constantly updated Acquisition
		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
		BEGIN TRAN ETLBalanceFact

		EXEC [Dimension].[Sp_BalanceFactLoad] @BatchKey, @RowsProcessed	OUTPUT											-- RunPosition Load
							
		UPDATE [Control].[ETLController] SET BalanceFactEndDate=GETDATE(), BalanceFactStatus=1, BalanceFactRowsProcessed = @RowsProcessed WHERE BatchKey=@BatchKey

		COMMIT TRAN ETLBalanceFact -- Commit main body of updates to progreess non-esential subsequent updates in their own transaction
		BEGIN TRAN ETLBalanceFact
				
		UPDATE STATISTICS [$(Dimensional)].FACT.Balance																			-- Update STATS on Fact Table

		UPDATE [$(Dimensional)].Control.CompleteDate SET BalanceCompleteDate = ISNULL((SELECT BatchToDate FROM [Control].[ETLController] WHERE BatchKey = @BatchKey),BalanceCompleteDate)
				
	END

	COMMIT TRAN ETLBalanceFact

END TRY
BEGIN CATCH
	DECLARE @ErrorMsg VARCHAR(4000)
	SET @ErrorMsg=ERROR_MESSAGE()

	ROLLBACK TRAN ETLBalanceFact

	Declare @EmailSubject varchar(2000)
	Set @EmailSubject = '*** Urgent *** ' + '$(ServerName)' + ' - BalanceFact ETL Failed,check ETL log/ETLController for error details'

	EXEC msdb.dbo.sp_send_dbmail																							-- Send Error mail to DW Team
	@profile_name	= 'DataWareHouse_Auto_Mails',
	@recipients		= 'bi.alert@WilliamHill.com.au',
	@body			=  @ErrorMsg,
	@subject		=  @EmailSubject;

	UPDATE [Control].[ETLController] SET BalanceFactEndDate=GETDATE(), BalanceFactStatus=-1, ErrorMessage = @ErrorMsg WHERE BatchKey=@BatchKey;
	THROW;

END CATCH	

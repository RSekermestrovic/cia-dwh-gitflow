﻿CREATE PROCEDURE [Control].[ETLContactSmsStaging]
	(
		@BatchKey INT = NULL
	)

AS 

BEGIN

Declare @FromDate dateTime2(3)
Declare @ToDate DateTime2(3)
Declare @ETSmsLatestTodate DateTime2(3)
Declare @ETLoadedDate Datetime2(3)
Declare @PreviousBatch	INT
Declare @PreviousBatchComplete	INT

Select	@ETSmsLatestTodate = IsNull(MAX(ToDate),CONVERT(DateTime2(3),'1900-01-01 00:00:00.000'))
From [$(Acquisition)].[ExactTargetSms].[CompleteDate]
	
		BEGIN TRY
			IF ISNUMERIC(@BatchKey)<>1																									
				SELECT @BatchKey=MIN(BatchKey) FROM [Control].[ETLController] WHERE DimensionStatus =1 AND ISNULL(ContactSmsStagingStatus,0)=0

				SELECT @PreviousBatch = (SELECT MAX(BatchKey) FROM [Control].[ETLController] WHERE BatchKey < @BatchKey);											

				SELECT @PreviousBatchComplete = (SELECT BatchKey FROM [Control].[ETLController] WHERE BatchKey = @PreviousBatch AND ContactSmsFactStatus = 1);
			
			IF EXISTS ( SELECT 1 FROM [Control].[ETLController] WHERE DimensionStatus = 1 AND ContactSmsStagingStatus IN (-1,-2,-3) 
					AND ErrorMessage LIKE '%Deadlock%')																					
			BEGIN
				SELECT 	@BatchKey=MIN(BatchKey) 
				FROM [Control].[ETLController] 
				WHERE DimensionStatus=1 AND ContactSmsStagingStatus IN(-1,-2,-3) AND ErrorMessage LIKE '%Deadlock%'				
				

				DELETE FROM Stage.Contact WHERE BatchKey=@BatchKey and JobIDSource='ETS'
				DELETE FROM Stage.Communication WHERE BatchKey=@BatchKey and CommunicationSource='ETS'

				UPDATE [Control].[ETLController]																			
					SET ContactSmsStagingStatus		=NULL,
						ContactSmsStagingStartDate	=NULL,
						ContactSmsStagingEndDate	=NULL,
						ErrorMessage		=NULL 
					WHERE BatchKey=	@BatchKey
									
				EXEC msdb.dbo.sp_send_dbmail																						
					@profile_name	= 'DataWareHouse_Auto_Mails',
					@recipients		= 'bi.alert@WilliamHill.com.au',
					@body			= 'Contact Sms Staging ETL Deadlocked, process has been restarted, check ETL log/ETLController for details',
					@subject		= 'Contact Sms Staging ETL Deadlocked, process has been restarted, check ETL log/ETLController for details'
			END


			IF (ISNUMERIC(@BatchKey)=1 AND  (ISNUMERIC(@PreviousBatchComplete)=1 OR @PreviousBatch IS NULL))
			BEGIN 

			Select @ETLoadedDate = CONVERT(DateTime,CONVERT(date,IsNull(BatchFromDate,'1900-01-01 00:00:00.000')))
			FROM [Control].[ETLController]
			Where BatchKey = @BatchKey

			IF  @ETSmsLatestTodate >= @ETLoadedDate

				BEGIN

					SELECT @FromDate= Convert(date,BatchFromDate) FROM [Control].[ETLController] WHERE BatchKey=@BatchKey
					SELECT @ToDate= Convert(date,BatchToDate) FROM [Control].[ETLController] WHERE BatchKey=@BatchKey
	
					UPDATE [Control].[ETLController] SET ContactSmsStagingStartDate=GETDATE() WHERE BatchKey=@BatchKey				

					EXEC [ExactTarget].[Sp_Communication_Sms] @FromDate, @ToDate, @BatchKey
					EXEC [ExactTarget].[Sp_Contact_Sms] @FromDate, @ToDate, @BatchKey
				
					--INSERT INTO [$(Acquisition)].ExactTarget.Latency
					--Select MAX(FromDate), MAX(FromDate) From Stage.Contact Where BatchKey = @BatchKey											

					UPDATE [Control].[ETLController] 
					SET ContactSmsStagingEndDate=GETDATE(),
						ContactSmsStagingStatus=1
						 WHERE BatchKey=@BatchKey	
						 				
					--UPDATE [Control].[ETLController] SET ContactSmsStagingStatus=1 WHERE BatchKey=@BatchKey							

					UPDATE [Control].[ETLController]																				
					SET ContactSmsStagingRowsProcessed=(SELECT COUNT(*) FROM STAGE.Contact WITH(NOLOCK) WHERE BatchKey=@BatchKey and JobIDSource='ETS') 
					WHERE BatchKey=@BatchKey
				
				END
			
			END 	


		END TRY
		
		BEGIN CATCH
			UPDATE [Control].[ETLController] 
			SET ContactSmsStagingEndDate=GETDATE(),
				ContactSmsStagingStatus=ISNULL(ContactSmsStagingStatus,0)-1 ,
				ErrorMessage = ERROR_MESSAGE()
			 WHERE BatchKey=@BatchKey	;					
			--UPDATE [Control].[ETLController] SET ContactSmsStagingStatus=ISNULL(ContactSmsStagingStatus,0)-1 WHERE BatchKey=@BatchKey;	
																																		
			--UPDATE [Control].[ETLController] SET ErrorMessage = ERROR_MESSAGE() WHERE BatchKey=@BatchKey;
			THROW;
		END CATCH

END

GO



﻿CREATE PROCEDURE [Control].[Truncate_Batch]
(	
	@BatchKey			int,					-- Mandatory - Specifies the Batch to be truncated
	@Schema				varchar(128) = NULL,	-- Optional - Nominates a schema within which all tables partitioned by BatchKey will have the specified batch truncated
	@Table				varchar(128) = NULL,	-- Optional - Nominates a single table which will have the specified batch truncated
	@ManagePartition	bit = 0,				-- Optional - (0=FALSE/1=TRUE) - Indicates whether the neceesary partitions to only truncate the specified batch will be created if they do not already exist
	@DryRun				bit = 0					-- Optional - (0=FALSE/1=TRUE) - Allows the procedure to be dry run whereby it will report all of its intended actions but not actually execute them
)
AS

	SET NOCOUNT ON 

	DECLARE	@BatchKeyColumn varchar(128) = 'BatchKey'

	DECLARE @PreviousBatchKey int = @BatchKey - 1

	DECLARE	@i int,
			@iSchemaName sysname,
			@iTableName sysname,
			@iPartitionNumber varchar(10),
			@iCompress varchar(10)

BEGIN TRY

	-- MANAGE PARTITIONS TO ALLOW TRUNCATION

	IF @ManagePartition = 1
		BEGIN
			EXEC [Control].Partitioning_Batch @BatchKey = @BatchKey, @Schema = @Schema, @Table = @Table, @GapFill = 0, @Trim = 0, @RowBoundary = NULL, @DryRun = @DryRun
			IF @BatchKey > 0
				EXEC [Control].Partitioning_Batch @BatchKey = @PreviousBatchKey, @Schema = @Schema, @Table = @Table, @GapFill = 0, @Trim = 0, @RowBoundary = NULL, @DryRun = @DryRun
		END

	-- GET PARTITIONS TO BE TRUNCATED BASED ON PARAMETERS PROVIDED

	SELECT	s.name SchemaName, 
			t.name TableName, 
			p.partition_number PartitionNumber, 
			p.data_compression_desc Compress, 
			CASE WHEN pr.Value = 1 THEN 1 WHEN prx.function_id IS NOT NULL THEN 1 ELSE 0 END CanTruncate,
			ROW_NUMBER() OVER (ORDER BY s.name DESC, t.name DESC) i
	INTO	#Partitions
	FROM	sys.columns c 
			INNER JOIN sys.tables t on (t.object_id = c.object_id)
			INNER JOIN sys.schemas s on (s.schema_id = t.schema_id)
			INNER JOIN sys.index_columns ic on (ic.object_id = c.object_id and ic.column_id = c.column_id)
			INNER JOIN sys.indexes i on (i.object_id = ic.object_id and i.index_id = ic.index_id)
			INNER JOIN sys.partition_schemes ps on (ps.data_space_id = i.data_space_id)
			INNER JOIN sys.partition_functions pf on (pf.function_id = ps.function_id)
			LEFT JOIN sys.partition_range_values pr on (pr.function_id = ps.function_id AND pr.Value = @BatchKey + pf.boundary_value_on_right)
			LEFT JOIN sys.partitions p on (p.object_id = c.object_id AND p.partition_number = pr.boundary_id)
			LEFT JOIN sys.partition_range_values prx on (prx.function_id = ps.function_id AND CONVERT(int,pr.Value) - CONVERT(int,prx.Value) = 1)
	WHERE	c.name = @BatchKeyColumn
			AND t.name = ISNULL(@Table,t.name)
			AND s.name = ISNULL(@Schema,s.name)
			AND ic.partition_ordinal <> 0
	SET @i = @@ROWCOUNT

select * from #Partitions

	-- CHECK PARTITIONS ARE SUITABLE FOR TRUNCATION (necessary because manage partition step is optional)

	IF EXISTS(SELECT 1 FROM #Partitions WHERE CanTruncate = 0)
		RAISERROR ('The specified Batch is not implemented as a single partition and cannot therefore be truncated',16,1)	
		
	-- TRUNCATE

	WHILE @i > 0 AND @DryRun = 0
	BEGIN
		SELECT	@iSchemaName = SchemaName, @iTableName = TableName, @iPartitionNumber = CONVERT(varchar(10),PartitionNumber), @iCompress = Compress FROM	#Partitions	WHERE i = @i
		EXEC ('BEGIN TRY DROP TABLE [Buffer].['+@iTableName+'] END TRY BEGIN CATCH END CATCH')
		EXEC ('SELECT * INTO [Buffer].['+@iTableName+'] FROM ['+@iSchemaName+'].['+@iTableName+'] WHERE 1=0')
		EXEC ('ALTER TABLE Buffer.['+@iTableName+'] REBUILD WITH (DATA_COMPRESSION = '+@iCompress+')')
		EXEC ('ALTER TABLE ['+@iSchemaName+'].['+@iTableName+'] SWITCH PARTITION '+@iPartitionNumber+' TO [Buffer].['+@iTableName+']')
		EXEC ('DROP TABLE [Buffer].['+@iTableName+']')
		SET @i = @i - 1	
	END

END TRY

BEGIN CATCH
	THROW;
END CATCH

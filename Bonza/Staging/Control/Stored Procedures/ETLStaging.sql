﻿CREATE PROCEDURE [Control].[ETLStaging]
(
	@BatchKey int	= NULL
)
AS 

	DECLARE	@FromDate					DATETIME,
	 		@ToDate						DATETIME

	SET TRANSACTION ISOLATION LEVEL READ COMMITTED
    BEGIN TRAN ETLStaging 

BEGIN TRY

	IF ISNUMERIC(@BatchKey) <> 1																				-- Auto Run - Pick next best batch to run
		SELECT	@BatchKey = MIN(BatchKey) 
		FROM	[Control].[ETLController] c
		WHERE	ISNULL(StagingStatus,0)=0
				AND EXISTS (SELECT 1 FROM [Control].[ETLController] c1 WHERE c1.BatchKey = c.BatchKey AND ISNULL(c1.AtomicStatus,0) = 1) -- Atomic completed successfully in current batch
				AND NOT EXISTS (SELECT 1 FROM [Control].[ETLController] c1 WHERE c1.BatchKey < c.BatchKey AND ISNULL(c1.StagingStatus,0) <> 1) -- No failed Staging steps in earlier batches
				AND NOT EXISTS (SELECT 1 FROM [Control].[ETLController] c1 WHERE c1.BatchKey < c.BatchKey AND ISNULL(c1.BatchStatus,0) <> 1 AND Control.ParameterValue ('ETL','SequentialBatches') <> 'No') -- Previous batch complete or Sequential batches overidden

	IF ISNUMERIC(@BatchKey)=1	-- Only Run if a suitable batch has been nominated or identified above
	BEGIN
		SELECT @FromDate= BatchFromDate, @ToDate= BatchToDate FROM [Control].[ETLController] WHERE BatchKey=@BatchKey

		UPDATE [Control].[ETLController] SET StagingStartDate=GETDATE() WHERE BatchKey=@BatchKey													-- Set StartTime for Staging
										
		COMMIT TRAN ETLStaging -- Commit the ETL Controller initiation and switch to SNAPSHOT for rest of processing due to contention issues and phantom updates from constantly updated Acquisition
		SET TRANSACTION ISOLATION LEVEL SNAPSHOT
		BEGIN TRAN ETLStaging

		/* These tables populate Stage.Transaction */
		EXEC [Intrabet].[Sp_TransactionBets]		@BatchKey						-- Atomic Bet To Staging
		EXEC [Intrabet].[Sp_FreeBetCredits]			@BatchKey,@FromDate,@ToDate		-- Free Bet Credits
		EXEC [Intrabet].[Sp_FreeBetExpiry]			@BatchKey						-- Free Bet Expiry
		EXEC [Intrabet].[Sp_Deposits]				@FromDate,@ToDate,@BatchKey		-- Deposits
		EXEC [Intrabet].[Sp_Withdrawals]			@FromDate,@ToDate,@BatchKey		-- WithDrawals
		EXEC [Intrabet].[Sp_OtherTrans]				@FromDate,@ToDate,@BatchKey		-- OtherTrans
		EXEC [Intrabet].[Sp_ClientTransfers]		@FromDate,@ToDate,@BatchKey		-- ClientTransfers
		EXEC [Intrabet].[Sp_InterOrgAccTransfer]	@BatchKey,@FromDate,@ToDate		-- Inter Org Account Transfers
			
		/* Stage.Account */
		EXEC [Control].[Invoker]	'Intrabet.Sp_Account', @BatchKey -- Test of new invoker logic
		EXEC [Intrabet].[Sp_AccountStatus]			@BatchKey,@FromDate,@ToDate		-- Dim Account Staging Load									

		EXEC [Control].[Invoker]	'Intrabet.Sp_Event', @BatchKey -- Test of new invoker logic
--		EXEC [Intrabet].[Sp_Event]					@FromDate,@ToDate,@BatchKey
		EXEC [Intrabet].[Sp_Promotion]				@BatchKey,@FromDate,@ToDate

		/* Stage.Instrument */
		EXEC [Control].[Invoker]	'Intrabet.Sp_Instrument_EFT', @BatchKey
		EXEC [Control].[Invoker]	'Intrabet.Sp_Instrument_Paypal', @BatchKey
		EXEC [Control].[Invoker]	'Intrabet.Sp_Instrument_Moneybookers', @BatchKey
		EXEC [Control].[Invoker]	'Intrabet.Sp_Instrument_BPay', @BatchKey

		--EXEC [Intrabet].[Sp_Instrument]				@FromDate,@ToDate,@BatchKey

		EXEC [Intrabet].[Sp_Note]					@FromDate,@ToDate,@BatchKey
		EXEC [Intrabet].[Sp_User]					@FromDate,@ToDate,@BatchKey
		EXEC [Intrabet].[Sp_Class]					@FromDate,@ToDate,@BatchKey
		EXEC [Intrabet].[Sp_Market]					@FromDate,@ToDate,@BatchKey -- Not implemented
		EXEC [Intrabet].[Sp_Channel]				@BatchKey,@FromDate,@ToDate	
		--EXEC [Intrabet].[Sp_PriceType]				@FromDate,@ToDate,@BatchKey -- Currently a fixed dimension. Do not use until this is changed to Type 1.
		EXEC [Intrabet].[Sp_Structure]				@BatchKey,@FromDate,@ToDate	

		EXEC [Intrabet].[Sp_KPI]					@BatchKey,@FromDate,@ToDate		-- Cashout Differentials and AccountsOpened to KPI

		/* Stage Balance */
		EXEC [Intrabet].[Sp_BalanceStaging]			@BatchKey						-- Balance Staging Load
			
		-- Set 'Success' RunStatus for Staging
		UPDATE [Control].[ETLController] SET StagingEndDate=GETDATE(), StagingStatus=1 WHERE BatchKey=@BatchKey;	

		COMMIT TRAN ETLStaging -- Commit the main body of work and switch to seperate transaction to update rows processed, which is informational only
		BEGIN TRAN ETLStaging 

		-- Set RowsProcessed for Staging
		UPDATE [Control].[ETLController] SET StagingRowsProcessed=(SELECT COUNT(*) FROM Stage.[Transaction] WITH(NOLOCK) WHERE BatchKey=@BatchKey) WHERE BatchKey=@BatchKey	
		
	END
			
	COMMIT TRAN ETLStaging

END TRY

BEGIN CATCH
	DECLARE @ErrorMsg VARCHAR(4000)
	SET @ErrorMsg=ERROR_MESSAGE()

	ROLLBACK TRAN ETLStaging

	Declare @EmailSubject varchar(2000)
	Set @EmailSubject = '*** Urgent *** ' + '$(ServerName)' + ' - Staging ETL Failed,check ETL log/ETLController for error details'

	EXEC msdb.dbo.sp_send_dbmail																							-- Send Error mail to DW Team
	@profile_name	= 'DataWareHouse_Auto_Mails',
	@recipients		= 'bi.alert@WilliamHill.com.au',
	@body			=  @ErrorMsg,
	@subject		=  @EmailSubject;

	UPDATE [Control].[ETLController] SET StagingEndDate=GETDATE(), StagingStatus=-1, ErrorMessage = @ErrorMsg WHERE BatchKey=@BatchKey;

	THROW;

END CATCH

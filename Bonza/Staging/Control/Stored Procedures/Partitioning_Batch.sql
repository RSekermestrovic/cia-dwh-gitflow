﻿CREATE PROCEDURE [Control].[Partitioning_Batch]
(	
	@BatchKey		int = NULL,				-- Optional - Specifies a new Batch, which may or may not be specified in the ETL_Controller already, for which an additional partition is to be added
	@Schema			varchar(128) = NULL,	-- Optional - Nominates a schema within which all tables partitioned by BatchKey will have their partitions maintained (note that any changes to the partition functions of these tabes will also impact any other tables using those functions)
	@Table			varchar(128) = NULL,	-- Optional - Nominates a single table which if partitioned by BatchKey will have its partitions maintained (note that any changes to the partition function of this tabes will also impact any other tables using that function)
	@GapFill		bit = 0,				-- Optional - (0=FALSE/1=TRUE) - Indicates whether any gaps (BatchKeys in ETL_Controller for which there aren't currently partitions), in additiona tot he new BatchKey in the parameter above should be created 
	@Trim			bit = 0,				-- Optional - (0=FALSE/1=TRUE) - Indicates whether any exisiting partitons for which there aren't current BatchKeys in ETL_Controller or included the above parameter should be merged out.
	@RowBoundary	int = 0,				-- Optional - Sets the boundary between PAGE and ROW compression in the partitions, i.e. 0 = PAGE compression for all partitions, 2= ROW compression for last 2 partitions and PAGE for all others, etc. NULL = do not perform any compression changes
	@DryRun			bit = 0					-- Optional - (0=FALSE/1=TRUE) - Allows the procedure to be dry run whereby it will report all of its intended actions but not actually execute them
											-- NOTE, this function will always maintain one empty highest partition to speed the processes of adding new highest partitons without having to move data
)
AS

	SET NOCOUNT ON 

	DECLARE	@BatchKeyColumn varchar(128) = 'BatchKey'

	DECLARE @ProcessedToBatchKey int

	DECLARE	@i int,
			@SQL varchar(MAX)

BEGIN

	SELECT	@ProcessedToBatchKey = MAX(CASE WHEN BatchStatus = 1 THEN BatchKey ELSE NULL END) 
	FROM	Control.ETLController WITH (NOLOCK) WHERE BatchKey > 0

	-- MANAGE PARTITIONS

	SELECT	s.name SchemaName, ic.object_id TableId, t.name TableName, ic.index_id IndexId, i.name IndexName, i.type_desc IndexType, ps.name PartitionScheme, pf.name PartitionFunction, pf.function_id PartitionFunctionId, pf.boundary_value_on_right RightBoundary 
	INTO	#Indexes
	FROM	sys.columns c 
			INNER JOIN sys.index_columns ic on (ic.object_id = c.object_id and ic.column_id = c.column_id)
			INNER JOIN sys.indexes i on (i.object_id = ic.object_id and i.index_id = ic.index_id)
			INNER JOIN sys.partition_schemes ps on (ps.data_space_id = i.data_space_id)
			INNER JOIN sys.partition_functions pf on (pf.function_id = ps.function_id)
			INNER JOIN sys.tables t on (t.object_id = c.object_id)
			INNER JOIN sys.schemas s on (s.schema_id = t.schema_id)
	WHERE	c.name = @BatchKeyColumn
			AND t.name = ISNULL(@Table,t.name)
			AND s.name = ISNULL(@Schema,s.name)
			AND ic.partition_ordinal <> 0

	SELECT	PartitionScheme, PartitionFunction, PartitionFunctionId, RightBoundary, CONVERT(int,Value) Value
	INTO	#Values
	FROM	(SELECT DISTINCT PartitionScheme, PartitionFunction, PartitionFunctionId, RightBoundary FROM #Indexes) i
			INNER JOIN sys.partition_range_values pr on (pr.function_id = i.PartitionFunctionId)

	SELECT	ROW_NUMBER() OVER (ORDER BY PartitionFunction DESC, Value ASC) i, sql
	INTO	#Partitions
	FROM	(	SELECT	'ALTER PARTITION SCHEME '+ PartitionScheme +' NEXT USED [PRIMARY] ALTER PARTITION FUNCTION '+PartitionFunction+'() SPLIT RANGE ('+CONVERT(varchar,Value)+')' sql, PartitionFunction, Value
				FROM	(	(	SELECT DISTINCT PartitionScheme, PartitionFunction, @BatchKey + RightBoundary Value FROM #Indexes
								UNION
								SELECT i.PartitionScheme, i.PartitionFunction, e.BatchKey + RightBoundary Value FROM Control.ETLController e, (SELECT DISTINCT PartitionScheme, PartitionFunction, RightBoundary FROM #Indexes) i WHERE @GapFill = 1
							)
							EXCEPT
							SELECT PartitionScheme, PartitionFunction, Value from #Values
						) x
				UNION ALL
				SELECT	'ALTER PARTITION FUNCTION '+PartitionFunction+'() MERGE RANGE ('+CONVERT(varchar,Value)+')' sql, PartitionFunction, Value
				FROM	(	SELECT PartitionScheme, PartitionFunction, Value from #Values WHERE @Trim = 1
							EXCEPT
							(	SELECT i.PartitionScheme, i.PartitionFunction, e.BatchKey + RightBoundary Value FROM Control.ETLController e, (SELECT DISTINCT PartitionScheme, PartitionFunction, RightBoundary FROM #Indexes) i
								UNION
								SELECT DISTINCT PartitionScheme, PartitionFunction, @BatchKey + RightBoundary Value FROM #Indexes
							)
						) x
			) y
	WHERE	sql IS NOT NULL
	SET @i = @@ROWCOUNT

	SELECT sql PartitionManagement FROM #Partitions ORDER BY i

	WHILE @i > 0 AND @DryRun = 0
	BEGIN
		SELECT	@SQL = sql FROM	#Partitions	WHERE i = @i
		EXEC(@SQL)
		SET @i = @i - 1	
	END


	-- MANAGE COMPRESSION

	IF @RowBoundary IS NOT NULL
	BEGIN
		WITH PageBoundary AS
			(	SELECT	i.PartitionFunctionId, MAX(pr.boundary_id + i.RightBoundary) - @RowBoundary PageLimit
				FROM	(SELECT DISTINCT PartitionFunctionId, RightBoundary FROM #Indexes) i
						INNER JOIN sys.partition_range_values pr WITH (NOLOCK) on (pr.function_id = i.PartitionFunctionId)
				WHERE	CONVERT(int,Value)<= @ProcessedToBatchKey 
				GROUP BY i.PartitionFunctionId
			)
		SELECT	ROW_NUMBER() OVER (ORDER BY SchemaName DESC, TableName DESC, PartitionNumber DESC, IndexName DESC) i, 
				'ALTER ' + CASE WHEN IndexName IS NULL THEN 'TABLE ' ELSE 'INDEX ['+ IndexName +'] ON ' END + 
							'[' + SchemaName + '].[' + TableName + ']' + 
							' REBUILD PARTITION = ' + CONVERT(VARCHAR(20),PartitionNumber) + 
							' WITH (/*SORT_IN_TEMPDB = ON, ONLINE = ON,*/ DATA_COMPRESSION = ' + TargetCompression + ') ' sql
		INTO	#Compression
		FROM	(	SELECT	i.SchemaName,
							i.TableName, 
							i.IndexName, 
							p.partition_number PartitionNumber, 
							p.data_compression_desc CurrentCompression, 
							CASE WHEN PageLimit >= p.partition_number THEN 'PAGE' ELSE 'ROW' END TargetCompression
					FROM	#Indexes i WITH (NOLOCK) 
							INNER JOIN PageBoundary b ON (b.PartitionFunctionId = i.PartitionFunctionId)
							INNER JOIN sys.partitions p WITH (NOLOCK) ON (p.object_id = i.TableId AND p.index_id = i.IndexId)
				) x
		WHERE	TargetCompression <> CurrentCompression
		SET @i = @@ROWCOUNT

		SELECT sql CompressionManagement from #Compression

		WHILE @i > 0 AND @DryRun = 0
		BEGIN
			SELECT	@SQL = sql FROM	#Compression WHERE i = @i
			EXEC(@SQL)
			SET @i = @i - 1	
		END
	END

END

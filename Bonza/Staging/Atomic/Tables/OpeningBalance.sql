﻿CREATE TABLE [Atomic].[OpeningBalance] (
    [BatchKey]            INT           NOT NULL,
    [DayDate]             DATE          NOT NULL,
    [LedgerId]            INT           NOT NULL,
    [FreeBet]             BIT           NOT NULL,
    [FromDate]            DATETIME2 (3) NOT NULL,
    [AccountEnabled]      BIT           NOT NULL,
    [Status]              CHAR (1)      NOT NULL,
    [ClientBalance]       MONEY         NOT NULL,
    [UnsettledBalance]    MONEY         NOT NULL,
    [PendingDebitBalance] MONEY         NOT NULL,
    [LifetimebetsPlaced]  INT           NULL,
    [LifetimeStake]       MONEY         NOT NULL,
    [LifetimePayout]      MONEY         NOT NULL,
    [FirstCreditDate]     DATETIME2 (3) NULL,
    [LastCreditDate]      DATETIME2 (3) NULL,
    [LifetimeCredit]      MONEY         NOT NULL,
    [FirstDebitDate]      DATETIME2 (3) NULL,
    [LastDebitDate]       DATETIME2 (3) NULL,
    [LifetimeDebit]       MONEY         NOT NULL,
    [FirstBetId]          BIGINT        NULL,
    [FirstBetDate]        DATETIME2 (3) NULL,
    [FirstBetType]        INT           NULL,
    [FirstWinPlace]       TINYINT       NULL,
    [FirstBetGroupType]   VARCHAR (20)  NULL,
    [FirstChannel]        INT           NULL,
    [FirstEventId]        INT           NULL,
    [FirstClassId]        INT           NULL,
    [LastBetId]           BIGINT        NULL,
    [LastBetDate]         DATETIME2 (3) NULL,
    [LastBetType]         INT           NULL,
    [LastWinPlace]        TINYINT       NULL,
    [LastBetGroupType]    VARCHAR (20)  NULL,
    [LastChannel]         INT           NULL,
    [LastEventId]         INT           NULL,
    [LastClassId]         INT           NULL,
    [FirstCreditId]       BIGINT        NULL,
    [LastCreditId]        BIGINT        NULL
) ON [par_sch_int_Stage] ([BatchKey])
WITH (DATA_COMPRESSION = ROW );
GO

ALTER TABLE [Atomic].[OpeningBalance] SET (LOCK_ESCALATION = AUTO)
GO


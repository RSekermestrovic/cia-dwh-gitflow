﻿CREATE TABLE [Atomic].[Balance] (
    [BatchKey]        INT          NOT NULL,
    [DayDate]         DATE         NOT NULL,
    [LedgerID]        INT          NOT NULL,
    [OrgID]           INT          NOT NULL,
    [OrgName]         VARCHAR (4)  NOT NULL,
    [AccountTypeName] VARCHAR (7)  NOT NULL,
    [LedgerTypeId]    INT          NOT NULL,
    [LedgerTypeName]  VARCHAR (50) NOT NULL,
    [Enabled]         BIT          NOT NULL,
    [Status]          CHAR (1)     NOT NULL,
    [StatusName]      VARCHAR (7)  NOT NULL,
    [Balance]         MONEY        NOT NULL
) ON [par_sch_int_Stage] ([BatchKey])
WITH (DATA_COMPRESSION = ROW);













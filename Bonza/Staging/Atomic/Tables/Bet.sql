﻿CREATE TABLE [Atomic].[Bet] (
    [BatchKey]                   INT             NOT NULL,
    [Action]                     VARCHAR (10)    NOT NULL,
    [TransactionId]              BIGINT          NOT NULL,
    [TransactionIdSource]        CHAR (3)        NOT NULL,
    [UserID]                     VARCHAR (200)   NULL,
    [UserIdSource]               CHAR (3)        NOT NULL,
    [MasterTransactionTimestamp] DATETIME2 (3)   NOT NULL,
    [FromDate]                   DATETIME2 (3)   NOT NULL,
    [BetGroupID]                 BIGINT          NOT NULL,
    [BetGroupIDSource]           CHAR (3)        NOT NULL,
    [BetGroupType]               VARCHAR (20)    NOT NULL,
    [LedgerId]                   BIGINT          NOT NULL,
    [LedgerSource]               CHAR (3)        NOT NULL,
    [Channel]					 INT		     NOT NULL,
    [ActionChannel]				 INT			 NOT NULL,
    [CampaignId]                 INT             NOT NULL,
    [CampaignIdSource]           CHAR (3)        NOT NULL,
    [FreeBetId]                  INT             NOT NULL,
    [FreeBetIdSource]            CHAR (3)        NOT NULL,
    [WalletId]                   CHAR (1)        NOT NULL,
    [PromotionId]                INT             NOT NULL,
    [PromotionIdSource]          CHAR (3)        NOT NULL,
    [BetId]                      BIGINT          NOT NULL,
    [BetIdSource]                CHAR (3)        NOT NULL,
    [BetStake]                   MONEY           NOT NULL,
    [BetPayout]                  MONEY           NOT NULL,
	[BetPrice]                   MONEY           NOT NULL,
	[BetStakeDelta]			     MONEY           NOT NULL,
    [BetPayoutDelta]             MONEY           NOT NULL,
    [BetType]                    VARCHAR (10)    NOT NULL,
    [BetSubType]                 VARCHAR (20)    NOT NULL,
    [PriceTypeId]				 INT			 NOT NULL,  --NEW
	[PriceTypeSource]			 CHAR (3)        NOT NULL,	--NEW
	[LegacyBetId]                BIGINT          NULL,
    [NumberOfPermutations]       INT             NOT NULL,
    [PermutationStake]           MONEY           NOT NULL,
    [NumberOfLegs]               TINYINT         NOT NULL,
    [LegId]                      BIGINT          NOT NULL,
    [LegIdSource]                CHAR (3)        NOT NULL,
    [LegStake]                   MONEY           NOT NULL,
    [LegStakeTrued]              MONEY           NOT NULL,
    [LegPayout]                  MONEY           NOT NULL,
    [LegPayoutTrued]             MONEY           NOT NULL,
    [LegNumber]                  TINYINT         NOT NULL,
    [LegType]                    VARCHAR (32)    NOT NULL,
    [ClassId]                    INT             NOT NULL,
    [ClassIdSource]              CHAR (3)        NOT NULL,
    [EventID]                    INT             NOT NULL,
    [EventIdSource]              CHAR (3)        NOT NULL,
	[MarketID]                   INT             NOT NULL, --NEW
    [MarketIdSource]             CHAR (3)        NOT NULL, --NEW
    [LegCompetitorId]            INT             NOT NULL,
    [LegCompetitorIdSource]      CHAR (3)        NOT NULL,
    [NumberOfPositions]          TINYINT         NOT NULL,
    [PositionNumber]             TINYINT         NOT NULL,
    [PositionStake]              DECIMAL (19, 8) NOT NULL,
    [PositionPayout]             DECIMAL (19, 8) NOT NULL,
    [PositionType]               VARCHAR (7)     NOT NULL,
    [NumberOfGroupings]          TINYINT         NOT NULL,
    [GroupingNumber]             TINYINT         NOT NULL,
    [GroupingStake]              DECIMAL (19, 8) NOT NULL,
    [GroupingPayout]             DECIMAL (19, 8) NOT NULL,
    [GroupingType]               VARCHAR (32)    NOT NULL,
    [CompetitorId]               VARCHAR(255)    NOT NULL, -- NEW
    [CompetitorIdSource]         CHAR (3)        NOT NULL, -- NEW
    [Scratched]                  BIT             NOT NULL,
    [MappingId]                  SMALLINT        NULL,
	[InPlay]					 CHAR(1)		 NULL,
	[ClickToCall]				 CHAR(1)		 NULL,
	[CashoutType]				 INT			 NOT NULL,
	[IsDoubleDown]				 BIT			 NOT NULL,
	[IsDoubledDown]				 BIT			 NOT NULL
) ON [par_sch_int_Stage] ([BatchKey]);
GO

ALTER TABLE [Atomic].[Bet] SET (LOCK_ESCALATION = AUTO)
GO









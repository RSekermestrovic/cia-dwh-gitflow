﻿CREATE TABLE [Atomic].[Leg] (
    [BatchKey]                   INT           NOT NULL,
    [Action]                     VARCHAR (10)  NOT NULL,
    [MasterTransactionTimestamp] DATETIME2 (3) NOT NULL,
    [FromDate]                   DATETIME2 (3) NOT NULL,
    [BetId]                      BIGINT        NOT NULL,
    [BetIdSource]                CHAR (3)      NOT NULL,
    [LegId]                      BIGINT        NOT NULL,
    [LegIdSource]                CHAR (3)      NOT NULL,
    [LegStake]                   MONEY         NOT NULL,
    [LegPayout]                  MONEY         NOT NULL,
    [LegNumber]                  TINYINT       NOT NULL,
    [LegType]                    VARCHAR (32)  NOT NULL,
    [GroupingType]               VARCHAR (32)  NOT NULL,
    [ClassId]                    INT           NOT NULL,
    [ClassIdSource]              CHAR (3)      NOT NULL,
    [EventID]                    INT           NOT NULL,
    [EventIdSource]              CHAR (3)      NOT NULL,
    [LegCompetitorId]            INT           NOT NULL,
    [LegCompetitorIdSource]      CHAR (3)      NOT NULL,
    [NumberOfPositions]          TINYINT       NOT NULL
) ON [par_sch_int_Stage] ([BatchKey])
WITH (DATA_COMPRESSION = ROW );













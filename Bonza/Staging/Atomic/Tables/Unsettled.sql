﻿CREATE TABLE [Atomic].[Unsettled] (
    [BatchKey]   INT    NOT NULL,
    [DayDate]    DATE   NOT NULL,
    [LedgerId]   INT    NOT NULL,
    [BetId]      BIGINT NOT NULL,
    [AllUpID]    BIGINT NULL,
    [EventID]    INT    NOT NULL,
    [SettledLeg] BIT    NOT NULL,
    [Amount]     MONEY  NOT NULL
) ON [par_sch_int_Stage] ([BatchKey])
WITH (DATA_COMPRESSION = ROW );
GO

ALTER TABLE [Atomic].[Unsettled] SET (LOCK_ESCALATION = AUTO)
GO




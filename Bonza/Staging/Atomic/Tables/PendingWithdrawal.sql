﻿CREATE TABLE [Atomic].[PendingWithdrawal] (
    [BatchKey]      INT    NOT NULL,
    [DayDate]       DATE   NOT NULL,
    [LedgerId]      INT    NOT NULL,
    [TransactionId] BIGINT NOT NULL,
    [Amount]        MONEY  NOT NULL
) ON [par_sch_int_Stage] ([BatchKey])
WITH (DATA_COMPRESSION = ROW );
GO

ALTER TABLE [Atomic].[PendingWithdrawal] SET (LOCK_ESCALATION = AUTO)
GO


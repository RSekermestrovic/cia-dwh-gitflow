﻿CREATE TABLE [Intrabet].[WithdrawalTypeMapping] (
    [WithdrawType]        INT          NULL,
    [TransactionType]     VARCHAR (32) NULL,
    [TransactionStatus]   VARCHAR (32) NULL,
    [TransactionMethod]   VARCHAR (32) NULL,
    [TransactionDetailId] CHAR (4)     NULL,
    [BalanceName]         VARCHAR (32) NULL
);


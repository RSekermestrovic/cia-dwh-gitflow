﻿CREATE TABLE [Intrabet].[Withdrawal_Reconciliation] (
    [Company]             VARCHAR (14)   NOT NULL,
    [TransactionType]     VARCHAR (32)   NULL,
    [TransactionMethod]   VARCHAR (32)   NOT NULL,
    [Surname]             VARCHAR (8000) NULL,
    [FirstName]           VARCHAR (8000) NULL,
    [clientid]            INT            NULL,
    [accountnumber]       INT            NULL,
    [transactiondate]     VARCHAR (30)   NULL,
    [Amount]              MONEY          NULL,
    [Notes]               VARCHAR (250)  NOT NULL,
    [transactionid]       BIGINT         NULL,
    [ChannelType]         VARCHAR (32)   NOT NULL,
    [requestid]           VARCHAR (30)   NOT NULL,
    [WithdrawID]          VARCHAR (30)   NOT NULL,
    [ExternalReference]   VARCHAR (100)  NULL,
    [ExternalAdditional1] VARCHAR (30)   NULL,
    [ExternalAdditional2] VARCHAR (8000) NULL,
    [RequestDate]         VARCHAR (30)   NULL,
    [AccountType]         VARCHAR (6)    NOT NULL
);


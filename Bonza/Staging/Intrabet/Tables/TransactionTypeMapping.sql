﻿CREATE TABLE [Intrabet].[TransactionTypeMapping] (
	[TransactionCode] [int] NULL,
	[TransactionTypeID] [char](3) NOT NULL,
	[TransactionType] [varchar](32) NULL,
	[TransactionStatusID] [char](3) NOT NULL,
	[TransactionStatus] [varchar](32) NULL,
	[TransactionMethodID] [char](3) NOT NULL,
	[TransactionMethod] [varchar](32) NULL,
	[TransactionDetailId] [char](4) NULL,
	[BalanceTypeID] [char](3) NULL,
	[BalanceTypeName] [varchar](32) NULL,
	[Direction] [smallint] NULL,
	[IsDeleted] [tinyint] NULL,
	[MappingKey] [int] IDENTITY(1,1) NOT NULL,
	[Enabled] [tinyint] NULL
);


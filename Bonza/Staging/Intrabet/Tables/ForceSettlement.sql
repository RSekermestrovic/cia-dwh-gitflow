﻿CREATE TABLE [Intrabet].[ForceSettlement] (
    [FromDate]    DATETIME2 (3) NOT NULL,
    [EventId]     INT           NOT NULL,
    [SettledDate] DATETIME      NOT NULL
);


GO
CREATE UNIQUE CLUSTERED INDEX [CI_ForceSettlement]
    ON [Intrabet].[ForceSettlement]([FromDate] ASC, [EventId] ASC);


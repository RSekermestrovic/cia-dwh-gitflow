﻿CREATE PROC [Intrabet].[Sp_Class]
(
	@FromDate	datetime2(3),
	@ToDate		datetime2(3),
	@BatchKey	int
)
AS 
BEGIN

	SET TRANSACTION ISOLATION LEVEL SNAPSHOT

BEGIN TRY

	INSERT INTO [Stage].[Class]
	SELECT	@BatchKey BatchKey,	
			EventType ClassId,
			'IBT' Source,
			[Description] SourceClassName,
			'UNKN' ClassCode,
			'Unknown' ClassName,
			'0x' ClassIcon,
			-1 SuperClassKey,
			'Unknown' SuperClassName,
			FromDate,
			CONVERT(datetime2(3),'9999-12-31 00:00:00.000') ToDate
	FROM	[$(Acquisition)].[IntraBet].[tblLUEventTypes]
	WHERE	FromDate > @FromDate AND FromDate <= @ToDate
	OPTION (RECOMPILE)

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Class', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
END


﻿CREATE PROC [Intrabet].[Sp_BetStaging]
(
	@BatchKey		int
)
WITH RECOMPILE
AS 
BEGIN TRY

/*
	Developer: Aaron Jackson
	Date: 23/09/2015
	Desc: 

	Test: EXEC [Intrabet].[Sp_BetStaging] @BatchKey = 1;
*/

	SET ANSI_NULLS ON;
	SET QUOTED_IDENTIFIER ON;
	SET NOCOUNT ON;

	DECLARE @RowCount INT;
	DECLARE @FromDate DATETIME2(3);

	SELECT @FromDate = BatchFromDate
	FROM [Control].[ETLController]
	WHERE BatchKey = @Batchkey;

	DECLARE @ProcName VARCHAR(255) = (SELECT OBJECT_NAME(@@PROCID));

	EXEC [Control].[Sp_Log] @BatchKey, @ProcName, 'LoadIntoStaging', 'Start';

	INSERT INTO [Stage].[Bet] WITH (TABLOCK) ([BatchKey], [BetGroupID], [BetGroupIDSource], [BetId], [BetIdSource], [LegId], [LegIdSource], [LedgerId], [LedgerSource], [WalletId], [PromotionId], [PromotionIDSource], 
										      [MasterTransactionTimestamp],[MasterDayText],[MasterTimeText],[Channel],[BetGroupType],[BetTypeName],[BetSubTypeName],[LegBetTypeName],[GroupingType],[PriceTypeId],
											  [PriceTypeSource],[ClassId],[ClassIdSource],[MarketID],[MarketIDSource],[BetStatus],[Price],[SelectionID],[SelectionIDSource],[Stake],[Payout],[StakeDelta],[PayoutDelta],[Fees])
	SELECT
		 [BatchKey]
		,[BetGroupID]
		,[BetGroupIDSource]
		,[BetId]
		,[BetIdSource]
		,[LegId]
		,[LegIdSource]
		,[LedgerId]
		,[LedgerSource]
		,[WalletId]  /* needs logic */
		,[PromotionId]
		,[PromotionIDSource]
		,[MasterTransactionTimestamp]
		,CONVERT(VARCHAR(11),CONVERT(DATE,a.MasterTransactionTimestamp)) as [MasterDayText]
		,CONVERT(VARCHAR(5),CONVERT(TIME,a.MasterTransactionTimestamp)) as [MasterTimeText]
		,[Channel]
		,[BetGroupType]
		,[BetType]
		,[BetSubType]
		,[LegType]
		,[GroupingType]
		,[PriceTypeId]
		,[PriceTypeSource]
		,[ClassId]
		,[ClassIdSource]
		,[MarketID]
		,[MarketIDSource]
		,[Action] as [BetStatus]
		,NULL as [Price]
		,[CompetitorID] as [SelectionID]
		,[CompetitorIDSource] as [SelectionIDSource]
		,[BetStake] as [Stake]
		,[BetPayout] as [Payout]
		,[BetStakeDelta] as [StakeDelta]
		,[BetPayoutDelta] as [PayoutDelta]    
		,0 as [Fees] /* needs logic */	
	FROM [Atomic].[Bet] a
	WHERE BatchKey = @BatchKey;

	EXEC [Control].Sp_Log @BatchKey, @ProcName, NULL, 'Success';

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, @ProcName , NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
﻿CREATE PROC [Intrabet].[Sp_Promotion]
(
	@BatchKey	INT,
	@BatchFromDate DATETIME2(3),
	@BatchToDate DATETIME2(3)
)

WITH RECOMPILE
AS 
BEGIN
BEGIN TRY

	-- EXEC [Intrabet].[Sp_Promotion] 343, '2015-05-07 00:03:55.053', '2015-05-08 00:00:52.647';

	SET TRANSACTION ISOLATION LEVEL READ COMMITTED --REPEATABLE READ

	EXEC [Control].Sp_Log @BatchKey,'Sp_Promotion','InsertIntoStaging','Start'

	INSERT INTO Stage.Promotion (PromotionId, PromotionType, PromotionName, OfferDate, ExpiryDate, BatchKey, Source)
	SELECT  A.PromotionId, A.PromotionType, MIN(A.PromotionName) as PromotionName, MIN(A.OfferDate) as OfferDate, MAX(A.ExpiryDate) as ExpiryDate, @BatchKey as BatchKey, A.Source 
	FROM 
		(
			SELECT 
				COALESCE(P.PromotionID,B.FreeBetID,-1) AS PromotionID,
				'Free Winnings' AS PromotionType,
				COALESCE(P.PromoDesc,B.ReasonForBet) AS PromotionName,
				CONVERT(DATE,ISNULL(PromoStart,'1900-01-01')) AS OfferDate,
				CONVERT(DATE,ISNULL(PromoEnd,'9999-12-31')) AS ExpiryDate,
				CASE WHEN P.PromotionID IS NOT NULL THEN 'IBP' ELSE 'IBF' END AS SOURCE
				FROM [$(Acquisition)].IntraBet.tblPromotionalBets B
				LEFT JOIN [$(Acquisition)].IntraBet.tblPromotions P	ON B.PromotionID=P.PromotionID AND P.FromDate<=B.FromDate AND P.ToDate>B.FromDate
				WHERE B.FromDate <= @BatchToDate and B.ToDate > @BatchToDate and B.FromDate >= @BatchFromDate
			UNION
			SELECT 
				COALESCE(PromotionID,BTransCode,-1) AS PromotionID,
				'Free Bet' AS WalletName,
				COALESCE(Notes,'N/A') AS PromotionName,
				CASE WHEN AMOUNT<0 THEN '1900-01-01' ELSE CONVERT(DATE,ISNULL(TransactionDate,'1900-01-01')) END AS OfferDate,
				CASE WHEN AMOUNT>0 THEN '9999-12-31' ELSE CONVERT(DATE,ISNULL(TransactionDate,'9999-12-31')) END AS ExpiryDate,
				CASE WHEN PromotionID IS NOT NULL THEN 'IBP' ELSE 'IBB' END AS SOURCE
				FROM [$(Acquisition)].IntraBet.tblBonusTransactions
				WHERE BTransCode NOT IN (-4,-6) and FromDate <= @BatchToDate and ToDate > @BatchToDate and FromDate >= @BatchFromDate

		) A
	WHERE A.PromotionID != 0
	GROUP BY A.PromotionId, A.PromotionType, A.Source;

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Promotion', NULL, 'Success'

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Promotion', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

END
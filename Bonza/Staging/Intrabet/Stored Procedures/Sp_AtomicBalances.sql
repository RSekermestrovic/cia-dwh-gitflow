﻿CREATE PROC [Intrabet].[Sp_AtomicBalances]
(
	@BatchKey	int,
	@FromDate	datetime2(3),
	@ToDate		datetime2(3)
)
WITH RECOMPILE
AS 

BEGIN

	EXEC [Control].Sp_Log	@BatchKey,'Sp_AtomicBalances','IncrementLoadToStaging','Start'

	BEGIN TRY
		SET TRANSACTION ISOLATION LEVEL READ COMMITTED --REPEATABLE READ
		BEGIN TRANSACTION
		;WITH
			d0 AS (	SELECT	CONVERT(date,@ToDate) DayDate, 1 iterations WHERE CONVERT(date,@ToDate) > @FromDate
					UNION ALL
					SELECT DATEADD(DAY,-1,d0.DayDate), d0.iterations + 1 FROM d0 WHERE DATEADD(DAY,-1,d0.DayDate) > @FromDate AND d0.iterations < 100
				),
			-- Limit on recursive iterations is 100 so have to nest recursive calls here to give a maximum period of 10,000 days
			d AS (	SELECT	DayDate FROM d0
					UNION ALL
					SELECT DATEADD(DAY,-100,d.DayDate) FROM d WHERE DATEADD(DAY,-100,d.DayDate) > @FromDate
				)
--		SELECT DayDate INTO #Day FROM d

		INSERT INTO	Atomic.Balance (BatchKey, DayDate, LedgerID, OrgID, OrgName, AccountTypeName, LedgerTypeId, LedgerTypeName, Enabled, Status, StatusName, Balance) 
		SELECT  @BatchKey,
				x.DayDate, 
				x.AccountId LedgerID, 
				ISNULL(c.OrgID,-1) OrgId, 
				CASE c.OrgID WHEN 1 THEN 'SBA' WHEN 2 THEN 'CBT' WHEN 3 THEN 'Tote' WHEN 8 THEN 'TWH' ELSE 'UNK' END OrgName, 
				CASE WHEN c.ClientType & 16 = 16 THEN 'Betback' WHEN SUBSTRING(c.Username,3,5) = 'PTEST' AND LEN(c.Username)=12 THEN 'Test' ELSE 'Client' END AccountTypeName,
				x.Ledger LedgerTypeId, 
				l.LedgerDescription LedgertypeName,
				x.AccountEnabled Enabled, 
				x.Status,
				CASE x.AccountEnabled WHEN 1 THEN 'Open' WHEN 0 THEN 'Closed' ELSE 'Unknown' END StatusName,
				x.Balance
		FROM	(	SELECT  d.DayDate, d.DayDate FromDate, a.AccountId, a.ClientId, a.Ledger, a.AccountEnabled, a.Status, a.Balance
					FROM	/*#Day*/ d 
							INNER JOIN [$(Acquisition)].IntraBet.tblAccounts a WITH (REPEATABLEREAD) ON (a.FromDate <= d.DayDate AND a.ToDate > d.DayDate)
					UNION ALL
					SELECT  CONVERT(date, a.FromDate) DayDate, a.FromDate, a.AccountId, a.ClientId, a.Ledger, a.AccountEnabled, a.Status, 0 Balance
					FROM	[$(Acquisition)].IntraBet.tblAccounts a WITH (REPEATABLEREAD) 
							LEFT JOIN [$(Acquisition)].IntraBet.tblAccounts a1 WITH (REPEATABLEREAD) ON (a1.AccountID = a.AccountID AND a1.ToDate = a.FromDate)
							LEFT JOIN [$(Acquisition)].IntraBet.tblAccounts a2 WITH (REPEATABLEREAD) ON (a2.AccountID = a.AccountID AND a2.FromDate <= CONVERT(date, a.FromDate) AND a2.ToDate > CONVERT(date, a.FromDate))
					WHERE	a.FromDate > @FromDate AND a.FromDate <= @ToDate
							AND a1.AccountID IS NULL
							AND a2.AccountID IS NULL
				) x
				LEFT JOIN [$(Acquisition)].IntraBet.tblClients c WITH (REPEATABLEREAD) ON (c.ClientId = x.ClientId AND c.FromDate <= x.FromDate AND c.ToDate > x.FromDate)
				INNER JOIN [$(Acquisition)].IntraBet.tblLULedgers l WITH (REPEATABLEREAD) ON (l.Ledger = x.Ledger and l.todate = '9999-12-31')
		OPTION (RECOMPILE);

		COMMIT TRANSACTION
		EXEC [Control].Sp_Log	@BatchKey,'Sp_AtomicBalances',NULL,'Success'
	END TRY
	BEGIN CATCH

		declare @ErrorMessage nvarchar(max), @ErrorSeverity int, @ErrorState int;
		select @ErrorMessage = ERROR_MESSAGE() + ' Line ' + cast(ERROR_LINE() as nvarchar(5)), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();
		rollback transaction;
		
		EXEC [Control].Sp_Log	@BatchKey,'Sp_AtomicBalances',NULL,'Failed',@ErrorMessage
		
		raiserror (@ErrorMessage, @ErrorSeverity, @ErrorState);
	
	END CATCH

END


CREATE PROC [Intrabet].[Sp_Instrument_Bpay]
(
	@BatchKey	int,
	@FromDate	datetime2(3),
	@ToDate		datetime2(3),
	@RowsProcessed	int = 0 OUTPUT
)
AS
BEGIN
	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
	DECLARE @Reload varchar(6) = CASE @FromDate WHEN '1900-01-01' THEN 'RELOAD' ELSE NULL END
	DECLARE @RowCount int
	DECLARE @i int
	
	SET TRANSACTION ISOLATION LEVEL SNAPSHOT;

BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------ Initialize Temp Tables ---------------------
	IF OBJECT_ID('TempDB..#Candidates','U') IS NOT NULL BEGIN DROP TABLE #Candidates END;

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Get BPays','Start', @Reload

	BEGIN TRANSACTION Instrument;
	
	SELECT ID,trnType,TimestampPPL,MAX(fromDate) fromDate  
	INTO #Candidates
	FROM (
			
			----------- tblClientBpayDetails ------------------
			SELECT	 ID,trnType,fromDate,TimestampPPL
			FROM (	
					SELECT BPayID AS ID,'BPY' trnType,fromDate,NULL TimestampPPL,ClientID,BillerCode, RefNumber ,Status
					FROM [$(Acquisition)].Intrabet.tblClientBpayDetails 
					WHERE	FromDate > @FromDate AND FromDate <= @ToDate AND ToDate > @FromDate and BPayID is not null		
					UNION ALL
					SELECT BPayID AS ID,'BPY' trnType,ToDate,NULL TimestampPPL,ClientID,BillerCode, RefNumber ,Status
					FROM [$(Acquisition)].Intrabet.tblClientBpayDetails 
					WHERE	ToDate > @FromDate AND ToDate <= @ToDate and BPayID is not null
			) x
			GROUP BY ID,trnType,fromDate,TimestampPPL,ClientID,BillerCode, RefNumber ,Status
			HAVING COUNT(*) <> 2	

		 ) x
	GROUP BY ID,trnType,TimestampPPL
	OPTION (RECOMPILE)
	
	EXEC [Control].Sp_Log	@BatchKey,@Me,'InsertIntoStaging','Start', @Reload, @RowsProcessed = @@ROWCOUNT

	INSERT	into Stage.Instrument (BatchKey,InstrumentId,Source,AccountNumber,SafeAccountNumber,AccountName,BSB,ExpiryDate,Verified,VerifyAmount,InstrumentType,ProviderName,FromDate)
	SELECT	
			@BatchKey						  AS BatchKey, 
			C.id							  AS instrumentID,
			ISNULL(C.trnType, 'Unknown')		  AS Source, 
			ISNULL(Bp.RefNumber,'Unknown')	  AS AccountNumber,
			ISNULL(Bp.RefNumber,'Unknown')	  AS SafeAccountNumber,
			'N/A'						  AS AccountName,		
			ISNULL(Bp.BillerCode,'Unknown')	  AS BSB,
			NULL							  AS ExpiryDate,
			'U/K'						  AS Verified,
			NULL							  AS VerifiedAmount,
			'BPAY'						  AS InstrumentType,
			'Unknown'						  AS ProviderName,
			c.FromDate					  AS FromDate		

	FROM #Candidates C  with (nolock)
	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblClientBpayDetails BP WITH (NOLOCK) ON 
					C.ID =Cast(Bp.BPayID as varchar) 
					AND Bp.FromDate <= c.FromDate AND Bp.ToDate > C.FromDate

	OPTION (RECOMPILE, FORCE ORDER, LOOP JOIN)

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

							
	COMMIT TRANSACTION Instrument;

	EXEC [Control].Sp_Log @BatchKey, @Me, NULL, 'Success', @RowsProcessed = @RowsProcessed

	UPDATE [Control].[ConfigurationItems] Set Value = 'N' Where ConfigurationItem = 'Intrabet.Sp_Instrument_Bpay'

END TRY
BEGIN CATCH

	ROLLBACK TRANSACTION Instrument;
	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Instrument_Bpay', NULL, 'Failed', @ErrorMessage;
	RAISERROR(@ErrorMessage,16,1);
	THROW;

END CATCH
																					   																					
END

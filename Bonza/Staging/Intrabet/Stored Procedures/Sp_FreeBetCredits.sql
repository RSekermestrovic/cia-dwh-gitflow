﻿CREATE PROC [Intrabet].[Sp_FreeBetCredits]
(
	@BatchKey	INT,
	@FromDate	DATETIME2(3),
	@ToDate		DATETIME2(3),
	@RowsProcessed	int = 0 OUTPUT
)
AS 
BEGIN

	SET TRANSACTION ISOLATION LEVEL SNAPSHOT

	DECLARE @RowCount INT;

BEGIN TRY

	EXEC [Control].Sp_Log	@BatchKey,'Sp_FreeBetCredits','LoadIntoStaging','Start'

		INSERT INTO Stage.[Transaction] (BatchKey,TransactionId,TransactionIdSource,BalanceTypeID,TransactionTypeID,TransactionStatusID,TransactionMethodID,TransactionDirection,BetGroupID,BetGroupIDSource,BetId,BetIdSource,LegId,LegIdSource,FreeBetID,ExternalID,External1,
		External2,UserID,UserIDSource,InstrumentID,InstrumentIDSource,LedgerId,LedgerSource,WalletId,PromotionId,PromotionIdSource,MasterTransactionTimestamp,MasterDayText,MasterTimeText,Channel,ActionChannel,CampaignId,TransactedAmount,BetGroupType,
		BetTypeName,BetSubTypeName,LegBetTypeName,GroupingType,PriceTypeId,PriceTypeSource,ClassId,ClassIdSource,EventID,EventIDSource,MarketId,MarketIdSource,LegNumber,NumberOfLegs,ExistsInDim,Conditional,MappingType,MappingId,FromDate,DateTimeUpdated,RequestID,WithdrawID,
		InPlay,ClickToCall,CashoutType,IsDoubleDown,IsDoubledDown)

		SELECT   DISTINCT   
			@BATCHKEY										AS BatchKey
			,x.TransactionId								AS TransactionID
			,X.Source										AS TransactionIDSource
			,m.BalanceTypeID								AS BalanceTypeID				-- Changed from BalanceName on 6Mar15
			,m.TransactionTypeID							AS TransactionTypeID
			,m.TransactionStatusID							AS TransactionStatusID
			,m.TransactionMethodID							AS TransactionMethodID
			,m.TransactionDirection							AS TransactionDirection
			,X.BetId										AS BetGroupID
			,X.Source										AS BetGroupIDSource
			,X.BetId										AS BetID
			,X.Source										AS BetIDSource
			,X.BetId										AS LegID		
			,X.Source										AS LegIDSource
			,ISNULL(x.PromotionId,-1)						AS FreeBetID /* AJ - wallet change */
			,'N/A'											AS ExternalID
			,'N/A'											AS ExternalID1
			,'N/A'											AS ExternalID2
			,0												AS userID
			,'IUU'											AS UserIDSource
			,0												AS InstrumentID
			,'N/A'											AS InstrumentIDSource
			,COALESCE(D.AccountId,-1)						AS LedgerId
			,'IAA'											AS LedgerSource

			,ISNULL(x.WalletId, -1)							AS WalletId /* AJ - wallet change */
			,ISNULL(x.PromotionId,-1)						AS PromotionId /* AJ - wallet change */
			,X.Source										AS PromotionIdSource /* AJ - wallet change */
			
			,ISNULL(x.TransactionTimestamp,'1900-01-01')							AS MasterTransactionTimestamp
			,CONVERT(varchar(10),ISNULL(x.TransactionTimestamp,'1900-01-01'),120)	AS MasterDayText
			,CONVERT(varchar(5),ISNULL(x.TransactionTimestamp,'1900-01-01'),108)	AS MasterTimeText
			,0												AS Channel
			,0												AS ActionChannel
			,x.CampaignId									AS CampaignId
			,x.TransactedAmount * m.Multiplier				AS TransactedAmount
			,'N/A'											AS BetGroupType
			,'N/A'											AS BetTypeName
			,'N/A'											AS BetSubTypeName
			,'N/A'											AS LegBetTypeName
			,'N/A'											AS GroupingType
			,0												AS PriceTypeID /* AJ - PriceType Change */
			,'N/A'											AS PriceTypeSource /* AJ - PriceType Change */
			,0												AS ClassID
			,'IBT'											AS ClassIDSource
			,0												AS EventID
			,'N/A'											AS EventIDSource
			,0												AS MarketID /* AJ - MarketId Change */
			,'N/A'											AS MarketIDSource /* AJ - MarketId Change */
			,0												AS LegNumber
			,0												AS NumberOfLegs
			,0												AS ExistsInDim
			,0												AS Conditional
			,90												AS MappingType
			,5000											AS MappingID
			,x.FromDate										AS FromDate
			,0												AS UpdateDateTime
			,NULL											AS RequestID
			,NULL											AS WithdrawID
			,'N/A'											AS InPlay
			,'N/A'											AS ClickToCall
			,0												AS CashoutType
			,0												AS IsDoubleDown
			,0												AS IsDoubledDown
		FROM   (

			-------------- FREEBETS BEFORE JUN 2012 --------------------

				------------ VCI --------------------
				-- First Record of the FreeBETID
				-- FromDate of the record is between the Period of concern
				-- Dont include FreeBETID's where PromotionID IS NULL
				-------------------------------------

				SELECT 
					'+' Direction
					,- B.FreeBetID * 10 BetId
					,- B.FreeBetID * 10 TransactionId
					,B.ClientID
					,CASE WHEN (B.FreeBetID IS NULL OR B.FreeBetID = 0) THEN 'C' WHEN B.FreeBetID IS NOT NULL THEN 'B' ELSE 'U' END AS WalletID 
					,COALESCE(P.PromotionID,B.FreeBetID,-1) AS PromotionId											-- NULL handled Later
					,0 CampaignId
					,B.Value TransactedAmount
					,B.IssueDate TransactionTimestamp
					,B.FromDate
					,CASE WHEN P.PromotionID IS NOT NULL THEN 'IBP' ELSE 'IBF' END AS SOURCE						-- Case added Later
				FROM [$(Acquisition)].IntraBet.tblPromotionalBets B
				INNER JOIN [$(Acquisition)].IntraBet.tblPromotions P
					ON B.PromotionID=P.PromotionID AND P.FromDate<=B.FromDate AND P.ToDate>B.FromDate
				LEFT OUTER JOIN [$(Acquisition)].IntraBet.tblPromotionalBets C
					ON B.FreeBetID=C.FreeBetID AND P.FromDate=C.ToDate
				WHERE B.FromDate>=@FromDate AND B.FromDate<@ToDate
		--		AND B.PromotionID IS NOT NULL																		-- Removed Later
				AND C.FreeBetID IS NULL

				UNION ALL

				------------ VEI --------------------
				-- FromDate of the record is between the Period of concern
				-- BetUsed=1 but there in no entry of the Bet in the TblBets, hence should be an anomaly, classified as expiry.
				-- Dont include FreeBETID's where PromotionID IS NULL
				-------------------------------------

				SELECT 
					'-' Direction
					,- B.FreeBetID * 10 BetId
					,- B.FreeBetID * 10 TransactionId
					,B.ClientID
					,CASE WHEN (B.FreeBetID IS NULL OR B.FreeBetID = 0) THEN 'C' WHEN B.FreeBetID IS NOT NULL THEN 'B' ELSE 'U' END AS WalletID 
					,COALESCE(P.PromotionID,B.FreeBetID,-1) AS PromotionId
					,0 CampaignId
					,B.Value TransactedAmount
					,B.ExpiryDate TransactionTimestamp
					,B.FromDate
					,CASE WHEN P.PromotionID IS NOT NULL THEN 'IBP' ELSE 'IBF' END AS SOURCE
				FROM [$(Acquisition)].IntraBet.tblPromotionalBets B
				INNER JOIN [$(Acquisition)].IntraBet.tblPromotions P
					ON B.PromotionID=P.PromotionID AND P.FromDate<=B.FromDate AND P.ToDate>B.FromDate
				LEFT OUTER JOIN [$(Acquisition)].IntraBet.tblBets C
					ON B.FreeBetID=C.FreeBetID 
				WHERE B.FromDate>=@FromDate AND B.FromDate<@ToDate
		--		AND B.PromotionID IS NOT NULL -- Removed Later
				AND B.BetUsed=1
				AND C.FreeBetID IS NULL

				------------ VEI --------------------
				-- FromDate of the record is between the Period of concern
				-- BetUsed=0 and Expirydate is less than or equal to FromDate AND Bet has not been placed at a later date for the same FreeBetID
				-- Dont include FreeBETID's where PromotionID IS NULL
				-------------------------------------

				UNION ALL

				SELECT 
					'-' Direction
					,- B.FreeBetID * 10 BetId
					,- B.FreeBetID * 10 TransactionId
					,B.ClientID
					,CASE WHEN (B.FreeBetID IS NULL OR B.FreeBetID = 0) THEN 'C' WHEN B.FreeBetID IS NOT NULL THEN 'B' ELSE 'U' END AS WalletID 
					,COALESCE(P.PromotionID,B.FreeBetID,-1) AS PromotionId
					,0 CampaignId
					,B.Value TransactedAmount
					,B.ExpiryDate TransactionTimestamp
					,B.FromDate
					,CASE WHEN P.PromotionID IS NOT NULL THEN 'IBP' ELSE 'IBF' END AS SOURCE
				FROM [$(Acquisition)].IntraBet.tblPromotionalBets B
				INNER JOIN [$(Acquisition)].IntraBet.tblPromotions P
					ON B.PromotionID=P.PromotionID AND P.FromDate<=B.FromDate AND P.ToDate>B.FromDate
				WHERE B.FreeBetID NOT IN (SELECT FreeBetID FROM [$(Acquisition)].IntraBet.tblPromotionalBets WHERE BetUsed=1 GROUP BY FreeBetID)
				AND B.FromDate>=@FromDate AND B.FromDate<@ToDate
		--		AND B.PromotionID IS NOT NULL -- Removed Later
				AND B.BetUsed=0
				AND B.ExpiryDate<=B.FromDate
				
				---------------- FREEBETS AFTER SEP 2011 -----------------------------
		
				----------------BOTH VCI AND VEI ----------------------

				UNION ALL

		-- Approved Transactions (Including before delete transaction)

				SELECT 
						CASE WHEN t.Amount<0 THEN '-' ELSE '+' END AS Direction
						,-t.BonusTransID *10 BetID
						,-t.BonusTransID *10 TransactionID
						,t.ClientID
						,CASE WHEN (t.BTransCode IS NULL OR t.BTransCode = 0) THEN 'C' WHEN t.BTransCode IS NOT NULL THEN 'B' ELSE 'U' END AS WalletID --AJ: verify this
						,CASE WHEN t.Amount <0 THEN NULL ELSE COALESCE(NULLIF(t.PromotionID,0),t.BTransCode,-1) END AS PromotionId
						,0 CampaignId
						,Abs(t.Amount) TransactedAmount
						,t.TransactionDate TransactionTimestamp
						,t.FromDate
						,CASE WHEN NULLIF(t.PromotionID,0) IS NOT NULL THEN 'IBP' ELSE 'IBB' END AS SOURCE
					FROM [$(Acquisition)].IntraBet.tblBonusTransactions t
					LEFT OUTER JOIN [$(Acquisition)].IntraBet.tblBonusTransactions ta	ON t.BonusTransID=ta.BonusTransID and t.ToDate=ta.FromDate			-- Is the last record for a CDC Change
					WHERE t.BTransCode <>-4																												-- Dont Include Bets (-4)
					AND NOT (t.BTransCode =15 AND t.BETID IS NOT NULL)																				-- Include Bonus Bets cancelled (15) only if betid is NULL (bet cancel logic takes care if BetID is not null)
					AND t.FromDate>@FromDate AND t.FromDate<=@ToDate																					-- FromDate between the ETL Run Period
					AND ta.BonusTransID IS NULL																											-- Not a CDC Change

				-- DELETED Transactions (Only after delete Transaction)
			
					UNION ALL

					SELECT 
						CASE WHEN t.Amount<0 THEN 'd-' ELSE 'd+' END AS Direction
						,-t.BonusTransID *10 BetID
						,-t.BonusTransID *10 TransactionID
						,t.ClientID
						,CASE WHEN (t.BTransCode IS NULL OR t.BTransCode = 0) THEN 'C' WHEN t.BTransCode IS NOT NULL THEN 'B' ELSE 'U' END AS WalletID --AJ: verify this
						,CASE WHEN t.Amount <0 THEN NULL ELSE COALESCE(NULLIF(t.PromotionID,0),t.BTransCode,-1) END AS PromotionId
						,0 CampaignId
						,Abs(t.Amount) TransactedAmount
						,ISNULL(LT.CorrectedDate,t.todate) TransactionTimestamp																			-- ToDate of the delete record is when the Transaction was deleted.
						,t.Todate FromDate																								-- ToDate of the delete record is when the Transaction was deleted.
						,CASE WHEN NULLIF(t.PromotionID,0) IS NOT NULL THEN 'IBP' ELSE 'IBB' END AS SOURCE
					FROM [$(Acquisition)].IntraBet.tblBonusTransactions t
					LEFT OUTER JOIN [$(Acquisition)].IntraBet.tblBonusTransactions ta	ON t.BonusTransID=ta.BonusTransID and t.ToDate=ta.FromDate			-- Is the last record, genuine delete record
					LEFT OUTER JOIN [$(Acquisition)].IntraBet.Latency LT ON t.ToDate=LT.OriginalDate											-- Join to Latency Table, on ToDate of the deleted record
					WHERE t.BTransCode <>-4																												-- Dont Include Bets (-4)
					AND NOT (t.BTransCode =15 AND t.BETID IS NOT NULL)																				-- Include Bonus Bets cancelled (15) --only if betid is NULL (bet cancel logic takes care if BetID is not null)
					AND t.ToDate>@FromDate AND t.ToDate<=@ToDate																						-- ToDate between the ETL Run Period (genuine delete record)
					AND ta.BonusTransID IS NULL																											-- Not a CDC Change


			)X
		INNER JOIN (SELECT '+' Direction, 'VCI-' TransactionDetailId, 'P&L' BalanceTypeID, -1 Multiplier,'BO' TransactionTypeID, 'CO' TransactionStatusID, 'BO' TransactionMethodID, 'DR' TransactionDirection			-- Changed from BalanceName on 6Mar15 (All below)
					UNION ALL
					SELECT '+' Direction, 'VCI+' TransactionDetailId, 'CLI' BalanceTypeID, 1 Multiplier,'BO' TransactionTypeID, 'CO' TransactionStatusID, 'BO' TransactionMethodID, 'CR' TransactionDirection	
					UNION ALL
					SELECT '-' Direction, 'VEI-' TransactionDetailId, 'CLI' BalanceTypeID, -1 Multiplier,'BO' TransactionTypeID, 'EX' TransactionStatusID, 'BO' TransactionMethodID, 'DR' TransactionDirection
					UNION ALL
					SELECT '-' Direction, 'VEI+' TransactionDetailId, 'P&L' BalanceTypeID, 1 Multiplier,'BO' TransactionTypeID, 'EX' TransactionStatusID, 'BO' TransactionMethodID, 'CR' TransactionDirection
					UNION ALL
					SELECT 'd+' Direction, 'VCI-' TransactionDetailId, 'CLI' BalanceTypeID, -1 Multiplier,'BO' TransactionTypeID, 'CO' TransactionStatusID, 'BO' TransactionMethodID, 'DR' TransactionDirection	
					UNION ALL
					SELECT 'd+' Direction, 'VCI+' TransactionDetailId, 'P&L' BalanceTypeID, 1 Multiplier,'BO' TransactionTypeID, 'CO' TransactionStatusID, 'BO' TransactionMethodID, 'CR' TransactionDirection	
					UNION ALL
					SELECT 'd-' Direction, 'VEI-' TransactionDetailId, 'P&L' BalanceTypeID, -1 Multiplier,'BO' TransactionTypeID, 'EX' TransactionStatusID, 'BO' TransactionMethodID, 'DR' TransactionDirection
					UNION ALL
					SELECT 'd-' Direction, 'VEI+' TransactionDetailId, 'CLI' BalanceTypeID, 1 Multiplier,'BO' TransactionTypeID, 'EX' TransactionStatusID, 'BO' TransactionMethodID, 'CR' TransactionDirection
				   ) M 
			ON (M.Direction = X.Direction)
		LEFT OUTER JOIN [$(Acquisition)].IntraBet.tblClients C					ON X.ClientID=C.ClientID AND C.ToDate='9999-12-31 00:00:00.000'
		LEFT OUTER JOIN [$(Acquisition)].IntraBet.tblAccounts D				ON X.ClientID=D.ClientID AND D.ToDate='9999-12-31 00:00:00.000'
	OPTION (RECOMPILE)

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey, 'Sp_FreeBetCredits', NULL, 'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_FreeBetCredits', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

END

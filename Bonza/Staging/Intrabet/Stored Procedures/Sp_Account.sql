﻿CREATE PROCEDURE [Intrabet].[Sp_Account]
(
	@BatchKey		int,
	@FromDate		datetime2(3),
	@ToDate			datetime2(3),
	@RowsProcessed	int = 0 OUTPUT
)
AS

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
	DECLARE @Reload varchar(6) = CASE @FromDate WHEN '1900-01-01' THEN 'RELOAD' ELSE NULL END
	DECLARE @RowCount int
	DECLARE @i int

	SET TRANSACTION ISOLATION LEVEL SNAPSHOT;

BEGIN TRY

	------ Initialize Temp Tables ---------------------

	IF OBJECT_ID('TempDB..#Clients','U') IS NOT NULL BEGIN DROP TABLE #Clients END;
	IF OBJECT_ID('TempDB..#Accounts','U') IS NOT NULL BEGIN DROP TABLE #Accounts END;
	IF OBJECT_ID('TempDB..#Candidates','U') IS NOT NULL BEGIN DROP TABLE #Candidates END;
	IF OBJECT_ID('TempDB..#MissingOpenDate','U') IS NOT NULL BEGIN DROP TABLE #MissingOpenDate END;
	IF OBJECT_ID('TempDB..#DerivedOpenDate','U') IS NOT NULL BEGIN DROP TABLE #DerivedOpenDate END;

	----------------------------------------------------------------------------------------------------------------------------------

	EXEC [Control].Sp_Log	@BatchKey,@Me,'ClientChanges','Start', @Reload

	SELECT	ClientId, MIN(FromDate) FirstDate, MAX(FromDate) FromDate
	INTO	#Clients
	FROM	(
				----------- tblClients -----------
				SELECT	ClientId, FromDate
				FROM (	SELECT	ClientID, FromDate, Title, FirstName, MiddleName, Surname, DOB, Gender, OrgID, ClientType, PIN, CB_Client_key, 1 trg
						FROM	[$(Acquisition)].[IntraBet].tblClients
						WHERE	FromDate > @FromDate AND FromDate <= @ToDate AND ToDate > @FromDate
						UNION ALL
						SELECT	ClientID, ToDate, Title, FirstName, MiddleName, Surname, DOB, Gender, OrgID, ClientType, PIN, CB_Client_key, 2 trg
						FROM	[$(Acquisition)].[IntraBet].tblClients
						WHERE	ToDate > @FromDate AND ToDate <= @ToDate
					) x
				GROUP BY ClientID, FromDate, Title, FirstName, MiddleName, Surname, DOB, Gender, OrgID, ClientType, PIN, CB_Client_key
				HAVING COUNT(*) <> 2 AND MIN(trg) = 1 -- capture changes excluding deletions (want to keep last version prior to deletion)
				UNION ALL
				----------- tblClientInfo -----------
				SELECT	ClientId, FromDate
				FROM (	SELECT	ClientID, FromDate, StartDate, StatementFrequency, StatementMethod, Channel, 1 trg
						FROM	[$(Acquisition)].[IntraBet].tblClientInfo
						WHERE	FromDate > @FromDate AND FromDate <= @ToDate AND ToDate > @FromDate
						UNION ALL
						SELECT	ClientID, ToDate, StartDate, StatementFrequency, StatementMethod, Channel, 1 trg
						FROM	[$(Acquisition)].[IntraBet].tblClientInfo
						WHERE	ToDate > @FromDate AND ToDate <= @ToDate
					) x
				GROUP BY ClientID, FromDate, StartDate, StatementFrequency, StatementMethod, Channel
				HAVING COUNT(*) <> 2 AND MIN(trg) = 1 -- capture changes excluding deletions (want to keep last version prior to deletion)
				UNION ALL
				----------- tblClientAddresses ---------
				SELECT	ClientId, FromDate
				FROM (	SELECT	ClientID, AddressType, FromDate, EMAIL, Phone, FAX, Address1, Address2, Suburb, State, PostCode, Mobile, Country, altEmail, StateID, 1 trg
						FROM	[$(Acquisition)].[IntraBet].tblClientAddresses
						WHERE	FromDate > @FromDate AND FromDate <= @ToDate AND ToDate > @FromDate
						UNION ALL
						SELECT	ClientID, AddressType, ToDate, EMAIL, Phone, FAX, Address1, Address2, Suburb, State, PostCode, Mobile, Country, altEmail, StateID, 2 trg
						FROM	[$(Acquisition)].[IntraBet].tblClientAddresses
						WHERE	ToDate > @FromDate AND ToDate <= @ToDate
					) x
				GROUP BY ClientID, AddressType, FromDate, EMAIL, Phone, FAX, Address1, Address2, Suburb, State, PostCode, Mobile, Country, altEmail, StateID
				HAVING COUNT(*) <> 2 AND MIN(trg) = 1 -- capture changes excluding deletions (want to keep last version prior to deletion)
				UNION ALL
				----------- tblClientAffiliateInfo ---------
				SELECT	ClientId, FromDate
				FROM (	SELECT	ClientID, FromDate, BTag2, TrafficSource, RefURL, CampaignID, Keywords, 1 trg
						FROM	[$(Acquisition)].[IntraBet].tblClientAffiliateInfo
						WHERE	FromDate > @FromDate AND FromDate <= @ToDate AND ToDate > @FromDate
						UNION ALL
						SELECT	ClientID, ToDate, BTag2, TrafficSource, RefURL, CampaignID, Keywords, 2 trg
						FROM	[$(Acquisition)].[IntraBet].tblClientAffiliateInfo
						WHERE	ToDate > @FromDate AND ToDate <= @ToDate
					) x
				GROUP BY ClientID, FromDate, BTag2, TrafficSource, RefURL, CampaignID, Keywords
				HAVING COUNT(*) <> 2 AND MIN(trg) = 1 -- capture changes excluding deletions (want to keep last version prior to deletion)
				UNION ALL
				----------- tblSubscriptionCentre ---------
				SELECT	ClientId, FromDate
				FROM (	SELECT	ClientID, FromDate, exactTargetID, 1 trg
						FROM	[$(Acquisition)].[IntraBet].tblSubscriptionCentre
						WHERE	FromDate > @FromDate AND FromDate <= @ToDate AND ToDate > @FromDate
						UNION ALL
						SELECT	ClientID, ToDate, exactTargetID, 2 trg
						FROM	[$(Acquisition)].[IntraBet].tblSubscriptionCentre
						WHERE	ToDate > @FromDate AND ToDate <= @ToDate
					) x
				GROUP BY ClientID, FromDate, exactTargetID
				HAVING COUNT(*) <> 2 AND MIN(trg) = 1 -- capture changes excluding deletions (want to keep last version prior to deletion)
			) x
	GROUP BY ClientId
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'AccountChanges','Start', @Reload, @RowsProcessed = @@ROWCOUNT

	SELECT	AccountId, ClientId, AccountNumber, MIN(FromDate) FirstDate, MAX(FromDate) FromDate, MAX(NewLedger) NewLedger
	INTO	#Accounts
	FROM	(	SELECT	AccountId, ClientId, AccountNumber, FromDate, MIN(NewLedger) NewLedger
				FROM	(	SELECT	AccountId, ClientId, AccountNumber, FromDate, MIN(NewLedger) NewLedger
							FROM (	SELECT	AccountId, ClientID, AccountNumber, FromDate, 1 NewLedger, Ledger, 1 trg
									FROM	[$(Acquisition)].[IntraBet].tblAccounts
									WHERE	FromDate > @FromDate AND FromDate <= @ToDate AND ToDate > @FromDate
									UNION ALL
									SELECT	AccountId, ClientID, AccountNumber, ToDate, 0 NewLedger, Ledger, 2 trg
									FROM	[$(Acquisition)].[IntraBet].tblAccounts
									WHERE	ToDate > @FromDate AND ToDate <= @ToDate
								) x
							GROUP BY AccountId, ClientID, AccountNumber, FromDate, Ledger
							HAVING COUNT(*) <> 2 AND MIN(trg) = 1 -- capture changes excluding deletions (want to keep last version prior to deletion)
						) x
				GROUP BY AccountId, ClientID, AccountNumber, FromDate
			) y
	GROUP BY AccountId, ClientID, AccountNumber
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'CombinedChanges','Start', @Reload, @RowsProcessed = @@ROWCOUNT

	SELECT	AccountId, ClientId, AccountNumber, MAX(FirstDate) FirstDate, MAX(FromDate) FromDate, MAX(NewLedger) NewLedger
	INTO	#Candidates
	FROM	(	SELECT	AccountId, ClientId, AccountNumber, FirstDate, FromDate, NewLedger
				FROM	#Accounts
				UNION ALL
				SELECT	a.AccountId, a.ClientId, a.AccountNumber, c.FirstDate, c.FromDate, 0 NewLedger
				FROM	#Clients c
						INNER JOIN [$(Acquisition)].IntraBet.tblAccounts a ON (a.ClientID = c.ClientID AND a.FromDate <= c.FromDate AND a.ToDate > c.Fromdate)
			) x
	GROUP BY AccountId, ClientId, AccountNumber
	OPTION (RECOMPILE, TABLE HINT(a, FORCESEEK))

	EXEC [Control].Sp_Log	@BatchKey,@Me,'MissingOpenDate','Start', @Reload, @RowsProcessed = @@ROWCOUNT

	SELECT	c.AccountId, c.ClientID, c.AccountNumber, c.FromDate
	INTO	#MissingOpenDate
	FROM	#Candidates c
			LEFT JOIN [$(Acquisition)].[IntraBet].tblClientInfo i ON i.ClientID = c.ClientID AND i.FromDate <= c.FromDate AND i.ToDate > c.FromDate
	WHERE	c.AccountNumber > 1 OR i.StartDate = CONVERT(datetime,'1900-01-01') OR i.StartDate IS NULL
	OPTION (RECOMPILE, TABLE HINT(i, FORCESEEK))


	EXEC [Control].Sp_Log	@BatchKey,@Me,'DerivedOpenDate','Start', @Reload, @RowsProcessed = @@ROWCOUNT

	SELECT	c.AccountId, MIN(ISNULL(t.TransactionDate,a.FromDate)) AccountOpenedDate, CASE COUNT(t.ClientId) WHEN 0 THEN 'Account' ELSE 'Transaction' END Origin
	INTO	#DerivedOpenDate
	FROM	#MissingOpenDate c
			INNER JOIN [$(Acquisition)].IntraBet.tblAccounts a ON (a.AccountID = c.AccountID)
			LEFT JOIN [$(Acquisition)].IntraBet.tblTransactions t ON (t.ClientID = c.ClientID AND t.AccountNumber = c.AccountNumber AND t.FromDate = CONVERT(datetime2(3),'2013-12-19') AND a.FromDate = CONVERT(datetime2(3),'2013-12-19'))
	GROUP BY c.AccountID
	OPTION (RECOMPILE, TABLE HINT(a, FORCESEEK), TABLE HINT(t, FORCESEEK))

	EXEC [Control].Sp_Log	@BatchKey,@Me,'InsertIntoStaging','Start', @Reload, @RowsProcessed = @@ROWCOUNT

	INSERT	Stage.Account
		(	BatchKey, 
			LedgerID, LedgerSource, LedgerSequenceNumber, LegacyLedgerID, LedgerTypeID, LedgerType, LedgerOpenedDate, LedgerOpenedDayText, LedgerOpenedDateOrigin,
			AccountID, AccountSource, PIN, UserName, ExactTargetID, AccountFlags,
			AccountOpenedChannelId, SourceBTag2, SourceTrafficSource, SourceRefURL, SourceCampaignId, SourceKeywords,
			StatementMethod, StatementFrequency,
			AccountTitle, AccountSurname, AccountFirstName, AccountMiddleName, AccountGender, AccountDOB,
			HasHomeAddress, HomeAddress1, HomeAddress2, HomeSuburb, HomeStateCode, HomeState, HomePostCode, HomeCountryCode, HomeCountry,
			HomePhone, HomeMobile, HomeFax, HomeEmail, HomeAlternateEmail, 
			HasPostalAddress, PostalAddress1, PostalAddress2, PostalSuburb, PostalStateCode, PostalState, PostalPostCode, PostalCountryCode, PostalCountry,
			PostalPhone, PostalMobile, PostalFax, PostalEmail, PostalAlternateEmail,
			HasBusinessAddress, BusinessAddress1, BusinessAddress2, BusinessSuburb, BusinessStateCode, BusinessState, BusinessPostCode, BusinessCountryCode, BusinessCountry,
			BusinessPhone, BusinessMobile, BusinessFax, BusinessEmail, BusinessAlternateEmail,
			HasOtherAddress, OtherAddress1, OtherAddress2, OtherSuburb, OtherStateCode, OtherState, OtherPostCode, OtherCountryCode, OtherCountry,
			OtherPhone, OtherMobile, OtherFax, OtherEmail, OtherAlternateEmail,
			FirstDate, FromDate
		)
	SELECT	@BatchKey BatchKey,
			x.AccountID LedgerId,
			'IAA' as LedgerSource,
			x.AccountNumber LedgerSequenceNumber,
			CASE ISNULL(C.CB_Client_key,0) WHEN 0 THEN x.AccountID ELSE c.CB_Client_key END LegacyLedgerID,
			a.Ledger LedgerTypeID,
			ISNULL(l.LedgerDescription,'Unknown') LedgerType,
			ISNULL(dd.AccountOpenedDate, ci.StartDate) LedgerOpenedDate,
			CONVERT(char(10),CONVERT(date,ISNULL(dd.AccountOpenedDate, ci.StartDate))) LedgerOpenedDayText,
			ISNULL(dd.Origin, 'Client') LedgerOpenedDateOrigin,
			x.ClientID AccountID,
			'IAC' AccountSource,
			c.PIN,
			c.Username,
			ISNULL(CONVERT(varchar(36), sc.exactTargetID),'N/A') ExactTargetID,
			c.ClientType AccountFlags,
			ISNULL(ci.Channel, -1) AccountOpenedChannelId,
			cai.BTag2 SourceBTag2,
			cai.TrafficSource SourceTrafficSource,
			cai.RefURL SourceRefURL,
			cai.CampaignID SourceCampaignID,
			cai.Keywords SourceKeywords,
			ISNULL(sm.[Description], 'Unknown') StatementMethod,
			ISNULL(sf.[Description], 'Unknown') StatementFrequency,
			c.Title AccountTitle,
			c.Surname AccountSurname,
			c.FirstName AccountFirstName,
			c.MiddleName AccountMiddleName,
			c.Gender AccountGender,
			c.DOB AccountDOB,
			CASE WHEN ha.AddressType IS NOT NULL THEN 1 ELSE 0 END HasHomeAddress,
			ha.Address1 HomeAddress1,
			ha.Address2 HomeAddress2,
			ha.Suburb HomeSuburb,
			ha.State HomeStateCode,
			hs.StateName HomeState,
			ha.PostCode HomePostCode,
			hc.CountryAbbr HomeCountryCode,
			hc.CountryName HomeCountry,
			ha.Phone HomePhone,
			ha.Mobile HomeMobile,
			ha.Fax HomeFax,
			ha.Email HomeEmail,
			ha.altEmail HomeAlternateEmail,
			CASE WHEN pa.AddressType IS NOT NULL THEN 1 ELSE 0 END HasPostalAddress,
			pa.Address1 PostalAddress1,
			pa.Address2 PostalAddress2,
			pa.Suburb PostalSuburb,
			pa.State PostalStateCode,
			ps.StateName PostalState,
			pa.PostCode PostalPostCode,
			pc.CountryAbbr PostalCountryCode,
			pc.CountryName PostalCountry,
			pa.Phone PostalPhone,
			pa.Mobile PostalMobile,
			pa.Fax PostalFax,
			pa.Email PostalEmail,
			pa.altEmail PostalAlternateEmail,
			CASE WHEN ba.AddressType IS NOT NULL THEN 1 ELSE 0 END HasBusinessAddress,
			ba.Address1 BusinessAddress1,
			ba.Address2 BusinessAddress2,
			ba.Suburb BusinessSuburb,
			ba.State BusinessStateCode,
			bs.StateName BusinessState,
			ba.PostCode BusinessPostCode,
			bc.CountryAbbr BusinessCountryCode,
			bc.CountryName BusinessCountry,
			ba.Phone BusinessPhone,
			ba.Mobile BusinessMobile,
			ba.Fax BusinessFax,
			ba.Email BusinessEmail,
			ba.altEmail BusinessAlternateEmail,
			CASE WHEN oa.AddressType IS NOT NULL THEN 1 ELSE 0 END HasOtherAddress,
			oa.Address1 OtherAddress1,
			oa.Address2 OtherAddress2,
			oa.Suburb OtherSuburb,
			oa.State OtherStateCode,
			os.StateName OtherState,
			oa.PostCode OtherPostCode,
			oc.CountryAbbr OtherCountryCode,
			oc.CountryName OtherCountry,
			oa.Phone OtherPhone,
			oa.Mobile OtherMobile,
			oa.Fax OtherFax,
			oa.Email OtherEmail,
			oa.altEmail OtherAlternateEmail,
			ISNULL(l1.CorrectedDate,x.FirstDate) FirstDate,
			ISNULL(l2.CorrectedDate,x.FromDate) FromDate
	FROM	#Candidates x
			INNER JOIN [$(Acquisition)].IntraBet.tblAccounts a ON a.AccountID = x.AccountID AND a.FromDate <= x.FromDate AND a.ToDate > x.FromDate
			LEFT JOIN #DerivedOpenDate dd ON dd.AccountID = x.AccountID
			LEFT JOIN [$(Acquisition)].IntraBet.tblLULedgers l ON l.Ledger = a.Ledger AND l.FromDate <= x.FromDate AND l.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].IntraBet.tblClients c ON c.ClientID = x.ClientID AND c.FromDate <= x.FromDate AND c.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].IntraBet.tblClientInfo ci ON ci.ClientID = x.ClientID AND ci.FromDate <= x.FromDate AND ci.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].IntraBet.tblSubscriptionCentre sc ON sc.clientID = x.ClientID AND sc.FromDate <= x.FromDate AND sc.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].IntraBet.tblClientAffiliateInfo cai ON cai.clientID = x.ClientID AND cai.FromDate <= x.FromDate AND cai.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].IntraBet.tblLUStatementMethods sm ON sm.StatementMethod = ci.StatementMethod AND sm.FromDate <= x.FromDate AND sm.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].IntraBet.tblLUStatementFrequency sf ON sf.StatementFrequency = ci.StatementFrequency AND sf.FromDate <= x.FromDate AND sf.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].[IntraBet].tblClientAddresses ha ON ha.ClientID = x.ClientID AND ha.FromDate <= x.FromDate AND ha.ToDate > x.FromDate AND ha.AddressType=1
			LEFT JOIN [$(Acquisition)].[IntraBet].tblLUStates hs ON hs.Code = ha.StateId AND hs.FromDate <= x.FromDate AND hs.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].[IntraBet].tblLUCountry hc ON hc.Country = ha.Country AND hc.FromDate <= x.FromDate AND hc.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].[IntraBet].tblClientAddresses pa ON pa.ClientID = x.ClientID AND pa.FromDate <= x.FromDate AND pa.ToDate > x.FromDate AND pa.AddressType=2
			LEFT JOIN [$(Acquisition)].[IntraBet].tblLUStates ps ON ps.Code = pa.StateId AND ps.FromDate <= x.FromDate AND ps.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].[IntraBet].tblLUCountry pc ON pc.Country = pa.Country AND pc.FromDate <= x.FromDate AND pc.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].[IntraBet].tblClientAddresses ba ON ba.ClientID = x.ClientID AND ba.FromDate <= x.FromDate AND ba.ToDate > x.FromDate AND ba.AddressType=3
			LEFT JOIN [$(Acquisition)].[IntraBet].tblLUStates bs ON bs.Code = ba.StateId AND bs.FromDate <= x.FromDate AND bs.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].[IntraBet].tblLUCountry bc ON bc.Country = ba.Country AND bc.FromDate <= x.FromDate AND bc.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].[IntraBet].tblClientAddresses oa ON oa.ClientID = x.ClientID AND oa.FromDate <= x.FromDate AND oa.ToDate > x.FromDate AND oa.AddressType=4
			LEFT JOIN [$(Acquisition)].[IntraBet].tblLUStates os ON os.Code = oa.StateId AND os.FromDate <= x.FromDate AND os.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].[IntraBet].tblLUCountry oc ON oc.Country = oa.Country AND oc.FromDate <= x.FromDate AND oc.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].Intrabet.Latency  l1 WITH(FORCESEEK)  on l1.OriginalDate = x.FirstDate --AND l1.OriginalDate >= @FromDate
			LEFT JOIN [$(Acquisition)].Intrabet.Latency  l2 WITH(FORCESEEK)  on l2.OriginalDate = x.FromDate --AND l2.OriginalDate >= @FromDate
	OPTION (RECOMPILE, FORCE ORDER, LOOP JOIN) --TABLE HINT(c, FORCESEEK), TABLE HINT(ci, FORCESEEK), TABLE HINT(cai, FORCESEEK), TABLE HINT(sc, FORCESEEK), TABLE HINT(ha, FORCESEEK), TABLE HINT(pa, FORCESEEK), TABLE HINT(ba, FORCESEEK), TABLE HINT(oa, FORCESEEK))

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey, @Me, NULL, 'Success', @RowsProcessed = @RowsProcessed

	Update [Control].[ConfigurationItems] Set Value = 'N' Where ConfigurationItem = 'Intrabet.Sp_Account'

END TRY
BEGIN CATCH
	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Account', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

GO



﻿CREATE PROCEDURE [Intrabet].[Sp_Structure]
(
	@BatchKey	INT,
	@FromDate	DATETIME2(3),
	@ToDate		DATETIME2(3),
	@RowsProcessed	int = 0 OUTPUT
)
AS

	DECLARE @RowCount INT

	SET TRANSACTION ISOLATION LEVEL SNAPSHOT

BEGIN TRY

	------ Initialize Temp Tables ---------------------

	IF OBJECT_ID('TempDB..#Introducers','U') IS NOT NULL BEGIN DROP TABLE #Introducers END
	IF OBJECT_ID('TempDB..#Handlers','U') IS NOT NULL BEGIN DROP TABLE #Handlers END
	IF OBJECT_ID('TempDB..#Candidates','U') IS NOT NULL BEGIN DROP TABLE #Candidates END
	IF OBJECT_ID('TempDB..#Current','U') IS NOT NULL BEGIN DROP TABLE #Current END
	IF OBJECT_ID('TempDB..#PreviousOrg','U') IS NOT NULL BEGIN DROP TABLE #PreviousOrg END
	IF OBJECT_ID('TempDB..#Previous','U') IS NOT NULL BEGIN DROP TABLE #Previous END
	IF OBJECT_ID('TempDB..#FY','U') IS NOT NULL BEGIN DROP TABLE #FY END

	---------------------------------------------------- Get unique ClientID and latest FromDate for all pertinent changes within batch window --------------------------------------

	EXEC [Control].Sp_Log	@BatchKey,'Sp_Structure','Introducer Changes','Start'

	SELECT	ClientId, MAX(FromDate) FromDate
	INTO	#Introducers
	FROM	(	SELECT ClientId, FromDate, IntroducedName, CanManageVIP FROM [$(Acquisition)].IntraBet.tblIntroducers WHERE FromDate > @FromDate AND FromDate <= @ToDate AND ToDate > @ToDate
				UNION ALL
				SELECT ClientId, ToDate, IntroducedName, CanManageVIP FROM [$(Acquisition)].IntraBet.tblIntroducers WHERE ToDate > @FromDate AND ToDate <= @ToDate AND FromDate <= @FromDate
			) x
	GROUP BY ClientId, IntroducedName, CanManageVIP
	HAVING COUNT(*) <> 2
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,'Sp_Structure','Handler Changes','Start', @RowsProcessed = @@ROWCOUNT

	SELECT	DebtHandlerId, MAX(FromDate) FromDate
	INTO	#Handlers
	FROM	(	SELECT DebtHandlerId, FromDate, HandlerName FROM [$(Acquisition)].IntraBet.tblLUDebtHandlers WITH(FORCESEEK) WHERE FromDate > @FromDate AND FromDate <= @ToDate AND ToDate > @ToDate
				UNION ALL
				SELECT DebtHandlerId, ToDate, HandlerName FROM [$(Acquisition)].IntraBet.tblLUDebtHandlers WITH(FORCESEEK) WHERE ToDate > @FromDate AND ToDate <= @ToDate AND FromDate <= @FromDate
			) x
	GROUP BY DebtHandlerId, HandlerName
	HAVING COUNT(*) <> 2
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,'Sp_Structure','Client Changes','Start', @RowsProcessed = @@ROWCOUNT

	SELECT	ClientId, MAX(FromDate) FromDate
	INTO	#Candidates
	FROM	(	SELECT	ClientId, MAX(FromDate) FromDate
				FROM	(	SELECT ClientId, FromDate, OrgId FROM [$(Acquisition)].IntraBet.tblClients WITH(FORCESEEK) WHERE FromDate > @FromDate AND FromDate <= @ToDate AND ToDate > @ToDate
							UNION ALL
							SELECT ClientId, ToDate, OrgId FROM [$(Acquisition)].IntraBet.tblClients WITH(FORCESEEK) WHERE ToDate > @FromDate AND ToDate <= @ToDate AND FromDate <= @FromDate
						) x
				GROUP BY ClientId, OrgId
				HAVING COUNT(*) <> 2
				UNION ALL
				SELECT	ClientId, MAX(FromDate) FromDate
				FROM	(	SELECT ClientId, FromDate, IntroducedBy, ManagedBy, Handled, VIP FROM [$(Acquisition)].IntraBet.tblClientInfo WITH(FORCESEEK) WHERE FromDate > @FromDate AND FromDate <= @ToDate AND ToDate > @ToDate
							UNION ALL
							SELECT ClientId, ToDate, IntroducedBy, ManagedBy, Handled, VIP FROM [$(Acquisition)].IntraBet.tblClientInfo WITH(FORCESEEK) WHERE ToDate > @FromDate AND ToDate <= @ToDate AND FromDate <= @FromDate
						) x
				GROUP BY ClientId, IntroducedBy, ManagedBy, Handled, VIP
				HAVING COUNT(*) <> 2
				UNION ALL
				SELECT	c.ClientId, i.FromDate
				FROM	[$(Acquisition)].IntraBet.tblClientInfo c
						INNER JOIN #Introducers i ON (i.ClientId = c.IntroducedBy AND i.FromDate >= c.FromDate AND i.FromDate < c.ToDate)
				UNION ALL
				SELECT	c.ClientId, h.FromDate
				FROM	[$(Acquisition)].IntraBet.tblClientInfo c
						INNER JOIN #Handlers h ON (h.DebtHandlerId = c.IntroducedBy AND h.FromDate >= c.FromDate AND h.FromDate < c.ToDate)
				UNION ALL
				SELECT	a.ClientId, a.FromDate
				FROM	[$(Acquisition)].IntraBet.tblAccounts a WITH(FORCESEEK)
						LEFT JOIN [$(Acquisition)].IntraBet.tblAccounts a1 WITH(FORCESEEK) ON (a1.AccountID = a.AccountID AND a1.ToDate = a.FromDate)
				WHERE	a.FromDate > @FromDate AND a.FromDate <= @ToDate AND a.ToDate > @FromDate
						AND a1.AccountID IS NULL
			) x
	GROUP BY ClientId
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,'Sp_Structure','Current Attributes','Start', @RowsProcessed = @@ROWCOUNT

	DROP TABLE #Introducers
	DROP TABLE #Handlers

	SELECT	x.ClientId, x.FromDate, ISNULL(c.OrgID,0) OrgId, ISNULL(i.IntroducedBy,0) IntroducedBy, ISNULL(i.ManagedBy,0) ManagedBy, ISNULL(i.Handled,0) Handled, ISNULL(i.VIP,0) VIPType
	INTO	#Current
	FROM	#Candidates x
			INNER JOIN [$(Acquisition)].IntraBet.tblClients c ON (c.ClientID = x.ClientID AND c.FromDate <= x.FromDate AND c.ToDate > x.FromDate)
			INNER JOIN [$(Acquisition)].IntraBet.tblClientInfo i ON (i.ClientID = x.ClientID AND i.FromDate <= x.FromDate AND i.ToDate > x.FromDate)
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,'Sp_Structure','Previous Organisation','Start', @RowsProcessed = @@ROWCOUNT

	DROP TABLE #Candidates

	SELECT	ClientId, FromDate, 
			OrgId, 
			CASE PreviousOrgId WHEN OrgId THEN AbsoluteFromDate ELSE OrgIdFromDate END OrgIdFromDate,
			CASE PreviousOrgId WHEN OrgId THEN NULL ELSE PreviousOrgId END PreviousOrgId,
			IntroducedBy, ManagedBy, Handled, VIPType
	INTO	#PreviousOrg
	FROM	(	SELECT	DISTINCT x.ClientId, x.FromDate, x.OrgID, x.IntroducedBy, x.ManagedBy, x.Handled, x.VIPType,
						FIRST_VALUE(c.ToDate) OVER (PARTITION BY x.ClientId ORDER BY CASE WHEN ISNULL(c.OrgId,0) <> x.OrgId THEN 1 ELSE 2 END, c.FromDate DESC) OrgIdFromDate,
						FIRST_VALUE(ISNULL(c.OrgID,0)) OVER (PARTITION BY x.ClientId ORDER BY CASE WHEN ISNULL(c.OrgId,0) <> x.OrgId THEN 1 ELSE 2 END, c.FromDate DESC) PreviousOrgId,
						FIRST_VALUE(c.FromDate) OVER (PARTITION BY x.ClientId ORDER BY c.FromDate) AbsoluteFromDate
				FROM	#Current x
						INNER JOIN [$(Acquisition)].IntraBet.tblClients c ON (c.ClientID = x.ClientID AND c.FromDate <= x.FromDate)
			) x
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,'Sp_Structure','Previous Attributes','Start', @RowsProcessed = @@ROWCOUNT

	DROP TABLE #Current

	SELECT	ClientId, FromDate, OrgId, OrgIdFromDate, PreviousOrgId, 
			IntroducedBy, 
			CASE PreviousIntroducedBy WHEN IntroducedBy THEN AbsoluteFromDate ELSE IntroducedByFromDate END IntroducedByFromDate,
			CASE PreviousIntroducedBy WHEN IntroducedBy THEN NULL ELSE PreviousIntroducedBy END PreviousIntroducedBy,
			ManagedBy, 
			CASE PreviousManagedBy WHEN ManagedBy THEN AbsoluteFromDate ELSE ManagedByFromDate END ManagedByFromDate,
			CASE PreviousManagedBy WHEN ManagedBy THEN NULL ELSE PreviousManagedBy END PreviousManagedBy,
			Handled, 
			CASE PreviousHandled WHEN Handled THEN AbsoluteFromDate ELSE HandledFromDate END HandledFromDate,
			CASE PreviousHandled WHEN Handled THEN NULL ELSE PreviousHandled END PreviousHandled,
			VIPType,
			CASE PreviousVIPType WHEN VIPType THEN AbsoluteFromDate ELSE VIPTypeFromDate END VIPTypeFromDate,
			CASE PreviousVIPType WHEN VIPType THEN NULL ELSE PreviousVIPType END PreviousVIPType
	INTO	#Previous
	FROM	(	SELECT	DISTINCT x.ClientId, x.FromDate, x.OrgID, x.OrgIdFromDate, x.PreviousOrgId, x.IntroducedBy, x.ManagedBy, x.Handled, x.VIPType,
						FIRST_VALUE(c.ToDate) OVER (PARTITION BY x.ClientId ORDER BY CASE WHEN ISNULL(c.IntroducedBy,0) <> x.IntroducedBy THEN 1 ELSE 2 END, c.FromDate DESC) IntroducedByFromDate,
						FIRST_VALUE(ISNULL(c.IntroducedBy,0)) OVER (PARTITION BY x.ClientId ORDER BY CASE WHEN ISNULL(c.IntroducedBy,0) <> x.IntroducedBy THEN 1 ELSE 2 END, c.FromDate DESC) PreviousIntroducedBy,
						FIRST_VALUE(c.ToDate) OVER (PARTITION BY x.ClientId ORDER BY CASE WHEN ISNULL(c.ManagedBy,0) <> x.ManagedBy THEN 1 ELSE 2 END, c.FromDate DESC) ManagedByFromDate,
						FIRST_VALUE(ISNULL(c.ManagedBy,0)) OVER (PARTITION BY x.ClientId ORDER BY CASE WHEN ISNULL(c.ManagedBy,0) <> x.ManagedBy THEN 1 ELSE 2 END, c.FromDate DESC) PreviousManagedBy,
						FIRST_VALUE(c.ToDate) OVER (PARTITION BY x.ClientId ORDER BY CASE WHEN ISNULL(c.Handled,0) <> x.Handled THEN 1 ELSE 2 END, c.FromDate DESC) HandledFromDate,
						FIRST_VALUE(ISNULL(c.Handled,0)) OVER (PARTITION BY x.ClientId ORDER BY CASE WHEN ISNULL(c.Handled,0) <> x.Handled THEN 1 ELSE 2 END, c.FromDate DESC) PreviousHandled,
						FIRST_VALUE(c.ToDate) OVER (PARTITION BY x.ClientId ORDER BY CASE WHEN ISNULL(c.VIP,0) <> x.VIPType THEN 1 ELSE 2 END, c.FromDate DESC) VIPTypeFromDate,
						FIRST_VALUE(ISNULL(c.VIP,0)) OVER (PARTITION BY x.ClientId ORDER BY CASE WHEN ISNULL(c.VIP,0) <> x.VIPType THEN 1 ELSE 2 END, c.FromDate DESC) PreviousVIPType,
						FIRST_VALUE(c.FromDate) OVER (PARTITION BY x.ClientId ORDER BY c.FromDate) AbsoluteFromDate
				FROM	#PreviousOrg x
						LEFT JOIN [$(Acquisition)].IntraBet.tblClientInfo c ON (c.ClientID = x.ClientID AND c.FromDate <= x.FromDate)
			) x
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,'Sp_Structure','Financial Year Attributes','Start', @RowsProcessed = @@ROWCOUNT

	DROP TABLE #PreviousOrg

	SELECT	x.ClientId,x.FromDate,
			x.OrgID, 
			x.OrgIdFromDate,
			x.PreviousOrgId,
			c2014.OrgID FY2014OrgId,
			c2015.OrgID FY2015OrgId,
			c2016.OrgID FY2016OrgId,
			x.IntroducedBy, 
			x.IntroducedByFromDate,
			x.PreviousIntroducedBy,
			i2014.IntroducedBy FY2014IntroducedBy,
			i2015.IntroducedBy FY2015IntroducedBy,
			i2016.IntroducedBy FY2016IntroducedBy,
			x.ManagedBy, 
			x.ManagedByFromDate,
			x.PreviousManagedBy,
			i2014.ManagedBy FY2014ManagedBy,
			i2015.ManagedBy FY2015ManagedBy,
			i2016.ManagedBy FY2016ManagedBy,
			x.Handled, 
			x.HandledFromDate,
			x.PreviousHandled,
			i2014.Handled FY2014Handled,
			i2015.Handled FY2015Handled,
			i2016.Handled FY2016Handled,
			x.VIPType,
			x.VIPTypeFromDate,
			x.PreviousVIPType,
			i2014.VIP FY2014VIPType,
			i2015.VIP FY2015VIPType,
			i2016.VIP FY2016VIPType
	INTO	#FY
	FROM	#Previous x
			LEFT JOIN	(SELECT ClientId, OrgId FROM [$(Acquisition)].IntraBet.tblClients WHERE FromDate <= CONVERT(datetime2(3),'2014-12-31') AND ToDate > CONVERT(datetime2(3),'2014-12-31')) c2014 ON (c2014.ClientID = x.ClientID)
			LEFT JOIN	(	SELECT	DISTINCT p.ClientId, FIRST_VALUE(c.OrgId) OVER (PARTITION BY p.ClientId ORDER BY c.FromDate) OrgId -- For 2015, if account was ever TW, that takes precedence, otherwise default to what the OrgId was at fiscal year end. The WHERE clause will ensure that only these 2 possibilities are returned; TW will always be earlier of any multiple records found by FromDate
							FROM	#Previous p
									INNER JOIN [$(Acquisition)].IntraBet.tblClients c ON (c.ClientID = p.ClientID)
							WHERE	c.FromDate <= CONVERT(datetime2(3),'2016-02-29') -- End of migration
									AND c.ToDate > CONVERT(datetime2(3),'2015-01-01') -- Start of year
									AND ((c.FromDate < CONVERT(datetime2(3),'2015-12-30') AND c.ToDate >= CONVERT(datetime2(3),'2015-12-30')) OR c.OrgID = 8)
						) c2015 ON (c2015.ClientId = x.ClientId)
			LEFT JOIN	(	SELECT	DISTINCT p.ClientId, FIRST_VALUE(c.OrgId) OVER (PARTITION BY p.ClientId ORDER BY c.FromDate) OrgId -- As with TW in 2015, for 2016, if account was ever CBT, that takes precedence, otherwise default to what the OrgId was at fiscal year end. The WHERE clause will ensure that only these 2 possibilities are returned; CBT will always be earlier of any multiple records found by FromDate
							FROM	#Previous p
									INNER JOIN [$(Acquisition)].IntraBet.tblClients c ON (c.ClientID = p.ClientID)
							WHERE	c.FromDate <= CONVERT(datetime2(3),'2016-12-28') -- End of year 
									AND c.ToDate > CONVERT(datetime2(3),'2015-12-30') -- Start of year
									AND ((c.FromDate <= CONVERT(datetime2(3),'2016-12-28') AND c.ToDate > CONVERT(datetime2(3),'2016-12-28')) OR c.OrgID = 2) -- get all versions of orgid for each client for the year, if CBT (orgid = 2) then take the earliest using analytic function above, otherwise further limit the selection to whatever the final value for the year was
						) c2016 ON (c2016.ClientID = x.ClientID)
			LEFT JOIN	(SELECT ClientId, IntroducedBy, ISNULL(ManagedBy,0) ManagedBy, Handled, VIP FROM [$(Acquisition)].IntraBet.tblClientInfo WHERE FromDate <= CONVERT(datetime2(3),'2014-12-31') AND ToDate > CONVERT(datetime2(3),'2014-12-31')) i2014 ON (i2014.ClientID = x.ClientID)
			LEFT JOIN	(SELECT ClientId, IntroducedBy, ISNULL(ManagedBy,0) ManagedBy, Handled, VIP FROM [$(Acquisition)].IntraBet.tblClientInfo WHERE FromDate <= CONVERT(datetime2(3),'2015-12-30') AND ToDate > CONVERT(datetime2(3),'2015-12-30')) i2015 ON (i2015.ClientID = x.ClientID)
			LEFT JOIN	(SELECT ClientId, IntroducedBy, ISNULL(ManagedBy,0) ManagedBy, Handled, VIP FROM [$(Acquisition)].IntraBet.tblClientInfo WHERE FromDate <= CONVERT(datetime2(3),'2016-12-28') AND ToDate > CONVERT(datetime2(3),'2016-12-28')) i2016 ON (i2016.ClientID = x.ClientID)
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,'Sp_Structure','Insert Staging','Start', @RowsProcessed = @@ROWCOUNT

	DROP TABLE #Previous

	INSERT	Stage.Structure 
			(	BatchKey, LedgerId, LedgerSource, InPlay, AccountId, 
				OrgId, OrgIdFromDate, PreviousOrgId, FY2014OrgId, FY2015OrgId, FY2016OrgId,
				IntroducerId, Introducer, IntroducerFromDate, PreviousIntroducerId, PreviousIntroducer, FY2014IntroducerId, FY2014Introducer, FY2015IntroducerId, FY2015Introducer, FY2016IntroducerId, FY2016Introducer,
				ManagerId, Manager, CanManageVIP, ManagerFromDate, PreviousManagerId, PreviousManager, PreviousCanManageVIP, FY2014ManagerId, FY2014Manager, FY2014CanManageVIP, FY2015ManagerId, FY2015Manager, FY2015CanManageVIP, FY2016ManagerId, FY2016Manager, FY2016CanManageVIP, 
				HandlerId, Handler, HandlerFromDate, PreviousHandlerId, PreviousHandler, FY2014HandlerId, FY2014Handler, FY2015HandlerId, FY2015Handler, FY2016HandlerId, FY2016Handler,
				VIPType, VIPTypeFromDate, PreviousVIPType, FY2014VIPType, FY2015VIPType, FY2016VIPType,
				FromDate
			)
	SELECT	@BatchKey BatchKey,
			a.AccountID LedgerId,
			'IAA' LedgerSource,
			y.InPlay,
			x.ClientId AccountId, 
			x.OrgId,
			x.OrgIdFromDate,
			x.PreviousOrgId,
			x.FY2014OrgId,
			x.FY2015OrgId,
			x.FY2016OrgId,
			x.IntroducedBy IntroducerId,
			i.IntroducedName Introducer, 
			x.IntroducedByFromDate IntroducerFromDate,
			x.PreviousIntroducedBy PreviousIntroducerId,
			ip.IntroducedName PreviousIntroducer,
			x.FY2014IntroducedBy FY2014IntroducerId,
			i14.IntroducedName FY2014Introducer,
			x.FY2015IntroducedBy FY2015IntroducerId,
			i15.IntroducedName FY2015Introducer,
			x.FY2016IntroducedBy FY2016IntroducerId,
			i16.IntroducedName FY2016Introducer,
			x.ManagedBy ManagerId,
			m.IntroducedName Manager, 
			m.CanManageVIP,
			x.ManagedByFromDate ManagerFromDate,
			x.PreviousManagedBy PreviousManagerId,
			mp.IntroducedName PreviousManager,
			mp.CanManageVIP PreviousCanManageVIP,
			x.FY2014ManagedBy FY2014ManagerId,
			m14.IntroducedName FY2014Manager,
			m14.CanManageVIP FY2014CanManageVIP,
			x.FY2015ManagedBy FY2015ManagerId,
			m15.IntroducedName FY2015Manager,
			m15.CanManageVIP FY2015CanManageVIP,
			x.FY2016ManagedBy FY2016ManagerId,
			m16.IntroducedName FY2016Manager,
			m16.CanManageVIP FY2016CanManageVIP,
			x.Handled HandlerId,
			h.HandlerName Handler, 
			x.HandledFromDate HandlerFromDate,
			x.PreviousHandled PreviousHandlerId,
			hp.HandlerName PreviousHandler,
			x.FY2014Handled FY2014HandlerId,
			h14.HandlerName FY2014Handler,
			x.FY2015Handled FY2015HandlerId,
			h15.HandlerName FY2015Handler,
			x.FY2016Handled FY2016HandlerId,
			h16.HandlerName FY2016Handler,
			x.VIPType,
			x.VIPTypeFromDate,
			x.PreviousVIPType,
			x.FY2014VIPType,
			x.FY2015VIPType,
			x.FY2016VIPType,
			x.FromDate 
	FROM	#FY x
			INNER JOIN [$(Acquisition)].IntraBet.tblAccounts a ON (a.ClientID = x.ClientID AND a.FromDate <= x.FromDate AND a.ToDate > x.FromDate)
			LEFT HASH JOIN [$(Acquisition)].IntraBet.tblIntroducers i ON (i.ClientID = x.IntroducedBy AND i.FromDate <= x.FromDate AND i.ToDate > x.FromDate)
			LEFT HASH JOIN [$(Acquisition)].IntraBet.tblIntroducers ip ON (ip.ClientID = x.PreviousIntroducedBy AND ip.FromDate <= x.IntroducedByFromDate AND ip.ToDate > x.IntroducedByFromDate)
			LEFT HASH JOIN [$(Acquisition)].IntraBet.tblIntroducers i14 ON (i14.ClientID = x.FY2014IntroducedBy AND i14.FromDate <= CONVERT(datetime2(3),'2014-12-31') AND i14.ToDate > CONVERT(datetime2(3),'2014-12-31'))
			LEFT HASH JOIN [$(Acquisition)].IntraBet.tblIntroducers i15 ON (i15.ClientID = x.FY2015IntroducedBy AND i15.FromDate <= CONVERT(datetime2(3),'2015-12-30') AND i15.ToDate > CONVERT(datetime2(3),'2015-12-30'))
			LEFT HASH JOIN [$(Acquisition)].IntraBet.tblIntroducers i16 ON (i16.ClientID = x.FY2016IntroducedBy AND i16.FromDate <= CONVERT(datetime2(3),'2016-12-28') AND i16.ToDate > CONVERT(datetime2(3),'2016-12-28'))
			LEFT HASH JOIN [$(Acquisition)].IntraBet.tblIntroducers m ON (m.ClientID = x.ManagedBy AND m.FromDate <= x.FromDate AND m.ToDate > x.FromDate)
			LEFT HASH JOIN [$(Acquisition)].IntraBet.tblIntroducers mp ON (mp.ClientID = x.PreviousManagedBy AND mp.FromDate <= x.IntroducedByFromDate AND mp.ToDate > x.IntroducedByFromDate)
			LEFT HASH JOIN [$(Acquisition)].IntraBet.tblIntroducers m14 ON (m14.ClientID = x.FY2014ManagedBy AND m14.FromDate <= CONVERT(datetime2(3),'2014-12-31') AND m14.ToDate > CONVERT(datetime2(3),'2014-12-31'))
			LEFT HASH JOIN [$(Acquisition)].IntraBet.tblIntroducers m15 ON (m15.ClientID = x.FY2015ManagedBy AND m15.FromDate <= CONVERT(datetime2(3),'2015-12-30') AND m15.ToDate > CONVERT(datetime2(3),'2015-12-30'))
			LEFT HASH JOIN [$(Acquisition)].IntraBet.tblIntroducers m16 ON (m16.ClientID = x.FY2016ManagedBy AND m16.FromDate <= CONVERT(datetime2(3),'2016-12-28') AND m16.ToDate > CONVERT(datetime2(3),'2016-12-28'))
			LEFT HASH JOIN [$(Acquisition)].IntraBet.tblLUDebtHandlers h ON (h.DebtHandlerID = x.Handled AND h.FromDate <= x.FromDate AND h.ToDate > x.FromDate)
			LEFT HASH JOIN [$(Acquisition)].IntraBet.tblLUDebtHandlers hp ON (hp.DebtHandlerID = x.PreviousHandled AND hp.FromDate <= x.IntroducedByFromDate AND hp.ToDate > x.IntroducedByFromDate)
			LEFT HASH JOIN [$(Acquisition)].IntraBet.tblLUDebtHandlers h14 ON (h14.DebtHandlerID = x.FY2014Handled AND h14.FromDate <= CONVERT(datetime2(3),'2014-12-31') AND h14.ToDate > CONVERT(datetime2(3),'2014-12-31'))
			LEFT HASH JOIN [$(Acquisition)].IntraBet.tblLUDebtHandlers h15 ON (h15.DebtHandlerID = x.FY2015Handled AND h15.FromDate <= CONVERT(datetime2(3),'2015-12-30') AND h15.ToDate > CONVERT(datetime2(3),'2015-12-30'))
			LEFT HASH JOIN [$(Acquisition)].IntraBet.tblLUDebtHandlers h16 ON (h16.DebtHandlerID = x.FY2016Handled AND h16.FromDate <= CONVERT(datetime2(3),'2016-12-28') AND h16.ToDate > CONVERT(datetime2(3),'2016-12-28'))
			CROSS APPLY (SELECT 'Y' InPlay UNION ALL SELECT 'N' InPlay) y
	OPTION (RECOMPILE)

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Structure', NULL, 'Success', @RowsProcessed = @RowCount

	DROP TABLE #FY

END TRY
BEGIN CATCH
	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Structure', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

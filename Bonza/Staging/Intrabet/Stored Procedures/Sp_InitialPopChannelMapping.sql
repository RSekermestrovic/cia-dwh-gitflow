Create Procedure Intrabet.Sp_InitialPopChannelMapping

as

IF not exists (Select [Channel] From [Intrabet].[ChannelMapping])
Begin
Begin transaction
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (1, N'Internet')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (2, N'Mobile')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (3, N'Internet')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (4, N'Kiosk')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (5, N'Internet')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (6, N'Internet')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (11, N'Internet')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (12, N'Mobile')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (14, N'Kiosk')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (15, N'Mobile')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (16, N'Mobile')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (17, N'API')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (18, N'API')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (21, N'Internet')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (22, N'Mobile')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (24, N'Kiosk')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (31, N'Internet')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (50, N'API')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (101, N'Mobile')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (102, N'Mobile')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (105, N'Telephone')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (200, N'API')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (210, N'API')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (301, N'Internet')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (302, N'Internet')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (401, N'Mobile')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (501, N'Internet')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (502, N'Mobile')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (505, N'Telephone')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (512, N'Mobile')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (515, N'Mobile')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (516, N'CMS')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (601, N'Internet')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (602, N'Internet')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (603, N'Mobile')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (604, N'Telephone')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (605, N'Betback')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (611, N'Telephone')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (612, N'Mobile')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (613, N'Internet')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (614, N'Mobile')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (615, N'Mobile')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (616, N'TAB')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (617, N'TAB')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (618, N'Brokering')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (619, N'TAB')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (620, N'TAB')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (621, N'Concierge')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (622, N'Kiosk')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (623, N'Mobile')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (624, N'Mobile')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (801, N'Internet')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (802, N'Mobile')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (805, N'Telephone')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (811, N'Internet')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (812, N'Mobile')
INSERT [Intrabet].[ChannelMapping] ([Channel], [ChannelName]) VALUES (815, N'Telephone')
If @@ERROR = 0
Begin
	Commit transaction
End
Else
Begin
	Rollback transaction
End
End

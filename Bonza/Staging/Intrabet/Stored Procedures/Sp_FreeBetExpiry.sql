﻿CREATE PROC [Intrabet].[Sp_FreeBetExpiry]
(
	@BatchKey	INT,
	@RowsProcessed	int = 0 OUTPUT
)
AS 
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED -- this should be / is a recursive process - hence relaxed isolation level

	DECLARE @RowCount INT;

BEGIN TRY

	EXEC [Control].Sp_Log	@BatchKey,'Sp_FreeBetExpiry','CandidateExpiry','Start'

	BEGIN TRY DROP TABLE #ExpiryCandidates END TRY BEGIN CATCH END CATCH
	BEGIN TRY DROP TABLE #ExpiryMain END TRY BEGIN CATCH END CATCH


	SELECT 	LedgerID, MasterTransactionTimestamp, PromotionId, TransactionId, DATEADD(DD,-35,MasterTransactionTimestamp)	MTTMinus35									-- Get Expiry records with no wallet
	INTO #ExpiryCandidates 
	FROM Stage.[Transaction] 
	WHERE TransactionTypeID = 'BO' 
	and TransactionStatusID = 'EX' 
	and TransactionMethodID = 'BO' 
	AND WalletId = 'B' 
	AND BatchKey=@BATCHKEY 
	GROUP BY LedgerID,MasterTransactionTimestamp,PromotionId,TransactionId
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,'Sp_FreeBetExpiry','CandidateVouchers','Start', @RowsProcessed = @@ROWCOUNT

	SELECT DISTINCT A.LedgerId, A.MasterTransactionTimestamp, A.PromotionId, A.PromotionIDSource													-- Check Credits within the same batch in Staging
	INTO #ExpiryMain
	FROM Stage.[Transaction] A
	INNER JOIN #ExpiryCandidates B ON A.LedgerID=B.LedgerID
	WHERE	A.TransactionTypeID = 'BO' and A.TransactionStatusID = 'CO' and A.TransactionMethodID = 'BO'
	AND		A.BATCHKEY=@BATCHKEY
				
	UNION ALL
				
	SELECT A.LedgerID, T.MasterTransactionTimestamp, P.PromotionId, P.Source
	FROM (SELECT DISTINCT LedgerID FROM #ExpiryCandidates) x
		INNER JOIN [$(Dimensional)].Dimension.Account A 				ON A.LedgerID = x.LedgerId
		INNER JOIN [$(Dimensional)].FACT.[Transaction] T				ON T.AccountKey=A.ACCOUNTKEY																							-- Check Credits if previously loaded into Fact
		INNER JOIN [$(Dimensional)].Dimension.Promotion P 				ON T.PromotionKey=P.PromotionKey 
		INNER JOIN [$(Dimensional)].Dimension.[TransactionDetail] D 	ON T.TransactionDetailKey=D.TransactionDetailKey
	WHERE	D.TransactionDetailId IN ('VCI+','VCI-')
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,'Sp_FreeBetExpiry','UpdateStaging','Start', @RowsProcessed = @@ROWCOUNT

/* AJ: the below update needs to be reviewed later */

	UPDATE Stage
	SET Stage.PromotionId		=	CASE WHEN CONVERT(VARCHAR(16),CONVERT(DATETIME2,Stage.MasterTransactionTimestamp)) = '2014-04-28 13:27' THEN 4952 ELSE COALESCE(X.PromotionId,Y.PromotionId,999999) END,
		Stage.PromotionIDSource =	COALESCE(X.PromotionIdSource,Y.PromotionIdSource,'IBF'),
		Stage.FreeBetID			=	CASE WHEN CONVERT(VARCHAR(16),CONVERT(DATETIME2,Stage.MasterTransactionTimestamp)) = '2014-04-28 13:27' THEN 4952 ELSE COALESCE(X.PromotionId,Y.PromotionId,999999) END,
		Stage.ExistsInDim		=	CASE WHEN X.PromotionId = -1 THEN 16 ELSE 0 END
	FROM Stage.[Transaction] Stage
	INNER JOIN #ExpiryCandidates A on A.TransactionId=STAGE.TransactionId
	LEFT OUTER JOIN
		(SELECT A.LedgerID, A.MasterTransactionTimestamp, TRANSACTIONID, B.PromotionId, B.PromotionIDSource, DENSE_RANK() OVER(PARTITION BY A.LedgerID, A.MasterTransactionTimestamp ORDER BY B.MasterTransactionTimestamp DESC) AS RNK 
			FROM #ExpiryCandidates A
			INNER JOIN #ExpiryMain B ON A.LedgerID=B.LedgerID AND B.MasterTransactionTimestamp<=A.MTTMinus35
		) X ON A.LedgerID = X.LedgerID AND A.TRANSACTIONID =X.TRANSACTIONID
	LEFT OUTER JOIN 
		(SELECT A.LedgerID, A.MasterTransactionTimestamp, TRANSACTIONID, B.PromotionId, B.PromotionIDSource, DENSE_RANK() OVER(PARTITION BY A.LedgerID, A.MasterTransactionTimestamp ORDER BY B.MasterTransactionTimestamp DESC) AS RNK 
			FROM #ExpiryCandidates A
			INNER JOIN #ExpiryMain B ON A.LedgerID=B.LedgerID AND B.MasterTransactionTimestamp<=A.MasterTransactionTimestamp
		) Y ON A.LedgerID=Y.LedgerID AND A.TRANSACTIONID=Y.TRANSACTIONID
	WHERE ISNULL(X.RNK,1) = 1 
	AND ISNULL(Y.RNK,1) = 1
	AND STAGE.BatchKey = @BATCHKEY
	OPTION (RECOMPILE, HASH JOIN);

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey, 'Sp_FreeBetExpiry', NULL, 'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_FreeBetExpiry', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
END

﻿CREATE PROC [Intrabet].[Sp_Note]
(
	@FromDate	datetime2(3),
	@ToDate		datetime2(3),
	@BatchKey	int
)
WITH RECOMPILE
AS 
BEGIN
BEGIN TRY

	SET NOCOUNT ON;

	BEGIN TRANSACTION Notes;

	EXEC [Control].Sp_Log	@BatchKey,'Sp_Note','InsertIntoStaging','Start';

	INSERT INTO [Stage].[Note] (NoteId,NoteSource, Notes,BatchKey,FromDate)
	SELECT	TransactionId, 'ITN', Notes,@BatchKey , FromDate
	FROM	(	SELECT t.TransactionId, t.FromDate, t.Notes, MAX(t.FromDate) OVER (PARTITION BY t.TransactionId) LastFromDate
				FROM  [$(Acquisition)].Intrabet.tblTransactions t WITH (NOLOCK, FORCESEEK([NI_tblTransactions_FD(A)](FromDate))) 
						LEFT JOIN [$(Acquisition)].Intrabet.tblTransactions t1 WITH (NOLOCK, FORCESEEK([CI_tblTransactions(A)](TransactionId))) ON (t1.TransactionId = t.TransactionId AND t1.ToDate = t.Fromdate AND t1.Notes <> t.Notes)
				WHERE	t.FromDate >= @FromDate and t.FromDate < @ToDate AND t.ToDate > @FromDate
						AND t.Notes <> '' AND t.Notes IS NOT NULL
						AND t1.TransactionId IS NULL
			) x
	WHERE	FromDate = LastFromdate
	OPTION (RECOMPILE);

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Note', NULL, 'Success', @RowsProcessed = @@ROWCOUNT;

	COMMIT TRANSACTION Notes;

END TRY
BEGIN CATCH

	ROLLBACK TRANSACTION Notes;
	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Note', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

END
﻿CREATE PROC [Intrabet].[Sp_Deposits]
(
	@FromDate		DATETIME2(3),
	@ToDate			DATETIME2(3),
	@BatchKey		INT,
	@RowsProcessed	int = 0 OUTPUT
)
AS
BEGIN
 
	SET TRANSACTION ISOLATION LEVEL SNAPSHOT

	DECLARE @RowCount INT;

BEGIN TRY 

-- APPROVED DEPOSITS IN SOURCE

	EXEC [Control].Sp_Log	@BatchKey,'Sp_Deposits','DepositsApproved','Start'
	
	BEGIN TRY DROP TABLE #depositapproved END TRY BEGIN CATCH END CATCH	-- Drop temp table if exists
	
	SELECT T.TransactionID,T.Notes,T.TransactionDate,T.Amount,T.FromDate,T.Channel,T.ToDate,T.ClientID,T.TransactionCode ,M.BalanceTypeID,M.TransactionDetailId,M.TransactionTypeID,M.TransactionStatusID,
		M.TransactionMethodID,CASE WHEN RIGHT(M.TransactionDetailId,1) = '+' THEN 'CR' ELSE 'DR' END AS TransactionDirection,C.Username,A.AccountId	-- Changed from BalanceName,TrnactionMethod 6Mar15
	INTO #depositapproved
	FROM [$(Acquisition)].Intrabet.tblTransactions T	 
	INNER JOIN [$(Acquisition)].Intrabet.tblAccounts A	 			ON (A.ClientID = T.ClientID and A.AccountNumber = T.AccountNumber and A.ToDate = CONVERT(datetime2(3),'9999-12-31'))
	INNER JOIN Intrabet.TransactionTypeMapping M	 		ON (T.TransactionCode=M.TransactionCode AND CASE WHEN T.AMOUNT >=0 THEN 1 ELSE -1 END =M.Direction AND M.IsDeleted=0) 
	INNER JOIN [$(Acquisition)].Intrabet.tblClients C 	 			ON (C.ClientID = T.ClientID and C.ToDate = CONVERT(datetime2(3),'9999-12-31'))
	WHERE	M.transactiontypeID = 'DP'						-- Changed from TransactionType 6Mar15
	AND		T.FromDate > @FromDate AND T.FromDate <= @ToDate
	AND		T.ToDate >= @FromDate
	AND		M.[ENABLED] = 1
	OPTION (RECOMPILE);
	
-- DELETED DEPOSITS IN SOURCE
	
	EXEC [Control].Sp_Log	@BatchKey,'Sp_Deposits','DepositsDeleted','Start', @RowsProcessed = @@ROWCOUNT

	BEGIN TRY DROP TABLE #depositdeleted END TRY BEGIN CATCH END CATCH -- Drop temp table if exists
	
	SELECT T.TransactionID,T.Notes,T.TransactionDate,T.Amount,T.FromDate,T.Channel,T.ToDate,T.ClientID,T.TransactionCode,M.BalanceTypeID,M.TransactionDetailId,M.TransactionTypeID,M.TransactionStatusID,
		M.TransactionMethodID,CASE WHEN RIGHT(M.TransactionDetailId,1) = '+' THEN 'CR' ELSE 'DR' END AS TransactionDirection,C.Username,A.AccountId	-- Changed from BalanceName,TrnactionMethod 6Mar15
	INTO #depositdeleted
	FROM [$(Acquisition)].Intrabet.tblTransactions T  
	INNER JOIN [$(Acquisition)].Intrabet.tblAccounts A	 			ON (A.ClientID = T.ClientID and A.AccountNumber = T.AccountNumber and A.ToDate = CONVERT(datetime2(3),'9999-12-31'))
	INNER JOIN Intrabet.TransactionTypeMapping M	 		ON (T.TransactionCode=M.TransactionCode AND CASE WHEN T.AMOUNT >=0 THEN 1 ELSE -1 END =M.Direction AND M.IsDeleted=1) 
	INNER JOIN [$(Acquisition)].Intrabet.tblClients C	 			ON (C.ClientID = T.ClientID and C.ToDate = CONVERT(datetime2(3),'9999-12-31'))
	WHERE	M.transactiontypeID = 'DP'						-- Changed from TransactionType 6Mar15
	AND		T.ToDate > @FromDate AND T.ToDate <= @ToDate
	AND		T.ToDate < '9999-12-30'
	AND		M.[ENABLED]=1
	OPTION (RECOMPILE)

-------------- INSERT INTO STAGING -----------------------------

	EXEC [Control].Sp_Log	@BatchKey,'Sp_Deposits','InsertIntoStaging','Start', @RowsProcessed = @@ROWCOUNT

	INSERT INTO Stage.[TRANSACTION] (BatchKey,TransactionId,TransactionIdSource,BalanceTypeID,TransactionTypeID,TransactionStatusID,TransactionMethodID,TransactionDirection,BetGroupID,BetGroupIDSource,BetId,BetIdSource,LegId,LegIdSource,FreeBetID,	-- Changed from BalanceName 6Mar15
	ExternalID,External1,External2,UserID,UserIDSource,InstrumentID,InstrumentIDSource,LedgerId,LedgerSource,WalletId,PromotionId,PromotionIDSource,MasterTransactionTimestamp,MasterDayText,MasterTimeText,Channel,ActionChannel,
	CampaignId,TransactedAmount,BetGroupType,BetTypeName,BetSubTypeName,LegBetTypeName,GroupingType,PriceTypeId,PriceTypeSource,ClassId,ClassIdSource,EventID,EventIDSource,MarketID,MarketIdSource,LegNumber,NumberOfLegs,ExistsInDim,Conditional,
	MappingType,MappingId,FromDate,DateTimeUpdated,RequestID,WithdrawID,InPlay,ClickToCall,CashoutType,IsDoubleDown,IsDoubledDown)

	SELECT 
		@BatchKey AS BatchKey,
		T.TransactionID AS TransactionID,
		'IBT' AS TransactionIDSource,
		ISNULL(T.BalanceTypeID,'UK') AS BalanceTypeID,							-- Changed from BalanceName 6Mar15
		ISNULL(T.TransactionTypeID,'UK') AS TransactionTypeID,
		ISNULL(T.TransactionStatusID,'UK') AS TransactionStatusID,
		ISNULL(T.TransactionMethodID,'UK') AS TransactionMethodID,
		ISNULL(T.TransactionDirection,'UN') AS TransactionDirection,
		T.TransactionID AS BetGroupID,
		'IBT' AS BetGroupIDSource,
		T.TransactionID AS BetID,
		'IBT' AS BetIDSource,
		T.TransactionID AS LegID,
		'IBT' AS LegIDSource,
		0 AS FreeBetID,
		CASE  T.TransactionMethodID										-- Changed from TransactionMethod 6Mar15
			WHEN 'MB' THEN CONVERT(VARCHAR(32),B.mb_transaction_id)
			WHEN 'BY' THEN T.Notes
			WHEN 'CC' THEN CC.pnref
			WHEN 'PP' THEN P.PPTransactionID
			WHEN 'PO' THEN T.Notes
			ELSE NULL 
			END AS ExternalID,
		ISNULL(	CASE  T.TransactionMethodID	
				WHEN 'Moneybookers' THEN b.pay_from_email
				WHEN 'Credit Card' THEN cc.authcode
				ELSE NULL	
				END,'NULL') AS External1,
		ISNULL(	Case When Att.[Description] like '% CBA%' Then 'CBA'
				 When Att.[Description] like '% NAB%' Then 'NAB'
				 When (Att.[Description] like '% WBC%' or Att.[Description] like '% Westpac%')Then 'WBC'
				 Else 'Unknown'
			End,'NULL')  AS External2,  --LN: Bank name to be transferred to External2 in Contract dimension for deposits
		ISNULL(tl.UserCode,-4) as UserID,
		'IUU' as UserIDSource,
		ISNULL(CASE T.TransactionMethodID	
					WHEN 'BY' THEN TRY_CONVERT(VARCHAR(50),CBp.BPayID)
					WHEN 'EF' THEN TRY_CONVERT(VARCHAR(50),CBK.BankID )
					WHEN 'MB' THEN TRY_CONVERT(VARCHAR(50),b.pay_from_email)
					WHEN 'PP' THEN TRY_CONVERT(VARCHAR(50),Pg.PayerID)
					ELSE TRY_CONVERT(VARCHAR(50),0)
					END, TRY_CONVERT(VARCHAR(50),-1)) AS InstrumentID,
		CASE T.TransactionMethodID	
					WHEN 'BY' THEN 'BPY'
					WHEN 'EF' THEN 'EFT'
					WHEN 'MB' THEN 'MBK'
					WHEN 'PP' THEN 'PPL'
					ELSE 'N/A' 
					END AS InstrumentIDSource,
		T.AccountId LedgerID,
		CASE WHEN T.AccountId=0 THEN 'N/A' ELSE 'IAA' END AS LedgerSource,
		'C' AS WalletID,
		0 AS PromotionID,
		'N/A' AS PromotionIDSource,
		CASE WHEN CONVERT(TIME,T.FromDate) ='00:00:00.000' THEN COALESCE(L.TransactionDate,T.TransactionDate) ELSE ISNULL(LT.CorrectedDate,T.FromDate) END AS  MasterTransactionTimeStamp,	-- FromDate is most accurate except when FromDate is midnight.
		CASE WHEN CONVERT(TIME,T.FromDate) ='00:00:00.000' THEN CONVERT(VARCHAR(11),CONVERT(DATE,COALESCE(L.TransactionDate,T.TransactionDate))) ELSE CONVERT(VARCHAR(11),CONVERT(DATE,ISNULL(LT.CorrectedDate,T.FromDate))) END AS MasterDayText,
		CASE WHEN CONVERT(TIME,T.FromDate) ='00:00:00.000' THEN CONVERT(VARCHAR(5),CONVERT(TIME,COALESCE(L.TransactionDate,T.TransactionDate))) ELSE CONVERT(VARCHAR(5),CONVERT(TIME,ISNULL(LT.CorrectedDate,T.FromDate))) END  AS MasterTimeText,
		--T.TransactionDate AS MasterEffectiveDate,
		--ISNULL(ch.ChannelName,'Internet') AS ChannelName,
		ISNULL(T.Channel,-1) Channel,
		ISNULL(T.Channel,-1) ActionChannel,
		0 AS CampaignID,
		ABS(T.Amount)*(CASE WHEN RIGHT(T.TransactionDetailId,1)='+' THEN 1 ELSE -1 END) AS TransactedAmount,
		'Straight' AS BetGroupType,
		'N/A' AS BetTypeName,
		'N/A' AS BetSubTypeName,
		'N/A' AS LegBetTypeName,
		'Straight' AS GroupingType,
		0 AS PriceTypeID, /* AJ - PriceType Change */
		'N/A' AS PriceTypeSource, /* AJ - PriceType Change */
		0 AS ClassID,
		'IBT' AS ClassIDSource,
		0 AS EventID,
		'N/A' AS EventIDSource,
		0 AS MarketID, /* AJ - MarketId Change */
		'N/A' AS MarketIDSource, /* AJ - MarketId Change */
		1 AS LegNumber,
		1 AS NumberOfLegs,
		0 AS ExistsInDim,
		0 AS Conditional,
		1 AS MappingType,
		1 AS MappingID,
		T.FromDate AS FromDate,
		0 AS DateTimeUpdated,
		NULL AS RequestID,
		NULL AS WithdrawID,
		'N/A' InPlay,
		'N/A' ClickToCall,
		0 CashoutType,
		0 IsDoubleDown,
		0 IsDoubledDown
	FROM #depositapproved T	 
	--LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblTransactions T1					ON (T.TransactionID=T1.TransactionID AND T.FromDate=T1.ToDate)	-- Check if current record is the first version or updated version
	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblMoneybookersdepositresponse B		ON (B.intrabettransactionid = T.transactionid and B.ToDate = CONVERT(datetime2(3),'9999-12-31'))
	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblTransactionLogs	L					ON (L.TransactionID = T.TransactionID and L.ToDate = CONVERT(datetime2(3),'9999-12-31'))
	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblCreditCardTransactions CC			ON (CC.transactionid = T.transactionid AND CC.ClientID=T.ClientID and CC.ToDate = CONVERT(datetime2(3),'9999-12-31'))
	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblPayPalDEC P							ON (P.transactionid = T.transactionid and P.ToDate = CONVERT(datetime2(3),'9999-12-31'))
	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblPayPalGEC Pg						ON (P.Token=Pg.Token  and Pg.ToDate = CONVERT(datetime2(3),'9999-12-31'))
	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblClientWithdrawRequest R				ON (R.RequestID = TRY_CONVERT(int,t.Notes) AND ISNULL(TRY_CONVERT(int,t.Notes),0) <> 0 and R.ToDate = CONVERT(datetime2(3),'9999-12-31'))
	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblClientBpaydetails Bp				ON (Bp.BPayID = R.WithdrawID AND r.WithdrawID <> -1 and Bp.ToDate = CONVERT(datetime2(3),'9999-12-31'))
	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblClientBankdetails K					ON (K.BankID = R.WithdrawID AND r.WithdrawID <> -1 and K.ToDate = CONVERT(datetime2(3),'9999-12-31'))
	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblClientWithdrawRequest CWR			ON (CWR.RequestID = TRY_CONVERT(int,t.Notes) AND ISNULL(TRY_CONVERT(int,t.Notes),0) <> 0 and CWR.ToDate = CONVERT(datetime2(3),'9999-12-31'))
	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblClientBpaydetails CBp				ON (CBp.BPayID = R.WithdrawID  and CBp.ToDate = CONVERT(datetime2(3),'9999-12-31'))
	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblClientBankdetails CBK				ON (CBK.BankID = R.WithdrawID and CBK.ToDate = CONVERT(datetime2(3),'9999-12-31'))
	--LEFT OUTER JOIN Intrabet.ChannelMapping CH							ON (CH.Channel = T.Channel)
	LEFT OUTER JOIN [$(Acquisition)].IntraBet.tblTransactionLogs tl					ON T.TransactionID=tl.TransactionID and T.FromDate=tl.FromDate
	LEFT OUTER JOIN [$(Acquisition)].IntraBet.Latency LT								ON (T.FromDate=LT.OriginalDate)	-- Join to Latency Table
	LEFT OUTER JOIN [$(Acquisition)].IntraBet.tblLUTransactionTypes Att  on (Att.TransactionCode=t.TransactionCode and Att.todate>='9999-12-30 00:00:00.000')
	WHERE	T.BalanceTypeID IS NOT NULL						-- Changed from BalanceName 6Mar15

UNION ALL

	SELECT 
		@BatchKey AS BatchKey,
		T.TransactionID AS TransactionID,
		'IBT' AS TransactionIDSource,
		ISNULL(T.BalanceTypeID,'UK') AS BalanceTypeID,			-- Changed from BalanceName 6Mar15
		ISNULL(T.TransactionTypeID,'UK') AS TransactionTypeID,
		ISNULL(T.TransactionStatusID,'UK') AS TransactionStatusID,
		ISNULL(T.TransactionMethodID,'UK') AS TransactionMethodID,
		ISNULL(T.TransactionDirection,'UN') AS TransactionDirection,
		T.TransactionID AS BetGroupID,
		'IBT' AS BetGroupIDSource,
		T.TransactionID AS BetID,
		'IBT' AS BetIDSource,
		T.TransactionID AS LegID,
		'IBT' AS LegIDSource,
		0 AS FreeBetID,
		CASE  T.TransactionMethodID						-- Changed from TransactionMethod 6Mar15
			WHEN 'MB' THEN CONVERT(VARCHAR(32),B.mb_transaction_id)
			WHEN 'BY' THEN T.Notes
			WHEN 'CC' THEN CC.pnref
			WHEN 'PP' THEN P.PPTransactionID
			WHEN 'PO' THEN T.Notes
			ELSE NULL 
			END AS ExternalID,
		ISNULL(	CASE  T.TransactionMethodID	
				WHEN 'Moneybookers' THEN b.pay_from_email
				WHEN 'Credit Card' THEN cc.authcode
				ELSE NULL	
				END,'NULL') AS External1,
		ISNULL(	Case When Att.[Description] like '% CBA%' Then 'CBA'
				 When Att.[Description] like '% NAB%' Then 'NAB'
				 When (Att.[Description] like '% WBC%' or Att.[Description] like '% Westpac%')Then 'WBC'
				 Else 'Unknown'
			End,'NULL')  AS External2, --LN: Bank name to be transferred to External2 in Contract dimension for deposits
		ISNULL(d.UserCode,ISNULL(tl.UserCode,-4)) as UserID,
		'IUU' as UserIDSource,
		ISNULL(CASE T.TransactionMethodID	
					WHEN 'BY' THEN TRY_CONVERT(VARCHAR(50),CBp.BPayID)
					WHEN 'EF' THEN TRY_CONVERT(VARCHAR(50),CBK.BankID )
					WHEN 'MB' THEN TRY_CONVERT(VARCHAR(50),b.pay_from_email)
					WHEN 'PP' THEN TRY_CONVERT(VARCHAR(50),Pg.PayerID)
					ELSE  TRY_CONVERT(VARCHAR(50),0)
					END,  TRY_CONVERT(VARCHAR(50),-1)) AS InstrumentID,
		CASE T.TransactionMethodID	
					WHEN 'BY' THEN 'BPY'
					WHEN 'EF' THEN 'EFT'
					WHEN 'MB' THEN 'MBK'
					WHEN 'PP' THEN 'PPL'
					ELSE 'N/A' 
					END AS InstrumentIDSource,
		T.AccountId LedgerID,
		CASE WHEN T.AccountId=0 THEN 'N/A' ELSE 'IAA' END AS LedgerSource,
		'C' AS WalletID,
		0 AS PromotionID,
		'N/A' AS PromotionIDSource,
		COALESCE(LT.CorrectedDate,T.ToDate,T.TransactionDate) AS  MasterTransactionTimeStamp,										-- ToDate is the best Time record of then the transaction was deleted
		CONVERT(VARCHAR(11),CONVERT(DATE,COALESCE(LT.CorrectedDate,T.ToDate,T.TransactionDate))) AS MasterDayText,
		CONVERT(VARCHAR(5),CONVERT(TIME,COALESCE(LT.CorrectedDate,T.ToDate,T.TransactionDate))) AS MasterTimeText,
		--ISNULL(ch.ChannelName,'Internet') AS ChannelName,
		ISNULL(T.Channel,-1) Channel,
		ISNULL(T.Channel,-1) ActionChannel,
		0 AS CampaignID,
		ABS(T.Amount)*(CASE WHEN RIGHT(T.TransactionDetailId,1)='+' THEN 1 ELSE -1 END) AS TransactedAmount,
		'Straight' AS BetGroupType,
		'N/A' AS BetTypeName,
		'N/A' AS BetSubTypeName,
		'N/A' AS LegBetTypeName,
		'Straight' AS GroupingType,
		0 AS PriceTypeID, /* AJ - PriceType Change */
		'N/A' AS PriceTypeSource, /* AJ - PriceType Change */
		0 AS ClassID,
		'IBT' AS ClassIDSource,
		0 AS EventID,
		'N/A' AS EventIDSource,
		0 AS MarketID, /* AJ - MarketId Change */
		'N/A' AS MarketIDSource, /* AJ - MarketId Change */
		1 AS LegNumber,
		1 AS NumberOfLegs,
		0 AS ExistsInDim,
		0 AS Conditional,
		1 AS MappingType,
		1 AS MappingID, -- 1 Withdrawal or Deposit -- Not betting transaction
		T.ToDate AS FromDate, -- ToDate of the record is the actual event Date time, which the recursive will later apply on the TransactionDateTime
		0 AS DateTimeUpdated,
		NULL AS RequestID,
		NULL AS WithdrawID,
		'N/A' InPlay,
		'N/A' ClickToCall,
		0 CashoutType,
		0 IsDoubleDown,
		0 IsDoubledDown
	FROM #depositdeleted T	
	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblTransactions T1						ON (T.TransactionID=T1.TransactionID AND T.ToDate=T1.FromDate)	-- Check if this is the last record of the Transaction. Other wise its just an update.
	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblMoneybookersdepositresponse B		ON (B.intrabettransactionid = T.transactionid and B.ToDate = CONVERT(datetime2(3),'9999-12-31'))
	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblTransactionLogs	L 					ON (L.TransactionID = T.TransactionID and L.ToDate = CONVERT(datetime2(3),'9999-12-31'))
	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblCreditCardTransactions CC			ON (CC.transactionid = T.transactionid AND CC.ClientID=T.ClientID and CC.ToDate = CONVERT(datetime2(3),'9999-12-31'))
	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblPayPalDEC P							ON (P.transactionid = T.transactionid and P.ToDate = CONVERT(datetime2(3),'9999-12-31'))
	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblPayPalGEC Pg						ON (P.Token=Pg.Token  and Pg.ToDate = CONVERT(datetime2(3),'9999-12-31'))
	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblClientWithdrawRequest R				ON (R.RequestID = TRY_CONVERT(int,t.Notes) AND ISNULL(TRY_CONVERT(int,t.Notes),0) <> 0 and R.ToDate = CONVERT(datetime2(3),'9999-12-31'))
	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblClientBpaydetails  Bp				ON (Bp.BPayID = R.WithdrawID AND r.WithdrawID <> -1 and Bp.ToDate = CONVERT(datetime2(3),'9999-12-31'))
	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblClientBankdetails K					ON (K.BankID = R.WithdrawID AND r.WithdrawID <> -1 and K.ToDate = CONVERT(datetime2(3),'9999-12-31'))
	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblClientWithdrawRequest CWR			ON (CWR.RequestID = TRY_CONVERT(int,t.Notes) AND ISNULL(TRY_CONVERT(int,t.Notes),0) <> 0 and CWR.ToDate = CONVERT(datetime2(3),'9999-12-31'))
	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblClientBpaydetails CBp				ON (CBp.BPayID = CWR.WithdrawID  and CBp.ToDate = CONVERT(datetime2(3),'9999-12-31'))
	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblClientBankdetails CBK				ON (CBK.BankID = R.WithdrawID and CBK.ToDate = CONVERT(datetime2(3),'9999-12-31'))			
	--LEFT OUTER JOIN Intrabet.ChannelMapping CH 							ON (CH.Channel = T.Channel)
	LEFT OUTER JOIN [$(Acquisition)].IntraBet.tblDeletedTransactions d				ON t.TransactionID=D.TransactionID and T.ToDate=d.FromDate
	LEFT OUTER JOIN [$(Acquisition)].IntraBet.tblTransactionLogs tl					ON T.TransactionID=tl.TransactionID and T.ToDate=tl.FromDate
	LEFT OUTER JOIN [$(Acquisition)].IntraBet.Latency LT								ON (T.ToDate=LT.OriginalDate)	-- Join to Latency Table
	LEFT OUTER JOIN [$(Acquisition)].IntraBet.tblLUTransactionTypes Att  on (Att.TransactionCode=t.TransactionCode and Att.todate>='9999-12-30 00:00:00.000')
	WHERE	T.BalanceTypeID IS NOT NULL
	AND		T1.TransactionID IS NULL
	OPTION (RECOMPILE)

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Deposits', NULL, 'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Deposits', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
END
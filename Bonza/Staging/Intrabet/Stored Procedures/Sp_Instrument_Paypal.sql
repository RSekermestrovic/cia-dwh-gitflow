CREATE PROC [Intrabet].[Sp_Instrument_Paypal]
(
	@BatchKey	int,
	@FromDate	datetime2(3),
	@ToDate		datetime2(3),
	@RowsProcessed	int = 0 OUTPUT
)
AS
BEGIN
	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
	DECLARE @Reload varchar(6) = CASE @FromDate WHEN '1900-01-01' THEN 'RELOAD' ELSE NULL END
	DECLARE @RowCount int
	DECLARE @i int
	
	SET TRANSACTION ISOLATION LEVEL SNAPSHOT;

BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------ Initialize Temp Tables ---------------------
	IF OBJECT_ID('TempDB..#Candidates','U') IS NOT NULL BEGIN DROP TABLE #Candidates END;

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Get Paypals','Start', @Reload

	BEGIN TRANSACTION Instrument;
	
	SELECT ID,trnType,MAX(TimestampPPL) TimestampPPL,MAX(fromDate) fromDate  
	INTO #Candidates
	FROM (

			----------- tblPaypalGEc --------------------------
			SELECT	ID,trnType,FromDate,TimeStamp as TimestampPPL
			FROM (	
					SELECT PayerID AS ID,'PPL' trnType,fromDate,TimeStamp 
					FROM [$(Acquisition)].Intrabet.tblPaypalGEc pp
					WHERE FromDate > @FromDate AND FromDate <= @ToDate AND ToDate > @FromDate AND PayerID is not null and PayerID <>'n/a'		
			UNION ALL
					SELECT PayerID AS ID,'PPL' trnType,ToDate ,TimeStamp
					FROM [$(Acquisition)].Intrabet.tblPaypalGEc 
					WHERE	ToDate > @FromDate AND ToDate <= @ToDate AND PayerID is not null and PayerID <>'n/a'
			) x
			GROUP BY ID,trnType,FromDate,TimeStamp
			HAVING COUNT(*) <> 2

		 ) x
	GROUP BY ID,trnType
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'InsertIntoStaging','Start', @Reload, @RowsProcessed = @@ROWCOUNT

	INSERT	into Stage.Instrument (BatchKey,InstrumentId,Source,AccountNumber,SafeAccountNumber,AccountName,BSB,ExpiryDate,Verified,VerifyAmount,InstrumentType,ProviderName,FromDate)
	SELECT	
			@BatchKey																			AS BatchKey, 
			C.id																				AS instrumentID,
			ISNULL(C.trnType, 'Unknown')														AS Source, 
			ISNULL(Pg.Token,'Unknown')															AS AccountNumber,
			ISNULL(Pg.Token,'Unknown')															AS SafeAccountNumber,
			ISNULL(Pg.Payer,'Unknown')															AS AccountName,	 		
			'N/A'																				AS BSB,
			NULL																				AS ExpiryDate,
			Case When pg.Payerstatus='verified' 
			     Then 'Yes'
				 Else 'No' End																	AS Verified,
			NULL																				AS VerifiedAmount,
			'PayPal'																			AS InstrumentType,
			'N/A'																				AS ProviderName,
			c.FromDate																			AS FromDate		
	FROM #Candidates C  with (nolock)
	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblPayPalGEC Pg WITH (NOLOCK) ON 
					C.ID COLLATE SQL_Latin1_General_CP1_CI_AS=Cast(Pg.PayerID  as varchar) 
					and c.TimestampPPL=Pg.[Timestamp]
					AND Pg.FromDate <= c.FromDate AND Pg.ToDate > C.FromDate

	OPTION (RECOMPILE, FORCE ORDER, LOOP JOIN)

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount
								
	COMMIT TRANSACTION Instrument;

	EXEC [Control].Sp_Log @BatchKey, @Me, NULL, 'Success', @RowsProcessed = @RowsProcessed

	UPDATE [Control].[ConfigurationItems] Set Value = 'N' Where ConfigurationItem = 'Intrabet.Sp_Instrument_Paypal'

END TRY
BEGIN CATCH

	ROLLBACK TRANSACTION Instrument;
	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Instrument_Paypal', NULL, 'Failed', @ErrorMessage;
	RAISERROR(@ErrorMessage,16,1);
	THROW;

END CATCH
																					   																					
END

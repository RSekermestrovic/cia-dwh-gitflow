﻿Create Procedure [Intrabet].[Sp_Withdrawals]
(
	@FromDate	DATETIME2(3),
	@ToDate		DATETIME2(3),
	@BatchKey	INT,
	@RowsProcessed	int = 0 OUTPUT
)
AS 
BEGIN

	SET TRANSACTION ISOLATION LEVEL SNAPSHOT

	DECLARE @RowCount INT;

BEGIN TRY
	
	-- APPROVED WITHDRAWALS IN SOURCE	

	SET ANSI_NULLS ON;
	SET QUOTED_IDENTIFIER ON;
	SET NOCOUNT ON;

	EXEC [Control].Sp_Log	@BatchKey,'Sp_Withdrawals','WithdrawalsApproved','Start'

	BEGIN TRY	DROP TABLE #withdrawalapproved	END TRY	BEGIN CATCH	END CATCH

	SELECT
	T.TransactionID,T.TransactionCode,T.Notes,T.TransactionDate,T.Amount,T.FromDate,T.Channel,T.ToDate,T.ClientID,M.BalanceTypeID,M.IsDeleted,M.TransactionDetailId,M.TransactionTypeID,M.TransactionStatusID,
	M.TransactionMethodID,CASE WHEN RIGHT(M.TransactionDetailId,1) = '+' THEN 'CR' ELSE 'DR' END AS TransactionDirection,C.Username,A.AccountId
	INTO #withdrawalapproved
	FROM [$(Acquisition)].Intrabet.tblTransactions T		     
	INNER JOIN [$(Acquisition)].Intrabet.tblAccounts A		  ON (A.ClientID = T.ClientID and A.AccountNumber = T.AccountNumber and A.ToDate = CONVERT(datetime2(3),'9999-12-31') AND	T.FromDate > @FromDate AND T.FromDate <= @ToDate)
	INNER JOIN Intrabet.TransactionTypeMapping M   ON (T.TransactionCode=M.TransactionCode AND CASE WHEN T.AMOUNT >=0 THEN 1 ELSE -1 END =M.Direction AND M.IsDeleted=0 AND M.transactiontypeID = 'WI' AND M.[ENABLED] = 1) 
	INNER JOIN [$(Acquisition)].Intrabet.tblClients C		  ON (C.ClientID = T.ClientID and C.ToDate = CONVERT(datetime2(3),'9999-12-31'))	
	WHERE T.ToDate >= @FromDate
	OPTION (RECOMPILE);

	-- CHECK FOR ISOLATED Completion of Withdrawals, which do not have requests in acquisition (for eg before the CDC was swithched on requests would be overwritten by completions)
	-- Delete request transactions created for (502,501,7,26,28) except those non-isolated transactions obtained from subquery below:

	EXEC [Control].Sp_Log	@BatchKey,'Sp_Withdrawals','WithdrawalsApprovedAdjustment','Start', @RowsProcessed = @@ROWCOUNT

	DELETE A
	FROM #withdrawalapproved A
	WHERE TRANSACTIONID IN (	SELECT A.TransactionID			--- Check if a previous transaction is present in Source
								FROM #withdrawalapproved A 
								INNER JOIN [$(Acquisition)].IntraBet.tblTransactions B  ON A.TRANSACTIONID=B.TRANSACTIONID AND A.FromDate=B.ToDate
								WHERE A.TransactionCode IN (502,501,7,26,28,31)	-- Added 31 for Reversal of Withdrawals using WebSite
								
								UNION	

								SELECT A.TransactionID			--- Check if a previous transaction is present in the same batch
								FROM #withdrawalapproved A 
								INNER JOIN #withdrawalapproved B  ON A.TRANSACTIONID=B.TRANSACTIONID AND A.FromDate=B.ToDate
								WHERE A.TransactionCode IN (502,501,7,26,28,31) -- Added 31 for Reversal of Withdrawals using WebSite
							)
	AND TransactionCode IN (502,501,7,26,28,31)
	AND (TransactionDetailId LIKE 'WR%' OR TransactionDetailId LIKE 'WD%') AND IsDeleted=0
	OPTION (RECOMPILE)

	-- DELETED WITHDRAWALS IN SOURCE	

	BEGIN TRY	DROP TABLE #withdrawaldeleted	END TRY	BEGIN CATCH	END CATCH

	EXEC [Control].Sp_Log	@BatchKey,'Sp_Withdrawals','WithdrawalsDeleted','Start', @RowsProcessed = @@ROWCOUNT

	SELECT
	T.TransactionID,T.TransactionCode,T.Notes,T.TransactionDate,T.Amount,T.FromDate,T.Channel,T.ToDate,T.ClientID,M.BalanceTypeID,M.TransactionDetailId,M.TransactionTypeID,M.TransactionStatusID,
	M.TransactionMethodID,CASE WHEN RIGHT(M.TransactionDetailId,1) = '+' THEN 'CR' ELSE 'DR' END AS TransactionDirection,C.Username,A.AccountId
	INTO #withdrawaldeleted
	FROM [$(Acquisition)].Intrabet.tblTransactions T		     
	INNER JOIN [$(Acquisition)].Intrabet.tblAccounts A		  ON (A.ClientID = T.ClientID and A.AccountNumber = T.AccountNumber and A.ToDate = CONVERT(datetime2(3),'9999-12-31'))
	INNER JOIN Intrabet.TransactionTypeMapping M	  ON (T.TransactionCode=M.TransactionCode AND CASE WHEN T.AMOUNT >=0 THEN 1 ELSE -1 END =M.Direction AND M.IsDeleted=1) 
	INNER JOIN [$(Acquisition)].Intrabet.tblClients C		  ON (C.ClientID = T.ClientID and C.ToDate = CONVERT(datetime2(3),'9999-12-31'))
	WHERE	M.transactiontypeID = 'WI'
	AND		T.ToDate > @FromDate AND T.ToDate <= @ToDate
	AND		T.ToDate<'9999-12-30'
	AND		M.[ENABLED]=1
	OPTION (RECOMPILE)

----------------------------- INSERT INTO STAGING ---------------------------

	EXEC [Control].Sp_Log	@BatchKey,'Sp_Withdrawals','InsertIntoStaging','Start', @RowsProcessed = @@ROWCOUNT
	-- WITHDRAWAL REQUESTS

	SELECT
		@BatchKey AS BatchKey,
		T.TransactionID AS TransactionID,
		'IBT' AS TransactionIDSource,
		ISNULL(T.BalanceTypeID,'UK') AS BalanceTypeID,
		CASE WHEN T.TransactionCode IN (32,33) AND (T.Amount+ISNULL(T10.Amount,0))<0 
			 THEN CASE WHEN RIGHT(T.TransactionDetailId,1)='+' THEN REPLACE(REPLACE(T.TransactionDetailId,'X',ISNULL(M.TRANSACTIONDETAILID,'U')),'+','-')	-- For a negative reversal, reverse the signs
					   ELSE REPLACE(REPLACE(T.TransactionDetailId,'X',ISNULL(M.TRANSACTIONDETAILID,'U')),'-','+') END 
		ELSE 
			CASE WHEN RIGHT(T.TransactionDetailId,2) IN ('X-','X+') THEN REPLACE(T.TransactionDetailId,'X',ISNULL(M.TRANSACTIONDETAILID,'U')) ELSE ISNULL(T.TransactionDetailId,'UK') END END AS TransactionDetailId,
		T.TransactionID AS BetGroupID,
		'IBT' AS BetGroupIDSource,
		T.TransactionID AS BetID,
		'IBT' AS BetIDSource,
		T.TransactionID AS LegID,
		'IBT' AS LegIDSource,
		0 AS FreeBetID,
		CASE REPLACE(T.TransactionMethodID,'AL',ISNULL(M.TransactionMethodID,T.TransactionMethodID))				-- For Reversals, since Method is determined later, replace the 'Assigned Later' by the Method of the Request got by Join 'M'
				WHEN 'BY' THEN Left(B.RefNumber,6)+'-'+Right(B.RefNumber,4) 
				WHEN 'EF' THEN K.AccountNumber 
				WHEN 'MB' THEN E.Email 
				WHEN 'PP' THEN E.Email 
				ELSE 'NULL' 
				END ExternalID,
		CASE REPLACE(T.TransactionMethodID,'AL',ISNULL(M.TransactionMethodID,T.TransactionMethodID))				-- For Reversals, since Method is determined later, replace the 'Assigned Later' by the Method of the Request got by Join 'M'
				WHEN 'BY' THEN CONVERT(varchar,B.BPayID) 
				WHEN 'EF' THEN K.BankBSB 
				WHEN 'MB' THEN CONVERT(varchar,ISNULL(E.NumberOfEmail,0)) 
				WHEN 'PP' THEN CONVERT(varchar,ISNULL(E.NumberOfEmail,0))  
				ELSE 'NULL' 
				END External1,
		CASE REPLACE(T.TransactionMethodID,'AL',ISNULL(M.TransactionMethodID,T.TransactionMethodID))				-- For Reversals, since Method is determined later, replace the 'Assigned Later' by the Method of the Request got by Join 'M'
				WHEN 'BY' THEN B.BillerCode 
				WHEN 'EF' THEN REPLACE(REPLACE(K.AccountName,', ',' '),',',' ') 
				ELSE 'NULL'
				END External2,
		ISNULL(tl.UserCode,-4) AS UserID,
		'IUU' AS UserIDSource,
		ISNULL(CASE REPLACE(T.TransactionMethodID,'AL',ISNULL(M.TransactionMethodID,T.TransactionMethodID))
				WHEN 'BY' THEN TRY_CONVERT(VARCHAR(50),B.BPayID)
				WHEN 'EF' THEN TRY_CONVERT(VARCHAR(50),K.BankID)
				WHEN 'MB' THEN TRY_CONVERT(VARCHAR(50),Mb.pay_from_email)
				WHEN 'PP' THEN TRY_CONVERT(VARCHAR(50),Pg.PayerID)
				ELSE  TRY_CONVERT(VARCHAR(50),0) 
				END,  TRY_CONVERT(VARCHAR(50),-1)) AS InstrumentID,
		ISNULL(CASE REPLACE(T.TransactionMethodID,'AL',ISNULL(M.TransactionMethodID,T.TransactionMethodID))
				WHEN 'BY' THEN 'BPY'
				WHEN 'EF' THEN 'EFT'
				WHEN 'MB' THEN 'MBK'
				WHEN 'PP' THEN 'PPL'
				ELSE 'N/A' END,'Unk')  AS InstrumentIDSource,
		T.AccountId LedgerID,
		CASE WHEN T.AccountId=0 THEN 'N/A' ELSE 'IAA' END AS LedgerSource,

		'C' as WalletId,
		0 AS PromotionID,
		'N/A' AS PromotionIDSource,
		
		CASE WHEN CONVERT(TIME,T.FromDate) ='00:00:00.000' THEN COALESCE(L.TransactionDate,T.TransactionDate) ELSE ISNULL(LT.CorrectedDate,T.FromDate) END AS  MasterTransactionTimeStamp,	-- FromDate is most accurate except when FromDate is midnight.
		CASE WHEN CONVERT(TIME,T.FromDate) ='00:00:00.000' THEN CONVERT(VARCHAR(11),CONVERT(DATE,COALESCE(L.TransactionDate,T.TransactionDate))) ELSE CONVERT(VARCHAR(11),CONVERT(DATE,ISNULL(LT.CorrectedDate,T.FromDate))) END AS MasterDayText,
		CASE WHEN CONVERT(TIME,T.FromDate) ='00:00:00.000' THEN CONVERT(VARCHAR(5),CONVERT(TIME,COALESCE(L.TransactionDate,T.TransactionDate))) ELSE CONVERT(VARCHAR(5),CONVERT(TIME,ISNULL(LT.CorrectedDate,T.FromDate))) END  AS MasterTimeText,
		--ISNULL(ch.ChannelName,'Internet') AS ChannelName,
		IsNull(t.Channel,-1) Channel,
		0 AS CampaignID,
		CASE WHEN T.TransactionCode IN (32,33) THEN		-- Seperate code for Withdrawal Reversals
			CASE WHEN (T.Amount+ISNULL(T10.Amount,0))<0 THEN ABS((T.Amount+ISNULL(T10.Amount,0)))*(CASE WHEN RIGHT(T.TransactionDetailId,1)='+' THEN 1 ELSE -1 END)*(-1)	--	For Reversals where Reversal Amt is greater than Request, opposite signs
			ELSE ABS((T.Amount+ISNULL(T10.Amount,0)))*(CASE WHEN RIGHT(T.TransactionDetailId,1)='+' THEN 1 ELSE -1 END) END
		ELSE ABS(T.Amount)*(CASE WHEN RIGHT(T.TransactionDetailId,1)='+' THEN 1 ELSE -1 END) END AS TransactedAmount, -- For Reversals add up the subsequent Request
		'Straight' AS BetGroupType,
		'N/A' AS BetTypeName,
		'N/A' AS BetSubTypeName,
		'N/A' AS LegBetTypeName,
		'Straight' AS GroupingType,
		0 AS PriceTypeID, /* AJ - PriceType Change */
		'N/A' AS PriceTypeSource, /* AJ - PriceType Change */
		0 AS ClassID,
		'IBT' AS ClassIDSource,
		0 AS EventID,
		'N/A' AS EventIDSource,
		0 AS MarketID, /* AJ - MarketId Change */
		'N/A' AS MarketIDSource, /* AJ - MarketId Change */
		1 AS LegNumber,
		1 AS NumberOfLegs,
		0 AS ExistsInDim,
		0 AS Conditional,
		1 AS MappingType,
		1 AS MappingID, -- 1 Withdrawal or Deposit -- Not betting transaction
		T.FromDate AS FromDate,
		0 AS DateTimeUpdated,
		R.requestid AS RequestId,
		R.WithdrawID AS WithdrawId
	INTO #Withdrawals
	FROM #withdrawalapproved T	

-- ************** Joins for Reverse Withdrawal Start **************

	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblTransactions T1								ON (T.TransactionID=T1.TransactionID AND T.FromDate=T1.ToDate)	
																											-- Used to get the TransactionMethod on the Request Transaction for Those Reversals (eg TC 31) without TranMethods

	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblTransactions T9								ON (T.ClientID=T9.ClientID AND T.FromDate=T9.ToDate AND ABS(T.Amount)=ABS(T9.Amount))	
																											-- Used to get TransactionMethod on TrasnactionCode 32 and 33
																											-- FD on TransactionCodes 32,33 will be same as TD on Request (eg TC 10)
																											-- TransactionID is different so using the abover join on Client/Dates/Amount.

	LEFT OUTER JOIN (SELECT TRANSACTIONCODE,MAX(LEFT(RIGHT(TRANSACTIONDETAILID,2),1)) AS TRANSACTIONDETAILID,MAX(TransactionMethodID) AS TransactionMethodID	
					 FROM Intrabet.TransactionTypeMapping  WHERE [Enabled]=1
					 GROUP BY TRANSACTIONCODE) M	
																											ON (ISNULL(T1.TransactionCode,T9.TransactionCode)=M.TransactionCode)				
																											-- Use the TransactionMethod from (T1 or T9) and use as TransactionMethod of Reversal.

	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblTransactions T10							ON ( T.ClientID=T10.ClientID AND T.FromDate=T10.FromDate AND ISNULL(T.Notes,1)= ISNULL(T10.Notes,1) 
																											AND T10.TransactionCode IN (5,10,11,14,17,18,25,27,29,30,5022,5026,6045)) 
																											-- Requests that happened on same (FD AND Notes) as Reversal, used to calculate reversal amount
																											-- There Can be multiple Reversal requests on the same FD, so using Notes as well
																											-- Notes can be empty sometimes, so using ISNULL on notes

	LEFT OUTER JOIN (	SELECT A.ClientID,A.TransactionID,A.TransactionCode 
						FROM [$(Acquisition)].IntraBet.tblTransactions A 	
						INNER JOIN (	SELECT CLIENTID,FROMDATE 
										FROM [$(Acquisition)].IntraBet.tblTransactions  
										WHERE TRANSACTIONCODE IN (32,33) AND FromDate > @FromDate AND FromDate <= @ToDate
										GROUP BY CLIENTID,FROMDATE) B ON A.CLIENTID=B.CLIENTID AND A.FROMDATE=B.FROMDATE
						WHERE A.TRANSACTIONCODE IN(5,10,11,14,17,18,25,27,29,30,5022,5026,6045)
					) T11																					ON (T.ClientId=T11.ClientId AND T.TransactionID=T11.TransactionID AND T.TransactionCode=T11.TransactionCode)	
																											-- Dont Include Requests that have already been adjusted by the reversal above.
																																							
-- ************** Joins for Reverse Withdrawal End **************

	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblClientWithdrawRequest R						ON (R.RequestID = TRY_CONVERT(int,t.Notes) AND ISNULL(TRY_CONVERT(int,t.Notes),0) <> 0 and R.ToDate = CONVERT(datetime2(3),'9999-12-31'))
	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblClientBpaydetails B							ON (B.BPayID = R.WithdrawID AND r.WithdrawID <> -1 and B.ToDate = CONVERT(datetime2(3),'9999-12-31'))
	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblClientBankdetails K							ON (K.BankID = R.WithdrawID AND r.WithdrawID <> -1 and K.ToDate = CONVERT(datetime2(3),'9999-12-31'))
	LEFT OUTER JOIN (	SELECT	ClientID, MAX(Email) Email, COUNT(DISTINCT Email) NumberOfEmail 
						FROM	(	SELECT ClientID, Email FROM [$(Acquisition)].Intrabet.tblClientaddresses  WHERE Email is not null and ToDate = '9999-12-31'
									UNION ALL
									SELECT ClientID, altEmail Email FROM [$(Acquisition)].Intrabet.tblClientaddresses  WHERE altEmail is not null and ToDate = '9999-12-31'
								) x
						GROUP BY ClientID
						HAVING count(distinct Email)  = 1
					  ) E																				ON (E.ClientID = T.ClientID)
	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblTransactionLogs L						ON (L.TransactionID = T.TransactionID and L.ToDate = CONVERT(datetime2(3),'9999-12-31'))
	--LEFT OUTER JOIN	Intrabet.ChannelMapping CH								ON (CH.Channel = T.Channel)
	LEFT OUTER JOIN [$(Acquisition)].IntraBet.Latency	LT								ON (T.FromDate=LT.OriginalDate)
	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblMoneybookersdepositresponse Mb			ON (T1.TransactionID=Mb.IntrabetTransactionID  and Mb.ToDate = CONVERT(datetime2(3),'9999-12-31'))
	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblPayPalDEC Pd							ON (T1.TransactionID=Pd.TransactionID  and Pd.ToDate = CONVERT(datetime2(3),'9999-12-31') and Pd.Token is not null)
	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblPayPalGEC Pg							ON (Pd.Token=Pg.Token  and Pg.ToDate = CONVERT(datetime2(3),'9999-12-31'))
	LEFT OUTER JOIN [$(Acquisition)].IntraBet.tblTransactionLogs  tl						ON T.TransactionID=tl.TransactionID and T.FromDate=tl.FromDate
	WHERE	T.BalanceTypeID IS NOT NULL
	AND		T11.ClientID IS NULL

UNION ALL

	-- DELETED WITHDRAWAL REQUESTS

	SELECT
		@BatchKey AS BatchKey,
		T.TransactionID AS TransactionID,
		'IBT' AS TransactionIDSource,
		ISNULL(T.BalanceTypeID,'UK') AS BalanceTypeID,
		CASE WHEN RIGHT(T.TransactionDetailId,2) IN ('X-','X+') THEN REPLACE(T.TransactionDetailId,'X',ISNULL(M.TRANSACTIONDETAILID,'U')) ELSE ISNULL(T.TransactionDetailId,'UK') END AS TransactionDetailId,
		T.TransactionID AS BetGroupID,
		'IBT' AS BetGroupIDSource,
		T.TransactionID AS BetID,
		'IBT' AS BetIDSource,
		T.TransactionID AS LegID,
		'IBT' AS LegIDSource,
		0 AS FreeBetID,
		CASE REPLACE(T.TransactionMethodID,'AL',ISNULL(M.TransactionMethodID,T.TransactionMethodID))
									WHEN 'BY' THEN Left(B.RefNumber,6)+'-'+Right(B.RefNumber,4) 
									WHEN 'EF' THEN K.AccountNumber 
									WHEN 'MB' THEN E.Email 
									WHEN 'PP' THEN E.Email 
									ELSE 'NULL' 
									END ExternalID,
		CASE REPLACE(T.TransactionMethodID,'AL',ISNULL(M.TransactionMethodID,T.TransactionMethodID))
									WHEN 'BY' THEN CONVERT(varchar,B.BPayID) 
									WHEN 'EF' THEN K.BankBSB 
									WHEN 'MB' THEN CONVERT(varchar,ISNULL(E.NumberOfEmail,0)) 
									WHEN 'PP' THEN CONVERT(varchar,ISNULL(E.NumberOfEmail,0))  
									ELSE 'NULL' 
									END External1,
		CASE REPLACE(T.TransactionMethodID,'AL',ISNULL(M.TransactionMethodID,T.TransactionMethodID))
									WHEN 'BY' THEN B.BillerCode 
									WHEN 'EF' THEN REPLACE(REPLACE(K.AccountName,', ',' '),',',' ') 
									ELSE 'NULL'
									END External2,
		ISNULL(d.UserCode,ISNULL(tl.UserCode,-4)) AS UserID,
		'IUU' AS UserIDSource,
		ISNULL(CASE REPLACE(T.TransactionMethodID,'AL',ISNULL(M.TransactionMethodID,T.TransactionMethodID))
				WHEN 'BY' THEN TRY_CONVERT(VARCHAR(50),B.BPayID)
				WHEN 'EF' THEN TRY_CONVERT(VARCHAR(50),K.BankID)
				WHEN 'MB' THEN TRY_CONVERT(VARCHAR(50),Mb.pay_from_email)
				WHEN 'PP' THEN TRY_CONVERT(VARCHAR(50),Pg.PayerID)
				ELSE  TRY_CONVERT(VARCHAR(50),0)
				END,  TRY_CONVERT(VARCHAR(50),-1)) AS InstrumentID,
		ISNULL(CASE REPLACE(T.TransactionMethodID,'AL',ISNULL(M.TransactionMethodID,T.TransactionMethodID))
				WHEN 'BY' THEN 'BPY'
				WHEN 'EF' THEN 'EFT'
				WHEN 'MB' THEN 'MBK'
				WHEN 'PP' THEN 'PPL'
				ELSE 'N/A' END,'Unk')  AS InstrumentIDSource,
		T.AccountId LedgerID,
		CASE WHEN T.AccountId=0 THEN 'N/A' ELSE 'IAA' END AS LedgerSource,
		'C' as WalletId,
		0 AS PromotionID,
		'N/A' AS PromotionIDSource,
		COALESCE(LT.CorrectedDate,T.ToDate,T.TransactionDate) AS  MasterTransactionTimeStamp,
		CONVERT(VARCHAR(11),CONVERT(DATE,COALESCE(LT.CorrectedDate,T.ToDate,T.TransactionDate))) AS MasterDayText,
		CONVERT(VARCHAR(5),CONVERT(TIME,COALESCE(LT.CorrectedDate,T.ToDate,T.TransactionDate))) AS MasterTimeText,
		--ISNULL(ch.ChannelName,'Internet') AS ChannelName,
		IsNull(t.Channel,-1) Channel,
		0 AS CampaignID,
		ABS(T.Amount)*(CASE WHEN RIGHT(T.TransactionDetailId,1)='+' THEN 1 ELSE -1 END) AS TransactedAmount,
		'Straight' AS BetGroupType,
		'N/A' AS BetTypeName,
		'N/A' AS BetSubTypeName,
		'N/A' AS LegBetTypeName,
		'Straight' AS GroupingType,
		0 AS PriceTypeID, /* AJ - PriceType Change */
		'N/A' AS PriceTypeSource, /* AJ - PriceType Change */
		0 AS ClassID,
		'IBT' AS ClassIDSource,
		0 AS EventID,
		'N/A' AS EventIDSource,
		0 AS MarketID, /* AJ - MarketId Change */
		'N/A' AS MarketIDSource, /* AJ - MarketId Change */
		1 AS LegNumber,
		1 AS NumberOfLegs,
		0 AS ExistsInDim,
		0 AS Conditional,
		1 AS MappingType,
		1 AS MappingID,
		T.ToDate AS FromDate,	-- ToDate of the record is the actual event Date time, which the recursive will later apply on the TransactionDateTime
		0 AS DateTimeUpdated,
		R.requestid AS RequestId,
		R.WithdrawID AS WithdrawId
	FROM #withdrawaldeleted T
	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblTransactions T1							ON (T.TransactionID=T1.TransactionID AND T.ToDate=T1.FromDate)	-- Check if this is the last record of the Transaction. Other wise its just an update.
	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblTransactions T2							ON (T.TransactionID=T2.TransactionID AND T.FromDate=T2.ToDate)	-- Used to get the Reqest Transaction for Those without TranMethods (like Reversals)
	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblTransactions T9							ON (T.ClientID=T9.ClientID AND T.FromDate=T9.ToDate AND ABS(T.Amount)=ABS(T9.Amount))		-- Used to get the Reqest Transaction for Those without TranMethods (like Reversals)
	LEFT OUTER JOIN (SELECT TRANSACTIONCODE,MAX(LEFT(RIGHT(TRANSACTIONDETAILID,2),1)) AS TRANSACTIONDETAILID,MAX(TransactionMethodID) AS TransactionMethodID				-- Get method of the request transaction for Reversals
					 FROM Intrabet.TransactionTypeMapping  WHERE [Enabled]=1
					 GROUP BY TRANSACTIONCODE) M														ON (ISNULL(T2.TransactionCode,T9.TransactionCode)=M.TransactionCode)
	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblClientWithdrawRequest R					ON (R.RequestID = TRY_CONVERT(int,t.Notes) AND ISNULL(TRY_CONVERT(int,t.Notes),0) <> 0 and R.ToDate = CONVERT(datetime2(3),'9999-12-31'))
	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblClientBpaydetails B						ON (B.BPayID = R.WithdrawID AND r.WithdrawID <> -1 and B.ToDate = CONVERT(datetime2(3),'9999-12-31'))
	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblClientBankdetails K						ON (K.BankID = R.WithdrawID AND r.WithdrawID <> -1 and K.ToDate = CONVERT(datetime2(3),'9999-12-31'))
	LEFT OUTER JOIN (	SELECT	ClientID, MAX(Email) Email, COUNT(DISTINCT Email) NumberOfEmail 
						FROM	(	SELECT ClientID, Email FROM [$(Acquisition)].Intrabet.tblClientaddresses  WHERE Email is not null and ToDate = '9999-12-31'
									UNION ALL
									SELECT ClientID, altEmail Email FROM [$(Acquisition)].Intrabet.tblClientaddresses  WHERE altEmail is not null and ToDate = '9999-12-31'
								) x
						GROUP BY ClientID
						HAVING count(distinct Email)  = 1
					  ) E																		ON (E.ClientID = T.ClientID)
	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblTransactionLogs L						ON (L.TransactionID = T.TransactionID and L.ToDate = CONVERT(datetime2(3),'9999-12-31'))
	--LEFT JOIN Intrabet.ChannelMapping CH									ON (CH.Channel = T.Channel)
	LEFT OUTER JOIN [$(Acquisition)].IntraBet.Latency	LT								ON (T.ToDate=LT.OriginalDate)

	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblMoneybookersdepositresponse Mb			ON (T1.TransactionID=Mb.IntrabetTransactionID  and Mb.ToDate = CONVERT(datetime2(3),'9999-12-31'))
	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblPayPalDEC Pd							ON (T1.TransactionID=Pd.TransactionID  and Pd.ToDate = CONVERT(datetime2(3),'9999-12-31') and Pd.Token is not null)
	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblPayPalGEC Pg							ON (Pd.Token=Pg.Token  and Pg.ToDate = CONVERT(datetime2(3),'9999-12-31'))
	LEFT OUTER JOIN [$(Acquisition)].IntraBet.tblDeletedTransactions  d					ON T.TransactionID=D.TransactionID and T.ToDate=d.FromDate
	LEFT OUTER JOIN [$(Acquisition)].IntraBet.tblTransactionLogs  tl					ON T.TransactionID=tl.TransactionID and T.ToDate=tl.FromDate

	WHERE	T.BalanceTypeID IS NOT NULL
	AND		T1.TransactionID IS NULL
	OPTION (RECOMPILE);



	INSERT INTO Stage.[Transaction] (BatchKey,TransactionId,TransactionIdSource,BalanceTypeID,TransactionTypeID,TransactionStatusID,TransactionMethodID,TransactionDirection,BetGroupID,BetGroupIDSource,BetId,BetIdSource,LegId,LegIdSource,FreeBetID,
	ExternalID,External1,External2,UserID,UserIDSource,InstrumentID,InstrumentIDSource,LedgerID,LedgerSource,WalletId, PromotionID, PromotionIDSource,MasterTransactionTimestamp,MasterDayText,MasterTimeText,Channel,ActionChannel,CampaignId,TransactedAmount,BetGroupType,
	BetTypeName,BetSubTypeName,LegBetTypeName,GroupingType,PriceTypeId,PriceTypeSource,ClassId,ClassIdSource,EventID,EventIDSource,MarketID,MarketIdSource,LegNumber,NumberOfLegs,ExistsInDim,Conditional,MappingType,MappingId,FromDate,DateTimeUpdated,RequestId,WithdrawID,
	InPlay,ClickToCall,CashoutType,IsDoubleDown,IsDoubledDown)
	SELECT	a.BatchKey,a.TransactionID,a.TransactionIDSource,a.BalanceTypeID,IsNull(b.TransactionTypeID,'UK'),IsNull(b.TransactionStatusID,'UK'),IsNull(b.TransactionMethodID,'UK'),CASE WHEN RIGHT(IsNull(b.TransactionDetailId,'UN'),1) = '+' THEN 'CR' ELSE 'DR' END AS TransactionDirection,
			a.BetGroupID,a.BetGroupIDSource,a.BetID,BetIdSource,LegId,LegIdSource,FreeBetID,ExternalID,External1,External2,UserID, UserIDSource, InstrumentID,InstrumentIDSource,LedgerID,LedgerSource,WalletId,PromotionID,PromotionIDSource,MasterTransactionTimestamp,MasterDayText,MasterTimeText,Channel,Channel ActionChannel,CampaignId,TransactedAmount,BetGroupType,
			BetTypeName,BetSubTypeName,LegBetTypeName,GroupingType,PriceTypeId,PriceTypeSource,ClassId,ClassIdSource,EventID,EventIDSource,MarketID,MarketIdSource,LegNumber,NumberOfLegs,ExistsInDim,Conditional,MappingType,MappingId,FromDate,DateTimeUpdated,RequestId,WithdrawID,
			'N/A' InPlay,'N/A' ClickToCall, 0 CashoutType, 0 IsDoubleDown, 0 IsDoubledDown
	FROM #Withdrawals a
	LEFT OUTER JOIN (Select Distinct TransactionDetailId, TransactionTypeID, TransactionStatusID, TransactionMethodID From Intrabet.TransactionTypeMapping) b on a.TransactionDetailId = b.TransactionDetailId
	OPTION (RECOMPILE);
	
	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Withdrawals', NULL, 'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Withdrawals', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
END

CREATE PROC [Intrabet].[Sp_AtomicOpeningBalances]
(
	@BatchKey	int,
	@FromDate	datetime2(3),
	@ToDate		datetime2(3),
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @RowCount INT;

	SET TRANSACTION ISOLATION LEVEL SNAPSHOT;
	
BEGIN TRY

	BEGIN TRY DROP TABLE #Days END TRY BEGIN CATCH END CATCH -- Speculative attempt to drop pre-existing Temp tables
	BEGIN TRY DROP TABLE #AccountBalances END TRY BEGIN CATCH END CATCH
	BEGIN TRY DROP TABLE #Transactions END TRY BEGIN CATCH END CATCH
	BEGIN TRY DROP TABLE #TransactionBalances END TRY BEGIN CATCH END CATCH
	BEGIN TRY DROP TABLE #BonusBalances END TRY BEGIN CATCH END CATCH
	BEGIN TRY DROP TABLE #UnsettledAllups END TRY BEGIN CATCH END CATCH
	BEGIN TRY DROP TABLE #UnsettledBets END TRY BEGIN CATCH END CATCH
	BEGIN TRY DROP TABLE #UnsettledBalances END TRY BEGIN CATCH END CATCH
	BEGIN TRY DROP TABLE #Bets END TRY BEGIN CATCH END CATCH
	BEGIN TRY DROP TABLE #BetBalances END TRY BEGIN CATCH END CATCH
	BEGIN TRY DROP TABLE #FirstLastBets END TRY BEGIN CATCH END CATCH
	BEGIN TRY DROP TABLE #BetBalancesFirstLast END TRY BEGIN CATCH END CATCH

	EXEC [Control].Sp_Log	@BatchKey,'Sp_AtomicOpeningBalances','NewAccounts','Start'

	INSERT	Atomic.OpeningBalance
			(	BatchKey, DayDate, LedgerId, FreeBet, FromDate, AccountEnabled, Status, 
				ClientBalance, UnsettledBalance, PendingDebitbalance, LifetimeStake, LifetimePayout, 
				FirstCreditDate, LastCreditDate, LifetimeCredit, FirstDebitDate, LastDebitDate, LifetimeDebit,
				FirstBetId, FirstBetDate, FirstBetType, FirstChannel, FirstEventId, FirstClassID,
				LastBetId, LastBetDate, LastBetType, LastChannel, LastEventId, LastClassID, LifetimebetsPlaced, FirstWinPlace, LastWinPlace, FirstCreditId, LastCreditId)
	SELECT  @BatchKey BatchKey, CONVERT(date, ISNULL(l.CorrectedDate,a.FromDate)) DayDate, a.AccountId, 0 FreeBet, a.FromDate, a.AccountEnabled, a.Status, 
			0 ClientBalance, 0 UnsettledBalance, 0 PendingDebitBalance, 0 LifetimeStake, 0 LifetimePayout,
			NULL FirstCreditDate, NULL LastCreditDate, 0 LifetimeCredit, NULL FirstDebitDate, NULL LastDebitDate, 0 LifetimeDebit,
			NULL FirstBetId, NULL FirstBetDate, NULL FirstBetType, NULL FirstChannel, NULL FirstEventId, NULL FirstClassID,
			NULL LastBetId, NULL LastBetDate, NULL LastBetType, NULL LastChannel, NULL LastEventId, 
			NULL LastClassID, 0 LifetimebetsPlaced, NULL FirstWinPlace, NULL LastWinPlace, NULL FirstCreditId, NULL LastCreditId
	FROM	[$(Acquisition)].IntraBet.tblAccounts a 
			LEFT JOIN [$(Acquisition)].Intrabet.Latency l ON (l.OriginalDate = a.FromDate AND l.OriginalDate > @FromDate)
			LEFT JOIN [$(Acquisition)].IntraBet.tblAccounts a1 ON (a1.AccountID = a.AccountID AND a1.ToDate = a.FromDate)
	WHERE	a.FromDate > @FromDate
			AND a.FromDate <= @ToDate 
			AND a.FromDate > CONVERT(datetime2(3),CONVERT(date,a.FromDate)) -- This processes is designed to ensure an OpeningBalance record is created for accounts opened mid-way through the day - if an account is actually opened on the stroke of midnight, then its first day of life will be picked up by the normal "midnights in the batch" logic below and including it here will create a duplicate
			AND a1.AccountID IS NULL
	OPTION (RECOMPILE, TABLE HINT(a, FORCESEEK([NI_tblAccounts_FD(A)](FromDate))), TABLE HINT(a1, FORCESEEK([CI_tblAccounts(A)](AccountId))))

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,'Sp_AtomicOpeningBalances','DayBoundaries','Start', @RowsProcessed = @RowCount

	SELECT	@BatchKey BatchKey, DATEADD(day,1,CONVERT(date,CorrectedDate)) DayDate, OriginalDate FromDate, CorrectedDate CorrectedFromDate
	INTO #Days
	FROM	(	SELECT	OriginalDate, CorrectedDate, LEAD(Correcteddate,1,NULL) OVER (ORDER BY OriginalDate) NextCorrectedDate 
				FROM	[$(Acquisition)].IntraBet.Latency l 
				WHERE	OriginalDate BETWEEN @FromDate AND @ToDate
			) y
	WHERE	CONVERT(date,CorrectedDate) <> CONVERT(date,NextCorrecteddate)
	OPTION (RECOMPILE)

	----------------NEW ADDITIONAL DATE IN CASE OF NO LATENCY RECORD------------------

	SELECT	TOP 1
			@BatchKey BatchKey,  
			DATEADD(day,1,CONVERT(date,CorrectedDate)) DayDate, 
			OriginalDate FromDate,
			CorrectedDate CorrectedFromDate
	INTO	#PotentialFirstDate
	FROM	[$(Acquisition)].IntraBet.Latency 
	WHERE	OriginalDate <= @FromDate
	ORDER BY OriginalDate DESC

	IF (SELECT MIN(DayDate) FROM #Days) <> (SELECT DayDate FROM #PotentialFirstDate) 
		INSERT INTO #Days SELECT * FROM #PotentialFirstDate
	-----------------------------------------------------------------------------------

	IF (SELECT Count(*) FROM #Days) = 0
		BEGIN
			DROP TABLE #Days
			SET @RowCount = 0
		END
	ELSE 
		BEGIN
			EXEC [Control].Sp_Log	@BatchKey,'Sp_AtomicOpeningBalances','AccountBalances','Start', @RowsProcessed = @@ROWCOUNT

			SELECT  d.BatchKey, d.DayDate, a.AccountId, d.FromDate, a.ClientId, a.AccountNumber, a.AccountEnabled, a.Status, a.Balance ClientBalance
			INTO	#AccountBalances
			FROM	#Days d
					INNER JOIN [$(Acquisition)].IntraBet.tblAccounts a ON (a.FromDate <= d.FromDate AND a.ToDate > d.FromDate AND a.FromDate < @ToDate and a.ToDate > @FromDate)
			OPTION (RECOMPILE)

			EXEC [Control].Sp_Log	@BatchKey,'Sp_AtomicOpeningBalances','Transactions','Start', @RowsProcessed = @@ROWCOUNT

			DROP TABLE #Days

			SELECT	ClientId, AccountNumber, FromDay, ToDay, TransactionType, PendingTransactionId,TransactionID, -- Group together all transactions under common day ranges to reduce the number of rows unless they are pending transactions which need to feature in a transaction level extract later
					SUM(PendingAmount) PendingBalance,
					MIN(TransactionDate) FirstDate,
					MAX(TransactionDate) LastDate,
					SUM(Amount) Amount,
					COUNT(*) TransactionCount
			INTO	#Transactions
			FROM	(	SELECT	b.ClientId, b.AccountNumber, convert(date,ISNULL(f.CorrectedDate,b.FromDate)) FromDay, convert(date,ISNULL(t.CorrectedDate,b.ToDate)) ToDay, m.TransactionType,
								CASE 
									WHEN m.Pending <> 'Y' THEN NULL 
									WHEN b.ToDate = '9999-12-30' THEN NULL
									ELSE TransactionId 
								END PendingTransactionId, 
								CASE 
									WHEN m.Pending <> 'Y' THEN 0 
									WHEN b.ToDate = '9999-12-30' THEN 0 
									WHEN m.TransactionType = 'Withdrawal' THEN ISNULL(-Amount,0) 
									ELSE ISNULL(Amount,0)
								END PendingAmount, 
								TransactionDate,
								b.TransactionID,
								ISNULL(Amount,0) Amount
						FROM	[$(Acquisition)].IntraBet.tblTransactions b 
								LEFT JOIN [$(Acquisition)].Intrabet.Latency f ON (f.OriginalDate = b.FromDate)
								LEFT JOIN [$(Acquisition)].Intrabet.Latency t ON (t.OriginalDate = b.ToDate) -- allow for 31/12/9999
								INNER JOIN (SELECT	TransactionCode, 
													TransactionType, 
													MIN(CASE 
															WHEN TransactionCode = 17 THEN 'Y' -- Temporary measure until understand why Tran Code 17 has a 'Complete' mapping?
															WHEN TransactionCode = 8 THEN 'N' -- Temporary measure until understand why Tran Code 8 does not have a 'Complete' mapping?
															WHEN TransactionCode IN (31,32,33) THEN 'N'
															WHEN TransactionStatus = 'Complete' THEN 'N' 
															ELSE 'Y' 
														END) Pending 
											FROM	Intrabet.TransactionTypeMapping 
											WHERE 	TransactionType IN ('Deposit','Withdrawal')
											GROUP BY TransactionCode, TransactionType
										) m ON (m.TransactionCode = b.TransactionCode)
						WHERE	b.ToDate > @FromDate AND b.FromDate < @ToDate
					) y
			GROUP BY ClientId, AccountNumber, FromDay, ToDay, TransactionType, PendingTransactionId,TransactionID
			OPTION (RECOMPILE)

			EXEC [Control].Sp_Log	@BatchKey,'Sp_AtomicOpeningBalances','Transaction Balances','Start', @RowsProcessed = @@ROWCOUNT

			SELECT	x.BatchKey, x.DayDate, x.AccountId,
					SUM(CASE t.TransactionType WHEN 'Withdrawal' THEN t.PendingBalance ELSE 0 END) PendingDebitBalance,
					MIN(CASE t.TransactionType WHEN 'Deposit' THEN t.FirstDate ELSE NULL END) FirstCreditDate,
					MAX(CASE t.TransactionType WHEN 'Deposit' THEN t.LastDate ELSE NULL END) LastCreditDate,
					MIN(CASE t.TransactionType WHEN 'Deposit' THEN t.TransactionID ELSE NULL END) FirstCreditId,
					MAX(CASE t.TransactionType WHEN 'Deposit' THEN t.TransactionID ELSE NULL END) LastCreditId,
					SUM(CASE t.TransactionType WHEN 'Deposit' THEN t.Amount ELSE 0 END) LifetimeCredit,
					SUM(CASE t.TransactionType WHEN 'Deposit' THEN t.TransactionCount ELSE 0 END) CreditCount,
					MIN(CASE t.TransactionType WHEN 'Withdrawal' THEN t.FirstDate ELSE NULL END) FirstDebitDate,
					MAX(CASE t.TransactionType WHEN 'Withdrawal' THEN t.LastDate ELSE NULL END) LastDebitDate,
					SUM(CASE t.TransactionType WHEN 'Withdrawal' THEN t.Amount ELSE 0 END) LifetimeDebit,
					SUM(CASE t.TransactionType WHEN 'Withdrawal' THEN t.TransactionCount ELSE 0 END) DebitCount
			INTO	#TransactionBalances
			FROM	#AccountBalances x
					INNER JOIN #Transactions t ON (t.clientid = x.clientid and t.AccountNumber = x.accountnumber and t.FromDay < x.DayDate and t.ToDay >= x.DayDate)
			GROUP BY BatchKey, DayDate, AccountId
			OPTION (RECOMPILE)

			EXEC [Control].Sp_Log	@BatchKey,'Sp_AtomicOpeningBalances','PendingWithdrawal','Start', @RowsProcessed = @@ROWCOUNT

			INSERT	Atomic.PendingWithdrawal (BatchKey, DayDate, LedgerId, TransactionId, Amount)
			SELECT	a.BatchKey, a.DayDate, a.AccountId, t.PendingTransactionId TransactionId, t.Amount
			FROM	#AccountBalances a
					INNER JOIN #Transactions t ON (t.ClientID = a.ClientID AND t.AccountNumber = a.AccountNumber AND t.FromDay < a.DayDate AND t.ToDay >= a.DayDate)
			WHERE	TransactionType = 'Withdrawal'
					AND	PendingTransactionId IS NOT NULL
			OPTION (RECOMPILE)

			EXEC [Control].Sp_Log	@BatchKey,'Sp_AtomicOpeningBalances','BonusBalances','Start', @RowsProcessed = @@ROWCOUNT

			DROP TABLE #Transactions

			SELECT	BatchKey, DayDate, AccountId, 
					SUM(Amount) ClientBalance, 
					MIN(CreditDate) FirstCreditDate, 
					MAX(CreditDate) LastCreditDate, 
					SUM(Credit) LifetimeCredit, 
					SUM(CreditCount) CreditCount, 
					MIN(DebitDate) FirstDebitDate, 
					MAX(DebitDate) LastDebitDate, 
					SUM(Debit) LifetimeDebit, 
					SUM(DebitCount) DebitCount
			INTO	#BonusBalances
			FROM	(	SELECT	x.BatchKey, x.DayDate, x.AccountId, 
								t.Amount, 
								CASE WHEN BTransCode = -4 THEN NULL WHEN t.Amount < 0 THEN NULL ELSE TransactionDate END CreditDate, 
								CASE WHEN BTransCode = -4 THEN 0 WHEN t.Amount < 0 THEN 0 ELSE Amount END Credit, 
								CASE WHEN BTransCode = -4 THEN 0 WHEN t.Amount < 0 THEN 0 ELSE 1 END CreditCount, 
								CASE WHEN BTransCode = -4 THEN NULL WHEN t.Amount > 0 THEN NULL ELSE TransactionDate END DebitDate, 
								CASE WHEN BTransCode = -4 THEN 0 WHEN t.Amount > 0 THEN 0 ELSE Amount END Debit, 
								CASE WHEN BTransCode = -4 THEN 0 WHEN t.Amount > 0 THEN 0 ELSE 1 END DebitCount
						FROM	#AccountBalances x
								INNER JOIN [$(Acquisition)].Intrabet.tblBonusTransactions t ON (t.ClientID = x.ClientID AND x.AccountNumber = 1 and t.FromDate <= x.FromDate and t.ToDate > x.FromDate)
						UNION ALL
						SELECT	x.BatchKey, x.DayDate, x.AccountId, 
								CASE WHEN t.BetUsed = 0 and t.ExpiryDate >= x.DayDate THEN t.Value ELSE 0 END Amount, 
								t.IssueDate CreditDate, 
								t.Value Credit,
								1 CreditCount,
								CASE WHEN t.BetUsed = 0 and t.ExpiryDate < x.DayDate THEN t.ExpiryDate ELSE NULL END DebitDate,
								CASE WHEN t.BetUsed = 0 and t.ExpiryDate < x.DayDate THEN -t.Value ELSE 0 END Debit,
								CASE WHEN t.BetUsed = 0 and t.ExpiryDate < x.DayDate THEN 1 ELSE 0 END DebitCount
						FROM	#AccountBalances x
								INNER JOIN [$(Acquisition)].Intrabet.tblPromotionalBets t ON (t.ClientID = x.ClientID AND x.AccountNumber = 1 and t.FromDate <= x.FromDate and t.ToDate > x.FromDate)
					) y
			GROUP BY BatchKey, DayDate, AccountId
			OPTION (RECOMPILE)

			EXEC [Control].Sp_Log	@BatchKey,'Sp_AtomicOpeningBalances','UnsettledAllups','Start', @RowsProcessed = @@ROWCOUNT

			SELECT	DISTINCT x.BatchKey, x.DayDate, x.FromDate, x.AccountID, b.AllUpID
			INTO	#UnsettledAllups
			FROM	#AccountBalances x
					INNER JOIN [$(Acquisition)].IntraBet.tblBets b ON (b.clientid = x.clientid AND b.FromDate <= x.FromDate AND b.ToDate > x.FromDate and b.ToDate > @FromDate AND b.FromDate < @ToDate)
			WHERE	x.AccountNumber = 1 AND b.AllUpID IS NOT NULL AND b.Valid = 1 AND b.Settled = 0
			OPTION (RECOMPILE)

			EXEC [Control].Sp_Log	@BatchKey,'Sp_AtomicOpeningBalances','UnsettledAllUpBets','Start', @RowsProcessed = @@ROWCOUNT

			SELECT	x.BatchKey, x.DayDate, x.FromDate, x.AccountId, b.BetId, ISNULL(b.FreeBetID, 0) FreeBetId, b.AllUpID, b.EventID, IsNull(e.EventType,-1) ClassID, b.Settled, ISNULL(b.AmountToWin,0) + ISNULL(b.AmountToPlace,0) Stake
			INTO	#UnsettledBets
			FROM	#UnsettledAllups x
					INNER JOIN [$(Acquisition)].IntraBet.tblBets b ON (b.AllUpID = x.AllUpID AND b.FromDate <= x.FromDate AND b.ToDate > x.FromDate and b.ToDate > @FromDate AND b.FromDate < @ToDate)
					LEFT JOIN	[$(Acquisition)].Intrabet.tblEvents e ON (e.EventID = b.EventID AND e.FromDate <= x.FromDate AND e.ToDate > x.FromDate AND e.ToDate > @FromDate AND e.FromDate < @ToDate) -- join to Events to get the Event Type which is used as natural key for ClassId
			OPTION (RECOMPILE)

			DROP TABLE #UnsettledAllups

			EXEC [Control].Sp_Log	@BatchKey,'Sp_AtomicOpeningBalances','UnsettledSingleBets','Start', @RowsProcessed = @@ROWCOUNT
			INSERT	#UnsettledBets (BatchKey, DayDate, FromDate, AccountID, BetID, FreeBetId, AllUpID, EventID, ClassID, Settled, Stake)
			SELECT	x.BatchKey, x.DayDate, x.FromDate, x.AccountID, b.BetID, ISNULL(b.FreeBetID, 0) FreeBetId, b.AllUpID, b.EventID, IsNull(e.EventType,-1) ClassID, b.Settled, ISNULL(b.AmountToWin,0) + ISNULL(b.AmountToPlace,0) Stake
			FROM	#AccountBalances x
					INNER JOIN [$(Acquisition)].IntraBet.tblBets b ON (b.clientid = x.clientid AND b.FromDate <= x.FromDate AND b.ToDate > x.FromDate and b.ToDate > @FromDate AND b.FromDate < @ToDate)
					LEFT JOIN	[$(Acquisition)].Intrabet.tblEvents e ON (e.EventID = b.EventID AND e.FromDate <= x.FromDate AND e.ToDate > x.FromDate AND e.ToDate > @FromDate AND e.FromDate < @ToDate) -- join to Events to get the Event Type which is used as natural key for ClassId
			WHERE	x.AccountNumber = 1 AND b.AllUpID IS NULL AND b.Valid = 1 AND b.Settled = 0 AND ISNULL(b.AmountToWin,0) + ISNULL(b.AmountToPlace,0) <> 0
			OPTION (RECOMPILE)

			EXEC [Control].Sp_Log	@BatchKey,'Sp_AtomicOpeningBalances','UnsettledTidy','Start', @RowsProcessed = @@ROWCOUNT
			DELETE	u 
			FROM	#UnsettledBets u 
			INNER JOIN (	SELECT	y.BatchKey, y.DayDate, y.BetId,
									CASE MIN(a.WinMultiFactor + a.PlaceMultiFactor)  OVER (PARTITION BY y.DayDate, y.AllUpId) WHEN 0 THEN 1 ELSE 0 END FullySettled
							FROM	(	SELECT	x.BatchKey, x.DayDate, x.FromDate, x.AllUpId, x.BetId, CASE WHEN e.SettledDate <= x.DayDate THEN 1 ELSE Settled END Settled
										FROM	#UnsettledBets x
												LEFT JOIN [$(Acquisition)].IntraBet.tblEvents e ON (e.EventID = x.EventID AND e.FromDate <= x.FromDate AND e.ToDate > x.FromDate and e.ToDate > @FromDate AND e.FromDate < @ToDate)
									) y
									LEFT JOIN [$(Acquisition)].IntraBet.tblAllUps a ON (a.Betid = y.BetID AND a.FromDate <= y.FromDate AND a.ToDate > y.FromDate AND y.Settled = 1 and a.ToDate > @FromDate AND a.FromDate < @ToDate)
						) z ON (z.BatchKey = u.BatchKey AND z.DayDate = u.DayDate AND z.BetID = u.BetID)
			WHERE	FullySettled = 1
			OPTION (RECOMPILE)

			EXEC [Control].Sp_Log	@BatchKey,'Sp_AtomicOpeningBalances','Unsettled','Start', @RowsProcessed = @@ROWCOUNT
			INSERT	Atomic.Unsettled (BatchKey, DayDate, LedgerId, BetId, AllUpID, EventID, SettledLeg, Amount)
			SELECT	BatchKey, DayDate, AccountId, BetId, AllUpID, EventID, Settled SettledLeg,
					ROUND(Amount/NumberOfLegs,4) 
						+	CASE 
								WHEN (Amount - NumberOfLegs * ROUND(Amount/NumberOfLegs,4))/0.0001 >= LegNumber THEN 0.0001
								WHEN (Amount - NumberOfLegs * ROUND(Amount/NumberOfLegs,4))/0.0001 <= -LegNumber THEN -0.0001
								ELSE 0.0000
							END Amount
			FROM	(	SELECT	BatchKey, DayDate, AccountId, BetId, AllUpID, EventID, Settled, 
								SUM(Stake) OVER (PARTITION BY BatchKey, DayDate, ISNULL(AllUpId,BetId)) Amount,
								DENSE_RANK() OVER (PARTITION BY BatchKey, DayDate, ISNULL(AllUpId,BetId) ORDER BY BetId) LegNumber, 
								COUNT(*) OVER (PARTITION BY BatchKey, DayDate, ISNULL(AllUpId,BetId)) NumberOfLegs
						FROM	#UnsettledBets
					) x
			OPTION (RECOMPILE)

			EXEC [Control].Sp_Log	@BatchKey,'Sp_AtomicOpeningBalances','UnsettledBalances','Start', @RowsProcessed = @@ROWCOUNT

			SELECT	BatchKey, DayDate, AccountID, CASE FreeBetId WHEN 0 THEN 0 ELSE 1 END FreeBet, SUM(Stake) UnsettledBalance, COUNT(*) Bets
			INTO	#UnsettledBalances
			FROM	#UnsettledBets x
			group by BatchKey, DayDate, AccountID, CASE FreeBetId WHEN 0 THEN 0 ELSE 1 END
			OPTION (RECOMPILE)

			DROP TABLE #UnsettledBets

			EXEC [Control].Sp_Log	@BatchKey,'Sp_AtomicOpeningBalances','Bets','Start', @RowsProcessed = @@ROWCOUNT

			SELECT	ClientId, SIGN(ISNULL(FreeBetId,0)) FreeBet, CONVERT(date,ISNULL(f.CorrectedDate,b.fromdate)) FromDay, CONVERT(date,ISNULL(t.CorrectedDate,b.todate)) ToDay, 
					SUM(ISNULL(AmountToWin,0)+ISNULL(AmountToPlace,0)) Stake, SUM(ISNULL(PayoutWin,0)+ISNULL(PayoutPlace,0)) Payout, 
					MIN(BetId) FirstBetId, 
					MAX(BetId) LastBetId,
					COUNT(*) BetsPlaced
			INTO	#Bets
			FROM	[$(Acquisition)].IntraBet.tblBets b
					LEFT JOIN [$(Acquisition)].Intrabet.Latency f ON (f.OriginalDate = b.FromDate)
					LEFT JOIN [$(Acquisition)].Intrabet.Latency t ON (t.OriginalDate = b.ToDate) -- allow for 31/12/9999
			WHERE	b.Valid = 1 AND b.ToDate > @FromDate AND b.FromDate < @ToDate
					AND CONVERT(date,ISNULL(f.CorrectedDate,b.fromdate)) <> CONVERT(date,ISNULL(t.CorrectedDate,b.todate)) -- Only keep versions of the bet record which span day boundaries and are therefore to be used in daily balances
					AND (b.AmountToWin > 0 or b.AmountToPlace > 0) -- Limit to primary leg of multis so that the count returns number of bets, not legs
			GROUP BY clientid, SIGN(ISNULL(FreeBetId,0)), CONVERT(date,ISNULL(f.CorrectedDate,b.fromdate)), CONVERT(date,ISNULL(t.CorrectedDate,b.todate))
			OPTION (RECOMPILE)

			EXEC [Control].Sp_Log	@BatchKey,'Sp_AtomicOpeningBalances','BetBalances','Start', @RowsProcessed = @@ROWCOUNT

			SELECT	BatchKey, DayDate, x.FromDate, AccountId, FreeBet,
					SUM(Stake) LifetimeStake,
					SUM(Payout) LifetimePayout, 
					MIN(FirstBetId) FirstBetId, 
					MAX(LastBetId) LastBetId,
					SUM(BetsPlaced) BetsPlaced
			INTO	#BetBalances
			FROM	#AccountBalances x
					INNER JOIN #Bets b ON (b.clientid = x.clientid AND x.AccountNumber = 1 AND b.FromDay < x.DayDate AND b.ToDay >= x.DayDate)
			GROUP BY BatchKey, DayDate, x.FromDate, AccountId, FreeBet
			OPTION (RECOMPILE)

			DROP TABLE #Bets

			EXEC [Control].Sp_Log	@BatchKey,'Sp_AtomicOpeningBalances','FirstLastBets','Start', @RowsProcessed = @@ROWCOUNT

			SELECT	x.BatchKey, x.DayDate, b.BetID, b.BetDate, b.BetType, b.AmountToPlace, b.AmountToWin, ISNULL(gg.GroupDesc,'Straight') BetGroupType, b.Channel, b.EventID, IsNull(e.EventType,-1) ClassID
			INTO	#FirstLastBets
			FROM	(	SELECT DISTINCT BatchKey, DayDate, FromDate, FirstBetId BetId FROM #BetBalances
									UNION
									SELECT DISTINCT BatchKey, DayDate, FromDate, LastBetId BetId FROM #BetBalances
					) x
					INNER JOIN [$(Acquisition)].Intrabet.tblBets b ON (b.BetId = x.BetId AND b.FromDate <= x.FromDate AND b.ToDate > x.FromDate and b.ToDate > @FromDate AND b.FromDate < @ToDate)
					LEFT JOIN	[$(Acquisition)].Intrabet.tblEvents e ON (e.EventID = b.EventID AND e.FromDate <= x.FromDate AND e.ToDate > x.FromDate AND e.ToDate > @FromDate AND e.FromDate < @ToDate) -- join to Events to get the Event Type which is used as natural key for ClassId
					LEFT JOIN	[$(Acquisition)].Intrabet.tblAllUpGroupBets ag ON (ag.BetID = x.BetID AND ag.ToDate >= CONVERT(datetime2(3), '9999-12-30'))
					LEFT JOIN	[$(Acquisition)].Intrabet.tblLUAllupGroups gg ON (gg.AllupGroupID = ag.AllupGroupID AND gg.ToDate >= CONVERT(datetime2(3), '9999-12-30'))
			OPTION (RECOMPILE)

			EXEC [Control].Sp_Log	@BatchKey,'Sp_AtomicOpeningBalances','BetBalanceFirstLast','Start', @RowsProcessed = @@ROWCOUNT

			SELECT	x.*, 
					f.BetDate FirstBetDate, f.BetType FirstBetType, f.AmountToPlace FirstAmountToPlace, f.AmountToWin FirstAmountToWin, f.BetGroupType FirstBetGroupType, f.Channel FirstChannel, f.EventID FirstEventId, f.ClassID FirstClassId,
					l.BetDate LastBetDate, l.BetType LastBetType, l.AmountToPlace LastAmountToPlace, l.AmountToWin LastAmountToWin, l.BetGroupType LastBetGroupType, l.Channel LastChannel, l.EventID LastEventId, l.ClassID LastClassId
			INTO	#BetBalancesFirstLast
			FROM	#BetBalances x
					LEFT JOIN #FirstLastBets f ON (f.BatchKey = x.BatchKey AND f.DayDate = x.DayDate AND f.BetId = x.FirstBetId)
					LEFT JOIN #FirstLastBets l ON (l.BatchKey = x.BatchKey AND l.DayDate = x.DayDate AND l.BetId = x.LastBetId)
			OPTION (RECOMPILE)

			DROP TABLE #BetBalances
			DROP TABLE #FirstLastBets

			EXEC [Control].Sp_Log	@BatchKey,'Sp_AtomicOpeningBalances','Atomic','Start', @RowsProcessed = @@ROWCOUNT

			INSERT	Atomic.OpeningBalance
					(	BatchKey, DayDate, LedgerId, FreeBet, FromDate, AccountEnabled, Status, 
						ClientBalance, UnsettledBalance, PendingDebitbalance, LifetimeStake, LifetimePayout, 
						FirstCreditDate, LastCreditDate, LifetimeCredit, FirstDebitDate, LastDebitDate, LifetimeDebit,
						FirstBetId, FirstBetDate, FirstBetType, FirstChannel, FirstEventId, FirstClassId,
						LastBetId, LastBetDate, LastBetType, LastChannel, LastEventId, LastClassId, LifetimebetsPlaced,FirstWinPlace, LastWinPlace, FirstBetGroupType, LastBetGroupType, FirstCreditId, LastCreditId)
			SELECT  x.BatchKey, x.DayDate, x.AccountId, x.FreeBet, x.FromDate, x.AccountEnabled, x.Status, 
					x.ClientBalance, ISNULL(u.UnsettledBalance,0) UnsettledBalance, x.PendingDebitbalance, 
					ISNULL(b.LifetimeStake,0) + ISNULL(c.LifetimeStake,0) LifetimeStake, 
					ISNULL(b.LifetimePayout,0) + ISNULL(c.LifetimePayout,0) LifetimePayout, 
					CASE WHEN c.FirstCreditId IS NULL THEN x.FirstCreditDate ELSE c.FirstCreditDate END FirstCreditDate, 
					CASE WHEN x.LastCreditId IS NULL THEN c.LastCreditDate ELSE x.LastCreditDate END LastCreditDate, 
					ISNULL(x.LifetimeCredit,0) + ISNULL(c.LifetimeCredit,0) LifetimeCredit, 
					ISNULL(c.FirstDebitDate, x.FirstDebitDate) FirstDebitDate, 
					ISNULL(x.LastDebitDate, c.LastDebitdate) LastDebitDate, 
					ISNULL(x.LifetimeDebit,0) + ISNULL(c.LifetimeDebit,0) LifetimeDebit, 
					ISNULL(c.FirstBetId,b.FirstBetId) FirstBetId, 
					CASE WHEN c.FirstBetId IS NULL THEN b.FirstBetDate ELSE c.FirstBetDate END FirstBetDate, 
					CASE WHEN c.FirstBetId IS NULL THEN b.FirstBetType ELSE c.FirstBetType END FirstBetType, 
					CASE WHEN c.FirstBetId IS NULL THEN b.FirstChannel ELSE c.FirstChannel END FirstChannel, 
					CASE WHEN c.FirstBetId IS NULL THEN b.FirstEventId ELSE c.FirstEventId END FirstEventId, 
					CASE WHEN c.FirstBetId IS NULL THEN b.FirstClassId ELSE c.FirstClassId END FirstClassId, 
					ISNULL(b.LastBetId,c.LastBetId) LastBetId, 
					CASE WHEN b.LastBetId IS NULL THEN c.LastBetDate ELSE b.LastBetDate END LastBetDate, 
					CASE WHEN b.LastBetId IS NULL THEN c.LastBetType ELSE b.LastBetType END LastBetType, 
					CASE WHEN b.LastBetId IS NULL THEN c.LastChannel ELSE b.LastChannel END LastChannel, 
					CASE WHEN b.LastBetId IS NULL THEN c.LastEventId ELSE b.LastEventId END LastEventId, 
					CASE WHEN b.LastBetId IS NULL THEN c.LastClassId ELSE b.LastClassId END LastClassId, 
					ISNULL(b.BetsPlaced,0) + ISNULL(c.LifetimeBetsPlaced,0) LifetimeBetsPlaced,
					CASE
						WHEN c.FirstBetId IS NOT NULL THEN c.FirstWinPlace 
						WHEN b.FirstAmountToPlace > 0 and b.FirstAmountToWin = 0 THEN 2 
						WHEN b.FirstAmountToWin > 0 and b.FirstAmountToPlace = 0 THEN 1
						WHEN b.FirstAmountToWin > 0 and b.FirstAmountToPlace > 0 THEN 3
						ELSE NULL END FirstWinPlace,
					CASE
						WHEN b.LastBetId IS NULL THEN c.LastWinPlace 
						WHEN b.LastAmountToPlace > 0 and b.LastAmountToWin = 0 THEN 2
						WHEN b.LastAmountToWin > 0 and b.LastAmountToPlace = 0 THEN 1
						WHEN b.LastAmountToWin > 0 and b.LastAmountToPlace > 0 THEN 3
						ELSE NULL END LastWinPlace,
					CASE WHEN c.FirstBetId IS NULL THEN b.FirstBetGroupType ELSE c.FirstBetGroupType END FirstBetGroupType, 
					CASE WHEN b.LastBetId IS NULL THEN c.LastBetGroupType ELSE b.LastBetGroupType END LastBetGroupType, 
					ISNULL(c.FirstCreditId, x.FirstCreditId) FirstCreditId, 
					ISNULL(x.LastCreditId, c.LastCreditId) LastCreditId
			FROM	(	SELECT  a.BatchKey, a.DayDate, a.AccountId, 0 FreeBet, a.FromDate, a.AccountEnabled, a.Status, 
								a.ClientBalance, ISNULL(t.PendingDebitBalance,0) PendingDebitBalance, 
								t.FirstCreditDate, t.LastCreditDate, ISNULL(t.LifetimeCredit,0) LifetimeCredit, 
								t.FirstDebitDate, t.LastDebitDate, ISNULL(t.LifetimeDebit,0) LifetimeDebit, t.FirstCreditId, t.LastCreditId
						FROM	#AccountBalances a
								LEFT JOIN #TransactionBalances t ON (t.BatchKey = a.BatchKey AND t.DayDate = a.DayDate AND t.AccountID = a.AccountID)
						UNION ALL
						SELECT  a.BatchKey, a.DayDate, a.AccountId, 1 FreeBet, a.FromDate, a.AccountEnabled, a.Status, 
								ISNULL(f.ClientBalance,0) ClientBalance, 0 PendingDebitbalance, 
								f.FirstCreditDate, f.LastCreditDate, ISNULL(f.LifetimeCredit,0) LifetimeCredit, 
								f.FirstDebitDate, f.LastDebitDate, ISNULL(f.LifetimeDebit,0) LifetimeDebit, null, null
						FROM	#AccountBalances a
								LEFT JOIN #BonusBalances f ON (f.BatchKey = a.BatchKey AND f.DayDate = a.DayDate AND f.AccountID = a.AccountID)
					) x
					LEFT JOIN #BetBalancesFirstLast b ON (b.BatchKey = x.BatchKey AND b.DayDate = x.DayDate AND b.AccountID = x.AccountID AND b.FreeBet = x.FreeBet)
					LEFT JOIN #UnsettledBalances u ON (u.BatchKey = x.BatchKey AND u.DayDate = x.DayDate AND u.AccountID = x.AccountID AND u.FreeBet = x.FreeBet)
					LEFT JOIN Centrebet.Balance c ON (c.LedgerId = x.AccountId AND c.FreeBet = x.FreeBet)
			OPTION (RECOMPILE)

			SET @RowCount = @@ROWCOUNT
			SET @RowsProcessed = @RowsProcessed + @RowCount

			DROP TABLE #AccountBalances
			DROP TABLE #TransactionBalances
			DROP TABLE #BonusBalances
			DROP TABLE #BetBalancesFirstLast
			DROP TABLE #UnsettledBalances
		END

	EXEC [Control].Sp_Log	@BatchKey,'Sp_AtomicOpeningBalances',NULL,'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log	@BatchKey, 'Sp_AtomicOpeningBalances', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH


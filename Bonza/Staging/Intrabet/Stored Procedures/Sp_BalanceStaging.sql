CREATE PROC [Intrabet].[Sp_BalanceStaging]
(
	@BatchKey	int
)
WITH RECOMPILE
AS
BEGIN TRY

	/*
	AJ 07/07: We need to use try/catch blocks to trap errors properly.
	We also need to log so we can troubleshoot these errors.
	*/

--DECLARE @BatchKey INT
--Set @BatchKey = 318

	SET ANSI_NULLS ON;
	SET QUOTED_IDENTIFIER ON;
	SET NOCOUNT ON;

	EXEC [Control].Sp_Log	@BatchKey,'Sp_BalanceStaging','LoadIntoStaging','Start'


	SELECT	BatchKey,
			CASE
				WHEN BalanceType = 'Client'			   THEN 'CLI'
				WHEN BalanceType = 'UnsettledBalance'  THEN 'UNS'
				WHEN BalanceType = 'PendingWithdrawal' THEN 'PWD'
				WHEN BalanceType = 'ProfitLoss'		   THEN 'P&L'
				WHEN BalanceType = 'Intercept'		   THEN 'INT'
				WHEN BalanceType = 'PendingDeposit'	   THEN 'PDP'
				ELSE 'UNK'	/* AJ 07/07: Added this just incase */
			END AS BalanceTypeID, 
			LedgerID,
			'IAA' as LedgerSource,
			WalletID,
			OpeningBalance,
			CONVERT(MONEY,0) as TransactedAmount,
			OpeningBalance as ClosingBalance,
			FromDate,
			DayDate
	INTO #Unpivoted	
	FROM
	(
		SELECT	BatchKey,
				DayDate,
				LedgerId,
				'IAA' as LedgerSource,
				CASE
					WHEN FreeBet = 0 THEN 'C'
					WHEN Freebet = 1 THEN 'B'
					ELSE 'U'
				END as [WalletID], /* AJ  just needs wallet - 0 or 1 -- verify data maps correctly*/ 
				FromDate,
				ClientBalance as Client,
				UnsettledBalance,
				PendingDebitBalance as PendingWithdrawal,
				LifetimeStake - LifetimePayout - UnsettledBalance as ProfitLoss,
				CONVERT(MONEY,0.0) as Intercept,
				CONVERT(MONEY,0.0) as PendingDeposit
		FROM [Atomic].[OpeningBalance] a
		Where BatchKey = @BatchKey
	) a
	CROSS APPLY
	(
		Values (a.Client,'Client'), (UnsettledBalance, 'UnsettledBalance'), (PendingWithdrawal, 'PendingWithdrawal'), 
				(ProfitLoss, 'ProfitLoss'), (Intercept, 'Intercept'), (PendingDeposit, 'PendingDeposit')
	) u(OpeningBalance,BalanceType)
	
--------------------------------------------------------------------
-----------------------INSERT NEW BATCH-----------------------------
--------------------------------------------------------------------

	INSERT INTO [Stage].[Balance] WITH(TABLOCK)
	SELECT * 
	FROM
	(
	SELECT	a.BatchKey,
			a.BalanceTypeID,
			a.LedgerId,
			a.LedgerSource,
			a.WalletID,
			CONVERT(VARCHAR,a.DayDate) as MasterDayText,
			'N/A' AnalysisDayText,
			a.OpeningBalance,
			a.TransactedAmount,
			a.ClosingBalance,
			a.FromDate,
			a.DayDate
	FROM #Unpivoted a
	) UDATA

	EXEC [Control].Sp_Log @BatchKey, 'Sp_BalanceStaging', NULL, 'Success'

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_BalanceStaging', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

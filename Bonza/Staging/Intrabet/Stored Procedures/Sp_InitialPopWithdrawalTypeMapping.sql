Create Procedure Intrabet.Sp_InitialPopWithdrawalTypeMapping

as

IF not exists (Select [WithdrawType] From [Intrabet].[WithdrawalTypeMapping])
Begin
Begin transaction
INSERT [Intrabet].[WithdrawalTypeMapping] ([WithdrawType], [TransactionType], [TransactionStatus], [TransactionMethod], [TransactionDetailId], [BalanceName]) VALUES (1, N'Withdrawal', N'Request', N'Cheque', N'WRQ+', N'Pending Withdrawal')
INSERT [Intrabet].[WithdrawalTypeMapping] ([WithdrawType], [TransactionType], [TransactionStatus], [TransactionMethod], [TransactionDetailId], [BalanceName]) VALUES (1, N'Withdrawal', N'Request', N'Cheque', N'WRQ-', N'Client')
INSERT [Intrabet].[WithdrawalTypeMapping] ([WithdrawType], [TransactionType], [TransactionStatus], [TransactionMethod], [TransactionDetailId], [BalanceName]) VALUES (2, N'Withdrawal', N'Request', N'EFT', N'WRE+', N'Pending Withdrawal')
INSERT [Intrabet].[WithdrawalTypeMapping] ([WithdrawType], [TransactionType], [TransactionStatus], [TransactionMethod], [TransactionDetailId], [BalanceName]) VALUES (2, N'Withdrawal', N'Request', N'EFT', N'WRE-', N'Client')
INSERT [Intrabet].[WithdrawalTypeMapping] ([WithdrawType], [TransactionType], [TransactionStatus], [TransactionMethod], [TransactionDetailId], [BalanceName]) VALUES (3, N'Withdrawal', N'Request', N'BPay', N'WRB+', N'Pending Withdrawal')
INSERT [Intrabet].[WithdrawalTypeMapping] ([WithdrawType], [TransactionType], [TransactionStatus], [TransactionMethod], [TransactionDetailId], [BalanceName]) VALUES (3, N'Withdrawal', N'Request', N'BPay', N'WRB-', N'Client')
INSERT [Intrabet].[WithdrawalTypeMapping] ([WithdrawType], [TransactionType], [TransactionStatus], [TransactionMethod], [TransactionDetailId], [BalanceName]) VALUES (4, N'Withdrawal', N'Request', N'PayPal', N'WRL+', N'Pending Withdrawal')
INSERT [Intrabet].[WithdrawalTypeMapping] ([WithdrawType], [TransactionType], [TransactionStatus], [TransactionMethod], [TransactionDetailId], [BalanceName]) VALUES (4, N'Withdrawal', N'Request', N'PayPal', N'WRL-', N'Client')
INSERT [Intrabet].[WithdrawalTypeMapping] ([WithdrawType], [TransactionType], [TransactionStatus], [TransactionMethod], [TransactionDetailId], [BalanceName]) VALUES (5, N'Withdrawal', N'Request', N'Moneybookers', N'WRS+', N'Pending Withdrawal')
INSERT [Intrabet].[WithdrawalTypeMapping] ([WithdrawType], [TransactionType], [TransactionStatus], [TransactionMethod], [TransactionDetailId], [BalanceName]) VALUES (5, N'Withdrawal', N'Request', N'Moneybookers', N'WRS-', N'Client')
INSERT [Intrabet].[WithdrawalTypeMapping] ([WithdrawType], [TransactionType], [TransactionStatus], [TransactionMethod], [TransactionDetailId], [BalanceName]) VALUES (6, N'Withdrawal', N'Request', N'Neteller', N'WRN+', N'Pending Withdrawal')
INSERT [Intrabet].[WithdrawalTypeMapping] ([WithdrawType], [TransactionType], [TransactionStatus], [TransactionMethod], [TransactionDetailId], [BalanceName]) VALUES (6, N'Withdrawal', N'Request', N'Neteller', N'WRN-', N'Client')
If @@ERROR = 0
Begin
	Commit transaction
End
Else
Begin
	Rollback transaction
End
End

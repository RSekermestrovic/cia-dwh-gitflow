CREATE PROC [Intrabet].[Sp_Instrument_EFT]
(
	@BatchKey	int,
	@FromDate	datetime2(3),
	@ToDate		datetime2(3),
	@RowsProcessed	int = 0 OUTPUT
)
AS
BEGIN
	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
	DECLARE @Reload varchar(6) = CASE @FromDate WHEN '1900-01-01' THEN 'RELOAD' ELSE NULL END
	DECLARE @RowCount int
	DECLARE @i int
	
	SET TRANSACTION ISOLATION LEVEL SNAPSHOT;

BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------ Initialize Temp Tables ---------------------
	IF OBJECT_ID('TempDB..#Candidates','U') IS NOT NULL BEGIN DROP TABLE #Candidates END;

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Get EFTs','Start', @Reload

	BEGIN TRANSACTION Instrument;
	
	SELECT ID,trnType,TimestampPPL,MAX(fromDate) fromDate  
	INTO #Candidates
	FROM (
			----------- tblClientBankDetails ------------------
			SELECT	 ID,trnType,fromDate,TimestampPPL
			FROM (	
					SELECT BankID AS ID,'EFT' trnType,fromDate,NULL TimestampPPL,ClientID,BankBSB,BankName,AccountNumber,Status,AccountName 
					FROM [$(Acquisition)].Intrabet.tblClientBankDetails 
					WHERE	FromDate > @FromDate AND FromDate <= @ToDate AND ToDate > @FromDate and BankID is not null		
			UNION ALL
					SELECT BankID AS ID,'EFT' trnType,ToDate,NULL TimestampPPL,ClientID,BankBSB,BankName,AccountNumber,Status,AccountName 
					FROM [$(Acquisition)].Intrabet.tblClientBankDetails 
					WHERE	ToDate > @FromDate AND ToDate <= @ToDate and BankID is not null
			) x
			GROUP BY ID,trnType,fromDate,TimestampPPL,ClientID,BankBSB,BankName,AccountNumber,Status,AccountName 
			HAVING COUNT(*) <> 2

		 ) x
	GROUP BY ID,trnType,TimestampPPL
	OPTION (RECOMPILE)
	
	EXEC [Control].Sp_Log	@BatchKey,@Me,'InsertIntoStaging','Start', @Reload, @RowsProcessed = @@ROWCOUNT

	INSERT	into Stage.Instrument (BatchKey,InstrumentId,Source,AccountNumber,SafeAccountNumber,AccountName,BSB,ExpiryDate,Verified,VerifyAmount,InstrumentType,ProviderName,FromDate)
	SELECT	
			@BatchKey																			AS BatchKey, 
			C.id																				AS instrumentID,
			ISNULL(C.TrnType,'Unknown')															AS Source, 
			ISNULL(BD.AccountNumber,'Unknown')													AS AccountNumber,
			ISNULL(BD.AccountNumber,'Unknown')													AS SafeAccountNumber,
			ISNULL(BD.AccountName,'Unknown')													AS AccountName,		
			ISNULL(bd.BankBsb,'Unknown')														AS BSB,
			NULL																				AS ExpiryDate,
			'N/A'																				AS Verified,
			NULL																				AS VerifiedAmount,
			ISNULL(C.TrnType,'Unknown')															AS InstrumentType,
			ISNULL(bd.BankName,'Unknown')														AS ProviderName,
			c.FromDate																			AS FromDate	
				
	FROM #Candidates C  with (nolock)
	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblClientBankDetails BD WITH (NOLOCK) ON 					
					C.ID =Cast(Bd.BankID as varchar)
					AND BD.FromDate <= c.FromDate AND BD.ToDate > C.FromDate
						
	OPTION (RECOMPILE, FORCE ORDER, LOOP JOIN)

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

						
	COMMIT TRANSACTION Instrument;

	EXEC [Control].Sp_Log @BatchKey, @Me, NULL, 'Success', @RowsProcessed = @RowsProcessed

	UPDATE [Control].[ConfigurationItems] Set Value = 'N' Where ConfigurationItem = 'Intrabet.Sp_Instrument_EFT'

END TRY
BEGIN CATCH

	ROLLBACK TRANSACTION Instrument;
	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Instrument_EFT', NULL, 'Failed', @ErrorMessage;
	RAISERROR(@ErrorMessage,16,1);
	THROW;

END CATCH
																					   																					
END

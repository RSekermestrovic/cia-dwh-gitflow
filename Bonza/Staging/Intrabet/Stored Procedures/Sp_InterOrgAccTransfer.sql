﻿CREATE PROC [Intrabet].[Sp_InterOrgAccTransfer]
(
	@BatchKey	INT,
	@FromDate	DATETIME2(3),
	@ToDate		DATETIME2(3),
	@RowsProcessed	int = 0 OUTPUT
)
AS
BEGIN
 
	SET TRANSACTION ISOLATION LEVEL SNAPSHOT

	DECLARE @RowCount INT;

BEGIN TRY

	BEGIN TRY DROP TABLE #CompanyXrfDebit END TRY BEGIN CATCH END CATCH
	BEGIN TRY DROP TABLE #CompanyXrfCredit END TRY BEGIN CATCH END CATCH

	EXEC [Control].Sp_Log	@BatchKey,'Sp_InterOrgAccTransfer','DebitTransactions','Start'

	SELECT 'Debit' AS Type,A.AccountID,A.Balance,C.FromDate,C1.ClientID,C1.OrgID 
	INTO #CompanyXrfDebit
	FROM [$(Acquisition)].IntraBet.tblClients C
	INNER LOOP JOIN [$(Acquisition)].IntraBet.tblClients C1	ON C.ClientID=C1.ClientID AND C.FromDate=C1.ToDate AND C.OrgID<>C1.OrgID				-- Check if this is a debit Transfer
	INNER LOOP JOIN [$(Acquisition)].IntraBet.tblAccounts A   ON C1.ClientID=A.ClientID AND A.FromDate<=C1.FromDate AND A.ToDate>C1.FromDate			-- Get Account Details of the Debit Transaction for the last active version of the debit transaction
	WHERE C.FromDate >= @FromDate AND C.FromDate < @ToDate AND c.ToDate > @FromDate
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,'Sp_InterOrgAccTransfer','CreditTransactions','Start', @RowsProcessed = @@ROWCOUNT

	SELECT 'Credit' AS Type,A.AccountID,A.Balance,C.FromDate,C.ClientID,C.OrgID 
	INTO #CompanyXrfCredit
	FROM [$(Acquisition)].IntraBet.tblClients C
	INNER LOOP JOIN [$(Acquisition)].IntraBet.tblClients C1	ON C.ClientID=C1.ClientID AND C.FromDate=C1.ToDate AND C.OrgID<>C1.OrgID				-- Check for any changes in OrgID compared to previous record
	INNER LOOP JOIN [$(Acquisition)].IntraBet.tblAccounts A   ON C.ClientID=A.ClientID AND A.FromDate<=C.FromDate AND A.ToDate>C.FromDate				-- Get Account Details at the moment the Transfer happened
	WHERE C.FromDate >= @FromDate AND C.FromDate < @ToDate AND c.ToDate > @FromDate 
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,'Sp_InterOrgAccTransfer','InsertIntoStaging','Start', @RowsProcessed = @@ROWCOUNT


	INSERT INTO Stage.[Transaction] (BatchKey,TransactionId,TransactionIdSource,BalanceTypeID,TransactionTypeID,TransactionStatusID,TransactionMethodID,TransactionDirection,BetGroupID,BetGroupIDSource,BetId,BetIdSource,LegId,LegIdSource,FreeBetID,
	ExternalID,External1,External2,UserID,UserIDSource,InstrumentID,InstrumentIDSource,LedgerID,LedgerSource,WalletId,PromotionId,PromotionIDSource,MasterTransactionTimestamp,MasterDayText,MasterTimeText,Channel, ActionChannel,CampaignId,TransactedAmount,BetGroupType,
	BetTypeName,BetSubTypeName,LegBetTypeName,GroupingType,PriceTypeId,PriceTypeSource,ClassId,ClassIdSource,EventID,EventIDSource,MarketID,MarketIdSource,LegNumber,NumberOfLegs,ExistsInDim,Conditional,MappingType,MappingId,FromDate,DateTimeUpdated,RequestID,WithdrawID,
	InPlay,ClickToCall,CashoutType,IsDoubleDown,IsDoubledDown)
	SELECT 
			@BatchKey																AS BatchKey
		,ISNULL(DR.AccountId,-1)												AS TransactionID
		,'IBC'																	AS TransactionIDSource
		,'CLI'																	AS BalanceTypeID
		,'TR'																	AS TransactionTypeID
		,'CO'																	AS TransactionStatusID
		,'MI'																	AS TransactionMethodID
		,'DR'																	AS TransactionDirection
		,ISNULL(DR.AccountId,-1)												AS BetGroupID
		,'IBT'																	AS BetGroupIDSource
		,ISNULL(DR.AccountId,-1)												AS BetId
		,'IBT'																	AS BetIdSource
		,ISNULL(DR.AccountId,-1)												AS LegId
		,'IBT'																	AS LegIdSource
		,0																		AS FreeBetID
		,'N/A'																	AS ExternalID
		,'N/A'																	AS External1
		,'N/A'																	AS External2
		,0																		AS UserID
		,'IUU'																	AS UserIDSource
		,0																		AS InstrumentID
		,'N/A'																	AS InstrumentIDSource
		,ISNULL(DR.AccountId,-1)												AS LedgerID
		,CASE WHEN DR.AccountId=0 THEN 'N/A' ELSE 'IAA' END						AS LedgerSource
		,'C'																	AS WalletId
		,0																		AS PromotionId
		,'N/A'																	AS PromotionIDSource
		,ISNULL(LT.CorrectedDate,CR.FromDate)									AS MasterTransactionTimestamp
		,CONVERT(VARCHAR(11),CONVERT(DATE,ISNULL(LT.CorrectedDate,CR.FromDate))) AS MasterDayText
		,CONVERT(VARCHAR(5),CONVERT(TIME,ISNULL(LT.CorrectedDate,CR.FromDate)))	 AS MasterTimeText
		,0																		AS Channel
		,0																		AS ActionChannel
		,0																		AS CampaignId
		,CR.Balance*(-1)														AS TransactedAmount
		,'Straight'																AS BetGroupType
		,'N/A'																	AS BetTypeName
		,'N/A'																	AS BetSubTypeName
		,'N/A'																	AS LegBetTypeName
		,'Straight'																AS GroupingType
		,0																		AS PriceTypeID /* AJ - PriceType Change */
		,'N/A'																	AS PriceTypeSource /* AJ - PriceType Change */
		,0																		AS ClassID
		,'IBT'																	AS ClassIDSource
		,0																		AS EventID
		,'N/A'																	AS EventIDSource
		,0																		AS MarketID /* AJ - MarketId Change */
		,'N/A'																	AS MarketIDSource /* AJ - MarketId Change */
		,1																		AS LegNumber
		,1																		AS NumberOfLegs
		,0																		AS ExistsInDim
		,0																		AS Conditional
		,1																		AS MappingType
		,1																		AS MappingID
		,CR.FromDate															AS FromDate
		,0																		AS DateTimeUpdated
		,NULL																	AS RequestID
		,NULL																	AS WithdrawID
		,'N/A'																	AS InPlay
		,'N/A'																	AS ClickToCall
		,0																		AS CashoutType
		,0																		AS IsDoubleDown
		,0																		AS IsDoubledDown
	FROM #CompanyXrfDebit DR
	INNER JOIN #CompanyXrfCredit CR									ON DR.AccountID=CR.AccountID
	LEFT JOIN [$(Acquisition)].IntraBet.Latency LT					ON CR.FromDate=LT.OriginalDate

	UNION ALL

	SELECT 
			@BatchKey																AS BatchKey
		,ISNULL(DR.AccountId,-1)												AS TransactionID
		,'IBC'																	AS TransactionIDSource
		,'CLI'																	AS BalanceTypeID
		,'TR'																	AS TransactionTypeID
		,'CO'																	AS TransactionStatusID
		,'MI'																	AS TransactionMethodID
		,'CR'																	AS TransactionDirection
		,ISNULL(DR.AccountId,-1)												AS BetGroupID
		,'IBT'																	AS BetGroupIDSource
		,ISNULL(DR.AccountId,-1)												AS BetId
		,'IBT'																	AS BetIdSource
		,ISNULL(DR.AccountId,-1)												AS LegId
		,'IBT'																	AS LegIdSource
		,0																		AS FreeBetID
		,'N/A'																	AS ExternalID
		,'N/A'																	AS External1
		,'N/A'																	AS External2
		,0																		AS UserID
		,'IUU'																	AS UserIDSource
		,0																		AS InstrumentID
		,'N/A'																	AS InstrumentIDSource
		,ISNULL(CR.AccountId,-1)												AS LedgerID
		,CASE WHEN CR.AccountId=0 THEN 'N/A' ELSE 'IAA' END						AS LedgerSource
		,'C'																	AS WalletId
		,0																		AS PromotionId
		,'N/A'																	AS PromotionIDSource
		,ISNULL(LT.CorrectedDate,CR.FromDate)									AS MasterTransactionTimestamp
		,CONVERT(VARCHAR(11),CONVERT(DATE,ISNULL(LT.CorrectedDate,CR.FromDate))) AS MasterDayText
		,CONVERT(VARCHAR(5),CONVERT(TIME,ISNULL(LT.CorrectedDate,CR.FromDate)))  AS MasterTimeText
		,0																		AS Channel
		,0																		AS ActionChannel
		,0																		AS CampaignId
		,CR.Balance																AS TransactedAmount
		,'Straight'																AS BetGroupType
		,'N/A'																	AS BetTypeName
		,'N/A'																	AS BetSubTypeName
		,'N/A'																	AS LegBetTypeName
		,'Straight'																AS GroupingType
		,0																		AS PriceTypeID /* AJ - PriceType Change */
		,'N/A'																	AS PriceTypeSource /* AJ - PriceType Change */
		,0																		AS ClassID
		,'IBT'																	AS ClassIDSource
		,0																		AS EventID
		,'N/A'																	AS EventIDSource
		,0																		AS MarketID /* AJ - MarketId Change */
		,'N/A'																	AS MarketIDSource /* AJ - MarketId Change */
		,1																		AS LegNumber
		,1																		AS NumberOfLegs
		,0																		AS ExistsInDim
		,0																		AS Conditional
		,1																		AS MappingType
		,1																		AS MappingID
		,CR.FromDate															AS FromDate
		,0																		AS DateTimeUpdated
		,NULL																	AS RequestID
		,NULL																	AS WithdrawID
		,'N/A'																	AS InPlay
		,'N/A'																	AS ClickToCall
		,0																		AS CashoutType
		,0																		AS IsDoubleDown
		,0																		AS IsDoubledDown
	FROM #CompanyXrfDebit DR
	INNER HASH JOIN #CompanyXrfCredit CR									ON DR.AccountID=CR.AccountID
	LEFT LOOP JOIN [$(Acquisition)].IntraBet.Latency LT					ON CR.FromDate=LT.OriginalDate
	OPTION (RECOMPILE)
		
	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey, 'Sp_InterOrgAccTransfer', NULL, 'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_InterOrgAccTransfer', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
END


﻿CREATE PROC [Intrabet].[Sp_TransactionBets]
(
	@BatchKey		int,
	@RowsProcessed	int = 0 OUTPUT
)
AS 
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED -- clear of Acquistion layer so don't need SNAPSHOT isolation
 
BEGIN TRY

	SET ANSI_NULLS ON;
	SET QUOTED_IDENTIFIER ON;
	SET NOCOUNT ON;

	DECLARE @Trued char(1) = 'Y'

	DECLARE @RowCount INT;

	---------------------------------------------------------------------------
	--- PART 1 - POPULATE CANDIDATE TABLE OF BETS TO BE LOADED INTO STAGING ---
	---------------------------------------------------------------------------

	EXEC [Control].Sp_Log @BatchKey, 'Sp_TransactionBets', 'LoadIntoStaging', 'Start';

	SET @Trued = CASE Control.ParameterValue('Bet','TruedEqualShare') WHEN 'No' THEN 'N' ELSE 'Y' END

	INSERT INTO Stage.[Transaction] (BatchKey,TransactionId,TransactionIdSource,BalanceTypeID,TransactionTypeID,TransactionStatusID,TransactionMethodID,TransactionDirection,BetGroupID,BetGroupIDSource,BetId,BetIdSource,LegId,LegIdSource,FreeBetID,ExternalID,External1,
												External2,UserID,UserIDSource,InstrumentID,InstrumentIDSource,LedgerID,LedgerSource,WalletId,PromotionId,PromotionIDSource,MasterTransactionTimestamp,MasterDayText,MasterTimeText,Channel,ActionChannel,
												CampaignId,TransactedAmount,BetGroupType,BetTypeName,BetSubTypeName,LegBetTypeName,GroupingType,PriceTypeId,PriceTypeSource,ClassId,ClassIdSource,EventID,EventIDSource,MarketID,MarketIdSource,
												LegNumber,NumberOfLegs,ExistsInDim,Conditional,MappingType,MappingId,FromDate,DateTimeUpdated,RequestID,WithdrawID,ClickToCall,InPlay,CashoutType,IsDoubleDown,IsDoubledDown)
	SELECT	a.BatchKey,
			a.TransactionId,
			a.TransactionIdSource,
			z.BalanceTypeID,					-- Changed from BalanceName on 6Mar15
			z.TransactionTypeID,
			z.TransactionStatusID,
			z.TransactionMethodID,
			z.TransactionDirection,
			CASE WHEN LegacyBetId > 700000000000000000 THEN CONVERT(bigint,SUBSTRING(CONVERT(varchar,LegacyBetId),7,10)) ELSE a.BetGroupID END BetGroupID,
			CASE WHEN LegacyBetId > 700000000000000000 THEN 'GTT' ELSE a.BetGroupIDSource END BetGroupIDSource,
			CASE WHEN LegacyBetId > 700000000000000000 THEN CONVERT(bigint,SUBSTRING(CONVERT(varchar,LegacyBetId),7,10)) ELSE a.BetId END BetId,
			CASE WHEN LegacyBetId > 700000000000000000 THEN 'GTT' ELSE a.BetIdSource END BetIdSource,
			a.LegId,
			a.LegIdSource,
			a.FreeBetID,
			CASE WHEN LegacyBetId > 700000000000000000 THEN a.BetId ELSE NULL END ExternalID,
			NULL External1,
			NULL External2,
			a.UserID,
			a.UserIDSource,
			0 InstrumentID,
			'N/A' InstrumentIDSource,
			a.LedgerID,
			a.LedgerSource,

			-- WalletID, PromotionID

			CASE
				WHEN (a.WalletId = 'B' AND z.WalletSign = -1) THEN 'C'
				WHEN (a.WalletId = 'C' AND z.WalletSign = -1) THEN 'B'
				WHEN (z.WalletSign = 0) THEN 'C'
				ELSE a.WalletId
			END WalletId,
			a.PromotionId,
			a.PromotionIdSource, 
			
			a.MasterTransactionTimestamp,
			CONVERT(VARCHAR(11),CONVERT(DATE,a.MasterTransactionTimestamp)) AS MasterDayText, -- Are these still needed with time zones?
			CONVERT(VARCHAR(5),CONVERT(TIME,a.MasterTransactionTimestamp)) AS MasterTimeText, -- Are these still needed with time zones?
			a.Channel,
			a.ActionChannel,
			a.CampaignId,
			CASE WHEN a.BetType = 'Exotic' THEN a.BetStake WHEN @Trued = 'Y' THEN a.LegStakeTrued ELSE a.LegStake END * z.StakeDirection -- Transactions only include one line for exotics, even if the exotic is technically multi-leg - e.g. Quadrella - so only use the Bet Stake/Payout and just take the first leg of exotics (below)
			+	CASE 
					WHEN a.BetType = 'Exotic'THEN CASE WHEN a.FreeBetId <> 0 AND a.BetStake > a.BetPayout THEN a.BetStake ELSE a.BetPayout END	-- If the winnings on a free bet are less than the stake, there will be nothing to payout to client, 
					ELSE CASE WHEN a.FreeBetId <> 0 AND a.LegStake > a.LegPayout THEN CASE @Trued WHEN 'Y' THEN a.LegStakeTrued ELSE a.LegStake END ELSE CASE @Trued WHEN 'Y' THEN a.LegPayoutTrued ELSE a.LegPayout END END								-- but need to force payout equal to stake to ensure we don't claim more money from client as a reversed win
				END * z.PayoutDirection TransactedAmount,
			a.BetGroupType,
			a.BetType,
			a.BetSubType,
			a.LegType,
			a.GroupingType,
			--PriceType
			a.PriceTypeID,
			a.PriceTypeSource,
			a.ClassId,
			'IBT' AS ClassIdSource,
			a.EventID,
			a.EventIdSource,
			--MarketID
			a.MarketID,
			a.MarketIdSource,
			a.LegNumber,
			a.NumberOfLegs,
			0 ExistsInDim,
			0 Conditional,
			0 MappingType,
			a.MappingId,
			a.FromDate,
			NULL DateTimeUpdated,
			NULL RequestID,
			NULL WithdrawID,
			--CASE WHEN ISNULL(d.UserCode,ISNULL(tl.UserCode,ISNULL(B.UserCode,-4))) = 100144 THEN 'Y' ELSE 'N' END AS ClickToCall,
			--CASE WHEN ctc.BetID is not null THEN 'Y' ELSE 'N' END AS ClickToCall,
			--CASE WHEN a.MasterTransactionTimestamp >= ISNULL(RC.Cutoff,'9999-12-31') THEN 'Y' ELSE 'N' END AS InPlay
			a.ClickToCall,
			a.InPlay,
			a.CashoutType,
			a.IsDoubleDown,
			a.IsDoubledDown
	FROM	Atomic.Bet a
			INNER HASH JOIN	(	SELECT 'Request' Action, 3 FreeBet, 'BRI-' TransactionDetailId, -1 StakeDirection, 0 PayoutDirection, 'CLI' BalanceTypeID, +1 WalletSign, 'BT' TransactionTypeID, 'RE' TransactionStatusID, 'SY' TransactionMethodID, 'DR' TransactionDirection UNION ALL	-- Changed from BalanceName on 6Mar15 (All below)
								SELECT 'Request' Action, 3 FreeBet, 'BRI+' TransactionDetailId, +1 StakeDirection, 0 PayoutDirection, 'INT' BalanceTypeID, +1 WalletSign, 'BT' TransactionTypeID, 'RE' TransactionStatusID, 'SY' TransactionMethodID, 'CR' TransactionDirection UNION ALL
								SELECT 'Decline' Action, 3 FreeBet, 'BDU-' TransactionDetailId, -1 StakeDirection, 0 PayoutDirection, 'INT' BalanceTypeID, +1 WalletSign, 'BT' TransactionTypeID, 'DE' TransactionStatusID, 'UK' TransactionMethodID, 'DR' TransactionDirection UNION ALL
								SELECT 'Decline' Action, 3 FreeBet, 'BDU+' TransactionDetailId, +1 StakeDirection, 0 PayoutDirection, 'CLI' BalanceTypeID, +1 WalletSign, 'BT' TransactionTypeID, 'DE' TransactionStatusID, 'UK' TransactionMethodID, 'CR' TransactionDirection UNION ALL
								SELECT 'Accept' Action, 3 FreeBet, 'BAI-' TransactionDetailId, -1 StakeDirection, 0 PayoutDirection, 'INT' BalanceTypeID, +1 WalletSign, 'BT' TransactionTypeID, 'AC' TransactionStatusID, 'SY' TransactionMethodID, 'DR' TransactionDirection UNION ALL
								SELECT 'Accept' Action, 3 FreeBet, 'BAI+' TransactionDetailId, +1 StakeDirection, 0 PayoutDirection, 'UNS' BalanceTypeID, +1 WalletSign, 'BT' TransactionTypeID, 'AC' TransactionStatusID, 'SY' TransactionMethodID, 'CR' TransactionDirection UNION ALL
								SELECT 'Cancel' Action, 3 FreeBet, 'BCI-' TransactionDetailId, -1 StakeDirection, 0 PayoutDirection, 'UNS' BalanceTypeID, +1 WalletSign, 'BT' TransactionTypeID, 'CN' TransactionStatusID, 'VO' TransactionMethodID, 'DR' TransactionDirection UNION ALL
								SELECT 'Cancel' Action, 3 FreeBet, 'BCI+' TransactionDetailId, +1 StakeDirection, 0 PayoutDirection, 'CLI' BalanceTypeID, +1 WalletSign, 'BT' TransactionTypeID, 'CN' TransactionStatusID, 'VO' TransactionMethodID, 'CR' TransactionDirection UNION ALL
								SELECT 'Loss' Action, 3 FreeBet, 'BLI-' TransactionDetailId, -1 StakeDirection, 0 PayoutDirection, 'UNS' BalanceTypeID, +1 WalletSign, 'BT' TransactionTypeID, 'LO' TransactionStatusID, 'SE' TransactionMethodID, 'DR' TransactionDirection UNION ALL
								SELECT 'Loss' Action, 3 FreeBet, 'BLI+' TransactionDetailId, +1 StakeDirection, 0 PayoutDirection, 'P&L' BalanceTypeID, +1 WalletSign, 'BT' TransactionTypeID, 'LO' TransactionStatusID, 'SE' TransactionMethodID, 'CR' TransactionDirection UNION ALL
								SELECT 'Win' Action, 3 FreeBet, 'BWI-' TransactionDetailId, -1 StakeDirection, 0 PayoutDirection, 'UNS' BalanceTypeID, +1 WalletSign, 'BT' TransactionTypeID, 'WN' TransactionStatusID, 'SE' TransactionMethodID, 'DR' TransactionDirection UNION ALL		-- (BOTH) subtract stake from Hold
								SELECT 'Win' Action, 1 FreeBet, 'BWI-' TransactionDetailId, +1 StakeDirection, -1 PayoutDirection, 'P&L' BalanceTypeID, +1 WalletSign, 'BT' TransactionTypeID, 'WN' TransactionStatusID, 'SE' TransactionMethodID, 'DR' TransactionDirection UNION ALL		-- (CASH) subtract payout less stake from P&L
								SELECT 'Win' Action, 1 FreeBet, 'BWI+' TransactionDetailId, 0 StakeDirection, +1 PayoutDirection, 'CLI' BalanceTypeID, +1 WalletSign, 'BT' TransactionTypeID, 'WN' TransactionStatusID, 'SE' TransactionMethodID, 'CR' TransactionDirection UNION ALL		-- (CASH) add payout to Client
								SELECT 'Win' Action, 2 FreeBet, 'BWI+' TransactionDetailId, +1 StakeDirection, 0 PayoutDirection, 'P&L' BalanceTypeID, +1 WalletSign, 'BT' TransactionTypeID, 'WN' TransactionStatusID, 'SE' TransactionMethodID, 'CR' TransactionDirection UNION ALL		-- (FREE) add stake to P&L
								SELECT 'Win' Action, 2 FreeBet, 'BWI-' TransactionDetailId, +1 StakeDirection, -1 PayoutDirection, 'P&L' BalanceTypeID, -1 WalletSign, 'BT' TransactionTypeID, 'WN' TransactionStatusID, 'SE' TransactionMethodID, 'DR' TransactionDirection UNION ALL		-- (FREE) subtract payout less stake from P&L
								SELECT 'Win' Action, 2 FreeBet, 'BWI+' TransactionDetailId, -1 StakeDirection, +1 PayoutDirection, 'CLI' BalanceTypeID, 0 WalletSign, 'BT' TransactionTypeID, 'WN' TransactionStatusID, 'SE' TransactionMethodID, 'CR' TransactionDirection UNION ALL		-- (FREE) add payout less stake to Client
								SELECT 'Scratch' Action, 1 FreeBet, 'BCS-' TransactionDetailId, -1 StakeDirection, 0 PayoutDirection, 'UNS' BalanceTypeID, +1 WalletSign, 'BT' TransactionTypeID, 'CN' TransactionStatusID, 'SC' TransactionMethodID, 'DR' TransactionDirection UNION ALL	-- (BOTH) subtract scratched stake from hold 	
								SELECT 'Scratch' Action, 1 FreeBet, 'BCS+' TransactionDetailId, 0 StakeDirection, +1 PayoutDirection, 'CLI' BalanceTypeID, +1 WalletSign, 'BT' TransactionTypeID, 'CN' TransactionStatusID, 'SC' TransactionMethodID, 'CR' TransactionDirection UNION ALL	-- (BOTH) add scratched payout to client
								SELECT 'Cashout' Action, 3 FreeBet, 'BOI-' TransactionDetailId, -1 StakeDirection, 0 PayoutDirection, 'UNS' BalanceTypeID, +1 WalletSign, 'BT' TransactionTypeID, 'CO' TransactionStatusID, 'SE' TransactionMethodID, 'DR' TransactionDirection UNION ALL		-- (BOTH) subtract stake from Hold
								SELECT 'Cashout' Action, 1 FreeBet, 'BOI-' TransactionDetailId, +1 StakeDirection, -1 PayoutDirection, 'P&L' BalanceTypeID, +1 WalletSign, 'BT' TransactionTypeID, 'CO' TransactionStatusID, 'SE' TransactionMethodID, 'DR' TransactionDirection UNION ALL		-- (CASH) subtract payout less stake from P&L
								SELECT 'Cashout' Action, 1 FreeBet, 'BOI+' TransactionDetailId, 0 StakeDirection, +1 PayoutDirection, 'CLI' BalanceTypeID, +1 WalletSign, 'BT' TransactionTypeID, 'CO' TransactionStatusID, 'SE' TransactionMethodID, 'CR' TransactionDirection UNION ALL		-- (CASH) add payout to Client
								SELECT 'Cashout' Action, 2 FreeBet, 'BOI+' TransactionDetailId, +1 StakeDirection, 0 PayoutDirection, 'P&L' BalanceTypeID, +1 WalletSign, 'BT' TransactionTypeID, 'CO' TransactionStatusID, 'SE' TransactionMethodID, 'CR' TransactionDirection UNION ALL		-- (FREE) add stake to P&L
								SELECT 'Cashout' Action, 2 FreeBet, 'BOI-' TransactionDetailId, +1 StakeDirection, -1 PayoutDirection, 'P&L' BalanceTypeID, -1 WalletSign, 'BT' TransactionTypeID, 'CO' TransactionStatusID, 'SE' TransactionMethodID, 'DR' TransactionDirection UNION ALL		-- (FREE) subtract payout less stake from P&L
								SELECT 'Cashout' Action, 2 FreeBet, 'BOI+' TransactionDetailId, -1 StakeDirection, +1 PayoutDirection, 'CLI' BalanceTypeID, 0 WalletSign, 'BT' TransactionTypeID, 'CO' TransactionStatusID, 'SE' TransactionMethodID, 'CR' TransactionDirection	UNION ALL	-- (FREE) add payout less stake to Client
								SELECT 'Fee+' Action, 3 FreeBet, 'FPD-' TransactionDetailId, -1 StakeDirection, 0 PayoutDirection, 'CLI' BalanceTypeID, +1 WalletSign, 'FE' TransactionTypeID, 'PY' TransactionStatusID, 'DD' TransactionMethodID, 'DR' TransactionDirection UNION ALL
								SELECT 'Fee+' Action, 3 FreeBet, 'FPD+' TransactionDetailId, +1 StakeDirection, 0 PayoutDirection, 'P&L' BalanceTypeID, +1 WalletSign, 'FE' TransactionTypeID, 'PY' TransactionStatusID, 'DD' TransactionMethodID, 'CR' TransactionDirection UNION ALL
								SELECT 'Fee-' Action, 3 FreeBet, 'FRD-' TransactionDetailId, -1 StakeDirection, 0 PayoutDirection, 'P&L' BalanceTypeID, +1 WalletSign, 'FE' TransactionTypeID, 'RF' TransactionStatusID, 'DD' TransactionMethodID, 'DR' TransactionDirection UNION ALL
								SELECT 'Fee-' Action, 3 FreeBet, 'FRD+' TransactionDetailId, +1 StakeDirection, 0 PayoutDirection, 'CLI' BalanceTypeID, +1 WalletSign, 'FE' TransactionTypeID, 'RF' TransactionStatusID, 'DD' TransactionMethodID, 'CR' TransactionDirection
								--SELECT 'Scratch' Action, 1 FreeBet, 'BCS+' TransactionDetailId, +1 StakeDirection, -1 PayoutDirection, 'P&L' BalanceTypeID, +1 WalletSign, 'BT' TransactionTypeID, 'CN' TransactionStatusID, 'SC' TransactionMethodID, 'CR' TransactionDirection			-- (BOTH) add any shortfall between stake and payout to P&L (e.g. when scrathed refund is not full stake)	
						) z ON (z.Action = a.Action AND z.FreeBet & CASE a.FreeBetId WHEN 0 THEN 1 ELSE 2 END > 0)
			--LEFT OUTER JOIN #RacingCutoff RC on a.EventID = RC.EventID
			--LEFT OUTER JOIN #CTC ctc on a.BetId = ctc.BetID
	WHERE	BatchKey = @BatchKey
			AND a.PositionNumber = 1 AND a.GroupingNumber = 1	-- As the Transaction fact is staged at a bet leg grain, just fix the selection to the first position and group for each leg and retrieve leg level stakes and payments (seeabove)
			AND (a.BetType <> 'Exotic' OR a.BetId = a.LegId)	-- Transactions only include one line per exotics, even if the exotic is technically multi-leg - e.g. Quadrella - so only take the first leg of exotics and retrieve bet level stakes and payments (above)
	OPTION (RECOMPILE)

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey,'Sp_TransactionBets',NULL,'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_TransactionBets', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
END

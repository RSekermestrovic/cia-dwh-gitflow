﻿CREATE PROC [Intrabet].[Sp_Event]
(
	@FromDate	datetime2(3),
	@ToDate		datetime2(3),
	@BatchKey	int
)
WITH RECOMPILE
AS 
BEGIN
BEGIN TRY

	SET TRANSACTION ISOLATION LEVEL SNAPSHOT

	-- Speculative attempt to drop pre-existing Temp table, don't care if it fails due to non-existence
	BEGIN TRY DROP TABLE #Candidate	END TRY BEGIN CATCH	END CATCH 
	BEGIN TRY DROP TABLE #Resulted END TRY BEGIN CATCH END CATCH
	BEGIN TRY DROP TABLE #RaceInfo	END TRY BEGIN CATCH	END CATCH 
	BEGIN TRY DROP TABLE #Abandoned END TRY BEGIN CATCH END CATCH

	EXEC [Control].Sp_Log	@BatchKey,'Sp_Event','IncrementTriggerLogic','Start'

	SELECT	ISNULL(e6.EventId, e.EventId) EventId, MAX(e.FromDate) FromDate--, MAX(CASE WHEN e6.EventID IS NULL THEN Levels ELSE Levels + 1 END) Levels, MAX(ISNULL(CONVERT(varchar,e6.EventId) + '|','') + e.Heritage) Heritage
	INTO #Candidate
	FROM	(	SELECT	ISNULL(e5.EventId, e.EventId) EventId, MAX(e.FromDate) FromDate, MAX(CASE WHEN e5.EventID IS NULL THEN Levels ELSE Levels + 1 END) Levels, MAX(ISNULL(CONVERT(varchar,e5.EventId) + '|','') + e.Heritage) Heritage
				FROM	(	SELECT	ISNULL(e4.EventId, e.EventId) EventId, MAX(e.FromDate) FromDate, MAX(CASE WHEN e4.EventID IS NULL THEN Levels ELSE Levels + 1 END) Levels, MAX(ISNULL(CONVERT(varchar,e4.EventId) + '|','') + e.Heritage) Heritage
							FROM	(	SELECT	ISNULL(e3.EventId, e.EventId) EventId, MAX(e.FromDate) FromDate, MAX(CASE WHEN e3.EventID IS NULL THEN Levels ELSE Levels + 1 END) Levels, MAX(ISNULL(CONVERT(varchar,e3.EventId) + '|','') + e.Heritage) Heritage
										FROM	(	SELECT	ISNULL(e2.EventId, e.EventId) EventId, MAX(e.FromDate) FromDate, MAX(CASE WHEN e2.EventID IS NULL THEN Levels ELSE Levels + 1 END) Levels, MAX(ISNULL(CONVERT(varchar,e2.EventId) + '|','') + e.Heritage) Heritage
													FROM	(	SELECT	ISNULL(e1.EventId, e.EventId) EventId, MAX(e.FromDate) FromDate, MAX(CASE WHEN e1.EventID IS NULL THEN Levels ELSE Levels + 1 END) Levels, MAX(ISNULL(CONVERT(varchar,e1.EventId) + '|','') + e.Heritage) Heritage
																FROM	(	SELECT	EventId, MAX(FromDate) FromDate, 1 Levels, CONVERT(varchar,EventId) Heritage  
																			FROM	(	SELECT	EventId, FromDate 
																						FROM	(	SELECT	EventId, FromDate, MasterEventId, EventName, CASE IsMasterEvent WHEN 1 THEN NULL ELSE EventDate END EventDate, 
																											CASE IsMasterEvent WHEN 1 THEN NULL ELSE EventType END EventType, CASE IsMasterEvent WHEN 1 THEN NULL ELSE VenueId END VenueId,'F' Src
																											, IsAbandonedEvent, ResultsIn, RaceNumber, LiveStreamPerformId
																									FROM	[$(Acquisition)].IntraBet.tblEvents ef
																									WHERE	FromDate > @FromDate AND FromDate <= @ToDate
																											AND ToDate > @FromDate
																									UNION ALL
																									SELECT	EventId, ToDate, MasterEventId, EventName, CASE IsMasterEvent WHEN 1 THEN NULL ELSE EventDate END EventDate, 
																											CASE IsMasterEvent WHEN 1 THEN NULL ELSE EventType END EventType, CASE IsMasterEvent WHEN 1 THEN NULL ELSE VenueId END VenueId,'T' Src
																											, IsAbandonedEvent, ResultsIn, RaceNumber, LiveStreamPerformId
																									FROM	[$(Acquisition)].IntraBet.tblEvents et
																									WHERE	ToDate > @FromDate AND ToDate <= @ToDate
																								) x
																						GROUP BY EventId, FromDate, MasterEventId, EventName, EventDate, EventType, VenueId, IsAbandonedEvent, ResultsIn, RaceNumber, LiveStreamPerformId
																						HAVING COUNT(*) <> 2 and Max(Src)<>'T'
																					) y
																			GROUP BY EventId
																		) e
																		LEFT JOIN [$(Acquisition)].IntraBet.tblEvents e1  ON (e1.MasterEventID = e.EventID AND e1.EventID <> e.EventID AND e1.FromDate <= e.FromDate AND e1.ToDate > e.FromDate)
																GROUP BY ISNULL(e1.EventId, e.EventId)
															) e
															LEFT JOIN [$(Acquisition)].IntraBet.tblEvents e2  ON (e2.MasterEventID = e.EventID AND e2.EventID <> e.EventID AND e2.FromDate <= e.FromDate AND e2.ToDate > e.FromDate)
													GROUP BY ISNULL(e2.EventId, e.EventId)
												) e
												LEFT JOIN [$(Acquisition)].IntraBet.tblEvents e3  ON (e3.MasterEventID = e.EventID AND e3.EventID <> e.EventID AND e3.FromDate <= e.FromDate AND e3.ToDate > e.FromDate)
										GROUP BY ISNULL(e3.EventId, e.EventId)
									) e
									LEFT JOIN [$(Acquisition)].IntraBet.tblEvents e4  ON (e4.MasterEventID = e.EventID AND e4.EventID <> e.EventID AND e4.FromDate <= e.FromDate AND e4.ToDate > e.FromDate)
							GROUP BY ISNULL(e4.EventId, e.EventId)
						) e
						LEFT JOIN [$(Acquisition)].IntraBet.tblEvents e5  ON (e5.MasterEventID = e.EventID AND e5.EventID <> e.EventID AND e5.FromDate <= e.FromDate AND e5.ToDate > e.FromDate)
				GROUP BY ISNULL(e5.EventId, e.EventId)
			) e
			LEFT JOIN [$(Acquisition)].IntraBet.tblEvents e6  ON (e6.MasterEventID = e.EventID AND e6.EventID <> e.EventID AND e6.FromDate <= e.FromDate AND e6.ToDate > e.FromDate)
	GROUP BY ISNULL(e6.EventId, e.EventId)

	--there is an issue with errors in the pricing data where the intrabetid is used more than once in different events
	--as there is no way to determine the correct record, both records are ignored
	select IntrabetID, max(RaceGroup) RaceGroup, max(RaceClass) RaceClass, max(HybridPricingTemplate) HybridPricingTemplate, max(TotalPrizeMoney) TotalPrizeMoney, max(WeightType) WeightType, max(Distance) Distance
	into #RaceInfo
	from #Candidate e
	INNER JOIN [$(Acquisition)].PricingData.Race  r on e.eventID=r.IntrabetID and e.FromDate>=r.FromDate and e.FromDate<r.ToDate
	group by r.IntrabetID
	having count(*)=1
			
	select A.EventID ,CASE WHEN MAX(todate)>='9999-12-30' THEN NULL ELSE MAX(todate)END ResultedDate 
	INTO #Resulted
	from [$(Acquisition)].[IntraBet].[tblEvents] A with(nolock) 
	INNER JOIN #Candidate C ON A.EventID=C.EventId
	where A.ResultsIn=0 
	group by a.EventID

	select C.EventID ,ISNULL(Max(B.ToDate), MIN(A.FromDate)) AbandonedDate
	INTO #Abandoned
	from #Candidate C
	INNER JOIN [$(Acquisition)].[IntraBet].[tblEvents] A with(nolock)  ON A.EventID=C.EventId and A.IsAbandonedEvent=1 and A.FromDate<=C.FromDate
	LEFT JOIN [$(Acquisition)].[IntraBet].[tblEvents] B with(nolock)  ON B.EventID=C.EventId and B.IsAbandonedEvent=0 and B.FromDate<=C.FromDate
	group by c.EventId

	EXEC [Control].Sp_Log	@BatchKey,'Sp_Event','InsertIntoStaging','Start',@RowsProcessed = @@RowCount

	INSERT	into Stage.Event (BatchKey, Source, SourceEventId, SourceEvent, SourceEventType, ParentEventId, ParentEvent, 
							GrandparentEventId, GrandparentEvent, GreatGrandparentEventId, GreatGrandparentEvent,
							SourceEventDate, ResultedDateTime, SourceRaceNumber, SourceVenueId, SourceVenue, SourceVenueType,SourceVenueEventType, SourceVenueEventTypeName, SourceVenueStateCode,
							SourceVenueState, SourceVenueCountryCode, SourceVenueCountry, ClassID, ClassSource, SourceSubSportType
							, SourceRaceGroup, SourceRaceClass, SourceHybridPricingTemplate, SourcePrizePool, SourceWeightType, SourceDistance, SourceEventAbandoned, EventAbandonedDateTime, SourceLiveStreamPerformId, SourceTrackCondition, SourceEventWeather, FromDate)
	SELECT	@BatchKey BatchKey, 
			'IEI' Source, 
			e.EventId SourceEventId, 
			e.EventName SourceEvent, 
			et.description SourceEventType,
			ep.EventId SourceParentEventId, 
			ep.EventName SourceParentEvent, 
			egp.EventId SourceGrandparentEventId, 
			egp.EventName SourceGrandparentEvent, 
			eggp.EventId SourceGreatGrandparentEventId, 
			eggp.EventName SourceGreatGrandparentEvent,
			e.EventDate SourceEventDate, 
			R.ResultedDate ResultedDateTime,
			e.RaceNumber SourceRaceNumber,
			v.VenueID SourceVenueId,
			v.VenueName SourceVenue,
			v.VenueType SourceVenueType, 
			v.EventType SourceVenueEventType,
			et2.Description SourceVenueEventTypeName,
			v.stateCode as SourceVenuestateCode, 
			s.StateName as SourceVenueState,
			v.CountryCode as SourceVenueCountryCode, 
			ct.CountryName as SourceVenueCountry,
			e.EventType ClassID, 
			'IBT' ClassSource,
			sst.[Description] as SourceSubSportType,
			ri.RaceGroup as SourceRaceGroup,
			ri.RaceClass as SourceRaceClass,
			ri.HybridPricingTemplate as SourceHybridPricingTemplate,
			ri.TotalPrizeMoney as SourcePrizePool,
			ri.WeightType as SourceWeightType,
			ri.Distance as SourceDistance,
			e.IsAbandonedEvent SourceEventAbandoned,
			A.AbandonedDate EventAbandonedDateTime,
			e.LiveStreamPerformId,
			ISNULL(tc.Track,tc2.Track) SourceTrackCondition,
			ISNULL(tc.Weather,tc2.Weather) SourceEventWeather,
			c.FromDate
	FROM	#Candidate c 
			INNER JOIN [$(Acquisition)].IntraBet.tblEvents e  ON (e.EventID = c.EventId AND e.FromDate <= c.FromDate AND e.ToDate > c.FromDate)
			LEFT JOIN [$(Acquisition)].IntraBet.tblEvents ep  ON (ep.EventID = e.MasterEventId AND ep.FromDate <= c.FromDate AND ep.ToDate > c.FromDate)
			LEFT JOIN [$(Acquisition)].IntraBet.tblEvents egp  ON (egp.EventID = ep.MasterEventId AND egp.FromDate <= c.FromDate AND egp.ToDate > c.FromDate)
			LEFT JOIN [$(Acquisition)].IntraBet.tblEvents eggp  ON (eggp.EventID = egp.MasterEventId AND eggp.FromDate <= c.FromDate AND eggp.ToDate > c.FromDate)
			left join [$(Acquisition)].[Intrabet].[tblLUEventTypes] et 
			on et.eventtype = e.eventtype and (et.FromDate<=c.FromDate and et.ToDate>c.FromDate)
			left outer join [$(Acquisition)].[IntraBet].[tblLUVenues] v 
			on v.venueid = e.venueid and (v.FromDate<=c.FromDate and v.ToDate>c.FromDate)
			left outer join [$(Acquisition)].[IntraBet].[tblLUEventTypes] et2 
			on v.EventType = et2.EventType and (et2.FromDate<=v.FromDate and et2.ToDate>v.FromDate)
			left outer join [$(Acquisition)].[IntraBet].[tblLUStates] s 
			on v.StateCode = s.Code and (s.FromDate<=c.FromDate and s.ToDate>c.FromDate)
			left outer join [$(Acquisition)].[IntraBet].[tblLUCountry] ct 
			on v.CountryCode = ct.Country and (ct.FromDate<=c.FromDate and ct.ToDate>c.FromDate)
			LEFT OUTER JOIN [$(Acquisition)].[IntraBet].[tblLUSubSportTypes] sst on e.SportSubType = sst.SportSubType and e.EventType = sst.EventType and sst.ToDate = '9999-12-31 00:00:00.000'
			LEFT OUTER JOIN #RaceInfo ri on c.EventId = ri.IntrabetID
			LEFT OUTER JOIN #Resulted R on R.EventID=C.EventId
			LEFT OUTER JOIN [$(Acquisition)].IntraBet.tblTrackConditions tc on e.VenueID=tc.VenueID and ISNULL(v.EventType,e.EventType) = tc.EventType and c.FromDate>=tc.FromDate and c.FromDate<tc.ToDate
			LEFT OUTER JOIN [$(Acquisition)].IntraBet.tblTrackConditions tc2 on e.VenueID=tc2.VenueID and c.FromDate>=tc2.FromDate and c.FromDate<tc2.ToDate and tc.VenueID is null
			LEFT JOIN #Abandoned A on c.EventId=A.EventID
where isnull(e.IsMasterEvent,0)<>1
OPTION(RECOMPILE, TABLE HINT(e, FORCESEEK), TABLE HINT(ep, FORCESEEK), TABLE HINT(egp, FORCESEEK), TABLE HINT(eggp, FORCESEEK))

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Event', NULL, 'Success',@RowsProcessed = @@RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Event', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

END

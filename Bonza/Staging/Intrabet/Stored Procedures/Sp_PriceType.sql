﻿CREATE PROC [Intrabet].[Sp_PriceType]
(
	@FromDate	datetime2(3),
	@ToDate		datetime2(3),
	@BatchKey	int
)
WITH RECOMPILE
AS 
BEGIN TRY

	SET TRANSACTION ISOLATION LEVEL SNAPSHOT;

	-- This is a fixed dimension for now

	EXEC [Control].Sp_Log @BatchKey,'Sp_PriceType','Load into Staging','Start'

	INSERT INTO Stage.PriceType (BatchKey, PriceTypeId, Source, PriceTypeName)
	SELECT DISTINCT
		@BatchKey as [BatchKey],
		BT.BetType as [PriceTypeID],
		'IBB' as [Source],
		--BT.ShortName as [PriceTypeName]
		BTM.ProductName
	FROM [$(Acquisition)].Intrabet.tblLUBetTypes as BT
	LEFT JOIN [Intrabet].[BetTypeMapping] as BTM on BT.BetType = BTM.BetType
	WHERE BT.FromDate <= @ToDate and BT.ToDate > @ToDate and BT.FromDate >= @FromDate
	AND BTM.ProductName != 'Unknown'

	-- Need to correct this. Its filtering out some records. we want rn = 1
	
	EXEC [Control].Sp_Log @BatchKey, 'Sp_PriceType', NULL, 'Success'

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_PriceType', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
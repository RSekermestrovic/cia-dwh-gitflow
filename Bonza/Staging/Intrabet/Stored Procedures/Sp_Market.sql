CREATE PROC [Intrabet].[Sp_Market]
(
	@FromDate	datetime2(3),
	@ToDate		datetime2(3),
	@BatchKey	int
)
WITH RECOMPILE
AS 
BEGIN TRY

	SET TRANSACTION ISOLATION LEVEL SNAPSHOT

	BEGIN TRY DROP TABLE #Candidates END TRY BEGIN CATCH END CATCH
	BEGIN TRY DROP TABLE #masterevents END TRY BEGIN CATCH END CATCH
	BEGIN TRY DROP TABLE #markets END TRY BEGIN CATCH END CATCH
	BEGIN TRY DROP TABLE #groupings END TRY BEGIN CATCH END CATCH
	BEGIN TRY DROP TABLE #MarketTypes END TRY BEGIN CATCH END CATCH
	BEGIN TRY DROP TABLE #suspended END TRY BEGIN CATCH END CATCH
	BEGIN TRY DROP TABLE #Opened END TRY BEGIN CATCH END CATCH
	
	EXEC [Control].Sp_Log @BatchKey,'Sp_Market','Get Markets','Start',@RowsProcessed = @@ROWCOUNT

	SELECT	EventID,Max(FromDate) FromDate 
	into #Candidates
	from(Select EVentID,FromDate
			FROM (	SELECT	EventID,EventName,Suspended,SettledDate,FromDate
					FROM	[$(Acquisition)].[IntraBet].tblEvents 
					WHERE	FromDate > @FromDate AND FromDate <= @ToDate AND ToDate > convert(datetime,@FromDate)
					UNION ALL
					SELECT	EventID,EventName,Suspended,SettledDate,ToDate as FromDate
					FROM	[$(Acquisition)].[IntraBet].tblEvents 
					WHERE	ToDate > convert(datetime,@FromDate) AND ToDate <=  convert(datetime,@ToDate)
				) x
	GROUP BY EventID,EventName,Suspended,SettledDate,FromDate
	HAVING COUNT(*) <> 2)y 
	group by EventID 
	option(recompile);

	EXEC [Control].Sp_Log @BatchKey,'Sp_Market','Get Market details','Start',@RowsProcessed = @@RowCount

	--Selecting the interested field values for the shortlisted candidates
	--A market is suspended when settledDate is not null even if Suspended=0
	SELECT	e.EventID MarketID,EventName MarketName,masterEventID,Eventtype, eventSubtype, Suspended OriginalSuspended, 
	(Case When Suspended=0 and SettledDate is null Then Suspended Else 1 End) DerivedSuspended,SettledDate,e.FromDate,
	Case When e.IsLiveEvent=1 Then 'Yes' Else 'No' End as LiveMarket,
	Case When e.IsFutureEvent=1 Then 'Yes' Else 'No' End as FutureMarket,
	e.Limit as MaxWinnings,
	e.MaxStake 
	into #Markets
	FROM	#Candidates t
	inner join [$(Acquisition)].[IntraBet].tblEvents  e  on e.EventID=t.EventID and e.FromDate=t.FromDate and e.TODate> convert(datetime,@ToDate)
	option(recompile);

	EXEC [Control].Sp_Log @BatchKey,'Sp_Market','Get Suspended datetime','Start',@RowsProcessed = @@RowCount
	--Identifynig the date when market is suspended
	select A.EventID MarketId,ISNULL(MAX(B.ToDate),MIN(A.FromDate))  SuspendedDateTime
	INTO #suspended
	from #Markets M
	INNER JOIN (select eventID,fromDate,todate from [$(Acquisition)].[IntraBet].[tblEvents] where suspended=1) A ON A.EventID=M.MarketId --and A.Suspended=1
	LEFT OUTER JOIN (select eventID,fromDate,todate from [$(Acquisition)].[IntraBet].[tblEvents] where suspended=0) B   ON B.EventID=M.MarketId and B.ToDate=A.FromDate  --and B.Suspended=0
	where M.DerivedSuspended=1  
	group by a.EventID
	option(recompile);

	EXEC [Control].Sp_Log @BatchKey,'Sp_Market','Get Opened date time','Start',@RowsProcessed = @@RowCount
	select A.EventID MarketId,min(A.FromDate) AS OpenedDateTime 
	INTO #Opened
	from #Markets M
	LEFT OUTER JOIN  (select eventID,fromDate from [$(Acquisition)].[IntraBet].[tblEvents] where Suspended=0) A   	ON A.EventID=M.MarketId --and A.Suspended=0
	where M.DerivedSuspended=0
	group by a.EventID
	option(recompile);

	EXEC [Control].Sp_Log @BatchKey,'Sp_Market','Get master events','Start',@RowsProcessed = @@RowCount
	SELECT   x.MasterEventId,
			e.FromDate,
			e.ToDate,
			e.EventName MasterEventName
	INTO #MasterEvents
	FROM	(SELECT  DISTINCT MasterEventId FROM #Markets) x
			INNER JOIN [$(Acquisition)].IntraBet.tblEvents e with(Forceseek([CI_tblEvents(A)](EventID))) ON (e.EventID = x.MasterEventID AND e.FromDate < @toDate AND e.ToDate >  @FromDate)
	
	EXEC [Control].Sp_Log @BatchKey,'Sp_Market','Get Market types','Start',@RowsProcessed = @@RowCount
	SELECT  e.MarketId , 
			e.MarketName ,
			e.FromDate,
			e.MasterEventId,
			ep.MasterEventName,
			et.[Description] Sport,
			est.[Description] Market
	INTO #MarketTypes
	FROM	#Markets e
			LEFT JOIN #MasterEvents ep WITH (NOLOCK)  ON ep.MasterEventID = e.MasterEventId AND ep.FromDate <= e.FromDate AND ep.ToDate > e.FromDate
			LEFT JOIN [$(Acquisition)].[Intrabet].[tblLUEventTypes] et WITH (NOLOCK) on et.eventtype = e.eventtype and (et.FromDate<=e.FromDate and et.ToDate>e.FromDate)
			LEFT JOIN [$(Acquisition)].[IntraBet].[tblLUEventSubTypes] est WITH (NOLOCK) on e.EventSubType = est.EventSubType and (est.FromDate<=e.FromDate and est.ToDate>e.FromDate)


	EXEC [Control].Sp_Log @BatchKey,'Sp_Market','Get Market Groupings','Start',@RowsProcessed = @@RowCount
	Select
		a.MarketId,
		MAX(IsNull(b.MarketType,'Other')) MarketType,
		MAX(IsNull(b.MarketGroup,'Other')) MarketGroup,
		MAX(IsNull(b.LevyRate,'Other')) LevyRate,
		MAX(a.Sport) Sport,
		MAX(a.Market) Market
	INTO #Groupings
	FROM #MarketTypes a
	LEFT OUTER JOIN [Reference].[MarketTypeMap] b on a.Sport = b.Sport 
	and SUBSTRING(REPLACE(MarketName,MasterEventName,''),CHARINDEX('-',REPLACE(MarketName,MasterEventName,''),0) + CASE WHEN (CHARINDEX('-',REPLACE(MarketName,MasterEventName,''),0))>0 THEN 2 ELSE 0 END,LEN(REPLACE(MarketName,MasterEventName,''))-CHARINDEX('-',REPLACE(MarketName,MasterEventName,''),0)) like b.Keyword
	Group By a.MarketId
	
	EXEC [Control].Sp_Log @BatchKey,'Sp_Market','Insert into Staging','Start',@RowsProcessed = @@RowCount
	INSERT INTO Stage.Market (BatchKey, Source, MarketId, Market, MarketType, MarketGroup, FutureMarket, LiveMarket, MaxWinnings, MaxStake, MarketOpen, MarketOpenedDateTime, MarketClosed, MarketClosedDateTime, MarketSettled, MarketSettledDateTime, LevyRate, Sport, FromDate)
	Select 
		@BatchKey BatchKey, 
		'IEI' Source, 
		a.MarketId, 
		a.MarketName,
		IsNull(M.Market,M.MarketType) MarketType,
		IsNull(M.Market, M.MarketGroup) MarketGroup,
		a.FutureMarket,
		a.LiveMarket,
		a.MaxWinnings,
		a.MaxStake,
		(Case When DerivedSuspended=0 Then 'Yes' Else 'No' End) Opened,
		O.OpenedDateTime,
		(Case When DerivedSuspended=0 Then 'No' Else 'Yes' End) Suspended,
		(Case when OriginalSuspended=0 Then a.SettledDate Else s.SuspendedDateTime End) SuspendedDateTime,
		(Case when a.settledDate is Null Then 'No' Else 'Yes' End) Settled,
		a.SettledDate SettledDateTime,
		M.LevyRate,
		M.Sport,
		a.FromDate 
	From #Markets a
	LEFT OUTER JOIN #Groupings M on M.MarketId=a.MarketId
	LEFT OUTER JOIN #suspended S on S.MarketId=a.MarketId
	LEFT OUTER JOIN #Opened O on O.MarketId=a.MarketId
	EXEC [Control].Sp_Log @BatchKey, 'Sp_Market', NULL, 'Success',@RowsProcessed = @@RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Market', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
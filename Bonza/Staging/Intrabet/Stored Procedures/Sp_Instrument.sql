﻿CREATE PROC [Intrabet].[Sp_Instrument]
(
	@FromDate	datetime2(3),
	@ToDate		datetime2(3),
	@BatchKey	int
)
WITH RECOMPILE
AS
BEGIN
BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Instrument', 'Start', 'Start';

	BEGIN TRANSACTION Instrument;

	BEGIN TRY -- Speculative attempt to drop pre-existing Temp table, don't care if it fails due to non-existence
		DROP TABLE #Candidate
	END TRY BEGIN CATCH	END CATCH 

	Create table #Candidate
	(
		ID varchar(75),
		trnType varchar(max),
		fromdate datetime,
		TimestampPPL datetime
	);

	Insert into #Candidate(ID,trnType,FromDate)
	select distinct mbk.pay_from_email,'MoneyBookers',max(mbk.fromDate )
	from [$(Acquisition)].Intrabet.tblMoneybookersdepositresponse mbk
	where  mbk.FromDate>=@FromDate and mbk.todate>@Todate and mbk.FromDate<@ToDate and mbk.pay_from_email  is not null and mbk.pay_from_email<>'n/a'
	group by mbk.pay_from_email;

	Insert into #Candidate(ID,trnType,FromDate)
	select Bpy.BPayID,'BPay',Bpy.fromDate 
	from [$(Acquisition)].Intrabet.tblClientBpayDetails Bpy
	where  Bpy.FromDate>=@FromDate and Bpy.todate>@Todate and Bpy.FromDate<@ToDate and Bpy.BPayID  is not null; 

	Insert into #Candidate
	select distinct Ppl.PayerID as id,'Paypal',max(Ppl.fromDate) ,max(Ppl.[Timestamp]) 
	from [$(Acquisition)].Intrabet.tblPaypalGEc Ppl
	where  Ppl.FromDate>=@FromDate and Ppl.todate>@Todate and Ppl.FromDate<@Todate and Ppl.PayerID  is not null and Ppl.PayerID <>'n/a'
	group by Ppl.PayerID;

	Insert into #Candidate(ID,trnType,FromDate)
	select distinct Bnk.BankID,'EFT',max(Bnk.fromDate )
	from [$(Acquisition)].Intrabet.tblClientBankDetails Bnk
	where  Bnk.FromDate>=@FromDate and Bnk.todate>@Todate and Bnk.FromDate<@ToDate and Bnk.BankID is not null
	group by Bnk.BankID;

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Instrument', 'InsertIntoStaging', 'Start';

	INSERT	into Stage.Instrument (BatchKey,InstrumentId,Source,AccountNumber,SafeAccountNumber,AccountName,BSB,ExpiryDate,Verified,VerifyAmount,InstrumentType,ProviderName,FromDate)
	SELECT	
			@BatchKey																			AS BatchKey, 
			C.id																				AS instrumentID,
			ISNULL(CASE  C.TrnType
				WHEN 'Moneybookers' THEN 'MBK'
				WHEN 'BPay' THEN 'BPY'
				WHEN 'PayPal' THEN 'PPL'
				WHEN 'EFT' THEN 'EFT'
				ELSE 'Unknown'	
			END,'Unknown')																		AS Source, 
			ISNULL(CASE  C.TrnType
						WHEN 'EFT' THEN (Case
											When BD.AccountNumber=''
											Then 'Unknown'
											Else BD.AccountNumber
										  End)
						When 'BPaY' Then (Case
											When Bp.BillerCode=''
											Then 'Unknown'
											Else Bp.BillerCode
										  End)
						WHEN 'PayPal' THEN (Case
												When Pg.Token=''
												Then 'Unknown'
												Else Pg.Token
											End)
						ELSE 'N/A'	
					END,'Unknown')																AS AccountNumber,
			ISNULL(CASE  C.TrnType
						WHEN 'EFT' THEN (Case
											When BD.AccountNumber='' or BD.AccountNumber is null
											Then 'Unknown'
											Else isnull((Replicate('*',Len(BD.AccountNumber)-4)+Right(BD.AccountNumber,4)),'')
										  End)
						When 'BPaY' Then (Case
											When Bp.BillerCode=''
											Then 'Unknown'
											Else Bp.BillerCode
										End)
						WHEN 'PayPal' THEN (Case
												When Pg.Token=''
												Then 'Unknown'
												Else Pg.Token
											End)
						ELSE 'N/A'	
					END,'Unknown')																AS SafeAccountNumber,
			ISNULL(CASE  C.TrnType
				WHEN 'PayPal' THEN (Case
										When Pg.Payer='' or Pg.Payer is null
										Then 'Unknown'
										Else Pg.Payer
									End)
				WHEN 'EFT' THEN (Case
									When BD.AccountName='' or BD.AccountName is null
									Then 'Unknown'
									Else BD.AccountName
								 End)
				ELSE 'N/A'	
			END,'Unknown')																		AS AccountName,		
			ISNULL(CASE  C.TrnType
						WHEN 'EFT' THEN (Case
											When(bd.BankBsb not like '%[a-z,.,?]%' and Len(Replace(Replace(bd.BankBsb,'-',''),' ',''))<=8)
											Then (Case
													When (Replace(Replace(bd.BankBsb,'-',''),' ','') like '[0-9][0-9][0-9][0-9][0-9][0-9]')
													Then Replace(Replace(bd.BankBsb,'-',''),' ','')
													Else 'Unknown'
												  End)
											Else 'Unknown'
										End)
						ELSE 'N/A'	
					END,'Unknown')																AS BSB,
			NULL																				AS ExpiryDate,
			ISNULL(CASE C.TrnType
				WHEN 'PayPal' THEN (Case When pg.Payerstatus='verified' Then 'Yes'
										 Else 'No'
									End)
				ELSE 'U/K'	
			END,'U/K')																			AS Verified,
			NULL																				AS VerifiedAmount,
			ISNULL(CASE C.TrnType
				WHEN 'Moneybookers' THEN 'MoneyBookers'
				WHEN 'BPay' THEN 'BPAY'
				WHEN 'PayPal' THEN 'PayPal'
				WHEN 'EFT' THEN 'EFT'
				ELSE 'Unknown'	
			END,'Unknown')																		AS InstrumentType,
			ISNULL(CASE  C.TrnType
				WHEN 'BPay' THEN 'Unknown'
				WHEN 'EFT' THEN (Case 
									When pn.bankName is not null
									Then pn.BankName
									When bd.BankName<>''
									Then (Case
										  When (Replace(Replace(Replace(bd.BankName,'*',''),0,''),']','')) in ('',' ')
										  Then 'Unknown'
										  Else Replace(Replace(Replace(bd.BankName,'*',''),0,''),']','')
										 End)
									Else 'Unknown'
								End)
				ELSE 'N/A'	
			END,'Unknown')																		AS ProviderName,
			c.FromDate																			AS FromDate		
	FROM	
	#Candidate C  with (nolock)
	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblClientBpayDetails BP WITH (NOLOCK)	
	ON (C.TrnType COLLATE SQL_Latin1_General_CP1_CI_AS='Bpay' and C.ID =Cast(Bp.BPayID as varchar)and 
	(Bp.ToDate = CONVERT(datetime2(3),'9999-12-31') or Bp.ToDate = CONVERT(datetime2(3),'9999-12-30')))
	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblClientBankDetails Bd WITH (NOLOCK)	
	ON (C.TrnType COLLATE SQL_Latin1_General_CP1_CI_AS='Eft' and C.ID =Cast(Bd.BankID as varchar) and 
	(Bd.ToDate = CONVERT(datetime2(3),'9999-12-31') or Bd.ToDate = CONVERT(datetime2(3),'9999-12-30')))	
	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblMoneybookersdepositresponse B WITH (NOLOCK) 
	ON (C.TrnType COLLATE SQL_Latin1_General_CP1_CI_AS='MoneyBookers' and C.ID =Cast(B.ID as varchar )
	and (B.ToDate = CONVERT(datetime2(3),'9999-12-31') or B.ToDate = CONVERT(datetime2(3),'9999-12-30')))
	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblPayPalGEC Pg WITH (NOLOCK) 
	ON (C.TrnType COLLATE SQL_Latin1_General_CP1_CI_AS='Paypal' and C.ID COLLATE SQL_Latin1_General_CP1_CI_AS=Cast(Pg.PayerID  as varchar) and c.TimestampPPL=Pg.[Timestamp]
		and (Pg.ToDate = CONVERT(datetime2(3),'9999-12-31') or Pg.ToDate = CONVERT(datetime2(3),'9999-12-30')))
	LEFT OUTER JOIN Reference.ProviderNames_BSB pn WITH (NOLOCK)
	on (pn.bsb  =(select Case
							When(bd.BankBsb not like '%[a-z,.,?]%')
							Then (Case
									When(Replace(Replace(bd.BankBsb,'-',''),' ','') like '[0-9][0-9][0-9][0-9][0-9][0-9]')
									Then (Case
												When (Substring(bd.BankBSB,1,3) in (select cast(bsb as varchar(max)) from Reference.ProviderNames_BSB))
												Then cast(Substring(bd.BankBSB,1,3)as int) 
												Else cast(Substring(bd.BankBSB,1,2)as int) 
											End) 
										Else 1111111
									End)
								Else 2222222
							End))	;

	--Delete duplicate rows if any (as there are duplicates in paypal)
	WITH CTE AS
	(
		SELECT instrumentID, Source, RN = ROW_NUMBER()OVER(PARTITION BY instrumentID, source ORDER BY instrumentID)
		FROM Stage.Instrument
	)
	DELETE FROM CTE WHERE RN > 1;
							
	COMMIT TRANSACTION Instrument;

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Instrument', NULL, 'Success';

END TRY
BEGIN CATCH

	ROLLBACK TRANSACTION Instrument;
	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Instrument', NULL, 'Failed', @ErrorMessage;
	RAISERROR(@ErrorMessage,16,1);
	THROW;

END CATCH
																					   																					
END

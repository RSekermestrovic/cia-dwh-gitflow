﻿CREATE PROC [Intrabet].[Sp_OtherTrans]
(
	@FromDate	DATETIME2(3),
	@ToDate		DATETIME2(3),
	@BatchKey	INT,
	@RowsProcessed	int = 0 OUTPUT
)
AS 
BEGIN

	SET TRANSACTION ISOLATION LEVEL SNAPSHOT

	DECLARE @RowCount INT;

BEGIN TRY

	-- Get list of BetBack Account to exclude
	
	EXEC [Control].Sp_Log	@BatchKey,'Sp_OtherTrans','ExcludedAccounts','Start'

	BEGIN TRY DROP TABLE #ExcludedAccounts END TRY BEGIN CATCH END CATCH

	SELECT A.FromValue AS ClientID															
	INTO #ExcludedAccounts
	FROM Reference.Transformation A
	INNER JOIN Reference.MappingTransformation B ON A.TransformationKey=B.TransformationKey
	INNER JOIN Reference.Mapping C ON B.MappingKey=C.MappingKey
	WHERE C.FromDatabase='Acquisition' AND C.FromSchema='IntraBet' AND C.ToColumn='AccountTypeName'
	AND A.ToValue IN ('BetBack')
	OPTION (RECOMPILE)

	-- APPROVED OtherTransS IN SOURCE	

	EXEC [Control].Sp_Log	@BatchKey,'Sp_OtherTrans','ApprovedTransactions','Start', @RowsProcessed = @@ROWCOUNT

	BEGIN TRY	DROP TABLE #OtherTransapproved	END TRY	BEGIN CATCH	END CATCH

	SELECT
		T.TransactionID, T.Notes, T.TransactionDate, T.Amount, T.FromDate, T.Channel, T.ToDate, T.ClientID, M.BalanceTypeID, M.TransactionDetailId,M.TransactionTypeID,M.TransactionStatusID,
		M.TransactionMethodID,CASE WHEN RIGHT(M.TransactionDetailId,1) = '+' THEN 'CR' ELSE 'DR' END AS TransactionDirection, C.Username, A.AccountId
	INTO #OtherTransapproved
	FROM [$(Acquisition)].Intrabet.tblTransactions T		
	INNER JOIN [$(Acquisition)].Intrabet.tblAccounts A 		ON (A.ClientID = T.ClientID and A.AccountNumber = T.AccountNumber and A.ToDate = CONVERT(datetime2(3),'9999-12-31'))
	INNER JOIN Intrabet.TransactionTypeMapping M 		ON (T.TransactionCode=M.TransactionCode AND CASE WHEN T.AMOUNT >=0 THEN 1 ELSE -1 END =M.Direction AND M.IsDeleted=0) 
	INNER JOIN [$(Acquisition)].Intrabet.tblClients C 		ON (C.ClientID = T.ClientID and C.ToDate = CONVERT(datetime2(3),'9999-12-31'))
	WHERE	M.transactiontypeID IN ('AD','BO')
	AND		T.FromDate > @FromDate AND T.FromDate <= @ToDate AND T.ToDate > @FromDate
	AND		T.ClientID NOT IN (SELECT CLIENTID FROM #ExcludedAccounts) 
	AND		M.[Enabled]=1
	OPTION (RECOMPILE)

	-- DELETED OtherTransS IN SOURCE	

	EXEC [Control].Sp_Log	@BatchKey,'Sp_OtherTrans','DeletedTransactions','Start', @RowsProcessed = @@ROWCOUNT

	BEGIN TRY	DROP TABLE #OtherTransdeleted	END TRY	BEGIN CATCH	END CATCH

	SELECT
		T.TransactionID, T.Notes, T.TransactionDate, T.Amount, T.FromDate, T.Channel, T.ToDate, T.ClientID, M.BalanceTypeID, M.TransactionDetailId,M.TransactionTypeID,M.TransactionStatusID,
		M.TransactionMethodID,CASE WHEN RIGHT(M.TransactionDetailId,1) = '+' THEN 'CR' ELSE 'DR' END AS TransactionDirection, C.Username, A.AccountID
	INTO #OtherTransdeleted
	FROM [$(Acquisition)].Intrabet.tblTransactions T		
	INNER JOIN [$(Acquisition)].Intrabet.tblAccounts A		ON (A.ClientID = T.ClientID and A.AccountNumber = T.AccountNumber and A.ToDate = CONVERT(datetime2(3),'9999-12-31'))
	INNER JOIN Intrabet.TransactionTypeMapping M 		ON (T.TransactionCode=M.TransactionCode AND CASE WHEN T.AMOUNT >=0 THEN 1 ELSE -1 END =M.Direction AND M.IsDeleted=1) 
	INNER JOIN [$(Acquisition)].Intrabet.tblClients C 		ON (C.ClientID = T.ClientID and C.ToDate = CONVERT(datetime2(3),'9999-12-31'))
	WHERE	M.transactiontypeID IN ('AD','BO')
	AND		T.ToDate > @FromDate AND T.ToDate <= @ToDate
	AND		T.ToDate<'9999-12-30'
	AND		T.ClientID NOT IN (SELECT CLIENTID FROM #ExcludedAccounts ) 
	AND		M.[Enabled]=1
	OPTION (RECOMPILE)

------------------------------------ INSERT INTO STAGING -----------------------------------

	EXEC [Control].Sp_Log	@BatchKey,'Sp_OtherTrans','InsertIntoStaging','Start', @RowsProcessed = @@ROWCOUNT;

	INSERT INTO Stage.[Transaction] (BatchKey,TransactionId,TransactionIdSource,BalanceTypeID,TransactionTypeID,TransactionStatusID,TransactionMethodID,TransactionDirection,BetGroupID,BetGroupIDSource,BetId,BetIdSource,LegId,LegIdSource,FreeBetID,
	ExternalID,External1,External2,UserID,UserIDSource,InstrumentID,InstrumentIDSource,LedgerID,LedgerSource,WalletId,PromotionId,PromotionIDSource,MasterTransactionTimestamp,MasterDayText,MasterTimeText,Channel,ActionChannel,
	CampaignId,TransactedAmount,BetGroupType,BetTypeName,BetSubTypeName,LegBetTypeName,GroupingType,PriceTypeId,PriceTypeSource,ClassId,ClassIdSource,EventID,EventIDSource,MarketID,MarketIDSource,LegNumber,NumberOfLegs,ExistsInDim,Conditional,
	MappingType,MappingId,FromDate,DateTimeUpdated,RequestID,WithdrawID,InPlay,ClickToCall,CashoutType,IsDoubleDown,IsDoubledDown)

	-- OtherTrans REQUESTS

	SELECT
		@BatchKey AS BatchKey,
		T.TransactionID AS TransactionID,
		'IBT' AS TransactionIDSource,
		ISNULL(T.BalanceTypeID,'UK') AS BalanceTypeID,					-- Changed from BalanceName 6Mar15
		ISNULL(T.TransactionTypeID,'UK') AS TransactionTypeID,
		ISNULL(T.TransactionStatusID,'UK') AS TransactionStatusID,
		ISNULL(T.TransactionMethodID,'UK') AS TransactionMethodID,
		ISNULL(T.TransactionDirection,'UN') AS TransactionDirection,
		T.TransactionID AS BetGroupID,
		'IBT' AS BetGroupIDSource,
		T.TransactionID AS BetID,
		'IBT' AS BetIDSource,
		T.TransactionID AS LegID,
		'IBT' AS LegIDSource,
		0 AS FreeBetID,
		'N/A' AS ExternalID,
		'N/A' AS External1,
		'N/A' AS External2,
		ISNULL(tl.UserCode,0) as userID,
		'IUU' AS UserIDSource,
		0 AS InstrumentID,
		'N/A' AS InstrumentIDSource,
		T.AccountId LedgerID,
		CASE WHEN T.AccountId=0 THEN 'N/A' ELSE 'IAA' END AS LedgerSource,
		'C' AS WalletId,
		0 AS PromotionId,
		'N/A' AS PromotionIDSource,
		CASE WHEN CONVERT(TIME,T.FromDate) ='00:00:00.000' THEN COALESCE(L.TransactionDate,T.TransactionDate) ELSE ISNULL(LT.CorrectedDate,T.FromDate) END AS  MasterTransactionTimeStamp,	-- FromDate is most accurate except when FromDate is midnight.
		CASE WHEN CONVERT(TIME,T.FromDate) ='00:00:00.000' THEN CONVERT(VARCHAR(11),CONVERT(DATE,COALESCE(L.TransactionDate,T.TransactionDate))) ELSE CONVERT(VARCHAR(11),CONVERT(DATE,ISNULL(LT.CorrectedDate,T.FromDate))) END AS MasterDayText,
		CASE WHEN CONVERT(TIME,T.FromDate) ='00:00:00.000' THEN CONVERT(VARCHAR(5),CONVERT(TIME,COALESCE(L.TransactionDate,T.TransactionDate))) ELSE CONVERT(VARCHAR(5),CONVERT(TIME,ISNULL(LT.CorrectedDate,T.FromDate))) END  AS MasterTimeText,
		--T.TransactionDate AS MasterEffectiveDate,
		--ISNULL(ch.ChannelName,'Internet') AS ChannelName,
		IsNull(t.Channel,-1) Channel,
		IsNull(t.Channel,-1) ActionChannel,
		0 AS CampaignID,
		ABS(T.Amount)*(CASE WHEN RIGHT(T.TransactionDetailId,1)='+' THEN 1 ELSE -1 END) AS TransactedAmount,
		'Straight' AS BetGroupType,
		'N/A' AS BetTypeName,
		'N/A' AS BetSubTypeName,
		'N/A' AS LegBetTypeName,
		'Straight' AS GroupingType,
		0 AS PriceTypeID, /* AJ - PriceType Change */
		'N/A' AS PriceTypeSource, /* AJ - PriceType Change */
		0 AS ClassID,
		'IBT'	AS ClassIDSource,
		0		AS EventID,
		'N/A'	AS EventIDSource,
		0		AS MarketID,
		'N/A'	AS MarketIDSource,
		1 AS LegNumber,
		1 AS NumberOfLegs,
		0 AS ExistsInDim,
		0 AS Conditional,
		1 AS MappingType,
		1 AS MappingID,
		T.FromDate AS FromDate,
		0 AS DateTimeUpdated,
		NULL AS RequestID,
		NULL AS WithdrawID,
		'N/A' InPlay,
		'N/A' ClickToCall,
		0 CashoutType, 
		0 IsDoubleDown, 
		0 IsDoubledDown
	FROM #OtherTransapproved T 
	LEFT LOOP JOIN [$(Acquisition)].Intrabet.tblTransactions T1					ON (T.TransactionID=T1.TransactionID AND T.FromDate=T1.ToDate)
	LEFT LOOP JOIN [$(Acquisition)].Intrabet.tblTransactionLogs L				ON (L.TransactionID = T.TransactionID and L.ToDate = CONVERT(datetime2(3),'9999-12-31'))
	--LEFT JOIN Intrabet.ChannelMapping CH							ON (CH.Channel = T.Channel)
	LEFT LOOP JOIN [$(Acquisition)].IntraBet.Latency LT							ON T.FromDate=LT.OriginalDate
	LEFT LOOP JOIN [$(Acquisition)].IntraBet.tblTransactionLogs  tl				ON T.TransactionID=tl.TransactionID and T.FromDate=tl.FromDate
	WHERE	T.BalanceTypeID IS NOT NULL
	

UNION ALL

	-- DELETED OtherTrans REQUESTS

	SELECT
		@BatchKey AS BatchKey,
		T.TransactionID AS TransactionID,
		'IBT' AS TransactionIDSource,
		ISNULL(T.BalanceTypeID,'UK') AS BalanceTypeID,						-- Changed from BalanceName 6Mar15
		ISNULL(T.TransactionTypeID,'UK') AS TransactionTypeID,
		ISNULL(T.TransactionStatusID,'UK') AS TransactionStatusID,
		ISNULL(T.TransactionMethodID,'UK') AS TransactionMethodID,
		ISNULL(T.TransactionDirection,'UN') AS TransactionDirection,
		T.TransactionID AS BetGroupID,
		'IBT' AS BetGroupIDSource,
		T.TransactionID AS BetID,
		'IBT' AS BetIDSource,
		T.TransactionID AS LegID,
		'IBT' AS LegIDSource,
		0 AS FreeBetID,
		'N/A' AS ExternalID,
		'N/A' AS External1,
		'N/A' AS External2,
		ISNULL(d.UserCode,ISNULL(tl.UserCode,0)) as userID,
		'IUU' AS UserIDSource,
		0 AS InstrumentID,
		'N/A' AS InstrumentIDSource,
		T.AccountId LedgerID,
		CASE WHEN T.AccountId=0 THEN 'N/A' ELSE 'IAA' END AS LedgerSource,
		'C' AS WalletId,
		0 AS PromotionId,
		'N/A' AS PromotionIDSource,
		COALESCE(LT.CorrectedDate,T.ToDate,T.TransactionDate) AS  MasterTransactionTimeStamp,
		CONVERT(VARCHAR(11),CONVERT(DATE,COALESCE(LT.CorrectedDate,T.ToDate,T.TransactionDate))) AS MasterDayText,
		CONVERT(VARCHAR(5),CONVERT(TIME,COALESCE(LT.CorrectedDate,T.ToDate,T.TransactionDate))) AS MasterTimeText,
		--ISNULL(ch.ChannelName,'Internet') AS ChannelName,
		IsNull(t.Channel,-1) Channel,
		IsNull(t.Channel,-1) ActionChannel,
		0 AS CampaignID,
		ABS(T.Amount)*(CASE WHEN RIGHT(T.TransactionDetailId,1)='+' THEN 1 ELSE -1 END) AS TransactedAmount,
		'Straight' AS BetGroupType,
		'N/A' AS BetTypeName,
		'N/A' AS BetSubTypeName,
		'N/A' AS LegBetTypeName,
		'Straight' AS GroupingType,
		0 AS PriceTypeID, /* AJ - PriceType Change */
		'N/A' AS PriceTypeSource, /* AJ - PriceType Change */
		0 AS ClassID,
		'IBT'	AS ClassIDSource,
		0		AS EventID,
		'N/A'	AS EventIDSource,
		0		AS MarketID,
		'N/A'	AS MarketIDSource,
		1 AS LegNumber,
		1 AS NumberOfLegs,
		0 AS ExistsInDim,
		0 AS Conditional,
		1 AS MappingType,
		1 AS MappingID,
		T.ToDate AS FromDate,	-- ToDate of the record is the actual event Date time, which the recursive will later apply on the TransactionDateTime
		0 AS DateTimeUpdated,
		NULL AS RequestID,
		NULL AS WithdrawID,
		'N/A' InPlay,
		'N/A' ClickToCall,
		0 CashoutType, 
		0 IsDoubleDown, 
		0 IsDoubledDown
	FROM #OtherTransdeleted T 
	LEFT LOOP JOIN [$(Acquisition)].Intrabet.tblTransactions T1	 			ON (T.TransactionID=T1.TransactionID AND T.ToDate=T1.FromDate)
	LEFT LOOP JOIN [$(Acquisition)].Intrabet.tblTransactionLogs L	 		ON (L.TransactionID = T.TransactionID and L.ToDate = CONVERT(datetime2(3),'9999-12-31'))
	--LEFT JOIN		Intrabet.ChannelMapping CH		 				ON (CH.Channel = T.Channel)
	LEFT LOOP JOIN [$(Acquisition)].IntraBet.Latency LT			 			ON T.ToDate=LT.OriginalDate
	LEFT LOOP JOIN [$(Acquisition)].IntraBet.tblDeletedTransactions d		ON T.TransactionID=D.TransactionID and T.ToDate=d.FromDate
	LEFT LOOP JOIN [$(Acquisition)].IntraBet.tblTransactionLogs  tl			ON T.TransactionID=tl.TransactionID and T.ToDate=tl.FromDate
	WHERE	T1.TransactionID IS NULL
	AND		T.BalanceTypeID IS NOT NULL
	OPTION (RECOMPILE)

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey, 'Sp_OtherTrans', NULL, 'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_OtherTrans', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
END

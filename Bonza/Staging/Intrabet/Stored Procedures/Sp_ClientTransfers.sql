﻿CREATE PROC [Intrabet].[Sp_ClientTransfers]
(
	@FromDate	DATETIME2(3),
	@ToDate		DATETIME2(3),
	@BatchKey	INT,
	@RowsProcessed	int = 0 OUTPUT
)
AS 
BEGIN

	SET TRANSACTION ISOLATION LEVEL SNAPSHOT

	DECLARE @RowCount INT;

BEGIN TRY

-- Initialise Temp Tables

		BEGIN TRY DROP TABLE #ExcludedAccounts END TRY BEGIN CATCH END CATCH					
		BEGIN TRY DROP TABLE #AccountTransDebit END TRY BEGIN CATCH END CATCH
		BEGIN TRY DROP TABLE #AccountTransCredit END TRY BEGIN CATCH END CATCH
		BEGIN TRY DROP TABLE #AccountTransDeleted END TRY BEGIN CATCH END CATCH
		BEGIN TRY DROP TABLE #AccountTransCandidates END TRY BEGIN CATCH END CATCH
		BEGIN TRY DROP TABLE #AccountTransCandidatesFinal END TRY BEGIN CATCH END CATCH		

-- List of all Intrabet Betback accounts to excluded from the client transfer, system accounts included
		
		EXEC [Control].Sp_Log	@BatchKey,'Sp_ClientTransfers','ExcludedAccounts','Start'

		SELECT A.FromValue AS ClientID															
		INTO #ExcludedAccounts
		FROM Reference.Transformation A
		INNER JOIN Reference.MappingTransformation B ON A.TransformationKey=B.TransformationKey
		INNER JOIN Reference.Mapping C ON B.MappingKey=C.MappingKey
		WHERE C.FromDatabase='Acquisition' AND C.FromSchema='IntraBet' AND C.ToColumn='AccountTypeName'
		AND A.ToValue IN ('BetBack')
		OPTION (RECOMPILE)

-- List of all Client to Client Debit Transactions 

		EXEC [Control].Sp_Log	@BatchKey,'Sp_ClientTransfers','DebitTransactions','Start', @RowsProcessed = @@ROWCOUNT

		SELECT FromDate,ToDate,TransactionID,ClientID,Amount,AccountNumber,TransactionDate,TransactionCode,Channel
		INTO #AccountTransDebit
		FROM [$(Acquisition)].IntraBet.tblTransactions WHERE TransactionCode IN (907,902,903,908) AND Amount<0 --AND ToDate='9999-12-31'
		AND FromDate > @FromDate AND FromDate <= @ToDate AND ToDate > @FromDate
		AND ClientID NOT IN (SELECT ClientID FROM #ExcludedAccounts)
		OPTION (RECOMPILE)

-- List of all Client to Client Credit Transactions 

		EXEC [Control].Sp_Log	@BatchKey,'Sp_ClientTransfers','CreditTransactions','Start', @RowsProcessed = @@ROWCOUNT

		SELECT FromDate,ToDate,TransactionID,ClientID,Amount,AccountNumber,TransactionDate,TransactionCode,Channel
		INTO #AccountTransCredit
		FROM [$(Acquisition)].IntraBet.tblTransactions WHERE TransactionCode IN (907,902,903,908) AND Amount>=0 --AND ToDate='9999-12-31'
		AND FromDate > @FromDate AND FromDate <= @ToDate AND ToDate > @FromDate
		AND ClientID NOT IN (SELECT ClientID FROM #ExcludedAccounts)
		OPTION (RECOMPILE)

-- List of all Deleted Transfer Transactions 
		
		EXEC [Control].Sp_Log	@BatchKey,'Sp_ClientTransfers','DeletedTransactions','Start', @RowsProcessed = @@ROWCOUNT

		SELECT T.FromDate,T.TODATE,T.TransactionID,T.ClientID,A.AccountID,T.Amount,T.TransactionDate,T.TransactionCode,T.Channel						
		INTO #AccountTransDeleted
		FROM [$(Acquisition)].IntraBet.tblTransactions T 
		LEFT OUTER JOIN [$(Acquisition)].IntraBet.tblTransactions T1 ON T.TransactionID=T1.TransactionID AND T.ToDate=T1.FromDate		-- Check to see if its really a delete or just a CDC update.
		LEFT OUTER JOIN [$(Acquisition)].IntraBet.tblAccounts A ON T.ClientID=A.ClientID AND T.ToDate>=A.FromDate AND T.ToDate<A.ToDate AND T.AccountNumber=A.AccountNumber	-- Join to get AccountID 
		WHERE T.TransactionCode IN (907,902,903,908)
		AND T.ToDate > @FromDate AND T.ToDate <= @ToDate AND T.ToDate<'9999-12-30'
		AND T1.TransactionID IS NULL
		OPTION (RECOMPILE)

-- Link Debit Transactions to Credit Transaction to get the complete 'Range of Credit Candidates' for each debit transcation. 

		EXEC [Control].Sp_Log	@BatchKey,'Sp_ClientTransfers','MapDebitandCredit','Start', @RowsProcessed = @@ROWCOUNT

		SELECT
			A.FromDate AS DR_FromDate,
			A.ToDate as DR_ToDate,
			A.TransactionID AS DR_TransactionID,
			A.ClientID AS DR_ClientID,
			A.AccountNumber AS DR_AccountNumber,
			A.Amount AS DR_Amount,
			A.TransactionDate AS DR_TransactionDate,
			B.FromDate AS CR_FromDate,
			B.ToDate as CR_ToDate,
			B.TransactionID AS CR_TransactionID,
			B.ClientID AS CR_ClientID,
			B.AccountNumber as CR_AccountNumber,
			B.Amount AS CR_Amount,
			B.TransactionDate AS CR_TransactionDate,
			ABS(A.TransactionID-B.TransactionID) AS TranID_Diff,
			ABS(DATEDIFF(minute,A.TransactionDate,B.TransactionDate)) AS TranDate_Diff,
			IsNull(A.Channel,b.Channel) Channel
		INTO #AccountTransCandidates
		FROM #AccountTransDebit A
		FULL OUTER JOIN #AccountTransCredit B 
				ON	(	
						A.TransactionCode=B.TransactionCode										-- Link within the same Transaction Code
						AND ABS(A.TransactionID-B.TransactionID)<10000							-- Usually Both DR and CR Transaction are 1 transaction apart, but they can be several 1000's apart too, so we get the range of best values to filter later. 
						AND ABS(A.Amount)=ABS(B.Amount)											-- Amount on DR and CR should be the same
						AND CONVERT(DATE,A.TransactionDate)=CONVERT(DATE,B.TransactionDate)		-- Usually TransactionDates on DR and CR are the same to the millisecond, sometimes they vary upto a few seconds. So we get the range of best values. 
					)
		--WHERE ISNULL(A.ClientID,0) NOT IN (SELECT ClientID FROM #ExcludedAccounts) 
		--AND   ISNULL(B.ClientID,0) NOT IN (SELECT ClientID FROM #ExcludedAccounts)
		OPTION (RECOMPILE)

-- Get the best version of the Debit and Credit Transaction obtained from the above 'Range of candidates' logic

		EXEC [Control].Sp_Log	@BatchKey,'Sp_ClientTransfers','BestValueAlgorithm','Start', @RowsProcessed = @@ROWCOUNT

		SELECT
			DR_FromDate,
			DR_ToDate,
			DR_TransactionID,
			DR_ClientID,
			DR_Amount,
			DA.AccountID AS DR_AccountID,
			DR_TransactionDate,
			CR_FromDate,
			CR_ToDate,
			CR_TransactionID,
			CR_ClientID,
			CR_Amount,
			CA.AccountID AS CR_AccountID,
			CR_TransactionDate,
			x.Channel,
			CASE da.ledger WHEN 4 THEN 1 ELSE 0 END WriteOff
		INTO #AccountTransCandidatesFinal  -- DROP TABLE #AccountTransCandidatesFinal 
		FROM (
				SELECT *,DENSE_RANK() OVER(PARTITION BY DR_TransactionID ORDER BY TRANID_Diff) AS TranID_Rank,DENSE_RANK() OVER(PARTITION BY DR_TransactionID ORDER BY TranDate_Diff) AS TranDate_Rank 
				FROM #AccountTransCandidates
			) X
		LEFT OUTER JOIN [$(Acquisition)].IntraBet.tblAccounts DA ON X.DR_ClientID=DA.ClientID AND X.DR_AccountNumber=DA.AccountNumber AND DA.ToDate='9999-12-31'
		LEFT OUTER JOIN [$(Acquisition)].IntraBet.tblAccounts CA ON X.CR_ClientID=CA.ClientID AND X.CR_AccountNumber=CA.AccountNumber AND CA.ToDate='9999-12-31'
		WHERE X.TranID_Rank=1 AND X.TranDate_Rank=1
		OPTION (RECOMPILE)

-- Insert into Staging those Transactions where there is a proper link between Debit and Credit Transactions

	EXEC [Control].Sp_Log @BatchKey,'Sp_ClientTransfers','InsertIntoStaging','Start', @RowsProcessed = @@ROWCOUNT

	INSERT INTO Stage.[Transaction] (BatchKey,TransactionId,TransactionIdSource,BalanceTypeID,TransactionTypeID,TransactionStatusID,TransactionMethodID,TransactionDirection,BetGroupID,BetGroupIDSource,BetId,BetIdSource,LegId,LegIdSource,FreeBetID,
	ExternalID,External1,External2,UserID,UserIDSource,InstrumentID,InstrumentIDSource,LedgerId,LedgerSource,WalletId,PromotionId,PromotionIDSource,MasterTransactionTimestamp,MasterDayText,MasterTimeText,Channel,ActionChannel,
	CampaignId,TransactedAmount,BetGroupType,BetTypeName,BetSubTypeName,LegBetTypeName,GroupingType,PriceTypeId,PriceTypeSource,ClassId,ClassIdSource,EventID,EventIDSource,MarketID,MarketIDSource,LegNumber,NumberOfLegs,ExistsInDim,Conditional,
	MappingType,MappingId,FromDate,DateTimeUpdated,RequestID,WithdrawID,InPlay,ClickToCall,CashoutType,IsDoubleDown,IsDoubledDown)

		-- CREDITS

		SELECT 
			 @BatchKey																AS BatchKey
			,DR.DR_TransactionID													AS TransactionID
			,'IBT'																	AS TransactionIDSource
			,'CLI'																	AS BalanceTypeID
			,'TR'																	AS TransactionTypeID
			,'CO'																	AS TransactionStatusID
			,CASE DR.WriteOff WHEN 1 THEN 'WO' ELSE 'AT' END						AS TransactionMethodID
			,'DR'																	AS TransactionDirection
			,DR.DR_TransactionID													AS BetGroupID
			,'IBT'																	AS BetGroupIDSource
			,DR.DR_TransactionID													AS BetId
			,'IBT'																	AS BetIdSource
			,DR.DR_TransactionID													AS LegId
			,'IBT'																	AS LegIdSource
			,0																		AS FreeBetID
			,'N/A'																	AS ExternalID
			,'N/A'																	AS External1
			,'N/A'																	AS External2
			,ISNULL(d.UserCode,ISNULL(tl.UserCode,0))								AS UserID
			,'IUU'																	AS UserIDSource
			,0																		AS InstrumentID
			,'N/A'																	AS InstrumentIDSource
			,ISNULL(DR.DR_AccountId,-1)												AS LedgerId
			,CASE WHEN DR.DR_AccountId=0 THEN 'N/A' ELSE 'IAA' END					AS LedgerSource
			,'C'																	AS WalletId
			,0																		AS PromotionId
			,'N/A'																	AS PromotionIDSource
			,CASE WHEN CONVERT(TIME,DR.DR_FromDate)='00:00:00.000'	THEN DR.DR_TransactionDate ELSE ISNULL(LT.CorrectedDate,DR.DR_FromDate)																			END		AS MasterTransactionTimestamp
			,CASE WHEN CONVERT(TIME,DR.DR_FromDate)='00:00:00.000'	THEN CONVERT(VARCHAR(11),CONVERT(DATE,DR.DR_TransactionDate)) ELSE CONVERT(VARCHAR(11),CONVERT(DATE,ISNULL(LT.CorrectedDate,DR.DR_FromDate)))	END		AS MasterDayText
			,CASE WHEN CONVERT(TIME,DR.DR_FromDate)='00:00:00.000'	THEN CONVERT(VARCHAR(5),CONVERT(TIME,DR.DR_TransactionDate)) ELSE CONVERT(VARCHAR(5),CONVERT(TIME,ISNULL(LT.CorrectedDate,DR.DR_FromDate)))		END		AS MasterTimeText
			--,'Others'																AS ChannelName
			,IsNull(DR.Channel,-1)													AS Channel
			,IsNull(DR.Channel,-1)													AS ActionChannel
			,0																		AS CampaignId
			,DR.DR_Amount															AS TransactedAmount
			,'Straight'																AS BetGroupType
			,'N/A'																	AS BetTypeName
			,'N/A'																	AS BetSubTypeName
			,'N/A'																	AS LegBetTypeName
			,'Straight'																AS GroupingType
			,0																		AS PriceTypeID /* AJ - PriceType Change */
			,'N/A'																	AS PriceTypeSource /* AJ - PriceType Change */
			,0																		AS ClassID
			,'IBT'																	AS ClassIDSource
			,0																		AS EventID
			,'N/A'																	AS EventIDSource
			,0																		AS MarketID
			,'N/A'																	AS MarketIDSource
			,1																		AS LegNumber
			,1																		AS NumberOfLegs
			,0																		AS ExistsInDim
			,0																		AS Conditional
			,1																		AS MappingType
			,1																		AS MappingID
			,DR.DR_FromDate															AS FromDate
			,0																		AS DateTimeUpdated
			,NULL																	AS RequestID
			,NULL																	AS WithdrawID
			,'N/A'																	AS InPlay
			,'N/A'																	AS ClickToCall
			,0																		AS CashoutType
			,0																		AS IsDoubleDown
			,0																		AS IsDoubledDown
		FROM #AccountTransCandidatesFinal DR
		LEFT OUTER JOIN [$(Acquisition)].IntraBet.Latency LT						ON DR.DR_FromDate=LT.OriginalDate
		LEFT OUTER JOIN [$(Acquisition)].IntraBet.tblDeletedTransactions  d 		ON DR.DR_TransactionID=D.TransactionID and DR.DR_FromDate=d.FromDate
		LEFT OUTER JOIN [$(Acquisition)].IntraBet.tblTransactionLogs tl				ON DR.DR_TransactionID=tl.TransactionID and DR.DR_FromDate=tl.FromDate
		WHERE DR.DR_TRANSACTIONID IS NOT NULL

		UNION ALL

		-- DEBITS

		SELECT 
			 @BatchKey																AS BatchKey
			,CR.CR_TransactionID													AS TransactionID
			,'IBT'																	AS TransactionIDSource
			,'CLI'																	AS BalanceTypeID
			,'TR'																	AS TransactionTypeID
			,'CO'																	AS TransactionStatusID
			,CASE CR.WriteOff WHEN 1 THEN 'WO' ELSE 'AT' END						AS TransactionMethodID
			,'CR'																	AS TransactionDirection
			,ISNULL(CR.DR_TransactionID,CR.CR_TransactionID)						AS BetGroupID				-- Contract ID for the Credit Transaction is the Debit Transation to make the link.
			,'IBT'																	AS BetGroupIDSource
			,CR.CR_TransactionID													AS BetId
			,'IBT'																	AS BetIdSource
			,CR.CR_TransactionID													AS LegId
			,'IBT'																	AS LegIdSource
			,0																		AS FreeBetID
			,'N/A'																	AS ExternalID
			,'N/A'																	AS External1
			,'N/A'																	AS External2
			,ISNULL(tl.UserCode,0)													AS UserID
			,'IUU'																	AS UserIDSource
			,0																		AS InstrumentID
			,'N/A'																	AS InstrumentIDSource
			,ISNULL(CR.CR_AccountId,-1)												AS LedgerId
			,CASE WHEN CR.CR_AccountId=0 THEN 'N/A' ELSE 'IAA' END					AS LedgerSource
			,'C'																	AS WalletId
			,0																		AS PromotionId
			,'N/A'																	AS PromotionIDSource
			,CASE WHEN CONVERT(TIME,CR.CR_FromDate)='00:00:00.000'	THEN CR.CR_TransactionDate ELSE ISNULL(LT.CorrectedDate,CR.CR_FromDate)																			END		AS MasterTransactionTimestamp
			,CASE WHEN CONVERT(TIME,CR.CR_FromDate)='00:00:00.000'	THEN CONVERT(VARCHAR(11),CONVERT(DATE,CR.CR_TransactionDate)) ELSE CONVERT(VARCHAR(11),CONVERT(DATE,ISNULL(LT.CorrectedDate,CR.CR_FromDate)))	END		AS MasterDayText
			,CASE WHEN CONVERT(TIME,CR.CR_FromDate)='00:00:00.000'	THEN CONVERT(VARCHAR(5),CONVERT(TIME,CR.CR_TransactionDate)) ELSE CONVERT(VARCHAR(5),CONVERT(TIME,ISNULL(LT.CorrectedDate,CR.CR_FromDate)))		END		AS MasterTimeText
			--,'Others'																AS ChannelName
			,IsNull(CR.Channel,-1)													AS Channel
			,IsNull(CR.Channel,-1)													AS ActionChannel
			,0																		AS CampaignId
			,CR.CR_Amount															AS TransactedAmount
			,'Straight'																AS BetGroupType
			,'N/A'																	AS BetTypeName
			,'N/A'																	AS BetSubTypeName
			,'N/A'																	AS LegBetTypeName
			,'Straight'																AS GroupingType
			,0																		AS PriceTypeID /* AJ - PriceType Change */
			,'N/A'																	AS PriceTypeSource /* AJ - PriceType Change */
			,0																		AS ClassID
			,'IBT'																	AS ClassIDSource
			,0																		AS EventID
			,'N/A'																	AS EventIDSource
			,0																		AS MarketID
			,'N/A'																	AS MarketIDSource
			,1																		AS LegNumber
			,1																		AS NumberOfLegs
			,0																		AS ExistsInDim
			,0																		AS Conditional
			,1																		AS MappingType
			,1																		AS MappingID
			,CR.CR_FromDate															AS FromDate
			,0																		AS DateTimeUpdated
			,NULL																	AS RequestID
			,NULL																	AS WithdrawID
			,'N/A'																	AS InPlay
			,'N/A'																	AS ClickToCall
			,0																		AS CashoutType
			,0																		AS IsDoubleDown
			,0																		AS IsDoubledDown
		FROM #AccountTransCandidatesFinal CR
		LEFT OUTER JOIN [$(Acquisition)].IntraBet.Latency LT						ON CR.CR_FromDate=LT.OriginalDate
		LEFT OUTER JOIN [$(Acquisition)].IntraBet.tblTransactionLogs tl			ON CR.CR_TransactionID=tl.TransactionID and CR.CR_FromDate=tl.FromDate
		WHERE CR.CR_TRANSACTIONID IS NOT NULL

		UNION ALL

		-- DELETIONS

		SELECT 
			 @BatchKey																AS BatchKey
			,DL.TransactionID														AS TransactionID
			,'IBT'																	AS TransactionIDSource
			,'CLI'																	AS BalanceTypeID
			,'TR'																	AS TransactionTypeID
			,'DE'																	AS TransactionStatusID
			,'AT'																	AS TransactionMethodID
			,CASE WHEN DL.Amount >0 THEN 'DR' ELSE 'CR' END							AS TransactionDirection
			,DL.TransactionID														AS BetGroupID
			,'IBT'																	AS BetGroupIDSource
			,DL.TransactionID														AS BetId
			,'IBT'																	AS BetIdSource
			,DL.TransactionID														AS LegId
			,'IBT'																	AS LegIdSource
			,0																		AS FreeBetID
			,'N/A'																	AS ExternalID
			,'N/A'																	AS External1
			,'N/A'																	AS External2
			,ISNULL(d.UserCode,ISNULL(tl.UserCode,0))								AS UserID
			,'IUU'																	AS UserIDSource
			,0																		AS InstrumentID
			,'N/A'																	AS InstrumentIDSource
			,ISNULL(DL.AccountID,-1)												AS LedgerId
			,CASE WHEN DL.AccountID=0 THEN 'N/A' ELSE 'IAA' END						AS LedgerSource
			,'C'																	AS WalletId
			,0																		AS PromotionId
			,'N/A'																	AS PromotionIDSource
			,ISNULL(LT.CorrectedDate,DL.ToDate)										AS MasterTransactionTimestamp
			,CONVERT(VARCHAR(11),CONVERT(DATE,ISNULL(LT.CorrectedDate,DL.ToDate)))	AS MasterDayText
			,CONVERT(VARCHAR(5),CONVERT(TIME,ISNULL(LT.CorrectedDate,DL.ToDate)))	AS MasterTimeText
			--,'Others'																AS ChannelName
			,IsNull(DL.Channel,-1)													AS Channel
			,IsNull(DL.Channel,-1)													AS ActionChannel
			,0																		AS CampaignId
			,(-1)*DL.Amount															AS TransactedAmount
			,'Straight'																AS BetGroupType
			,'N/A'																	AS BetTypeName
			,'N/A'																	AS BetSubTypeName
			,'N/A'																	AS LegBetTypeName
			,'Straight'																AS GroupingType
			,0																		AS PriceTypeID /* AJ - PriceType Change */
			,'N/A'																	AS PriceTypeSource /* AJ - PriceType Change */
			,0																		AS ClassID
			,'IBT'																	AS ClassIDSource
			,0																		AS EventID
			,'N/A'																	AS EventIDSource
			,0																		AS MarketID
			,'N/A'																	AS MarketIDSource
			,1																		AS LegNumber
			,1																		AS NumberOfLegs
			,0																		AS ExistsInDim
			,0																		AS Conditional
			,1																		AS MappingType
			,1																		AS MappingID
			,DL.FromDate															AS FromDate
			,0																		AS DateTimeUpdated
			,NULL																	AS RequestID
			,NULL																	AS WithdrawID
			,'N/A'																	AS InPlay
			,'N/A'																	AS ClickToCall
			,0																		AS CashoutType
			,0																		AS IsDoubleDown
			,0																		AS IsDoubledDown
		FROM #AccountTransDeleted DL
		LEFT OUTER JOIN [$(Acquisition)].IntraBet.Latency LT						ON DL.ToDate=LT.OriginalDate
		LEFT OUTER JOIN [$(Acquisition)].IntraBet.tblDeletedTransactions d		ON DL.TransactionID=D.TransactionID and DL.FromDate=d.FromDate
		LEFT OUTER JOIN [$(Acquisition)].IntraBet.tblTransactionLogs tl			ON DL.TransactionID=tl.TransactionID and DL.FromDate=tl.FromDate
		OPTION (RECOMPILE)


	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey, 'Sp_ClientTransfers', NULL, 'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_ClientTransfers', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
END

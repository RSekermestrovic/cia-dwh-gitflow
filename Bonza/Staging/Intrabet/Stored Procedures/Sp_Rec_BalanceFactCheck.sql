﻿CREATE PROC [Intrabet].[Sp_Rec_BalanceFactCheck]
(
	@BatchKey	int
)

WITH RECOMPILE
AS

BEGIN

--Declare @BatchKey INT 
--SET @BatchKey = 314;
EXEC [Control].Sp_Log	@BatchKey,'Sp_Rec_BalanceFactCheck','Initialise current transactions','Start'
-------------------------------------------------------------------------------
---Current Batch Transactions that needs to be used throughout the process-----
-------------------------------------------------------------------------------

SELECT	BatchKey,
		BalanceTypeID,
		AccountId,
		AccountIDSource,
		CASE 
		WHEN WalletId = 0 THEN WalletID
		ELSE 1
		END AS WalletId,
		TransactedAmount,
		MasterDayText,
		FromDate,
		MasterTransactionTimestamp
INTO #Txns
FROM [Stage].[Transaction]
Where BatchKey = @BatchKey
and ExistsInDim = 0

Select	
	BatchKey,
	BalanceTypeID,
	AccountId,
	AccountIDSource,
	WalletId,
	MasterDayText,
	AnalysisDayText,
	SUM(IsNull(TransactedAmount,0))	as TransactedAmount	
INTO #SummedTransactionsAnalysis
From
(
	Select	a.*,
			--CASE
			--WHEN BalanceName = 'Hold' THEN 'UNS'
			--ELSE IsNull(b.BalanceTypeID,-1) 
			--END AS BalanceTypeID, 
			CONVERT(VARCHAR(11),CONVERT(DATE,DATEADD(MI,Z.OffsetMinutes,a.MasterTransactionTimestamp)))	AS AnalysisDayText			
	From #Txns a WITH (NOLOCK)
	--LEFT OUTER JOIN Dimensional.PreProd.BalanceType b WITH (NOLOCK) on a.BalanceName = b.BalanceTypeName
	INNER JOIN Reference.TimeZone Z	WITH (NOLOCK) ON a.MasterTransactionTimestamp>=Z.FromDate AND a.MasterTransactionTimestamp<Z.ToDate AND Z.TimeZone='Analysis'
) a
Group By
	BatchKey,
	BalanceTypeID,
	AccountId,
	AccountIDSource,
	WalletId,
	MasterDayText,
	AnalysisDayText
	--option(fast 1000)
----------------------------------------------------------------------------
----------------------------------------------------------------------------


EXEC [Control].Sp_Log	@BatchKey,'Sp_Rec_BalanceFactCheck','Time Boundary Analysis','Start'


-----------------------------------------------------------------------------
------------------------CHECK BATCH FOR TIME BOUNDARY CROSSING---------------
-----------------------------------------------------------------------------

DECLARE @PreviousAnalysisDayText VARCHAR(11)
DECLARE @LastDayKey INT
DECLARE @NextAnalysisDayText VARCHAR(11)

SET @PreviousAnalysisDayText = null
SET @PreviousAnalysisDayText = 
(
-----IF CROSSED DAY BOUNDARY FOR ANALYSIS-----
Select	
		--a.BatchFromDate,
		--a.BatchToDate,
		--b.FromDate,
		--b.ToDate,
		--b.OffsetMinutes,
		--DATEADD(MI,-1*OffsetMinutes,(DATEADD(DD,1,CONVERT(DATETIME,CONVERT(DATE,BatchToDate))))) as DateTimeReference,
		--CONVERT(VARCHAR(11),DATEADD(dd,1,BatchToDate)) as AnalysisDayText,
		--DateDiff(dd,BatchFromDate, BatchToDate)
		CONVERT(VARCHAR(11),BatchToDate) as AnalysisDayText
From [Control].ETLController a
INNER JOIN [Reference].[TimeZone] b on a.BatchToDate >= b.FromDate and a.BatchToDate < b.ToDate
LEFT OUTER JOIN [$(Acquisition)].[IntraBet].[Latency] c on a.BatchToDate = c.OriginalDate
Where BatchKey = @BatchKey
and b.TimeZone = 'Analysis'
and IsNull(c.CorrectedDate,BatchToDate) >= DATEADD(MI,-1*OffsetMinutes,(DATEADD(DD,1,CONVERT(DATETIME,CONVERT(DATE,IsNull(c.CorrectedDate,BatchToDate)))))) -- UNCOMMENT WHEN DONE--!!!##@#@
)


IF @PreviousAnalysisDayText IS NOT NULL
BEGIN

----IF SO, THEN GET DATA FROM DIM AND LOAD----

SET @NextAnalysisDayText = CONVERT(VARCHAR(11),DATEADD(DD,1,CONVERT(DATE,@PreviousAnalysisDayText)))
SET @LastDayKey = (Select MAX(DayKey) From [$(Dimensional)].Dimension.DayZone
Where AnalysisDayText = @PreviousAnalysisDayText and MasterDayText = 'N/A')

Select	@BatchKey as BatchKey,
		BT.BalanceTypeID,
		a.AccountID,
		a.AccountIDSource,
		W.WalletId,
		'N/A' as MasterDayText,
		@NextAnalysisDayText as AnalysisDayText,
		OpeningBalance,
		TotalTransactedAmount as TransactedAmount,
		OpeningBalance as ClosingBalance,
		CONVERT(DateTime,@NextAnalysisDayText) as FromDate
INTO #LastBalance
From [$(Dimensional)].Fact.Balance a
INNER JOIN [$(Dimensional)].Dimension.BalanceType BT ON a.BalanceTypeKey = BT.BalanceTypeKey
INNER JOIN [$(Dimensional)].Dimension.Wallet W ON a.WalletKey = W.WalletKey
Where DayKey = @LastDayKey


Select	BalanceTypeKey AS BalanceKey,
		CASE WHEN WalletKey = 0 THEN 0 ELSE 1 END AS WalletKey,
		DayKey,
		AccountID,
		TransactedAmount
INTO #AdditionalAnalysisTime_
From [$(Dimensional)].Fact.[Transaction]
Where DayKey IN
(
	Select	Distinct
			DayKey
	From [$(Dimensional)].Dimension.DayZone
	Where AnalysisDayText = @PreviousAnalysisDayText
	and MasterDayKey <> -1
	and RIGHT(DayKey,1) <> 3
)
and CreatedBatchKey < @BatchKey --- for interday

-----------add time that occurred in current batch but not in analysis time---------

INSERT INTO #AdditionalAnalysisTime_
Select BalanceTypeKey, WalletKey, DayKey, AccountId, TransactedAmount 
From #SummedTransactionsAnalysis a
INNER JOIN [$(Dimensional)].Dimension.DayZone b on a.MasterDayText = b.MasterDayText and a.AnalysisDayText = b.AnalysisDayText
INNER JOIN [$(Dimensional)].Dimension.Wallet W on a.WalletId = W.WalletId
INNER JOIN [$(Dimensional)].Dimension.BalanceType Bl on a.BalanceTypeID = Bl.BalanceTypeID
Where DayKey IN
(
	Select DayKey
	From [$(Dimensional)].Dimension.DayZone
	Where MasterDayText = @PreviousAnalysisDayText and AnalysisDayText = @PreviousAnalysisDayText---don't want anthing that occured only in analysis
	and RIGHT(DayKey,1) = 0
)
------------------------------------------------------------------------------------

Select	AccountID,
		BalanceTypeID,
		WalletId,
		AnalysisDayText,
		SUM(TransactedAmount) as TransactedAmount
INTO #AdditionalAnalysisSummary_
From #AdditionalAnalysisTime_ a WITH(NOLOCK)
INNER JOIN [$(Dimensional)].Dimension.DayZone b WITH(NOLOCK) on a.DayKey = b.DayKey
INNER JOIN [$(Dimensional)].Dimension.BalanceType BT WITH(NOLOCK) ON a.BalanceKey = BT.BalanceTypeKey
INNER JOIN [$(Dimensional)].Dimension.Wallet W WITH(NOLOCK) ON a.WalletKey = W.WalletKey
Group By
	AccountID,
	BalanceTypeID,
	WalletId,
	AnalysisDayText

INSERT INTO [Stage].[Balance] WITH(TABLOCK)
Select	a.BatchKey,
		a.BalanceTypeID,
		a.AccountId,
		a.AccountIdSource,
		a.WalletID,
		'N/A' as MasterDayText,
		a.AnalysisDayText,
		IsNull(a.OpeningBalance,0) + IsNull(b.TransactedAmount,0) as OpeningBalance,
		a.TransactedAmount,
		IsNull(a.OpeningBalance,0) + IsNull(b.TransactedAmount,0) as ClosingBalance,
		a.FromDate,
		--DateAdd(MINUTE,-1*Z.OffSetMinutes,a.FromDate) as MasterTimeStamp
		a.FromDate as MasterTimeStamp
From #LastBalance a 
LEFT OUTER JOIN #AdditionalAnalysisSummary_ b on a.AccountId = b.AccountID and a.BalanceTypeID = b.BalanceTypeID and a.WalletID = b.WalletId
and a.AnalysisDayText = b.AnalysisDayText
--INNER JOIN Staging.Reference.TimeZone Z ON @ToDate>=Z.FromDate AND @ToDate<Z.ToDate AND Z.TimeZone='Analysis'

DROP TABLE #LastBalance
DROP TABLE #AdditionalAnalysisTime_
DROP TABLE #AdditionalAnalysisSummary_

END

--------------------------------------------------------------
---------END ANAYLIS BASE ADD IF DAY BOUNDARY CROSS-----------
--------------------------------------------------------------

----------------------------------------------------------
----------------BEGIN NORMAL ANALYSIS ADD-----------------
----------------------------------------------------------
EXEC [Control].Sp_Log	@BatchKey,'Sp_Rec_BalanceFactCheck','Normal Analysis','Start'

--------------Get Current Batch already as master in stage------------
Select * 
INTO #Unpivoted
From Stage.Balance
Where BatchKey = @BatchKey
and AnalysisDayText = 'N/A'
and MasterDayText NOT IN (ISNULL(@NextAnalysisDayText,'9999-09-09'))

--------------Get distinct Master Day Keys from Current Batch------------
Select DayKey
INTO #MasterDayKey
From [$(Dimensional)].Dimension.DayZone
Where AnalysisDayText IN (Select DISTINCT MasterDayText From #Unpivoted)
and MasterDayText <> 'N/A'
and RIGHT(DayKey,1) = 1

-------------Less the time difference---------------
Select	BalanceTypeKey AS BalanceKey,
		--AccountKey,
		CASE WHEN WalletKey = 0 THEN 0 ELSE 1 END AS WalletKey,
		t.DayKey,
		AccountID,
		TransactedAmount
INTO #AdditionalAnalysisTime
From [$(Dimensional)].Fact.[Transaction] t
INNER JOIN #MasterDayKey m on m.DayKey = t.DayKey
and CreatedBatchKey < @BatchKey

INSERT INTO #AdditionalAnalysisTime
Select BalanceTypeKey, WalletKey, b.DayKey, AccountId, TransactedAmount 
From #SummedTransactionsAnalysis a
INNER JOIN [$(Dimensional)].Dimension.DayZone b on a.MasterDayText = b.MasterDayText and a.AnalysisDayText = b.AnalysisDayText
INNER JOIN [$(Dimensional)].Dimension.Wallet W on a.WalletId = W.WalletId
INNER JOIN [$(Dimensional)].Dimension.BalanceType Bl on a.BalanceTypeID = Bl.BalanceTypeID
INNER JOIN #MasterDayKey m on m.DayKey = b.DayKey

Select	AccountID,
		BalanceTypeID,
		WalletId,
		AnalysisDayText,
		SUM(TransactedAmount) as TransactedAmount
INTO #AdditionalAnalysisSummary
From #AdditionalAnalysisTime a WITH(NOLOCK)
INNER JOIN [$(Dimensional)].Dimension.DayZone b WITH(NOLOCK) on a.DayKey = b.DayKey
INNER JOIN [$(Dimensional)].Dimension.BalanceType BT WITH(NOLOCK) ON a.BalanceKey = BT.BalanceTypeKey
INNER JOIN [$(Dimensional)].Dimension.Wallet W WITH(NOLOCK) ON a.WalletKey = W.WalletKey
Group By
	AccountID,
	BalanceTypeID,
	WalletId,
	AnalysisDayText


INSERT INTO [Stage].[Balance] WITH(TABLOCK)
Select	a.BatchKey,
		a.BalanceTypeID,
		a.AccountId,
		a.AccountIdSource,
		a.WalletID,
		'N/A' as MasterDayText,
		MasterDayText as AnalysisDayText,
		IsNull(a.OpeningBalance,0) - IsNull(b.TransactedAmount,0) as OpeningBalance,
		a.TransactedAmount,
		IsNull(a.OpeningBalance,0) - IsNull(b.TransactedAmount,0) as ClosingBalance,
		a.FromDate,
		--DateAdd(MINUTE,-1*Z.OffSetMinutes,a.MasterTimeStamp) as MasterTimeStamp
		a.MasterTimeStamp
From #Unpivoted a 
LEFT OUTER JOIN #AdditionalAnalysisSummary b on a.AccountId = b.AccountID and a.BalanceTypeID = b.BalanceTypeID and a.WalletID = b.WalletId
and a.MasterDayText = b.AnalysisDayText
--INNER JOIN Staging.Reference.TimeZone Z ON a.MasterTimeStamp>=Z.FromDate AND a.MasterTimeStamp<Z.ToDate AND Z.TimeZone='Analysis'


DROP TABLE #Unpivoted
DROP TABLE #AdditionalAnalysisTime
DROP TABLE #AdditionalAnalysisSummary

--------------------------------------------------------------
---------------------END ANALYSIS LOAD------------------------
--------------------------------------------------------------
EXEC [Control].Sp_Log	@BatchKey,'Sp_Rec_BalanceFactCheck','Transactions Checking','Start'
-----------------------------------------------------
----------------------Get Combined-------------------
-----------------------------------------------------

Select * 
INTO #CurrentBalance
From [$(Dimensional)].Fact.Balance
Where AccountID IN
(
Select Distinct AccountId From #SummedTransactionsAnalysis
)
and DayKey IN
(
	Select	Distinct
			DayKey 
	From #SummedTransactionsAnalysis c
	INNER JOIN [$(Dimensional)].Dimension.DayZone b on c.AnalysisDayText = b.AnalysisDayText and b.MasterDayText = 'N/A'
	UNION
	Select	Distinct
			DayKey 
	From #SummedTransactionsAnalysis c
	INNER JOIN [$(Dimensional)].Dimension.DayZone b on c.MasterDayText = b.MasterDayText and b.AnalysisDayText = 'N/A'
)
--and AccountID NOT IN
--(
--Select Distinct AccountID From #Unpivoted
--)

Select	BatchKey,
		BalanceTypeID,
		AccountID,
		AccountIDSource,
		WalletId,
		MasterDayText,
		AnalysisDayText,
		OpeningBalance,
		SUM(TransactedAmount) as TransactedAmount,
		OpeningBalance + SUM(TransactedAmount) as ClosingBalance,
		FromDate,
		BalanceTypeKey
INTO #NewTransactions_PreFactTxn
From
(
	Select	@BatchKey as BatchKey,
			BT.BalanceTypeID,
			a.AccountID,
			a.AccountIDSource,
			W.WalletId,
			b.MasterDayText,
			b.AnalysisDayText as AnalysisDayText,
			a.OpeningBalance,
			IsNull(c.TransactedAmount,0) as TransactedAmount,
			IsNull(a.OpeningBalance,0) + IsNull(c.TransactedAmount,0) as ClosingBalance,
			--c.FromDate
			CASE WHEN b.MasterDayText IN ('UNK','N/A') THEN GetDate() ELSE CONVERT(DateTime, b.MasterDayText) END AS FromDate,
			BT.BalanceTypeKey
	
	From #CurrentBalance a WITH(NOLOCK)
	INNER HASH JOIN [$(Dimensional)].Dimension.DayZone b WITH(NOLOCK) on a.DayKey = b.DayKey
	INNER HASH JOIN [$(Dimensional)].Dimension.BalanceType BT WITH(NOLOCK) ON a.BalanceTypeKey = BT.BalanceTypeKey
	INNER HASH JOIN [$(Dimensional)].Dimension.Wallet W WITH(NOLOCK) ON a.WalletKey = W.WalletKey
	INNER HASH JOIN #SummedTransactionsAnalysis c WITH(NOLOCK) on a.AccountID = c.AccountId and a.AccountIDSource = c.AccountIDSource and b.MasterDayText = c.MasterDayText
									and	BT.BalanceTypeID = c.BalanceTypeID 	and W.WalletId = c.WalletId  --and IsNull(b.AnalysisDayText,0) = IsNull(c.AnalysisDayText,0)
	Where b.AnalysisDayText = 'N/A'
) a
Group By
	BatchKey,
	BalanceTypeID,
	AccountID,
	AccountIDSource,
	WalletId,
	MasterDayText,
	AnalysisDayText,
	OpeningBalance,
	FromDate,
	BalanceTypeKey
--Select * 
--INTO #CurrentBalanceAnalysis
--From Dimensional.PreProd.Balance
--Where AccountID IN
--(
--Select Distinct AccountId From #SummedTransactionsAnalysis
--)
--and DayKey IN
--(
--Select	Distinct
--		DayKey 
--From #SummedTransactionsAnalysis c
--INNER JOIN Dimensional.PreProd.DayZone b on c.AnalysisDayText = b.AnalysisDayText and b.MasterDayText is null
--)

Select	@BatchKey as BatchKey,
		BT.BalanceTypeID,
		a.AccountID,
		a.AccountIDSource,
		W.WalletId,
		c.MasterDayText,
		c.AnalysisDayText as AnalysisDayText,
		a.OpeningBalance,
		IsNull(c.TransactedAmount,0) as TransactedAmount,
		IsNull(a.OpeningBalance,0) + IsNull(c.TransactedAmount,0) as ClosingBalance,
		--c.FromDate
		CASE WHEN b.AnalysisDayText IN ('UNK','N/A') THEN GetDate() ELSE CONVERT(DateTime, b.AnalysisDayText) END AS FromDate,
		BT.BalanceTypeKey
INTO #NewTransactions_PreFactTxn_Analysis
From #CurrentBalance a WITH(NOLOCK)
INNER HASH JOIN [$(Dimensional)].Dimension.DayZone b WITH(NOLOCK) on a.DayKey = b.DayKey
INNER HASH JOIN [$(Dimensional)].Dimension.BalanceType BT WITH(NOLOCK) ON a.BalanceTypeKey = BT.BalanceTypeKey
INNER HASH JOIN [$(Dimensional)].Dimension.Wallet W WITH(NOLOCK) ON a.WalletKey = W.WalletKey
INNER HASH JOIN #SummedTransactionsAnalysis c WITH(NOLOCK) on a.AccountID = c.AccountId and a.AccountIDSource = c.AccountIDSource and b.AnalysisDayText = c.AnalysisDayText
								and	BT.BalanceTypeID = c.BalanceTypeID 	and W.WalletId = c.WalletId  --and IsNull(b.AnalysisDayText,0) = IsNull(c.AnalysisDayText,0)
Where b.MasterDayText = 'N/A'

-------------------------------------------------------------------------
--------------------------See if existing Txns exist---------------------

--Select	AccountID,
--		DayKey,
--		BalanceKey,
--		CASE WHEN T.WalletKey = 0 THEN 0 ELSE 1 END AS WalletKey,
--		IsNull(T.TransactedAmount,0) as TransactedAmount
--INTO #FactTxn
--From Dimensional.Fact.[Transaction] T
--Where DayKey IN
--(
--Select Distinct DayKey From #NewTransactions_PreFactTxn a
--INNER JOIN Dimensional.PreProd.DayZone b2 on a.MasterDayText = b2.MasterDayText and a.MasterDayText = b2.AnalysisDayText
--)
--and T.CreatedBatchKey < @BatchKey

Select	AccountID,
		DayKey,
		BalanceTypeKey AS BalanceKey,
		CASE WHEN T.WalletKey = 0 THEN 0 ELSE 1 END AS WalletKey,
		IsNull(T.TransactedAmount,0) as TransactedAmount
INTO #FactTxn_Analysis
From [$(Dimensional)].Fact.[Transaction] T
Where DayKey IN
(
	Select Distinct DayKey From [$(Dimensional)].Dimension.DayZone
	Where MasterDayText IN
	(
		Select Distinct b2.AnalysisDayText From #NewTransactions_PreFactTxn_Analysis a
		INNER JOIN [$(Dimensional)].Dimension.DayZone b2 on a.AnalysisDayText = b2.AnalysisDayText --and a.MasterDayText = b2.AnalysisDayText
		and RIGHT(DayKey,1) <> 3
		UNION
		Select Distinct b2.MasterDayText From #NewTransactions_PreFactTxn a
		INNER JOIN [$(Dimensional)].Dimension.DayZone b2 on a.MasterDayText = b2.MasterDayText and a.MasterDayText = b2.AnalysisDayText
	)
	OR
	AnalysisDayText IN
	(
		Select Distinct b2.AnalysisDayText From #NewTransactions_PreFactTxn_Analysis a
		INNER JOIN [$(Dimensional)].Dimension.DayZone b2 on a.AnalysisDayText = b2.AnalysisDayText --and a.MasterDayText = b2.AnalysisDayText
		and RIGHT(DayKey,1) <> 3
		UNION
		Select Distinct b2.MasterDayText From #NewTransactions_PreFactTxn a
		INNER JOIN [$(Dimensional)].Dimension.DayZone b2 on a.MasterDayText = b2.MasterDayText and a.MasterDayText = b2.AnalysisDayText
	)
)
and T.CreatedBatchKey < @BatchKey

-------------SUM THE EXISTING TXN's-------------
--Select	AccountID,
--		DayKey,
--		BalanceKey,
--		WalletKey,
--		SUM(TransactedAmount) as TransactedAmount
--INTO #FactTxnSUM
--From #FactTxn
--Group By 
--	AccountID,
--	DayKey,
--	BalanceKey,
--	WalletKey

Select	AccountID,
		DayKey,
		BalanceKey,
		WalletKey,
		SUM(TransactedAmount) as TransactedAmount
INTO #FactTxnSUM_Analysis
From #FactTxn_Analysis
Group By 
	AccountID,
	DayKey,
	BalanceKey,
	WalletKey
		

Select	BatchKey,
		BalanceTypeID,
		a.AccountID,
		a.AccountIDSource,
		a.WalletId,
		a.MasterDayText,
		a.AnalysisDayText as AnalysisDayText,
		a.OpeningBalance,
		a.TransactedAmount + IsNull(T.TransactedAmount,0) as TransactedAmount,
		a.OpeningBalance + a.TransactedAmount + IsNull(T.TransactedAmount,0) as ClosingBalance,
		a.FromDate,
		a.FromDate as MasterTimeStamp
INTO #NewTransactions
From #NewTransactions_PreFactTxn a
LEFT OUTER JOIN 
(
	Select	a.AccountID,
			a.BalanceKey,
			a.WalletKey,
			b2.MasterDayText,
			SUM(TransactedAmount) as TransactedAmount
	From #FactTxnSUM_Analysis a
	INNER JOIN [$(Dimensional)].Dimension.DayZone b2 on a.DayKey = b2.DayKey
	Group By	a.AccountID,
				a.BalanceKey,
				a.WalletKey,
				b2.MasterDayText
) T on	a.AccountId = T.AccountID and a.MasterDayText = T.MasterDayText and a.BalanceTypeKey = T.BalanceKey
											and a.WalletId = T.WalletKey
----here----
--Select	BatchKey,
--		BalanceTypeID,
--		a.AccountID,
--		a.AccountIDSource,
--		a.WalletId,
--		null as MasterDayText,
--		a.AnalysisDayText as AnalysisDayText,
--		a.OpeningBalance,
--		a.TransactedAmount + IsNull(T.TransactedAmount,0) as TransactedAmount,
--		a.OpeningBalance + a.TransactedAmount + IsNull(T.TransactedAmount,0) as ClosingBalance,
--		a.FromDate
--INTO #NewTransactionsAnalysis
--From #NewTransactions_PreFactTxn_Analysis a
--INNER JOIN Dimensional.PreProd.DayZone b2 on a.MasterDayText = b2.MasterDayText and a.AnalysisDayText = b2.AnalysisDayText
--LEFT OUTER JOIN #FactTxnSUM_Analysis T on	a.AccountId = T.AccountID and b2.DayKey = T.DayKey and a.BalanceTypeKey = T.BalanceKey
--											and a.WalletId = T.WalletKey

Select	BatchKey,
		BalanceTypeID,
		a.AccountID,
		a.AccountIDSource,
		a.WalletId,
		'N/A' as MasterDayText,
		a.AnalysisDayText as AnalysisDayText,
		a.OpeningBalance,
		a.TransactedAmount + IsNull(T.TransactedAmount,0) as TransactedAmount,
		a.OpeningBalance + a.TransactedAmount + IsNull(T.TransactedAmount,0) as ClosingBalance,
		a.FromDate,
		a.FromDate as MasterTimeStamp
INTO #NewTransactionsAnalysis
From 
(
Select	BatchKey, BalanceTypeID, AccountID, AccountIDSource, WalletId, 'N/A' as MasterDayText, AnalysisDayText, OpeningBalance,
		SUM(TransactedAmount) as TransactedAmount,
		OpeningBalance + SUM(TransactedAmount) as ClosingBalance,
		FromDate, BalanceTypeKey
From #NewTransactions_PreFactTxn_Analysis
Group By BatchKey, BalanceTypeID, AccountID, AccountIDSource, WalletId,AnalysisDayText, OpeningBalance,
		FromDate, BalanceTypeKey
) a
LEFT OUTER JOIN 
(
Select	a.AccountID,
		a.BalanceKey,
		a.WalletKey,
		b2.AnalysisDayText,
		SUM(TransactedAmount) as TransactedAmount
From #FactTxnSUM_Analysis a
INNER JOIN [$(Dimensional)].Dimension.DayZone b2  on a.DayKey = b2.DayKey
Group By a.AccountID,
		a.BalanceKey,
		a.WalletKey,
		b2.AnalysisDayText
) T on a.AccountId = T.AccountID and a.AnalysisDayText = T.AnalysisDayText and a.BalanceTypeKey = T.BalanceKey
											and a.WalletId = T.WalletKey


---------------------------------------------------------------------------------------------------------------
--------------------------------------------UPDATE NEW ITEMS---------------------------------------------------
---------------------------------------------------------------------------------------------------------------

EXEC [Control].Sp_Log	@BatchKey,'Sp_Rec_BalanceFactCheck','Update Existing','Start'

Update b
Set b.TransactedAmount = IsNull(b.TransactedAmount,0) + IsNull(a.TransactedAmount,0),
	b.ClosingBalance = IsNull(b.ClosingBalance,0) + IsNull(a.TransactedAmount,0)
from 
(
Select	BatchKey,
		BalanceTypeID,
		AccountId,
		AccountIDSource,
		WalletId,
		MasterDayText,
		'N/A' as AnalysisDayText,
		SUM(TransactedAmount) as  TransactedAmount
From #SummedTransactionsAnalysis
Group By
	BatchKey,
	BalanceTypeID,
	AccountId,
	AccountIDSource,
	WalletId,
	MasterDayText
) a
INNER JOIN [Stage].[Balance] b on a.BalanceTypeID = b.BalanceTypeID
								and a.AccountId = b.AccountID and a.AccountIDSource = b.AccountIDSource and a.WalletId = b.WalletId 
								and a.MasterDayText = b.MasterDayText and b.AnalysisDayText = 'N/A'
Where b.BatchKey = (Select MAX(BatchKey) From #SummedTransactionsAnalysis)


Update b
Set b.TransactedAmount = IsNull(b.TransactedAmount,0) + IsNull(a.TransactedAmount,0),
	b.ClosingBalance = IsNull(b.ClosingBalance,0) + IsNull(a.TransactedAmount,0)
from 
(
Select	BatchKey,
		BalanceTypeID,
		AccountId,
		AccountIDSource,
		WalletId,
		'N/A' as MasterDayText,
		AnalysisDayText,
		SUM(TransactedAmount) as  TransactedAmount
From #SummedTransactionsAnalysis
Group By
	BatchKey,
	BalanceTypeID,
	AccountId,
	AccountIDSource,
	WalletId,
	AnalysisDayText
) a
INNER JOIN [Stage].[Balance] b on a.BalanceTypeID = b.BalanceTypeID
								and a.AccountId = b.AccountID and a.AccountIDSource = b.AccountIDSource and a.WalletId = b.WalletId 
								--and a.MasterDayText = b.MasterDayText 
								and a.AnalysisDayText = b.AnalysisDayText and b.MasterDayText = 'N/A'
Where b.BatchKey = (Select MAX(BatchKey) From #SummedTransactionsAnalysis)


---------------------------------------------------------------------------------------------------------------
--------------------------------------------INSERT NEW ITEMS---------------------------------------------------
---------------------------------------------------------------------------------------------------------------

EXEC [Control].Sp_Log	@BatchKey,'Sp_Rec_BalanceFactCheck','Final Loading New Items','Start'

/*  AJ - 01/09 - "Id rather ask for forgiveness then ask for permission"
	Get list of ids for a given batch
	load into temp table
	drive query from this temp table
*/

SELECT DISTINCT
	BatchKey,
	AccountID,
	AccountIDSource,
	BalanceTypeID,
	WalletId,
	MasterDayText,
	AnalysisDayText
INTO #Candidate
FROM Stage.Balance
WHERE BatchKey = @BatchKey;

INSERT INTO [Stage].[Balance] WITH(TABLOCK)
Select 
		a.BatchKey,
		a.BalanceTypeID,
		a.AccountID,
		a.AccountIDSource,
		a.WalletId,
		a.MasterDayText,
		a.AnalysisDayText,
		a.OpeningBalance,
		a.TransactedAmount,
		a.ClosingBalance,
		a.FromDate,
		a.MasterTimeStamp
From #NewTransactions a
LEFT OUTER JOIN #Candidate b on	a.BatchKey = b.BatchKey and a.AccountID = b.AccountID and a.AccountIDSource = b.AccountIDSource
									and	a.BalanceTypeID = b.BalanceTypeID and a.WalletId = b.WalletId and a.MasterDayText = b.MasterDayText
Where b.AccountID is null
OPTION (RECOMPILE);


INSERT INTO [Stage].[Balance] WITH(TABLOCK)
Select 	a.BatchKey,
		a.BalanceTypeID,
		a.AccountID,
		a.AccountIDSource,
		a.WalletId,
		a.MasterDayText,
		a.AnalysisDayText,
		a.OpeningBalance,
		a.TransactedAmount,
		a.ClosingBalance,
		a.FromDate,
		a.MasterTimeStamp
From #NewTransactionsAnalysis a
LEFT OUTER JOIN #Candidate b on	a.BatchKey = b.BatchKey and a.AccountID = b.AccountID and a.AccountIDSource = b.AccountIDSource
									and	a.BalanceTypeID = b.BalanceTypeID and a.WalletId = b.WalletId and a.AnalysisDayText = b.AnalysisDayText
Where b.AccountID IS NULL
OPTION (RECOMPILE);


EXEC [Control].Sp_Log	@BatchKey,'Sp_Rec_BalanceFactCheck',NULL,'Success'


END
GO
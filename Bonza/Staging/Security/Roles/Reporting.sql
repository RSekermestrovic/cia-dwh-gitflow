﻿CREATE ROLE [Reporting]
go
Grant Select on [control].ETLController to [Reporting]
go
Grant Select on [Reference].[TimeZone] to [Reporting]
go